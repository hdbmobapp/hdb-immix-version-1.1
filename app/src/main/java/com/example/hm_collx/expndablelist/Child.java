package com.example.hm_collx.expndablelist;

public class Child {

	private String Name;
	private String total_users;
	private String punches;
	private String total_pay;
	private String acitive_users;
	private String user_id;
	private String flag;
	private String str_region;
    private String str_Location;


	private double str_latitude;
	private double str_longitude;

	private String str_date_time;

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String gettotal_users() {
		return total_users;
	}

	public void settotal_users(String total_users) {
		this.total_users = total_users;
	}
	public String gettotal_pay() {
		return total_pay;
	}

	public void settotal_pay(String total_pay) {
		this.total_pay = total_pay;
	}
	
	public String getpunches() {
		return punches;
	}

	public void setpunches(String punches) {
		this.punches = punches;
	}
	public String getacitive_users() {
		return acitive_users;
	}

	public void setacitive_users(String acitive_users) {
		this.acitive_users = acitive_users;
	}
	
	public String getusers_id() {
		return user_id;
	}

	public void setusers_id(String user_id) {
		this.user_id = user_id;
	}

	public String getflag() {
		return flag;
	}

	public void setflag(String flag) {
		this.flag = flag;
	}
	
	public String getregion() {
		return str_region;
	}

	public void setregion(String str_region) {
		this.str_region = str_region;
	}
	
	public double getLattitude() {
		return str_latitude;
	}

	public double setLatitude(double str_latitude) {
		return this.str_latitude = str_latitude;
	}
	public double getLongtude() {
		return str_longitude;
	}

	public double setLongitude(double str_longitude) {
		return this.str_longitude = str_longitude;
	}
	
	public String getdatetime() {
		return str_date_time;
	}

	public void setdatetime(String str_date_time) {
		this.str_date_time = str_date_time;
	}

    public String getLocation() {
        return str_Location;
    }

    public void setLocation(String str_Location) {
        this.str_Location = str_Location;
    }


}
