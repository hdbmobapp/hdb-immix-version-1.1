package com.example.hm_collx.expndablelist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.ACMTypes;
import com.example.hdb.R;

import java.util.ArrayList;

public class ExpandListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<Group> groups;
	View veiw;

	public ExpandListAdapter(Context context, ArrayList<Group> groups) {
		this.context = context;
		this.groups = groups;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		ArrayList<Child> chList = groups.get(groupPosition).getItems();
		return chList.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final Child child = (Child) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_item, null);

		}
		veiw = convertView;

		TextView tv_name = (TextView) convertView.findViewById(R.id.txt_name);
		TextView tv_total_users = (TextView) convertView
				.findViewById(R.id.txt_total_users);
		TextView tv_active_users = (TextView) convertView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) convertView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) convertView
				.findViewById(R.id.txt_total_pay);

		TextView tv_locate_me = (TextView) convertView
				.findViewById(R.id.txt_locate_me);

		tv_locate_me.setVisibility(View.GONE);

		tv_name.setText(child.getName().toString());
		tv_total_users.setText("Users  "  + System.getProperty ("line.separator")+ child.gettotal_users().toString());
		tv_active_users.setText("A Users  " + System.getProperty ("line.separator")
				+ child.getacitive_users().toString());
		tv_total_punches.setText("Punches " + System.getProperty ("line.separator") + child.getpunches().toString());
		tv_total_pay.setText("Pay " + System.getProperty ("line.separator") + child.gettotal_pay().toString());

		convertView.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent intent = new Intent(context, ACMTypes.class);
				intent.putExtra("str_region", child.getName().toString());
				intent.putExtra("from", "expand");
				intent.putExtra("total_users", child.gettotal_users()
						.toString());
				intent.putExtra("active_users", child.getacitive_users()
						.toString());
				intent.putExtra("total_punches", child.getpunches().toString());
				intent.putExtra("total_pay", child.gettotal_pay().toString());
				intent.putExtra("name", child.getName().toString());
				
				context.startActivity(intent);
				// popup(veiw);

			}
		});
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		ArrayList<Child> chList = groups.get(groupPosition).getItems();
		return chList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		Group group = (Group) getGroup(groupPosition);
		if (convertView == null) {
			@SuppressWarnings("static-access")
			LayoutInflater inf = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inf.inflate(R.layout.group_item, null);
		}

		TextView tv_indicator = (TextView) convertView
				.findViewById(R.id.indicator_icon);

		if (isExpanded) {
			tv_indicator.setText("-");
		} else {
			tv_indicator.setText("+");
		}

		TextView tv = (TextView) convertView.findViewById(R.id.group_name);
		tv.setText(group.getName());
		TextView tv_total_users = (TextView) convertView
				.findViewById(R.id.txt_total_users_cnt);
		TextView tv_active_users = (TextView) convertView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) convertView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) convertView
				.findViewById(R.id.txt_total_pay);

		TextView tv_locate = (TextView) convertView
				.findViewById(R.id.txt_locate);
		tv_locate.setVisibility(View.GONE);

		tv_total_users.setText("Users" + System.getProperty ("line.separator") + group.gettotal_users().toString());
		tv_active_users.setText("A Users" + System.getProperty ("line.separator")
				+ group.getacitive_users().toString());
		tv_total_punches.setText("Punches"  + System.getProperty ("line.separator")+ group.getpunches().toString());
		tv_total_pay.setText("Pay"  + System.getProperty ("line.separator")+ group.gettotal_pay().toString());
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public Void popup(View v) {

		/** Instantiating PopupMenu class */
		PopupMenu popup = new PopupMenu(context, v);

		popup.getMenu().add("Sales");
		popup.getMenu().add("Collections");
		/** Adding menu items to the popumenu */
		// popup.getMenuInflater().inflate(R.menu.popup, popup.getMenu());

		/** Defining menu item click listener for the popup menu */
		popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

			@Override
			public boolean onMenuItemClick(MenuItem item) {
				Toast.makeText(context,
						"You selected the action : " + item.getTitle(),
						Toast.LENGTH_SHORT).show();
				return true;
			}
		});

		/** Showing the popup menu */
		popup.show();
		return null;
	}

}
