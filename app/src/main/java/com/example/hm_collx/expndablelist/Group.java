package com.example.hm_collx.expndablelist;

import java.util.ArrayList;

public class Group {

	private String Name;
	private ArrayList<Child> Items;
	private String Id;
	private Integer total_child;

	private String total_users;
	private String punches;
	private String total_pay;
	private String acitive_users;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public ArrayList<Child> getItems() {
		return Items;
	}

	public void setItems(ArrayList<Child> Items) {
		this.Items = Items;
	}

	public String getId() {
		return Id;
	}

	public void setId(String Id) {
		this.Id = Id;
	}

	public Integer getTotalchild() {
		return total_child;
	}

	public void setTotalchild(Integer total_child) {
		this.total_child = total_child;
	}

	public String gettotal_users() {
		return total_users;
	}

	public void settotal_users(String total_users) {
		this.total_users = total_users;
	}

	public String gettotal_pay() {
		return total_pay;
	}

	public void settotal_pay(String total_pay) {
		this.total_pay = total_pay;
	}

	public String getpunches() {
		return punches;
	}

	public void setpunches(String punches) {
		this.punches = punches;
	}

	public String getacitive_users() {
		return acitive_users;
	}

	public void setacitive_users(String acitive_users) {
		this.acitive_users = acitive_users;
	}
}