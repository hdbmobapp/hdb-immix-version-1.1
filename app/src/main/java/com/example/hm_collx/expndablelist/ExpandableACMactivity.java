package com.example.hm_collx.expndablelist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.EditRequest;
import com.example.hdb.HdbHome;
import com.example.hdb.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class ExpandableACMactivity extends Activity {
	ProgressDialog PD;
	ConnectionDetector cd;
	private ExpandAcmListAdapter ExpAdapter;
	private ExpandableListView ExpandList;
	JSONObject jsonobject;
	UserFunctions userFunction;
	ArrayList<Group> list;
	ProgressDialog mProgressDialog;
	String category_id;
	JSONArray jsonarray;
	ProgressBar linlaHeaderProgress;
	ExpandableListView listView;
	TextView text_error_msg;
	static String str_from = null;
	String str_region = null;
	String str_supervisor_id = null;

	String str_total_users = null;
	String str_total_punches = null;
	String str_active_users = null;
	String str_total_pay = null;
	String str_name = null;
    String str_location = null;
	static String str_type = null;

	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expndablelist_main);
		cd = new ConnectionDetector(getApplicationContext());

		ExpandList = (ExpandableListView) findViewById(R.id.exp_list);
		ExpandList.setGroupIndicator(null);
		text_error_msg = (TextView) findViewById(R.id.text_error_msg);

		linlaHeaderProgress = (ProgressBar) findViewById(R.id.new_loading);
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

		userFunction = new UserFunctions();
		userFunction.checkforLogout(pref, ExpandableACMactivity.this);

		Intent myIntent = getIntent();

		str_from = myIntent.getStringExtra("from");
		str_region = myIntent.getStringExtra("str_region");
		str_name = myIntent.getStringExtra("name");
		str_type = myIntent.getStringExtra("type");

		if (str_from.compareTo("acm") == 0) {
			str_supervisor_id = myIntent.getStringExtra("supervisor_id");
		}
		str_total_users = myIntent.getStringExtra("total_users");
		str_active_users = myIntent.getStringExtra("active_users");
		str_total_pay = myIntent.getStringExtra("total_pay");
		str_total_punches = myIntent.getStringExtra("total_punches");

		// TextView v = new TextView(this);
		// v.setGravity(Gravity.CENTER);
		// v.setTextSize(40);
		// v.setHeight(100);
		// v.setBackgroundResource(R.drawable.hardware);

		// ImageView iv = new ImageView(this);
		// int width = 60;
		// int height = 60;
		// RelativeLayout.LayoutParams parms = new
		// RelativeLayout.LayoutParams(width,height);
		// iv.setLayoutParams(parms);
		// iv.setBackgroundResource(R.drawable.hardware);
		// listView.addParallaxedHeaderView(v);

		if (cd.isConnectingToInternet()) {

			new DownloadJSON().execute();
		} else {
			text_error_msg.setText(getResources().getString(
					R.string.no_internet));
			text_error_msg.setVisibility(View.VISIBLE);
		}

	}

	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			linlaHeaderProgress.setVisibility(View.VISIBLE);

		}

		@Override
		protected Void doInBackground(Void... params) {
				jsonobject = userFunction.getACMList(str_region, "A", str_type);
    			System.out.println("jsonobjectad:::" + jsonobject);

			if (jsonobject != null) {
				// text_error_msg.setVisibility(View.GONE);
				list = new ArrayList<Group>();
				ArrayList<Child> ch_list;

				try {
					// jsonarray = jsonobject.getJSONArray("Data");

					// for (int k = 0; k < jsonarray.length(); k++) {
					// jsonobject = jsonarray.getJSONObject(k);

					Group gru = new Group();
					// int child_size = jsonobject.getInt("child_size");
					// gru.setTotalchild(jsonobject.getInt("child_size"));

					// gru.setName(jsonobject.getString("region"));
					// gru.settotal_users(jsonobject.getString("total_users"));
					// gru.setacitive_users(jsonobject.getString("active_users"));
					// gru.setpunches(jsonobject.getString("Punches"));
					// gru.settotal_pay(jsonobject.getString("pay"));
					if (str_from.compareTo("acm") == 0) {
						gru.setName(str_name);

					} else {
						gru.setName(str_type);
					}
					gru.settotal_users(str_total_users);
					gru.setacitive_users(str_active_users);
					gru.setpunches(str_total_punches);
					gru.settotal_pay(str_total_pay);

					ch_list = new ArrayList<Child>();

					// map.put("flag", jsonobject.getString("flag"));
					// map.put("latitude", jsonobject.getString("latitude"));
					// map.put("longitude", jsonobject.getString("longitude"));
					// map.put("datetime", jsonobject.getString("datetime"));
					// map.put("region", jsonobject.getString("region"));

					// System.out.println("group:::"+gru.getName().matches("foot wear"));

					/*
					 * Iterator<String> key = jsonobject.keys(); while
					 * (key.hasNext()) { String k = key.next();
					 *
					 * Group gru = new Group(); gru.setName(k); ch_list = new
					 * ArrayList<Child>();
					 *
					 * JSONArray ja = jsonobject.getJSONArray(k);
					 */
					// yes

					JSONArray category = (JSONArray) jsonobject
							.getJSONArray("ACMList");
					for (int i = 0; i < category.length(); i++) {

						JSONObject jo = category.getJSONObject(i);
						Child ch = new Child();

						ch.setName(jo.getString("user_name"));
						ch.setusers_id(jo.getString("emp_id"));

						ch.settotal_users(jo.getString("total_users"));
						ch.setacitive_users(jo.getString("active_users"));
						ch.setpunches(jo.getString("total_punches"));
						ch.settotal_pay(jo.getString("total_pay"));
						ch.setflag(jo.getString("flag"));
						ch.setregion(jo.getString("region"));
						ch.setLatitude(jo.getDouble("latitude"));
						ch.setLongitude(jo.getDouble("longitude"));
						ch.setdatetime(jo.getString("datetime"));
                        ch.setLocation(jo.getString("Location"));

						ch_list.add(ch);
						System.out.println("child:::jo::" + ch);

					} // for loop end
					gru.setItems(ch_list);

					list.add(gru);

					System.out.println("list::::" + ch_list);
					// } // while loop end

				} catch (JSONException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			System.out.println("groups:::" + list);
			if (jsonobject == null) {
				// userFunction.cutomToast(getResources().getString(R.string.error_message),getApplicationContext());
				text_error_msg.setText(getResources().getString(
						R.string.error_message));
				text_error_msg.setVisibility(View.VISIBLE);
			} else {
				ExpAdapter = new ExpandAcmListAdapter(
						ExpandableACMactivity.this, list);
				ExpandList.setAdapter(ExpAdapter);
			}
			linlaHeaderProgress.setVisibility(View.GONE);
			// ExpandList.expandGroup(0);

			// PD.dismiss();

		}
	}

	public void go_home_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);

	}

	public void go_dataEntry_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				EditRequest.class);
		startActivity(home_activity);

	}
}
