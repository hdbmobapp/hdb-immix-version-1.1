package com.example.hm_collx.expndablelist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.EditRequest;
import com.example.hdb.HdbHome;
import com.example.hdb.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class ExpandMainActivity extends Activity {
	ProgressDialog PD;
	ConnectionDetector cd;
    JSONObject jsonobject=null;
	UserFunctions userFunction;
	ArrayList<Group> list;
	ProgressDialog mProgressDialog;
	String category_id;
	JSONArray jsonarray;
	ProgressBar linlaHeaderProgress;
	ExpandableListView listView;
	TextView text_error_msg;

	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expndablelist_main);
		cd = new ConnectionDetector(getApplicationContext());

        ExpandableListView expandList = (ExpandableListView) findViewById(R.id.exp_list);
		expandList.setGroupIndicator(null);
		linlaHeaderProgress = (ProgressBar) findViewById(R.id.new_loading);
		
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

		userFunction = new UserFunctions();
		userFunction.checkforLogout(pref, ExpandMainActivity.this);

		Intent myIntent = getIntent();
		category_id = myIntent.getStringExtra("cat_id");
		listView = (ExpandableListView) findViewById(R.id.exp_list);
		text_error_msg = (TextView) findViewById(R.id.text_error_msg);

		// TextView v = new TextView(this);
		// v.setGravity(Gravity.CENTER);
		// v.setTextSize(40);
		// v.setHeight(100);
		// v.setBackgroundResource(R.drawable.hardware);

		// ImageView iv = new ImageView(this);
		// int width = 60;
		// int height = 60;
		// RelativeLayout.LayoutParams parms = new
		// RelativeLayout.LayoutParams(width,height);
		// iv.setLayoutParams(parms);
		// iv.setBackgroundResource(R.drawable.hardware);
		// listView.addParallaxedHeaderView(v);
		if (cd.isConnectingToInternet()) {

			new DownloadJSON().execute();
		} else {
			text_error_msg.setText(getResources().getString(
					R.string.no_internet));
			text_error_msg.setVisibility(View.VISIBLE);
		}

	}

	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			linlaHeaderProgress.setVisibility(View.VISIBLE);

		}

		@Override
		protected Void doInBackground(Void... params) {
			jsonobject = userFunction.GetPanIndiaData();
			System.out.println("jsonobjectad:::"+jsonobject);
			if (jsonobject != null) {
				text_error_msg.setVisibility(View.GONE);
				list = new ArrayList<Group>();
				ArrayList<Child> ch_list;

				try {
					// jsonarray = jsonobject.getJSONArray("Data");

					// for (int k = 0; k < jsonarray.length(); k++) {
					// jsonobject = jsonarray.getJSONObject(k);

					Group gru = new Group();
					// int child_size = jsonobject.getInt("child_size");
					// gru.setTotalchild(jsonobject.getInt("child_size"));

					gru.setName(jsonobject.getString("region"));
					gru.settotal_users(jsonobject.getString("total_users"));
					gru.setacitive_users(jsonobject.getString("active_users"));
					gru.setpunches(jsonobject.getString("Punches"));
					gru.settotal_pay(jsonobject.getString("pay"));

					ch_list = new ArrayList<Child>();

					// System.out.println("group:::"+gru.getName().matches("foot wear"));

					/*
					 * Iterator<String> key = jsonobject.keys(); while
					 * (key.hasNext()) { String k = key.next();
					 *
					 * Group gru = new Group(); gru.setName(k); ch_list = new
					 * ArrayList<Child>();
					 *
					 * JSONArray ja = jsonobject.getJSONArray(k);
					 */
					// yes

					JSONArray category = (JSONArray) jsonobject
							.getJSONArray("child");
					for (int i = 0; i < category.length(); i++) {

						JSONObject jo = category.getJSONObject(i);
						Child ch = new Child();

						ch.setName(jo.getString("region"));
						ch.settotal_users(jo.getString("total_users"));
						ch.setacitive_users(jo.getString("active_users"));
						ch.setpunches(jo.getString("Punches"));
						ch.settotal_pay(jo.getString("pay"));

						ch_list.add(ch);
						System.out.println("child:::jo::" + ch);

					} // for loop end
					gru.setItems(ch_list);

					list.add(gru);

					System.out.println("list::::" + ch_list);
					// } // while loop end

				} catch (JSONException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			System.out.println("groups:::" + list);
			if (jsonobject == null) {
				// userFunction.cutomToast(getResources().getString(R.string.error_message),getApplicationContext());
				text_error_msg.setText(getResources().getString(
						R.string.error_message));
				text_error_msg.setVisibility(View.VISIBLE);
			} else {
                ExpandListAdapter expAdapter = new ExpandListAdapter(ExpandMainActivity.this,
                        list);
				listView.setAdapter(expAdapter);
			}
			linlaHeaderProgress.setVisibility(View.GONE);
			// PD.dismiss();
			//ExpandList.expandGroup(0);
		}
	}
	public void go_home_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);

	}
	
	public void go_dataEntry_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				EditRequest.class);
		startActivity(home_activity);

	}
}
