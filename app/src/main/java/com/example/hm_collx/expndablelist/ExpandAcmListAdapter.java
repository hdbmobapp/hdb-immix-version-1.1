package com.example.hm_collx.expndablelist;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;

import java.util.ArrayList;

public class ExpandAcmListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<Group> groups;

	public ExpandAcmListAdapter(Context context, ArrayList<Group> groups) {
		this.context = context;
		this.groups = groups;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		ArrayList<Child> chList = groups.get(groupPosition).getItems();
		return chList.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final Child child = (Child) getChild(groupPosition, childPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.child_item, null);

		}

		TextView tv_name = (TextView) convertView.findViewById(R.id.txt_name);
		TextView tv_total_users = (TextView) convertView
				.findViewById(R.id.txt_total_users);
		TextView tv_active_users = (TextView) convertView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) convertView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) convertView
				.findViewById(R.id.txt_total_pay);
		TextView tv_locate_me = (TextView) convertView
				.findViewById(R.id.txt_locate_me);

        TextView tv_locattion = (TextView) convertView
                .findViewById(R.id.txt_location);

		if (ExpandableACMactivity.str_from.compareTo("acm") == 0) {
			tv_locate_me.setVisibility(View.VISIBLE);
		}
		tv_name.setText(child.getName().toString());
		tv_total_users.setText("Users  " + System.getProperty("line.separator")
				+ child.gettotal_users().toString());
		tv_active_users.setText("A Users  "
				+ System.getProperty("line.separator")
				+ child.getacitive_users().toString());
		tv_total_punches.setText("Punches "
				+ System.getProperty("line.separator")
				+ child.getpunches().toString());
		tv_total_pay.setText("Pay " + System.getProperty("line.separator")
				+ child.gettotal_pay().toString());

        if (!child.getLocation().toString().equals("null")) {
            tv_locattion.setText("( " + child.getLocation().toString() + " )");
        }
        tv_locate_me.setVisibility(View.INVISIBLE);
		tv_locate_me.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);

				final double latitude = child.getLattitude();
				final double longitude = child.getLongtude();
				final String label = child.getdatetime().toString();
				if (latitude == 0) {
					show();
				}

				new Thread(new Runnable() {
					public void run() {
						// Get the position

						String uriBegin = "geo:" + latitude + "," + longitude;
						String query = latitude + "," + longitude + "(" + label
								+ ")";
						String encodedQuery = Uri.encode(query);
						String uriString = uriBegin + "?q=" + encodedQuery
								+ "&z=16";
						Uri uri = Uri.parse(uriString);

						if (latitude > 0) {
							Intent intent = new Intent(
									Intent.ACTION_VIEW, uri);

							try {
								context.startActivity(intent);
							} catch (ActivityNotFoundException ex) {
								try {
									Intent unrestrictedIntent = new Intent(
											Intent.ACTION_VIEW, uri);
									context.startActivity(unrestrictedIntent);
								} catch (ActivityNotFoundException innerEx) {
									Toast.makeText(
											context,
											"Please install a maps application",
											Toast.LENGTH_LONG).show();
								}
							}
						}

					}
				}).start();
			}

		});

		convertView.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				/*
				 * str_latitude =null; str_longitude=null;
				 * 
				 * Intent intent = null;
				 * System.out.println("lopop::::"+child.getIslast
				 * ().toString()+"child:::"+child.getId().toString());
				 * if(child.getIslast().toString().compareTo("1")==0){ intent =
				 * new Intent(context, ExpandableListView.class); Bundle args =
				 * new Bundle(); args.putString("from","mainactivity" );
				 * args.putString("cat_id","");
				 * args.putString("lattitude",str_latitude);
				 * args.putString("longitude",str_longitude );
				 * args.putString("radius","20" );
				 * 
				 * intent.putExtras(args);
				 * 
				 * }else{ intent = new Intent(context,
				 * ExpandMainActivity.class); intent.putExtra("cat_id",
				 * child.getId().toString()); } context.startActivity(intent);
				 */

				Intent intent = new Intent(context, ExpandableCOCA_Activity.class);
				if (child.getflag().toString().compareTo("A") == 0) {
					intent.putExtra("acm", "expand");

					intent.putExtra("str_region", child.getregion().toString());
					System.out.println("superid::::"
							+ child.getusers_id().toString());
					intent.putExtra("from", "acm");
					intent.putExtra("supervisor_id", child.getusers_id()
							.toString());
					intent.putExtra("name", child.getName().toString());

					intent.putExtra("total_users", child.gettotal_users()
							.toString());
					intent.putExtra("active_users", child.getacitive_users()
							.toString());
					intent.putExtra("total_punches", child.getpunches()
							.toString());
					intent.putExtra("total_pay", child.gettotal_pay()
							.toString());

					context.startActivity(intent);
				}

			}
		});
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		ArrayList<Child> chList = groups.get(groupPosition).getItems();
		return chList.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		Group group = (Group) getGroup(groupPosition);
		if (convertView == null) {
			@SuppressWarnings("static-access")
			LayoutInflater inf = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inf.inflate(R.layout.group_item, null);
		}

		TextView tv_indicator = (TextView) convertView
				.findViewById(R.id.indicator_icon);

		if (isExpanded) {
			tv_indicator.setText("-");
		} else {
			tv_indicator.setText("+");
		}

		TextView tv = (TextView) convertView.findViewById(R.id.group_name);
		tv.setText(group.getName());
		TextView tv_total_users = (TextView) convertView
				.findViewById(R.id.txt_total_users_cnt);
		TextView tv_active_users = (TextView) convertView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) convertView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) convertView
				.findViewById(R.id.txt_total_pay);

		TextView tv_locate = (TextView) convertView
				.findViewById(R.id.txt_locate);
		tv_locate.setVisibility(View.GONE);

		tv_total_users.setText("Users"
				+ System.getProperty("line.separator")
				+ group.gettotal_users().toString());
		tv_active_users.setText("A Users"
				+ System.getProperty("line.separator")
				+ group.getacitive_users().toString());
		tv_total_punches.setText("Punches"
				+ System.getProperty("line.separator")
				+ group.getpunches().toString());
		tv_total_pay.setText("Pay" + System.getProperty("line.separator")
				+ group.gettotal_pay().toString());
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public void show() {
		Toast.makeText(context, "cant find ....", Toast.LENGTH_LONG).show();

	}
}
