package com.example.img_upload_retrofit;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageInterface {
    @Multipart
    @POST("/HDBFS_APPS/PD_COLLX/COLLX/image_upload.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file);
}