package com.example.vijay.finone.libraries;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by obaro on 02/04/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    ProgressDialog mProgressDialog;

    String str_losid, str_userid, str_prod, str_const, str_branch, str_cust_name, str_cibil, str_pd_place, person_met, str_emp_name,
            str_industry, str_emp_catg, str_off_setup, str_no_emp, str_off_address, str_designation, str_dept, str_job_role, str_qual,
            str_yrs_job, str_total_exp, str_gross_sal, str_landmark, str_area_name, str_picode, str_product, str_setup, str_industry_se, str_buz_typ, str_gross_profit,
            str_per_income, str_hts_profile, str_ts, str_pd_coll, str_coll, str_recomm,
            str_emi_month, str_cc_os, str_buz_income, str_income_rent, str_income_agri, str_total_income, str_surplus, str_loan_usage, str_recomm_amt, str_remarks, turnover, net_profit, str_cu_key_row_id,
            str_lat, str_long, str_entry_type, pic_count, str_trans, str_depend, str_act_turnover, str_fixed_cost, str_season_buz, str_buz_months, str_turnover, str_net_profit, gross_profit, map_addr_cust, map_addr_coll;


    String coll_losid, coll_prod, coll_cust_name, coll_person_met, coll_relation, coll_owner_name, coll_collateral, coll_prop_type, coll_prop_status, coll_bank_notice, coll_redev_notice, coll_socoff, coll_tenant, coll_prop_access,
            coll_prop_area_match, coll_prop_area, coll_market, coll_ref1, coll_ref2, coll_rat1, coll_rate2, str_make, str_varient, str_model, str_serial, str_kilo_run, str_distance_type, str_distance_km, str_coll_distance, str_coll_distance_km, coll_userid, coll_mapp_addr;


    ProgressDialog dialog;

    JSONObject jObj = null;
    String json = null;

    public static final String DATABASE_NAME = "SQLitePD_len.db";
    private static final int DATABASE_VERSION = 2;

    public static final String TBL_CUST = "tbl_cust_master";
    public static final String TBL_COLL = "tbl_coll_master";
    private static final String DATABASE_PICTURE_TABLE = "key_pic_master";
    private static final String DATABASE_PD_TABLE = "key_pd_master";
    private static final String DATABASE_COLL_TABLE = "key_coll_master";

    private static final String DATABASE_CONS_TABLE = "key_cons_master";

    JSONObject jsonobject;


    //customer table

    public static final String COLUMN_ID = "_id";
    public static final String LOSID = "losid";
    public static final String PRODUCT = "product";
    public static final String CONST = "const";
    public static final String BRANCH = "branch";
    public static final String CUST_NAME = "cust_name";
    public static final String CIBIL_SCORE = "cib_score";
    public static final String PD_PLACE = "pd_place";
    public static final String PERSON_MET = "person_met";
    public static final String FIRM_NAME = "firm_name";
    public static final String EMP_CATG = "emp_catg";
    public static final String INDUSTRY = "industry";
    public static final String OFFICE_SETUP = "off_setup";
    public static final String EMP_COUNT = "emp_count";
    public static final String OL_OFF_VALID = "ol_valid";
    public static final String DESIGNATION = "designation";
    public static final String DEPARTMENT = "department";
    public static final String JOB_ROLE = "job_role";
    public static final String JOB_STABIITY = "job_stability";
    public static final String TOTAL_WORK_EXP = "total_xp";
    public static final String NET_SAL = "net_sal";
    public static final String QUALIFICATION = "qual";
    public static final String LANDMARK = "landmark";
    public static final String AREA_NAME = "area_name";
    public static final String SETUP_SIZE = "setup_size";
    public static final String PINCODE = "pincode";
    public static final String INDUSTRY_SAL = "industry_sal";
    public static final String BUZ_TYPE = "buz_type";
    public static final String SERVICES = "services";
    public static final String TURNOVER = "turnover";
    public static final String GROSS_PROFIT = "gross_profit";
    public static final String NET_PROFIT = "net_profit";
    public static final String OBLI_EMI_MONTHLY = "obi_emi";
    public static final String CC_OS = "cc_os";
    public static final String OTH_BUZ_INCOME = "income_buz";
    public static final String OTH_INCOME_RENT = "income_rent";
    public static final String OTH_INCOME_AGRI = "income_agri";
    public static final String TOTAL_INCOME_PA = "total_income";
    public static final String TOTAL_SURPLUS = "total_surplus";
    public static final String PERCEIVED_INCOME = "perceived_income";
    public static final String HRP_NTS_PROFILE = "nts_profile";
    public static final String LOAN_END_USAGE = "oa_usage";
    public static final String PD_COLLATERAL = "pd_coll";
    public static final String COLLATERAL = "coll";
    public static final String PD_RECOMM = "pd_recomm";
    public static final String PD_RECOMM_AMT = "recomm_amt";
    public static final String PD_REMARKS = "remarks";

    public static final String LATTITUDE = "lat";
    public static final String LONGITDE = "long";
    public static final String PD_UPLOAD_BY = "uploaded_by";

    public static final String PHOTO_COUNT = "photo_count";

    public static final String FLAG = "flag";
    public static final String TS = "ts";
    public static final String DISTANCE_TYPE = "DISTANCE_TYPE";
    public static final String DISTANCE_KM = "DISTANCE_KM";

    public static final String TRANSFER = "transfer";
    public static final String DEPENDENTS = "dependents";
    public static final String ACT_TURNOVER = "act_turnover";
    public static final String FIXED_COST = "fixed_cost";
    public static final String SEASON_BUZ = "season_buz";
    public static final String SEASON_MONTHS = "season_months";
    public static final String STR_TURNOVER = "str_turnover";
    public static final String STR_NET_PROFIT = "str_net_profit";
    public static final String STR_GROSS_PROFIT = "str_gross_profit";

    public static final String CUST_MAP_ADDR = "cust_map_addr";


    //collateral table

    public static final String COLL_LOS = "coll_los";
    public static final String COLL_PROD = "coll_prod";
    public static final String COLL_BRANCH = "coll_branch";
    public static final String COLL_CUST = "COLL_CUST";
    public static final String COLL_PERSON = "COLL_PERSON";
    public static final String COLL_RELATION = "COLL_RELATION";
    public static final String COLL_OWNER = "COLL_OWNER";
    public static final String COLL_COLLATERAL = "COLL_COLLATERAL";
    public static final String COLL_PROPERTY_TYPE = "COLL_PROPERTY_TYPE";

    public static final String COLL_BANK_NOTICE = "COLL_BANK_NOTICE";
    public static final String COLL_REDEVLOPOE_NOTICE = "COLL_REDEVLOPOE_NOTICE";
    public static final String COLL_SOC_OFF_NOTUCE = "COLL_SOC_OFF_NOTUCE";
    public static final String COLL_TENANT_VINTAGE = "COLL_TENANT_VINTAGE";
    public static final String COLL_PROPERTY_ACCESS = "COLL_PROPERTY_ACCESS";
    public static final String COLL_PROPERTY_AREA_MATCH = "COLL_PROPERTY_AREA_MATCH";
    public static final String COLL_PROPERTY_AREA = "COLL_PROPERTY_AREA";
    public static final String COLL_MARKETABILITY = "COLL_MARKETABILITY";
    public static final String COLL_REFRENCE1 = "COLL_REFRENCE1";
    public static final String COLL_RATE_QUOTE1 = "COLL_RATE_QUOTE1";
    public static final String COLL_REFERENCE2 = "COLL_REFERENCE2";
    public static final String COLL_RATE_QUOTE2 = "COLL_RATE_QUOTE2";
    public static final String COLL_STATUS = "COLL_STATUS";
    public static final String COLL_UPLOAD_BY = "COLL_UPLOAD_BY";
    public static final String COLL_LAT = "COLL_LAT";
    public static final String COLL_LONG = "COLL_LONG";
    public static final String COLL_TS = "COLL_TS";
    public static final String COLL_PIC_COUNT = "COLL_PIC_COUNT";

    public static final String COLL_VEHICLE_MAKE = "COLL_VEHICLE_MAKE";
    public static final String COLL_VEHICLE_MODEL = "COLL_VEHICLE_MODEL";
    public static final String COLL_OWNER_SERIAL = "COLL_OWNER_SERIAL";
    public static final String COLL_KILO_RUN = "COLL_KILO_RUN";
    public static final String COLL_VARIENT = "COLL_VARIENT";

    public static final String COLL_SRNO = "COLL_SRNO";
    public static final String COLL_FLAG = "COLL_FLAG";

    public static final String COLL_DISTANCE_TYPE = "COLL_DISTANCE_TYPE";
    public static final String COLL_DISTANCE_KM = "COLL_DISTANCE_KM";

    public static final String COLL_MAP_ADDR = "cust_map_addr";

    //image table


    public static final String IMAGE_TABLE_NAME = "tbl_image_master";
    public static final String IMAGE_COLUMN_ID = "_id";
    public static final String IMAGE_LOSID = "img_losid";
    public static final String IMAGE_FORMNO = "img_formno";
    public static final String IMAGE_USERID = "img_userid";
    public static final String IMAGE_TS = "img_ts";
    public static final String IMAGE_MAPCOUNT = "img_map_count";
    public static final String IMAGE_STATUS = "img_status";
    public static final String IMAGE_LAST = "img_lastid";
    public static final String IMAGE_PATH = "img_path";
    public static final String IMAGE_CAPTION = "img_captions";

    //product table

    //PAY MODE TABLE

    public static final String KEY_pm_id = "pm_id";
    public static final String KEY_pay_mode = "pay_mode ";
    public static final String KEY_pm_flag = "pm_flag";
    public static final String KEY_pm_srno = "pm_srno";
    public static final String KEY_pm_val = "pm_val";

    //PRODUCT TABLE

    public static final String KEY_pd_id = "pd_id";
    public static final String KEY_product = "product";
    public static final String KEY_pd_flag = "pd_flag";
    public static final String KEY_pd_srno = "pd_srno";
    public static final String KEY_pd_val = "pd_val";

    //CONSTITUTION TABLE

    public static final String KEY_cons_id = "cons_id";
    public static final String KEY_cons = "cons";
    public static final String KEY_cons_flag = "cons_flag";
    public static final String KEY_cons_srno = "cons_srno";
    public static final String KEY_cons_val = "cons_val";

    public static final String KEY_STATUS = "status";
    private static String KEY_SUCCESS = "success";

    private static final String IMAGE_DIRECTORY_NAME = "/HDB";
    ConnectionDetector cd;
    String str_user_id;
    UserFunctions userFunction;
    SharedPreferences pref;
    Context context;
    private static int VERSION = 1;
    private static String DBNAME = "HDBHMCV";
    private SQLiteDatabase mDB;

    ArrayList<String> get_masters;

    ArrayList<String> get_flag;

    ArrayList<String> arr_pd, arr_pd_val, arr_cons, arr_cons_val;


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        userFunction = new UserFunctions();
        this.context = context;
        cd = new ConnectionDetector(context);
        this.mDB = getWritableDatabase();
        pref = context.getSharedPreferences("MyPref", 0);

        get_masters = new ArrayList<String>();

        get_flag = new ArrayList<String>();


        arr_pd = new ArrayList<String>();
        arr_pd_val = new ArrayList<String>();
        arr_cons = new ArrayList<String>();
        arr_cons_val = new ArrayList<String>();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        userFunction = new UserFunctions();

        db.execSQL(
                "CREATE TABLE " + TBL_CUST +
                        "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LOSID + " TEXT, " +
                        PRODUCT + " TEXT, " +
                        CONST + " TEXT, " +
                        BRANCH + " TEXT, " +
                        CUST_NAME + " TEXT, " +
                        CIBIL_SCORE + " TEXT, " +
                        PD_PLACE + " TEXT, " +
                        PERSON_MET + " TEXT, " +
                        FIRM_NAME + " TEXT, " +
                        EMP_CATG + " TEXT, " +
                        INDUSTRY + " TEXT, " +
                        OFFICE_SETUP + " TEXT, " +
                        EMP_COUNT + " TEXT, " +
                        OL_OFF_VALID + " TEXT, " +
                        DESIGNATION + " TEXT, " +
                        DEPARTMENT + " TEXT, " +
                        JOB_ROLE + " TEXT, " +
                        JOB_STABIITY + " TEXT, " +
                        TOTAL_WORK_EXP + " TEXT, " +
                        NET_SAL + " TEXT, " +
                        QUALIFICATION + " TEXT, " +
                        LANDMARK + " TEXT, " +
                        AREA_NAME + " TEXT, " +
                        SETUP_SIZE + " TEXT, " +
                        PINCODE + " TEXT, " +
                        INDUSTRY_SAL + " TEXT, " +
                        BUZ_TYPE + " TEXT, " +
                        SERVICES + " TEXT, " +
                        TURNOVER + " TEXT, " +
                        GROSS_PROFIT + " TEXT, " +
                        NET_PROFIT + " TEXT, " +
                        OBLI_EMI_MONTHLY + " TEXT, " +
                        CC_OS + " TEXT, " +
                        OTH_BUZ_INCOME + " TEXT, " +
                        OTH_INCOME_RENT + " TEXT, " +
                        OTH_INCOME_AGRI + " TEXT, " +
                        TOTAL_INCOME_PA + " TEXT, " +
                        TOTAL_SURPLUS + " TEXT, " +
                        PERCEIVED_INCOME + " TEXT, " +
                        HRP_NTS_PROFILE + " TEXT, " +
                        LOAN_END_USAGE + " TEXT, " +
                        PD_COLLATERAL + " TEXT, " +
                        COLLATERAL + " TEXT, " +
                        PD_RECOMM + " TEXT, " +
                        PD_RECOMM_AMT + " TEXT, " +
                        PD_REMARKS + " TEXT, " +
                        FLAG + " TEXT, " +
                        TS + " TEXT, " +
                        LATTITUDE + " TEXT, " +
                        LONGITDE + " TEXT, " +
                        PHOTO_COUNT + " TEXT , " +
                        PD_UPLOAD_BY + " TEXT , " +
                        TRANSFER + " TEXT , " +
                        DEPENDENTS + " TEXT , " +
                        ACT_TURNOVER + " TEXT , " +
                        FIXED_COST + " TEXT , " +
                        SEASON_BUZ + " TEXT , " +
                        SEASON_MONTHS + " TEXT ," +
                        DISTANCE_TYPE + " TEXT ,"
                        + DISTANCE_KM + " TEXT ," +
                        STR_TURNOVER + " TEXT ,"
                        + STR_NET_PROFIT + " TEXT ,"
                        + STR_GROSS_PROFIT + " TEXT ,"
                        + CUST_MAP_ADDR + "  TEXT )");

        String sql_coll_query = "create table " + TBL_COLL + " ( "

                + COLL_SRNO + " integer primary key autoincrement , "

                + COLL_LOS + " text , "
                + COLL_PROD + " text , " + COLL_BRANCH + " text , " + COLL_CUST + " text, "
                + COLL_PERSON + " text , " + COLL_RELATION + " text , " + COLL_OWNER + " text , " + COLL_COLLATERAL + " text , " + COLL_PROPERTY_TYPE + " text, "
                + COLL_STATUS + " text , " + COLL_BANK_NOTICE + " text , " + COLL_REDEVLOPOE_NOTICE + " text , " + COLL_SOC_OFF_NOTUCE + " text , " + COLL_TENANT_VINTAGE + " text, "
                + COLL_PROPERTY_ACCESS + " text , " + COLL_PROPERTY_AREA_MATCH + " text , " + COLL_PROPERTY_AREA + " text , " + COLL_MARKETABILITY + " text , " + COLL_REFRENCE1 + " text, "
                + COLL_RATE_QUOTE1 + " text , " + COLL_REFERENCE2 + " text , " + COLL_RATE_QUOTE2 + " text , " + COLL_UPLOAD_BY + " text , " + COLL_TS + " text , " + COLL_LAT + " text , " + COLL_LONG + " text , " + COLL_PIC_COUNT + " text , " + COLL_FLAG + " text, " +
                COLL_VEHICLE_MAKE + " text , " + COLL_VARIENT + " text , " + COLL_VEHICLE_MODEL + " text , " + COLL_OWNER_SERIAL + " text , " + COLL_KILO_RUN + " text ," + COLL_DISTANCE_TYPE + " text , " + COLL_DISTANCE_KM + " text , " + COLL_MAP_ADDR + " text )";

        System.out.print("QUERY TABLE:::" + sql_coll_query);
        db.execSQL(sql_coll_query);

        db.execSQL(
                "CREATE TABLE " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        IMAGE_LOSID + " TEXT, " +

                        IMAGE_USERID + " TEXT, " +
                        IMAGE_TS + " TEXT, " +
                        IMAGE_MAPCOUNT + " TEXT, " +

                        IMAGE_PATH + " TEXT, " +
                        IMAGE_CAPTION + " TEXT, " +
                        IMAGE_STATUS + " INTEGER)"
        );

        String sql_pd_query = "create table " + DATABASE_PD_TABLE + " ( "
                + KEY_pd_srno + " integer primary key autoincrement , "
                + KEY_product + " text , " + KEY_pd_val + " text , " + KEY_pd_id + " text, "
                + KEY_pd_flag + " text )";

        db.execSQL(sql_pd_query);


        String sql_cons_query = "create table " + DATABASE_CONS_TABLE + " ( "
                + KEY_cons_srno + " integer primary key autoincrement , "
                + KEY_cons + " text , " + KEY_cons_val + " text , " + KEY_cons_id + " text, "
                + KEY_cons_flag + " text )";

        db.execSQL(sql_cons_query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TBL_CUST);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_PICTURE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_PD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CONS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_COLL);
        onCreate(db);
    }


    public boolean update_cust_office_details(String industry, String emp_catg, String off_setup, String no_emp, String off_address, String desig, String dept,
                                              String job_role, String job_stability, String total_exp, String net_sal, String ts, String trans, String depend) {


        mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(INDUSTRY, industry);
        args.put(EMP_CATG, emp_catg);
        args.put(OFFICE_SETUP, off_setup);
        args.put(EMP_COUNT, no_emp);
        args.put(OL_OFF_VALID, off_address);
        args.put(DESIGNATION, desig);
        args.put(DEPARTMENT, dept);
        args.put(JOB_ROLE, job_role);
        args.put(JOB_STABIITY, job_stability);
        args.put(TOTAL_WORK_EXP, total_exp);
        args.put(NET_SAL, net_sal);
        args.put(PHOTO_COUNT, pic_count);
        args.put(TRANSFER, trans);
        args.put(DEPENDENTS, depend);


        Boolean res = false;

        //  =  mDB.update(TBL_CUST, args, null, null) > 0;

        res = mDB.update(TBL_CUST, args, TS + " = ? ", new String[]{ts}) > 0;

        //s = mDB.update(TBL_CUST, args, TS + " = " + ts, null) > 0;

        System.out.println("off_:::" + res);
        return true;
    }

    public boolean update_cust_income_details(String emi_month, String cc_os, String buz_income, String income_rent, String income_agri, String total_income, String surplus, String per_income, String nts_profile, String loan_usage, String pd_coll, String coll, String recomm,
                                              String recomm_amt, String remarks, String ts, String lat, String longs, String userid, String pic_count, String distance_type, String distance_km, String map_address) {
        mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(OBLI_EMI_MONTHLY, emi_month);
        args.put(CC_OS, cc_os);
        args.put(OTH_BUZ_INCOME, buz_income);
        args.put(OTH_INCOME_RENT, income_rent);
        args.put(OTH_INCOME_AGRI, income_agri);
        args.put(TOTAL_INCOME_PA, total_income);
        args.put(TOTAL_SURPLUS, surplus);
        args.put(OBLI_EMI_MONTHLY, emi_month);
        args.put(PERCEIVED_INCOME, per_income);
        args.put(HRP_NTS_PROFILE, nts_profile);
        args.put(LOAN_END_USAGE, loan_usage);
        args.put(PD_COLLATERAL, pd_coll);
        args.put(COLLATERAL, coll);
        args.put(PD_RECOMM, recomm);
        args.put(PD_RECOMM_AMT, recomm_amt);
        args.put(PD_REMARKS, remarks);
        args.put(LATTITUDE, lat);
        args.put(LONGITDE, longs);
        args.put(PD_UPLOAD_BY, userid);
        args.put(PHOTO_COUNT, pic_count);
        args.put(FLAG, "1");
        args.put(DISTANCE_TYPE, distance_type);
        args.put(DISTANCE_KM, distance_km);

        args.put(CUST_MAP_ADDR, map_address);
        Boolean res = false;

        res = mDB.update(TBL_CUST, args, TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income:::" + res);

        return true;
    }

    public Boolean update_property(String prop_type, String prop_status, String bank_notice, String redev_notice, String soc_off, String tenant_vintage, String prop_access, String prop_area_match, String prop_area, String market,
                                   String ref1, String ref2, String rate1, String rate2, String ts, String flag, String distance_type, String distance_km, String userid, String pics_count, String lat, String longs, String map_address) {

        System.out.println("PROPERTY TS::::" + ts);

        SQLiteDatabase db = this.getWritableDatabase();
        //  mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(COLL_PROPERTY_TYPE, prop_type);
        args.put(COLL_STATUS, prop_status);
        args.put(COLL_BANK_NOTICE, bank_notice);
        args.put(COLL_REDEVLOPOE_NOTICE, redev_notice);
        args.put(COLL_SOC_OFF_NOTUCE, soc_off);
        args.put(COLL_TENANT_VINTAGE, tenant_vintage);
        args.put(COLL_PROPERTY_ACCESS, prop_access);
        args.put(COLL_PROPERTY_AREA_MATCH, prop_area_match);
        args.put(COLL_PROPERTY_AREA, prop_area);
        args.put(COLL_MARKETABILITY, market);
        args.put(COLL_REFRENCE1, ref1);
        args.put(COLL_REFERENCE2, ref2);
        args.put(COLL_RATE_QUOTE1, rate1);
        args.put(COLL_RATE_QUOTE2, rate2);

        args.put(COLL_DISTANCE_TYPE, distance_type);
        args.put(COLL_DISTANCE_KM, distance_km);
        args.put(COLL_FLAG, flag);
        args.put(COLL_UPLOAD_BY, userid);
        args.put(COLL_PIC_COUNT, pics_count);
        args.put(COLL_LAT, lat);
        args.put(COLL_LONG, longs);
        args.put(COLL_MAP_ADDR, map_address);
        //   db.update(TBL_COLL, args, COLL_TS + " = ? ", new String[]{ts});
        Boolean res = db.update(TBL_COLL, args, COLL_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income:::" + res);
        return true;
    }

    public Boolean update_vehicle(String kilo_run, String model, String varient, String seriel, String make, String ts, String flag, String distance_type, String distance_km, String userid, String lat, String longs, String pic_count, String map_addr) {

        System.out.println("PROPERTY TS::::" + ts);

        SQLiteDatabase db = this.getWritableDatabase();
        //  mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(COLL_KILO_RUN, kilo_run);
        args.put(COLL_VEHICLE_MODEL, model);
        args.put(COLL_VARIENT, varient);
        args.put(COLL_OWNER_SERIAL, seriel);
        args.put(COLL_VEHICLE_MAKE, make);
        args.put(COLL_FLAG, flag);
        args.put(COLL_DISTANCE_TYPE, distance_type);
        args.put(COLL_DISTANCE_KM, distance_km);
        args.put(COLL_UPLOAD_BY, userid);

        args.put(COLL_LAT, lat);
        args.put(COLL_LONG, longs);
        args.put(COLL_PIC_COUNT, pic_count);
        args.put(COLL_MAP_ADDR, map_addr);
        //   db.update(TBL_COLL, args, COLL_TS + " = ? ", new String[]{ts});
        Boolean res = db.update(TBL_COLL, args, COLL_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income:::" + res);
        return true;
    }

    public long insert_cust_pd(String losid, String product, String cons, String branch, String cust_name, String cib_score, String pd_place, String person_met, String emp_name, String flag, String ts) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(LOSID, losid);
        contentValues.put(PRODUCT, product);
        contentValues.put(CONST, cons);
        contentValues.put(BRANCH, branch);

        contentValues.put(CUST_NAME, cust_name);
        contentValues.put(CIBIL_SCORE, cib_score);
        contentValues.put(PD_PLACE, pd_place);
        contentValues.put(PERSON_MET, person_met);
        contentValues.put(FIRM_NAME, emp_name);

        contentValues.put(TS, ts);
        contentValues.put(FLAG, "0");
        return db.insert(TBL_CUST, null, contentValues);

    }

    public String getCustomerPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + LOSID + ") FROM "
                + TBL_CUST + " WHERE " + FLAG + "= 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public String getCollPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + COLL_LOS + ") FROM " + TBL_COLL + " WHERE " + COLL_FLAG + "= 1 LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }


    public boolean update_cust_office_details_se(String landmark, String area_name, String setup_size, String pincode, String industry, String buz_type, String product, String turn_over,
                                                 String gross_profit, String net_profit, String ts, String act_turnover, String fixed_cost, String season_buz, String season_months, String str_turnover, String str_net_profit, String str_gross_profit, String str_services
    ) {
        System.out.println("SE TS::::" + ts);


        mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(LANDMARK, landmark);
        args.put(AREA_NAME, area_name);
        args.put(SETUP_SIZE, setup_size);
        args.put(PINCODE, pincode);
        args.put(INDUSTRY, industry);
        args.put(BUZ_TYPE, buz_type);
        args.put(INDUSTRY, product);
        args.put(TURNOVER, turn_over);
        args.put(GROSS_PROFIT, gross_profit);
        args.put(NET_PROFIT, net_profit);

        args.put(ACT_TURNOVER, act_turnover);
        args.put(FIXED_COST, fixed_cost);
        args.put(SEASON_BUZ, season_buz);
        args.put(SEASON_MONTHS, season_months);
        args.put(STR_TURNOVER, str_turnover);
        args.put(STR_NET_PROFIT, str_net_profit);
        args.put(STR_GROSS_PROFIT, str_gross_profit);
        args.put(SERVICES, str_services);

        Boolean res = false;

        res = mDB.update(TBL_CUST, args, TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("RES SE:::" + res);

        return true;
    }

    public String get_Customers_request(String ts, String entru_type) {

        System.out.println("TS CUST::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;
        if (ts != null) {
            System.out.println("TS NOT NULL");

            countQuery = "SELECT *  FROM " + TBL_CUST + " WHERE "
                    + FLAG + "= 1  AND " + TS + " = " + "'" + ts + "' LIMIT 1";

            System.out.println("QUERY CUST::::" + countQuery);

        } else {
            countQuery = "SELECT *  FROM " + TBL_CUST + " WHERE "
                    + FLAG + "= 1 LIMIT 1";

        }

        if (entru_type == "OFF") {
            str_entry_type = "OFF";
        } else {
            str_entry_type = "ON";
        }


        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    str_cu_key_row_id = cursor.getString(0);
                    str_losid = cursor.getString(1);
                    str_product = cursor.getString(2);
                    str_const = cursor.getString(3);
                    str_branch = cursor.getString(4);
                    str_cust_name = cursor.getString(5);
                    str_cibil = cursor.getString(6);
                    str_pd_place = cursor.getString(7);
                    person_met = cursor.getString(8);
                    str_emp_name = cursor.getString(9);
                    str_emp_catg = cursor.getString(10);
                    str_industry = cursor.getString(11);
                    str_off_setup = cursor.getString(12);
                    str_no_emp = cursor.getString(13);
                    str_off_address = cursor.getString(14);
                    str_designation = cursor.getString(15);
                    str_dept = cursor.getString(16);
                    str_job_role = cursor.getString(17);
                    str_yrs_job = cursor.getString(18);
                    str_total_exp = cursor.getString(19);
                    str_gross_sal = cursor.getString(20);
                    str_qual = cursor.getString(21);
                    str_landmark = cursor.getString(22);
                    str_area_name = cursor.getString(23);
                    str_setup = cursor.getString(24);
                    str_picode = cursor.getString(25);
                    str_industry_se = cursor.getString(26);
                    str_buz_typ = cursor.getString(27);
                    str_prod = cursor.getString(28);
                    turnover = cursor.getString(29);
                    str_gross_profit = cursor.getString(30);
                    net_profit = cursor.getString(31);
                    str_emi_month = cursor.getString(32);
                    str_cc_os = cursor.getString(33);
                    str_buz_income = cursor.getString(34);
                    str_income_rent = cursor.getString(35);
                    str_income_agri = cursor.getString(36);
                    str_total_income = cursor.getString(37);
                    str_surplus = cursor.getString(38);
                    str_per_income = cursor.getString(39);
                    str_hts_profile = cursor.getString(40);
                    str_loan_usage = cursor.getString(41);
                    str_pd_coll = cursor.getString(42);
                    str_coll = cursor.getString(43);
                    str_recomm = cursor.getString(44);
                    str_recomm_amt = cursor.getString(45);
                    str_remarks = cursor.getString(46);
                    str_ts = cursor.getString(48);
                    str_lat = cursor.getString(49);
                    str_long = cursor.getString(50);

                    pic_count = cursor.getString(51);
                    str_user_id = cursor.getString(52);
                    str_trans = cursor.getString(53);
                    str_depend = cursor.getString(54);
                    str_act_turnover = cursor.getString(55);
                    str_fixed_cost = cursor.getString(56);
                    str_season_buz = cursor.getString(57);
                    str_buz_months = cursor.getString(58);
                    str_coll_distance = cursor.getString(59);
                    str_coll_distance_km = cursor.getString(60);
                    str_turnover = cursor.getString(61);
                    str_net_profit = cursor.getString(62);
                    gross_profit = cursor.getString(63);
                    map_addr_cust = cursor.getString(64);

                }

                cursor.close();

                System.out.println("ROW ID ::::" + str_cu_key_row_id + "LOSID::::" + str_losid);

                if (str_losid == null || str_losid.equals("") || str_losid.equals("null")) {

                } else {
                    if (!cd.isConnectingToInternet()) {
                        userFunction.cutomToast("No internet connection...", context);

                    } else {
                        new InsertCustomorData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public String get_Collateral_request(String ts, String entru_type) {

        System.out.println("TS COLL::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;
        if (ts != null) {
            System.out.println("TS NOT NULL");

            countQuery = "SELECT *  FROM " + TBL_COLL + " WHERE "
                    + COLL_FLAG + "= 1  AND " + COLL_TS + " = " + "'" + ts + "' LIMIT 1";

            System.out.println("QUERY UPDT:::" + countQuery);


        } else {
            countQuery = "SELECT *  FROM " + TBL_COLL + " WHERE "
                    + COLL_FLAG + "= 1 LIMIT 1";

        }

        if (entru_type == "OFF") {
            str_entry_type = "OFF";
        } else {
            str_entry_type = "ON";
        }


        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    str_cu_key_row_id = cursor.getString(0);
                    coll_losid = cursor.getString(1);
                    coll_prod = cursor.getString(2);
                    coll_cust_name = cursor.getString(4);
                    coll_person_met = cursor.getString(5);
                    coll_relation = cursor.getString(6);
                    coll_owner_name = cursor.getString(7);
                    coll_collateral = cursor.getString(8);
                    coll_prop_type = cursor.getString(9);
                    coll_prop_status = cursor.getString(10);
                    coll_bank_notice = cursor.getString(11);
                    coll_redev_notice = cursor.getString(12);
                    coll_socoff = cursor.getString(13);
                    coll_tenant = cursor.getString(14);
                    coll_prop_access = cursor.getString(15);
                    coll_prop_area_match = cursor.getString(16);
                    coll_prop_area = cursor.getString(17);
                    coll_market = cursor.getString(18);
                    coll_ref1 = cursor.getString(19);
                    coll_rat1 = cursor.getString(20);
                    coll_ref2 = cursor.getString(21);
                    coll_rate2 = cursor.getString(22);
                    str_ts = cursor.getString(24);
                    str_lat = cursor.getString(25);
                    str_long = cursor.getString(26);

                    pic_count = cursor.getString(27);
                    str_user_id = cursor.getString(22);

                    str_make = cursor.getString(29);
                    str_varient = cursor.getString(30);
                    str_model = cursor.getString(31);
                    str_serial = cursor.getString(32);
                    str_kilo_run = cursor.getString(33);
                    str_distance_type = cursor.getString(34);
                    str_distance_km = cursor.getString(35);
                    coll_userid = cursor.getString(23);

                    coll_mapp_addr = cursor.getString(36);

                }

                cursor.close();


                System.out.println("ROW ID ::::" + str_cu_key_row_id + "LOSID::::" + coll_losid + "MARKET:::" + coll_market);

                if (coll_losid == null || coll_losid.equals("") || coll_losid.equals("null")) {

                } else {

                    if (!cd.isConnectingToInternet()) {
                        userFunction.cutomToast("No internet connection...", context);
                    } else {
                        new InsertCollateralData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public class InsertCustomorData extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            //   String str_pic_count = DBHelper.getCasePhotoCount(cu_ts);

            jsonobject = userFunction.InsertData(
                    str_losid,
                    str_product,
                    str_const,
                    str_branch,
                    str_cust_name,
                    str_cibil,
                    str_pd_place,
                    person_met,
                    str_emp_name,
                    str_emp_catg,
                    str_industry,
                    str_off_setup,
                    str_no_emp,
                    str_off_address,
                    str_designation,
                    str_dept,
                    str_job_role,
                    str_yrs_job,
                    str_total_exp,
                    str_gross_sal,
                    str_qual,
                    str_landmark,
                    str_area_name,
                    str_setup,
                    str_picode,
                    str_industry_se,
                    str_buz_typ,
                    str_prod,
                    turnover,
                    str_gross_profit,
                    net_profit,
                    str_emi_month,
                    str_cc_os,
                    str_buz_income,
                    str_income_rent,
                    str_income_agri,
                    str_total_income,
                    str_surplus,
                    str_per_income,
                    str_hts_profile,
                    str_loan_usage,
                    str_pd_coll,
                    str_coll,
                    str_recomm,
                    str_recomm_amt,
                    str_remarks, str_ts, str_lat, str_long, str_entry_type, pic_count, str_user_id, str_trans, str_depend, str_act_turnover, str_fixed_cost, str_season_buz, str_buz_months,
                    str_coll_distance, str_coll_distance_km, str_turnover, str_net_profit, gross_profit, map_addr_cust);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            deleteCustomerInfo(str_cu_key_row_id);
                            userFunction
                                    .cutomToast("Customer PD Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);
                        }else{
                            mProgressDialog.dismiss();

                            userFunction
                                    .cutomToast("Something gets wrong with connectivity..", context);

                            Intent dashActivity = new Intent(context,
                                    Dashboard.class);

                            context.startActivity(dashActivity);
                        }
                    }
                } else {

                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);

                    context.startActivity(dashActivity);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class InsertCollateralData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            //   String str_pic_count = DBHelper.getCasePhotoCount(cu_ts);

            jsonobject = userFunction.InsertData_Coll(
                    coll_losid, coll_prod, coll_cust_name, coll_person_met, coll_relation, coll_owner_name, coll_collateral, coll_prop_type, coll_prop_status, coll_bank_notice, coll_redev_notice, coll_socoff, coll_tenant, coll_prop_access, coll_prop_area_match,
                    coll_prop_area, coll_market, coll_ref1, coll_rat1, coll_ref2, coll_rate2, str_make, str_varient, str_model, str_serial, str_kilo_run, str_ts, str_lat, str_long, str_userid, pic_count, str_entry_type, str_distance_type, str_distance_km, coll_userid, coll_mapp_addr);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            deleteCollInfo(str_cu_key_row_id);
                            userFunction
                                    .cutomToast("Collateral PD Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);
                        }else{

                            mProgressDialog.dismiss();

                            userFunction
                                    .cutomToast("Something gets wrong with connectivity..", context);

                            Intent dashActivity = new Intent(context,
                                    Dashboard.class);

                            context.startActivity(dashActivity);
                        }
                    }
                } else {

                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);

                    context.startActivity(dashActivity);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteCustomerInfo(String key_row_id) {
        Log.d("DELETING", key_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(TBL_CUST, COLUMN_ID + "=" + " '" + key_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public boolean deleteCollInfo(String key_row_id) {
        Log.d("DELETING", key_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(TBL_COLL, COLL_SRNO + "=" + " '" + key_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }


    public boolean insertImage(String path, String losid, String userid, String ts, String map_count, String status, String image_cation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);

        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_MAPCOUNT, map_count);

        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);


        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean updateImage(String ts, int status) {

        System.out.println("UPDT IMG TS:::" + ts);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        db.update(IMAGE_TABLE_NAME, contentValues, IMAGE_TS + " = ? ", new String[]{ts});
        return true;
    }

    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + IMAGE_PATH + ") FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }

    public String getphoto() {

        SQLiteDatabase db = this.getReadableDatabase();
        String image_path = null;
        Integer image_id = null;

        String countQuery_val = "SELECT " + IMAGE_COLUMN_ID + "," + IMAGE_PATH + "  FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_val, null);

        System.out.println("countQuerymlop:" + countQuery_val);

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    image_id = cursor.getInt(0);
                    image_path = cursor.getString(1);

                    System.out.println("image_path:" + image_id);
                    System.out.println("image_path:image_path::::" + image_path);

                    if (image_path != null) {
                        upload_image(image_path);
                    } else {
                        System.out.println("image_path:" + image_path);

                        deleteImage(image_id);
                    }
                    // String cmp_iomage = compressImage(image_path, image_new_path, image_latitude, image_longitude);

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            cursor.close();
            db.close();

        }
        //     closeConnection();

        return image_path;
        // System.out.println("countQuery:"+product_qnt);
    }


    public void upload_image(String img_path) {

        System.out.println("img_path::::::" + img_path);
        if (img_path != null) {
            //	Log.d("uploading File : ", img_path);
            //  String new_img_path="/storage/emulated/0/PD/"+img_path+".jpg";
            // System.out.println("new_img_path::::::"+new_img_path);
            File file1 = new File(img_path);
            if (file1.exists()) {
                // Log.d("uploading File Esists : ", "true");
                System.out.println("img_path::::11222" + img_path);
                if (cd.isConnectingToInternet()) {
                    doFileUpload(img_path);
                } else {
                    userFunction.cutomToast("No internet connection...", context);
                }
            } else {
                //  System.out.println("img_path::::8877" + new_img_path);

                this.deletepic(img_path);
                this.deletepic(img_path);

            }
        } else {
            try {
                if (Integer.parseInt(this.getPendingPhotoCount()) == 0) {

                    File mediaStorageDir;
                    String extStorageDirectory = Environment
                            .getExternalStorageDirectory()
                            .toString();

                    // External sdcard location
                    mediaStorageDir = new File(
                            extStorageDirectory
                                    + IMAGE_DIRECTORY_NAME);

                    if (mediaStorageDir.isDirectory()) {
                        try {
                            String[] children = mediaStorageDir
                                    .list();
                            for (int i = 0; i < children.length; i++) {
                                new File(mediaStorageDir, children[i]).delete();
                            }

                        } catch (Exception e) {
                            // block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    public int doFileUpload(final String selectedFilePath) {
        File file1 = new File(selectedFilePath);

        int serverResponseCode = 0;
        String response = "";

        HttpsURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length - 1];

        if (!selectedFile.isFile()) {
            dialog.dismiss();

            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL("https://hdbapp.hdbfs.com/HDB_PD_LENDING_v2.9/img_upload.php");
                connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploadedfile1", selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile1\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();
                System.out.println("serverResponseMessage::" + serverResponseMessage);


                if (serverResponseCode == 200) {
                    String line;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "iso-8859-1"));
                    while ((line = bufferedReader.readLine()) != null) {
                        response += line;
                    }
                }

                if (response != null) {

                    try {

                        json = response.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    // try parse the string to a JSON object
                    try {
                        jObj = new JSONObject(json);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                } else {
                    jObj = null;
                }


                if (jObj != null) {
                    try {
                        System.out.println("response_str::::" + jObj);
                        if (jObj.getString(KEY_STATUS) != null) {
                            String res = jObj.getString(KEY_STATUS);
                            String resp_success = jObj.getString(KEY_SUCCESS);
                            if (Integer.parseInt(res) == 200
                                    && resp_success.equals("true")) {
                                deletepic(selectedFilePath);
                                file1.delete();
                            } else {
                                Log.i("RESPONSE MESSAGE ", jObj.getString("message").toString());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(context, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            //dialog.dismiss();
            return serverResponseCode;
        }
    }



    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_PATH + "=" + " '"
                    + path + "'", null) > 0;
            db.close();


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }

    public Integer deleteImage(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(IMAGE_TABLE_NAME,
                IMAGE_COLUMN_ID + " = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<String> get_masters() {

        SQLiteDatabase db = this.getReadableDatabase();


        String count_pd = "SELECT COUNT (" + KEY_product + ") FROM "
                + DATABASE_PD_TABLE;
        Cursor cursor1 = db.rawQuery(count_pd, null);

        String count_cons = "SELECT COUNT (" + KEY_cons + ") FROM "
                + DATABASE_CONS_TABLE;


        Cursor cursor2 = db.rawQuery(count_cons, null);

        System.out.println("cursor 1::" + cursor1 + "  Cursor 2" + cursor2);
        String cons_count = "";

        String pd_count = "";

        try {
            if (cursor1 != null || cursor2 != null) {

                if (cursor1.moveToNext()) {
                    pd_count = cursor1.getString(0);

                }
                cursor1.close();

                if (cursor2.moveToNext()) {
                    cons_count = cursor2.getString(0);

                }
                cursor2.close();


            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor1.close();
            cursor2.close();
            db.close();
        }

        get_masters.add(cons_count);

        get_masters.add(pd_count);


        System.out.println("PROD CNT:: " + pd_count + " CONS COUNT::" + cons_count);


        return get_masters;
    }

    public ArrayList<String> get_master_flag() {

        SQLiteDatabase db = this.getReadableDatabase();
        String cons_flag = null;
        String cons_id = null;


        String pd_flag = null;
        String pd_id = null;

        String countQuery = "SELECT " + KEY_pd_flag + "," + KEY_pd_id + " FROM "
                + DATABASE_PD_TABLE + " LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pd_flag = cursor.getString(0);
                    pd_id = cursor.getString(1);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        SQLiteDatabase db1 = this.getReadableDatabase();

        String countQuery1 = "SELECT " + KEY_cons_flag + " , " + KEY_cons_id + "  FROM "
                + DATABASE_CONS_TABLE + " LIMIT 1";
        Cursor cursor1 = db1.rawQuery(countQuery1, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor1 != null) {

                if (cursor1.moveToNext()) {
                    cons_flag = cursor1.getString(0);
                    cons_id = cursor1.getString(1);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }


        get_flag.add(pd_flag);
        get_flag.add(pd_id);
        get_flag.add(cons_flag);
        get_flag.add(cons_id);

        return get_flag;
        // System.out.println("countQuery:"+product_qnt);
    }

    public void delete_pd_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_PD_TABLE, null, null);
        db.close();
    }

    public void delete_cons_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_CONS_TABLE, null, null);
        db.close();
    }


    public long insert_prod(String prod, String pd_val, String pd_flag, String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (prod != "" && pd_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_product, prod);
            values.put(KEY_pd_val, pd_val);
            values.put(KEY_pd_flag, pd_flag);
            values.put(KEY_pd_id, pd_id);
            long lid = db.insert(DATABASE_PD_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }


    public long insert_cons(String cons, String cons_val, String cons_flag, String cons_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (cons != "" && cons_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_cons, cons);
            values.put(KEY_cons_val, cons_val);
            values.put(KEY_cons_flag, cons_flag);
            values.put(KEY_cons_id, cons_id);
            long lid = db.insert(DATABASE_CONS_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }


    public ArrayList<String> get_masters_pd() {

        SQLiteDatabase db = this.getReadableDatabase();

        String product = null;


        arr_pd.add("Select Product");

        String countQuery = "SELECT " + KEY_product + " FROM "
                + DATABASE_PD_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    product = cursor.getString(0);

                    arr_pd.add(product);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_pd;
        // System.out.println("countQuery:"+product_qnt);
    }


    public ArrayList<String>
    get_masters_pd_val() {

        SQLiteDatabase db = this.getReadableDatabase();
        String pd_val = null;

        arr_pd_val.add("");

        String countQuery = "SELECT " + KEY_pd_val + " FROM "
                + DATABASE_PD_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_val = cursor.getString(0);

                    arr_pd_val.add(pd_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }
        return arr_pd_val;

    }


    public ArrayList<String> get_masters_cons() {

        SQLiteDatabase db = this.getReadableDatabase();

        String cons = null;


        arr_cons.add("Select Constituition");

        String countQuery = "SELECT " + KEY_cons + " FROM "
                + DATABASE_CONS_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    cons = cursor.getString(0);

                    arr_cons.add(cons);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_cons;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_cons_val() {

        SQLiteDatabase db = this.getReadableDatabase();


        String cons_val = null;

        arr_cons_val.add("");

        String countQuery = "SELECT " + KEY_cons_val + " FROM "
                + DATABASE_CONS_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    cons_val = cursor.getString(0);

                    arr_cons_val.add(cons_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_cons_val;

    }

    public long insert_coll_intro(String losid,String product,String cust_name,String person_met,String relation,String owner_name,String coll,String ts,String flag){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        System.out.println("nsbcnksab" + losid + product + cust_name + person_met + relation + owner_name + coll + ts);

        contentValues.put(COLL_LOS, losid);
        contentValues.put(COLL_PROD, product);
        contentValues.put(COLL_CUST, cust_name);
        contentValues.put(COLL_PERSON, person_met);
        contentValues.put(COLL_RELATION, relation);
        contentValues.put(COLL_OWNER, owner_name);
        contentValues.put(COLL_COLLATERAL, coll);

        contentValues.put(COLL_TS, ts);
        contentValues.put(COLL_FLAG, flag);
        return db.insert(TBL_COLL, null, contentValues);

    }

    public void delete_pic() {
        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + IMAGE_TABLE_NAME + " where " + IMAGE_STATUS + "= 0";

        db.execSQL(del_img);

        db.close();


    }


    public void delete_data() {


        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + TBL_CUST + " where " + FLAG  + "= 0";

        db.execSQL(del_img);

        db.close();

    }


    public void delete_data_coll() {

        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + TBL_COLL + " where " + COLL_FLAG  + "= 0";

        db.execSQL(del_img);

        db.close();

    }

    public Cursor getImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + ">? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0"});

        return res;
    }

}