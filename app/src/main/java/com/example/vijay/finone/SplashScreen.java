package com.example.vijay.finone;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.DatabaseHandler;
import com.example.vijay.finone.libraries.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class SplashScreen extends Activity {
    SharedPreferences pref;

    DBHelper eDB, rDB;

    SharedPreferences sharedpreferences;
    UserFunctions userFunction;
    JSONObject json;
    private ConnectionDetector cd;


    String res, resp_success;

    public static String KEY_STATUS = "status";

    public static String KEY_SUCCESS = "success";

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";

    JSONObject jsonobject;
    JSONArray jsonarray, jsonarray1, jsonarray2, jsonarray3;

    ProgressDialog mProgressDialog;

    ArrayList<String> al_cons, al_cons_val, al_cons_flag, al_cons_id, al_product, al_prod_val, al_prod_flag, al_prod_id;

    ArrayList<String> check_master;

    ArrayList<String> master_flag;

    DatabaseHandler db;
    String flag_cons, id_cons, flag_pd, id_pd;


    //String fisrt_run = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_splashscreen);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        eDB = new DBHelper(this);

        rDB = new DBHelper(this);
        userFunction = new UserFunctions();
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        //	rDB=new RequestDB(this);

        eDB.delete_pic();
        eDB.delete_data();
        eDB.delete_data_coll();


        //cons code
        al_cons = new ArrayList<String>();
        al_cons_val = new ArrayList<String>();
        al_cons_flag = new ArrayList<String>();
        al_cons_id = new ArrayList<String>();

        check_master = new ArrayList<String>();
        master_flag = new ArrayList<String>();


        //pay mode


        //product
        al_product = new ArrayList<String>();
        al_prod_val = new ArrayList<String>();
        al_prod_flag = new ArrayList<String>();
        al_prod_id = new ArrayList<String>();


        check_master = rDB.get_masters();

        //checkforLogout();


        if (check_master.get(0).equals("0") || check_master.get(1).equals("0")) {
            //  new Download_Masters().execute();

            System.out.println("MASTERS LOAD");

            new Download_Masters().execute();
        } else {
            master_flag = rDB.get_master_flag();

            System.out.println(" FLAG DC:::" + master_flag.get(0) + " FLAG PM::: " + master_flag.get(2));


            flag_pd = master_flag.get(0);
            id_pd = master_flag.get(1);

            flag_cons = master_flag.get(2);
            id_cons = master_flag.get(3);

            new Verify_Masters().execute();

        }



/*
	    int SPLASH_SCREEN_TIME = 2000;

        new Handler().postDelayed(new Runnable() {
			SharedPreferences settings = getSharedPreferences("prefs", 0);
			boolean firstRun = settings.getBoolean("firstRun", true);

			@Override
			public void run() {
				// This is method will be executed when SPLASH_SCREEN_TIME is
				// over, Now you can call your Home Screen

				if (firstRun) {
					// here run your first-time instructions, for example :
					// startActivityForResult( new Intent(SplashScreen.this,
					// InstructionsActivity.class),INSTRUCTIONS_CODE);
					Intent iHomeScreen = new Intent(SplashScreen.this,
							HdbHome.class);
					startActivity(iHomeScreen);
				} else {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent i = new Intent(SplashScreen.this, HdbHome.class);
					startActivity(i);
				}

				// Finish Current Splash Screen, as it should be visible only
				// once when application start
				finish();
			}
		}, SPLASH_SCREEN_TIME);

        */
    }

    public Void checkforLogout() {
        String str_last_date = pref.getString("current_date", null);

        //	String str_last_date="2016-08-15";

        System.out.println("LAST DATE::::" + str_last_date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = sdf.format(new Date());

        Date date1 = null;
        Date date2 = null;

        try {
            if (current_date != null) {
                date1 = sdf.parse(current_date);
            }
            if (str_last_date != null) {
                date2 = sdf.parse(str_last_date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (current_date != null && str_last_date != null) {
            if (date1.after(date2)) {
                System.out.println("Date1 is after Date2");
                Editor editor = pref.edit();
                editor.putInt("is_login", 0);
                editor.putInt("off_is_login", 0);
                editor.commit();
            }
        }
        return null;
    }


    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
/*
			mProgressDialog = new ProgressDialog(SplashScreen.this);
			mProgressDialog.setMessage("Loading Masters...Plz wait");
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
			*/
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_masters(null);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                            JSONObject jobj1 = jsonobject.getJSONObject("data1");

                            JSONObject jobj2 = jsonobject.getJSONObject("data2");


                            jsonarray1 = jobj1.getJSONArray("cons");

                            jsonarray2 = jobj2.getJSONArray("product");

                            if (jsonarray1 != null && jsonarray2 != null) {


                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_cons.add(jsonobject.getString("cons"));
                                    al_cons_val.add(jsonobject.getString("cons_val"));
                                    al_cons_flag.add(jsonobject.getString("cons_flag"));
                                    al_cons_id.add(jsonobject.getString("cons_id"));

                                }


                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        } else {

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    //	mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        if (jsonarray1 != null && jsonarray2 != null) {


                            rDB.delete_cons_table();
                            rDB.delete_pd_table();


                            for (int i = 0; i < al_product.size(); i++) {

                                System.out.println("AL PD:::" + al_product.get(i));
                                System.out.println("AL PD:::" + al_prod_val.get(i));
                                System.out.println("AL PD:::" + al_prod_flag.get(i));
                                System.out.println("AL PD:::" + al_prod_id.get(i));

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));
                            }

                            for (int i = 0; i < al_cons.size(); i++) {

                                System.out.println("AL PD:::" + al_cons.get(i));
                                System.out.println("AL PD:::" + al_cons_val.get(i));
                                System.out.println("AL PD:::" + al_cons_flag.get(i));
                                System.out.println("AL PD:::" + al_cons_id.get(i));

                                rDB.insert_cons(al_cons.get(i), al_cons_val.get(i), al_cons_flag.get(i), al_cons_id.get(i));
                            }

                            //mProgressDialog.dismiss();

                            Intent messagingActivity = new Intent(SplashScreen.this, HdbHome.class);
                            startActivity(messagingActivity);
                            finish();

                        }

                    } else {
                        //mProgressDialog.dismiss();
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        //loginErrorMsg.setText(jsonobject.getString("message").toString());
                        userFunction.cutomToast(jsonobject.getString("message").toString(), SplashScreen.this);

                        //pd_details_layout.setVisibility(View.GONE);

                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
/*
			mProgressDialog = new ProgressDialog(LoginActivity.this);
			mProgressDialog.setMessage("Loading Masters...Plz wait");
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();

*/
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = null;
                jsonobject = userFunction.check_masters(flag_cons, id_cons, flag_pd, id_pd);

                System.out.println("JSONOBJ::::" + jsonobject);

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_VM) != null) {
                        res = jsonobject.getString(KEY_STATUS_VM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_VM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                            System.out.println("jobj1::jsonobject::::" + jsonobject);


                            JSONObject jobj4 = null, jobj3 = null;
                            if (!jsonobject.isNull("data1")) {
                                jobj3 = jsonobject.getJSONObject("data1");
                                jsonarray1 = jobj3.getJSONArray("arr_cons");
                            }

                            if (!jsonobject.isNull("data2")) {
                                jobj4 = jsonobject.getJSONObject("data2");
                                jsonarray2 = jobj4.getJSONArray("arr_product");
                            }


                            System.out.println("jobj1:::" + jobj3);
                            System.out.println("jobj2:::" + jobj4);

                            if (jsonarray1 != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_cons.add(jsonobject.getString("cons"));
                                    al_cons_val.add(jsonobject.getString("cons_val"));
                                    al_cons_flag.add(jsonobject.getString("cons_flag"));
                                    al_cons_id.add(jsonobject.getString("cons_id"));

                                }
                            }

                            if (jsonarray2 != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id"));

                                }
                            }

                        } else {
                            mProgressDialog.dismiss();
                            Editor edt = pref.edit();
                            edt.putInt("is_login", 0);

                            edt.commit();
                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    //mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray1);


                        if (jsonarray1 != null) {

                            rDB.delete_cons_table();

                            for (int i = 0; i < al_cons.size(); i++) {

                                rDB.insert_cons(al_cons.get(i), al_cons_val.get(i), al_cons_flag.get(i), al_cons_id.get(i));

                            }

                        }

                        if (jsonarray2 != null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }
                        }

                        //	mProgressDialog.dismiss();

                        Intent messagingActivity = new Intent(SplashScreen.this, HdbHome.class);
                        startActivity(messagingActivity);
                        finish();


                    } else {

                        //mProgressDialog.dismiss();
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        userFunction.cutomToast(jsonobject.getString("message").toString(), SplashScreen.this);
                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}