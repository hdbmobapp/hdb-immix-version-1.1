package com.example.vijay.finone;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.Dashboard;


/**
 * Created by Administrator on 20-01-2017.
 */
public class ImagesList extends Activity {
    DBHelper dbHelper;
    Integer total_pic_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_imageslist);

        ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("Pending Images ");

       /* Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });*/

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);
        setlistview();

    }

    public void setlistview() {
        dbHelper = new DBHelper(this);
        TextView txt_upload =(TextView)findViewById(R.id.txt_upload);
        final Cursor cursor = dbHelper.getImages();
        total_pic_count = cursor.getCount();
        if(total_pic_count>0){
            txt_upload.setVisibility(View.GONE);
        }else{
            txt_upload.setVisibility(View.VISIBLE);
        }
        ListView lvItems = (ListView) findViewById(R.id.list);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageAdapter todoAdapter = new ImageAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        todoAdapter.notifyDataSetChanged();
        lvItems.refreshDrawableState();

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(ImagesList.this, Dashboard.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                MainActivity.class);
        startActivity(home_activity);

    }


}
