package com.example.vijay.finone;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.MyAdapter;
import com.example.vijay.finone.libraries.UserFunctions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Vijay on 11/23/2016.
 */
public class Coll_Intro  extends Fragment implements AdapterView.OnItemSelectedListener{

    Button btn_next;
    Spinner spn_relation,spn_product,spn_coll;

    UserFunctions userFunction;
    RelativeLayout relprod;

    DBHelper dbHelp;
    SharedPreferences pData;

    RelativeLayout coll_layout;

    String[] relation={"Select Relationship","Daughter-in law","Self","Spouse","Parent","Brother","Sister-in-Law","Grandparent"};

    String[] collateral={"Select Collateral","Property","Auto"};

    ArrayList al_product,al_prod_val;

    ArrayAdapter adp_relation,adp_prod,adp_coll;

    EditText edt_los,edt_cust_name,edt_person_met,edt_owner;

    String str_los,str_prod,str_cust_name,str_person_met,str_relation,str_owner,str_coll;

    String str_ts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pd_len_coll_intro, container, false);

        dbHelp=new DBHelper(getContext());

        userFunction=new UserFunctions();

        pData = getActivity().getSharedPreferences("pData", 0);


        al_product=new ArrayList<>();

        al_prod_val=new ArrayList<>();

        al_product=dbHelp.get_masters_pd();

        btn_next=(Button)view.findViewById(R.id.btn_getotp);

        relprod=(RelativeLayout)view.findViewById(R.id.tableRow11);



        spn_relation=(Spinner)view.findViewById(R.id.spn_relation);

        spn_product=(Spinner)view.findViewById(R.id.spn_product);

        spn_coll =(Spinner)view.findViewById(R.id.spn_coll);

        edt_los =(EditText)view.findViewById(R.id.edt_losid);

        edt_cust_name =(EditText)view.findViewById(R.id.edt_cust_name);
        edt_person_met =(EditText)view.findViewById(R.id.edt_person_met);
        edt_owner =(EditText)view.findViewById(R.id.edt_owner);
        edt_los =(EditText)view.findViewById(R.id.edt_losid);


        adp_relation = new MyAdapter(getActivity(),R.layout.sp_display_layout,
                relation);
        adp_relation
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_relation.setAdapter(adp_relation);
        spn_relation.setOnItemSelectedListener(this);
        adp_relation.notifyDataSetChanged();

        adp_prod = new MyAdapter(getActivity(), R.layout.sp_display_layout,R.id.txt_visit,
                al_product);
        adp_prod
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_product.setAdapter(adp_prod);
        spn_product.setOnItemSelectedListener(this);
        adp_prod.notifyDataSetChanged();

        adp_coll = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                collateral);
        adp_coll
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_coll.setAdapter(adp_coll);
        spn_coll.setOnItemSelectedListener(this);
        adp_coll.notifyDataSetChanged();

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Boolean ins=true;

                getdata();

                try {

                    if(str_los.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter LOSID",getActivity());
                        ins=false;

                    }else {
                        if(str_los.charAt(0)=='0'&& ins==true){
                            userFunction.cutomToast("LOSID should not start with zero",getActivity());
                            ins=false;
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_prod=="" || str_prod==null)&& ins==true){
                        userFunction.cutomToast("Please select product",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_cust_name.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Customer Name",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_person_met.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Name of Person met",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_relation=="" || str_relation==null)&& ins==true){
                        userFunction.cutomToast("Please select Relationship",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_owner.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Owner Name",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_coll=="" || str_coll==null)&& ins==true){
                        userFunction.cutomToast("Please Select Collateral",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                if(ins==true) {
                    str_ts = getDateTime();
                    SharedPreferences.Editor pEdit = pData.edit();

                    pEdit.putString("str_los", str_los);
                    pEdit.putString("str_ts", str_ts);

                    pEdit.commit();

                    System.out.println("TSSS::::" + str_ts);

                    long update=0;

                     update = dbHelp.insert_coll_intro(str_los, str_prod, str_cust_name, str_person_met, str_relation, str_owner, str_coll, str_ts,"0");

                    System.out.println("UPDATE VAL::::" + update);


                    if (update > 0) {
                        if (str_coll.equals("Property")) {

                            SharedPreferences.Editor pEdit1 = pData.edit();

                            pEdit1.putString("collateral", str_coll);


                            pEdit1.commit();

                            Fragment prop = new Coll_Property();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, prop);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                        } else {

                            SharedPreferences.Editor pEdit2 = pData.edit();

                            pEdit2.putString("collateral", str_coll);

                            pEdit2.commit();

                            Fragment vehicle = new Coll_Vehicle();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, vehicle);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                        }
                    }

                }
            }
        });

                return view;

    }


    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

           case R.id.spn_product:

               if(position!=0){

                   str_prod=al_product.get(position).toString();

               }else {
                   str_prod="";
               }
               break;

            case R.id.spn_relation:

                if(position!=0){
                    str_relation=relation[position];
                }else{
                    str_relation="";
                }
                break;


            case R.id.spn_coll:
                if(position!=0){
                    str_coll=collateral[position];
                }else {
                    str_coll="";
                }
                break;



        }
    }

    void getdata(){
        str_los=edt_los.getText().toString();
        str_cust_name=edt_cust_name.getText().toString();
        str_person_met=edt_person_met.getText().toString();
        str_owner=edt_owner.getText().toString();
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }



}
