package com.example.vijay.finone;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.Home_PD_COLLX;
import com.example.hdb.R;
import com.example.vijay.finone.MyUploads.MyuploadsFragmentMain;
import com.example.vijay.finone.libraries.Dashboard;
import com.example.vijay.finone.libraries.UserFunctions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DatabaseHandler;

import com.example.vijay.finone.libraries.UserFunctions;


public class HdbHome extends Activity {

    JSONObject jsonobject = null;
    UserFunctions userFunction;
    public static final String KEY_STATUS = "status";
    String str_user_id, str_user_name, str_branch;
    public static String msg_count = "0";
    TextView txt_user_id, txt_user_name, txt_branch;
    SharedPreferences pref;
    ListView list;
    String[] itemname = {"Visit Details", "Upload In Progress", "My Uploads", "Message Box", "Contact Us"};
    Integer[] imgid = {R.drawable.pd_image, R.drawable.upload, R.drawable.efficiancy, R.drawable.mail_box, R.drawable.contact_ud};

    String[] itemname_co = {"Visit Details", "Upload In Progress", "My Uploads", "Message Box", "Contact Us"};
    Integer[] imgid_co = {R.drawable.pd_image, R.drawable.upload, R.drawable.efficiancy, R.drawable.mail_box, R.drawable.contact_ud};

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;


    ProgressDialog mProgressDialog;
    Typeface font;
    ConnectionDetector cd;
    HdbHomeListAdapter adapter;
    String str_designation;
    Integer is_login, off_is_login;
    String str_designation_val;
    public static String str_total;

    int total_int = 0;

    // DatabaseHandler db;
    info.hdb.libraries.DatabaseHandler db;
    HashMap<String, String> user = new HashMap<String, String>();
    private String android_id;

    Integer off_rowcount;

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("ON START HOME::::::::");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_hdb_home);

        cd = new ConnectionDetector(HdbHome.this);


        userFunction = new UserFunctions();

        ActivityCompat.requestPermissions(HdbHome.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);


        db = new info.hdb.libraries.DatabaseHandler(this);

//        font = Typeface.createFromAsset(this.getAssets(),
        //              "fonts/MyriadPro-Regular.otf");
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        str_branch = pref.getString("user_brnch", null);
        str_designation = pref.getString("user_designation", "NO");
        str_designation_val = pref.getString("user_designation_value", "0");

        //  userFunction.checkforLogout(pref, HdbHome.this);

        is_login = pref.getInt("is_login", 0);

        off_is_login = pref.getInt("off_is_login", 0);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        System.out.println("ON CREaTE HOME::::::::");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.commit();


        System.out.println("off Login:::" + off_is_login);
        this.SetLayout();

          /*
        if (userFunction.isUserLoggedIn(HdbHome.this) && is_login > 0  ) {
            System.out.println("isUserLoggedIn:::"+off_is_login);

            this.SetLayout();
        } else {

            user=   db.off_getUserDetails();

            System.out.println("HASHMAP::::" + user);

            String val=(String)user.get("uid");
            String dev_val=(String)user.get("dev_id");


            off_rowcount=db.off_getRowCount();
            if(off_rowcount>0) {
                if (!dev_val.equals("")) {
                    if (userFunction.off_isUserLoggedIn(HdbHome.this) && off_is_login > 0 && dev_val.equals(android_id))

                    {
                        System.out.println("off_isUserLoggedIn:::" + off_is_login);
                        this.SetLayout();

                    } else {
                        System.out.println("redirect login:::" + off_is_login);
                        Intent messagingActivity = new Intent(HdbHome.this,
                                LoginActivity.class);
                        messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(messagingActivity);
                        finish();
                        // this.SetLayout();
                    }
                }else{
                    if (userFunction.off_isUserLoggedIn(HdbHome.this) && off_is_login > 0) {

                        System.out.println("off_isUserLoggedIn:::" + off_is_login);
                        this.SetLayout();
                    }else {
                        System.out.println("redirect login vcvbv:::" + off_is_login);
                        Intent messagingActivity = new Intent(HdbHome.this,
                                LoginActivity.class);
                        messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(messagingActivity);
                        finish();
                    }
                }

            }else{
                System.out.println("redirect login:::" + off_is_login);
                Intent messagingActivity = new Intent(HdbHome.this,
                        LoginActivity.class);
                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(messagingActivity);
                finish();
            }


        }

 */
        // System.out.println("HEHHUHU");

        //   RequestDB rdb = new RequestDB(HdbHome.this);

        int int_new_caset_pending_count = 0;
        int Pending_Photo_Count = 0;

/*
        int_new_caset_pending_count = Integer.parseInt(rdb.getPendingCount());
        Pending_Photo_Count = Integer.parseInt(eDB.getPendingPhotoCount());

        total_int = int_new_caset_pending_count  + Pending_Photo_Count;
        str_total = String.valueOf(total_int);


        if (cd.isConnectingToInternet()) {
            new GetDashboardInfo().execute();
        }
*/

        if (str_designation.equals("0")) {
            adapter = new HdbHomeListAdapter(this, itemname_co, imgid_co);
        } else {
            adapter = new HdbHomeListAdapter(this, itemname, imgid);
        }

        System.out.println("off Login HEHE:::" + off_is_login);

        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (is_login > 0 || off_is_login > 0) {
                    System.out.println("IF::::");

                    GoTo(position);
                } else {
                    System.out.println("ELSE::::");
                    Intent messagingActivity = new Intent(HdbHome.this, LoginActivity.class);
                    messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(messagingActivity);
                    finish();

                }
            }
        });
    }

    protected void SetLayout() {
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);
        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);

        // txt_user_id.setTypeface(font);
        // txt_user_n)ame.setTypeface(font);
        // txt_branch.setTypeface(font);

    }

    public void GoTo(Integer i) {
        if (total_int > 0) {
            userFunction.cutomToast("Please upload data in Upload in progress tab ....Pending Count=" + str_total, HdbHome.this);
        }
        switch (i) {

            case 0:

                System.out.println("CASE 1 ::::");
                Intent messagingActivity = new Intent(HdbHome.this, FIrstScreen.class);
                startActivity(messagingActivity);

                break;

            case 1:
                Intent effe = new Intent(HdbHome.this, Dashboard.class);
                startActivity(effe);
                //userfunction.cutomToast("To be updated..", HdbHome.this);
                break;

            case 2:
                Intent cases = new Intent(HdbHome.this, MyuploadsFragmentMain.class);
                startActivity(cases);
                //userfunction.cutomToast("To be updated..", HdbHome.this);
                break;

            case 3:
                Intent message_box = new Intent(HdbHome.this, MessageBox.class);
                startActivity(message_box);
                break;

            case 4:
                Intent Contact_us = new Intent(HdbHome.this, Contact_us.class);
                startActivity(Contact_us);
                break;

            default:

                break;

        }

    }

    public void logout(View v) {
        // do stuff
        Intent hme = new Intent(HdbHome.this, Home_PD_COLLX.class);
        startActivity(hme);
        //  alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                HdbHome.this.finish();
                @SuppressWarnings("unused") boolean islogoutBoolean = true;

                SharedPreferences.Editor editor = pref.edit();

                editor.putInt("off_is_login", 0);
                editor.commit();

                islogoutBoolean = userFunction.logoutUser(getApplicationContext());

                               /* off_islogoutBoolean = userfunction
                                        .off_logoutUserclear(getApplicationContext());*/

                if (islogoutBoolean == true) {
                    Intent i = new Intent(HdbHome.this, com.example.hdb.LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

}