package com.example.vijay.finone;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.example.vijay.finone.libraries.DBHelper;


/**
 * Created by Administrator on 23-02-2016.
 */

public class ImageAdapter extends CursorAdapter {
    ImageLoader imageLoader;
    public DisplayImageOptions img_options;
    String priority;

    String img_path;
    SharedPreferences pref;
    DBHelper edb;
    ProgressDialog mProgressDialog;
    Context cnt;
    long length;

    public ImageAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        img_options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.slide_2)
                .showImageForEmptyUri(R.drawable.slide_2)
                .showImageOnFail(R.drawable.slide_2).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        edb = new DBHelper(context);
        cnt = context;
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.image_listview_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        String str_losid = null;
       // String str_form_no = null;
        // Find fields to populate in inflated template
        TextView tvlosid = (TextView) view.findViewById(R.id.tvlosid);
        TextView tvform_no = (TextView) view.findViewById(R.id.tv_app_form);
        TextView tvlabel = (TextView) view.findViewById(R.id.tv_label);
        TextView tv_size = (TextView) view.findViewById(R.id.tv_images_size);

        ImageView img_image = (ImageView) view.findViewById(R.id.flag);
        Button btn_upload = (Button) view.findViewById(R.id.btn_upload);
        Button btn_resize = (Button) view.findViewById(R.id.btn_resize);
        // Extract properties from cursor
        String body = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
        //  int priority = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        priority = cursor.getString(6);
        str_losid = cursor.getString(1);
       // str_form_no = cursor.getString(2);
        //  String priority=pref.getString("img_name",null);
        img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
         length = 0;
        try {
            File file = new File(img_path);
            length = file.length();
            length = length / 1024;
        } catch (Exception e) {

        }

        if (length == 0) {
            //  view.setVisibility(View.GONE);
            System.out.println("rrrrrrrrrrrrrrrrrrr" + length);
            //  Toast.makeText(cnt, "Image not found in your internal storage ", Toast.LENGTH_LONG).show();
            edb.deletepic(img_path);
            Intent i = new Intent(cnt, ImagesList.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            cnt.startActivity(i);

        }


        String str_long = Long.toString(length);
        // Populate fields with extracted properties
        tvlosid.setText(str_losid);
       // tvform_no.setText(str_form_no);
        tvlabel.setText(priority);
        tv_size.setText(str_long + " kb");

        if(length>200){
            btn_resize.setVisibility(View.VISIBLE);
        }else
        {
            btn_resize.setVisibility(View.GONE);

        }
        //  imageLoader.displayImage(imageUri, img_image);
        String imageUri_ll = "file:///" + img_path; // from SD card

        imageLoader.displayImage(imageUri_ll, img_image);

//        imageLoader.displayImage(img_path,
//                img_image, img_options);
        btn_upload.setTag(cursor.getString(5));
        btn_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {



                    new UploadPhoto().execute(arg0.getTag().toString());




            }
        });

        btn_resize.setTag(cursor.getString(5));
        btn_resize.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                new ResizePhoto().execute(arg0.getTag().toString());
            }
        });

    }


    private class UploadPhoto extends AsyncTask<String, String, String> {
        private String str_path;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(cnt);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                str_path = params[0];
                edb.upload_image(str_path);

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {
            Intent i = new Intent(cnt,
                    ImagesList.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            cnt.startActivity(i);
            mProgressDialog.dismiss();

        }

    }

    private class ResizePhoto extends AsyncTask<String, String, String> {
        private String str_path;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(cnt);
            mProgressDialog.setMessage("Compressing ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                str_path = params[0];
                // edb.upload_image(str_path);
                FileOutputStream out = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeFile(str_path, options);
                try {

                    out = new FileOutputStream(str_path);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 70, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {
            Intent i = new Intent(cnt,
                    ImagesList.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            cnt.startActivity(i);
            mProgressDialog.dismiss();

        }

    }
}