package com.example.vijay.finone;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.MyAdapter;
import com.example.vijay.finone.libraries.UserFunctions;


public class FragIntro extends Fragment implements AdapterView.OnItemSelectedListener{

    EditText edt_prod,edt_const,edt_branch,edt_cust_name,edt_score,edt_losid,edt_person_met,edt_emp_name;

    ConnectionDetector cd;
    SharedPreferences pref;

    private Handler mHandler = new Handler();

    DBHelper dbhelp;

    LinearLayout main_lay,otp_lay;

    String[] pd_place = {"PD Place", "Office", "Residence"};

    String[] exist_cust = {"Existing Customer", "Yes","No"};

    Spinner spn_pd_place,spn_exist_cust;

    ArrayAdapter adp_pd_place,adp_exist;

    ProgressDialog mProgressDialog,mProgressDialog1;
    JSONObject json;
    UserFunctions userFunction;

     String random_no,str_losid,str_userid,str_ts,str_prod,str_const,str_branch,str_cust_name,str_cibil;

    Spinner spn_prod,spn_const;
    ArrayAdapter adp_prod,adp_const;

    String str_pd_place,person_met,str_emp_name;
     String str_constid;
    Button btn_search,btn_next;
    JSONObject jsonobject;
    JSONArray json_arr;

    ArrayList product,prod_id,cons, cons_id;;




    SharedPreferences pData;

    int j=0;
    ScrollableTabsActivity main_act;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");

    private static final Pattern MOBILE_PATTERN = Pattern
            .compile("[0-9]{10}");

   public static int MY_PERMISSIONS_REQUEST_READ_CONTACTS=0;

    public  static Boolean is_self_emp;

    public FragIntro() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.pd_len_intro, container, false);


        userFunction=new UserFunctions();
        cd=new ConnectionDetector(getContext());
        pref = getActivity().getSharedPreferences("MyPref", 0);
        dbhelp =new DBHelper(getContext());

        pData = getActivity().getSharedPreferences("pData", 0);
        // Inflate the layout for this fragment


        product = dbhelp.get_masters_pd();

        prod_id = dbhelp.get_masters_pd_val();

        cons = dbhelp.get_masters_cons();

        cons_id = dbhelp.get_masters_cons_val();
        //System.out.println("PRODUCT:::" + str_prod);



        edt_losid = (EditText) view.findViewById(R.id.edt_losid);
         btn_next = (Button) view.findViewById(R.id.btn_getotp);
        edt_prod = (EditText) view.findViewById(R.id.edt_prod);

        edt_branch = (EditText) view.findViewById(R.id.edt_branch);
            edt_cust_name = (EditText) view.findViewById(R.id.edt_cust_name);


        edt_person_met = (EditText) view.findViewById(R.id.edt_person_met);

        edt_emp_name = (EditText) view.findViewById(R.id.edt_emp);

        spn_pd_place = (Spinner) view.findViewById(R.id.spn_pd_place);

        spn_prod = (Spinner) view.findViewById(R.id.spn_prod);
        spn_const = (Spinner) view.findViewById(R.id.spn_const);



        view.setBackgroundColor(Color.WHITE);


        adp_pd_place = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                pd_place);
        adp_pd_place
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_pd_place.setAdapter(adp_pd_place);
        spn_pd_place.setOnItemSelectedListener(this);
        adp_pd_place.notifyDataSetChanged();

        adp_prod = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                product);
        adp_prod
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_prod.setAdapter(adp_prod);
        spn_prod.setOnItemSelectedListener(this);
        adp_prod.notifyDataSetChanged();

        adp_const = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                cons);
        adp_const
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_const.setAdapter(adp_const);
        spn_const.setOnItemSelectedListener(this);
        adp_const.notifyDataSetChanged();




/*spn_prod.setSelection(0);
        str_pro*/

       /* btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                str_losid = edt_losid.getText().toString();
                str_ts = getDateTime();
                str_userid = "HDB36801";
                str_prod = null;

                if (cd.isConnectingToInternet() == true) {

                    new Insert_Loan().execute();
                } else {

                    userFunction.cutomToast("Check your internet connection !", getActivity());
                }

            }
        });
*/
        btn_next.setEnabled(true);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getdata();
                boolean ins=true;

                try {

                    if(str_losid.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter LOSID",getActivity());
                        ins=false;

                    }else {
                        if(str_losid.charAt(0)=='0'&& ins==true){
                            userFunction.cutomToast("LOSID should not start with zero",getActivity());
                            ins=false;
                        }

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_prod=="" || str_prod==null)&& ins==true){
                        userFunction.cutomToast("Please select product",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_const.equals("") || str_const==null  ) && ins==true){
                        userFunction.cutomToast("Please select Constitution",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_cust_name.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Customer Name",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_pd_place.equals("") || str_pd_place==null) && ins==true){
                        userFunction.cutomToast("Please select PD Place",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(person_met.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Person Met",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_emp_name.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Employer Name",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                    System.out.println("TS INSERT:::" + str_ts);

                if(ins==true) {

                    str_ts = getDateTime();
                    SharedPreferences.Editor pEdit = pData.edit();

                    pEdit.putString("str_los", str_losid);
                    pEdit.putString("str_ts", str_ts);

                    pEdit.putString("str_prod", str_prod);
                    pEdit.commit();


                    long rowid = 0;


                    rowid = dbhelp.insert_cust_pd(str_losid, str_prod, str_const, str_branch, str_cust_name, str_cibil, str_pd_place, person_met, str_emp_name, "0", str_ts);

                    System.out.println("ROWID:::" + rowid);

                    if (Integer.valueOf(str_constid) == 16 || Integer.valueOf(str_constid) == 25) {

                        System.out.println("intro ts::::" + pData.getString("str_ts", null));

                        Fragment twofrag = new CustOffDetails();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, twofrag);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    } else {

                        System.out.println("inside else");
                        Fragment three = new CustOffDetailsSE();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, three);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                    }
                }

            }
        });

        return view;

    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    private boolean Check_pattern(String string,int i) {

        Boolean status=false;
        switch (i) {
            case 1:
               status=  MOBILE_PATTERN.matcher(string).matches();
           break;
            case 2:
                status=   PAN_PATTERN.matcher(string).matches();
                break;

        }

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_pd_place :
                if(position==0){
                    str_pd_place="";
                }else {
                    str_pd_place = pd_place[position];
                }
                break;

            case R.id.spn_prod :

                if(position==0){
                    str_prod="";

                }else {
                    str_prod = prod_id.get(position).toString();
                }
                break;

            case R.id.spn_const :

                if(position==0){
                    str_const="";
                }else {
                    str_const = cons.get(position).toString();
                    str_constid = cons_id.get(position).toString();
                }
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    public class Insert_Loan extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
           mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
             mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {



            jsonobject = userFunction.insert_Loan(str_losid, str_userid, str_ts);
            //  jsonobject = upload_data();

            if (jsonobject != null) {
                // Create an array
                // Set the JSON Objects into the array
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getActivity());
                mProgressDialog.dismiss();

            } else {
                try {
                    String KEY_STATUS = "status";
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                       String resp_success= jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            mHandler.postDelayed(mRunnable, 1000);
                            /*
                            if(str_has_score.equals("null")) {
                                txt_agree_score.setText(str_has_score);
                            }*/
                            // onStop();
                        } else {
//                            userFunction.cutomToast(
//                                    getResources().getString(R.string.error_message),
//                                    getApplicationContext());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

          //  mProgressDialog.dismiss();
        }
    }

    public class DownloadCiblireport extends AsyncTask<String, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
/*
 mProgressDialog1 = new ProgressDialog(getActivity());
            mProgressDialog1.setMessage("Please wait  1234");
            mProgressDialog1.setCancelable(false);
            mProgressDialog1.setIndeterminate(true);
            mProgressDialog1.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog1.show();

*/
        }

        @Override
        protected Void doInBackground(String... params) {


            jsonobject = userFunction.get_Loan(str_losid, str_ts);
            //  jsonobject = upload_data();

            if (jsonobject != null) {
                // Create an array

                // Set the JSON Objects into the array
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
            /*   userFunction.cutomToast(
                      getResources().getString(R.string.error_message),
                     getActivity());
                mProgressDialog.dismiss();*/

            } else {
                try {
                    String KEY_STATUS = "status";
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        int i;


                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            str_prod = jsonobject.getString("product");
                            str_const = jsonobject.getString("const");
                            str_branch = jsonobject.getString("branch");
                            str_cust_name = jsonobject.getString("cust_name");
                            str_cibil = jsonobject.getString("cib_score");
                            str_constid = jsonobject.getString("const_id");

                            edt_prod.setText(str_prod);
                            edt_const.setText(str_const);
                            edt_branch.setText(str_branch);
                            edt_cust_name.setText(str_cust_name);
                            edt_score.setText(str_cibil);


                            if (str_constid == null) {

                            } else {
                                if (str_constid.equals("null")) {

                                } else {
                                    if (str_constid.equals("")) {


                                    } else {
                                        btn_next.setEnabled(true);
                                    }
                                }
                            }

                            /*
                            if(str_has_score.equals("null")) {
                                txt_agree_score.setText(str_has_score);
                            }*/
                            // onStop();
                        } else {
//                            userFunction.cutomToast(
//                                    getResources().getString(R.string.error_message),
//                                    getApplicationContext());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            System.out.println("PROD::::" + str_prod);

            if (str_prod != null) {
                if (!str_prod.equals("")) {
                    if (!str_prod.equals("null")) {
                        if (mProgressDialog.isShowing()) {
                            mProgressDialog.dismiss();
                        }
                    }

                }else{

                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                userFunction.cutomToast("Please Enter valid losid !",getActivity());
            }

            }

        }
    }


    private final Runnable mRunnable = new Runnable() {
        public void run() {

            if(str_prod==null || str_prod=="" || str_prod.equals("null")
                    ) {

                new DownloadCiblireport().execute();
            }
            mHandler.postDelayed(mRunnable, 1000);
        }
    };

   void getdata(){

       str_losid=edt_losid.getText().toString();

       str_branch=edt_branch.getText().toString();
       str_cust_name=edt_cust_name.getText().toString();

       person_met=edt_person_met.getText().toString();
       str_emp_name=edt_emp_name.getText().toString();

    }

}