package com.example.vijay.finone;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.UserFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class MyCasesListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;
	UserFunctions user_function;


	public MyCasesListAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		ViewHolder holder = null;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = null;
		// Get the position
		resultp = data.get(position);

		if (itemView == null) {
			// The view is not a recycled one: we have to inflate
			itemView = inflater.inflate(R.layout.pd_len_my_cases_list_item, parent,
					false);
			
			holder = new ViewHolder();


			holder.tv_losid = (TextView) itemView.findViewById(R.id.txt_losid);

			holder.tv_coll_verified = (TextView) itemView
					.findViewById(R.id.txt_contact_person);
			holder.tv_contact_person_name = (TextView) itemView
					.findViewById(R.id.txt_contact_person_name);
			holder.tv_contact_person = (TextView) itemView
					.findViewById(R.id.txt_contact_person);




            holder.tv_coll_pics = (TextView) itemView
                    .findViewById(R.id.txt_add_coll);

            holder.tv_form_pics = (TextView) itemView
                    .findViewById(R.id.txt_add_forms);




			holder.tv_pd_customer_name = (TextView) itemView
					.findViewById(R.id.txt_pd_customer_name);
			holder.tv_co_customer_name = (TextView) itemView
					.findViewById(R.id.txt_co_customer_name);




            holder.tv_add_pd = (TextView) itemView
                    .findViewById(R.id.txt_add_pd);



			itemView.setTag(holder);
		} else {
			// View recycled !
			// no need to inflate
			// no need to findViews by id
			holder = (ViewHolder) itemView.getTag();
		}

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }



		holder.tv_losid.setText(resultp.get(MyCases.LOSID));
		holder.tv_contact_person_name.setText(resultp.get(MyCases.CO_PIC_COUNT));
		holder.tv_contact_person.setText(resultp.get(MyCases.CU_PIC_COUNT));

		holder.tv_pd_customer_name.setText(resultp.get(MyCases.PD_Customer_name));
		holder.tv_co_customer_name.setText(resultp.get(MyCases.CO_Customer_name));

/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}
*/

		return itemView;
	}

	private static class ViewHolder {
		public TextView tv_pd_status;
		public TextView tv_losid;
        public TextView tv_app_form_no;
        public TextView tv_add_pd;

        public TextView tv_visit_mode;
		public TextView tv_coll_verified;
		public TextView tv_contact_person_name;
		public TextView tv_contact_person;
        public TextView tv_upload_date;

        public TextView tv_coll_pics;
        public TextView tv_form_pics;
        public TextView tv_new_pics;
		public TextView tv_pd_customer_name;
		public TextView tv_co_customer_name;



    }

}
