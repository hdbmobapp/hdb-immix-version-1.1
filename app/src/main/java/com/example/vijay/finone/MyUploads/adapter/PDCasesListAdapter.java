package com.example.vijay.finone.MyUploads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.vijay.finone.MyUploads.Customer_uploadsFragment;
import com.example.vijay.finone.MyUploads.MyuploadsFragmentMain;

import com.example.vijay.finone.MyUploads.finLos;
import com.example.hdb.R;;
import com.example.vijay.finone.libraries.UserFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class PDCasesListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;
	UserFunctions user_function;
Integer int_pg_from;


	public PDCasesListAdapter(Context context,
							  ArrayList<HashMap<String, String>> arraylist,Integer pg_from) {
		this.context = context;
		data = arraylist;
		int_pg_from=pg_from;

		TableRow row_remraks;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		ViewHolder holder = null;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = null;
		// Get the position
		resultp = data.get(position);



		if (itemView == null) {
			// The view is not a recycled one: we have to inflate
			itemView = inflater.inflate(R.layout.pd_len_cases_list_item, parent,
					false);



			holder = new ViewHolder();
			holder.tv_losid = (TextView) itemView.findViewById(R.id.txt_losid);

			holder.tr_remarks = (TableRow) itemView.findViewById(R.id.row_remarks);

			holder.tv_pd_customer_name = (TextView) itemView
					.findViewById(R.id.txt_pd_customer_name);

			holder.tv_coll_pics = (TextView) itemView
					.findViewById(R.id.txt_pic_count);

			holder.tv_upload_by = (TextView) itemView
					.findViewById(R.id.txt_upload_by);

			itemView.setTag(holder);

			if(int_pg_from==2){
				holder.tr_remarks.setVisibility(View.GONE);

			}
		} else {
			// View recycled !
			// no need to inflate
			// no need to findViews by id
			holder = (ViewHolder) itemView.getTag();
		}

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


		holder.tv_losid.setText(resultp.get(Customer_uploadsFragment.LOSID));


		holder.tv_losid.setText(resultp.get(Customer_uploadsFragment.LOSID));
        holder.tv_upload_by.setText(resultp.get(Customer_uploadsFragment.CU_REMARKS));
		holder.tv_pd_customer_name.setText(resultp.get(Customer_uploadsFragment.PD_Customer_name));
		holder.tv_coll_pics.setText(resultp.get(Customer_uploadsFragment.PIC_COUNT));



		itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resultp = data.get(position);

					// new InsertData().execute();
					Intent pd_form = new Intent(context, finLos.class);
					pd_form.putExtra("pd_losid", resultp.get(Customer_uploadsFragment.LOSID));

					pd_form.putExtra("pd_remarks", resultp.get(Customer_uploadsFragment.CU_REMARKS));
					pd_form.putExtra("pd_cust_name", resultp.get(Customer_uploadsFragment.PD_Customer_name));
					pd_form.putExtra("pd_pic_count", resultp.get(Customer_uploadsFragment.PIC_COUNT));



				pd_form.putExtra("pd_upload_by", resultp.get(Customer_uploadsFragment.UPLOAD_BY));
				pd_form.putExtra("pg_from", int_pg_from);
					pd_form.putExtra("sr_no", resultp.get(Customer_uploadsFragment.SRNO_cust));

					pd_form.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

				System.out.println("SR NO111 ::" + resultp.get(Customer_uploadsFragment.SRNO_cust));

				context.startActivity(pd_form);

			}
		});


/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}



        holder.tv_form_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, PDForm.class);
                pd_form.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));
                context.startActivity(pd_form);
            }

        });

        holder.tv_add_pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, ExistingPD.class);
                pd_form.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));

                pd_form.putExtra("product_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                pd_form.putExtra("asset_type",resultp.get(Customer_uploadsFragment.FORM_NO));

                context.startActivity(pd_form);
            }

        });

        holder.tv_coll_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                Intent pd_coll = new Intent(context, AssetVerified.class);
               // pd_coll.putExtra("pd_upload_by",resultp.get(MyCases.PD_UPLOAD_BY));
                pd_coll.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_coll.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));

                pd_coll.putExtra("product_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                pd_coll.putExtra("asset_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                context.startActivity(pd_coll);
            }

        });
*/
		return itemView;
	}

	private static class ViewHolder {
		public TextView tv_losid;
        public TextView tv_app_form_no;
        public TextView tv_upload_by;
		public TextView tv_pd_customer_name;
		public TextView tv_coll_pics;
		public TextView tv_upload_date;
		public TextView tv_upload_by_new;
		public TableRow tr_remarks;




    }

}
