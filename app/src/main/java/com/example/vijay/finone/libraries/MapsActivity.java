package com.example.vijay.finone.libraries;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hdb.R;;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MapsActivity extends FragmentActivity implements GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener {
    double latitude;
    double longitude;
    GoogleMap googleMap;
    Button btn_refresh, btn_setlocation;
    SharedPreferences pref;
    String str_latitude;
    String str_longitude;
    String str_losid;
    String str_app_form_no;
    String str_ts;
    String str_user_id,pd_label;

    public  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=0;

    String str_from_act;
    Location location;
    UserFunctions userfunction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        //rdb = new RequestDB(MapsActivity.this);
        userfunction=new UserFunctions();

        Intent asset = getIntent();
        str_losid = asset.getStringExtra("str_losid");
      //  str_app_form_no = asset.getStringExtra("str_app_form_no_map");
      //  str_from_act = asset.getStringExtra("str_from_act");
        str_ts = asset.getStringExtra("str_ts");
        pd_label = asset.getStringExtra("pd_label");


        str_user_id = pref.getString("user_id", null);

        /*
        if (str_losid.trim().length() == 0) {
            str_losid = null;
        }
        if (str_app_form_no.trim().length() == 0) {
            str_app_form_no = null;
        }*/

        System.out.println("str_losid_no:::" + str_losid);
        //System.out.println("str_losid_no::sr_app_form_no:::" + str_app_form_no);

        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove("map_cont");
        peditor.commit();

        setlayout();
        btn_refresh = (Button) findViewById(R.id.btn_refresh);
        // mDrawerList.setAdapter(adapter);
        btn_setlocation = (Button) findViewById(R.id.btn_setlocation);

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent img_pinch = new Intent(
                        MapsActivity.this, MapsActivity.class);
                startActivity(img_pinch);
                finish();*/

                setlayout();
            }

        });

        btn_setlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_latitude = String.valueOf(latitude);
                str_longitude = String.valueOf(longitude);

        if(location!=null) {
            CaptureMapScreen();


        }else{

            System.out.println("loc not active");

            SharedPreferences.Editor peditor = pref.edit();
            peditor.putString("is_locate_clicked", "yes");
            peditor.putString("map_img_name", null);
            peditor.commit();
            userfunction.cutomToast("Location service is not active...Map not captured",MapsActivity.this);
        }
              finish();
            }

        });
    }

    public void setlayout() {


        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);

            googleMap.setOnMarkerDragListener(this);
            googleMap.setOnMapLongClickListener(MapsActivity.this);
            googleMap.setOnMapClickListener(MapsActivity.this);

            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);


            // Getting Current Location
             location = locationManager.getLastKnownLocation(provider);

//            Location loc=googleMap.getMyLocation();
//            System.out.println("location::"+loc+":::googleMap.getLatitude()::");

//            if(loc!=null){
//                latitude = loc.getLatitude();
//                longitude = loc.getLongitude();
//            }

            if (location != null) {

                onLocationChanged(location);
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                AddmarkerforStore();

            }

            // locationManager.requestLocationUpdates(provider, 20000, 0, this);
/*
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(17).bearing(0)
                    .tilt(60).build();

            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
*/
        }
    }

    public void onLocationChanged(Location location) {

        TextView tvLocation = (TextView) findViewById(R.id.tv_location);

        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);

        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


        AddmarkerforStore();

        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" + latitude + ", Longitude:" + longitude);

    }

    public void AddmarkerforStore() {

        // adding marker

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("my location")
                .snippet("")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED)));


    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onMarkerDrag(Marker arg0) {

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {
        LatLng dragPosition = arg0.getPosition();
        final double dragLat = dragPosition.latitude;
        final double dragLong = dragPosition.longitude;


    }

    @Override
    public void onMarkerDragStart(Marker arg0) {

    }

    @Override
    public void onMapClick(LatLng arg0) {
        //  map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
    }


    @Override
    public void onMapLongClick(LatLng arg0) {

        //create new marker when user long clicks
//        map.addMarker(new MarkerOptions()
//        .position(arg0)
//        .draggable(true));
    }

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }


    public void CaptureMapScreen() {


        System.out.println("loc is active");
        SharedPreferences.Editor peditor = pref.edit();

        peditor.putString("is_locate_clicked", "yes");
        peditor.commit();

        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;


            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {
                    String new_path = "/mnt/sdcard/DCIM/"  + str_losid + "_" + str_user_id + "_" + str_ts
                            + "_" + pd_label+"_MAP.jpg";


                    FileOutputStream out = new FileOutputStream(new_path);
                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);


                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("map_img_name", new_path);
                    peditor.putInt("is_map_captured", 1);
                    peditor.putString("pref_latitude", str_latitude);
                    peditor.putString("pref_longitude", str_longitude);
                    peditor.commit();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);

        // myMap is object of GoogleMap +> GoogleMap myMap;
        // which is initialized in onCreate() =>
        // myMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

}