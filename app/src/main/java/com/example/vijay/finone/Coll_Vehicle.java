package com.example.vijay.finone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.Dashboard;
import com.example.vijay.finone.GPSTracker_New;
import com.example.vijay.finone.libraries.ImageUtils;
import com.example.vijay.finone.MapsActivity;
import com.example.vijay.finone.libraries.MyAdapter;
import com.example.vijay.finone.libraries.ScalingUtilities;
import com.example.vijay.finone.libraries.UserFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vijay on 11/11/2016.
 */
public class Coll_Vehicle extends Fragment implements AdapterView.OnItemSelectedListener {


    static String str_full_structure_1, str_full_structure_2, str_full_structure_3;
    static  String str_full_structure_new_path_1 , str_full_structure_new_path_3,str_full_structure_new_path_2;

    static String ssstr_img_path;

    int MEDIA_IMAGE_COUNT , MEDIA_IMAGE_COUNT_1
            , MEDIA_IMAGE_COUNT_2
            , MEDIA_IMAGE_COUNT_3,MEDIA_IMAGE_COUNT_4,MEDIA_IMAGE_COUNT_5,MEDIA_IMAGE_COUNT_6;
    Integer img_width,img_height,img_com_size;


    private Uri url_file;

    int MEDIA_MAP_COUNT;
    int REQUEST_CAMERA = 100;
    private String imgPath;
    private static final String IMAGE_DIRECTORY_NAME = "/PD";
    int curr_DOC_no=0;

    SharedPreferences pref;
    UserFunctions userFunction;
    Spinner spn_distance_from_loc;
    EditText edt_distance_km;

    Location nwLocation;
    GPSTracker_New appLocationService;



    String str_distance_from_loc,str_distance_km;

    String lattitude = null, longitude = null;
    String map_address=null;


    double dbl_latitude = 0;
    double dbl_longitude = 0;

    DBHelper dbHelp;
    File new_file=null;

    int map_captured=0;

    String is_locate_clicked="";
    String map_path;

    String str_pd_photo_count,str_los,str_userid,str_ts;
    Bitmap my_bitmp=null;


    TextView location_lat,location_long;

    String str_img_path = null;

    public static final int MEDIA_TYPE_IMAGE = 1;
    File file;

    int i,j,k=0;

    Button btn_image1,btn_image2,btn_image3,  btn_locate_me;

    ImageView img1,img2,img3,img_map;
    SharedPreferences pData;
    ConnectionDetector cd;

    Spinner spn_vehicle_make;

    EditText edt_varient,edt_model,edt_seriel_no,edt_kilo_run;

    String str_vehicle_make,str_varient,str_model,str_seriel_no,str_kilo_run;

    String date_time;
    String str_uri;

    ArrayAdapter adp_make,adapter_distance_type;

    Button btn_next;

    String[] vehicle_make={"Select Vehicle Make",
            "ROLLS ROYCE",
            "HONDA",
            "QUATTRO",
            "ROLLS ROYCE",
            "FIAT INDIA",
            "FORD INDIA",
            "HINDUSTAN MOTORS",
            "MAHINDRA AND MAHINDRA",
            "NISSAN",
            "SKODA",
            "TATA MOTORS",
            "TOYOTA MOTOR",
            "VOLKSWAGEN INDIA",
            "MARUTI SUZUKI",
            "GENERAL MOTORS",
            "HYUNDAI MOTORS",
            "VOLKSWAGEN INDIA",
            "MERCEDES BENZ INDIA LTD",
            "TATA MOTORS",
            "BMW",
            "HUMMER",
            "RENAULT",
            "JLR",
            "CHEVROLET",
            "VOLVO INDIA PVT LTD.",
            "AUDI",
            "SSANGYONG",
            "LAND ROVER",
            "MITSUBISHI"};

    String[] distance_type = {"Select Distance", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_vehicle_make:

                if(spn_vehicle_make.getSelectedItemPosition()!=0) {

                    str_vehicle_make=vehicle_make[position].toString();

                }else{
                    str_vehicle_make="";
                }

                break;
            case R.id.spn_distance_from_loc:

                if(spn_distance_from_loc.getSelectedItemPosition()!=0) {

                    str_distance_from_loc=distance_type[position].toString();

                }else{
                    str_distance_from_loc="";
                }

                break;

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);

        View view = inflater.inflate(R.layout.pd_len_coll_vehicle, container, false);

        userFunction=new UserFunctions();
        cd= new ConnectionDetector(getContext());
        dbHelp=new DBHelper(getContext());

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        pData = getActivity().getSharedPreferences("pData", 0);
        pref = getContext().getSharedPreferences("MyPref", 0); // 0 -
        str_los= pData.getString("str_los", null);
        str_userid= pref.getString("user_id", null);
        str_ts= pData.getString("str_ts", null);

        img_height=pref.getInt("img_height", 600);
        img_width=pref.getInt("img_width", 400);
        img_com_size=pref.getInt("img_comp_size", 85);

        System.out.println("img_height:::"+img_height+"img_width:::"+img_width+"img_comp_size:::"+img_com_size);

        location_lat = (TextView) view.findViewById(R.id.location_lat);
        location_long = (TextView)view. findViewById(R.id.location_long);

        img_map = (ImageView) view.findViewById(R.id.img_map);
        img1 = (ImageView) view.findViewById(R.id.img_doc_1);
        img2 = (ImageView) view.findViewById(R.id.img_doc_2);
        img3 = (ImageView) view.findViewById(R.id.img_doc_3);

        btn_image1=(Button)view.findViewById(R.id.btn_DOC_1);
        btn_image2=(Button)view.findViewById(R.id.btn_DOC_3);
        btn_image3=(Button)view.findViewById(R.id.btn_DOC_2);
        btn_locate_me = (Button) view.findViewById(R.id.btn_locate_me);

        edt_varient=(EditText)view.findViewById(R.id.edt_varient);
        edt_kilo_run=(EditText)view.findViewById(R.id.edt_run);
        edt_model=(EditText)view.findViewById(R.id.edt_model);
        edt_seriel_no=(EditText)view.findViewById(R.id.edt_ser_no);

        btn_next=(Button)view.findViewById(R.id.btn_getotp);

        spn_distance_from_loc=(Spinner)view.findViewById(R.id.spn_distance_from_loc);
        edt_distance_km=(EditText)view.findViewById(
                R.id.edt_distance);


        view.setBackgroundColor(Color.WHITE);


        spn_vehicle_make=(Spinner)view.findViewById(R.id.spn_vehicle_make);

        edt_varient=(EditText)view.findViewById(R.id.edt_varient);
        edt_model=(EditText)view.findViewById(R.id.edt_model);
        edt_seriel_no=(EditText)view.findViewById(R.id.edt_ser_no);
        edt_kilo_run=(EditText)view.findViewById(R.id.edt_run);

        btn_next=(Button)view.findViewById(R.id.btn_getotp);



        adp_make = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                vehicle_make);
        adp_make
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_vehicle_make.setAdapter(adp_make);
        spn_vehicle_make.setOnItemSelectedListener(this);
        adp_make.notifyDataSetChanged();

        adapter_distance_type = new MyAdapter(getActivity(),
               R.layout.sp_display_layout,R.id.txt_visit, distance_type);
        adapter_distance_type
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);
        adapter_distance_type.notifyDataSetChanged();

        date_time=getDateTime();

        appLocationService = new GPSTracker_New(getContext());


        SharedPreferences.Editor editor1 = pref.edit();

        editor1.putString("is_locate_clicked", "");
        editor1.putInt("is_map_captured", 0);
        editor1.commit();

        str_full_structure_1="";
        str_full_structure_2="";
        str_full_structure_3="";

        if (appLocationService.isLocationEnabled) {
            dbl_latitude = appLocationService.getLatitude();
            dbl_longitude = appLocationService.getLongitude();
            //String addres = tracker.getCompleteAddressString(dbl_latitude, dbl_longitude, EditRequest.this);
            location_lat.setText("" + dbl_latitude);
            location_long.setText("" + dbl_longitude);


        } else {
            // show dialog box to user to enable location
            appLocationService.askToOnLocation();
        }



        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), getActivity());
                }


                map_captured = pref.getInt("is_map_captured", 0);

                if (map_captured == 0) {

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("is_locate_clicked", "");
                    peditor.commit();


                    Intent img_pinch = new Intent(
                            getActivity(), MapsActivity.class);

                    img_pinch.putExtra("str_losid", str_los);

                    img_pinch.putExtra("str_ts", str_ts);
                    img_pinch.putExtra("pd_label", "COLL");

                    startActivity(img_pinch);
                } else {
                    userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                            getActivity());
                }
            }

        });


        btn_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i = 1;
                curr_DOC_no = 4;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_los);
                peditor.commit();

                takePicture();
/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
                */

            }

        });


        btn_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                j = 1;

                curr_DOC_no = 6;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_los);
                peditor.commit();

                takePicture();
                /*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/
            }

        });

        btn_image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                k = 1;

                curr_DOC_no = 5;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_los);
                peditor.commit();

                takePicture();
/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/
            }


        });






        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Boolean ins=true;
                getdata();

                map_captured = pref.getInt("is_map_captured", 0);

                String map = Integer.toString(map_captured);
                String img_new_path = pref.getString("map_img_name", null);


                MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT_1 + MEDIA_IMAGE_COUNT_2 + MEDIA_IMAGE_COUNT_3+MEDIA_IMAGE_COUNT_4+MEDIA_IMAGE_COUNT_5+
                        MEDIA_IMAGE_COUNT_6;


                if (img_new_path != null) {
                    MEDIA_MAP_COUNT=1;
                    MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT+MEDIA_MAP_COUNT;
                }else{
                    MEDIA_MAP_COUNT=0;
                }


                try {

                    if((str_vehicle_make=="" || str_vehicle_make==null)&& ins==true){
                        userFunction.cutomToast("Please Select Vehicle Make",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_kilo_run.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Kilometer Run",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_kilo_run.length()<3 && ins==true){
                        userFunction.cutomToast("Please Enter min 3 digit Kilometer Run",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_varient.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Varient",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }



                try {

                    if(str_model.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Model",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_seriel_no.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Serial No",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_seriel_no.equals("0") && ins==true){
                        userFunction.cutomToast("Please Enter Non-zero value in Ownership Serial No",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_distance_from_loc.equals("") && ins==true){
                        userFunction.cutomToast("Please select Distance Type",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_distance_km.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Distance in KM",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }



                str_pd_photo_count = String.valueOf(MEDIA_IMAGE_COUNT);

                try {
                    if(MEDIA_MAP_COUNT>0){
                        if(MEDIA_IMAGE_COUNT<=3 && ins==true) {
                            userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                            ins = false;
                        }
                    }

                    if(MEDIA_MAP_COUNT==0){
                        if(MEDIA_IMAGE_COUNT<=2  && ins==true) {
                            userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    is_locate_clicked=pref.getString("is_locate_clicked","");

                    if (is_locate_clicked.equals("") && ins == true) {
                        userFunction.cutomToast("Please Capture MAP by clicking locate me button...",
                                getActivity());
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if((str_model.length()>4 || str_model.length()<4)  && ins==true){
                        userFunction.cutomToast("Please Enter Model of length 4 only",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                String map_latitude = pref.getString("pref_latitude", "0");
                String map_longitude = pref.getString("pref_longitude", "0");
                map_address = pref.getString("map_address", null);

                dbl_latitude = Double.parseDouble(map_latitude);
                dbl_longitude = Double.parseDouble(map_longitude);

                if (dbl_latitude > 0) {
                    lattitude = String.valueOf(dbl_latitude);
                    longitude = String.valueOf(dbl_longitude);
                    location_lat.setText(map_latitude);
                    location_long.setText(map_longitude);
                } else {
                    dbl_latitude = appLocationService.getLatitude();
                    dbl_longitude = appLocationService.getLongitude();

                    lattitude = String.valueOf(dbl_latitude);
                    longitude = String.valueOf(dbl_longitude);
                }

                lattitude=location_lat.getText().toString();
                longitude=location_long.getText().toString();


                System.out.println("map_captured::::" + map_captured + "::::str_distance_type::::" + str_distance_from_loc + "::::str_txt_distacne:::" + str_distance_km);
                try {
                    if (map_captured == 1 && ins == true) {
                        if (str_distance_from_loc.equals("Map not loading") == true) {
                            if (dbl_latitude>0) {
                                userFunction.cutomToast("You already captured map... select other Distance type..",
                                        getActivity());
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (map_captured == 1 && ins == true) {
                        if ((str_distance_from_loc.equals("Exact location")) || ((str_distance_from_loc.equals("Map not loading")))) {
                            ins = true;
                        }else{
                            if (Integer.parseInt(str_distance_km)==0) {
                                userFunction.cutomToast("Distance cant be a Zero...",
                                        getActivity());
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }



                if(ins==true){

                    Boolean tag=dbHelp.update_vehicle(str_kilo_run,str_model,str_varient,str_seriel_no,str_vehicle_make,str_ts,"1",str_distance_from_loc,str_distance_km,str_userid,lattitude,longitude,str_pd_photo_count,map_address);

                    if (img_new_path != null && dbl_latitude>0) {

                        dbHelp.insertImage(img_new_path, str_los, str_los, str_ts, map, "0", "MAP");
                    }

                    if(!str_full_structure_1.equals("")){

                        dbHelp.insertImage(str_full_structure_1, str_los, str_userid, str_ts, map, "0", "vis_card");

                    }
                    if(!str_full_structure_2.equals("")){

                        dbHelp.insertImage(str_full_structure_2, str_los, str_userid, str_ts, map, "0", "int_setup");
                    }

                    if(!str_full_structure_3.equals("")) {
                        dbHelp.insertImage(str_full_structure_3, str_los, str_userid, str_ts, map, "0", "ext_setup");
                    }

                    if (cd.isConnectingToInternet() == true) {

                        if (tag == true) {

                            dbHelp.updateImage(str_ts, 1);
                            dbHelp.get_Collateral_request(str_ts, "ON");

                         //   getActivity().finish();
                        }

                    }else{
                        userFunction.cutomToast(getResources().getString(R.string.local_data), getActivity());

                        dbHelp.updateImage(str_ts, 1);
                        Intent home_activity = new Intent(getActivity(),
                                Dashboard.class);
                        startActivity(home_activity);

                        getActivity().finish();
                    }

                }

            }
        });


        return view;
    }


    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // Check for SD Card
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                        .show();
            } else {
                // Locate the image folder in your SD Card
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "HDB");
                // Create a new folder if no folder named SDImageTutorial exist
                file.mkdirs();
            }

            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;
        try {

            String curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


            if (type == MEDIA_TYPE_IMAGE) {
                file = new File(mediaStorageDir, str_los + "_" + str_userid + "_"
                        + str_ts + "_" + "COLL"+"_"+curr_no + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                this.imgPath = file.getAbsolutePath();
                System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
                SharedPreferences.Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;

                    default:
                        break;

                }
                editor.commit();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        str_uri = pref.getString("str_uri", null);
        url_file=Uri.parse(str_uri);
        System.out.println("STR IMG PTH1111::::"+url_file);

            System.out.println("STR IMG PTH2222::::"+url_file);



            try {
                if (requestCode == REQUEST_CAMERA) {
                    if (resultCode == Activity.RESULT_OK) {

                        if (url_file != null) {
                            // String str_img_path = url_file.toString();
                            str_uri = pref.getString("str_uri", null);
                            url_file = Uri.parse(str_uri);

                            String img_new;
                            File new_img_file = new File(url_file.toString());
                            img_new = new_img_file.getAbsolutePath();

                            try {
                                img_new = img_new.replace("/file:", "");

                                Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(img_new, img_height, img_width, str_userid, str_los, str_ts, img_com_size, lattitude, longitude, date_time);


                                ssstr_img_path = storeImage(bitmap, str_userid, str_los, str_ts, curr_DOC_no);

                            } catch (Exception e) {
                                ssstr_img_path = new_file.getAbsolutePath();
                                ssstr_img_path = ssstr_img_path.replace("/file:", "");
                            }
                            SharedPreferences.Editor editor = pData.edit();

                            System.out.println("STR IMG PTH3333::::" + ssstr_img_path);

                            switch (pData.getInt("curr_DOC_no", 0)) {
                                case 4:
                                    String picUri = pData.getString("str_DOC_1", null);
                                    display_img(ssstr_img_path, img1, "4");

                                    onCaptureImageResult(data, 4, ssstr_img_path, btn_image1);

                                    break;

                                case 5:

                                    String picUri2 = pData.getString("str_DOC_2", null);

                                    display_img(ssstr_img_path, img2, "5");
                                    onCaptureImageResult(data, 5, ssstr_img_path, btn_image2);

                                    break;

                                case 6:


                                    String picUri3 = pData.getString("str_DOC_3", null);

                                    display_img(ssstr_img_path, img3, "6");
                                    onCaptureImageResult(data, 6, ssstr_img_path, btn_image3);

                                    break;


                            }
                        }
                    }

                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        // Delete the cancelled image

                        try {
                            //deleteLastPic();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }

                        SharedPreferences.Editor editor = pData.edit();
                        switch (pData.getInt("curr_DOC_no", 0)) {
                            case 4:
                                if (fileDelete(pData.getString("str_DOC_1", null))) {
                                    editor.putString("str_DOC_1", "");
                                }


                                break;
                            case 5:
                                if (fileDelete(pData.getString("str_DOC_2", null))) {
                                    editor.putString("str_DOC_2", "");
                                }

                                break;
                            case 6:
                                if (fileDelete(pData.getString("str_DOC_3", null))) {
                                    editor.putString("str_DOC_3", "");
                                }
                                break;

                            default:
                                break;

                        }

                        editor.commit();

                        // user cancelled Image capture
                        Toast.makeText(getActivity(),
                                "User cancelled image capture", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        // failed to capture image
                        Toast.makeText(getActivity(),
                                "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                                .show();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }



    }


    boolean display_img(String pic_uri,ImageView thumb_nail,String doc_no){

        if(thumb_nail!=img_map) {
            str_img_path = decodeFile(pic_uri,doc_no);

            File img_file = new File(str_img_path);
            if (img_file.exists()) {
                my_bitmp = BitmapFactory.decodeFile(img_file.getAbsolutePath());

            }
        }
        thumb_nail.setImageBitmap(my_bitmp);
        return  true;
    }

    private String decodeFile(String path,String docno) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        try {
            System.out.println("::::::-3" + imgPath);
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(imgPath, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= 600 && unscaledBitmap.getHeight() <= 800)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }
            System.out.println("::::::-2");
            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/PD");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            System.out.println("::::::-1");

            String s = str_los + "_" + str_userid + "_"
                    + str_ts + "_" + "COLL" + "_" + docno + ".jpg";

            File f = new File(mFolder.getAbsolutePath(), s);

            System.out.println("::::::0");

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                System.out.println("::::::1");
                Point p = new Point();
                p.set(10, 20);
                //  scaledBitmap = waterMark(scaledBitmap, "Form No= " + form_no + " LOS ID= " + losid + " Username = " + userid, p, Color.BLACK, 90, 14, true);
                System.out.println("::::::2");
                p.set(10, 40);
                // scaledBitmap = waterMark(scaledBitmap, "Lat= " + map_lat + " Long=" + map_long + " Time=" + ts_date, p, Color.BLACK, 90, 14, true);
                fos = new FileOutputStream(f);
                System.out.println("::::::3");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("::::::4");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("::::::5");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            //decodeFile(path);
            //compressImage(path);
            System.out.println("::::::6");

        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                        System.out.println(path + " : Deleted");
                    } else {
                        System.out.println(path + " : CANT Dieleted");
                    }
                } else {
                    System.out.println(path + " : Do Not Exists");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }


    private void onCaptureImageResult(Intent data,  Integer img_no, String img_uri,Button dest_btn) {

      /*  boolean is_losid_empty= isEmptyString(str_losid_no);

        if(is_losid_empty==true){
            str_losid_no="null";
        }
        */

        SharedPreferences.Editor editor = pData.edit();
        switch (img_no) {
            case 4:

                String new_path = str_los + "_" + str_los + "_"
                        + str_ts + "_" + "COLL" + "_" + "4";

                editor.putString("str_DOC_1", img_uri);
                editor.putString("str_New_DOC_1", new_path);
                editor.commit();

                MEDIA_IMAGE_COUNT_1 = 1;

                str_full_structure_1 = pData.getString("str_DOC_1", null);
                str_full_structure_new_path_1 = pData.getString("str_New_DOC_1", null);

                System.out.println("PHOTO 1::::" + str_full_structure_new_path_1);

                break;
            case 5:
                String new_path2 = str_los + "_" + str_userid + "_"
                        + str_ts + "_" + "COLL" + "_" + "5";

                editor.putString("str_DOC_2", img_uri);
                editor.putString("str_New_DOC_2", new_path2);
                editor.commit();
                MEDIA_IMAGE_COUNT_2 = 1;

                str_full_structure_2 = pData.getString("str_DOC_2", null);
                str_full_structure_new_path_2 = pData.getString("str_New_DOC_2", null);
                break;
            case 6:

                String new_path3 = str_los + "_" + str_userid + "_" + str_ts + "_"
                        + "_" + "COLL" + "_" + "6";

                editor.putString("str_DOC_3", img_uri);
                editor.putString("str_New_DOC_3", new_path3);
                editor.commit();

                MEDIA_IMAGE_COUNT_3 = 1;

                str_full_structure_3 = pData.getString("str_DOC_3", null);
                str_full_structure_new_path_3 = pData.getString("str_New_DOC_3", null);
                break;


            default:
                break;
        }

    }


    void getdata(){

        str_kilo_run=edt_kilo_run.getText().toString();
        str_model=edt_model.getText().toString();
        str_seriel_no=edt_seriel_no.getText().toString();
        str_varient=edt_varient.getText().toString();

        str_distance_km=edt_distance_km.getText().toString();
    }



    private  File getOutputMediaFile1235(){

        String curr_no=null;
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        /*
        File mediaStorageDir = new File(Environment.getExternallStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
                 */
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }

        Uri imgUri = null;
        try {

            curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


            file = new File(mediaStorageDir, str_los + "_" + str_userid + "_"
                    + str_ts + "_" + "COLL"+"_"+curr_no + ".jpeg");

            if (file.exists()) {
                file.delete();
            }
            imgUri = Uri.fromFile(file);
            this.imgPath = file.getAbsolutePath();
            System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
            SharedPreferences.Editor editor = pData.edit();

            switch (Integer.parseInt(curr_no)) {
                case 4:
                    editor.putString("str_DOC_1", this.imgPath);

                    break;
                case 5:
                    editor.putString("str_DOC_2", this.imgPath);
                    break;
                case 6:
                    editor.putString("str_DOC_3", this.imgPath);
                    break;

                default:
                    break;
            }
            editor.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        new_file=new File(mediaStorageDir.getPath() + File.separator + str_los + "_" + str_userid + "_" + str_ts + "_COLL_" + curr_no + ".JPEG");
        return new_file;
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        url_file = Uri.fromFile(getOutputMediaFile1235());
        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove("str_uri");
        peditor.commit();
        str_uri=url_file.toString();
        peditor.putString("str_uri", str_uri);
        peditor.commit();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        startActivityForResult(intent, 100);
    }


    private String storeImage(Bitmap image, String userid, String losid, String ts, Integer str_pic_name) {
        String new_strMyImagePath = null;
        String pic_id = str_pic_name.toString();
        File pictureFile = getOutputMediaFile2(userid, losid, ts, pic_id);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

            return new_strMyImagePath;
        }
    }

    private File getOutputMediaFile2(String userid, String losid, String ts, String str_pic_name) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = losid + "_" + userid + "_" + ts + "_COLL_" + str_pic_name + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy:hhmmss");
        String format = s.format(new Date());
        return format;
    }

}
