package com.example.vijay.finone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.Dashboard;
import com.example.vijay.finone.GPSTracker_New;

import com.example.vijay.finone.libraries.ImageUtils;
import com.example.vijay.finone.MapsActivity;
import com.example.vijay.finone.libraries.MyAdapter;
import com.example.vijay.finone.libraries.ScalingUtilities;
import com.example.vijay.finone.libraries.UserFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vijay on 11/11/2016.
 */
public class Coll_Property extends Fragment implements AdapterView.OnItemSelectedListener {

    int MEDIA_IMAGE_COUNT , MEDIA_IMAGE_COUNT_1
            , MEDIA_IMAGE_COUNT_2
            , MEDIA_IMAGE_COUNT_3,MEDIA_IMAGE_COUNT_4,MEDIA_IMAGE_COUNT_5,MEDIA_IMAGE_COUNT_6;

    File new_file=null;

    String map_address=null;


    int MEDIA_MAP_COUNT;
    int REQUEST_CAMERA = 100;
    private String imgPath;
    private static final String IMAGE_DIRECTORY_NAME = "/PD";
    int curr_DOC_no=0;

    SharedPreferences pref;
    UserFunctions userFunction;

    DBHelper dbHelp;

    int map_captured=0;
    String str_uri;
    String is_locate_clicked="";
    String map_path;

    String str_pd_photo_count,str_los,str_userid,str_ts;
    Bitmap my_bitmp=null;

    String str_img_path = null;
    public static final int MEDIA_TYPE_IMAGE = 1;
    File file;

    int i,j,k=0;

    double dbl_latitude = 0;
    double dbl_longitude = 0;

    private Uri url_file;

    Spinner spn_prop_type,spn_prop_status,spn_bank_notice,spn_redev_notice,spn_soc_visit,spn_tenant_vintage,spn_prop_access,spn_prop_area_matcg,spn_prop_area,
    spn_maketability;

    TextView location_lat,location_long;

    String str_distance_from_loc,str_distance_km,str_soc_visit;
    EditText edt_ref1,edt_ref2,edt_rate1,edt_rate2;
    String lattitude = null, longitude = null;

    Button btn_next,btn_image1,btn_image2,btn_image3,  btn_locate_me;

    ImageView img1,img2,img3,img_map;
    SharedPreferences pData;
    ConnectionDetector cd;

    RelativeLayout lay_tenant;

    Spinner spn_distance_from_loc;
    EditText edt_distance_km;

    Integer img_width,img_height,img_com_size;

    Location nwLocation;
    GPSTracker_New appLocationService;

    static  String ssstr_img_path;

    ArrayAdapter adp_bank_notice,adp_redevelope_notice,adp_soc_visit_notice,adp_tenant_vintage,adp_prop_are,adp_prop_type,adp_prop_status,adp_prop_access,adp_prop_area,adp_marketability,adapter_distance_type;

    String str_prop_status,str_prop_type,str_bank_notice,str_redev_notice,str_sco_off,str_tenant_vintage,str_prop_access,str_prop_area_match,str_prop_area,str_marketability,str_ref1,str_rate_quote1,str_ref2,str_rate_quote2;

    String[] prop_type={"Select Property Type","Flat","Independent House","Row House","Industrial Property","Shop","Godown","Res cum Office","Plot","Any Other"};

    String[] prop_status={"Select Property Status","Self Occupied","Rented","Part rented but self occupied","Vacant"};

    String[] prop_access={"Select Property Access","Easily Accessible by Car","Easily Accessible by Two Wheeler","Tough to access property","Not Traceable"};

    String[] prop_area={"Select Property Area","<500 sqft","501-1000 sqft","1001-1500 sqft","1501-3000 sqft","3001-5000 sqft","5001-10000 sqft",">10000 sqft"};

    String[] marketability={"Select Marketability","Good Quality Good Liquidity","Poor Quality Good Liquidity","Good Quality Poor Liquidity","Poor Quality Poor Liquidity"};

    String[] bank_noticeyesno={"Select bank notice at premises","Yes","No"};
    String[] redevelope_noticeyesno={"Select Redevelopement Notice","Yes","No"};
    String[] soc_visit_noticeyesno={"Select society visit for ownership","Yes","No"};
    String[] tenant_vintageyesno={"Select tenant vintage confirm if rented","<6M","7M-12M","13M-24M","25M-36M",">36M"};

    String[] prop_areyesno={"Select Property area match with docs", "Yes", "No"};

    String[] distance_type = {"Select Distance", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    String date_time;

    static String str_full_structure_1, str_full_structure_2, str_full_structure_3;
    static  String str_full_structure_new_path_1 , str_full_structure_new_path_3,str_full_structure_new_path_2;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_prop_type:

                if(position!=0){
                    str_prop_type=prop_type[position];
                }else{
                    str_prop_type="";
                }
                break;

            case R.id.spn_prop_status:

                if(position!=0){

                    str_prop_status=prop_status[position];

                    if(position==2){
                        lay_tenant.setVisibility(View.VISIBLE);
                    }else{
                        lay_tenant.setVisibility(View.GONE);
                        spn_tenant_vintage.setSelection(0);
                        str_tenant_vintage="";
                    }


                }else {
                    str_prop_status="";
                }
                break;

            case R.id.spn_bank_notice:

                if(position!=0){
                    str_bank_notice=bank_noticeyesno[position];
                }else {
                    str_bank_notice="";
                }
                break;

            case R.id.spn_redevelope_notice:

                if(position!=0){
                    str_redev_notice=redevelope_noticeyesno[position];
                }else {
                    str_redev_notice="";
                }
                break;

            case R.id.spn_tenant_vintage:

                if(position!=0){
                    str_tenant_vintage=tenant_vintageyesno[position];
                }else {
                    str_tenant_vintage="";

                }
                break;

            case R.id.spn_soc_visit:

                if(position!=0){
                    str_soc_visit=soc_visit_noticeyesno[position];
                }else {
                    str_soc_visit="";
                }
                break;

            case R.id.spn_property_access:

                if(position!=0){
                    str_prop_access=prop_access[position];
                }else {
                    str_prop_access="";
                }
                break;

            case R.id.spn_property_area_match:

                if(position!=0){
                    str_prop_area_match=prop_areyesno[position];
                }else {
                    str_prop_area_match="";

                }
                break;

            case R.id.spn_property_area:

                if(position!=0){
                    str_prop_area=prop_area[position];
                }else {
                    str_prop_area="";

                }
                break;

            case R.id.spn_marketability:

                if(position!=0){
                    str_marketability=marketability[position];
                }else {
                    str_marketability="";

                }
                break;

            case R.id.spn_distance_from_loc:

                if(spn_distance_from_loc.getSelectedItemPosition()!=0) {

                    str_distance_from_loc=distance_type[position];

                }else {
                    str_distance_from_loc="";
                }

                break;

        }
        }

            @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
                super.onCreateView(inflater,container,savedInstanceState);

                View view = inflater.inflate(R.layout.pd_len_coll_property, container, false);

                userFunction=new UserFunctions();
                cd= new ConnectionDetector(getContext());
                dbHelp=new DBHelper(getContext());

                pData = getActivity().getSharedPreferences("pData", 0);
                pref = getContext().getSharedPreferences("MyPref", 0); // 0 -
                 str_los= pData.getString("str_los", null);
                str_userid= pref.getString("user_id", null);
                str_ts= pData.getString("str_ts", null);

                date_time=getDateTime();

                System.out.println("jSBDKJA" + str_ts);

                location_lat = (TextView) view.findViewById(R.id.location_lat);
                location_long = (TextView)view. findViewById(R.id.location_long);

                img_map = (ImageView) view.findViewById(R.id.img_map);
                img1 = (ImageView) view.findViewById(R.id.img_doc_1);
                img2 = (ImageView) view.findViewById(R.id.img_doc_2);
                img3 = (ImageView) view.findViewById(R.id.img_doc_3);

        spn_prop_type=(Spinner)view.findViewById(R.id.spn_prop_type);
        spn_prop_status=(Spinner)view.findViewById(R.id.spn_prop_status);
        spn_bank_notice=(Spinner)view.findViewById(R.id.spn_bank_notice);
        spn_redev_notice=(Spinner)view.findViewById(R.id.spn_redevelope_notice);
        spn_soc_visit=(Spinner)view.findViewById(R.id.spn_soc_visit);
        spn_tenant_vintage=(Spinner)view.findViewById(R.id.spn_tenant_vintage);
        spn_prop_access=(Spinner)view.findViewById(R.id.spn_property_access);
        spn_prop_area=(Spinner)view.findViewById(R.id.spn_property_area);
        spn_prop_area_matcg=(Spinner)view.findViewById(R.id.spn_property_area_match);
        spn_maketability=(Spinner)view.findViewById(R.id.spn_marketability);

                lay_tenant=(RelativeLayout)view.findViewById(R.id.lay_tenant);

                spn_distance_from_loc=(Spinner)view.findViewById(R.id.spn_distance_from_loc);
                edt_distance_km=(EditText)view.findViewById(
                        R.id.edt_distance);

                edt_rate1=(EditText)view.findViewById(R.id.edt_rate1);
                edt_rate2=(EditText)view.findViewById(R.id.edt_rate2);
                edt_ref1=(EditText)view.findViewById(R.id.edt_ref1);
                edt_ref2=(EditText)view.findViewById(R.id.edt_ref2);

                btn_image1=(Button)view.findViewById(R.id.btn_DOC_1);
                btn_image2=(Button)view.findViewById(R.id.btn_DOC_2);
                btn_image3=(Button)view.findViewById(R.id.btn_DOC_3);
                 btn_locate_me = (Button) view.findViewById(R.id.btn_locate_me);

                btn_next=(Button)view.findViewById(R.id.btn_getotp);

                view.setBackgroundColor(Color.WHITE);

                adp_bank_notice = new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        bank_noticeyesno);
                adp_bank_notice
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_bank_notice.setAdapter(adp_bank_notice);
                spn_bank_notice.setOnItemSelectedListener(this);
                adp_bank_notice.notifyDataSetChanged();


                adp_prop_status= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        prop_status);
                adp_prop_status
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_prop_status.setAdapter(adp_prop_status);
                spn_prop_status.setOnItemSelectedListener(this);
                adp_prop_status.notifyDataSetChanged();

                adp_prop_type= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        prop_type);
                adp_prop_type
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_prop_type.setAdapter(adp_prop_type);
                spn_prop_type.setOnItemSelectedListener(this);;
                adp_prop_type.notifyDataSetChanged();

                adp_redevelope_notice= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        redevelope_noticeyesno);
                adp_redevelope_notice
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_redev_notice.setAdapter(adp_redevelope_notice);
                spn_redev_notice.setOnItemSelectedListener(this);
                adp_redevelope_notice.notifyDataSetChanged();

                adp_soc_visit_notice= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        soc_visit_noticeyesno);
                adp_soc_visit_notice
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_soc_visit.setAdapter(adp_soc_visit_notice);
                spn_soc_visit.setOnItemSelectedListener(this);
                adp_soc_visit_notice.notifyDataSetChanged();

                adp_tenant_vintage= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        tenant_vintageyesno);
                adp_tenant_vintage
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_tenant_vintage.setAdapter(adp_tenant_vintage);
                spn_tenant_vintage.setOnItemSelectedListener(this);
                adp_tenant_vintage.notifyDataSetChanged();

                adp_prop_access= new MyAdapter(getActivity(), R.layout.sp_display_layout,R.id.txt_visit,
                        prop_access);
                adp_prop_access
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_prop_access.setAdapter(adp_prop_access);
                spn_prop_access.setOnItemSelectedListener(this);
                adp_prop_access.notifyDataSetChanged();

                adp_prop_are= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        prop_areyesno);
                adp_prop_are
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_prop_area_matcg.setAdapter(adp_prop_are);
                spn_prop_area_matcg.setOnItemSelectedListener(this);
                adp_prop_are.notifyDataSetChanged();

                adp_prop_area= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,
                        prop_area);
                adp_prop_area
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_prop_area.setAdapter(adp_prop_area);
                spn_prop_area.setOnItemSelectedListener(this);
                adp_prop_are.notifyDataSetChanged();

                adp_marketability= new MyAdapter(getActivity(),R.layout.sp_display_layout,R.id.txt_visit,marketability
                        );
                adp_marketability
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_maketability.setAdapter(adp_marketability);
                spn_maketability.setOnItemSelectedListener(this);
                adp_marketability.notifyDataSetChanged();

                adapter_distance_type = new MyAdapter(getActivity(),
                        R.layout.sp_display_layout,R.id.txt_visit, distance_type);
                adapter_distance_type
                        .setDropDownViewResource(R.layout.spinner_layout);
                spn_distance_from_loc.setAdapter(adapter_distance_type);
                spn_distance_from_loc.setOnItemSelectedListener(this);
                adapter_distance_type.notifyDataSetChanged();


                appLocationService = new GPSTracker_New(getContext());

               // nwLocation = appLocationService
               //         .getLocation();


                if (appLocationService.isLocationEnabled) {
                    dbl_latitude = appLocationService.getLatitude();
                    dbl_longitude = appLocationService.getLongitude();
                    //String addres = tracker.getCompleteAddressString(dbl_latitude, dbl_longitude, EditRequest.this);
                    location_lat.setText("" + dbl_latitude);
                    location_long.setText("" + dbl_longitude);


                } else {
                    // show dialog box to user to enable location
                    appLocationService.askToOnLocation();
                }

                SharedPreferences.Editor editor = pref.edit();

                editor.putString("is_locate_clicked", "");
                editor.putInt("is_map_captured", 0);

                editor.commit();

                img_height=pref.getInt("img_height",600);
                img_width=pref.getInt("img_width", 400);
                img_com_size=pref.getInt("img_comp_size", 85);

                double ltt = 0;
                double lott = 0;
                str_full_structure_1="";
                str_full_structure_2="";
                str_full_structure_3="";

                btn_locate_me.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!cd.isConnectingToInternet()) {
                            userFunction.cutomToast(getResources().getString(R.string.no_internet), getActivity());
                        }

                        map_captured = pref.getInt("is_map_captured", 0);

                        if (map_captured == 0) {

                            SharedPreferences.Editor peditor = pref.edit();
                            peditor.putString("is_locate_clicked", "");
                            peditor.commit();

                            Intent img_pinch = new Intent(
                                    getActivity(), MapsActivity.class);
                            img_pinch.putExtra("str_losid", str_los);

                            img_pinch.putExtra("str_ts", str_ts);
                            img_pinch.putExtra("pd_label", "COLL");

                            startActivity(img_pinch);
                        } else {
                            userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                                    getActivity());
                        }
                    }

                });


                btn_image1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        i = 1;
                        curr_DOC_no = 1;

                        SharedPreferences.Editor peditor = pData.edit();
                        peditor.putInt("curr_DOC_no", curr_DOC_no);
                        peditor.putString("str_losid_no", str_los);
                        peditor.commit();

                        takePicture();
/*
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                getOutputMediaFile(MEDIA_TYPE_IMAGE));
                        startActivityForResult(intent, REQUEST_CAMERA);
*/
                    }

                });


                btn_image2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        j = 1;

                        curr_DOC_no = 2;


                        SharedPreferences.Editor peditor = pData.edit();
                        peditor.putInt("curr_DOC_no", curr_DOC_no);
                        peditor.putString("str_losid_no", str_los);
                        peditor.commit();

                        takePicture();
/*
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                getOutputMediaFile(MEDIA_TYPE_IMAGE));
                        startActivityForResult(intent, REQUEST_CAMERA);
                        */

                    }

                });

                btn_image3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        k=1;

                        curr_DOC_no = 3;



                        SharedPreferences.Editor peditor = pData.edit();
                        peditor.putInt("curr_DOC_no", curr_DOC_no);
                        peditor.putString("str_losid_no", str_los);
                        peditor.commit();

                        /*
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                getOutputMediaFile(MEDIA_TYPE_IMAGE));
                        startActivityForResult(intent, REQUEST_CAMERA);

                        */

                        takePicture();
                    }


                });


                btn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        getdata();
                        boolean ins=true;

                        map_captured = pref.getInt("is_map_captured", 0);

                        String map = Integer.toString(map_captured);
                        String img_new_path = pref.getString("map_img_name", null);


                        MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT_1 + MEDIA_IMAGE_COUNT_2 + MEDIA_IMAGE_COUNT_3+MEDIA_IMAGE_COUNT_4+MEDIA_IMAGE_COUNT_5+
                                MEDIA_IMAGE_COUNT_6;


                        if (img_new_path != null) {
                            MEDIA_MAP_COUNT=1;
                            MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT+MEDIA_MAP_COUNT;
                        }else{
                            MEDIA_MAP_COUNT=0;
                        }


                        try {

                            if((str_prop_type=="" || str_prop_type==null)&& ins==true){
                                userFunction.cutomToast("Please Select Property Type",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_prop_status=="" || str_prop_status==null)&& ins==true){
                                userFunction.cutomToast("Please Select Property Status",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_bank_notice=="" || str_bank_notice==null)&& ins==true){
                                userFunction.cutomToast("Please Select Bank Noticee at premises ",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_redev_notice=="" || str_redev_notice==null)&& ins==true){
                                userFunction.cutomToast("Please Select Redevelopement",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_soc_visit=="" || str_soc_visit==null)&& ins==true){
                                userFunction.cutomToast("Please Select Society Visit for ownership confirm",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }



                        try {

                            if((str_tenant_vintage=="" || str_tenant_vintage==null)&& str_prop_status.equals("Rented")&& ins==true){
                                userFunction.cutomToast("Please Select Tenant Vintage Confirm if rented",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_prop_access=="" || str_prop_access==null)&& ins==true){
                                userFunction.cutomToast("Please Select Property Access",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_prop_area_match=="" || str_prop_access==null)&& ins==true){
                                userFunction.cutomToast("Please Select Property Area match",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_prop_area=="" || str_prop_area==null)&& ins==true){
                                userFunction.cutomToast("Please Select Property Area",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_marketability=="" || str_marketability==null)&& ins==true){
                                userFunction.cutomToast("Please Select Marketability",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if(str_ref1.equals("") && ins==true){
                                userFunction.cutomToast("Please Enter Reference 1",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try {

                            if(edt_rate1.equals("") && ins==true){
                                userFunction.cutomToast("Please Enter Rate Quote 1 1",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if(edt_ref2.equals("") && ins==true){
                                userFunction.cutomToast("Please Enter Reference 2",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }


                        try {

                            if(edt_rate2.equals("") && ins==true){
                                userFunction.cutomToast("Please Enter Rate Quote 2 2",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if((str_distance_from_loc=="" || str_distance_from_loc==null)&& ins==true){
                                userFunction.cutomToast("Please Select Distance Type",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }


                        str_pd_photo_count = String.valueOf(MEDIA_IMAGE_COUNT);

                        try {
                            if(MEDIA_MAP_COUNT>0){
                                if(MEDIA_IMAGE_COUNT<=3 && ins==true) {
                                    userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                                    ins = false;
                                }
                            }

                            if(MEDIA_MAP_COUNT==0){
                                if(MEDIA_IMAGE_COUNT<=2 && ins==true) {
                                    userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                                    ins = false;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            if(str_distance_from_loc.equals("") && ins==true){
                                userFunction.cutomToast("Please select Distance Type",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        try {

                            if(str_distance_km.equals("") && ins==true){
                                userFunction.cutomToast("Please Enter Distance in KM",getActivity());
                                ins=false;

                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }



                        try {

                            is_locate_clicked=pref.getString("is_locate_clicked","");

                            if (is_locate_clicked.equals("") && ins == true) {
                                userFunction.cutomToast("Please Capture MAP by clicking locate me button...",
                                        getActivity());
                                ins = false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                        String map_latitude = pref.getString("pref_latitude", "0");
                        String map_longitude = pref.getString("pref_longitude", "0");
                        map_address = pref.getString("map_address", null);

                        dbl_latitude = Double.parseDouble(map_latitude);
                        dbl_longitude = Double.parseDouble(map_longitude);



                        if (dbl_latitude > 0) {
                            lattitude = String.valueOf(dbl_latitude);
                            longitude = String.valueOf(dbl_longitude);
                            //location_lat.setText(map_latitude);
                            // location_long.setText(map_longitude);

                            location_lat.setText(map_latitude);
                            location_long.setText(map_longitude);
                        } else {
                            dbl_latitude = appLocationService.getLatitude();
                            dbl_longitude = appLocationService.getLongitude();

                            lattitude = String.valueOf(dbl_latitude);
                            longitude = String.valueOf(dbl_longitude);

                        }

                        lattitude=location_lat.getText().toString();
                        longitude=location_long.getText().toString();



                        System.out.println("map_captured::::" + map_captured + "::::str_distance_type::::" + str_distance_from_loc + "::::str_txt_distacne:::" + str_distance_km);
                        try {
                            if (map_captured == 1 && ins == true) {
                                if (str_distance_from_loc.equals("Map not loading") == true) {
                                    if (dbl_latitude>0) {
                                        userFunction.cutomToast("You already captured map... select other Distance type..",
                                                getActivity());
                                        ins = false;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (map_captured == 1 && ins == true) {
                                if ((str_distance_from_loc.equals("Exact location")) || ((str_distance_from_loc.equals("Map not loading")))) {
                                    ins = true;
                                }else{
                                    if (Integer.parseInt(str_distance_km)==0) {
                                        userFunction.cutomToast("Distance cant be a Zero...",
                                                getActivity());
                                        ins = false;
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }




                        if(ins==true) {

                          //  getdata();

                            Boolean tag=dbHelp.update_property(str_prop_type,str_prop_status,str_bank_notice,str_redev_notice,str_soc_visit,str_tenant_vintage,str_prop_access,str_prop_area_match,str_prop_area,str_marketability,str_ref1,str_ref2,str_rate_quote1,str_rate_quote2,str_ts,"1",str_distance_from_loc,str_distance_km,str_userid,str_pd_photo_count,lattitude,longitude,map_address);

                            if(!str_full_structure_1.equals("")) {
                                dbHelp.insertImage(str_full_structure_1, str_los, str_userid, str_ts, map, "0", "vis_card");
                            }
                            if(!str_full_structure_2.equals("")) {
                                dbHelp.insertImage(str_full_structure_2, str_los, str_userid, str_ts, map, "0", "int_setup");
                            }
                            if(!str_full_structure_3.equals("")) {
                                dbHelp.insertImage(str_full_structure_3, str_los, str_userid, str_ts, map, "0", "ext_setup");
                            }

                            if (img_new_path != null && dbl_latitude>0) {
                                dbHelp.insertImage(img_new_path, str_los, str_los, str_ts, map, "0", "MAP");
                            }

                            if (cd.isConnectingToInternet() == true) {
                                if (tag == true) {

                                    dbHelp.get_Collateral_request(str_ts, "ON");
                                    dbHelp.updateImage(str_ts, 1);
                                  //  getActivity().finish();

                                }
                            }else{
                                userFunction.cutomToast(getResources().getString(R.string.local_data), getActivity());

                                dbHelp.updateImage(str_ts, 1);
                                Intent home_activity = new Intent(getActivity(),
                                        Dashboard.class);
                                startActivity(home_activity);
                                getActivity().finish();
                            }

                        }

                    }
                });

                return view;
    }

    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // Check for SD Card
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                        .show();
            } else {
                // Locate the image folder in your SD Card
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "HDB");
                // Create a new folder if no folder named SDImageTutorial exist
                file.mkdirs();
            }

            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;
        try {

            String curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


            if (type == MEDIA_TYPE_IMAGE) {
                file = new File(mediaStorageDir, str_los + "_" + str_userid + "_"
                        + str_ts + "_" + "COLL"+"_"+curr_no + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                this.imgPath = file.getAbsolutePath();
                System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
                SharedPreferences.Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;

                    default:
                        break;

                }
                editor.commit();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


            try {
                if (requestCode == REQUEST_CAMERA) {
                    if (resultCode == Activity.RESULT_OK) {

                        if (url_file != null) {
                            str_uri = pref.getString("str_uri", null);
                            url_file = Uri.parse(str_uri);

                            String new_img;
                            File new_img_file=new File(url_file.toString());
                            new_img = new_img_file.getAbsolutePath();



                            try {
                              //  str_img_path = str_img_path.replace("/file:", "");

                                Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(new_img, img_height, img_width, str_userid, str_los, str_ts, img_com_size, lattitude, longitude, date_time);

                                ssstr_img_path = storeImage(bitmap, str_userid, str_los, str_ts, curr_DOC_no);

                            } catch (Exception e) {
                                ssstr_img_path = new_file.getAbsolutePath();
                                ssstr_img_path = ssstr_img_path.replace("/file:", "");
                            }


                            SharedPreferences.Editor editor = pData.edit();

                            switch (pData.getInt("curr_DOC_no", 0)) {
                                case 1:
                                    String picUri = pData.getString("str_DOC_1", null);
                                    display_img(ssstr_img_path, img1, "1");

                                    onCaptureImageResult(data, 1, ssstr_img_path, btn_image1);

                                    break;

                                case 2:

                                    String picUri2 = pData.getString("str_DOC_2", null);

                                    display_img(ssstr_img_path, img2, "2");
                                    onCaptureImageResult(data, 2, ssstr_img_path, btn_image2);

                                    break;

                                case 3:

                                    String picUri3 = pData.getString("str_DOC_3", null);

                                    display_img(ssstr_img_path, img3, "3");
                                    onCaptureImageResult(data, 3, ssstr_img_path, btn_image3);

                                    break;


                            }
                        }
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // Delete the cancelled image

                    try {
                        //deleteLastPic();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    SharedPreferences.Editor editor = pData.edit();
                    switch (pData.getInt("curr_DOC_no", 0)) {
                        case 1:
                            if (fileDelete(pData.getString("str_DOC_1", null))) {
                                editor.putString("str_DOC_1", "");
                            }

                            break;
                        case 2:
                            if (fileDelete(pData.getString("str_DOC_2", null))) {
                                editor.putString("str_DOC_2", "");
                            }

                            break;
                        case 3:
                            if (fileDelete(pData.getString("str_DOC_3", null))) {
                                editor.putString("str_DOC_3", "");
                            }
                            break;

                        default:
                            break;

                    }

                    editor.commit();

                    // user cancelled Image capture
                    Toast.makeText(getActivity(),
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(getActivity(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

    }


    boolean display_img(String pic_uri,ImageView thumb_nail,String doc_no){

        if(thumb_nail!=img_map) {
            str_img_path = decodeFile(pic_uri,doc_no);


            File img_file = new File(str_img_path);
            if (img_file.exists()) {
                my_bitmp = BitmapFactory.decodeFile(img_file.getAbsolutePath());

            }
        }
        thumb_nail.setImageBitmap(my_bitmp);
        return  true;
    }

    private String decodeFile(String path,String docno) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        try {
            System.out.println("::::::-3" + imgPath);
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(imgPath, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= 600 && unscaledBitmap.getHeight() <= 800)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }
            System.out.println("::::::-2");
            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/PD");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            System.out.println("::::::-1");

            String s = str_los + "_" + str_userid + "_"
                    + str_ts + "_" + "COLL" + "_" + docno + ".jpg";

            File f = new File(mFolder.getAbsolutePath(), s);

            System.out.println("::::::0");

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                System.out.println("::::::1");
                Point p = new Point();
                p.set(10, 20);
                //  scaledBitmap = waterMark(scaledBitmap, "Form No= " + form_no + " LOS ID= " + losid + " Username = " + userid, p, Color.BLACK, 90, 14, true);
                System.out.println("::::::2");
                p.set(10, 40);
                // scaledBitmap = waterMark(scaledBitmap, "Lat= " + map_lat + " Long=" + map_long + " Time=" + ts_date, p, Color.BLACK, 90, 14, true);
                fos = new FileOutputStream(f);
                System.out.println("::::::3");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("::::::4");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("::::::5");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            //decodeFile(path);
            //compressImage(path);
            System.out.println("::::::6");

        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                        System.out.println(path + " : Deleted");
                    } else {
                        System.out.println(path + " : CANT Dieleted");
                    }
                } else {
                    System.out.println(path + " : Do Not Exists");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }


    private void onCaptureImageResult(Intent data,  Integer img_no, String img_uri,Button dest_btn) {

      /*  boolean is_losid_empty= isEmptyString(str_losid_no);

        if(is_losid_empty==true){
            str_losid_no="null";
        }
        */

        SharedPreferences.Editor editor = pData.edit();
        switch (img_no) {
            case 1:

                String new_path = str_los + "_" + str_userid + "_"
                        + str_ts + "_" + "COLL" + "_" + "1";

                editor.putString("str_DOC_1", img_uri);
                editor.putString("str_New_DOC_1", new_path);
                editor.commit();

                MEDIA_IMAGE_COUNT_1 = 1;

                str_full_structure_1 = pData.getString("str_DOC_1", null);
                str_full_structure_new_path_1 = pData.getString("str_New_DOC_1", null);

                System.out.println("PHOTO 1::::" + str_full_structure_new_path_1);


                break;
            case 2:
                String new_path2 = str_los + "_" + str_userid + "_"
                        + str_ts + "_" + "COLL" + "_" + "2";

                editor.putString("str_DOC_2", img_uri);
                editor.putString("str_New_DOC_2", new_path2);
                editor.commit();
                MEDIA_IMAGE_COUNT_2 = 1;

                str_full_structure_2 = pData.getString("str_DOC_2", null);
                str_full_structure_new_path_2 = pData.getString("str_New_DOC_2", null);
                break;
            case 3:

                String new_path3 = str_los + "_" + str_userid + "_" + str_ts + "_"
                        + "_" + "COLL" + "_" + "3";

                editor.putString("str_DOC_3", img_uri);
                editor.putString("str_New_DOC_3", new_path3);
                editor.commit();

                MEDIA_IMAGE_COUNT_3 = 1;

                str_full_structure_3 = pData.getString("str_DOC_3", null);
                str_full_structure_new_path_3 = pData.getString("str_New_DOC_3", null);
                break;


            default:
                break;
        }
    }


    void getdata(){

        str_ref1=edt_ref1.getText().toString();
        str_ref2=edt_ref2.getText().toString();
        str_rate_quote1=edt_rate1.getText().toString();
        str_rate_quote2=edt_rate2.getText().toString();
        str_distance_km=edt_distance_km.getText().toString();

    }

    private File getOutputMediaFile2(String userid, String losid, String ts, String str_pic_name) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = losid + "_" + userid + "_" + ts + "_COLL_" + str_pic_name + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private  File getOutputMediaFile1235(){

        String curr_no=null;
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        /*
        File mediaStorageDir = new File(Environment.getExternallStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
                 */
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }

        Uri imgUri = null;
        try {

            curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


            file = new File(mediaStorageDir, str_los + "_" + str_userid + "_"
                    + str_ts + "_" + "COLL"+"_"+curr_no + ".jpeg");

            if (file.exists()) {
                file.delete();
            }
            imgUri = Uri.fromFile(file);
            this.imgPath = file.getAbsolutePath();
            System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
            SharedPreferences.Editor editor = pData.edit();

            switch (Integer.parseInt(curr_no)) {
                case 1:
                    editor.putString("str_DOC_1", this.imgPath);

                    break;
                case 2:
                    editor.putString("str_DOC_2", this.imgPath);
                    break;
                case 3:
                    editor.putString("str_DOC_3", this.imgPath);
                    break;

                case 4:
                    editor.putString("str_DOC_4", this.imgPath);

                    break;
                case 5:
                    editor.putString("str_DOC_5", this.imgPath);
                    break;
                case 6:
                    editor.putString("str_DOC_6", this.imgPath);
                    break;
                default:
                    break;

            }
            editor.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }


        new_file=new File(mediaStorageDir.getPath() + File.separator + str_los + "_" + str_userid + "_" + str_ts + "_COLL_" + curr_no + ".JPEG");
        return new_file;
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        url_file = Uri.fromFile(getOutputMediaFile1235());

        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove(str_uri);
        peditor.commit();
        str_uri=url_file.toString();
        peditor.putString("str_uri", str_uri);
        peditor.commit();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        startActivityForResult(intent, 100);
    }

    private String storeImage(Bitmap image, String userid, String losid, String ts, Integer str_pic_name) {
        String new_strMyImagePath = null;
        String pic_id=str_pic_name.toString();
        File pictureFile = getOutputMediaFile2(userid, losid, ts, pic_id);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

            return new_strMyImagePath;
        }

    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy:hhmmss");
        String format = s.format(new Date());
        return format;
    }


}
