package com.example.vijay.finone;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.MapsActivity;
import com.example.vijay.finone.libraries.UserFunctions;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import com.example.vijay.finone.libraries.MyAdapter;
import com.example.vijay.finone.libraries. UserFunctions;
import android.provider.MediaStore;
import java.io.File;
import com.example.vijay.finone.libraries.ScalingUtilities;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import com.example.vijay.finone.libraries.ConnectionDetector;



public class CustOffDetails extends Fragment implements AdapterView.OnItemSelectedListener{
    EditText edt_name, edt_mob, edt_pan,edt_mname,edt_designation;



    ArrayList<String> str_prod,str_prod1,str_prodid;
    String str_product;
    ArrayList<String> page_zero;
    TextUtils txt_utls;
    public static String fname,mname,lname, mobile,prod,pan_no;

    TextInputLayout inp_mob,inp_fname,inp_lname,inp_pan_,inp_prod;

    LinearLayout main_lay,otp_lay;
    String industry[]={"Employer Industry","Advertising Industry", "Agricultural Industry", "Aluminium Industry", "Automobile Industry", "Aviation Industry", "Banking and Finance", "Bio-technology Industry",
            "Biscuit Industry", "Cement Industry", "Chocolate industry","Coir Industry", "Construction Industry", "Copper Industry", "Cosmetic Industry", "Cottage Industry",
            "Cotton Industry", "Dairy Industry", "Diamond Industry", "Electronic Industry", "Fashion Industry", "Fertilizer Industry", "Film Industry", "Food Processing Industry",
            "Furniture Industry", "Garment Industry", "Granite Industry", "Health Care Industry", "Hotel Industry", "Insurance Industry", "IT Industry", "Jewellery Industry",
            "Jute Industry", "Leather Industry", "Mining Industry", "Music Industry", "Mutual Fund Industry", "Oil Industry", "Paint Industry",
            "Paper Industry", "Pearl Industry", "Pharmaceutical Industry", "Plastic Industry", "Poultry Industry", "Power Industry", "Printing Industry", "Railway Industry", "Real Estate Industry", "Retail industry",
            "Rubber Industry", "Shipping Industry", "Silk Industry", "Soap industry", "Solar Industry","Steel Industry", "Sugar Industry", "Tea Industry", "Telecom Industry",
            "Television Industry", "Textile Industry", "Tobacco Industry", "Tourism Industry", "Toy Industry", "Tractor Industry", "Turbine Industry", "Weaving Industry", "Zinc Industry",
    };

    String emp_cat[]={"Employer Category","Private","Government"};

    String off_setup[]={"Office Setup","Old","New"};

    String emp_num[]={"No of Employees","<20","20-50","50-100",">100"};

    String off_addr[]={"Office Address Validated Online","Yes","No"};

    String job_role[]={"Job Role","Lower Management","Middle Management","Higher Management","Top Management","Class 1","Class 2","Class 3","Class 4"};

    String dept[]={"Department","Accounts & Finance", "Administration", "Human Resource", "Production", "Quality Control", "Research & Development", "Marketing & Sales", "IT / Customer Support",
            "Logistics / Transport", "Legal / Advisory", "Mechanical / Hardware", "Operations / backend support", "Training / Teaching", "Inventory / Stock",
            "Others"};

    String qual[]={"Qualification","Under Graduate","Graduate","Post Graduate","Professional"};

    String transfer[]={"Select Job Transferrable","Yes","No"};

    String dependents[]={"Select No of Dependents","1","2","3","4",">4"};


    ProgressDialog mProgressDialog;
    JSONObject json;
    UserFunctions userFunction;
    String random_no,str_user_id;
    public  static String str_appid;



    SharedPreferences pref;
    Boolean is_self_emp;

    String str_ts,str_industry,str_emp_catg,str_off_setup,str_no_emp,str_off_address,str_designation,str_dept,str_job_role,str_qual,
            str_yrs_job,str_total_exp,str_gross_sal,str_transfer,str_dependent;

    String losid;

    ImageLoader imageLoader;

    ScrollableTabsActivity main_act;
    ImageView img_map;

    Spinner spn_industry,spn_emp_catg,spn_off_setup,spn_off_num,spn_off_check,spn_job_role,spn_dept,spn_qual,spn_jobtransfer,spn_dependents;
    ArrayAdapter adp_industry,adp_emp_cat,adp_off_setup,adp_off_num,adp_off_chk,adp_job_role,adp_dept,adp_qual;

    Button btn_next,btn_visting_card,btn_internal_setup,btn_ext_setup;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");

    private static final Pattern MOBILE_PATTERN = Pattern
            .compile("[0-9]{10}");


    ImageView img_vis_card,img_extr,img_intr;

    DBHelper dbHelp;

    public  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;

    EditText edt_yrs_job,edt_total_exp,edt_gross_sal;



    SharedPreferences pData;



    String str_losid;


    static String str_full_structure_1, str_full_structure_2, str_full_structure_3;

    ArrayAdapter adp_trans,adp_depend;

    ConnectionDetector cd;

    String losid_no,
     timestamp,
     user_id;


    public CustOffDetails() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        super.onCreateView(inflater,container,savedInstanceState);

        View view = inflater.inflate(R.layout.pd_len_cust_off_details, container, false);

        main_act=new ScrollableTabsActivity();

      //  main_act.str_losid;
        userFunction=new UserFunctions();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 -

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putBoolean("is_self_emp", false);

        peditor.putString("map_img_name", null);
        peditor.putString("is_locate_clicked", "");
        peditor.putInt("is_map_captured", 0);
        peditor.commit();

        dbHelp=new DBHelper(getContext());

        cd=new ConnectionDetector(getActivity());

        pData = getActivity().getSharedPreferences("pData", 0);

      str_ts = pData.getString("str_ts", null);

        System.out.println("ts:::::" + str_ts);

        str_appid=str_user_id+"_"+getDateTime();

         losid_no = pData.getString("str_los", null);

         user_id =pref.getString("user_id",null);


        // Inflate the layout for this fragment


      //  }


        //System.out.println("PRODUCT:::" + str_prod);
         btn_next = (Button) view.findViewById(R.id.btn_getotp);

       // spn_prod = (Spinner) view.findViewById(R.id.spn_prod);
        edt_yrs_job = (EditText) view.findViewById(R.id.edt_years);

        edt_total_exp = (EditText) view.findViewById(R.id.edt_tot_exp);

        edt_gross_sal = (EditText) view.findViewById(R.id.edt_gross_sal);

        edt_designation = (EditText) view.findViewById(R.id.edt_designation);

        main_lay = (LinearLayout) view.findViewById(R.id.lay_main);

        inp_mob=(TextInputLayout)view.findViewById(R.id.input_layout_phone);

        spn_industry=(Spinner)view.findViewById(R.id.spn_industry);

        spn_emp_catg=(Spinner)view.findViewById(R.id.spn_emp_cat);

        spn_off_setup=(Spinner)view.findViewById(R.id.spn_off_setup);

        spn_off_num=(Spinner)view.findViewById(R.id.spn_emp_no);
      //  spn_off_check=(Spinner)view.findViewById(R.id.spn_);

      //  spn_=(Spinner)view.findViewById(R.id.spn_emp_no);

        spn_job_role=(Spinner)view.findViewById(R.id.spn_job_role);

        spn_dept=(Spinner)view.findViewById(R.id.spn_dept);

        spn_qual=(Spinner)view.findViewById(R.id.spn_qual);

        spn_jobtransfer=(Spinner)view.findViewById(R.id.spn_jobtransfer);

        spn_dependents=(Spinner)view.findViewById(R.id.spn_dependents);

        adp_depend = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, dependents);

        adp_depend .setDropDownViewResource(R.layout.spinner_layout);
        spn_dependents.setAdapter(adp_depend);
        spn_dependents.setOnItemSelectedListener(this);

        adp_trans = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, transfer);

        adp_trans .setDropDownViewResource(R.layout.spinner_layout);
        spn_jobtransfer.setAdapter(adp_trans);
        spn_jobtransfer.setOnItemSelectedListener(this);

        adp_dept = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, dept);

        adp_dept .setDropDownViewResource(R.layout.spinner_layout);
        spn_dept.setAdapter(adp_dept);
        spn_dept.setOnItemSelectedListener(this);



        adp_industry = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, industry);

        adp_industry .setDropDownViewResource(R.layout.spinner_layout);
        spn_industry.setAdapter(adp_industry);
        spn_industry.setOnItemSelectedListener(this);

        adp_industry.notifyDataSetChanged();

        adp_emp_cat = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, emp_cat);

        adp_emp_cat .setDropDownViewResource(R.layout.spinner_layout);
        spn_emp_catg.setAdapter(adp_emp_cat);
        spn_emp_catg.setOnItemSelectedListener(this);

        adp_emp_cat.notifyDataSetChanged();

        adp_off_setup = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, off_setup);

        adp_off_setup .setDropDownViewResource(R.layout.spinner_layout);
        spn_off_setup.setAdapter(adp_off_setup);
        spn_off_setup.setOnItemSelectedListener(this);

        adp_off_setup.notifyDataSetChanged();

        adp_off_num = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit,emp_num );

        adp_off_num .setDropDownViewResource(R.layout.spinner_layout);
        spn_off_num.setAdapter(adp_off_num);
        spn_off_num.setOnItemSelectedListener(this);

        adp_off_num.notifyDataSetChanged();




        adp_job_role = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit,job_role );

        adp_job_role .setDropDownViewResource(R.layout.spinner_layout);
        spn_job_role.setAdapter(adp_job_role);
        spn_job_role.setOnItemSelectedListener(this);

        adp_job_role.notifyDataSetChanged();

        adp_qual = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit,qual );

        adp_qual .setDropDownViewResource(R.layout.spinner_layout);
        spn_qual.setAdapter(adp_qual);
        spn_qual.setOnItemSelectedListener(this);

        adp_qual.notifyDataSetChanged();


/*
        edt_name.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_mname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_lname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_pan.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
*/

        view.setBackgroundColor(Color.WHITE);

/*spn_prod.setSelection(0);
        str_pro*/

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean ins=true;

                getdata();

                try {

                    if(Integer.valueOf(edt_yrs_job.getText().toString())>50 && ins==true){
                        userFunction.cutomToast("Job stability cant be greater than 50",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(Integer.valueOf(edt_total_exp.getText().toString())>50 && ins==true){
                        userFunction.cutomToast("Total Experience cant be greater than 50",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    System.out.println("Lenght:::" + edt_total_exp.getText().toString().length());

                    if(edt_gross_sal.getText().toString().length()<5 && ins==true){
                        userFunction.cutomToast("Enter atleast 5 digit gross salary",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {


                    if((str_emp_catg.equals("") || str_emp_catg==null) && ins==true){
                        userFunction.cutomToast("Select employer catagory",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {


                    if((str_industry.equals("") || str_industry==null) && ins==true){
                        userFunction.cutomToast("Select employer industry",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    if((str_off_setup.equals("") || str_off_setup==null) && ins==true){
                        userFunction.cutomToast("Select Office Setup",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((str_designation.equals("")|| str_designation.equals("null"))  && ins==true){
                        userFunction.cutomToast("Enter Designation",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {
                    if((str_dept.equals("")|| str_dept.equals("null"))  && ins==true){
                        userFunction.cutomToast("Enter Department",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((str_job_role.equals("")|| str_job_role.equals("null"))  && ins==true){
                        userFunction.cutomToast("Select Job Role",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((str_yrs_job.equals("")|| str_yrs_job.equals("null"))  && ins==true){
                        userFunction.cutomToast("Enter Job stability",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((str_total_exp.equals("")|| str_total_exp.equals("null"))  && ins==true){
                        userFunction.cutomToast("Enter Total Work Experience",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((Integer.valueOf(str_yrs_job)>Integer.valueOf(str_total_exp))  && ins==true){
                        userFunction.cutomToast("Job Stability should not be greater than total Work Experience",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_qual.equals("")|| str_qual.equals("null"))  && ins==true){
                        userFunction.cutomToast("Enter Qualification",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((str_transfer.equals("")|| str_transfer==null)  && ins==true){
                        userFunction.cutomToast("Select Job Transferable",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {



                    if((str_dependent.equals("")|| str_dependent==null ) && ins==true){
                        userFunction.cutomToast("Select No of Dependents",getActivity());
                        ins=false;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


              //  if(ins==true) {

                    str_gross_sal=edt_gross_sal.getText().toString();

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("gross_sal", str_gross_sal);
                    peditor.commit();


                    Boolean update = dbHelp.update_cust_office_details(str_industry, str_emp_catg, str_off_setup,str_no_emp,str_off_address,str_designation,str_dept,str_job_role,str_yrs_job,str_total_exp,str_gross_sal,str_ts,str_transfer,str_dependent);
                    if (update == true) {

                        Fragment twofrag = new CustIncomeDetls();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, twofrag);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }
               // }
                }
        });

        return view;
    }

    private boolean Check_pattern(String string,int i) {

        Boolean status=false;
        switch (i) {
            case 1:
                status=  MOBILE_PATTERN.matcher(string).matches();
                break;
            case 2:
                status=   PAN_PATTERN.matcher(string).matches();
                break;
        }

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_emp_cat:
                if(position==0){
                    str_emp_catg="";
                }else{
                    str_emp_catg=emp_cat[position];
                }
                break;

            case R.id.spn_industry:
                if(position==0){

                    str_industry="";
                }else{
                    str_industry=industry[position];
                }
                break;

            case R.id.spn_off_setup:
                if(position==0){
                    str_off_setup="";
                }else{
                    str_off_setup=off_setup[position];
                }
                break;

            case R.id.spn_emp_no:
                if(position==0){

                    str_no_emp="";
                }else{
                    str_no_emp=emp_num[position];
                }
                break;


            case R.id.spn_dept:
                if(position==0){
                    str_dept="";
                }else{
                    str_dept=dept[position];
                }
                break;

            case R.id.spn_job_role:
                if(position==0){

                    str_job_role="";
                }else{
                    str_job_role=job_role[position];
                }
                break;

            case R.id.spn_qual:
                if(position==0){

                    str_qual="";
                }else{
                    str_qual=job_role[position];
                }
                break;

            case R.id.spn_jobtransfer:
                if(position==0){

                    str_transfer="";
                }else{
                    str_transfer=transfer[position];
                }
                break;

            case R.id.spn_dependents:
                if(position==0){

                    str_dependent="";
                }else{
                    str_dependent=dependents[position];
                }
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }




    void getdata(){

        str_designation=edt_designation.getText().toString();
        str_designation=edt_designation.getText().toString();

        System.out.println("designation::::" + str_designation);
        str_yrs_job=edt_yrs_job.getText().toString();
        str_total_exp=edt_total_exp.getText().toString();
        str_gross_sal=edt_gross_sal.getText().toString();

    }


}