package com.example.vijay.finone;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.MapsActivity;
import com.example.vijay.finone.libraries.ScalingUtilities;
import com.example.vijay.finone.libraries.UserFunctions;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import com.example.vijay.finone.libraries.MyAdapter;


public class CustOffDetailsSE extends Fragment implements AdapterView.OnItemSelectedListener{
    EditText edt_act_turnover,edt_fixed_cost;
    Spinner spn_prod;
    ArrayAdapter adapter_prod;
    ArrayList<String> str_prod,str_prod1,str_prodid;
    ArrayList<String> page_zero;
    TextUtils txt_utls;
    public static String fname,mname,lname, mobile,prod,pan_no,self_emp;
    int i;
    public static BigDecimal net_cons,turn_cons,act_turnover;
    TextInputLayout inp_mob,inp_fname,inp_lname,inp_pan_,inp_prod;

    LinearLayout main_lay,otp_lay;

    String losid_no,str_img_path,upper_limit,lower_limit,str_turnover,str_net_profit,gross_profit;

    DBHelper dbHelp;

    Button btn_stock,btn_setup_inside,btn_setup_outside;

    int map_captured=0;

   int curr_DOC_no=0;

    Bitmap my_bitmp;

    Spinner spn_industry,spn_buz_type,spn_setup,spn_turnover,spn_gross_pro,spn_net_pro,spn_act_turnover,spn_seasonal_buz,spn_seasonal_months;
    ArrayAdapter adp_industry,adp_buz_type,adp_setup,adp_turnover,adp_gross_pro,adp_net_pro,adp_season_buz,adp_season_months;
    ConnectionDetector cd;


   EditText edt_landmark,edt_area_name,edt_setup_size,edt_pincode,edt_industry_sal,edt_buz_type,edt_gross_pr_ofit,edt_services,edt_turnover,edtgross_profit,edt_net_profit;

    String str_landmark,str_area_name,str_picode,str_product,str_setup,str_industry_se,str_buz_typ,str_gross_profit,str_ts,str_act_turnover,str_fixed_cost,str_season_buz,str_season_months,
    str_gross_cons;

    String industry[]={"Industry-Segment",
            "Agriculture", "Automobiles", "Cement", "Textile", "Chemical", "Computer & IT", "Real Estate", "Contractor", "Packaging", "Electronics", "Electrical", "Entertainment & Leisure",
            "Food Processing & Packaging", "FMCG", "Industrial Equipment", "Pharma & healthcare", "Metals", "Gems & Jewellery", "FMCG", "Bank & Fin. Services", "Professional Services",
            "Transportation & Logistics", "Paper", "Plastic", "Petroleum", "Others - Industries not classified elsewhere(manufacturing)", "Others - Industries not classified elsewhere(trading)",
            "Others - Industries not classified elsewhere(services)", "Education"
    };

    String buz_type[]={
"Business Type",
            "Manufacturer",
            "Job Work",
            "Wholesaler",
            "Retailer",
            "Dealer",
            "Service Provider",
            "Trader"
    };

    String setup[]={"Setup Size",
            "50-100 sqft",
            "100-250 Sqft",
            "250-500 sqft",
            "500-1000 Sqft",
            "1000-1500 Sqft",
            "1500-3000 Sqft",
            "3000-5000 Sqft",
            "5000-10000 Sqft",
            ">10000 Sqft"
    };

    String turn_over[]={"Turnover Sales",
    "<10 L",
            "10-25 L",
            ">25 - 50L",
            ">50 - 100 L",
            ">100 - 200 L",
            ">200 - 400 L",
            ">400 - 800 L",
            ">800 - 1000 L",
            ">1000L"};

    String turn_over_lower[]={"Turnover Sales",
            "0",
            "1000000",
            "2500000",
            "5000000",
            "10000000",
            "20000000",
            "40000000",
            "80000000",
            "100000000"};

    String turn_upper[]={"Turnover Sales",
            "1000000",
            "2500000",
            "5000000",
            "10000000",
            "20000000",
            "40000000",
            "80000000",
            "100000000",
            ""};

    String turnover_consider[]={"","500000",
            "1000000",
            "2500000",
            "5000000",
            "10000000",
            "20000000",
            "40000000",
            "80000000",
            "100000000",
    };

    String profit_per[]={"Gross Profit",
            "<2%",
            "2-3%",
            "3-5%",
            "5-10%",
            "11-15%",
            "15-20%",
            ">20%"
          };

    String profit_per1[]={"Net Profit",
            "<2%",
            "2-3%",
            "3-5%",
            "5-10%",
            "11-15%",
            "15-20%",
            ">20%"
    };

    String net_profit_consider[]={"",
            "1.5",
            "2.5",
            "4",
            "7.5",
            "13",
            "17",
            "20"
    };

    String season_buz[]={"Seasonal Business",
         "Yes","No"
    };

    String season_months[]={"Seasonal Months",
            "1","2","3","4",">4"
    };

    ProgressDialog mProgressDialog;
    JSONObject json;
    UserFunctions userFunction;
    String random_no,str_user_id;
    public  static String str_appid;
    ScrollableTabsActivity main_act;
    SharedPreferences pref,pData;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");

    private static final Pattern MOBILE_PATTERN = Pattern
            .compile("[0-9]{10}");

    public static final int MEDIA_TYPE_IMAGE = 1;
    File file;
    int REQUEST_CAMERA = 0;
    private String imgPath;
    private static final String IMAGE_DIRECTORY_NAME = "/PD";
    Button btn_next;

    ImageView img_stock,img_inside,img_outside;

    String user_id;

    static  String str_full_structure_new_path_1  , str_full_structure_new_path_3, str_full_structure_new_path_2 ;
    static String str_full_structure_1, str_full_structure_2, str_full_structure_3 ;

    RelativeLayout season_month;

    Button btn_locate_me;

    int MEDIA_IMAGE_COUNT ;
    int MEDIA_IMAGE_COUNT_1 ;
    int MEDIA_IMAGE_COUNT_2 ;
    int MEDIA_IMAGE_COUNT_3;

    int MEDIA_MAP_COUNT;

    String str_coll_photo_count;
    String is_locate_clicked="";

    public CustOffDetailsSE() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        super.onCreateView(inflater,container,savedInstanceState);

        View view = inflater.inflate(R.layout.pd_len_cust_off_details_se, container, false);

        userFunction=new UserFunctions();

        dbHelp=new DBHelper(getActivity());

        cd=new ConnectionDetector(getActivity());

        page_zero=new ArrayList<>();
        str_prod=new ArrayList<>();
        str_prod1=new ArrayList<>();
        str_prodid=new ArrayList<>();

        main_act=new ScrollableTabsActivity();


        self_emp="true";

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 -

        pData = getActivity().getSharedPreferences("pData", 0); // 0 -

        str_ts = pData.getString("str_ts", null);

        System.out.println("TS!!!:::" + str_ts);

        user_id =pref.getString("user_id",null);

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putBoolean("is_self_emp", true);
        peditor.putString("map_img_name", null);
        peditor.putInt("is_map_captured", 0);

        peditor.putString("is_locate_clicked", "");

        peditor.commit();

        losid_no = pData.getString("str_los", null);

        str_appid=str_user_id+"_"+getDateTime();

        // Inflate the layout for this fragment


        //System.out.println("PRODUCT:::" + str_prod);`

        season_month=(RelativeLayout)view.findViewById(R.id.tableRow15);

        btn_locate_me=(Button)view.findViewById(R.id.btn_locate_me);

         btn_next = (Button) view.findViewById(R.id.btn_getotp);

        img_stock=(ImageView)view.findViewById(R.id.img_doc_1);

        img_inside=(ImageView)view.findViewById(R.id.img_doc_2);

        img_outside=(ImageView)view.findViewById(R.id.img_doc_3);

        // spn_prod = (Spinner) view.findViewById(R.id.spn_prod);
        edt_landmark = (EditText) view.findViewById(R.id.edt_landmark);
        edt_area_name = (EditText) view.findViewById(R.id.edt_area_name);
        edt_pincode = (EditText) view.findViewById(R.id.edt_pincode);
        edt_services = (EditText) view.findViewById(R.id.edt_prod);
        edt_act_turnover = (EditText) view.findViewById(R.id.edt_act_tuenover);
        edt_fixed_cost = (EditText) view.findViewById(R.id.edt_fixed_cost);

        main_lay = (LinearLayout) view.findViewById(R.id.lay_main);
        inp_mob=(TextInputLayout)view.findViewById(R.id.input_layout_phone);

        spn_industry=(Spinner)view.findViewById(R.id.spn_industry_se);

        spn_buz_type=(Spinner)view.findViewById(R.id.spn_buz_type);

        spn_setup=(Spinner)view.findViewById(R.id.spn_setup_size);

        spn_turnover=(Spinner)view.findViewById(R.id.spn_turnover);

        spn_gross_pro=(Spinner)view.findViewById(R.id.spn_gross_profit);
        spn_net_pro=(Spinner)view.findViewById(R.id.spn_net_profit);

        spn_seasonal_buz=(Spinner)view.findViewById(R.id.spn_season_buz);

        spn_seasonal_months=(Spinner)view.findViewById(R.id.spn_season_months);

        adp_season_buz = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit,season_buz );

        adp_season_buz .setDropDownViewResource(R.layout.spinner_layout);
        spn_seasonal_buz.setAdapter(adp_season_buz);
        spn_seasonal_buz.setOnItemSelectedListener(this);

        adp_season_months = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit,season_months );

        adp_season_months .setDropDownViewResource(R.layout.spinner_layout);
        spn_seasonal_months.setAdapter(adp_season_months);
        spn_seasonal_months.setOnItemSelectedListener(this);

        adp_industry = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, industry);

        adp_industry .setDropDownViewResource(R.layout.spinner_layout);
        spn_industry.setAdapter(adp_industry);
        spn_industry.setOnItemSelectedListener(this);

        adp_industry.notifyDataSetChanged();

        adp_buz_type = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, buz_type);

        adp_buz_type .setDropDownViewResource(R.layout.spinner_layout);
        spn_buz_type.setAdapter(adp_buz_type);
        spn_buz_type.setOnItemSelectedListener(this);
        adp_buz_type.notifyDataSetChanged();



        adp_setup = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, setup);

        adp_setup .setDropDownViewResource(R.layout.spinner_layout);
        spn_setup.setAdapter(adp_setup);
        spn_setup.setOnItemSelectedListener(this);
        adp_setup.notifyDataSetChanged();

        adp_turnover = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, turn_over);

        adp_turnover .setDropDownViewResource(R.layout.spinner_layout);
        spn_turnover.setAdapter(adp_turnover);
        spn_turnover.setOnItemSelectedListener(this);
        adp_turnover.notifyDataSetChanged();

        adp_gross_pro = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, profit_per);

        adp_gross_pro .setDropDownViewResource(R.layout.spinner_layout);
        spn_gross_pro.setAdapter(adp_gross_pro);
        spn_gross_pro.setOnItemSelectedListener(this);
        adp_gross_pro.notifyDataSetChanged();

        adp_net_pro = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, profit_per1);

        adp_net_pro .setDropDownViewResource(R.layout.spinner_layout);
        spn_net_pro.setAdapter(adp_net_pro);
        spn_net_pro.setOnItemSelectedListener(this);
        adp_net_pro.notifyDataSetChanged();


/*
        edt_name.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_mname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_lname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_pan.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
*/
        view.setBackgroundColor(Color.WHITE);

/*spn_prod.setSelection(0);
        str_pro*/

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean ins = true;
                getdata();

                try {

                    if (str_landmark.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter Landmark", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_area_name.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter Street Name", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((str_setup == "" || str_setup == null) && ins == true) {
                        userFunction.cutomToast("Please select Setup Size", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if (str_picode.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter Pincode", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if ((str_industry_se == "" || str_industry_se == null) && ins == true) {
                        userFunction.cutomToast("Please select Industry Segment", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((str_buz_typ == "" || str_buz_typ == null) && ins == true) {
                        userFunction.cutomToast("Please select Business Type", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if (str_product.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter Product/Services", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((lower_limit == "" || lower_limit == null) && ins == true) {
                        userFunction.cutomToast("Please select turnover", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((new BigDecimal(str_act_turnover).compareTo(new BigDecimal(lower_limit))==-1 ||(new BigDecimal(str_act_turnover).compareTo(new BigDecimal(upper_limit))==1))&& ins == true) {
                        userFunction.cutomToast("Actual turnover should be in rage of"+lower_limit+"LACS and " +upper_limit+" LACS", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_fixed_cost.equals("") && ins == true) {
                        userFunction.cutomToast("Enter Fixed Cost PA", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((str_gross_profit.equals("") || str_gross_profit==null)&& ins == true) {
                        userFunction.cutomToast("Please select Gross Profit", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if (net_cons == null && ins == true) {
                        userFunction.cutomToast("Please select Net profit", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_act_turnover.equals("") && ins == true) {
                        userFunction.cutomToast("Enter Actual Trnover", getActivity());
                        ins = false;

                    }else{
                        act_turnover= new BigDecimal(str_act_turnover);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



                try {

                    if ((str_season_buz=="" ||str_season_buz == null) && ins == true) {
                        userFunction.cutomToast("Please select Season Business", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if(str_season_buz.equals("Yes")) {

                        if ((str_season_months == "" || str_season_months == null) && ins == true) {
                            userFunction.cutomToast("Please select Season Months", getActivity());
                            ins = false;

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (net_cons.compareTo(new BigDecimal(str_gross_profit))==1 && ins == true) {
                        userFunction.cutomToast("Net Profit should not be greater than Gross Profit", getActivity());
                        ins = false;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String turnover, net_profit;

                turnover = String.valueOf(turn_cons);
                net_profit = String.valueOf(net_cons);

                if (ins == true) {

                    boolean flag = dbHelp.update_cust_office_details_se(str_landmark, str_area_name, str_setup, str_picode, str_industry_se, str_buz_typ,
                            str_industry_se, turnover, str_gross_profit, net_profit, str_ts,str_act_turnover,str_fixed_cost,str_season_buz,str_season_months,str_turnover,str_net_profit,gross_profit,str_product);

                    if (flag == true) {

                        Fragment twofrag = new CustIncomeDetls();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                       fragmentTransaction.replace(R.id.container, twofrag);
                       // fragmentTransaction.replace(R.id.zerofrag,twofrag);
                       fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                    }
                }
            }

        });


        return view;
    }

    private boolean Check_pattern(String string,int i) {

        Boolean status=false;
        switch (i) {
            case 1:
                status=  MOBILE_PATTERN.matcher(string).matches();
                break;
            case 2:
                status=   PAN_PATTERN.matcher(string).matches();
                break;

        }

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_setup_size:

                if(position!=0) {
                    str_setup =setup[position];

                    System.out.println("turnover:::" + turn_cons);
                }else{
                    str_setup="";
                }
                break;

            case R.id.spn_industry_se:

                if(position!=0) {
                    str_industry_se =industry[position];

                    System.out.println("turnover:::" + str_industry_se);
                    // str_pic_catpure_list =list1.get(position);
                    //String.valueOf(position) ;
                }else{
                    str_industry_se="";
                }
                break;

            case R.id.spn_buz_type:

                if(position!=0) {
                    str_buz_typ =buz_type[position];
                }else{
                    str_buz_typ="";
                }
                break;

            case R.id.spn_gross_profit:

                if(position!=0) {
                    str_gross_profit =net_profit_consider[position];
                    gross_profit=profit_per[position];

                }else {
                    str_gross_profit="";
                }
                break;

            case R.id.spn_turnover:

                if(position!=0) {
                    turn_cons =new BigDecimal(turnover_consider[position]);

                    str_turnover=turn_over[position];

                    lower_limit=turn_over_lower[position];
                    upper_limit=turn_upper[position];

                    System.out.println("turnover:::" + turn_cons);
                    // str_pic_catpure_list =list1.get(position);
                    //String.valueOf(position) ;
                }else{
                    lower_limit="";
                    upper_limit="";

                }
                break;

            case R.id.spn_net_profit:
                if(position!=0) {
                    net_cons = new BigDecimal(net_profit_consider[position]);
                    System.out.println("net pro:::" + net_cons);

                    str_net_profit=profit_per[position];
                }else{
                    net_cons=null;
                }
                // str_pic_catpure_list =list1.get(position);
                //String.valueOf(position) ;

                break;

            case R.id.spn_season_buz:

                if(position!=0) {
                    str_season_buz =season_buz[position];

                    if(season_buz[position].toString().equals("Yes")){

                        season_month.setVisibility(View.VISIBLE);
                    }else{
                        season_month.setVisibility(View.GONE);
                    }
                }else{
                    str_season_buz="";
                }
                break;

            case R.id.spn_season_months:

                if(position!=0) {
                    str_season_months =season_months[position];
                }else{
                    str_season_months="";
                }
                break;

            default:
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // Check for SD Card
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                        .show();
            } else {
                // Locate the image folder in your SD Card
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "PD");
                // Create a new folder if no folder named SDImageTutorial exist
                file.mkdirs();
            }


            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;

        String curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));

        try {

            if (type == MEDIA_TYPE_IMAGE) {

                file = new File(mediaStorageDir, losid_no + "_" + user_id + "_"
                        + str_ts + "_" +"CU"+ "_"+curr_no + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                System.out.println(":::::imgUri" + imgUri);
                System.out.println(":::::imgPath" + imgPath);
                this.imgPath = file.getAbsolutePath();

                SharedPreferences.Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;
                    default:
                        break;

                }
                editor.commit();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == REQUEST_CAMERA) {
                if (resultCode == Activity.RESULT_OK) {
                    SharedPreferences.Editor editor = pData.edit();

                    switch (pData.getInt("curr_DOC_no", 0)) {
                        case 1:
                            String picUri = pData.getString("str_DOC_1", null);
                            display_img(picUri,img_stock,"1");

                            onCaptureImageResult(data, 1, picUri, btn_stock);

                            break;

                        case 2:

                            String picUri2 = pData.getString("str_DOC_2", null);

                            display_img(picUri2,img_inside,"2");
                            onCaptureImageResult(data, 2, picUri2,btn_setup_inside);

                            break;

                        case 3:

                            String picUri3 = pData.getString("str_DOC_3", null);

                            display_img(picUri3,img_outside,"3");
                            onCaptureImageResult(data, 3, picUri3,btn_setup_outside);

                            break;

                    }
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Delete the cancelled image

                try {
                    //deleteLastPic();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

                SharedPreferences.Editor editor = pData.edit();
                switch (pData.getInt("curr_DOC_no", 0)) {
                    case 1:
                        if (fileDelete(pData.getString("str_DOC_1", null))) {
                            editor.putString("str_DOC_1", "");
                        }

                        break;
                    case 2:
                        if (fileDelete(pData.getString("str_DOC_2", null))) {
                            editor.putString("str_DOC_2", "");
                        }

                        break;
                    case 3:
                        if (fileDelete(pData.getString("str_DOC_3", null))) {
                            editor.putString("str_DOC_3", "");
                        }
                        break;

                    default:
                        break;

                }

                editor.commit();

                // user cancelled Image capture
                Toast.makeText(getActivity(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getActivity(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
           }
        } catch (Exception e) {
            e.printStackTrace();
        }
        }

    private void onCaptureImageResult( Intent data, Integer img_no, String img_uri, Button dest_btn) {



      /*  boolean is_losid_empty= isEmptyString(str_losid_no);

        if(is_losid_empty==true){
            str_losid_no="null";
        }
        */


        SharedPreferences.Editor editor = pData.edit();
        switch (img_no) {
            case 1:

                String new_path = losid_no + "_" + user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "1";

                editor.putString("str_DOC_1", img_uri);
                editor.putString("str_New_DOC_1", new_path);
                editor.commit();

                  MEDIA_IMAGE_COUNT_1 = 1;

                str_full_structure_1 = pData.getString("str_DOC_1", null);

                str_full_structure_new_path_1 = pData.getString("str_New_DOC_1", null);

                System.out.println("PHOTO 1::::" + str_full_structure_1);


                break;
            case 2:
                String new_path2 = losid_no + "_" + user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "2";

                editor.putString("str_DOC_2", img_uri);
                editor.putString("str_New_DOC_2", new_path2);
                editor.commit();
                   MEDIA_IMAGE_COUNT_2 = 1;

                str_full_structure_2 = pData.getString("str_DOC_2", null);
                str_full_structure_new_path_2 = pData.getString("str_New_DOC_2", null);
                break;
            case 3:

                String new_path3 = losid_no + "_" + user_id + "_" + str_ts + "_"
                        + "_" + "CU" + "_" + "3";

                editor.putString("str_DOC_3", img_uri);
                editor.putString("str_New_DOC_3", new_path3);
                editor.commit();

                 MEDIA_IMAGE_COUNT_3 = 1;

                str_full_structure_3 = pData.getString("str_DOC_3", null);
                str_full_structure_new_path_3 = pData.getString("str_New_DOC_3", null);
                break;

            default:
                break;
        }

    }


    private String decodeFile(String path, String doc_no) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        try {
            System.out.println("::::::-3" + imgPath);
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(imgPath, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= 600 && unscaledBitmap.getHeight() <= 800)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }
            System.out.println("::::::-2");
            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/PD");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            System.out.println("::::::-1");

            String s = losid_no + "_" + user_id + "_"
                    + str_ts + "_" + "CU" + "_" + doc_no + ".jpg";

            System.out.println("real path:::"+s);

            File f = new File(mFolder.getAbsolutePath(), s);

            System.out.println("::::::0");

            strMyImagePath = f.getAbsolutePath();



            FileOutputStream fos = null;
            try {
                System.out.println("::::::1");
                Point p = new Point();
                p.set(10, 20);
                //  scaledBitmap = waterMark(scaledBitmap, "Form No= " + form_no + " LOS ID= " + losid + " Username = " + userid, p, Color.BLACK, 90, 14, true);
                System.out.println("::::::2");
                p.set(10, 40);
                // scaledBitmap = waterMark(scaledBitmap, "Lat= " + map_lat + " Long=" + map_long + " Time=" + ts_date, p, Color.BLACK, 90, 14, true);
                fos = new FileOutputStream(f);
                System.out.println("::::::3");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("::::::4");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("::::::5");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            //decodeFile(path);
            //compressImage(path);
            System.out.println("::::::6");

        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

    void getdata(){

        str_landmark=edt_landmark.getText().toString();
        str_area_name=edt_area_name.getText().toString();
        str_picode=edt_pincode.getText().toString();
        str_product=edt_services.getText().toString();
        str_act_turnover=edt_act_turnover.getText().toString();

        str_fixed_cost=edt_fixed_cost.getText().toString();

    }

    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                        System.out.println(path + " : Deleted");
                    } else {
                        System.out.println(path + " : CANT Dieleted");
                    }
                } else {
                    System.out.println(path + " : Do Not Exists");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }


    boolean display_img(String pic_uri,ImageView thumb_nail,String doc_no){

        str_img_path = decodeFile(pic_uri,doc_no);

        System.out.println("DISPLAY PATH:::" + str_img_path);

        File img_file=new File(str_img_path);
        if(img_file.exists()){
            my_bitmp  = BitmapFactory.decodeFile(img_file.getAbsolutePath());

        }
        thumb_nail.setImageBitmap(my_bitmp);
        return  true;
    }

}