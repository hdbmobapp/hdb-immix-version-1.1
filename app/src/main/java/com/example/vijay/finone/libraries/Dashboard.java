package com.example.vijay.finone.libraries;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hdb.R;
import com.example.vijay.finone.FIrstScreen;
import com.example.vijay.finone.HdbHome;
import com.example.vijay.finone.ImagesList;


import org.json.JSONObject;

import java.io.File;

public class Dashboard extends AppCompatActivity {

    TextView txt_user_id, txt_user_name, txt_branch, txt_document_progress,
            txt_upload_progress;
    TextView text_error_msg;

    Button btn_home;

    String str_user_id, str_user_name, str_branch;
    SharedPreferences pref;

    int int_cust_pending_count = 0;
    int int_asset_pending_count = 0;
    int int_app_form_pending_count = 0;
    int int_new_case_pending_count = 0;
    int PendingPhotoCoun = 0;


    SharedPreferences pData;

    Button btn_dashboard, btn_data_entry;
    ImageButton btn_logout;
    UserFunctions userFunction;
    DBHelper rdbl;
    JSONObject jsonobject = null;

    Handler handler;
    private ConnectionDetector cd;
    ProgressDialog mProgressDialog;

    Button btn_asset_upload;
    Button btn_pd_upload;
    Button btn_appform_upload , btn_photo_upload ,btn_pic_list;

    TextView textView9;

    Integer  total_pic_count;

    String str_ts=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_dshboard);
        cd = new ConnectionDetector(getApplicationContext());

        userFunction = new UserFunctions();
        rdbl = new DBHelper(Dashboard.this);


        final Cursor cursor = rdbl.getImages();
        total_pic_count = cursor.getCount();

      //  handler.postDelayed(timedTask, 10000);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = getApplicationContext().getSharedPreferences("pData", 0);


		/*
         * if (userFunction.isUserLoggedIn(getApplicationContext())) {
		 * startService(new Intent(this, GPSLocationService.class));
		 * startService(new Intent(this, LoanService.class)); SetLayout(); }
		 * else { Intent loginActivity = new Intent(Dashboard.this,
		 * LoginActivity.class); startActivity(loginActivity); finish(); }
		 */

        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);

        str_branch = pref.getString("user_brnch", null);

        textView9=(TextView)findViewById(R.id.txt_document_progress);

        SetLayout();

        this.getSupportActionBar().setDisplayShowHomeEnabled(false);
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);



       //  btn_home = (Button) home_view.findViewById(R.id.txt_home);

        btn_pic_list=(Button) findViewById(R.id.btn_pic_list);

        if(total_pic_count>0){
            btn_pic_list.setVisibility(View.VISIBLE);
            textView9.setVisibility(View.VISIBLE);
        }else{
            btn_pic_list.setVisibility(View.GONE);
            textView9.setVisibility(View.GONE);
        }

        home_title.setText("Upload In Progress");

        this.getSupportActionBar().setCustomView(home_view);
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);

/*
        btn_home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });*/


        btn_pd_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet() == true) {

                    rdbl.get_Customers_request(str_ts, "OFF");

                } else {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), getApplicationContext());

                    Intent home_activity = new Intent(getApplicationContext(),
                            Dashboard.class);
                    startActivity(home_activity);
                }

            }
        });

        btn_asset_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet() == true) {

                    rdbl.get_Collateral_request(str_ts, "OFF");

                } else {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), getApplicationContext());

                    Intent home_activity = new Intent(getApplicationContext(),
                            Dashboard.class);
                    startActivity(home_activity);
                }

            }
        });

/*        btn_photo_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {


                        new UploadPhoto().execute();

                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });*/

        btn_pic_list.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(getApplicationContext(), ImagesList.class);
                    startActivity(intent);
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });

            }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                com.example.vijay.finone.HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    protected void SetLayout() {
        text_error_msg = (TextView) findViewById(R.id.text_error_msg);
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);

        TextView txt_pd_cnt = (TextView) findViewById(R.id.txt_pd_info_upload);
        TextView txt_asset_cnt = (TextView) findViewById(R.id.txt_asset_info_upload);

        btn_asset_upload=(Button)findViewById(R.id.btn_asset_upload);
        btn_pd_upload=(Button)findViewById(R.id.btn_pd_upload);

        int_cust_pending_count = Integer.parseInt(rdbl.getCustomerPendingCount());

        PendingPhotoCoun=Integer.parseInt(rdbl.getPendingPhotoCount());

      int_asset_pending_count=Integer.parseInt(rdbl.getCollPendingCount());

        textView9.setText(String.valueOf(PendingPhotoCoun));


        txt_asset_cnt.setText(String.valueOf(int_asset_pending_count));

        txt_pd_cnt.setText(String.valueOf(int_cust_pending_count));


        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);

        if(int_cust_pending_count>0){
            btn_pd_upload.setBackgroundResource(R.drawable.btn_red);
        }
        // ---set Custom Font to textview

        if(int_asset_pending_count>0){
            btn_asset_upload.setBackgroundResource(R.drawable.btn_red);
        }
/*
        if(PendingPhotoCoun>0){
            btn_photo_upload.setBackgroundResource(R.drawable.btn_red);
        }*/
    }


    @SuppressWarnings("unused")
    private void tToast(String s) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }



    public void cleanUpCancel() {
        fileDelete(pData.getString("str_DOC_1", null));
        fileDelete(pData.getString("str_DOC_2", null));
        fileDelete(pData.getString("str_DOC_3", null));
        fileDelete(pData.getString("str_DOC_4", null));
        fileDelete(pData.getString("str_DOC_5", null));
        fileDelete(pData.getString("str_DOC_6", null));
        fileDelete(pData.getString("str_DOC_7", null));
        fileDelete(pData.getString("str_DOC_8", null));
        fileDelete(pData.getString("str_DOC_9", null));
        fileDelete(pData.getString("str_DOC_10", null));
    }

    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                    }
                } else {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }

    @Override
    public void onBackPressed() {
        // your code.
        // to stop anonymous runnable use handler.removeCallbacksAndMessages(null);

        Intent intent=new Intent(getApplicationContext(),HdbHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }


    public class UploadPhoto extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Dashboard.this);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                if (Integer.parseInt(rdbl.getPendingPhotoCount()) > 0) {


                            if (Integer.parseInt(rdbl.getCustomerPendingCount()) == 0) {
                                if (Integer.parseInt(rdbl.getCollPendingCount()) == 0) {

                                    // Only Starts if all the Requests are updated.
                                    String img_path = rdbl.getphoto();
                                }
                            }

                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (Integer.parseInt(rdbl.getPendingPhotoCount()) > 0) {


                        if (Integer.parseInt(rdbl.getCustomerPendingCount()) == 0) {

                        }else{
                            userFunction.cutomToast("first upload pending Customer PD",Dashboard.this);
                        }
            }
            Intent messagingActivity = new Intent(Dashboard.this,
                    Dashboard.class);
            startActivity(messagingActivity);

            mProgressDialog.dismiss();
        }

    }


}
