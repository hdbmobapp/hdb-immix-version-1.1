package com.example.vijay.finone;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.ConnectionDetector;

import com.example.vijay.finone.libraries.UserFunctions;

/**
 * Created by Vijay on 5/3/2016.
 */


public class FIrstScreen extends AppCompatActivity {

    UserFunctions userFunction;
    ArrayList<String> cons_desc,cons_id,prod_id,product,qual_code,qual_dsec,resi_val,resi_desc,state_id,state_desc,catg_desc;
    String email,password;

    ProgressDialog mProgressDialog;
    JSONObject json;

    ConnectionDetector cd;

    Button btn_dash, btn_new,btn_summry;
SharedPreferences pref,pref1;

    UserFunctions userfunction;


    JSONArray json_arr;

    public static final String KEY_STATUS = "status";


    Toolbar act_bar;

    String str_masters;

    int is_login;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_first_screen);

        SetLayout();

        userFunction=new UserFunctions();

        cd=new ConnectionDetector(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -


    }

  public   void SetLayout() {


/*
        act_bar=(Toolbar)findViewById(R.id.act_bar);
       this. setSupportActionBar(act_bar);

*/

      //    this.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      //    this.getSupportActionBar().setDisplayShowTitleEnabled(false);


      //   getSupportActionBar().setCustomView(cust_view);


      //  ActionBar logout = getActionBar();
      this.getSupportActionBar().setDisplayShowHomeEnabled(false);

      this.getSupportActionBar().setDisplayShowTitleEnabled(false);
      LayoutInflater logout_inflater = LayoutInflater.from(this);

      View logout_view = logout_inflater.inflate(R.layout.pd_len_homelayout, null);
      ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
      this.getSupportActionBar().setCustomView(logout_view);

      this.getSupportActionBar().setDisplayShowCustomEnabled(true);


      TextView home_title = (TextView) logout_view.findViewById(R.id.txt_title);
      home_title.setText("VISIT DETAILS");

      Button home_button = (Button) logout_view.findViewById(R.id.txt_home);
      home_button.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              Intent messagingActivity = new Intent(FIrstScreen.this, HdbHome.class);
              startActivity(messagingActivity);
              finish();
          }

      });


      btn_new = (Button) findViewById(R.id.btn_add);
      btn_summry = (Button) findViewById(R.id.btn_summ);

      btn_new.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              Intent j = new Intent(getApplicationContext(),
                      ScrollableTabsActivity.class);

              startActivity(j);
              finish();

          }

      });

      btn_summry.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent i = new Intent(getApplicationContext(),
                      CollateralScreen.class);

              startActivity(i);
              finish();

          }
      });
/*
       btn_dash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent dash_activity = new Intent(getApplicationContext(),
                        DashBoard.class);


                startActivity(dash_activity);

            }

        });
*/


  }


}