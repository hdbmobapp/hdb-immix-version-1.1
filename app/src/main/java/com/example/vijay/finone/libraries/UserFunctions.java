package com.example.vijay.finone.libraries;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;;

import org.json.JSONObject;

import com.example.vijay.finone.libraries.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

@SuppressLint("NewApi")
public class UserFunctions extends Activity {

	private JSONParser jsonParser;
    SharedPreferences pref;

	private static String URL = "https://hdbapp.hdbfs.com/HDB_PD_LENDING_v2.9.1/index.php";

	private static String URL_POST = "https://hdbapp.hdbfs.com/FINONE_CONN_test/index_post.php";

//	private static String URL = "http://220.226.218.9/HDB_COLL_V2.3/index.php";
//	private static String IndexURL = "http://220.226.218.9/HDB_COLL_V2.3/index_new.php";
//	private static String IndexURLGET = "http://220.226.218.9/HDB_COLL_V2.3/index_get.php";


	Context context;

	SharedPreferences pData;
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	  //  setContentView(R.layout.setting);
	  }
	// constructor
	public UserFunctions(){
		jsonParser = new JSONParser();

	}

	public JSONObject loginUser(String username, String password,String imei_no,String latitude,String longitude,String device_name,String device_man,String android_id){
		// Building Parameters
		JSONObject json = null ;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "search");
			map.put("target", "login");
			map.put("latitude", latitude);
			map.put("longitude", longitude);

			map.put("username", username);
			map.put("password", password);
			map.put("imei", imei_no);
			map.put("device_name", device_name);
			map.put("device_man", device_man);
            map.put("app_version", "HDB-PD-Lending v2.9");
			map.put("device_id", android_id);


			json = jsonParser.makeHttpRequest_new(URL,"POST", map,context);
			Log.e("JSON", json.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject insert_Loan(String losid,String user_id,String ts){
		// Building Parameters
		JSONObject json = null ;

		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "insert");
			map.put("target", "loan");
			map.put("losid", losid);
            map.put("user_id", user_id);
			map.put("ts", ts);
            json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);
		                      } catch (Exception e) {
			e.printStackTrace();
		}

		// Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject get_Loan(String losid,String ts){
		// Building Parameters
		JSONObject json = null ;

		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "read");
			map.put("target", "loan");
			map.put("losid", losid);
			map.put("ts", ts);

			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Log.e("JSON", json.toString();
		return json;
	}

	public void cutomToast(String message, Context contxt) {
		// Inflate the Layout
		ViewGroup parent = null;
		LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.custom_toast,
				parent, false);
		TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
		mesg_name.setText(message);
		// Create Custom Toast
		Toast toast = new Toast(contxt);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}

	public JSONObject InsertData(
								 String str_Losid,
								 String str_prod,
								 String str_const,
								 String str_branch,
								 String str_cust_name,
								 String str_cib_score,
								 String str_pd_place,
								 String str_person_met,
								 String str_firm_name,
								 String emp_catg,
								 String str_industry,
								 String off_setup,
								 String emp_count,
								 String off_putress,
								 String designation,
								 String dept,
								 String job_role,
								 String job_stability,
								 String total_exp,
								 String net_sal,
								 String qual,
								 String str_landmark,
								 String area_name,
								 String setup_size,
								 String pincode,
								 String industry_sal,
								 String buz_type,
								 String services,
								 String turnover,
								 String gross_profit,
								 String net_profit,
								 String emi_month,
								 String cc_os,
								 String buz_income,
								 String income_rent,
								 String income_agri,
								 String total_income,
								 String surplus,
								 String per_income,
								 String per_hrp_profile,
								 String loan_usage,
								 String pd_coll,
								 String coll,
								 String pd_recomm,
								 String pd_recomm_amt,
								 String remarks,
								 String ts,
								 String lat,
								 String longs,
								 String entry_type,
								 String pic_count,
								 String userid,
								 String trans,
								 String depend,
								 String act_turnover,
								 String fixed_cost,
								 String season_buz,
								 String season_months,
								 String distance_type,String distance_km,String str_turnover,String str_net_profit,String str_gross_profit,
								 String map_address
	) {
		// Building Parameters
		JSONObject json = null;

		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "insert_cust_pd");
			map.put("target", "loan");
			//map.put("Lat", latitude);
			//map.put("Long", longitude);
			map.put("str_losid", str_Losid);
			map.put("str_prod", str_prod);
			map.put("str_const", str_const);
			map.put("str_branch", str_branch);
			//   map.put("Comm_Dominated", str_Community);
			map.put("str_cust_name", str_cust_name);
			map.put("str_cib_score", str_cib_score);
			map.put("str_pd_place", str_pd_place);
			map.put("str_person_met", str_person_met);
			map.put("str_firm_name", str_firm_name);
			map.put("emp_catg", emp_catg);
			map.put("str_industry", str_industry);
			map.put("off_setup", off_setup);
			map.put("emp_count", emp_count);
			map.put("off_address", off_putress);
			map.put("designation", designation);
			map.put("dept", dept);
			map.put("job_role", job_role);
			map.put("job_stability", job_stability);
			map.put("total_exp", total_exp);
			map.put("net_sal", net_sal);
			map.put("qual", qual);
			map.put("str_landmark", str_landmark);
			map.put("area_name", area_name);
			map.put("setup_size", setup_size);
			map.put("pincode", pincode);
			map.put("buz_type", buz_type);
			map.put("industry_sal", industry_sal);
			map.put("services", services);
			map.put("turnover", turnover);
			map.put("gross_profit", gross_profit);
			map.put("net_profit", net_profit);
			map.put("emi_month", emi_month);
			map.put("cc_os", cc_os);
			map.put("buz_income", buz_income);
			map.put("income_rent", income_rent);
			map.put("income_agri", income_agri);
			map.put("total_income", total_income);
			map.put("surplus", surplus);
			map.put("per_income", per_income);
			map.put("per_hrp_profile", per_hrp_profile);

			map.put("loan_usage", loan_usage);
			map.put("pd_coll", pd_coll);
			map.put("coll", coll);
			map.put("pd_recomm", pd_recomm);
			map.put("pd_recomm_amt", pd_recomm_amt);
			map.put("remarks", remarks);

			map.put("ts", ts);
			map.put("lat", lat);
			map.put("longs", longs);
			map.put("entry_type", entry_type);
			map.put("pic_count", pic_count);
			map.put("userid", userid);
			map.put("trans", trans);
			map.put("depend", depend);
			map.put("act_turrnover", act_turnover);
			map.put("fixed_cost", fixed_cost);
			map.put("season_buz", season_buz);
			map.put("season_months", season_months);
			map.put("distance_type", distance_type);
			map.put("distance_km", distance_km);

			map.put("str_turnover", str_turnover);
			map.put("str_net", str_net_profit);
			map.put("str_gross", str_gross_profit);
			map.put("map_address_cust", map_address);

			System.out.println("REQ map " + map.toString());
			Log.e("REQ map", map.toString());

			json = jsonParser.makeHttpRequest_new(URL,"POST",map,UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject InsertData_Coll(
			String coll_los,
			String coll_product,
			String coll_cust_name,
			String coll_person_met,
			String coll_relation,
			String coll_owner_name,
			String coll_collateral,
			String prop_type,
			String prop_status,
			String bank_notice,
			String redevlope,
			String soc_off,
			String tenant_vintage,
			String prop_access,
			String prop_area_match,
			String prop_area,
			String marketability,
			String ref1,
			String rate1,
			String ref2,
			String rate2,
			String vehicle_make,
			String varient,
			String model,
			String serial_no,
			String kilo_run,String ts,String lat,String longs,String userid,String pic_count,String entry_type,
			String distance_type, String distance_km,String coll_userid,String map_address

	) {
		// Building Parameters
		JSONObject json = null;

		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "insert_coll_pd");
			map.put("target", "loan");
			//map.put("Lat", latitude);
			//map.put("Long", longitude);
			map.put("coll_los", coll_los);
			map.put("coll_product", coll_product);
			map.put("coll_cust_name", coll_cust_name);
			map.put("coll_person_met", coll_person_met);
			//   map.put("Comm_Dominated", str_Community);
			map.put("coll_relation", coll_relation);
			map.put("coll_owner_name", coll_owner_name);
			map.put("coll_collateral", coll_collateral);
			map.put("prop_type", prop_type);
			map.put("prop_status", prop_status);
			map.put("bank_notice", bank_notice);
			map.put("redevlope", redevlope);
			map.put("soc_off", soc_off);
			map.put("tenant_vintage", tenant_vintage);

			map.put("prop_access", prop_access);
			map.put("prop_area_match", prop_area_match);
			map.put("prop_area", prop_area);
			map.put("marketability", marketability);
			map.put("ref1", ref1);
			map.put("rate1", rate1);
			map.put("ref2", ref2);
			map.put("rate2", rate2);
			map.put("vehicle_make", vehicle_make);
			map.put("varient", varient);
			map.put("serial_no", serial_no);
			map.put("model", model);
			map.put("coll_owner_name", coll_owner_name);
			map.put("kilo_run", kilo_run);

			map.put("ts", ts);
			map.put("lat", lat);
			map.put("longs", longs);
			map.put("entry_type", entry_type);
			map.put("pic_count", pic_count);
			map.put("userid", userid);
			map.put("distance_type", distance_type);
			map.put("distance_km", distance_km);
			map.put("coll_userid", coll_userid);

			map.put("map_address_coll", map_address);

			System.out.println("REQ map " + map.toString());
			Log.e("REQ map", map.toString());
			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Log.e("JSON", json.toString();
		return json;
	}


	public JSONObject getCaseList(String user_id, String losid_no, String pg_no) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "search");
			map.put("target", "loan");
			map.put("userid", user_id);
			map.put("pg", pg_no);

			map.put("losid_form_no", losid_no);
			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}

	public void noInternetConnection(String message, Context contxt) {
		Toast toast = null;
		if (toast == null
				|| toast.getView().getWindowVisibility() != View.VISIBLE) {
			ViewGroup parent = null;
			LayoutInflater inflater = (LayoutInflater) contxt
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.toast_no_internet, parent,
					false);
			TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
			mesg_name.setText(message);
			// Create Custom Toast
			toast = new Toast(contxt);
			toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.setView(layout);
			toast.show();
		}
	}


	public boolean logoutUserclear(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		db.resetTables();

		return true;
	}

	public boolean off_logoutUserclear(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		db.off_resetTables();

		return true;
	}



	public boolean off_isUserLoggedIn(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		if(!db.chkDBExist){return false;}
		int count = db.off_getRowCount();
		if(count > 0){
			// user logged in
			return true;
		}
		return false;
	}

	public boolean isUserLoggedIn(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		if(!db.chkDBExist){return false;}
		int count = db.getRowCount();
		if(count > 0){
			// user logged in
			return true;
		}
		return false;
	}


	public boolean logoutUser(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		db.resetTables();
		//this.cutomToast("Hope you had a wonderful day! Do check back soon.",context);

		return true;
	}

	public JSONObject getMessageBox(String user_id, String page_no) {

		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("userid", user_id);
			map.put("target", "messages");
			map.put("action", "search");
			map.put("messages_of", "PD");

			map.put("pg", page_no);

			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject update_message(String str_msg_id, String str_user_id) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("target", "messages");
			map.put("action", "update");
			map.put("userid", str_user_id);
			map.put("msg_id", str_msg_id);
			map.put("messages_of", "PD");


			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}


	public JSONObject getCU_PDCaseList(String user_id, String losid_no,String pg_no) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();

			map.put("action", "get_pd");
			map.put("target", "loan");
			map.put("userid", user_id);
			map.put("losid", losid_no);

			map.put("pg", pg_no);

			map.put("losid_form_no", losid_no);
			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}


	public JSONObject getCO_PDCaseList(String user_id, String losid_no,String pg_no) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "get_coll");
			map.put("target", "loan");
			map.put("userid", user_id);
			map.put("losid", losid_no);

			map.put("pg", pg_no);

			map.put("losid_form_no", losid_no);
			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject get_masters(String id) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "getmasters");
			map.put("target", "loan");
			map.put("id", id);

			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}

	public JSONObject check_masters(String flag_cons,String id_cons,String flag_pd,String id_pd) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("action", "verify_masters");
			map.put("target", "loan");
			map.put("flag_cons", flag_cons);
			map.put("id_cons", id_cons);
			map.put("flag_pd", flag_pd);
			map.put("id_pd", id_pd);

			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);
			System.out.println("JSONOBJ::::"+json);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}


	public JSONObject update_newLos(String str_fin_losid,String str_pg_from,String str_sr_no,String losid,String userid) {
		JSONObject json = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("target", "loan");
			map.put("action", "update_los");
			map.put("sr_no", str_sr_no);
			map.put("str_fin_los", str_fin_losid);
			map.put("str_pg_from", str_pg_from);
			map.put("str_losid", losid);
			map.put("userid", userid);
			json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);

		} catch (Exception e) {
			e.printStackTrace();
		}
		//Log.e("JSON", json.toString();
		return json;
	}

}

