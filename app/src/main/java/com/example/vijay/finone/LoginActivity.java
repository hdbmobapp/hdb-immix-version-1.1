package com.example.vijay.finone;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.vijay.finone.GPSTracker_New;
import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.DatabaseHandler;
import com.example.vijay.finone.libraries.UserFunctions;
import com.google.android.gcm.GCMRegistrar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import com.example.vijay.finone.GPSTracker_New;

public class LoginActivity extends Activity {

    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;
    GPSTracker_New appLocationService;

    String flag_cons,id_cons,flag_pd,id_pd;

    Integer off_rowcount;


    public  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=0;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunctions userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    private static Typeface font;
    //String user_name = null;
    String gcm = null;
    @SuppressWarnings("unused")
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{2,14}");
    Context context;
    ProgressBar pb_progress;

    String res,resp_success;

    public  static String KEY_STATUS="status";

    public  static String KEY_SUCCESS="success";

    public  static String KEY_STATUS_VM="status";

    public  static String KEY_SUCCESS_VM="success";
    public  static String KEY_STATUS_GM="status";

    public  static String KEY_SUCCESS_GM="success";

    JSONObject jsonobject;
    JSONArray jsonarray,jsonarray1,jsonarray2,jsonarray3;

    ProgressDialog mProgressDialog;

    ArrayList<String> al_cons,al_cons_val,al_cons_flag,al_cons_id,
    al_product,al_prod_val,al_prod_flag,al_prod_id;

    String str_dispo_id,str_dispo,str_flag;

    ArrayList<String> check_master;

    ArrayList<String> master_flag;

    DatabaseHandler db;

    String lattitude = null, longitude = null;
    String map_address=null;


    String reg_id;

    SharedPreferences pref;
    EditText edt_new_version_link;
    String new_version;
    TextView txt_new_version;
    String new_version_name;

    DBHelper rDB;

    HashMap<String,String> user = new HashMap<String,String>();

    private String android_id;
    String check_master_flag;

    double dbl_latitude = 0;
    double dbl_longitude = 0;

    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,

    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.login);
       // font = Typeface.createFromAsset(getBaseContext().getAssets(),
         //       "fonts/Amaranth-Regular.otf");
        cd = new ConnectionDetector(getApplicationContext());

        userFunction = new UserFunctions();

        System.out.println("LOGIN REDIRECT::::");

        android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        rDB=new DBHelper(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        // for
        appLocationService = new GPSTracker_New(LoginActivity.this);

        checkPermissions();

        db=new DatabaseHandler(getApplicationContext());
/*
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        }else {
            android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

*/

        //cons code
        al_cons=new ArrayList<String>();
        al_cons_val=new ArrayList<String>();
        al_cons_flag=new ArrayList<String>();
        al_cons_id=new ArrayList<String>();

        check_master=new ArrayList<String>();
        master_flag=new ArrayList<String>();


        //pay mode



        //product
        al_product=new ArrayList<String>();
        al_prod_val=new ArrayList<String>();
        al_prod_flag=new ArrayList<String>();
        al_prod_id=new ArrayList<String>();

        SetLayout();

        check_master=rDB.get_masters();


        //System.out.println("MASTERS ARRAY:::1 "+check_master.get(0)+"MASTERS ARRAY:::1 "+check_master.get(1));

    }

    protected void SetLayout() {
        // Check if Internet present
        setContentView(R.layout.pd_len_sl_login);
        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        edt_new_version_link = (EditText) findViewById(R.id.edt_new_version_link);
        txt_new_version = (TextView) findViewById(R.id.txt_new_version);
        // ---set Custom Font to textview
      //  inputEmail.setTypeface(font);

        //inputPassword.setTypeface(font);

       // btnLogin.setTypeface(font);
       // loginErrorMsg.setTypeface(font);

        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);




        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
              //  GCMRegistrar.register(LoginActivity.this,
               //         GCMIntentService.SENDER_ID);
                reg_id = GCMRegistrar
                        .getRegistrationId(LoginActivity.this);

                System.out.println("reg_id:::::"+reg_id);

                email = inputEmail.getText().toString();
                password = inputPassword.getText().toString();
                // Check if Internet present
                TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);
         //       error_pwd.setTypeface(font);
           //     error_email.setTypeface(font);

                if (password.equals("") || email.equals("")) {

                    if (password.equals("")) {
                        error_pwd.setVisibility(View.VISIBLE);
                        error_pwd.setText("Please enter a password");

                    } else {
                        error_pwd.setVisibility(View.GONE);

                    }
                    if (email.equals("")) {
                        error_email.setVisibility(View.VISIBLE);
                        error_email.setText("Please enter a Employee Id");
                    } else {
                        error_email.setVisibility(View.GONE);

                    }
                } else {

                    if (cd.isConnectingToInternet()) {

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);

                        String map_latitude = pref.getString("pref_latitude", "0");
                        String map_longitude = pref.getString("pref_longitude", "0");
                        map_address = pref.getString("map_address", null);

                        dbl_latitude = Double.parseDouble(map_latitude);
                        dbl_longitude = Double.parseDouble(map_longitude);

                        if (dbl_latitude > 0) {
                            lattitude = String.valueOf(dbl_latitude);
                            longitude = String.valueOf(dbl_longitude);
                            //location_lat.setText(map_latitude);
                            // location_long.setText(map_longitude);
                        } else {
                            dbl_latitude = appLocationService.getLatitude();
                            dbl_longitude = appLocationService.getLongitude();
                            lattitude = String.valueOf(dbl_latitude);
                            longitude = String.valueOf(dbl_longitude);
                        }


                        lattitude=map_latitude;
                        longitude=map_longitude;

                        new Login().execute();

                    } else {

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);


                        String map_latitude = pref.getString("pref_latitude", "0");
                        String map_longitude = pref.getString("pref_longitude", "0");
                        map_address = pref.getString("map_address", null);

                        dbl_latitude = Double.parseDouble(map_latitude);
                        dbl_longitude = Double.parseDouble(map_longitude);

                        if (dbl_latitude > 0) {
                            lattitude = String.valueOf(dbl_latitude);
                            longitude = String.valueOf(dbl_longitude);
                            //location_lat.setText(map_latitude);
                            // location_long.setText(map_longitude);
                        } else {
                            dbl_latitude = appLocationService.getLatitude();
                            dbl_longitude = appLocationService.getLongitude();
                        }


                        lattitude=map_latitude;
                        longitude=map_longitude;

                        // userFunction.logoutUserclear(getApplicationContext());

                        user=   db.off_getUserDetails();

                        System.out.println("HASHMAP::::" + user);

                        String val=(String)user.get("uid");
                        String dev_val=(String)user.get("dev_id");

                     //   String userid=pref.getString("user_id", null);

                        System.out.println("USERID::::"+email);

                        System.out.println("DEVICE ID::::::::"+dev_val);

                        if(val!=null) {

                            if (val.equals(email)) {

                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "yyyy-MM-dd");
                                String current_date = sdf.format(new Date());

                                Editor editor = pref.edit();

                                editor.putInt("off_is_login", 1);

                                editor.putString("current_date_off", current_date);
                                editor.commit();
                                System.out.println("OFF LOGIN:::");

                                off_rowcount=db.off_getRowCount();
                                if(off_rowcount>0) {
                                    if (!dev_val.equals("")) {

                                        System.out.println("DEVICE ID VAL:::" + dev_val);

                                        if (!dev_val.equals(android_id)) {
                                            userFunction.cutomToast(
                                                    "You have changed your handset. Contact your supervisor",
                                                    getApplicationContext()

                                            );

                                            loginErrorMsg.setText("You have changed your handset. Contact your supervisor");


                                        } else {
                                            Intent messagingActivity = new Intent(
                                                    LoginActivity.this, HdbHome.class);
                                            startActivity(messagingActivity);
                                            finish();
                                        }
                                    }else {
                                        Intent messagingActivity = new Intent(
                                                LoginActivity.this, HdbHome.class);
                                        startActivity(messagingActivity);
                                        finish();
                                    }
                                }else {
                                    userFunction.noInternetConnection(
                                            "Check your connection settings!",
                                            getApplicationContext());

                                    loginErrorMsg.setText("Check your connection settings!");
                                }
                            } else {
                                userFunction.cutomToast(
                                        "Username or Password is incorrect",
                                        getApplicationContext());

                                loginErrorMsg.setText("Username or Password is incorrect");
                            }
                        }else {
                            userFunction.noInternetConnection(
                                    "Check your connection settings!",
                                    getApplicationContext());

                            loginErrorMsg.setText("Check your connection settings!");
                        }

                       /* userFunction.noInternetConnection(
                                "Check your connection settings!",
                                getApplicationContext());*/
                    }
                }

            }
        });

    }

    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());

                }

            }
        });
    }

    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            String str_imei = getIMEI(LoginActivity.this);

            try {
                String deviceName = android.os.Build.MODEL;
                String deviceMan = android.os.Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, str_imei,
                        lattitude, longitude, deviceName, deviceMan, android_id);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);


                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            // user successfully logged in'
                            // Store user details in SQLite Database10
                            DatabaseHandler db = new DatabaseHandler(
                                    getApplicationContext());
                            JSONObject json_user = json.getJSONObject("data");
                         //   JSONObject json_user1 = json.getJSONObject("imei");

                            // Clear all previous data in database
                        userFunction
                                    .logoutUserclear(getApplicationContext());

                            userFunction
                                    .off_logoutUserclear(getApplicationContext());


                            String KEY_EMAIL = "email";
                            String KEY_NAME = "name";
                            String KEY_UID = "id";
                            String KEY_deviceid = "imei";

                            db.addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID));

                            db.off_addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID),
                                    json.getString(KEY_deviceid)
                                   );

                            SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd");
                            String current_date = sdf.format(new Date());

                            Editor editor = pref.edit();
                            editor.putString("current_date", current_date);

                            editor.putInt("is_login", 1);
                            editor.putString("user_id",
                                    json_user.getString(KEY_UID).toString());
                            editor.putString("user_name",
                                    json_user.getString(KEY_NAME).toString());
                            String KEY_BRANCH = "branch";
                            editor.putString("user_brnch",
                                    json_user.getString(KEY_BRANCH).toString());
                            editor.putString("user_email",
                                    json_user.getString(KEY_EMAIL).toString());
                            editor.putString("user_designation",
                                    json_user.getString("designation").toString());
                            editor.putString("user_designation_value",
                                    json_user.getString("user_designation_value").toString());

                            editor.putString("avi_tel",
                                    json_user.getString("Avinash").toString());
                            editor.putString("sach_tel",
                                    json_user.getString("Sachin").toString());
                            editor.putString("vija_tel",
                                    json_user.getString("Vijay").toString());
                            editor.putString("service_tel",
                                    json_user.getString("Service").toString());

                            if(!json_user.getString("img_height").equals("")){
                                editor.putInt("img_height",
                                        Integer.valueOf(json_user.getString("img_height")));
                            }

                            if(!json_user.getString("img_width").equals("")) {
                                editor.putInt("img_width",
                                        Integer.valueOf(json_user.getString("img_width")));
                            }

                            if(!json_user.getString("img_comp_size").equals("")) {
                                editor.putInt("img_comp_size",
                                        Integer.valueOf(json_user.getString("img_comp_size").toString()));
                            }

                            System.out.println("Service::::::"+json_user.getString("Service").toString());
                            if (rememberMe.isChecked()) {
                                editor.putString("user_username", email);
                                editor.putString("user_password", password);
                            } else {
                                editor.putString("user_username", "");
                                editor.putString("user_password", "");
                            }
                            String loan_count = json_user.getString("loan_count");

                            int int_loan_count= Integer.parseInt(loan_count);
                            if(int_loan_count>0){
                               // userFunction.send_Alloc_notification("You have "+loan_count+" not attempt cases click here to view ",LoginActivity.this);
                            }


                            if(check_master.get(0).equals("0") || check_master.get(1).equals("0") ){
                                //  new Download_Masters().execute();

                                System.out.println("MASTERS LOAD");

                                new Download_Masters().execute();
                            }
                            else{
                                master_flag= rDB.get_master_flag();

                                System.out.println(" FLAG DC:::" + master_flag.get(0)+" FLAG PM::: "+ master_flag.get(2));


                                flag_pd= master_flag.get(0);
                                id_pd= master_flag.get(1);

                                flag_cons= master_flag.get(2);
                                id_cons= master_flag.get(3);

                                new Verify_Masters().execute();

                            }

                            editor.commit();

                            /*
                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, HdbHome.class);
                            startActivity(messagingActivity);
                            finish();
                            */

/*
                            if(check_master.get(0).equals("0") || check_master.get(1).equals("0") || check_master.get(2).equals("0")){
                                //  new Download_Masters().execute();

                                System.out.println("MASTERS LOAD");

                                new Download_Masters().execute();
                            }
                            else{
                            //    master_flag= rDB.get_master_flag();

                              System.out.println(" FLAG DC:::" + master_flag.get(0)+" FLAG PM::: "+ master_flag.get(2));

                               flag_dc = master_flag.get(0);
                                 id_dc= master_flag.get(1);
                                flag_pm= master_flag.get(2);
                                id_pm= master_flag.get(3);

                                flag_pd= master_flag.get(4);
                                id_pd= master_flag.get(5);

                                new Verify_Masters().execute();


                            } */

                        } else {
                            String KEY_DEVID = "imei";

                            if(!json.getString(KEY_DEVID).toString().equals("")) {
                                db.update_deviceid(json.getString(KEY_DEVID));
                            }

                            if (new_version != null && new_version != "") {
                                edt_new_version_link.setText(new_version);
                                edt_new_version_link.setVisibility(View.VISIBLE);
                                txt_new_version.setText(new_version_name);
                                txt_new_version.setVisibility(View.VISIBLE);
                            }
                            loginErrorMsg.setText(json.getString("message")
                                    .toString());
                        }
                    }
                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb_progress.setVisibility(View.GONE);

        }

    }



    public String getIMEI(Context context) {

        TelephonyManager mngr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }

    /*
     * private boolean CheckEmail(String email) {
     *
     * return EMAIL_PATTERN.matcher(email).matches(); }
     */
    private boolean CheckUsername(String username) {

        return USERNAME_PATTERN.matcher(username).matches();
    }

    private boolean CheckPassword(String password) {

        return PASSWORD_PATTERN.matcher(password).matches();
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void contact_us(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                Contact_us.class);
        startActivity(home_activity);

    }


    void showRationale(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("njdskfbsbfhkssf,m ?")
                .setCancelable(false)
                .setPositiveButton("Retry",

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (ContextCompat.checkSelfPermission(LoginActivity.this,
                                        Manifest.permission.READ_PHONE_STATE)
                                        != PackageManager.PERMISSION_GRANTED) {

                                    // Should we show an explanation?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                                            Manifest.permission.READ_PHONE_STATE)) {

                                        // Show an expanation to the user *asynchronously* -- don't block
                                        // this thread waiting for the user's response! After the user
                                        // sees the explanation, try again to request the permission.

                                    } else {

                                        // No explanation needed, we can request the permission.

                                        ActivityCompat.requestPermissions(LoginActivity.this,
                                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                        // app-defined int constant. The callback method gets the
                                        // result of the request.
                                    }
                                }

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_masters(null);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {



                            JSONObject jobj1 = jsonobject.getJSONObject("data1");

                            JSONObject jobj2 = jsonobject.getJSONObject("data2");



                            jsonarray1 = jobj1.getJSONArray("cons");

                            jsonarray2 = jobj2.getJSONArray("product");

                            if ( jsonarray1!=null && jsonarray2!=null ) {



                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_cons.add(jsonobject.getString("cons"));
                                    al_cons_val.add(jsonobject.getString("cons_val"));
                                    al_cons_flag.add(jsonobject.getString("cons_flag"));
                                    al_cons_id.add(jsonobject.getString("cons_id"));

                                }


                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        }else
                        {

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try{
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        if(jsonarray1!=null && jsonarray2!=null ) {


                            rDB.delete_cons_table();
                            rDB.delete_pd_table();



                            for (int i = 0; i < al_product.size(); i++) {

                                System.out.println("AL PD:::" + al_product.get(i));
                                System.out.println("AL PD:::" + al_prod_val.get(i));
                                System.out.println("AL PD:::" + al_prod_flag.get(i));
                                System.out.println("AL PD:::" + al_prod_id.get(i));

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));
                            }

                            for (int i = 0; i < al_cons.size(); i++) {

                                System.out.println("AL PD:::" + al_cons.get(i));
                                System.out.println("AL PD:::" + al_cons_val.get(i));
                                System.out.println("AL PD:::" + al_cons_flag.get(i));
                                System.out.println("AL PD:::" + al_cons_id.get(i));

                                rDB.insert_cons(al_cons.get(i),al_cons_val.get(i),al_cons_flag.get(i),al_cons_id.get(i));
                            }

                            mProgressDialog.dismiss();

                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, HdbHome.class);
                            startActivity(messagingActivity);
                            finish();

                        }

                    } else {
                        mProgressDialog.dismiss();
                        Editor editor= pref.edit();
                        editor.putInt("is_login",0);
                        editor.commit();
                        loginErrorMsg.setText(jsonobject.getString("message").toString());
                        //pd_details_layout.setVisibility(View.GONE);

                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {jsonobject=null;
                jsonobject = userFunction.check_masters(flag_cons,id_cons,flag_pd,id_pd);

                System.out.println("JSONOBJ::::"+jsonobject);

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_VM) != null) {
                        res = jsonobject.getString(KEY_STATUS_VM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_VM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                            System.out.println("jobj1::jsonobject::::"+jsonobject);


                            JSONObject jobj4=null,jobj3=null;
                            if(!jsonobject.isNull("data1")) {
                                 jobj3 = jsonobject.getJSONObject("data1");
                                jsonarray1 = jobj3.getJSONArray("arr_cons");
                            }

                            if(!jsonobject.isNull("data2")) {
                                 jobj4 = jsonobject.getJSONObject("data2");
                                jsonarray2 = jobj4.getJSONArray("arr_product");
                            }


                            System.out.println("jobj1:::"+jobj3);
                            System.out.println("jobj2:::"+jobj4);

                            if (jsonarray1 != null ) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_cons.add(jsonobject.getString("cons"));
                                    al_cons_val.add(jsonobject.getString("cons_val"));
                                    al_cons_flag.add(jsonobject.getString("cons_flag"));
                                    al_cons_id.add(jsonobject.getString("cons_id"));

                                }
                            }

                            if (jsonarray2 != null ) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id"));

                                }
                            }

                        }else
                        {
                            mProgressDialog.dismiss();
                            Editor edt=pref.edit();
                            edt.putInt("is_login",0);

                            edt.commit();
                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try{
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray1);



                        if(jsonarray1!=null) {

                            rDB.delete_cons_table();

                            for (int i = 0; i < al_cons.size(); i++) {

                                rDB.insert_cons(al_cons.get(i), al_cons_val.get(i), al_cons_flag.get(i), al_cons_id.get(i));

                            }

                        }

                        if(jsonarray2!=null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }
                        }

                        mProgressDialog.dismiss();

                        Intent messagingActivity = new Intent(
                                LoginActivity.this, HdbHome.class);
                        startActivity(messagingActivity);
                        finish();



                    } else {

                        mProgressDialog.dismiss();
                        Editor editor= pref.edit();
                        editor.putInt("is_login",0);
                        editor.commit();
                        loginErrorMsg.setText(jsonobject.getString("message").toString());
                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }



    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
                android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

            }
            return;
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

}
