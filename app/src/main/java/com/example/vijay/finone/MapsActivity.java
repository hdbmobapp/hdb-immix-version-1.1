package com.example.vijay.finone;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.vijay.finone.GPSTracker_New;
import com.example.vijay.finone.libraries.UserFunctions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileOutputStream;

import com.example.vijay.finone.libraries.UserFunctions;


public class MapsActivity extends FragmentActivity implements
        GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerDragListener {
    double latitude;
    double longitude;
    GoogleMap googleMap;
    Button btn_refresh, btn_setlocation;
    SharedPreferences pref;
    String str_latitude;
    String str_longitude;
    String str_losid;
    String str_ts;
    String str_user_id;
    String pd_label;

    String str_from_act;
    Location location;
    UserFunctions userfunction;
    Marker mCurrLocationMarker;
    LatLng latLng;
    String addres = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_len_activity_maps);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        userfunction = new UserFunctions();


        Intent asset = getIntent();

        str_losid = asset.getStringExtra("str_losid");
        //  str_app_form_no = asset.getStringExtra("str_app_form_no_map");
        //  str_from_act = asset.getStringExtra("str_from_act");
        str_ts = asset.getStringExtra("str_ts");

        pd_label = asset.getStringExtra("pd_label");

        str_user_id = pref.getString("user_id", null);


        System.out.println("TS MAP::::" + str_ts);

        str_user_id = pref.getString("user_id", null);


        if (str_losid.trim().length() == 0) {
            str_losid = null;
        }

        System.out.println("str_losid_no:::" + str_losid);


        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove("map_cont");
        peditor.commit();

        setlayout();

        btn_refresh = (Button) findViewById(R.id.btn_refresh);
        // mDrawerList.setAdapter(adapter);
        btn_setlocation = (Button) findViewById(R.id.btn_setlocation);

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLocationChanged();
            }

        });

        btn_setlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_latitude = String.valueOf(latitude);
                str_longitude = String.valueOf(longitude);

                if (str_latitude != null) {
                    CaptureMapScreen();
                } else {
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("is_locate_clicked", "yes");
                    peditor.putString("map_img_name", null);
                    peditor.commit();
                    userfunction.cutomToast("Location service is not active...Map not captured", MapsActivity.this);
                }
                finish();
            }

        });
    }

    public void setlayout() {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //    googleMap.clear();
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();
            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMarkerDragListener(this);
            googleMap.setOnMapLongClickListener(MapsActivity.this);
            googleMap.setOnMapClickListener(MapsActivity.this);


            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
            GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);
            // check if location is available
            if (tracker.isLocationEnabled()) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
            } else {
                // show dialog box to user to enable location
                tracker.askToOnLocation();
            }

            onLocationChanged();
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            if(longitude>0) {
                AddmarkerforStore();
            }

        }
    }

    public void onLocationChanged() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        TextView tvLocation = (TextView) findViewById(R.id.tv_location);

        GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);
        // check if location is available
        if (tracker.isLocationEnabled()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            addres = tracker.getCompleteAddressString(latitude, longitude, MapsActivity.this);
        } else {
            // show dialog box to user to enable location
            tracker.askToOnLocation();
        }

        // Creating a LatLng object for the current location
        latLng = new LatLng(latitude, longitude);
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        if(latitude>0) {
            AddmarkerforStore();
        }
        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" + latitude + ",Longitude:" + longitude + "\nAddress::" + addres);


    }

    public void AddmarkerforStore() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        // adding marker
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = googleMap.addMarker(markerOptions);

        /*
        Location ltd= googleMap.getMyLocation();
        System.out.println("getMyLocation::::"+ltd);
        double latt_dbl= ltd.getLatitude();
        double long_dbl= ltd.getLatitude();
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latt_dbl, long_dbl))
                .title("my location")
                .snippet("")
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
*/
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onMarkerDrag(Marker arg0) {

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {
        LatLng dragPosition = arg0.getPosition();
        final double dragLat = dragPosition.latitude;
        final double dragLong = dragPosition.longitude;
    }

    @Override
    public void onMarkerDragStart(Marker arg0) {

    }

    @Override
    public void onMapClick(LatLng arg0) {
        //  map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
    }


    @Override
    public void onMapLongClick(LatLng arg0) {

        //create new marker when user long clicks
//        map.addMarker(new MarkerOptions()
//        .position(arg0)
//        .draggable(true));
    }


    private void takeScreenshot() {


        String new_path = "/mnt/sdcard/DCIM/"  + str_losid + "_" + str_user_id + "_" + str_ts
                + "_" + pd_label+"_MAP.jpg";

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + new_path;

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    public void CaptureMapScreen() {

        SharedPreferences.Editor peditor = pref.edit();

        peditor.putString("is_locate_clicked", "yes");
        peditor.commit();


        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {


                    String new_path = "/mnt/sdcard/DCIM/"  + str_losid + "_" + str_user_id + "_" + str_ts
                            + "_" + pd_label+"_MAP.jpg";

                    System.out.println("SNAPSHT CLICKED");
                    FileOutputStream out = new FileOutputStream(new_path);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("map_img_name", new_path);
                    if(latitude>0) {
                        peditor.putInt("is_map_captured", 1);

                    }else{
                        peditor.putInt("is_map_captured", 0);
                    }                    peditor.putString("map_address", addres);
                    peditor.putString("pref_latitude", str_latitude);
                    peditor.putString("pref_longitude", str_longitude);
                    peditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);

    }


}