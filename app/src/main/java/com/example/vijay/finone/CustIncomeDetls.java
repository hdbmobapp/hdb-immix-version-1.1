package com.example.vijay.finone;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import android.support.v4.content.ContextCompat;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.hdb.R;
import com.example.vijay.finone.libraries.DBHelper;
import com.example.vijay.finone.libraries.Dashboard;
import com.example.vijay.finone.GPSTracker_New;
import com.example.vijay.finone.libraries.ImageUtils;
import com.example.vijay.finone.MapsActivity;
import com.example.vijay.finone.libraries.UserFunctions;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import com.example.vijay.finone.libraries.MyAdapter;

import com.example.vijay.finone.libraries.ConnectionDetector;
import com.example.vijay.finone.GPSTracker_New;


public class CustIncomeDetls extends Fragment implements AdapterView.OnItemSelectedListener{

    int i,j,k=0;
    EditText edt_name, edt_mob, edt_pan,edt_mname,edt_lname;
    Spinner spn_perceived_income,spn_pd_coll,spn_coll,spn_nts,spn_recomm;
    ArrayAdapter adp_perceived_income,adp_pd_coll,adp_coll,adp_nts,adp_recomm,adapter_distance_type;
    ArrayList<String> str_prod,str_prod1,str_prodid;

    Integer img_width,img_height,img_com_size;


    public static final int MEDIA_TYPE_IMAGE = 1;
    File file;

    File new_file=null;

    Location nwLocation;

    double dbl_latitude = 0;
    double dbl_longitude = 0;

    TableRow  row_stock,row_external;

    RelativeLayout row_coll;

    ArrayList<String> page_zero;
    TextUtils txt_utls;

    int REQUEST_CAMERA = 100;
    private String imgPath;
    private static final String IMAGE_DIRECTORY_NAME = "/PD";

    ConnectionDetector cd;

    TextInputLayout inp_mob,inp_fname,inp_lname,inp_pan_,inp_prod;

    LinearLayout main_lay,otp_lay;
    String perceived_income[]={"Perceived Income","10%",
            "10%-20%",
            "20%-30%",
            "30%-40%",
            "40%-50%",
            "50%-60%",
            "60%-70%",
            "70%-80%",
            "80%-90%",
            "90%-100%",
    };

    String pd_coll[]={"PD Collateral","Yes",
            "No"
    };

    String coll[]={ "Collateral","SORP",
            "SOCP","RCP","RRP","VRP","VCP","SOIP","Plot","Car"
    };

    String nts_profile[]={ "HRP NTS Profile","Yes",
            "No"
    };

    String recomm[]={ "Recommend","Positive",
            "Negative"
    };

    RelativeLayout  lay_coll,lay_pd_coll,lay_per_income;

    String product;
    ProgressDialog mProgressDialog;
    JSONObject json;
    UserFunctions userFunction;
    String random_no,str_user_id;
    public  static String str_appid;
    CustOffDetailsSE off_det;
    SharedPreferences pref,pData;
    EditText edt_total_income,edt_surpls,edt_oth_income,edt_oth_rent,edt_oth_agri,edt_emi_monthly,edt_credit_os,edt_surplus,
            edt_loan_usage,edt_recomm_amt,edt_remarks;

    BigDecimal net_profit,oth_buss_income,oth_income_rent,oth_income_agri,turnover,emi_monthly,cc_out,tot_sub1,tot_sub2,total_surplus;

    BigDecimal total_income;
    DBHelper dbHelp;

    String str_per_income,str_hts_profile,str_ts,str_pd_coll,str_coll,str_recomm,
    str_emi_month,str_cc_os,str_buz_income,str_income_rent,str_income_agri,str_total_income,str_surplus,str_loan_usage,str_recomm_amt,str_remarks
            ;
    String str_uri;


    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");

    private static final Pattern MOBILE_PATTERN = Pattern
            .compile("[0-9]{10}");

    Boolean is_self_emp;

    String lattitude = null, longitude = null;

    static  String ssstr_img_path;

    String gross_sal,user_id,str_losid;

    RelativeLayout row_per_income;

    GPSTracker_New appLocationService;

    public  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;

    static String str_full_structure_1, str_full_structure_2, str_full_structure_3, str_full_structure_4, str_full_structure_5, str_full_structure_6;
    static  String str_full_structure_new_path_1 , str_full_structure_new_path_3, str_full_structure_new_path_2, str_full_structure_new_path_4, str_full_structure_new_path_5, str_full_structure_new_path_6 ;

    int MEDIA_IMAGE_COUNT , MEDIA_IMAGE_COUNT_1
    , MEDIA_IMAGE_COUNT_2
    , MEDIA_IMAGE_COUNT_3,MEDIA_IMAGE_COUNT_4,MEDIA_IMAGE_COUNT_5,MEDIA_IMAGE_COUNT_6;


    int MEDIA_MAP_COUNT;
    Button btn_pic1,btn_pic2,btn_pic3,btn_pic4,btn_pic5,btn_pic6;
    ImageView img1,img2,img3,img4,img5,img6,img_map;

    TextView location_lat,location_long;

    int curr_DOC_no=0;

    private Uri url_file;

    int map_captured=0;

    String is_locate_clicked="";
    String map_path;

    String str_pd_photo_count;
    Bitmap my_bitmp=null;

    String date_time;

    String str_img_path = null;
    Spinner spn_distance_from_loc;
    EditText edt_distance_km;

    String str_distance_from_loc,str_distance_km;

    String map_address=null;

    String[] distance_type = {"Select Distance", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    public CustIncomeDetls() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(R.layout.pd_len_cust_income_details, container, false);


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        userFunction=new UserFunctions();

        cd= new ConnectionDetector(getContext());

        pref = getContext().getSharedPreferences("MyPref", 0); // 0 -

        pData = getContext().getSharedPreferences("pData", 0); // 0 -
        str_losid = pData.getString("str_los", null);
        
        is_self_emp = pref.getBoolean("is_self_emp", false);

        gross_sal = pref.getString("gross_sal", null);
        user_id = pref.getString("user_id", null);

        date_time=getDateTime1();

        img_height=pref.getInt("img_height", 600);
        img_width=pref.getInt("img_width", 400);
        img_com_size=pref.getInt("img_comp_size", 85);

        off_det=new CustOffDetailsSE();
        
        turnover=off_det.act_turnover;
        net_profit=off_det.net_cons;

         str_appid=str_user_id+"_"+getDateTime();
         str_ts = pData.getString("str_ts", null);
         product= pData.getString("str_prod", null);

        dbHelp=new DBHelper(getContext());

        // Inflate the layout for this fragment

        img_map = (ImageView) view.findViewById(R.id.img_map);

        if(map_captured!=0) {

            map_path=pref.getString("map_img_name",null);

           // display_img(map_path,img_map,"MAP");
        }

        //System.out.println("PRODUCT:::" + str_prod);

        if ( ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {



            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
           MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }

        appLocationService = new GPSTracker_New(getContext());

      //  nwLocation = appLocationService
       //         .getLocation();


        double ltt = 0;
        double lott = 0;

        str_full_structure_1="";
        str_full_structure_2="";
        str_full_structure_3="";
        str_full_structure_4="";
        str_full_structure_5="";
        str_full_structure_6="";
        location_lat = (TextView) view.findViewById(R.id.location_lat);
        location_long = (TextView)view. findViewById(R.id.location_long);

        if (appLocationService.isLocationEnabled) {
            dbl_latitude = appLocationService.getLatitude();
            dbl_longitude = appLocationService.getLongitude();
            //String addres = tracker.getCompleteAddressString(dbl_latitude, dbl_longitude, EditRequest.this);
            location_lat.setText("" + dbl_latitude);
            location_long.setText("" + dbl_longitude);


        } else {
            // show dialog box to user to enable location
            appLocationService.askToOnLocation();
        }

        Button btn_next = (Button) view.findViewById(R.id.btn_getotp);

        spn_distance_from_loc=(Spinner)view.findViewById(R.id.spn_distance_from_loc);
        edt_distance_km=(EditText)view.findViewById(
                R.id.edt_distance);

        // spn_prod = (Spinner) view.findViewById(R.id.spn_prod);

        lay_coll = (RelativeLayout) view.findViewById(R.id.tableRow15);

        lay_pd_coll = (RelativeLayout) view.findViewById(R.id.tableRow14);

        row_stock = (TableRow) view.findViewById(R.id.tbl_14);
        row_external = (TableRow) view.findViewById(R.id.tbl_15);

        row_per_income=(RelativeLayout) view.findViewById(R.id.tableRow10);

        if(is_self_emp==false){
            row_stock.setVisibility(View.GONE);
            row_external.setVisibility(View.GONE);
            row_per_income.setVisibility(View.GONE);
        }

        edt_oth_income = (EditText) view.findViewById(R.id.edt_buss_income);
        edt_oth_rent = (EditText) view.findViewById(R.id.edt_income_rent);
        edt_oth_agri = (EditText) view.findViewById(R.id.edt_income_agri);
        edt_emi_monthly = (EditText) view.findViewById(R.id.edt_emi_month);

        edt_credit_os = (EditText) view.findViewById(R.id.edt_cc_out);
        edt_surplus = (EditText) view.findViewById(R.id.edt_surplus);

        main_lay = (LinearLayout) view.findViewById(R.id.lay_main);

        inp_mob=(TextInputLayout)view.findViewById(R.id.input_layout_phone);

        spn_perceived_income=(Spinner)view.findViewById(R.id.spn_perc_income);
        edt_total_income=(EditText)view.findViewById(R.id.edt_total_income);


        edt_loan_usage=(EditText)view.findViewById(R.id.edt_loan_usage);
        edt_recomm_amt=(EditText)view.findViewById(R.id.edt_recomm_amt);
        edt_remarks=(EditText)view.findViewById(R.id.edt_remarks);

       row_coll =(RelativeLayout)view.findViewById(R.id.tableRow15);



        spn_pd_coll=(Spinner)view.findViewById(R.id.spn_pd_coll);
        spn_coll=(Spinner)view.findViewById(R.id.spn_coll);
        spn_nts=(Spinner)view.findViewById(R.id.spn_nts_profile);
        spn_recomm=(Spinner)view.findViewById(R.id.spn_recomm);

        adp_perceived_income = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, perceived_income);

        adp_perceived_income .setDropDownViewResource(R.layout.spinner_layout);
        spn_perceived_income.setAdapter(adp_perceived_income);
        spn_perceived_income.setOnItemSelectedListener(this);
        adp_perceived_income.notifyDataSetChanged();

        adp_pd_coll = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, pd_coll);

        adp_pd_coll .setDropDownViewResource(R.layout.spinner_layout);
        spn_pd_coll.setAdapter(adp_pd_coll);
        spn_pd_coll.setOnItemSelectedListener(this);
        adp_pd_coll.notifyDataSetChanged();

        adp_coll = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, coll);

        adp_coll .setDropDownViewResource(R.layout.spinner_layout);
        spn_coll.setAdapter(adp_coll);
        spn_coll.setOnItemSelectedListener(this);
        adp_coll.notifyDataSetChanged();

        adp_nts = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, nts_profile);

        adp_nts .setDropDownViewResource(R.layout.spinner_layout);
        spn_nts.setAdapter(adp_nts);
        spn_nts.setOnItemSelectedListener(this);
        adp_nts.notifyDataSetChanged();

        adp_recomm = new MyAdapter(getActivity(),
                R.layout.sp_display_layout ,R.id.txt_visit, recomm  );

        adp_recomm .setDropDownViewResource(R.layout.spinner_layout);
        spn_recomm.setAdapter(adp_recomm);
        spn_recomm.setOnItemSelectedListener(this);
        adp_recomm.notifyDataSetChanged();

        adapter_distance_type = new MyAdapter(getActivity(),
                R.layout.sp_display_layout,R.id.txt_visit, distance_type);
        adapter_distance_type
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);
        adapter_distance_type.notifyDataSetChanged();

        btn_pic1=(Button)view.findViewById(R.id.btn_DOC_1);
        btn_pic2=(Button)view.findViewById(R.id.btn_DOC_3);
        btn_pic3=(Button)view.findViewById(R.id.btn_DOC_2);
        btn_pic4=(Button)view.findViewById(R.id.btn_DOC_4);
        btn_pic5=(Button)view.findViewById(R.id.btn_DOC_5);
        btn_pic6=(Button)view.findViewById(R.id.btn_DOC_6);


        img1=(ImageView)view.findViewById(R.id.img_doc_1);
        img2=(ImageView)view.findViewById(R.id.img_doc_2);
        img3=(ImageView)view.findViewById(R.id.img_doc_3);
        img4=(ImageView)view.findViewById(R.id.img_doc_4);
        img5=(ImageView)view.findViewById(R.id.img_doc_5);
        img6=(ImageView)view.findViewById(R.id.img_doc_6);


        System.out.println("turnover111:::" + turnover);

        System.out.println("net pro111:::" + net_profit);

        System.out.println("gross sal:::" + gross_sal);

        System.out.println("self emp:::" + is_self_emp);


        if(product.equals("EBL")||product.equals("LAP")){
            lay_pd_coll.setVisibility(View.VISIBLE);
            lay_coll.setVisibility(View.VISIBLE);
        }else
        {
            if(product.equals("UCL")){

                lay_coll.setVisibility(View.VISIBLE);
            }

        }

                if (is_self_emp.equals(true)) {
                     total_income=null;
                    if(turnover!=null || net_profit!=null ) {
                        String trn = turnover.toString();
                        String net = net_profit.toString();
                        BigDecimal net_100= new BigDecimal(net);
                        BigDecimal net_trn= new BigDecimal(trn);
                        BigDecimal bg_100=new BigDecimal("100");
                        BigDecimal net_100_val= (net_100.divide(bg_100));

                        total_income = (net_trn.multiply(net_100_val));
                      //  NumberFormat disply_val= NumberFormat.getCurrencyInstance(locale.US);
                    }

                   // String str_tot_income= total_income.toString();
                    DecimalFormat df =new DecimalFormat("#.####");
                   // df.format(total_income);//bd_total_income.toString()
                    if(total_income!=null  ) {
                        edt_total_income.setText(String.valueOf(df.format(total_income)));
                    }
                }
            else {

                if (is_self_emp.equals(false)) {
                    if(!gross_sal.equals("")) {

                        System.out.println("jgghghjg"+gross_sal);
                        BigDecimal gross_12 = new BigDecimal(gross_sal).multiply(new BigDecimal("12"));
                        total_income = (gross_12);
                        String str_tot_income1 = total_income.toString();
                        edt_total_income.setText(str_tot_income1);
                    }
                }
            }

/*
        edt_name.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_mname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_lname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        edt_pan.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
*/
        view.setBackgroundColor(Color.WHITE);

/*spn_prod.setSelection(0);
        str_pro*/


        Button btn_locate_me = (Button) view.findViewById(R.id.btn_locate_me);
        // setImages();

        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), getActivity());
                }

                map_captured = pref.getInt("is_map_captured", 0);

                if (map_captured == 0) {

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("is_locate_clicked", "");
                    peditor.commit();

                    Intent img_pinch = new Intent(
                            getActivity(), MapsActivity.class);
                    img_pinch.putExtra("str_losid", str_losid);

                    img_pinch.putExtra("str_ts", str_ts);
                    img_pinch.putExtra("pd_label", "CU");


                    startActivity(img_pinch);
                } else {
                    userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                            getActivity());
                }
            }

        });

        btn_pic1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i=1;

                curr_DOC_no = 1;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();

/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/


            }


        });

        btn_pic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                j=1;

                curr_DOC_no = 3;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();

            }

        });

        btn_pic3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                k=1;

                curr_DOC_no = 2;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();
/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/
            }


        });


        btn_pic4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i=1;

                curr_DOC_no = 4;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();
                /*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/


            }


        });

        btn_pic5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                j=1;

                curr_DOC_no = 5;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();
                /*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/
            }

        });

        btn_pic6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                k=1;

                curr_DOC_no = 6;

                SharedPreferences.Editor peditor = pData.edit();
                peditor.putInt("curr_DOC_no", curr_DOC_no);
                peditor.putString("str_losid_no", str_losid);
                peditor.commit();

                takePicture();
/*
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        getOutputMediaFile(MEDIA_TYPE_IMAGE));
                startActivityForResult(intent, REQUEST_CAMERA);
*/
            }


        });



        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                System.out.println("submit click::::");
                getdata();
                boolean ins=true;


                try {

                    if(str_emi_month.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter EMI monthly",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_cc_os.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Credit card outstanding",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_buz_income.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Business income per Annum",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_income_rent.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter income rent per Annum",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if(str_income_agri.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter  income agri per Annum",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(  (is_self_emp==true && str_per_income==null) && ins==true){
                        userFunction.cutomToast("Please select Perceived income",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_hts_profile==null && ins==true){
                        userFunction.cutomToast("Please HRP NTS profile",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_loan_usage.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter loan end usage",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((product.equals("EBL")||product.equals("LAP")) && str_pd_coll==null && ins==true){
                        userFunction.cutomToast("Please select PD collateral",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if((product.equals("EBL")||product.equals("LAP") || product.equals("UCL")) && coll==null && ins==true){
                        userFunction.cutomToast("Please select collateral",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                try {

                    if((product.equals("UCL")) && coll==null && ins==true){
                        userFunction.cutomToast("Please select collateral",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_recomm==null && ins==true){
                        userFunction.cutomToast("Please select Recommend",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_recomm_amt.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter recommend amt",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_remarks.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Remarks",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }


                map_captured = pref.getInt("is_map_captured", 0);

                String map = Integer.toString(map_captured);
                String img_new_path = pref.getString("map_img_name", null);

                MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT_1 + MEDIA_IMAGE_COUNT_2 + MEDIA_IMAGE_COUNT_3+MEDIA_IMAGE_COUNT_4+MEDIA_IMAGE_COUNT_5+
                        MEDIA_IMAGE_COUNT_6;


                if (img_new_path != null) {
                    MEDIA_MAP_COUNT=1;
                    MEDIA_IMAGE_COUNT = MEDIA_IMAGE_COUNT+MEDIA_MAP_COUNT;
                }else{
                    MEDIA_MAP_COUNT=0;
                }

                str_pd_photo_count = String.valueOf(MEDIA_IMAGE_COUNT);


                try {
                    if(MEDIA_MAP_COUNT>0){
                        if(MEDIA_IMAGE_COUNT<=3 && ins==true) {
                            userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                            ins = false;
                        }
                    }

                    if(MEDIA_MAP_COUNT==0){
                        if(MEDIA_IMAGE_COUNT<=2 && ins==true) {
                            userFunction.cutomToast( getResources().getString(R.string.at_least_pic), getActivity());
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    is_locate_clicked=pref.getString("is_locate_clicked","");

                    if (is_locate_clicked.equals("") && ins == true) {
                        userFunction.cutomToast("Please Capture MAP by clicking locate me button...",
                                getActivity());
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if(str_distance_from_loc.equals("") && ins==true){
                        userFunction.cutomToast("Please select Distance Type",getActivity());
                        ins=false;

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                    if(str_distance_km.equals("") && ins==true){
                        userFunction.cutomToast("Please Enter Distance in KM",getActivity());
                        ins=false;

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



                String map_latitude = pref.getString("pref_latitude", "0");
                String map_longitude = pref.getString("pref_longitude", "0");
                map_address = pref.getString("map_address", null);

                dbl_latitude = Double.parseDouble(map_latitude);
                dbl_longitude = Double.parseDouble(map_longitude);

                if (dbl_latitude > 0) {
                    lattitude = String.valueOf(dbl_latitude);
                    longitude = String.valueOf(dbl_longitude);
                    //location_lat.setText(map_latitude);
                    // location_long.setText(map_longitude);
                    location_lat.setText(map_latitude);
                    location_long.setText(map_longitude);
                } else {
                    dbl_latitude = appLocationService.getLatitude();
                    dbl_longitude = appLocationService.getLongitude();

                    lattitude = String.valueOf(dbl_latitude);
                    longitude = String.valueOf(dbl_longitude);


                }

                lattitude=location_lat.getText().toString();
                longitude=location_long.getText().toString();




                map_captured = pref.getInt("is_map_captured", 0);

                System.out.println("map_captured::::" + map_captured + "::::str_distance_type::::" + str_distance_from_loc + "::::str_txt_distacne:::" + str_distance_from_loc);


                try {
                    if (map_captured == 1 && ins == true) {
                        if (str_distance_from_loc.equals("Map not loading") == true) {
                            if (dbl_latitude>0) {
                                userFunction.cutomToast("You already captured map... select other Distance type..",
                                        getActivity());
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (map_captured == 1 && ins == true) {
                        if ((str_distance_from_loc.equals("Exact location")) || ((str_distance_from_loc.equals("Map not loading")))) {
                            ins = true;
                        }else{
                            if (Integer.parseInt(str_distance_km)==0) {
                                userFunction.cutomToast("Distance cant be a Zero...",
                                        getActivity());
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }



                if(ins==true) {


                    if (img_new_path != null && dbl_latitude>0) {
                        dbHelp.insertImage(img_new_path, str_losid, user_id, str_ts, map, "0", "MAP");
                    }

                    if(!str_full_structure_1.equals("")) {
                        dbHelp.insertImage(str_full_structure_1, str_losid, user_id, str_ts, map, "0", "vis_card");
                    }
                    if(!str_full_structure_2.equals("")) {
                        dbHelp.insertImage(str_full_structure_2, str_losid, user_id, str_ts, map, "0", "int_setup");
                    }
                    if(!str_full_structure_3.equals("")) {
                        dbHelp.insertImage(str_full_structure_3, str_losid, user_id, str_ts, map, "0", "ext_setup");
                    }
                    if(!str_full_structure_4.equals("")) {
                        dbHelp.insertImage(str_full_structure_4, str_losid, user_id, str_ts, map, "0", "ext_setup");
                    }

                    if(!str_full_structure_5.equals("")) {
                    dbHelp.insertImage(str_full_structure_5, str_losid, user_id, str_ts, map, "0", "ext_setup");}

                    if(!str_full_structure_6.equals("")) {
                        dbHelp.insertImage(str_full_structure_6, str_losid, user_id, str_ts, map, "0", "ext_setup");
                    }

                    Boolean update;
                    update = dbHelp.update_cust_income_details(str_emi_month, str_cc_os, str_buz_income, str_income_rent, str_income_agri, str_total_income, str_surplus,
                            str_per_income, str_hts_profile, str_loan_usage, str_pd_coll, str_coll, str_recomm, str_recomm_amt, str_remarks, str_ts, lattitude, longitude,user_id,str_pd_photo_count,str_distance_from_loc,str_distance_km,map_address);


                    if (cd.isConnectingToInternet() == true) {
                        if (update == true) {

                            dbHelp.get_Customers_request(str_ts, "ON");
                            dbHelp.updateImage(str_ts, 1);
                           // getActivity().finish();

                        }
                    } else {
                        userFunction.cutomToast(getResources().getString(R.string.local_data), getActivity());

                        dbHelp.updateImage(str_ts, 1);
                        Intent home_activity = new Intent(getActivity(),
                                Dashboard.class);
                        startActivity(home_activity);
                        getActivity().finish();

                    }

                }
            }

        });

       edt_oth_income.addTextChangedListener(pincodeWatcher1);
        edt_oth_agri.addTextChangedListener(pincodeWatcher3);
       edt_oth_rent.addTextChangedListener(pincodeWatcher2);
        edt_emi_monthly.addTextChangedListener(pincodeWatcher4);
        edt_credit_os.addTextChangedListener(pincodeWatcher5);


        edt_oth_income.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                    if (hasFocus) {

/*
                      total_income= new BigDecimal( edt_total_income.getText().toString());
                        BigDecimal minus_amt;
                        if (!edt_oth_income.getText().toString().equals("")) {
                       //   if( is_self_emp.equals(true)) {
                              minus_amt =new BigDecimal (edt_oth_income.getText().toString());
                              total_income = total_income.subtract(minus_amt);
                              edt_total_income.setText(total_income.toString());
                            System.out.println("is_self_emp::::" + is_self_emp);

                            //   }
                          //  has_focus();
                        }
                        has_not_focus();
                        */
                      //  edt_oth_income.setText("");

                    }

            }
        });

        edt_oth_rent.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                    if (hasFocus) {
/*
                        total_income= new BigDecimal( edt_total_income.getText().toString());
                        System.out.println("TOTAL income:::"+total_income);

                        if (!edt_oth_rent.getText().toString().equals("")) {


                        //    if( is_self_emp.equals(true)) {
                                BigDecimal minus_amt1;
                                minus_amt1 = new BigDecimal(edt_oth_rent.getText().toString());
                                total_income = total_income.subtract(minus_amt1);
                                edt_total_income.setText(total_income.toString());
                         //   }
                           // has_focus();
                        }
                        has_not_focus();
                        */
                      //  edt_oth_rent.setText("");
                    }
            }
        });

        edt_oth_agri.setOnFocusChangeListener(new View.OnFocusChangeListener() {


            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
/*
                    total_income= new BigDecimal( edt_total_income.getText().toString());
                    if (!edt_oth_agri.getText().toString().equals("")) {

                      //  if( is_self_emp.equals(true)) {
                            BigDecimal minus_amt2;
                            minus_amt2 = new BigDecimal(edt_oth_agri.getText().toString());
                            total_income = total_income.subtract(minus_amt2);
                            edt_total_income.setText(total_income.toString());
                      //  }
                    }

                    has_not_focus();

                    */
                  //  edt_oth_agri.setText("");


                    // has_focus();
                }

            }
        });

        edt_emi_monthly.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
/*
                    if (!edt_emi_monthly.getText().toString().equals("")) {
                        BigDecimal minus_amt3;
                        minus_amt3 =new BigDecimal(edt_emi_monthly.getText().toString());
                        minus_amt3=minus_amt3.multiply(new BigDecimal("12"));

                        total_surplus = total_surplus.add(minus_amt3);
                        edt_surplus.setText(total_surplus.toString());
                    }
                    */
                 //   edt_emi_monthly.setText("");
                }

            }
        });


        edt_credit_os.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus){
/*
                    if(!edt_credit_os.getText().toString().equals("")) {
                        BigDecimal minus_amt3;
                        minus_amt3 = new BigDecimal(edt_credit_os.getText().toString());
                       minus_amt3= minus_amt3.multiply(new BigDecimal("0.1"));
                        total_surplus = total_surplus.add(minus_amt3);
                        edt_surplus.setText(total_surplus.toString());
                    }

                    */
                   // edt_credit_os.setText( "");
                }


            }
        });
return  view;

    }

    public void has_focus(){

        if(!edt_emi_monthly.getText().toString().equals("")) {
            BigDecimal minus_amt3;
            minus_amt3 = new BigDecimal(edt_emi_monthly.getText().toString());

            total_surplus = total_income.subtract(minus_amt3);
            edt_surplus.setText(total_surplus.toString());
        }
        edt_emi_monthly.setText("");


        if(!edt_credit_os.getText().toString().equals("")) {
            BigDecimal minus_amt3;
            minus_amt3 = new BigDecimal(edt_credit_os.getText().toString());
            total_surplus = total_income.subtract(minus_amt3);
            edt_surplus.setText(total_surplus.toString());
        }
        edt_credit_os.setText("");

    }


    public void has_not_focus(){
/*
        System.out.println("EMI MONTH::::" + pData.getString("emi_monthly", null).toString() + "CC OS::::" +
                pData.getString("cc_os", null).toString());

        String emi_moth= pData.getString("emi_monthly","").toString();
        BigDecimal emi_moth_dec;
        if(emi_moth.equals("")){
            emi_moth_dec=new BigDecimal("0.0");
        }else{
            emi_moth_dec=new BigDecimal(emi_moth);
        }

        String cc_oss= pData.getString("cc_os","").toString();
        BigDecimal cc_os_dec;
        if(cc_oss.equals("")){
            cc_os_dec=new BigDecimal("0.0");
        }else{
            cc_os_dec=new BigDecimal(cc_oss);
        }

        String tot_inc= pData.getString("tot_income","").toString();
        BigDecimal tot_inc_dec;
        if(tot_inc.equals("")){
            tot_inc_dec=new BigDecimal("0.0");
        }else{
            tot_inc_dec=new BigDecimal(tot_inc);
        }
*/
        if(is_self_emp!=null) {

/*if(emi_moth_dec==new BigDecimal( edt_emi_monthly.getText().toString().equals("")?"0.0":edt_emi_monthly.getText().toString())
       || tot_inc_dec==new BigDecimal( edt_total_income.getText().toString().equals("")?"0.0":edt_total_income.getText().toString())){}else {*/
            if (!edt_emi_monthly.getText().toString().equals("")) {
                // total_income = new BigDecimal(edt_total_income.getText().toString());

                emi_monthly = new BigDecimal(edt_emi_monthly.getText().toString());

                emi_monthly = emi_monthly.multiply(new BigDecimal("12"));

                if(!edt_total_income.getText().toString().equals("")) {
                    total_income = new BigDecimal( edt_total_income.getText().toString()
                    );

                    total_surplus = total_income.subtract(emi_monthly);
                }

            }
        }
        if (!edt_credit_os.getText().toString().equals(""))
        {

            cc_out = new BigDecimal(edt_credit_os.getText().toString());

            cc_out = cc_out.divide(new BigDecimal("10"));

            //total_income = new BigDecimal(edt_total_income.getText().toString());


            if(edt_emi_monthly.getText().toString().equals("") && total_income!=null){

                total_surplus = total_income.subtract(cc_out);
            }else {
                if(total_surplus!=null) {
                    total_surplus = total_surplus.subtract(cc_out);
                }
            }
            //   cc_out.multiply(new BigDecimal("0.1"));


        }


        if(is_self_emp!=null){

            //  if(/*cc_os_dec==new BigDecimal( edt_credit_os.getText().toString().equals("")?"0.0":edt_credit_os.getText().toString())
            //     || */   tot_inc_dec==new BigDecimal( edt_total_income.getText().toString().equals("")?"0.0":edt_total_income.getText().toString())){}else {


        }
        if(is_self_emp!=null){
            if(edt_emi_monthly.getText().toString().equals("") && edt_credit_os.getText().toString().equals("")) {

                if(!edt_total_income.getText().toString().equals("")){
                    total_surplus=new BigDecimal(edt_total_income.getText().toString());
                }

            }
        }
        if(total_surplus!=null) {
            edt_surplus.setText(total_surplus.toString());
        }
    }


    private  final TextWatcher pincodeWatcher1=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            BigDecimal minus_amt;
            if (!edt_oth_income.getText().toString().equals("")) {
                //   if( is_self_emp.equals(true)) {
                minus_amt =new BigDecimal (edt_oth_income.getText().toString());
                total_income = total_income.subtract(minus_amt);
                edt_total_income.setText(total_income.toString());
                System.out.println("is_self_emp::::" + is_self_emp);

                //   }
                //  has_focus();
            }

            has_not_focus();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() > 0) {




                System.out.println("HNF other income");
                if (edt_oth_income.getText().toString().equals("")) {
                    oth_buss_income = new BigDecimal("0.0");
                } else {
                    oth_buss_income = new BigDecimal(edt_oth_income.getText().toString());
                }

                //  if (is_self_emp.equals(true)) {


                if (!edt_total_income.getText().toString().equals("")) {
                    //   total_income = new BigDecimal(str_total_income);
                    total_income = total_income.add(oth_buss_income);
                }
                //   }

                if(total_income!=null) {

                    edt_total_income.setText(total_income.toString());
                }


                has_not_focus();
            }
        }


    }   ;

    private  final TextWatcher pincodeWatcher2=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            if (!edt_oth_rent.getText().toString().equals("")) {


                //    if( is_self_emp.equals(true)) {
                BigDecimal minus_amt1;
                minus_amt1 = new BigDecimal(edt_oth_rent.getText().toString());
                total_income = total_income.subtract(minus_amt1);
                edt_total_income.setText(total_income.toString());
                //   }
                // has_focus();
            }
            has_not_focus();

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() > 0) {

                if (edt_oth_rent.getText().toString().equals("")) {
                    oth_income_rent = new BigDecimal("0.0");
                } else {
                    oth_income_rent = new BigDecimal(edt_oth_rent.getText().toString());
                }

                //  if (is_self_emp.equals(true)) {

                //  total_income = new BigDecimal(edt_total_income.getText().toString());



                //   }
                if(total_income!=null) {
                    total_income = total_income.add(oth_income_rent);
                    edt_total_income.setText(total_income.toString());
                }

                has_not_focus();
            }
        }


    } ;



    private  final TextWatcher pincodeWatcher3=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (!edt_oth_agri.getText().toString().equals("")) {

                //  if( is_self_emp.equals(true)) {
                BigDecimal minus_amt2;
                minus_amt2 = new BigDecimal(edt_oth_agri.getText().toString());
                total_income = total_income.subtract(minus_amt2);
                edt_total_income.setText(total_income.toString());
                //  }
            }

            has_not_focus();

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() > 0) {


                if (edt_oth_agri.getText().toString().equals("")) {
                    oth_income_agri = new BigDecimal("0.0");
                } else {
                    oth_income_agri = new BigDecimal(edt_oth_agri.getText().toString());
                }

                //  if (is_self_emp.equals(true)) {

                if(!edt_total_income.getText().toString().equals("")) {
                    total_income = new BigDecimal(edt_total_income.getText().toString());

                }

                if(total_income!=null) {
                    total_income = total_income.add(oth_income_agri);
                }

                //  }


                if(total_income!=null) {
                    edt_total_income.setText(total_income.toString());
                }
                has_not_focus();
            }
        }


    };

    private  final TextWatcher pincodeWatcher4=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if (!edt_emi_monthly.getText().toString().equals("")) {
                BigDecimal minus_amt3;
                minus_amt3 =new BigDecimal(edt_emi_monthly.getText().toString());
                minus_amt3=minus_amt3.multiply(new BigDecimal("12"));

                if(total_surplus!=null) {
                    total_surplus = total_surplus.add(minus_amt3);
                    edt_surplus.setText(total_surplus.toString());
                }
            }

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() > 0) {


                if (edt_emi_monthly.getText().toString().equals("")) {
                    emi_monthly = new BigDecimal("0.0");
                } else {
                    emi_monthly =new BigDecimal(edt_emi_monthly.getText().toString());

                    emi_monthly=emi_monthly.multiply(new BigDecimal("12"));

                }

                //  if (is_self_emp != null) {


                  /*  if(edt_surplus.getText().toString().equals("")) {

                        System.out.println("TOT INCOME OFFSET ::::" + total_income);

                          //  total_income = new BigDecimal(edt_total_income.getText().toString());
                            total_surplus = total_income.subtract(emi_monthly);

                    }else{

                    }*/
                if (total_surplus==null && total_income!=null) {
                    //   total_income = new BigDecimal(edt_total_income.getText().toString());
                    total_surplus = total_income.subtract(emi_monthly);
                    edt_surplus.setText(total_surplus.toString());
                } else {
                    if(total_surplus!=null) {

                        // total_surplus = new BigDecimal(edt_surplus.getText().toString());
                        total_surplus = total_surplus.subtract(emi_monthly);
                        edt_surplus.setText(total_surplus.toString());
                    }
                }

                // }
            }
        }


    };


    private  final TextWatcher pincodeWatcher5=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            if(!edt_credit_os.getText().toString().equals("")) {
                BigDecimal minus_amt3;
                minus_amt3 = new BigDecimal(edt_credit_os.getText().toString());
                minus_amt3= minus_amt3.multiply(new BigDecimal("0.1"));

                if(total_surplus!=null) {
                    total_surplus = total_surplus.add(minus_amt3);
                    edt_surplus.setText(total_surplus.toString());
                }
            }
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() > 0) {


                if(edt_credit_os.getText().toString().equals("")){
                    cc_out=new BigDecimal("0.0");
                }else {
                    cc_out =new BigDecimal(edt_credit_os.getText().toString());

                    cc_out=cc_out.multiply(new BigDecimal("0.1"));
                }

                if(is_self_emp!=null){
                    if(total_surplus==null  && total_income!=null) {
                        //  total_income = new BigDecimal(edt_total_income.getText().toString());
                        total_surplus = total_income.subtract(cc_out);
                        edt_surplus.setText( total_surplus.toString());
                    }else {
                        if(total_surplus!=null) {
                            //   total_surplus = new BigDecimal( edt_surplus.getText().toString());
                            total_surplus = total_surplus.subtract(cc_out);
                            edt_surplus.setText( total_surplus.toString());
                        }
                    }
                }
            }
        }


    };


    private boolean Check_pattern(String string,int i) {

        Boolean status=false;
        switch (i) {
            case 1:
                status=  MOBILE_PATTERN.matcher(string).matches();
                break;
            case 2:
                status=   PAN_PATTERN.matcher(string).matches();
                break;

        }

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy:hhmmss");
        String format = s.format(new Date());
        return format;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_perc_income:
                if(position==0){
                    str_per_income="";

                }else{
                    str_per_income=perceived_income[position];
                }
                break;

            case R.id.spn_nts_profile:
                if(position==0){
                    str_hts_profile="";
                }else{
                    str_hts_profile=nts_profile[position];
                }
                break;

            case R.id.spn_pd_coll:

                if(position!=0){
                    str_pd_coll=pd_coll[position].toString();
                }else {
                    str_pd_coll="";

                }

                    if(position==1){

                        row_coll.setVisibility(View.VISIBLE);
                    }else{
                        row_coll.setVisibility(View.GONE);
                    }

                break;

            case R.id.spn_coll:

                if(position==0){
                    str_coll="";
                }else{
                    str_coll=coll[position].toString();
                }

                break;

            case R.id.spn_recomm:

                if(position==0){
                    str_recomm="";
                }else{
                    str_recomm=recomm[position].toString();
                }

                break;

            case R.id.spn_distance_from_loc:

                if(spn_distance_from_loc.getSelectedItemPosition()!=0) {

                    str_distance_from_loc=distance_type[position].toString();

                }else {
                    str_distance_from_loc="";
                }

                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    void getdata(){
      str_emi_month = edt_emi_monthly.getText().toString();
        str_cc_os = edt_credit_os.getText().toString();
        str_buz_income = edt_oth_income.getText().toString();
        str_income_rent = edt_oth_rent.getText().toString();
        str_income_agri = edt_oth_agri.getText().toString();
        str_total_income = edt_total_income.getText().toString();
        str_surplus = edt_surplus.getText().toString();
        str_loan_usage = edt_loan_usage.getText().toString();
        str_recomm_amt = edt_recomm_amt.getText().toString();
        str_remarks = edt_remarks.getText().toString();

        str_distance_km=edt_distance_km.getText().toString();
    }


    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // Check for SD Card
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                        .show();
            } else {
                // Locate the image folder in your SD Card
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "HDB");
                // Create a new folder if no folder named SDImageTutorial exist
                file.mkdirs();
            }

            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;
        try {

            String curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


            if (type == MEDIA_TYPE_IMAGE) {
                file = new File(mediaStorageDir, str_losid + "_" + user_id + "_"
                        + str_ts + "_" + "CU"+"_"+curr_no + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                this.imgPath = file.getAbsolutePath();
                System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
                SharedPreferences.Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;

                    case 4:
                        editor.putString("str_DOC_4", this.imgPath);

                        break;
                    case 5:
                        editor.putString("str_DOC_5", this.imgPath);
                        break;
                    case 6:
                        editor.putString("str_DOC_6", this.imgPath);
                        break;
                    default:
                        break;

                }
                editor.commit();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }

   boolean display_img(String pic_uri,ImageView thumb_nail,String doc_no){


       System.out.println("FILE PATH:::" + pic_uri);
        if(thumb_nail!=img_map) {
          //  str_img_path = decodeFile(pic_uri,doc_no);


            File img_file = new File(pic_uri);


            if (img_file.exists()) {
                System.out.println("INSIDE EXIST:::"+pic_uri);
                my_bitmp = BitmapFactory.decodeFile(img_file.getAbsolutePath());

            }
        }
        thumb_nail.setImageBitmap(my_bitmp);
        return  true;
    }

    public void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        url_file = Uri.fromFile(getOutputMediaFile1235());

        System.out.println("URL FILE:::"+url_file);
        SharedPreferences.Editor peditor = pref.edit();

        peditor.remove(str_uri);
        peditor.commit();

        str_uri=url_file.toString();

        peditor.putString("str_uri", str_uri);
        peditor.commit();

        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        startActivityForResult(intent, 100);
    }

    private  File getOutputMediaFile1235(){

        String curr_no=null;
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        /*
        File mediaStorageDir = new File(Environment.getExternallStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
                 */
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }

        Uri imgUri = null;
        try {

             curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));


                file = new File(mediaStorageDir, str_losid + "_" + user_id + "_"
                        + str_ts + "_" + "CU"+"_"+curr_no + ".jpeg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                this.imgPath = file.getAbsolutePath();
                System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
                SharedPreferences.Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;

                    case 4:
                        editor.putString("str_DOC_4", this.imgPath);

                        break;
                    case 5:
                        editor.putString("str_DOC_5", this.imgPath);
                        break;
                    case 6:
                        editor.putString("str_DOC_6", this.imgPath);
                        break;
                    default:
                        break;

                }
                editor.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }


        new_file=new File(mediaStorageDir.getPath() + File.separator + str_losid + "_" + user_id + "_" + str_ts + "_CU_" + curr_no + ".JPEG");
        return new_file;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("requestCode:::" + requestCode+"resultCode::::"+resultCode+"REQ CAM:");

        if(resultCode!=0) {


            try {
                System.out.println("store URL111:::" + str_uri);
                if (requestCode == REQUEST_CAMERA) {

                    System.out.println("store URL:222::" + str_uri);


                    str_uri = pref.getString("str_uri", null);
                    url_file = Uri.parse(str_uri);
                    String new_img;
                    System.out.println("store URL333:::" + str_uri);

                    if (url_file != null) {

                        File new_img_file = new File(url_file.toString());
                        new_img = new_img_file.getAbsolutePath();

                        try {
                            new_img = new_img.replace("/file:", "");

                            Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(new_img, img_height, img_width, user_id, str_losid, str_ts, img_com_size, lattitude, longitude, date_time);

                            ssstr_img_path = storeImage(bitmap, user_id, str_losid, str_ts, curr_DOC_no);

                        } catch (Exception e) {
                            ssstr_img_path = new_img_file.getAbsolutePath();
                            ssstr_img_path = ssstr_img_path.replace("/file:", "");
                        }


                        SharedPreferences.Editor editor = pData.edit();

                        switch (pData.getInt("curr_DOC_no", 0)) {

                            case 1:
                                String picUri = pData.getString("str_DOC_1", null);
                                display_img(ssstr_img_path, img1, "1");

                                onCaptureImageResult(data, 1, picUri, btn_pic1);

                                break;

                            case 2:

                                String picUri2 = pData.getString("str_DOC_2", null);

                                display_img(ssstr_img_path, img2, "2");
                                onCaptureImageResult(data, 2, picUri2, btn_pic2);

                                break;

                            case 3:


                                String picUri3 = pData.getString("str_DOC_3", null);

                                display_img(ssstr_img_path, img3, "3");
                                onCaptureImageResult(data, 3, picUri3, btn_pic3);

                                break;

                            case 4:
                                String picUri4 = pData.getString("str_DOC_4", null);
                                display_img(ssstr_img_path, img4, "4");

                                onCaptureImageResult(data, 4, picUri4, btn_pic4);


                                break;

                            case 5:

                                String picUri5 = pData.getString("str_DOC_5", null);

                                display_img(ssstr_img_path, img5, "5");
                                onCaptureImageResult(data, 5, picUri5, btn_pic5);

                                break;

                            case 6:


                                String picUri6 = pData.getString("str_DOC_6", null);

                                display_img(ssstr_img_path, img6, "6");
                                onCaptureImageResult(data, 6, picUri6, btn_pic6);

                                break;

                        }
                    }


                } else if (resultCode == Activity.RESULT_CANCELED) {
                    // Delete the cancelled image

                    try {
                        //deleteLastPic();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                    SharedPreferences.Editor editor = pData.edit();
                    switch (pData.getInt("curr_DOC_no", 0)) {
                        case 1:
                            if (fileDelete(pData.getString("str_DOC_1", null))) {
                                editor.putString("str_DOC_1", "");
                            }

                            break;
                        case 2:
                            if (fileDelete(pData.getString("str_DOC_2", null))) {
                                editor.putString("str_DOC_2", "");
                            }

                            break;
                        case 3:
                            if (fileDelete(pData.getString("str_DOC_3", null))) {
                                editor.putString("str_DOC_3", "");
                            }
                            break;

                        default:
                            break;

                    }

                    editor.commit();

                    // user cancelled Image capture
                    Toast.makeText(getActivity(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
                } else {
                    // failed to capture image
                    Toast.makeText(getActivity(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            }
        }

    private void onCaptureImageResult(Intent data,  Integer img_no, String img_uri,Button dest_btn) {



      /*  boolean is_losid_empty= isEmptyString(str_losid_no);

        if(is_losid_empty==true){
            str_losid_no="null";
        }
        */


        SharedPreferences.Editor editor = pData.edit();
        switch (img_no) {
            case 1:

                String new_path = str_losid + "_" + user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "1";

                editor.putString("str_DOC_1", img_uri);
                editor.putString("str_New_DOC_1", new_path);
                editor.commit();

                MEDIA_IMAGE_COUNT_1 = 1;

                str_full_structure_1 = pData.getString("str_DOC_1", null);
                str_full_structure_new_path_1 = pData.getString("str_New_DOC_1", null);



                System.out.println("PHOTO 1::::" + str_full_structure_new_path_1);


                break;
            case 2:
                String new_path2 = str_losid + "_" + str_user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "2";

                editor.putString("str_DOC_2", img_uri);
                editor.putString("str_New_DOC_2", new_path2);
                editor.commit();
                MEDIA_IMAGE_COUNT_2 = 1;

                str_full_structure_2 = pData.getString("str_DOC_2", null);
                str_full_structure_new_path_2 = pData.getString("str_New_DOC_2", null);
                break;
            case 3:

                String new_path3 = str_losid + "_" + str_user_id + "_" + str_ts + "_"
                        + "_" + "CU" + "_" + "3";

                editor.putString("str_DOC_3", img_uri);
                editor.putString("str_New_DOC_3", new_path3);
                editor.commit();

                MEDIA_IMAGE_COUNT_3 = 1;

                str_full_structure_3 = pData.getString("str_DOC_3", null);
                str_full_structure_new_path_3 = pData.getString("str_New_DOC_3", null);
                break;

            case 4:

                String new_path4 = str_losid + "_" + user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "4";

                editor.putString("str_DOC_4", img_uri);
                editor.putString("str_New_DOC_4", new_path4);
                editor.commit();

                MEDIA_IMAGE_COUNT_4 = 1;

                str_full_structure_4 = pData.getString("str_DOC_4", null);
                str_full_structure_new_path_4 = pData.getString("str_New_DOC_4", null);


                break;
            case 5:
                String new_path5 = str_losid + "_" + str_user_id + "_"
                        + str_ts + "_" + "CU" + "_" + "5";

                editor.putString("str_DOC_5", img_uri);
                editor.putString("str_New_DOC_5", new_path5);
                editor.commit();
                MEDIA_IMAGE_COUNT_5 = 1;

                str_full_structure_5= pData.getString("str_DOC_5", null);
                str_full_structure_new_path_5 = pData.getString("str_New_DOC_5", null);
                break;

            case 6:

                String new_path6 = str_losid + "_" + str_user_id + "_" + str_ts + "_"
                        + "_" + "CU" + "_" + "6";

                editor.putString("str_DOC_6", img_uri);
                editor.putString("str_New_DOC_6", new_path6);
                editor.commit();

                MEDIA_IMAGE_COUNT_6 = 1;

                str_full_structure_6 = pData.getString("str_DOC_6", null);
                str_full_structure_new_path_6 = pData.getString("str_New_DOC_6", null);
                break;


            default:
                break;
        }

    }


    private String storeImage(Bitmap image, String userid, String losid, String ts, Integer str_pic_name) {
        String new_strMyImagePath = null;
        String pic_id=str_pic_name.toString();
        File pictureFile = getOutputMediaFile2(userid, losid, ts, pic_id);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

            return new_strMyImagePath;
        }

    }


    /* private String decodeFile(String path,String docno) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        try {
            System.out.println("::::::-3" + imgPath);
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(imgPath, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= 600 && unscaledBitmap.getHeight() <= 800)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }
            System.out.println("::::::-2");
            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/PD");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            System.out.println("::::::-1");

            String s = str_losid + "_" + user_id + "_"
                    + str_ts + "_" + "CU" + "_" + docno + ".jpg";

            File f = new File(mFolder.getAbsolutePath(), s);

            System.out.println("::::::0");

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                System.out.println("::::::1");
                Point p = new Point();
                p.set(10, 20);
                //  scaledBitmap = waterMark(scaledBitmap, "Form No= " + form_no + " LOS ID= " + losid + " Username = " + userid, p, Color.BLACK, 90, 14, true);
                System.out.println("::::::2");
                p.set(10, 40);
                // scaledBitmap = waterMark(scaledBitmap, "Lat= " + map_lat + " Long=" + map_long + " Time=" + ts_date, p, Color.BLACK, 90, 14, true);
                fos = new FileOutputStream(f);
                System.out.println("::::::3");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("::::::4");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("::::::5");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            //decodeFile(path);
            //compressImage(path);
            System.out.println("::::::6");

        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }
*/
    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                        System.out.println(path + " : Deleted");
                    } else {
                        System.out.println(path + " : CANT Dieleted");
                    }
                } else {
                    System.out.println(path + " : Do Not Exists");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }

    private File getOutputMediaFile2(String userid, String losid, String ts, String str_pic_name) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PD-Lending");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = losid + "_" + userid + "_" + ts + "_CU_" + str_pic_name + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }





}