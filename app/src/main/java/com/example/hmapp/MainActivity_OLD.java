package com.example.hmapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.AppMenu.Dashboard;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.ImageUtils;
import com.example.hmapp.AppMenu.RequestDB;
import com.example.hmapp.hdbLibrary.UserFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.example.hmapp.hdbLibrary.ConnectionDetector;


public class MainActivity_OLD extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    ConnectionDetector cd;
    UserFunctions userFunctions;
    int REQUEST_CAMERA = 0;
    //RequestDB rdb;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private String imgPath;
    private static final String IMAGE_DIRECTORY_NAME = "/HDB";
    ExampleDBHelper dbHelper;
    RequestDB rDB;
    File file;
    int pic_position = 0;
    String[] pic_list;
    String[] pic_values;
    String[] cu_pic_values = {"0", "1", "2", "3", "4", "5"};
    String[] cu_pic_names = {"Select Image Category", "Full Structure 1", "Full Structure 2", "Full Structure 3", "Vicinity photo from property ", "Name Board Sign Board"};
    //HashMap<String,String> cu_pic_name =new HashMap<String,String>();
    String[] coll_pic_value = {"0", "1", "2", "3", "4", "5", "6"};
    String[] coll_pic_name = {"Select Image Category", "Vehicle Front", "Vehicle Back", "Vehicle Right", "Vehicle Left", "Internal cabin", "RC + Insurance"};

    String[] form_pic_values = {"0", "1", "2", "3", "4"};
    String[] form_pic_name = {"Select Image Category", "PD Form 1", "PD Form 2", "PD Form 3", "PD Form 4"};

    String[] new_pic_values = {"0", "1", "2", "3", "4", "5", "6", "7"};
    String[] new_pic_name = {"Select Image Category", "App Form 1", "App Form 2", "App Form 2/ID Proof", "App Form 3/Residence Proof", "Other1", "Other2", "Other3"};

    String str_pic_catpure_list = null;
    String str_pic_catpure_list1 = null;
    ArrayAdapter<String> adapter_pic_catpure;
    Spinner spn_pic_catpure;
    Integer int_total_pic;
    List<String> list;
    List<String> list1;
    String losid, form_no, userid, ts, map_count, last_id, id;
    String str_position;
    String str_total_pic_count, str_pic_name;
    Integer total_pic_count;

    String map_lat, map_long, ts_date;
    private Uri url_file;
    File new_file = null;
    String str_uri;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_activity_main_old);

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("IMAGE UPLOAD");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);


        dbHelper = new ExampleDBHelper(this);
        userFunctions = new UserFunctions();
        cd = new ConnectionDetector(this);
        rDB = new RequestDB(this);

        Intent i = getIntent();
        id = i.getStringExtra("id");


        if (id.equals("CU")) {

            pic_list = cu_pic_names;
            pic_values = cu_pic_values;
            losid = i.getStringExtra("cus_losid");
            form_no = i.getStringExtra("cus_formno");
            userid = i.getStringExtra("cus_userid");
            ts = i.getStringExtra("cus_ts");
            map_count = i.getStringExtra("cus_map");
            map_lat = i.getStringExtra("lat");
            map_long = i.getStringExtra("long");
            ts_date = i.getStringExtra("ts_date");

            //  last_id = i.getStringExtra("cus_last");
        }

        if (id.equals("CO")) {
            pic_list = coll_pic_name;
            pic_values = coll_pic_value;
            System.out.println("inside coll:::::::");
            losid = i.getStringExtra("coll_losid");
            form_no = i.getStringExtra("coll_formno");
            userid = i.getStringExtra("coll_userid");
            ts = i.getStringExtra("coll_ts");
            map_count = i.getStringExtra("coll_map");
            ts_date = i.getStringExtra("ts_date");
            map_lat = i.getStringExtra("lat");
            map_long = i.getStringExtra("long");
            // last_id = i.getStringExtra("coll_last");
        }

        if (id.equals("FORM")) {
            pic_list = form_pic_name;
            pic_values = form_pic_values;
            System.out.println("inside form:::::::");
            losid = i.getStringExtra("form_losid");
            form_no = i.getStringExtra("form_formno");
            userid = i.getStringExtra("form_userid");
            ts = i.getStringExtra("form_ts");
            map_count = i.getStringExtra("form_map");
            ts_date = i.getStringExtra("ts_date");
            map_lat = i.getStringExtra("lat");
            map_long = i.getStringExtra("long");
            //  last_id = i.getStringExtra("form_last");
        }

        if (id.equals("NEW")) {
            pic_list = new_pic_name;
            pic_values = new_pic_values;
            losid = i.getStringExtra("new_losid");
            form_no = i.getStringExtra("new_formno");
            userid = i.getStringExtra("new_userid");
            ts = i.getStringExtra("new_ts");
            map_count = i.getStringExtra("new_map");
            ts_date = i.getStringExtra("ts_date");
            map_lat = i.getStringExtra("lat");
            map_long = i.getStringExtra("long");
        }


        list = new ArrayList<String>(Arrays.asList(pic_values));
        list1 = new ArrayList<String>(Arrays.asList(pic_list));
        //  list = new ArrayList<String>(Arrays.asList(pic_values));

        ImageView btnSelectPhoto = (ImageView) findViewById(R.id.btnSelectPhoto);
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_pic_catpure_list != null) {
                    takePicture(str_pic_catpure_list);
/*
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            getOutputMediaFile(MEDIA_TYPE_IMAGE));
                    startActivityForResult(intent, REQUEST_CAMERA);
                    */
                } else {
                    Toast.makeText(MainActivity_OLD.this, "Select Image category.....", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        Button btnSubmitPhoto = (Button) findViewById(R.id.btnsubmitpic);
        btnSubmitPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (id) {
                    case "CU":
                        if (total_pic_count >= 3) {
                            Boolean status_co = rDB.update_status(ts, "1", "CU", str_total_pic_count);
                            if (cd.isConnectingToInternet() == true) {
                                if (status_co == true) {
                                    rDB.get_Customers_request(ts);
                                }
                            } else {
                                userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                                Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                                startActivity(cust);
                                finish();
                            }
                        } else {
                            userFunctions.cutomToast(getResources().getString(R.string.at_least_pic), MainActivity_OLD.this);
                        }

                        break;
                    case "CO":
                        if (total_pic_count >= 3) {
                            Boolean status_co = rDB.update_status(ts, "1", "CO", str_total_pic_count);
                            if (cd.isConnectingToInternet() == true) {
                                if (status_co == true) {
                                    rDB.get_asset_request(ts);
                                }
                            } else {
                                userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                                Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                                startActivity(cust);
                                finish();

                            }
                        } else {
                            userFunctions.cutomToast(getResources().getString(R.string.at_least_pic), MainActivity_OLD.this);
                        }
                        break;
                    case "FORM":

                        if (total_pic_count >= 3) {
                            Boolean status_form = rDB.update_status(ts, "1", "FORM", str_total_pic_count);
                            if (cd.isConnectingToInternet() == true) {
                                if (status_form == true) {

                                    System.out.println("INSIDE FORM:::::");
                                    rDB.get_app_form_request(ts);
                                }
                            } else {
                                userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                                Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                                startActivity(cust);
                                finish();

                            }
                        } else {
                            userFunctions.cutomToast(getResources().getString(R.string.at_least_pic), MainActivity_OLD.this);
                        }
                        break;

                    case "NEW":

                        if (total_pic_count >= 3) {
                            Boolean status_new = rDB.update_status(ts, "1", "NEW", str_total_pic_count);
                            if (cd.isConnectingToInternet() == true) {
                                if (status_new == true) {
                                    rDB.get_cases_request(ts);
                                }
                            } else {
                                userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                                Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                                startActivity(cust);
                                finish();
                            }
                        } else {
                            userFunctions.cutomToast(getResources().getString(R.string.at_least_pic), MainActivity_OLD.this);
                        }
                        break;


                }
                dbHelper.updateImage(ts, 1);
            }

        });

        spn_pic_catpure = (Spinner) findViewById(R.id.spn_pic_capture);
        //  ivImage = (ImageView) findViewById(R.id.ivImage);
        adapter_pic_catpure = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, list1);
        adapter_pic_catpure
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_pic_catpure.setAdapter(adapter_pic_catpure);
        spn_pic_catpure.setOnItemSelectedListener(this);

        setlistview();
    }


    public void setlistview() {
        dbHelper = new ExampleDBHelper(this);

        final Cursor cursor = dbHelper.getAllImages(ts);
        total_pic_count = cursor.getCount();
        GridView lvItems = (GridView) findViewById(R.id.listView1);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageListAdapter todoAdapter = new ImageListAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        Integer int_map_count = Integer.parseInt(map_count);
        int_total_pic = total_pic_count;
        if (int_map_count > 0) {
            total_pic_count = int_total_pic - 1;
        }
        str_total_pic_count = String.valueOf(total_pic_count);
    }


    private String currentDateFormat() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        String currentTimeStamp = dateFormat.format(new Date());
        return currentTimeStamp;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // Check for SD Card
            if (!Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                Toast.makeText(this, "Error! No SDCARD Found!", Toast.LENGTH_LONG)
                        .show();
            } else {
                // Locate the image folder in your SD Card
                file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "HDB");
                // Create a new folder if no folder named SDImageTutorial exist
                file.mkdirs();
            }


            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;
        try {

            if (type == MEDIA_TYPE_IMAGE) {
                System.out.println("PIC ID::" + str_pic_catpure_list);
                file = new File(mediaStorageDir, form_no + "_" + userid + "_" + losid + "_"
                        + ts + "_" + id + "_" + str_pic_catpure_list + "_" + str_position + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                System.out.println(":::::imgUri" + imgUri);
                System.out.println(":::::imgPath" + imgPath);
                this.imgPath = file.getAbsolutePath();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }

    private void onCaptureImageResult(Intent data) {
        //String sdfdf= getImageUri(getApplicationContext(),thumbnail).toString();

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_pic_capture:
                str_pic_catpure_list = Arrays.asList(pic_values).get(Arrays.asList(pic_list).indexOf(list1.get(position)));
                str_pic_catpure_list1 = list1.get(position);
                // str_pic_catpure_list =list1.get(position);
                //String.valueOf(position) ;
                str_position = String.valueOf(position);
                str_pic_name = list1.get(position);


                pic_position = Integer.parseInt(str_position);
                System.out.println("position:::::" + position);
                if (position == 0) {
                    str_pic_catpure_list = null;
                }

                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("str_pic_name", str_pic_name);
                peditor.commit();

                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    /*
    private String decodeFile(String path) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;
        try {
            System.out.println("::::::-3" + imgPath);
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= 600 && unscaledBitmap.getHeight() <= 800)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, 700, 900, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }
            System.out.println("::::::-2");
            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/HDB");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            System.out.println("::::::-1");

            String s = form_no + "_" + userid + "_" + losid + "_"
                    + ts + "_" + id + "_" + str_pic_catpure_list + ".jpg";

            File f = new File(mFolder.getAbsolutePath(), s);

            System.out.println("::::::0");

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                System.out.println("::::::1");
                Point p = new Point();
                p.set(10, 20);
                //  scaledBitmap = waterMark(scaledBitmap, "Form No= " + form_no + " LOS ID= " + losid + " Username = " + userid, p, Color.BLACK, 90, 14, true);
                System.out.println("::::::2");
                p.set(10, 40);
                // scaledBitmap = waterMark(scaledBitmap, "Lat= " + map_lat + " Long=" + map_long + " Time=" + ts_date, p, Color.BLACK, 90, 14, true);
                fos = new FileOutputStream(f);
                System.out.println("::::::3");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                System.out.println("::::::4");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println("::::::5");
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            //decodeFile(path);
            //compressImage(path);
            System.out.println("::::::6");

        }

        if (strMyImagePath == null) {
            return path;
        }
        File img_size = new File(strMyImagePath);
        long file_lenght = img_size.length();
        long file_lenght_new = file_lenght / 1024;

        if (file_lenght_new > 800) {

            strMyImagePath = null;
            userFunctions.cutomToast("Not enough memory to process Image clear Ram and try later ", MainActivity_OLD.this);
        }

        System.out.println("file_lenght_new:::::" + file_lenght_new);

        return strMyImagePath;

    }

*/

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    private String storeImage(Bitmap image, String userid, String losid, String ts, String str_pic_name) {
        String new_strMyImagePath = null;
        File pictureFile = getOutputMediaFile2(userid, losid, ts, str_pic_name, str_pic_name);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                fos.close();
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();
            System.out.println(":::::111111:::new_strMyImagePath::::" + new_strMyImagePath);

            return new_strMyImagePath;
        }

    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile2(String userid, String losid, String ts, String str_pic_name, String str_pic_catpure_list_n) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB_PD");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = form_no + "_" + userid + "_" + losid + "_"
                + ts + "_" + id + "_" + str_pic_catpure_list_n + "_" + str_position + ".jpg";

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        System.out.println("mediaFile::::" + mediaFile);

        return mediaFile;
    }

    public void takePicture(String str_pic_catpure_list_n) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File new_file3 = getOutputMediaFile2(userid, losid, ts, str_pic_name, str_pic_catpure_list_n);
        System.out.println(":::::111111" + url_file);
        url_file = Uri.fromFile(new_file3);
        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove(str_uri);
        peditor.commit();
        str_uri = url_file.toString();
        peditor.putString("str_uri", str_uri);
        peditor.commit();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        startActivityForResult(intent, 100);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                str_uri = pref.getString("str_uri", null);
                str_pic_name = pref.getString("str_pic_name", null);
                url_file = Uri.parse(str_uri);

                //imageView.setImageURI(file);
                if (url_file != null) {


                    String str_img_path = url_file.toString();
                    //  String ssstr_img_path = new_file.getAbsolutePath();
                    File new_img_file = new File(url_file.toString());
                    str_img_path = new_img_file.getAbsolutePath();

                    String ssstr_img_path = null;

                    try {
                        str_img_path = str_img_path.replace("/file:", "");

                        System.out.println("Insite  try:::" + str_img_path);
                        Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(str_img_path, 800, 600, userid, losid, ts, form_no, map_lat, map_long);
                        ssstr_img_path = storeImage(bitmap, userid, losid, ts, str_pic_catpure_list);
                    } catch (Exception e) {
                        System.out.println("Insite  catch:::" + str_img_path);

                        ssstr_img_path = new_img_file.getAbsolutePath();
                        ssstr_img_path = ssstr_img_path.replace("/file:", "");

                    }
                    if (id.equals("FORM")) {
                        // bitmap = ImageUtils.getInstant().getCompressedBitmap(ssstr_img_path, 1000, 800, userid, losid, ts,form_no,map_lat,map_long);
                        try {
                            Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(str_img_path, 1000, 800, userid, losid, ts, form_no, map_lat, map_long);
                            ssstr_img_path = storeImage(bitmap, userid, losid, ts, str_pic_catpure_list);
                            // System.out.println("Insite  try:::"+str_img_path);
                        } catch (Exception e) {
                            ssstr_img_path = new_img_file.getAbsolutePath();
                            ssstr_img_path.replace("/file:", "");

                        }
                    }
                    // String str_new_path = storeImage(bitmap, userid, losid, ts, str_pic_catpure_list);

                    if (str_uri != null) {
                        // show the result here.
                        if (dbHelper.insertImage(ssstr_img_path, losid, form_no, userid, ts, map_count, last_id, "0", str_pic_name)) {
                            //  Toast.makeText(getApplicationContext(), "iMAGE Inserted", Toast.LENGTH_SHORT).show();

                            boolean ss = list.contains(str_pic_catpure_list);
                            if (ss == true) {
                                //  list.remove(str_pic_catpure_list);
                                //  list1.remove(str_pic_catpure_list1);
                                adapter_pic_catpure = new ArrayAdapter<>(this,
                                        android.R.layout.simple_spinner_dropdown_item, list1);
                                adapter_pic_catpure
                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spn_pic_catpure.setAdapter(adapter_pic_catpure);
                                spn_pic_catpure.setOnItemSelectedListener(this);

                                Toast.makeText(getApplicationContext(), "IMAGE", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Could not Insert IMAGE", Toast.LENGTH_SHORT).show();
                        }
                        setlistview();
                    }
                }
            } else {

                userFunctions.cutomToast("Could not capture Image Try again", getApplicationContext());
            }
        }
    }

}
