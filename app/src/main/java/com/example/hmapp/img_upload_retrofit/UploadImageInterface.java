package com.example.hmapp.img_upload_retrofit;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface UploadImageInterface {
    @Multipart
    @POST("/HDB_PD_Com/HDB_PD_V4.0/image_upload.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file);
}