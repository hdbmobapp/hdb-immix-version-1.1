package com.example.hmapp.AppMenu;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.example.hdb.R;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.DatabaseHandler;
import com.example.hmapp.hdbLibrary.GPSTracker;
import com.example.hmapp.hdbLibrary.GPSTracker_New;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.example.hmapp.HdbHome;

public class LoginActivity extends AppCompatActivity {
    ProgressDialog mProgressDialog;

    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunctions userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    //private static Typeface font;
    //String user_name = null;
    String gcm = null;
    @SuppressWarnings("unused")

    Context context;
    ProgressBar pb_progress;

    GPSTracker gps;
    String reg_id;

    SharedPreferences pref;
    String lattitude = null, longitude = null;
    String str_imei;
    String device_id;
    JSONObject jsonobject;
    JSONArray jsonarray;

    public static String KEY_STATUS = "status";

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";
    ArrayList<String> al_product, al_prod_val, al_prod_flag, al_prod_id;
    ArrayList<String> al_branch, al_branch_val, al_branch_flag, al_branch_id;

    String res, resp_success;
    RequestDB rDB;
    ArrayList<String> check_master;
    ArrayList<String> master_flag;

    String flag_pd, id_pd;
    String str_check_master;
    GPSTracker_New tracker;
    double dbl_latitude = 0;
    double dbl_longitude = 0;
    String str_brach_count;
    String imei1,imei2;

    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA

    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.login);
//		font = Typeface.createFromAsset(getBaseContext().getAssets(),
//				"fonts/Amaranth-Regular.otf");
        cd = new ConnectionDetector(getApplicationContext());
        gps = new GPSTracker(this);
        userFunction = new UserFunctions();
        rDB = new RequestDB(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        // for
        check_master = new ArrayList<String>();
        check_master = rDB.get_masters();
        str_check_master = check_master.get(0);
        SetLayout();
       // inputPassword.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

    }

    protected void SetLayout() {
        // Check if Internet present
        setContentView(R.layout.pd_sl_login);
        tracker = new GPSTracker_New(this);

        checkPermissions();


        TelephonyManager tm = (TelephonyManager) getSystemService(context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            imei1 = tm.getImei(1);
            imei2 = tm.getImei(2);

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            imei1 = tm.getDeviceId(1);
            imei2 = tm.getDeviceId(2);
        }





        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        str_imei = getIMEI(LoginActivity.this);
        device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        al_product = new ArrayList<String>();
        al_prod_val = new ArrayList<String>();
        al_prod_flag = new ArrayList<String>();
        al_prod_id = new ArrayList<String>();

        al_branch = new ArrayList<String>();
        al_branch_val = new ArrayList<String>();
        al_branch_flag = new ArrayList<String>();
        al_branch_id = new ArrayList<String>();

        check_master = new ArrayList<String>();
        master_flag = new ArrayList<String>();
        //callPopup();

        // ---set Custom Font to textview
//		inputEmail.setTypeface(font);
//
//		inputPassword.setTypeface(font);
//
//		btnLogin.setTypeface(font);
//		loginErrorMsg.setTypeface(font);

        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);

        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {

                email = inputEmail.getText().toString();
                password = inputPassword.getText().toString();
                // Check if Internet present
                TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);


                if (password.equals("") || email.equals("")) {

                    if (password.equals("")) {
                        error_pwd.setVisibility(View.VISIBLE);
                        error_pwd.setText("Please enter a password");

                    } else {
                        error_pwd.setVisibility(View.GONE);

                    }
                    if (email.equals("")) {
                        error_email.setVisibility(View.VISIBLE);
                        error_email.setText("Please enter a Employee Id");
                    } else {
                        error_email.setVisibility(View.GONE);

                    }
                } else {


                    if (cd.isConnectingToInternet()) {

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);




                            dbl_latitude = tracker.getLatitude();
                            dbl_longitude = tracker.getLongitude();
                            lattitude=String.valueOf(dbl_latitude);
                            longitude=String.valueOf(dbl_longitude);


                        new Login().execute();

                    } else {
                        DatabaseHandler db = new DatabaseHandler(
                                getApplicationContext());
                        Integer tnt_getcount = db.getRowCount();
                        System.out.println("str_db_username:::" + tnt_getcount);

                        if (tnt_getcount > 0) {
                            HashMap<String, String> user_details = db.getUserDetails();
                            String str_db_username = user_details.get("uid");
                            String str_db_password = user_details.get("password");


                            if (str_db_username.equals(email)) {
                                if (str_db_password.equals(password)) {
                                    Editor editor = pref.edit();
                                    if (rememberMe.isChecked()) {
                                        editor.putString("user_username", email);
                                        editor.putString("user_password", password);
                                    } else {
                                        editor.putString("user_username", "");
                                        editor.putString("user_password", "");
                                    }
                                    editor.putInt("is_login", 1);
                                    editor.commit();

                                    Intent messagingActivity = new Intent(
                                            LoginActivity.this, HdbHome.class);
                                    startActivity(messagingActivity);
                                    finish();

                                } else {
                                    userFunction.cutomToast(
                                            getResources().getString(R.string.password_not_match),
                                            getApplicationContext());
                                }
                            } else {
                                userFunction.cutomToast(
                                        getResources().getString(R.string.userid_not_match),
                                        getApplicationContext());
                            }
                        } else {
                            userFunction.noInternetConnection(
                                    getResources().getString(R.string.no_internet),
                                    getApplicationContext());
                        }
                    }

                }

            }
        });

    }

    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());
                }

            }
        });
    }

    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work


            try {
                String deviceName = Build.MODEL;
                String deviceMan = Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, str_imei,
                        lattitude, longitude, deviceName, deviceMan, device_id,imei1,imei2);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {


                         str_brach_count=rDB.getbranchCount();
                         Integer  int_branch_count=0;
                         int_branch_count=(Integer.parseInt(str_brach_count));

                         System.out.println("int_branch_count::::"+int_branch_count);
                         if(int_branch_count==0){
                             new Download_bracnh_Masters().execute();
                         }else{
                             new Download_bracnh_Masters().execute();

                         }
                            // user successfully logged in'
                            // Store user details in SQLite Database
                            DatabaseHandler db = new DatabaseHandler(
                                    getApplicationContext());
                            JSONObject json_user = json.getJSONObject("data");

                            // Clear all previous data in database
                            userFunction
                                    .logoutUserclear(getApplicationContext());
                            String KEY_EMAIL = "email";
                            String KEY_NAME = "name";
                            String KEY_UID = "id";

                            db.addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID),
                                    str_imei,
                                    password);


                            SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd");
                            String current_date = sdf.format(new Date());

                            Editor editor = pref.edit();
                            editor.putString("current_date", current_date);


                            editor.putInt("is_login", 1);
                            editor.putString("user_id",
                                    json_user.getString(KEY_UID).toString());
                            editor.putString("user_name",
                                    json_user.getString(KEY_NAME).toString());
                            String KEY_BRANCH = "branch";
                            editor.putString("user_brnch",
                                    json_user.getString(KEY_BRANCH).toString());
                            editor.putString("user_email",
                                    json_user.getString(KEY_EMAIL).toString());
                            editor.putString("user_designation",
                                    json_user.getString("designation").toString());

                            editor.putString("avi_tel",
                                    json_user.getString("Avinash").toString());
                            editor.putString("sach_tel",
                                    json_user.getString("Sachin").toString());
                            editor.putString("vija_tel",
                                    json_user.getString("Vijay").toString());
                            editor.putString("service_tel",
                                    json_user.getString("Service").toString());

                            if (rememberMe.isChecked()) {
                                editor.putString("user_username", email);
                                editor.putString("user_password", password);
                            } else {
                                editor.putString("user_username", "");
                                editor.putString("user_password", "");
                            }
                            System.out.println("Inside:::" + json_user);
                            editor.commit();




                            System.out.println("MASTERS LOAD::::" + str_check_master);

                            if (str_check_master.equals("0")) {

                                System.out.println("MASTERS LOAD::::" + str_check_master);

                                new Download_Masters().execute();
                            } else {
                                System.out.println("MASTERS LOAD ELSE::::" + str_check_master);

                                master_flag = rDB.get_master_flag();
                                //System.out.println(" FLAG DC:::" + master_flag.get(0)+" FLAG PM::: "+ master_flag.get(2));

                                flag_pd = master_flag.get(0);
                                id_pd = master_flag.get(1);

                                new Verify_Masters().execute();

                            }
                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, HdbHome.class);
                            // startActivity(messagingActivity);
                            // finish();

                        } else {

                            loginErrorMsg.setText(json.getString("message")
                                    .toString());
                        }
                    }
                } else {
                    DatabaseHandler db = new DatabaseHandler(
                            getApplicationContext());
                    Integer tnt_getcount = db.getRowCount();

                    if (tnt_getcount > 0) {
                        HashMap<String, String> user_details = db.getUserDetails();
                        String str_db_username = user_details.get("uid");
                        String str_db_password = user_details.get("password");

                        if (str_db_username.equals(email)) {
                            if (str_db_password.equals(password)) {
                                Editor editor = pref.edit();
                                if (rememberMe.isChecked()) {
                                    editor.putString("user_username", email);
                                    editor.putString("user_password", password);
                                } else {
                                    editor.putString("user_username", "");
                                    editor.putString("user_password", "");
                                }
                                editor.commit();


                                Intent messagingActivity = new Intent(
                                        LoginActivity.this, HdbHome.class);
                                startActivity(messagingActivity);
                                finish();
                            } else {
                                userFunction.cutomToast(
                                        getResources().getString(R.string.password_not_match),
                                        getApplicationContext());
                            }
                        } else {
                            userFunction.cutomToast(
                                    getResources().getString(R.string.userid_not_match),
                                    getApplicationContext());
                        }
                    } else {
                        userFunction.noInternetConnection(
                                getResources().getString(R.string.no_internet),
                                getApplicationContext());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb_progress.setVisibility(View.GONE);

        }

    }

    public String getIMEI(Context context) {

        TelephonyManager mngr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();

    }



    public void getRegId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
//						GCMRegistrar.register(LoginActivity.this,
//								GCMIntentService.SENDER_ID);
//						reg_id = GCMRegistrar
//								.getRegistrationId(LoginActivity.this);

                        System.out.println("REG ID:::" + reg_id);
                    }
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                // reg_id;
            }
        }.execute(null, null, null);
    }

    public void go_to_faq(View v) {
        Intent faqclass = new Intent(
                LoginActivity.this, FAQ.class);
        faqclass.putExtra("int_sreen_val", 1);
        startActivity(faqclass);
    }




    public class Download_bracnh_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_branch_masters(str_brach_count,email);

                System.out.println("jsonobject::::"+jsonobject);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("data");

                            jsonarray = jobj.getJSONArray("branch_master");

                            if (jsonarray != null) {

                                for (int i = 0; i < jsonarray.length(); i++) {
                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_branch.add(jsonobject.getString("BranchID"));
                                    al_branch_val.add(jsonobject.getString("BRANCH_NAME"));
                                    al_branch_flag.add(jsonobject.getString("BranchID"));
                                    al_branch_id.add(jsonobject.getString("BranchID"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                   // mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                        if (jsonarray != null) {

                            rDB.delete_branch_table();

                            for (int i = 0; i < al_branch.size(); i++) {

                                System.out.println("AL PD:::" + al_branch.get(i));
                                System.out.println("AL PD:::" + al_branch_val.get(i));
                                System.out.println("AL PD:::" + al_branch_flag.get(i));
                                System.out.println("AL PD:::" + al_branch_id.get(i));

                                rDB.insert_branch(al_branch.get(i), al_branch_val.get(i), al_branch_flag.get(i), al_branch_id.get(i));

                            }
                        }

                    } else {
                        mProgressDialog.dismiss();

                        loginErrorMsg.setText(jsonobject.getString("message").toString()
                                .toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_masters(null);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("data2");

                            jsonarray = jobj.getJSONArray("product");

                            if (jsonarray != null) {

                                for (int i = 0; i < jsonarray.length(); i++) {
                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                        if (jsonarray != null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                System.out.println("AL PD:::" + al_product.get(i));
                                System.out.println("AL PD:::" + al_prod_val.get(i));
                                System.out.println("AL PD:::" + al_prod_flag.get(i));
                                System.out.println("AL PD:::" + al_prod_id.get(i));

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }

                            mProgressDialog.dismiss();
                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, HdbHome.class);
                            startActivity(messagingActivity);
                            finish();
                        }

                    } else {
                        mProgressDialog.dismiss();
                        //pd_details_layout.setVisibility(View.GONE);
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        loginErrorMsg.setText(jsonobject.getString("message").toString()
                                .toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.check_masters("1", "2", "3", "4", flag_pd, id_pd);

                System.out.println("JSONOBJ::::" + jsonobject);

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_VM) != null) {
                        res = jsonobject.getString(KEY_STATUS_VM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_VM);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            if (!jsonobject.isNull("data")) {
                                JSONObject jobj2 = jsonobject.getJSONObject("data");
                                jsonarray = jobj2.getJSONArray("data2");
                            }

                            if (jsonarray != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray);


                        if (jsonarray != null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }
                        }

                        mProgressDialog.dismiss();

                        Intent messagingActivity = new Intent(
                                LoginActivity.this, HdbHome.class);
                        startActivity(messagingActivity);
                        finish();


                    } else {
                        mProgressDialog.dismiss();
                        //pd_details_layout.setVisibility(View.GONE);
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        //pd_details_layout.setVisibility(View.GONE);
                        loginErrorMsg.setText(jsonobject.getString("message").toString()
                                .toString());
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    private void callPopup() {


        // custom dialog
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.pd_update_popup);
        dialog.setTitle("New Version is Available....");

        Button btn_redirect = (Button) dialog.findViewById(R.id.btn_redirect_to_new_version);

        // if button is clicked, close the custom dialog
        btn_redirect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://www.google.com");

                Intent redirct = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(redirct);
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }



}
