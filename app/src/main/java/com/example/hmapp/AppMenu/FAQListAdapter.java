package com.example.hmapp.AppMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.hmapp.hdbLibrary.UserFunctions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 01-10-2015.
 */
public class FAQListAdapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunctions user_function;
    Uri myUri;
    MediaPlayer  mediaPlayer;
    public FAQListAdapter(Context context,  ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
        mediaPlayer = new MediaPlayer();

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.pd_my_faq_list_item, parent,
                    false);

            holder = new ViewHolder();
// --            holder.tv_faq_ans= new ViewHolder();
            holder.tv_faq_quest = (TextView) itemView
                    .findViewById(R.id.txt_faq_quest);
            holder.tv_faq_ans = (TextView) itemView.findViewById(R.id.txt_faq_ans);

        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        holder.tv_faq_quest.setText(Html.fromHtml(resultp.get(FAQ.FAQ_question)));
        holder.tv_faq_ans.setText(Html.fromHtml(resultp.get(FAQ.FAQ_answer)));

        holder.songName = (TextView)itemView.findViewById(R.id.textView4);
        holder.startTimeField =(TextView)itemView.findViewById(R.id.textView1);
        holder.endTimeField =(TextView)itemView.findViewById(R.id.textView2);
        holder.seekbar = (SeekBar)itemView.findViewById(R.id.seekBar1);
        holder. playButton = (Button)itemView.findViewById(R.id.imageButton1);
        holder.pauseButton = (Button)itemView.findViewById(R.id.imageButton2);
        holder.songName.setText("song.mp3");
        //mediaPlayer = MediaPlayer.create(this, R.raw.song);


        holder. playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mediaPlayer.setDataSource("https://hdbapp.hdbfs.com/Recording/Voice_001.mp3");
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
        holder.seekbar.setClickable(false);
        holder.pauseButton.setEnabled(false);

/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}
*/
        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_faq_quest;
        public TextView startTimeField;
        public TextView endTimeField;

        public TextView songName;
        public TextView tv_faq_ans;
        public SeekBar seekbar;
        public Button playButton;
        public Button pauseButton;



    }


}