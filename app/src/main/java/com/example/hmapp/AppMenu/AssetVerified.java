package com.example.hmapp.AppMenu;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MainActivity_OLD;
import com.example.hmapp.hdbLibrary.AppLocationService;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;


/*
 * Created by Avinash on 21-08-2015.
 */
public class AssetVerified extends AppCompatActivity implements AdapterView.OnItemSelectedListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    TableRow tbl_asset_type, tbl_vehicle_no1, tbl_vehicle_no2;

    String str_distance_type;

    Spinner spn_coll_verified_at;
    EditText edt_coll_person_name, edt_vehicle_no, edt_reg_no;
    EditText edt_coll_verified_with, edt_asset_customer_name;
    String str_coll_verified_with;
    EditText edt_appform_no;
    EditText edt_losid;
    ArrayAdapter<String> adapter_coll_verified_at;
    ArrayAdapter<String> adapter_product;
    ArrayAdapter<String> adapter_distance_type;
    ArrayAdapter<String> adapter_asset_type;
    String[] coll_verified_at = {"Select", "Residence", "Office", "Plant", "Factory Site", "Other"};
    String str_coll_verified_at;
    TextView location_lat, location_long;

    String str_losid = null;
    String str_form_no;
    protected JSONObject jsonobject;
    private ConnectionDetector cd;
    SharedPreferences pref;
    SharedPreferences pData;
    public static final String KEY_STATUS = "status";
    ProgressDialog mProgressDialog;
    RequestDB rdb;
    UserFunctions userFunction;
    AppLocationService appLocationService;
    String latitude, longitude, ts;
    String str_user_id;
    String str_pd_uploaded_by;
    ArrayAdapter<String> adapter_contact_person;
    String str_contact_person;
    String[] contact_person = {"Select", "Applicant", "Co applicant", "Guarantor", "Director", "Partner", "Key Person", "Immediate Family Member", "Other"};

    String[] asset_type = {"Select", "New", "Used", "ReFinance"};
    int map_captured = 0;
    String[] distance_type = {"Select", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    Spinner spn_contact_person;
    String asset_ver;
    Spinner spn_product, spn_asset_type, spn_distance_from_loc;
    String str_product, str_asset_type, date ,str_branch;
    Spinner spn_branch;

    String getStr_Vehicle_reg_no, str_vehicle_no, str_reg_no;
    EditText txt_distacne;
    EditText edt_remarks;
    String str_remarks;

    ExampleDBHelper dbHelper;

    SharedPreferences.Editor peditor;
    String str_coll_person_name;
    String str_coll_customer_name;
    String str_txt_distacne;

    ArrayList<String> proudct_type;
    ArrayList<String> producttype_val;
    ArrayList<String> branch_list;

    double dbl_latitude = 0;
    double dbl_longitude = 0;
    String map_address = null;


    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;

    static  Double lat;
    static  Double lon;
    public boolean isLocationEnabled = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);

        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("ASSET PD");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

        dbHelper = new ExampleDBHelper(this);

        proudct_type = new ArrayList<>();
        producttype_val = new ArrayList<>();
        branch_list= new ArrayList<>();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) AssetVerified.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        checkLocation();

        SetLayout();

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }


    private void SetLayout() {

        ts = getDateTime();
        date = getDateTime1();

        setContentView(R.layout.pd_asset_to_verified);


        Intent asset = getIntent();
        asset_ver = asset.getStringExtra("asset_verify");

        rdb = new RequestDB(getApplicationContext());

        userFunction = new UserFunctions();
        userFunction.resetData(AssetVerified.this);
        appLocationService = new AppLocationService(AssetVerified.this);
        cd = new ConnectionDetector(AssetVerified.this);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        pData = getApplicationContext().getSharedPreferences("pData", 0);

        str_user_id = pref.getString("user_id", null);

        Intent i = getIntent();
        str_pd_uploaded_by = i.getStringExtra("pd_upload_by");
        str_losid = i.getStringExtra("pd_losid");
        str_form_no = i.getStringExtra("pd_form_no");
        str_product = i.getStringExtra("product_type");
        str_asset_type = i.getStringExtra("asset_type");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("map_img_name", null);
        peditor.putInt("is_map_captured", 0);
        peditor.commit();


        tbl_asset_type = (TableRow) findViewById(R.id.tbl_asset_type);
        tbl_vehicle_no1 = (TableRow) findViewById(R.id.tbl_vehicle_no_1);
        tbl_vehicle_no2 = (TableRow) findViewById(R.id.tbl_vehicle_no_2);


        spn_coll_verified_at = (Spinner) findViewById(R.id.spn_coll_address_type);

        spn_product = (Spinner) findViewById(R.id.spn_product);
        spn_asset_type = (Spinner) findViewById(R.id.spn_asset_type);
        spn_distance_from_loc = (Spinner) findViewById(R.id.spn_distance_from_loc);
        spn_branch = (Spinner) findViewById(R.id.spn_branch);

        edt_coll_person_name = (EditText) findViewById(R.id.edt_coll_person_name);
        edt_coll_verified_with = (EditText) findViewById(R.id.edt_coll_verified_with);
        edt_vehicle_no = (EditText) findViewById(R.id.edt_vehicle_no);
        edt_reg_no = (EditText) findViewById(R.id.edt_reg_no);
        edt_asset_customer_name = (EditText) findViewById(R.id.edt_asset_customer_name);
        txt_distacne = (EditText) findViewById(R.id.edt_distance);

        edt_remarks = (EditText) findViewById(R.id.edt_remarks);
        location_lat = (TextView) findViewById(R.id.location_lat);
        location_long = (TextView) findViewById(R.id.location_long);



        proudct_type = rdb.get_masters_pd();
        producttype_val = rdb.get_masters_pd_val();
        branch_list = rdb.get_masters_branch();



        Button btn_locate_me = (Button) findViewById(R.id.btn_locate_me);
        // setImages();

        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), AssetVerified.this);
                }

                str_losid = edt_losid.getText().toString();
                str_form_no = edt_appform_no.getText().toString();

                if (str_form_no.equals("") && str_losid.equals("")) {
                    missingLOSID();
                } else {
                    map_captured = pref.getInt("is_map_captured", 0);

                    if (map_captured == 0) {
                        Intent img_pinch = new Intent(
                                AssetVerified.this, MapsActivity.class);
                        img_pinch.putExtra("str_losid_map", str_losid);
                        img_pinch.putExtra("str_app_form_no_map", str_form_no);
                        img_pinch.putExtra("str_from_act", "CO");
                        img_pinch.putExtra("str_ts", ts);

                        startActivity(img_pinch);
                    } else {
                        userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                                getApplicationContext());

                    }
                }

            }
        });


        spn_contact_person = (Spinner) findViewById(R.id.spn_coll_person_type);

        adapter_coll_verified_at = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, coll_verified_at);
        adapter_coll_verified_at
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_coll_verified_at.setAdapter(adapter_coll_verified_at);
        spn_coll_verified_at.setOnItemSelectedListener(this);
/*
        adapter_product = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, product);
        adapter_product
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_product.setAdapter(adapter_product);
        spn_product.setOnItemSelectedListener(this);
*/
        ArrayAdapter<String> adapter_state_p = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, proudct_type);
        adapter_state_p
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_product.setAdapter(adapter_state_p);
        spn_product.setOnItemSelectedListener(this);


        ArrayAdapter<String> adapter_state_b = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, branch_list);
        adapter_state_p
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_branch.setAdapter(adapter_state_b);
        spn_branch.setOnItemSelectedListener(this);


        adapter_asset_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, asset_type);
        adapter_asset_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_asset_type.setAdapter(adapter_asset_type);
        spn_asset_type.setOnItemSelectedListener(this);


        adapter_contact_person = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, contact_person);
        adapter_contact_person
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_contact_person.setAdapter(adapter_contact_person);
        spn_contact_person.setOnItemSelectedListener(this);

        //       loadSpinnerData("Master_contact_prsn",spn_contact_person);

        adapter_distance_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, distance_type);
        adapter_distance_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);


        edt_appform_no = (EditText) findViewById(R.id.edt_appform_no);
        edt_losid = (EditText) findViewById(R.id.edt_losid);


        edt_appform_no.setText(str_form_no);
        edt_losid.setText(str_losid);


        if (str_losid != null) {
            edt_losid.setEnabled(false);
            edt_appform_no.setEnabled(false);

        }
        if (str_form_no != null) {
            edt_losid.setEnabled(false);
            edt_appform_no.setEnabled(false);

        }


        if (str_asset_type != null) {
            spn_asset_type.setSelection(getIndex(spn_asset_type, str_asset_type));
            // spn_asset_type.setEnabled(false);
        }


        Button btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isLocationEnabled = checkLocation();


                str_coll_person_name = edt_coll_person_name.getText().toString();
                str_coll_verified_with = edt_coll_verified_with.getText().toString();
                str_txt_distacne = txt_distacne.getText().toString();
                str_remarks = edt_remarks.getText().toString();
                str_losid = edt_losid.getText().toString();
                str_form_no = edt_appform_no.getText().toString();
                str_reg_no = edt_reg_no.getText().toString().replace(" ", "");
                str_vehicle_no = edt_vehicle_no.getText().toString().replace(" ", "");
                str_coll_customer_name = edt_asset_customer_name.getText().toString();

                boolean ins = true;

                try {
                    if (str_losid.isEmpty() && str_form_no.isEmpty()&& ins == true) {
                        userFunction.cutomToast("Please Enter Address ",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (str_product == null && ins == true) {
                        userFunction.cutomToast("Please Select Product Type",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_branch == null && ins == true) {
                        userFunction.cutomToast("Please Select Branch ",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (edt_coll_verified_with == null || str_coll_verified_with.isEmpty() || str_coll_verified_with.equals("null")&& ins == true) {
                        userFunction.cutomToast("Please Enter Address ",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_coll_person_name == null || str_coll_person_name.isEmpty() || str_coll_person_name.equals("null")&& ins == true) {
                        userFunction.cutomToast("Please Enter Contact Person  Name",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (str_coll_customer_name == null || str_coll_customer_name.isEmpty() || str_coll_customer_name.equals("null")) {
                        userFunction.cutomToast("Please Enter Customer  Name",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (str_remarks.isEmpty() && str_remarks.isEmpty()&& ins == true) {
                        userFunction.cutomToast("Please Enter Remarks ",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_remarks.length() < 10 &&
                            ins == true) {
                        userFunction.cutomToast("Please Enter Atleast 10 characters in Remarks",
                                AssetVerified.this);
                        ins = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!str_vehicle_no.trim().equals(str_reg_no.trim())) {
                        userFunction.cutomToast("Vehicle registration No does not match with above Reg No.",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_coll_verified_at == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Visit Person ",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_coll_verified_at == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Visit Type",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_distance_type == null && ins == true) {
                        userFunction.cutomToast("Please Select Locate-me Distance",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
/*
                Integer int_is_locate_clicked = pref.getInt("is_locate_clicked", 0);
                try {

                    if (int_is_locate_clicked ==0) {
                        userFunction.cutomToast("select Map",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
*/
                try {

                    if (str_txt_distacne == null || str_txt_distacne.isEmpty() || str_txt_distacne.equals("null")) {
                        userFunction.cutomToast("Please Insert Distance",
                                AssetVerified.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

              //  String map_latitude = pref.getString("pref_latitude", "0");
               // String map_longitude = pref.getString("pref_longitude", "0");
                map_address = pref.getString("map_address", null);
                 map_captured = pref.getInt("is_map_captured", 0);


                System.out.println("map_captured::::" + map_captured + "::::str_distance_type::::" + str_distance_type + "::::str_txt_distacne:::" + str_txt_distacne);
                try {
                    if (map_captured == 1 && ins == true) {
                        if (str_distance_type.equals("Map not loading") == true) {
                            if (dbl_latitude>0) {
                                userFunction.cutomToast("You already captured map... select other Distance type..",
                                        AssetVerified.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (map_captured == 1 && ins == true) {
                        if ((str_distance_type.equals("Exact location")) || ((str_distance_type.equals("Map not loading")))) {
                            ins = true;
                        }else{
                            if (Integer.parseInt(str_txt_distacne)==0) {
                                userFunction.cutomToast("Distance cant be a Zero...",
                                        AssetVerified.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                String img_new_path = pref.getString("map_img_name", null);

                if (ins == true) {

                  if(isLocationEnabled==true) {
                      insertDataSqlite();
                  }else{
                      checkLocation();
                  }
                }


            }

        });


        Button btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
            }

        });


    }


    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_coll_address_type:
                str_coll_verified_at = coll_verified_at[position];
                if (position == 0) {
                    str_coll_verified_at = null;
                }
                break;
            case R.id.spn_coll_person_type:
                str_contact_person = contact_person[position];
                if (position == 0) {
                    str_contact_person = null;

                }
                break;
            case R.id.spn_branch:
                str_branch = branch_list.get(position).toString();
                if (position == 0) {
                    str_branch = null;

                }
                break;

            case R.id.spn_product:
                str_product = producttype_val.get(position).toString();


                if (str_product.equals("Select")) {
                    str_product = null;
                }

                if (str_product.equals("UCL") || str_product.equals("TRL") || str_product.equals("TW") || str_product.equals("CVEH")) {

                    tbl_asset_type.setVisibility(View.VISIBLE);
                    tbl_vehicle_no1.setVisibility(View.VISIBLE);
                    tbl_vehicle_no2.setVisibility(View.VISIBLE);
                } else {
                    tbl_asset_type.setVisibility(View.GONE);
                    tbl_vehicle_no1.setVisibility(View.GONE);
                    tbl_vehicle_no2.setVisibility(View.GONE);
                }

                break;

            case R.id.spn_asset_type:
                str_asset_type = asset_type[position];
                if (position == 0) {
                    str_asset_type = null;
                }
                if (position == 1) {
                    tbl_vehicle_no1.setVisibility(View.GONE);
                    tbl_vehicle_no2.setVisibility(View.GONE);
                } else {
                    tbl_vehicle_no1.setVisibility(View.VISIBLE);
                    tbl_vehicle_no2.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    private void cleanUpCancel() {

        spn_coll_verified_at.setSelection(0);
        spn_product.setSelection(0);
        spn_asset_type.setSelection(0);
        spn_contact_person.setSelection(0);
        spn_distance_from_loc.setSelection(0);
        edt_coll_person_name.setText(null);
        edt_coll_verified_with.setText(null);
        edt_vehicle_no.setText(null);
        edt_reg_no.setText(null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cleanUpCancel();
    }


    private void missingLOSID() {
        userFunction.cutomToast("Please Enter The App Form No Or LOSID",
                getApplicationContext());
    }

    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String format = s.format(new Date());
        return format;
    }

    public void insertDataSqlite() {
        long newRowId = 0;
        System.out.println("map_address:::::" + map_address);
        newRowId = rdb.insertAssetInfo(str_losid, str_form_no,str_branch, str_contact_person,
                str_coll_person_name, str_coll_verified_at, str_coll_verified_with,
                str_user_id, latitude, longitude, ts, str_product, str_asset_type, str_vehicle_no, str_distance_type, str_coll_customer_name, "0", str_txt_distacne, str_remarks, map_address);

        String map_new_path = pref.getString("map_img_name", null);


        if (map_new_path != null ) {
            System.out.println("map_new_path::::"+map_new_path);

                dbHelper.insertImage(map_new_path, str_losid, str_form_no, str_user_id, ts, "", "", "0", "MAP");
        }

        if (str_losid == null || str_losid.isEmpty() || str_losid.equals("null")) {
            str_losid = null;
        }

        if (str_form_no == null || str_form_no.isEmpty() || str_form_no.equals("null")) {
            str_form_no = null;
        }

        if (newRowId > 0) {
            //------
            map_captured = pref.getInt("is_map_captured", 0);
            String map_coll = Integer.toString(map_captured);
            Intent asset_img = new Intent(AssetVerified.this, MainActivity_OLD.class);
            asset_img.putExtra("coll_losid", str_losid);
            asset_img.putExtra("coll_formno", str_form_no);
            asset_img.putExtra("coll_userid", str_user_id);
            asset_img.putExtra("coll_ts", ts);
            asset_img.putExtra("coll_map", map_coll);
            asset_img.putExtra("coll_last", newRowId);
            asset_img.putExtra("id", "CO");
            asset_img.putExtra("lat", latitude);
            asset_img.putExtra("long", longitude);
            asset_img.putExtra("ts_date", date);
            startActivity(asset_img);
            finish();

        }

    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

//Uri

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }

    private void loadSpinnerData(String Table_name, Spinner mp_spinner) {
        ExampleDBHelper db = new ExampleDBHelper(getApplicationContext());
        List<String> labels = db.getAllLabels(Table_name);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mp_spinner.setAdapter(dataAdapter);
    }

    boolean startWithZeros(String str,Integer count_ofzeros){
        StringBuilder sb=new StringBuilder();
        for(Integer i=0;i<count_ofzeros;i++)
            sb.append("0");
        return str.startsWith(sb.toString());
    }


    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(getApplicationContext(), "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        lon = location.getLongitude();
        location_lat.setText("" + lat);
        location_long.setText("" + lon);
        latitude = String.valueOf(lat);
        longitude = String.valueOf(lon);
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new  AlertDialog.Builder (AssetVerified.this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}

