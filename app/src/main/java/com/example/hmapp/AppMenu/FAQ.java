package com.example.hmapp.AppMenu;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 01-10-2015.
 */
public class FAQ extends AppCompatActivity {
    ListView mList;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;
    FAQListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    TextView txt_error;
    ProgressBar progressbar_loading;

    static String FAQ_question = "FAQ_quest";
    static String FAQ_answer = "FAQ_ans";
    EditText edt_faq_search;
    Button btn_search;
    int int_from_act;
    String str_keyword;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq);
        userFunction = new UserFunctions();
        cd = new ConnectionDetector(FAQ.this);
        set_layout();

        if (cd.isConnectingToInternet()) {
            new DownloadFAQ().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("FAQ");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);


        edt_faq_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }
                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    new DownloadFAQ().execute();
                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    public void set_layout() {
        Intent i = getIntent();
        int_from_act = i.getIntExtra("int_sreen_val", 0);

        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_faq_search = (EditText) findViewById(R.id.edt_faq_search);
        btn_search = (Button) findViewById(R.id.btn_search);
        mList = (ListView) findViewById(R.id.list);
        progressbar_loading=(ProgressBar)findViewById(R.id.progressbar_loading);

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                Middle_Screen.class);
        startActivity(home_activity);

    }


    public class DownloadFAQ extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            progressbar_loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            if (int_from_act == 1) {
                jsonobject = userFunction.getFAQList("1",str_keyword);
            } else {
                jsonobject = userFunction.getFAQList("2",str_keyword);
            }
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");

                arraylist = new ArrayList<>();

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("FAQ_quest", jsonobject.getString("FAQ_quest"));
                    map.put("FAQ_ans", jsonobject.getString("FAQ_ans"));
                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                adapter = new FAQListAdapter(FAQ.this, arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

            }
            progressbar_loading.setVisibility(View.GONE);

        }

    }

}
