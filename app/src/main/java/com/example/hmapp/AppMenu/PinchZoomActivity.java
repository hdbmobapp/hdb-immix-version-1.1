package com.example.hmapp.AppMenu;

/**
 * Created by Avinash on 07-09-2015.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import com.example.hdb.R;
import com.example.hmapp.hdbLibrary.TouchImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PinchZoomActivity extends AppCompatActivity {
    ImageLoader imageLoader;
    public DisplayImageOptions img_options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_pinch_zoom);
        img_options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        imageLoader = ImageLoader.getInstance();

        Intent i = getIntent();
        String str_image_path = i.getStringExtra("image_path");

        System.out.println("str_image_path::::" + str_image_path);
        TouchImageView mImageView;
        mImageView = (TouchImageView) findViewById(R.id.img);
        // imageLoader.displayImage("file:///" + str_image_path, mImageView);
        imageLoader.displayImage(str_image_path, mImageView);

        imageLoader.displayImage(str_image_path,
                mImageView, img_options);
    }

}