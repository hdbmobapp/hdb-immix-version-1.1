package com.example.hmapp.AppMenu;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.view.LayoutInflater;

import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Administrator on 04-09-2015.
 */
public class Middle_Screen extends AppCompatActivity {

    RadioButton rb_new, rb_existing;
    EditText edt_search;
    Button btn_submit;

    TextView txt_error;
    String str_losidno;
    String str_user_id;

    ConnectionDetector cd;
    UserFunctions userFunction = new UserFunctions();

    MyCasesListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;

    String str_pd_status;
    String str_losid;
    String str_app_form_no;
    String str_visit_mode;
    String str_coll_verified;
    String str_contact_person_name;
    String str_contact_person;
    String str_upload_date;

    LinearLayout pd_details_layout;
    String str_message;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    LinearLayout lin_new_pd;
    PopupWindow popupWindow;
ListView mList;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_middle_screen);
        boolean checked;
        cd = new ConnectionDetector(Middle_Screen.this);

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("SEARCH");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

        setlayout();


    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_search = (EditText) findViewById(R.id.edt_search);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        pd_details_layout = (LinearLayout) findViewById(R.id.pd_details_layout);

        mList = (ListView) findViewById(R.id.list);

        lin_new_pd = (LinearLayout) findViewById(R.id.lin_new_pd);
        lin_new_pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPopup();
            }

        });



        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  str_user_id=null;
                str_losidno = edt_search.getText().toString();

                if (str_losidno != null) {
                    if (cd.isConnectingToInternet()) {

                        new DownloadJSON().execute();
                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Please Select The Form No",
                            getApplicationContext());
                }

            }

        });

    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Middle_Screen.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            jsonobject = userFunction.getSingleCase(str_user_id, str_losidno);
            try {

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj=jsonobject.getJSONObject("Data");
                            jsonarray = jobj.getJSONArray("Data");
                            arraylist = new ArrayList<HashMap<String, String>>();

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);
                                map.put("LOSID", jsonobject.getString("LOSID"));
                                map.put("PD_Status",
                                        jsonobject.getString("PD_Status"));
                                map.put("Visited_At", jsonobject.getString("Visited_At"));
                                map.put("CU_PIC_COUNT", jsonobject.getString("CU_PIC_COUNT"));
                                map.put("CO_PIC_COUNT", jsonobject.getString("CO_PIC_COUNT"));
                                map.put("PF_PIC_COUNT", jsonobject.getString("PF_PIC_COUNT"));
                                map.put("Form_No", jsonobject.getString("Form_No"));
                                map.put("Product_type", jsonobject.getString("Product_type"));
                                map.put("Asset_type", jsonobject.getString("Asset_type"));
                                map.put("NEW_PIC_COUNT", jsonobject.getString("NEW_PIC_COUNT"));

                                // Set the JSON Objects into the array
                                arraylist.add(map);
                            }
                        } else {
                            str_message = "No such PD entry found Add New Entry for this LOSID/App Form No";
                        }
                    }
                } else {

                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
           TextView txt_message=(TextView)findViewById(R.id.txt_message);
            if (jsonobject == null) {


                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                //         txt_error.setText(getResources().getString(R.string.error_message));
                //          txt_error.setVisibility(View.VISIBLE);

            } else {
                pd_details_layout.setVisibility(View.VISIBLE);

                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    pd_details_layout.setVisibility(View.VISIBLE);
                    lin_new_pd.setVisibility(View.GONE);
                    txt_message.setVisibility(View.GONE);
                    adapter = new MyCasesListAdapter(Middle_Screen.this, arraylist);
                    adapter.notifyDataSetChanged();
                    mList.setAdapter(adapter);
                } else {
                    pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }



            }
            mProgressDialog.dismiss();

        }

    }



    private void callPopup() {


        // custom dialog
        final Dialog dialog = new Dialog(Middle_Screen.this);
        dialog.setContentView(R.layout.pd_popup);
        dialog.setTitle("Select Your Choice...");

        Button btn_exe_pd = (Button) dialog.findViewById(R.id.btn_exe_pd);
        Button btn_asst_verif = (Button) dialog.findViewById(R.id.btn_ass_verification);

        // if button is clicked, close the custom dialog
        btn_exe_pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pd_form = new Intent(Middle_Screen.this, ExistingPD.class);
                startActivity(pd_form);
                dialog.dismiss();
            }
        });
        btn_asst_verif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pd_form = new Intent(Middle_Screen.this, AssetVerified.class);
                startActivity(pd_form);
                dialog.dismiss();
            }
        });
        dialog.show();



    }
    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    }


