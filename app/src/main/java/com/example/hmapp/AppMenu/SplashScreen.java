package com.example.hmapp.AppMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.DatabaseHandler;
import com.example.hmapp.hdbLibrary.GPSTracker;
import com.example.hmapp.hdbLibrary.LoanService;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class SplashScreen extends AppCompatActivity {
    SharedPreferences pref;
    RequestDB rDB;
    ExampleDBHelper eDB;
    UserFunctions userfunction;
    DatabaseHandler db;
    ProgressDialog mProgressDialog;

    UserFunctions userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    String str_user_id;
    public static String KEY_STATUS = "status";

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";
    ArrayList<String> al_product, al_prod_val, al_prod_flag, al_prod_id;
    ArrayList<String> al_branch, al_branch_val, al_branch_flag, al_branch_id;
    JSONObject jsonobject;
    String res, resp_success;
    ArrayList<String> check_master;
    ArrayList<String> master_flag;
    JSONArray jsonarray,jsonarray1,jsonarray2;

    String flag_pd, id_pd;
    String str_check_master;
    String str_brach_count;

    //String fisrt_run = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_splashscreen);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        userfunction = new UserFunctions();
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
// get IMEI


        rDB = new RequestDB(this);
        eDB = new ExampleDBHelper(this);
        db = new DatabaseHandler(SplashScreen.this);

        al_product = new ArrayList<String>();
        al_prod_val = new ArrayList<String>();
        al_prod_flag = new ArrayList<String>();
        al_prod_id = new ArrayList<String>();

        al_branch = new ArrayList<String>();
        al_branch_val = new ArrayList<String>();
        al_branch_flag = new ArrayList<String>();
        al_branch_id = new ArrayList<String>();

        check_master = new ArrayList<String>();
        master_flag = new ArrayList<String>();

        rDB.delete_pd();
        eDB.delete_pd();

        checkforLogout();


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        rDB = new RequestDB(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        //str_user_name = pref.getString("user_name", null);
        //str_branch = pref.getString("user_brnch", null);
        //str_designation = pref.getString("user_designation", "NO");
        //is_login = pref.getInt("is_login", 0);


        str_brach_count = rDB.getbranchCount();
        Integer int_branch_count = 0;
        int_branch_count = (Integer.parseInt(str_brach_count));

        System.out.println("int_branch_count::::" + int_branch_count);
        if (int_branch_count == 0) {
            new Download_bracnh_Masters().execute();
        } else {
            new Download_bracnh_Masters().execute();

        }

        check_master = new ArrayList<String>();
        check_master = rDB.get_masters();
        System.out.println("MASTERS COUNT::::" + check_master);
        str_check_master = check_master.get(0);

        if (str_check_master.equals("0")) {

            System.out.println("MASTERS LOAD::::" + str_check_master);

            new Download_Masters().execute();
        } else {
            System.out.println("MASTERS LOAD ELSE::::" + str_check_master);

            master_flag = rDB.get_master_flag();
            //System.out.println(" FLAG DC:::" + master_flag.get(0)+" FLAG PM::: "+ master_flag.get(2));

            flag_pd = master_flag.get(0);
            id_pd = master_flag.get(1);

            new Verify_Masters().execute();

        }

/*
        int SPLASH_SCREEN_TIME = 2000;

        new Handler().postDelayed(new Runnable() {
            SharedPreferences settings = getSharedPreferences("prefs", 0);
            boolean firstRun = settings.getBoolean("firstRun", true);

            @Override
            public void run() {
                // This is method will be executed when SPLASH_SCREEN_TIME is
                // over, Now you can call your Home Screen

                if (firstRun) {
                    // here run your first-time instructions, for example :
                    // startActivityForResult( new Intent(SplashScreen.this,
                    // InstructionsActivity.class),INSTRUCTIONS_CODE);
                    Intent iHomeScreen = new Intent(SplashScreen.this,
                            HdbHome.class);
                   // startActivity(iHomeScreen);
                } else {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    Intent i = new Intent(SplashScreen.this, HdbHome.class);
                   // startActivity(i);
                }

                // Finish Current Splash Screen, as it should be visible only
                // once when application start
               // finish();
            }
        }, SPLASH_SCREEN_TIME);
        */
    }

    public Void checkforLogout() {
        String str_last_date = pref.getString("current_date", null);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = sdf.format(new Date());

        Date date1 = null;
        Date date2 = null;
        try {
            if (current_date != null) {
                date1 = sdf.parse(current_date);
            }
            if (str_last_date != null) {
                date2 = sdf.parse(str_last_date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (current_date != null && str_last_date != null) {
            if (date1.after(date2)) {
                userfunction.logoutUser(SplashScreen.this);
                //db.resetTables();

                System.out.println("Date1 is after Date2");
                Editor editor = pref.edit();
                editor.putInt("is_login", 0);
                editor.commit();

            }
        }
        return null;
    }


    public class Download_bracnh_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(SplashScreen.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_branch_masters("", str_user_id);

                System.out.println("jsonobject::::" + jsonobject);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("data");

                            jsonarray1 = jobj.getJSONArray("branch_master");

                            if (jsonarray1 != null) {

                                for (int i = 0; i < jsonarray1.length(); i++) {
                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_branch.add(jsonobject.getString("BranchID"));
                                    al_branch_val.add(jsonobject.getString("BRANCH_NAME"));
                                    al_branch_flag.add(jsonobject.getString("BranchID"));
                                    al_branch_id.add(jsonobject.getString("BranchID"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    // mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                        if (jsonarray1 != null) {

                            rDB.delete_branch_table();

                            for (int i = 0; i < al_branch.size(); i++) {

                                System.out.println("AL PD:::" + al_branch.get(i));
                                System.out.println("AL PD:::" + al_branch_val.get(i));
                                System.out.println("AL PD:::" + al_branch_flag.get(i));
                                System.out.println("AL PD:::" + al_branch_id.get(i));

                                rDB.insert_branch(al_branch.get(i), al_branch_val.get(i), al_branch_flag.get(i), al_branch_id.get(i));

                            }
                        }

                    } else {
                        //  mProgressDialog.dismiss();

                        // loginErrorMsg.setText(jsonobject.getString("message").toString()
                        //         .toString());
                        userFunction.cutomToast(jsonobject.getString("message").toString(), SplashScreen.this);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(SplashScreen.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_masters(null);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("data2");

                            jsonarray = jobj.getJSONArray("product");

                            if (jsonarray != null) {

                                for (int i = 0; i < jsonarray.length(); i++) {
                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    //  mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                        if (jsonarray != null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                System.out.println("AL PD:::" + al_product.get(i));
                                System.out.println("AL PD:::" + al_prod_val.get(i));
                                System.out.println("AL PD:::" + al_prod_flag.get(i));
                                System.out.println("AL PD:::" + al_prod_id.get(i));

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }

                            //                           mProgressDialog.dismiss();
                            Intent iHomeScreen = new Intent(SplashScreen.this,
                                    HdbHome.class);
                            startActivity(iHomeScreen);
                            finish();
                        }

                    } else {
                        mProgressDialog.dismiss();
                        //pd_details_layout.setVisibility(View.GONE);
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        // loginErrorMsg.setText(jsonobject.getString("message").toString()
                        //         .toString());

                        userFunction.cutomToast(jsonobject.getString("message").toString(), SplashScreen.this);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(SplashScreen.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
          //  mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.check_masters("1", "2", "3", "4", flag_pd, id_pd);

                System.out.println("JSONOBJ::::" + jsonobject);

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_VM) != null) {
                        res = jsonobject.getString(KEY_STATUS_VM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_VM);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            if (!jsonobject.isNull("data")) {
                                JSONObject jobj2 = jsonobject.getJSONObject("data");
                                jsonarray2 = jobj2.getJSONArray("data2");
                            }

                            if (jsonarray2 != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray2.length(); i++) {
                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                   // mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {




                        if (jsonarray2 != null) {

                            System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray);

                            rDB.delete_pd_table();

                            System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray);

                            for (int i = 0; i < al_product.size(); i++) {

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }
                        }

                       // mProgressDialog.dismiss();

                        Intent iHomeScreen = new Intent(SplashScreen.this,
                                HdbHome.class);
                        startActivity(iHomeScreen);
                        finish();


                    } else {
                      //  mProgressDialog.dismiss();
                        //pd_details_layout.setVisibility(View.GONE);
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        //pd_details_layout.setVisibility(View.GONE);
                        //loginErrorMsg.setText(jsonobject.getString("message").toString()
                        //        .toString());
                        userFunction.cutomToast(jsonobject.getString("message").toString(), SplashScreen.this);

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


}