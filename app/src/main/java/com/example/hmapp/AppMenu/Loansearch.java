package com.example.hmapp.AppMenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.MyUploads.MyuploadsFragmentMain;


/**
 * Created by Administrator on 25-04-2016.
 */
public class Loansearch extends AppCompatActivity {

    EditText edt_los_app_formno;
    RadioGroup radiogroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_loan_search);

        radiogroup = (RadioGroup) findViewById(R.id.myRadioGroup);
        Button btn_search = (Button) findViewById(R.id.btn_search);
        edt_los_app_formno = (EditText) findViewById(R.id.edt_los_appform);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_los_app_formno.getText().toString() == null || edt_los_app_formno.getText().toString().isEmpty()) {
                    // get selected radio button from radioGroup

                    Toast.makeText(Loansearch.this,
                            "Enter LOSID or App form No", Toast.LENGTH_SHORT).show();
                } else {
                    int selectedId = radiogroup.getCheckedRadioButtonId();

                    // find the radio button by returned id
                    RadioButton radioButton = (RadioButton) findViewById(selectedId);
                    Toast.makeText(Loansearch.this,
                            radioButton.getText(), Toast.LENGTH_SHORT).show();
                    String strLoan_no=null;
                    String app_form_no=null;

                    if(radioButton.getText().toString().equals("LOSID")){
                        strLoan_no=edt_los_app_formno.getText().toString();
                    }else{
                        app_form_no=edt_los_app_formno.getText().toString();
                    }

                    Intent loandetails = new Intent(Loansearch.this, MyuploadsFragmentMain.class);
                    loandetails.putExtra("losid", strLoan_no);
                    loandetails.putExtra("app_form_no", app_form_no);
                    loandetails.putExtra("from", "SEARCH");
                    startActivity(loandetails);
                }
            }
        });


    }

}
