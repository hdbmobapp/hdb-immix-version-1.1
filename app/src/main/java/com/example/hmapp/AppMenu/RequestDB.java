package com.example.hmapp.AppMenu;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.UserFunctions;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RequestDB extends SQLiteOpenHelper implements Runnable {
    JSONObject jsonobject;
    UserFunctions userFunction;
    ArrayList<String> get_masters;

    ArrayList<String> get_flag;

    ArrayList<String> arr_pd, arr_pd_val,arr_branch;

    /**
     * `
     * Database name
     */
    private static final String IMAGE_DIRECTORY_NAME = "/HDB";

    private static String DBNAME = "HDBHMCV";
    private static String KEY_SUCCESS = "success";
    private ConnectionDetector cd;
    ProgressDialog mProgressDialog;
    ExampleDBHelper exdbHelper;

    /**
     * Version number of the database
     */
    private static int VERSION = 1;

    public static final String KEY_NEW_Photo1 = "new_path";
    public static final String KEY_ASS_ROW_ID = "_id";
    public static final String KEY_CU_ROW_ID = "_id";

    public static final String KEY_ROW_ID = "_id";
    public static final String KEY_LOSID = "losid";
    public static final String KEY_BRANCH_NAME = "branch_name";

    public static final String KEY_APP_FORM_NO = "app_form_no";
    public static final String KEY_Photo1 = "photo_one";
    public static final String KEY_STATUS = "status";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";

    //----fields for table tbl_Customer_PD  --------//
    public static final String KEY_VISITED_AT = "visited_at";
    public static final String KEY_OTHER_LINK_ACC = "other_link_acc";
    public static final String KEY_CNT_PERSON_NAME = "cnt_person_name";
    public static final String KEY_CNT_PERSON = "cnt_person";
    public static final String KEY_COMM_DOMINATED = "comm_dominated";
    public static final String KEY_APPROACH = "approach";
    public static final String KEY_NEIGHBOUR_CHECK = "neighbour_check";
    public static final String KEY_COLL_DEPLOYMENT = "coll_deployment";
    public static final String KEY_PD_STATUS = "pd_status";
    public static final String KEY_COLL_VERIFIED = "coll_verified";
    public static final String KEY_REMARKS = "remarks";
    public static final String KEY_PD_UPLOAD_BY = "pd_upload_by";
    public static final String KEY_PRODUCT = "product";
    public static final String KEY_ASSET_TYPE = "asset_type";
    public static final String KEY_Distance = "distance";
    public static final String KEY_CU_DISTANCE_KM = "distance_km";


    public static final String KEY_PD5 = "pd5";
    public static final String KEY_PD6 = "pd6";

    public static final String KEY_TS = "timestamp";
    public static final String KEY_VEHICLE_NO = "reg_no";
    public static final String KEY_FEEDBACK = "feedback";
    public static final String KEY_RESIDENCE_TYPE = "residence_type";
    public static final String KEY_RESIDENCE_LOCALITY = "residence_locality";
    public static final String KEY_RESIDENCE_AREA = "residence_area";
    public static final String KEY_PD_CUSTOMER_NAME = "pd_cust_name";
    public static final String KEY_CU_OFFDATE = "cu_offdate";
    public static final String KEY_CU_MAP_ADDRESS = "cu_map_address";

    //----fields for table tbl_asset_verification  --------//
    public static final String KEY_COLL_ADDRESS_TYPE = "coll_address_type";
    public static final String KEY_COLL_ADDRESS = "coll_address";
    public static final String KEY_COLL_PERSON_TYPE = "coll_person_type";
    public static final String KEY_COLL_PERSON_NAME = "coll_person_name";
    public static final String KEY_COLL_UPLOAD_BY = "coll_upload_by";
    public static final String KEY_COLL_LATITUDE = "coll_latitude";
    public static final String KEY_COLL_LONGITUDE = "coll_longitude";
    public static final String KEY_COLL_Distance = "distance";
    public static final String KEY_pic_count = "pic_count";
    public static final String KEY_COLL_CUSTOMER_NAME = "coll_customer_name";
    public static final String KEY_COLL_OFFDATE = "coll_offdate";
    public static final String KEY_COLL_FLAG = "coll_flag";
    public static final String KEY_COLL_DISTANCE_KM = "distance_km";
    public static final String KEY_COLL_REMARKS = "key_remarks";
    public static final String KEY_COLL_MAP_ADDRESS = "coll_map_address";


    //----fields for table tbl_form_upload  --------//

    public static final String KEY_AP_ROW_ID = "_id";
    public static final String KEY_AP_LOS_ID = "ap_los_id";
    public static final String KEY_AP_APP_FORM_NO = "ap_app_form_no";
    public static final String KEY_AP_LATITUDE = "ap_latitude";
    public static final String KEY_AP_LONGITUDE = "ap_longitude";
    public static final String KEY_AP_UPLOAD_BY = "ap_upload_by";
    public static final String KEY_AP_TS = "ap_timestamp";
    public static final String KEY_AP_STATUS = "ap_status";
    public static final String KEY_AP_Distance = "distance";

    public static final String KEY_AP_OFFDATE = "ap_offdate";
    public static final String KEY_AP_DISTANCE_KM = "distance_km";


    /*NEW CASE*/
    private static int VERSION1 = 1;

    public static final String KEY_ROW_ID1 = "_id";

    public static final String KEY_LOSID1 = "losid";
    public static final String KEY_NEW_App_Form_NO = "app_form_no";
    public static final String KEY_Customer_Name = "customer_Name";
    public static final String KEY_Visit_Mode = "visit_mode";

    public static final String KEY_NEW_CASE_UPLOAD_BY = "userid";
    public static final String KEY_Upload_Date = "upload_date";
    public static final String KEY_GPS_Latitiude = "gps_latitiude";
    public static final String KEY_GPS_Longitude = "gps_longitude";

    public static final String KEY_NEW_Distance = "distance";

    public static final String KEY_NEWCASE_TS = "newcase_ts";
    public static final String KEY_NEWCASE_STATUS = "newcase_status";

    public static final String KEY_NEW_DISTANCE_KM = "distance_km";


    /**
     * A constant, stores the the table name
     */
    private static final String DATABASE_PICTURE_TABLE = "key_pic_master";
    private static final String DATABASE_CUSTOMER_TABLE = "key_customer_pd_master";
    private static final String DATABASE_ASSET_TABLE = "key_asset_pd_master";
    private static final String DATABASE_PD_FORM_TABLE = "key_pd_form_pd_master";
    private static final String DATABASE_NEW_CASE = "key_new_case";
    private static final String DATABASE_PD_TABLE = "key_pd_master";
    private static final String DATABASE_BRANCH_TABLE = "key_branch_master";





    //PRODUCT TABLE

    public static final String KEY_pd_id = "pd_id";
    public static final String KEY_product_pd = "product";
    public static final String KEY_pd_flag = "pd_flag";
    public static final String KEY_pd_srno = "pd_srno";
    public static final String KEY_pd_val = "pd_val";


    //branch master table

    public static final String KEY_branch = "branch";
    public static final String KEY_branch_id = "branch_id";
    public static final String KEY_branch_flag = "branch_flag";
    public static final String KEY_branch_srno = "branch_srno";
    public static final String KEY_branch_val = "branch_val";



    /**
     * An instance variable for SQLiteDatabase
     */
    private SQLiteDatabase mDB;
    Context context;
    SharedPreferences pref;
    String str_user_id;
    String str_user_name;

    String str_Visit_Type;
    String str_Community;
    String str_Approach;
    String str_Reference;
    String str_coll_deployment;
    String str_pd_status;
    String str_coll_verified;
    String str_contact_person;
    String str_losid_no, str_app_form_no, str_remarks, str_Cont_person_name, str_link_losid, str_other_link_acc;
    String latitude, longitude, ts, cu_ts, str_product, str_asset_type, str_vehicle_no, coll_ts, form_ts, case_ts, cu_distance_km, co_distance_km, ap_distance_km, str_new_distance_km;

    String ast_losid, ast_form_no, ast_coll_verified_at, ast_coll_verified_with, ast_contact_person,
            ast_coll_person_name, ast_latitude, ast_longitude, ast_pd_uploaded_by, str_coll_remarks;

    String pd_form_losid_no, pd_form_app_form_no, str_form_ts, pd_form_latitude, pd_form_longitude, pd_form_str_user_id;
    String ast_ts;
    String ast_status;
    String pd_form_row_id;
    String ast_row_id;
    String str_cu_key_row_id;
    Integer is_Cust_upload;
    String str_feedback;

    String nc_row_id, nc_losid, nc_upload_by_id, nc_customer_name, nc_visit_mode, nc_upload_date, nc_ts, nc_product, nc_appform_no, nc_latitude, nc_longitude, str_nc_pic_count;

    String str_pd_distance, str_coll_distance, str_new_distance, str_form_distance, str_residence_type, str_residence_locality, str_residence_area, str_pd_photo_count;
    String str_coll_photo_count, str_form_photo_count;
    String str_pd_cust_name, str_coll_cust_name;
    String str_pd_map_address, str_coll_map_address,str_branch;

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String format = s.format(new Date());
        return format;
    }

    /**
     * Constructor
     */
    public RequestDB(Context context) {
        super(context, DBNAME, null, VERSION);
        this.mDB = getWritableDatabase();
        exdbHelper = new ExampleDBHelper(context);
        userFunction = new UserFunctions();
        this.context = context;
        cd = new ConnectionDetector(context);
        pref = context.getSharedPreferences("MyPref", 0);
        get_masters = new ArrayList<String>();
        get_flag = new ArrayList<String>();
        arr_pd = new ArrayList<String>();
        arr_pd_val = new ArrayList<String>();

        arr_branch = new ArrayList<String>();

    }

    /**
     * This is a callback method, invoked when the method getReadableDatabase()
     * / getWritableDatabase() is called provided the database does not exists
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql_customer_query = "create table " + DATABASE_CUSTOMER_TABLE + "  ( "
                + KEY_CU_ROW_ID + " integer primary key autoincrement , "
                + KEY_LOSID + " text , "
                + KEY_APP_FORM_NO + " text , "
                + KEY_VISITED_AT + " text , "
                + KEY_OTHER_LINK_ACC + " text , "
                + KEY_CNT_PERSON_NAME + " text , "
                + KEY_CNT_PERSON + " text , "
                + KEY_COMM_DOMINATED + " text , "
                + KEY_APPROACH + " text , "
                + KEY_NEIGHBOUR_CHECK + " text , "
                + KEY_COLL_DEPLOYMENT + " text , "
                + KEY_PD_STATUS + " text , "
                + KEY_COLL_VERIFIED + " text , "
                + KEY_REMARKS + " text , "
                + KEY_PD_UPLOAD_BY + " text , "
                + KEY_LATITUDE + " text , "
                + KEY_LONGITUDE + " text , "
                + KEY_TS + " text , "
                + KEY_STATUS + " text , "
                + KEY_PRODUCT + " text , "
                + KEY_ASSET_TYPE + " text , "
                + KEY_VEHICLE_NO + " text , "
                + KEY_FEEDBACK + " text , "
                + KEY_Distance + " text , "
                + KEY_RESIDENCE_TYPE + " text , "
                + KEY_RESIDENCE_LOCALITY + " text , "
                + KEY_RESIDENCE_AREA + " text , "
                + KEY_PD_CUSTOMER_NAME + " text , "
                + KEY_PD6 + " text, "
                + KEY_pic_count + " text, "
                + KEY_CU_DISTANCE_KM + " text, "
                +  KEY_CU_MAP_ADDRESS + " text , "
                + KEY_BRANCH_NAME + " text)";
        db.execSQL(sql_customer_query);

        String sql_asset_query = "create table " + DATABASE_ASSET_TABLE + " ( "
                + KEY_ASS_ROW_ID + " integer primary key autoincrement , "
                + KEY_LOSID + " text , "
                + KEY_APP_FORM_NO + " text , "
                + KEY_COLL_PERSON_TYPE + " text , "
                + KEY_COLL_PERSON_NAME + " text , "
                + KEY_COLL_ADDRESS_TYPE + " text , "
                + KEY_COLL_ADDRESS + " text , "
                + KEY_COLL_LATITUDE + " text , "
                + KEY_COLL_LONGITUDE + " text , "
                + KEY_COLL_UPLOAD_BY + " text , "
                + KEY_TS + " text , "
                + KEY_STATUS + " text , "
                + KEY_PRODUCT + " text , "
                + KEY_ASSET_TYPE + " text , "
                + KEY_VEHICLE_NO + " text , "
                + KEY_COLL_Distance + " text , "
                + KEY_pic_count + " text , "
                + KEY_COLL_CUSTOMER_NAME + " text , "
                + KEY_PD6 + " text ,"
                + KEY_COLL_FLAG + " text ,"
                + KEY_COLL_DISTANCE_KM + " text ,"
                + KEY_COLL_REMARKS + " text ,"
                + KEY_COLL_MAP_ADDRESS + " text ,"
                + KEY_BRANCH_NAME + " text)";


        db.execSQL(sql_asset_query);

        String sql_pd_form_query = "create table " + DATABASE_PD_FORM_TABLE + " ( "
                + KEY_AP_ROW_ID + " integer primary key autoincrement , "
                + KEY_AP_LOS_ID + " text , "
                + KEY_AP_APP_FORM_NO + " text , "
                + KEY_AP_LATITUDE + " text , "
                + KEY_AP_LONGITUDE + " text , "
                + KEY_AP_UPLOAD_BY + " text , "
                + KEY_AP_TS + " text , "
                + KEY_STATUS + " text , "
                + KEY_AP_Distance + " text,"
                + KEY_pic_count + " text,"
                + KEY_AP_DISTANCE_KM + " text )";

        db.execSQL(sql_pd_form_query);


        String sql_query = "create table " + DATABASE_PICTURE_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key autoincrement , "
                + KEY_Photo1 + " text , " + KEY_NEW_Photo1 + " text , " + KEY_LATITUDE + " text , " + KEY_LONGITUDE + " text , " + KEY_STATUS + " text) ";

        db.execSQL(sql_query);

        String sql_query_new_case = "create table " + DATABASE_NEW_CASE
                + " ( " + KEY_ROW_ID1 + " integer primary key autoincrement , "
                + KEY_LOSID1 + " text , " + KEY_NEW_App_Form_NO + " text , " + KEY_NEW_CASE_UPLOAD_BY + " text , "
                + KEY_Customer_Name + " text , " + KEY_Visit_Mode
                + " text, " + KEY_Upload_Date + " text , " + KEY_GPS_Latitiude
                + " text," + KEY_GPS_Longitude + " text , " + KEY_TS + " text , " +
                "" + KEY_PRODUCT + " text ," + KEY_NEW_Distance + " text , "
                + KEY_pic_count + " text, "
                + KEY_STATUS + " text, "
                + KEY_NEW_DISTANCE_KM + " text )";

        db.execSQL(sql_query_new_case);

        String sql_pd_query = "create table " + DATABASE_PD_TABLE + " ( "
                + KEY_pd_srno + " integer primary key autoincrement , "
                + KEY_product_pd + " text , " + KEY_pd_val + " text , " + KEY_pd_id + " text, "
                + KEY_pd_flag + " text )";

        db.execSQL(sql_pd_query);


        String sql_branch_query = "create table " + DATABASE_BRANCH_TABLE + " ( "
                + KEY_pd_srno + " integer primary key autoincrement , "
                + KEY_branch + " text , " + KEY_branch_val + " text , " + KEY_branch_id + " text, "
                + KEY_branch_flag + " text )";

        db.execSQL(sql_branch_query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase DB, int arg1, int arg2) {
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_PICTURE_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_CUSTOMER_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_PD_FORM_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_NEW_CASE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_PD_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + str_coll_photo_count);



        // Create tables again
        onCreate(DB);
    }

    public void closeConnection() {
        mDB.close();
    }

    //new case
    public long insertNewCase(String locid, String new_app_form_no, String upload_by_id,
                              String customer_name,
                              String Visit_Mode,
                              String Upload_Date,
                              String GPS_Latitiude,
                              String GPS_Longitude,
                              String ts,
                              String distance,
                              String product,
                              String str_new_distance,
                              String str_status,
                              String str_new_distance_km

    ) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LOSID, locid);
        contentValues.put(KEY_NEW_App_Form_NO, new_app_form_no);
        contentValues.put(KEY_NEW_CASE_UPLOAD_BY, upload_by_id);
        contentValues.put(KEY_Customer_Name, customer_name);
        contentValues.put(KEY_Visit_Mode, Visit_Mode);
        contentValues.put(KEY_Upload_Date, Upload_Date);
        contentValues.put(KEY_GPS_Latitiude, GPS_Latitiude);
        contentValues.put(KEY_GPS_Longitude, GPS_Longitude);
        contentValues.put(KEY_TS, ts);
        contentValues.put(KEY_PRODUCT, product);
        contentValues.put(KEY_NEW_Distance, str_new_distance);
        contentValues.put(KEY_STATUS, str_status);
        contentValues.put(KEY_NEW_DISTANCE_KM, str_new_distance_km);


        if (mDB.isOpen()) {
            //  mDB.close();
        } else {
            mDB = this.getWritableDatabase();

        }

        return mDB.insert(DATABASE_NEW_CASE, null, contentValues);
    }

    public long insert_prod(String prod, String pd_val, String pd_flag, String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (prod != "" && pd_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_product_pd, prod);
            values.put(KEY_pd_val, pd_val);
            values.put(KEY_pd_flag, pd_flag);
            values.put(KEY_pd_id, pd_id);
            long lid = db.insert(DATABASE_PD_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public ArrayList<String> get_masters() {

        SQLiteDatabase db = this.getReadableDatabase();


        String count_pd = "SELECT COUNT (" + KEY_product_pd + ") FROM "
                + DATABASE_PD_TABLE;
        System.out.println("count_pd:" + count_pd);

        Cursor cursor2 = db.rawQuery(count_pd, null);

        System.out.println("countQuery:" + cursor2);

        String pd_count = "";

        try {
            if (cursor2 != null) {

                if (cursor2.moveToNext()) {
                    pd_count = cursor2.getString(0);

                }
                cursor2.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor2.close();
            db.close();
        }


        get_masters.add(pd_count);

        return get_masters;
    }


    public ArrayList<String> get_master_flag() {

        SQLiteDatabase db = this.getReadableDatabase();
        String pd_flag = null;
        String pd_id = null;


        SQLiteDatabase db2 = this.getReadableDatabase();

        String countQuery2 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PD_TABLE + " LIMIT 1";
        Cursor cursor2 = db2.rawQuery(countQuery2, null);

        System.out.println("countQuery:" + cursor2);

        try {
            if (cursor2 != null) {

                if (cursor2.moveToNext()) {
                    pd_flag = cursor2.getString(0);
                    pd_id = cursor2.getString(1);
                }
                cursor2.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db2.close();
        }
        get_flag.add(pd_flag);
        get_flag.add(pd_id);

        return get_flag;
        // System.out.println("countQuery:"+product_qnt);
    }

    public long insert_branch(String prod, String pd_val, String pd_flag, String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (prod != "" && pd_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_branch_id, prod);
            values.put(KEY_branch_val, pd_val);
            values.put(KEY_branch_id, pd_flag);
            values.put(KEY_branch_id, pd_id);
            long lid = db.insert(DATABASE_BRANCH_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public void delete_branch_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_BRANCH_TABLE, null, null);
        db.close();
    }
    public ArrayList<String> get_masters_branch() {

        SQLiteDatabase db = this.getReadableDatabase();

        String product = null;

        arr_branch.add("Select");

        String countQuery = "SELECT " + KEY_branch_val +" FROM "
                + DATABASE_BRANCH_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    product = cursor.getString(0);

                    arr_branch.add(product);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_branch;
        // System.out.println("countQuery:"+product_qnt);
    }


    public String getbranchCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_branch = "SELECT COUNT (" + KEY_branch_id + ") FROM "
                + DATABASE_BRANCH_TABLE ;
        Cursor cursor = db.rawQuery(countQuery_branch, null);


        String branch_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    branch_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

        closeConnection();
        return branch_count;

    }

    public ArrayList<String> get_masters_pd() {

        SQLiteDatabase db = this.getReadableDatabase();

        String product = null;

        arr_pd.add("Select");

        String countQuery = "SELECT " + KEY_product_pd  +" FROM "
                + DATABASE_PD_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    product = cursor.getString(0);

                    arr_pd.add(product);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_pd;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_pd_val() {

        SQLiteDatabase db = this.getReadableDatabase();


        String pd_val = null;

        arr_pd_val.add("");

        String countQuery = "SELECT " + KEY_pd_val  +" FROM "
                + DATABASE_PD_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_val = cursor.getString(0);

                    arr_pd_val.add(pd_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_pd_val;
        // System.out.println("countQuery:"+product_qnt);
    }


    public void delete_pd_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_PD_TABLE, null, null);
        db.close();
    }

    public long insert_photo(String path, String new_path, String latitude, String longitude, SQLiteDatabase db, String status) {

        if (path != "") {

            ContentValues values = new ContentValues();
            values.put(KEY_Photo1, path);
            values.put(KEY_NEW_Photo1, new_path);
            values.put(KEY_LATITUDE, latitude);
            values.put(KEY_LONGITUDE, longitude);
            values.put(KEY_STATUS, status);
            long lid = db.insert(DATABASE_PICTURE_TABLE, null, values);

            //   System.out.println("lid:::::" + lid);
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public String getphoto() {

        SQLiteDatabase db = this.getReadableDatabase();
        String image_path = null;
        String image_new_path = null;
        String image_latitude = null;
        String image_longitude = null;


        String countQuery_val = "SELECT " + KEY_Photo1 + "," + KEY_NEW_Photo1 + "," + KEY_LATITUDE + "," + KEY_LONGITUDE + "  FROM "
                + DATABASE_PICTURE_TABLE + " WHERE " + KEY_STATUS
                + "= 0 LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery_val, null);

        System.out.println("countQuerymlop:" + countQuery_val);

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    image_path = cursor.getString(0);
                    image_new_path = cursor.getString(1);
                    image_latitude = cursor.getString(2);
                    image_longitude = cursor.getString(3);

                    if (image_path.contains("MAP")) {
                     //   upload_image(image_path, image_path, image_latitude, image_longitude);
                    } else {
//                        String cmp_iomage = compressImage(image_path, image_new_path, image_latitude, image_longitude);
//                        if (cmp_iomage != null) {
                      //  upload_image(image_path, image_path, image_latitude, image_longitude);
//                        }
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            cursor.close();
            db.close();
        }
        //     closeConnection();

        return image_path;
        // System.out.println("countQuery:"+product_qnt);
    }


    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + KEY_Photo1 + ") FROM "
                + DATABASE_PICTURE_TABLE + " WHERE " + KEY_STATUS + "= 0";
        Cursor cursor = db.rawQuery(countQuery_pic, null);


        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

        closeConnection();
        return pending_count;

    }

    public String getPendingPhotoCount_not() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + KEY_Photo1 + ") FROM "
                + DATABASE_PICTURE_TABLE;
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);
        String pending_count = "";
        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        closeConnection();
        return pending_count;

    }

    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_PICTURE_TABLE, KEY_Photo1 + "=" + " '"
                    + path + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        closeConnection();

        // return true;
        return chkFlag;
    }




    public String getCustomerPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_CU_ROW_ID + ") FROM "
                + DATABASE_CUSTOMER_TABLE + " WHERE " + KEY_STATUS + "= 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public String getAssetPendingCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT COUNT (" + KEY_ASS_ROW_ID + ") FROM "
                + DATABASE_ASSET_TABLE + " WHERE " + KEY_STATUS + "= 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public String getPD_formPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_AP_ROW_ID + ") FROM "
                + DATABASE_PD_FORM_TABLE + " WHERE " + KEY_STATUS + " = 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public String getCasePendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_ROW_ID1 + ") FROM "
                + DATABASE_NEW_CASE + " where " + KEY_STATUS + " = 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public long insertCustomers_Info(String str_losid, String str_app_form_no, String str_branch,
                                     String str_visited_at, String str_other_acc, String str_cnt_person_name,
                                     String str_cnt_person, String str_approach,
                                     String str_neighbour_check, String str_coll_deployment, String str_pd_status,
                                     String str_coll_verified, String str_remarks,
                                     String str_pd_upload_by, String str_latitude,
                                     String str_longitude, String cu_ts, String str_product, String str_asset_type, String str_vehicle_no, String str_feed_bck, String str_pd_distance, String str_residence_type, String str_residence_locality, String str_residence_area, String str_pd_cust_name, String str_pd_offline_ts, String cust_status, String str_cu_distance_km,String str_pd_map_address) {


        //str_pd_offline_date=getDateTime();

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LOSID, str_losid);
        contentValues.put(KEY_APP_FORM_NO, str_app_form_no);
        contentValues.put(KEY_BRANCH_NAME, str_branch);

        contentValues.put(KEY_VISITED_AT, str_visited_at);
        contentValues.put(KEY_OTHER_LINK_ACC, str_other_acc);
        contentValues.put(KEY_CNT_PERSON_NAME, str_cnt_person_name);
        contentValues.put(KEY_CNT_PERSON, str_cnt_person);

        contentValues.put(KEY_APPROACH, str_approach);
        contentValues.put(KEY_NEIGHBOUR_CHECK, str_neighbour_check);
        contentValues.put(KEY_COLL_DEPLOYMENT, str_coll_deployment);
        contentValues.put(KEY_PD_STATUS, str_pd_status);
        contentValues.put(KEY_COLL_VERIFIED, str_coll_verified);
        contentValues.put(KEY_REMARKS, str_remarks);
        contentValues.put(KEY_PD_UPLOAD_BY, str_pd_upload_by);
        contentValues.put(KEY_LATITUDE, str_latitude);
        contentValues.put(KEY_LONGITUDE, str_longitude);
        contentValues.put(KEY_TS, cu_ts);

        contentValues.put(KEY_STATUS, "0");
        contentValues.put(KEY_PRODUCT, str_product);
        contentValues.put(KEY_ASSET_TYPE, str_asset_type);
        contentValues.put(KEY_VEHICLE_NO, str_vehicle_no);
        contentValues.put(KEY_FEEDBACK, str_feed_bck);
        contentValues.put(KEY_Distance, str_pd_distance);
        contentValues.put(KEY_RESIDENCE_TYPE, str_residence_type);
        contentValues.put(KEY_RESIDENCE_LOCALITY, str_residence_locality);
        contentValues.put(KEY_RESIDENCE_AREA, str_residence_area);

        contentValues.put(KEY_PD_CUSTOMER_NAME, str_pd_cust_name);
        contentValues.put(KEY_STATUS, cust_status);
        // contentValues.put(KEY_PD_of, str_pd_offline_ts);
        contentValues.put(KEY_CU_DISTANCE_KM, str_cu_distance_km);
        contentValues.put(KEY_CU_MAP_ADDRESS, str_pd_map_address);



        if (mDB.isOpen()) {
            //  mDB.close();
        } else {
            mDB = this.getWritableDatabase();

        }
        return mDB.insert(DATABASE_CUSTOMER_TABLE, null, contentValues);
    }

    public long insertAssetInfo(String str_losid, String str_app_form_no, String str_branch,
                                String coll_cnt_person_type, String coll_cnt_person_name,
                                String coll_address_type, String coll_address,
                                String str_coll_upload_by, String coll_latitude,
                                String coll_longitude, String coll_ts, String str_product,
                                String str_asset_type, String str_vehicle_no, String str_coll_distance,
                                String str_coll_cust_name, String str_flag, String str_coll_distance_km, String str_coll_remarks,String str_coll_map_address) {


        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LOSID, str_losid);
        contentValues.put(KEY_APP_FORM_NO, str_app_form_no);
        contentValues.put(KEY_BRANCH_NAME, str_branch);
        contentValues.put(KEY_COLL_PERSON_TYPE, coll_cnt_person_type);
        contentValues.put(KEY_COLL_PERSON_NAME, coll_cnt_person_name);
        contentValues.put(KEY_COLL_ADDRESS_TYPE, coll_address_type);
        contentValues.put(KEY_COLL_ADDRESS, coll_address);
        contentValues.put(KEY_COLL_UPLOAD_BY, str_coll_upload_by);
        contentValues.put(KEY_COLL_LATITUDE, coll_latitude);
        contentValues.put(KEY_COLL_LONGITUDE, coll_longitude);
        contentValues.put(KEY_TS, coll_ts);
        contentValues.put(KEY_STATUS, "0");

        contentValues.put(KEY_PRODUCT, str_product);
        contentValues.put(KEY_ASSET_TYPE, str_asset_type);
        contentValues.put(KEY_VEHICLE_NO, str_vehicle_no);
        contentValues.put(KEY_COLL_Distance, str_coll_distance);

        contentValues.put(KEY_COLL_CUSTOMER_NAME, str_coll_cust_name);
        contentValues.put(KEY_COLL_FLAG, str_flag);
        contentValues.put(KEY_COLL_DISTANCE_KM, str_coll_distance_km);
        contentValues.put(KEY_COLL_REMARKS, str_coll_remarks);
        contentValues.put(KEY_COLL_MAP_ADDRESS, str_coll_map_address);



        if (mDB.isOpen()) {
            // mDB.close();
        } else {
            mDB = this.getWritableDatabase();

        }
        return mDB.insert(DATABASE_ASSET_TABLE, null, contentValues);
    }

    public long insertPDInfo(String str_losid, String str_app_form_no,
                             String str_coll_upload_by, String str_latitude,
                             String str_longitude, String ts, String str_form_distance, String str_ap_distance_km) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_AP_LOS_ID, str_losid);
        contentValues.put(KEY_AP_APP_FORM_NO, str_app_form_no);
        contentValues.put(KEY_AP_UPLOAD_BY, str_coll_upload_by);
        contentValues.put(KEY_AP_LATITUDE, str_latitude);
        contentValues.put(KEY_AP_LONGITUDE, str_longitude);
        contentValues.put(KEY_AP_TS, ts);
        contentValues.put(KEY_STATUS, "0");
        contentValues.put(KEY_AP_Distance, str_form_distance);
        contentValues.put(KEY_AP_DISTANCE_KM, str_ap_distance_km);

        if (mDB.isOpen()) {
            // mDB.close();
        } else {
            mDB = this.getWritableDatabase();

        }
        return mDB.insert(DATABASE_PD_FORM_TABLE, null, contentValues);
    }

    public boolean deleteCustomerInfo(String key_row_id, String app_form_no) {
        Log.d("DELETING", key_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_CUSTOMER_TABLE, KEY_CU_ROW_ID + "=" + " '" + key_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public boolean deleteAssetInfo(String key_ass_row_id, String app_form_no) {
        Log.d("DELETING", key_ass_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_ASSET_TABLE, KEY_ASS_ROW_ID + "=" + " '" + key_ass_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public boolean deleteForm_info(String pd_form_row_id, String app_form_no) {
        Log.d("DELETING", pd_form_row_id);
        boolean chkFlag = false;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_PD_FORM_TABLE, KEY_AP_ROW_ID + "=" + " '" + pd_form_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        // return true;
        return chkFlag;
    }

    public boolean deleteNewCaseInfo(String key_row_id, String app_form_no) {
        Log.d("DELETING", key_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_NEW_CASE, KEY_CU_ROW_ID + "=" + " '" + key_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }


    public class InsertCustomorData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            String str_pic_count = exdbHelper.getCasePhotoCount(cu_ts);

            System.out.println("str_pd_map_address:::"+str_pd_map_address);

            jsonobject = userFunction.InsertData(str_app_form_no,
                    str_losid_no,str_branch,str_other_link_acc, str_Visit_Type, str_contact_person, str_Cont_person_name,

                    str_remarks, str_Approach,
                    str_Reference, str_coll_deployment, str_pd_status,
                    str_coll_verified, latitude, longitude, cu_ts, str_user_id, str_product, str_asset_type, str_vehicle_no, str_feedback, str_pd_distance, str_residence_type, str_residence_locality, str_residence_area, str_pic_count, str_pd_cust_name, cu_distance_km,str_pd_map_address);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            deleteCustomerInfo(str_cu_key_row_id, str_app_form_no);
                            userFunction
                                    .cutomToast("Customer PD Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);
                        }
                    }
                } else {

                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);

                    context.startActivity(dashActivity);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class InsertPD_formData extends AsyncTask<Void, Void, Void> {

        JSONObject pdform_jsonobject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            String str_pic_count = exdbHelper.getCasePhotoCount(str_form_ts);

            pdform_jsonobject = userFunction.InsertDataForm(pd_form_losid_no, pd_form_app_form_no, pd_form_latitude, pd_form_longitude, pd_form_str_user_id, str_form_distance, str_pic_count, str_form_ts, ap_distance_km);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (pdform_jsonobject != null) {
                    if (pdform_jsonobject.getString(KEY_STATUS) != null) {
                        String res = pdform_jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = pdform_jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            is_Cust_upload = 1;
                            deleteForm_info(pd_form_row_id, pd_form_app_form_no);
                            userFunction
                                    .cutomToast("Customer PD  form Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);

                        }
                    }
                } else {

                    mProgressDialog.dismiss();
                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);
                    context.startActivity(dashActivity);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class InsertAssetCollData extends AsyncTask<Void, Void, Void> {
        JSONObject ast_jsonobject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            String str_pic_count = exdbHelper.getCasePhotoCount(ast_ts);

            ast_jsonobject = userFunction.InsertDataAsset(ast_losid, ast_form_no,str_branch, ast_coll_verified_at, ast_coll_verified_with, ast_contact_person,
                    ast_coll_person_name, ast_latitude, ast_longitude, ast_pd_uploaded_by, str_product, str_asset_type, str_vehicle_no, str_coll_distance, str_pic_count, str_coll_cust_name, ast_ts, co_distance_km, str_coll_remarks,str_coll_map_address);


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (ast_jsonobject != null) {
                    if (ast_jsonobject.getString(KEY_STATUS) != null) {
                        String res = ast_jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = ast_jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            System.out.println("SDSDSDSD:::" + res);
                            deleteAssetInfo(ast_row_id, ast_form_no);

//call function for the image upload

                            userFunction
                                    .cutomToast("Asset PD Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);
                        }

                    }
                } else {

                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);
                    context.startActivity(dashActivity);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //   mProgressDialog.dismiss();

        }
    }

    String sql_query_new_case = "create table " + DATABASE_NEW_CASE
            + " ( " + KEY_ROW_ID1 + " integer primary key autoincrement , "
            + KEY_LOSID1 + " text , " + KEY_NEW_App_Form_NO + " text , " + KEY_NEW_CASE_UPLOAD_BY + " text , "
            + KEY_Customer_Name + " text , " + KEY_Visit_Mode
            + " text, " + KEY_Upload_Date + " text , " + KEY_GPS_Latitiude
            + " text," + KEY_GPS_Longitude + " text , " + KEY_TS + " text , " + KEY_PRODUCT + " text  ) ";

    public String get_cases_request(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;
        if (ts != null) {

            countQuery = "SELECT *  FROM " + DATABASE_NEW_CASE + " where " + KEY_TS + " = " + "'" + ts + "' AND " + KEY_STATUS + " = 1 LIMIT 1";
        } else {
            countQuery = "SELECT *  FROM " + DATABASE_NEW_CASE;
        }

        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    nc_row_id = cursor.getString(0);
                    nc_losid = cursor.getString(1);
                    nc_appform_no = cursor.getString(2);
                    nc_upload_by_id = cursor.getString(3);
                    nc_customer_name = cursor.getString(4);
                    nc_visit_mode = cursor.getString(5);
                    nc_upload_date = cursor.getString(6);
                    nc_latitude = cursor.getString(7);
                    nc_longitude = cursor.getString(8);
                    nc_ts = cursor.getString(9);
                    nc_product = cursor.getString(10);
                    str_new_distance = cursor.getString(11);
                    str_new_distance_km = cursor.getString(12);


                }
                cursor.close();

                if (nc_row_id.equals("")) {
                } else {
                    if (cd.isConnectingToInternet()) {
                        new InsertNewCaseData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public String get_Customers_request(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;
        if (ts != null) {
            countQuery = "SELECT *  FROM " + DATABASE_CUSTOMER_TABLE + " WHERE "
                    + KEY_STATUS + "= 1  AND " + KEY_TS + " = " + "'" + ts + "' LIMIT 1";

        } else {
            countQuery = "SELECT *  FROM " + DATABASE_CUSTOMER_TABLE + " WHERE "
                    + KEY_STATUS + "= 1 LIMIT 1";
        }


        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    str_cu_key_row_id = cursor.getString(0);
                    str_losid_no = cursor.getString(1);
                    str_app_form_no = cursor.getString(2);
                    str_Visit_Type = cursor.getString(3);
                    str_other_link_acc = cursor.getString(4);
                    str_Cont_person_name = cursor.getString(5);
                    str_contact_person = cursor.getString(6);
                    str_Community = cursor.getString(7);
                    str_Approach = cursor.getString(8);
                    str_Reference = cursor.getString(9);
                    str_coll_deployment = cursor.getString(10);
                    str_pd_status = cursor.getString(11);
                    str_coll_verified = cursor.getString(12);
                    str_remarks = cursor.getString(13);
                    str_user_id = cursor.getString(14);
                    latitude = cursor.getString(15);
                    longitude = cursor.getString(16);
                    str_product = cursor.getString(19);
                    str_asset_type = cursor.getString(20);
                    str_vehicle_no = cursor.getString(21);
                    str_feedback = cursor.getString(22);
                    str_pd_distance = cursor.getString(23);
                    str_pd_photo_count = cursor.getString(29);
                    str_residence_type = cursor.getString(24);
                    str_residence_locality = cursor.getString(25);
                    str_residence_area = cursor.getString(26);
                    str_pd_cust_name = cursor.getString(27);
                    cu_ts = cursor.getString(17);
                    cu_distance_km = cursor.getString(30);
                    str_pd_map_address = cursor.getString(31);
                    str_branch= cursor.getString(32);

                }
                cursor.close();

                if (Integer.parseInt(str_cu_key_row_id) > 0) {

                    if (cd.isConnectingToInternet()) {

                        new InsertCustomorData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public String get_asset_request(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;

        if (ts != null) {
            countQuery = "SELECT *  FROM " + DATABASE_ASSET_TABLE + " WHERE "
                    + KEY_STATUS + "= 1  AND " + KEY_TS + " = " + "'" + ts + "' LIMIT 1";
        } else {
            countQuery = "SELECT *  FROM " + DATABASE_ASSET_TABLE + " WHERE "
                    + KEY_STATUS + " = 1 LIMIT 1";

        }
        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    ast_row_id = cursor.getString(0);
                    ast_losid = cursor.getString(1);
                    ast_form_no = cursor.getString(2);
                    ast_contact_person = cursor.getString(3);
                    ast_coll_person_name = cursor.getString(4);
                    ast_coll_verified_at = cursor.getString(5);
                    ast_coll_verified_with = cursor.getString(6);
                    ast_latitude = cursor.getString(7);
                    ast_longitude = cursor.getString(8);
                    ast_pd_uploaded_by = cursor.getString(9);
                    ast_ts = cursor.getString(10);
                    ast_status = cursor.getString(11);
                    str_product = cursor.getString(12);
                    str_asset_type = cursor.getString(13);
                    str_vehicle_no = cursor.getString(14);
                    str_coll_distance = cursor.getString(15);
                    str_coll_photo_count = cursor.getString(16);
                    str_coll_cust_name = cursor.getString(17);
                    co_distance_km = cursor.getString(20);
                    str_coll_remarks = cursor.getString(21);
                    str_coll_map_address= cursor.getString(22);
                    str_branch= cursor.getString(23);

                }
                System.out.println("Cursor::" + cursor.getString(9));
                cursor.close();

                System.out.println("str_coll_photo_counbvnmbt::" + str_coll_photo_count);

                if (Integer.parseInt(ast_row_id) > 0) {
                    if (cd.isConnectingToInternet()) {
                        new InsertAssetCollData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public String get_app_form_request(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;

        if (ts != null) {
            countQuery = "SELECT *  FROM " + DATABASE_PD_FORM_TABLE + " WHERE "
                    + KEY_STATUS + "= 1  AND " + KEY_AP_TS + " = " + "'" + ts + "' LIMIT 1";
        } else {
            countQuery = "SELECT *  FROM " + DATABASE_PD_FORM_TABLE + " WHERE "
                    + KEY_STATUS + "= 1 LIMIT 1";
        }
        Cursor cursor = db.rawQuery(countQuery, null);
        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    pd_form_row_id = cursor.getString(0);
                    pd_form_losid_no = cursor.getString(1);
                    pd_form_app_form_no = cursor.getString(2);
                    pd_form_latitude = cursor.getString(3);
                    pd_form_longitude = cursor.getString(4);
                    pd_form_str_user_id = cursor.getString(5);
                    str_form_distance = cursor.getString(8);
                    str_form_photo_count = cursor.getString(9);
                    str_form_ts = cursor.getString(6);
                    ap_distance_km = cursor.getString(10);


                }
                cursor.close();
                System.out.println("str_form_distance:::" + str_form_distance);
                if (Integer.parseInt(pd_form_row_id) > 0) {
                    if (cd.isConnectingToInternet()) {
                        new InsertPD_formData().execute();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    @Override
    public void run() {

    }

    public String compressImage(String imageUri, String img_new_path, String image_latitude, String image_longitude) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        // by setting this field as true, the actual bitmap pixels are not
        // loaded in the memory. Just the bounds are loaded. If
        // you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);


        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        // max Height and width values of the compressed image is taken as
        // 816x612
        if (actualHeight == 0 && actualWidth == 0) {
            deletepic(imageUri);
        }

        System.out.println("actualHeight::" + actualHeight + ":::actualWidth:::" + actualWidth);

        float maxHeight;
        float maxWidth;
        if (img_new_path.contains("NEW")) {
            maxHeight = 1026.0f;
            maxWidth = 912.0f;
        } else if (img_new_path.contains("PD")) {
            maxHeight = 1026.0f;
            maxWidth = 912.0f;
        } else {
            maxHeight = 500.0f;
            maxWidth = 400.0f;
        }


        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        // setting inSampleSize value allows to load a scaled down version of
        // the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth,
                actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low
        // on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }


        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                    Bitmap.Config.ARGB_8888);

        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        } catch (Exception e) {
            System.out.println("Inside the poplkkas:::");
            //userFunction.cutomToast("unable to attached plz try again...", context);
           // create_bimap_image(imageUri, img_new_path, image_latitude, image_longitude);
            e.printStackTrace();
        }


        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2,
                middleY - bmp.getHeight() / 2, new Paint(
                        Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(img_new_path);
        try {
            out = new FileOutputStream(filename);
            // write the compressed bitmap at the destination specified by
            // filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 96, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        return filename;

    }

    public String getFilename(String new_path) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "HDB");

        if (!file.exists()) {
            file.mkdirs();
        }

        String uriSting = (file.getAbsolutePath() + "/" + new_path + ".jpg");

        file = new File(file.getAbsolutePath() + "/" + new_path + ".jpg");

        if (file.exists()) {
            file.delete();
        }

        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public Bitmap decodeFile(String path) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE = 1000;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE
                    && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    public boolean update_status(String ts, String status, String table, String pic_count) {
        mDB = this.getReadableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_STATUS, status);
        args.put(KEY_pic_count, pic_count);
        Boolean res = false;

        switch (table) {
            case "CU":
                res = mDB.update(DATABASE_CUSTOMER_TABLE, args, ts, null) > 0;

                break;

            case "CO":
                res = mDB.update(DATABASE_ASSET_TABLE, args, ts, null) > 0;
                break;

            case "FORM":
                res = mDB.update(DATABASE_PD_FORM_TABLE, args, ts, null) > 0;
                break;

            case "NEW":
                res = mDB.update(DATABASE_NEW_CASE, args, ts, null) > 0;
                break;

        }
        return res;
        /*
        String update_query="Update "+DATABASE_CUSTOMER_TABLE+ " set "+KEY_STATUS+" =1  where "+KEY_TS+ " = "+ts;
       return mDB.rawQuery(update_query,null);
        System.out.println("Update query::::"+update_query);
        */

    }

    public class InsertNewCaseData extends AsyncTask<Void, Void, Void> {
        JSONObject case_jsonobject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            String str_pic_count = exdbHelper.getCasePhotoCount(nc_ts);

            case_jsonobject = userFunction.InsertNewCaseData(nc_losid, nc_appform_no, nc_upload_by_id,
                    nc_customer_name, nc_visit_mode, nc_upload_date,
                    nc_ts, nc_product, nc_latitude, nc_longitude, str_new_distance, str_new_distance_km, str_pic_count);


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                System.out.println("case_jsonobject::::" + case_jsonobject);
                if (case_jsonobject != null) {
                    if (case_jsonobject.getString(KEY_STATUS) != null) {
                        String res = case_jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = case_jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            System.out.println("SDSDSDSD:::" + res);
                            deleteNewCaseInfo(nc_row_id, nc_appform_no);
                            userFunction
                                    .cutomToast("Successfully uploaded..", context);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);
                        }

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //   mProgressDialog.dismiss();
            mProgressDialog.dismiss();
        }
    }


    public boolean delete_pic(String key_ass_row_id, String app_form_no) {
        Log.d("DELETING", key_ass_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_ASSET_TABLE, KEY_ASS_ROW_ID + "=" + "'" + key_ass_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public void delete_pd() {
        SQLiteDatabase db = this.getWritableDatabase();
        String del_cust = "delete from " + DATABASE_CUSTOMER_TABLE + " where " + KEY_STATUS + "= 0";
        db.execSQL(del_cust);

        String del_coll = "delete from " + DATABASE_ASSET_TABLE + " where " + KEY_STATUS + "= 0";
        db.execSQL(del_coll);

        String del_form = "delete from " + DATABASE_PD_FORM_TABLE + " where " + KEY_STATUS + "= 0";
        db.execSQL(del_form);

        String del_new = "delete from " + DATABASE_NEW_CASE + " where " + KEY_STATUS + "= 0";
        db.execSQL(del_new);


    }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */

    public void redirect_dash() {

    }


}
