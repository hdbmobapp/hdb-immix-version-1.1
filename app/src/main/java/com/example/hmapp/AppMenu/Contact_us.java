package com.example.hmapp.AppMenu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;


public class Contact_us extends AppCompatActivity {

    String avi_rel, sach_tel, vija_tel, service_tel;
    SharedPreferences pref;

    TextView txt_call_avi;
    TextView txt_call_vijay;
    TextView txt_call_sachin;
    TextView text_call_service_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_contact_us);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        avi_rel = pref.getString("avi_tel", null);
        vija_tel = pref.getString("vija_tel", null);
        sach_tel = pref.getString("sach_tel", null);
        service_tel = pref.getString("service_tel", null);

        txt_call_avi = (TextView) findViewById(R.id.text_call_avi);
        txt_call_vijay = (TextView) findViewById(R.id.text_call_vijay);
        txt_call_sachin = (TextView) findViewById(R.id.text_call_sachin);
        text_call_service_desc = (TextView) findViewById(R.id.text_call_service_desc);

        txt_call_avi.setText(avi_rel);
        txt_call_vijay.setText(vija_tel);
        txt_call_sachin.setText(sach_tel);
        text_call_service_desc.setText(service_tel);


        TextView txt_email_avi = (TextView) findViewById(R.id.txt_email_avi);


        txt_email_avi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String[] TO = {"avinash.sonawane@hdbfs.com"};
                String[] CC = {"deepak.gupta@hdbfs.com", "vijay.panvelkar@hdbfs.com", "sachin.gajare@hdbfs.com"};
                sendEmail(TO, CC);
            }
        });

        TextView txt_email_vijay = (TextView) findViewById(R.id.txt_email_vijay);

        txt_email_vijay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String[] TO = {"vijay.panvelkar@hdbfs.com"};
                String[] CC = {"deepak.gupta@hdbfs.com", "avinash.sonawane@hdbfs.com", "sachin.gajare@hdbfs.com"};
                sendEmail(TO, CC);
            }
        });

        TextView txt_email_sachin = (TextView) findViewById(R.id.txt_email_sachin);

        txt_email_sachin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String[] TO = {"sachin.gajare@hdbfs.com"};
                String[] CC = {"deepak.gupta@hdbfs.com", "avinash.sonawane@hdbfs.com", "vijay.panvelkar@hdbfs.com"};
                sendEmail(TO, CC);
            }
        });
        TextView txt_email_servicedesc = (TextView) findViewById(R.id.txt_email_servicedesc);

        txt_email_servicedesc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String[] TO = {"Application.support@hdbfs.com"};
                String[] CC = {"deepak.gupta@hdbfs.com", "avinash.sonawane@hdbfs.com", "vijay.panvelkar@hdbfs.com"};
                sendEmail(TO, CC);
            }
        });


    }


    protected void sendEmail(String[] str_arr_to, String[] str_arr_cc) {
        Log.i("Send email", "");


        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, str_arr_to);
        emailIntent.putExtra(Intent.EXTRA_CC, str_arr_cc);
        // emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        // emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished sendi.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Contact_us.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    public void call_avi(View v) {
        Intent call_avi = new Intent(Intent.ACTION_CALL);
        call_avi.setData(Uri.parse("tel: " + avi_rel));
        startActivity(call_avi);
    }

    public void call_vijay(View v) {
        Intent call_avi = new Intent(Intent.ACTION_CALL);
        call_avi.setData(Uri.parse("tel: " + vija_tel));

        startActivity(call_avi);
    }

    public void call_sachin(View v) {
        Intent call_avi = new Intent(Intent.ACTION_CALL);
        call_avi.setData(Uri.parse("tel: " + sach_tel));
        startActivity(call_avi);
    }

    public void call_service_desc(View v) {
        Intent call_avi = new Intent(Intent.ACTION_CALL);
        call_avi.setData(Uri.parse("tel: " + service_tel));
        startActivity(call_avi);
    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }


}