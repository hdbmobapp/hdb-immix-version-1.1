package com.example.hmapp.AppMenu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.hmapp.hdbLibrary.UserFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class MyCasesListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;
	UserFunctions user_function;


	public MyCasesListAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		ViewHolder holder = null;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = null;
		// Get the position
		resultp = data.get(position);

		if (itemView == null) {
			// The view is not a recycled one: we have to inflate
			itemView = inflater.inflate(R.layout.pd_my_cases_list_item, parent,
					false);
			
			holder = new ViewHolder();

			holder.tv_pd_status = (TextView) itemView
					.findViewById(R.id.txt_pd_status);
			holder.tv_losid = (TextView) itemView.findViewById(R.id.txt_losid);
			holder.tv_visit_mode = (TextView) itemView
					.findViewById(R.id.txt_visit_mode);
			holder.tv_coll_verified = (TextView) itemView
					.findViewById(R.id.txt_contact_person);
			holder.tv_contact_person_name = (TextView) itemView
					.findViewById(R.id.txt_contact_person_name);
			holder.tv_contact_person = (TextView) itemView
					.findViewById(R.id.txt_contact_person);

            holder.tv_upload_date = (TextView) itemView
                    .findViewById(R.id.txt_upload_date);


            holder.tv_coll_pics = (TextView) itemView
                    .findViewById(R.id.txt_add_coll);

            holder.tv_form_pics = (TextView) itemView
                    .findViewById(R.id.txt_add_forms);

            holder.tv_app_form_no = (TextView) itemView
                    .findViewById(R.id.txt_app_form_no);


			holder.tv_pd_customer_name = (TextView) itemView
					.findViewById(R.id.txt_pd_customer_name);
			holder.tv_co_customer_name = (TextView) itemView
					.findViewById(R.id.txt_co_customer_name);




            holder.tv_add_pd = (TextView) itemView
                    .findViewById(R.id.txt_add_pd);

            holder.tv_new_pics = (TextView) itemView
                    .findViewById(R.id.txt_new_case_count);

			itemView.setTag(holder);
		} else {
			// View recycled !
			// no need to inflate
			// no need to findViews by id
			holder = (ViewHolder) itemView.getTag();
		}

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


		holder.tv_pd_status.setText(resultp.get(MyCases.PD_STATUS));
		holder.tv_losid.setText(resultp.get(MyCases.LOSID));
		holder.tv_visit_mode.setText(resultp.get(MyCases.VISIT_MODE));
		holder.tv_coll_verified.setText(resultp.get(MyCases.COLL_VERIFIED));
		holder.tv_contact_person_name.setText(resultp.get(MyCases.CO_PIC_COUNT));
		holder.tv_contact_person.setText(resultp.get(MyCases.CU_PIC_COUNT));
        holder.tv_upload_date.setText(resultp.get(MyCases.PF_PIC_COUNT));
        holder.tv_app_form_no.setText(resultp.get(MyCases.FORM_NO));
        holder.tv_new_pics.setText(resultp.get(MyCases.NEW_CASE_UPLOAD_BY));


		holder.tv_pd_customer_name.setText(resultp.get(MyCases.PD_Customer_name));
		holder.tv_co_customer_name.setText(resultp.get(MyCases.CO_Customer_name));

/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}
*/


        holder.tv_form_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, PDForm.class);
                pd_form.putExtra("pd_losid",resultp.get(MyCases.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(MyCases.FORM_NO));
                context.startActivity(pd_form);
            }

        });

        holder.tv_add_pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, ExistingPD.class);
                pd_form.putExtra("pd_losid",resultp.get(MyCases.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(MyCases.FORM_NO));

                pd_form.putExtra("product_type",resultp.get(MyCases.PD_Product_type));
                pd_form.putExtra("asset_type",resultp.get(MyCases.PD_Asset_type));

                context.startActivity(pd_form);
            }

        });

        holder.tv_coll_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                Intent pd_coll = new Intent(context, AssetVerified.class);
               // pd_coll.putExtra("pd_upload_by",resultp.get(MyCases.PD_UPLOAD_BY));
                pd_coll.putExtra("pd_losid",resultp.get(MyCases.LOSID));
                pd_coll.putExtra("pd_form_no",resultp.get(MyCases.FORM_NO));

                pd_coll.putExtra("product_type",resultp.get(MyCases.PD_Product_type));
                pd_coll.putExtra("asset_type",resultp.get(MyCases.PD_Asset_type));
                context.startActivity(pd_coll);
            }

        });

		return itemView;
	}

	private static class ViewHolder {
		public TextView tv_pd_status;
		public TextView tv_losid;
        public TextView tv_app_form_no;
        public TextView tv_add_pd;

        public TextView tv_visit_mode;
		public TextView tv_coll_verified;
		public TextView tv_contact_person_name;
		public TextView tv_contact_person;
        public TextView tv_upload_date;

        public TextView tv_coll_pics;
        public TextView tv_form_pics;
        public TextView tv_new_pics;
		public TextView tv_pd_customer_name;
		public TextView tv_co_customer_name;



    }

}
