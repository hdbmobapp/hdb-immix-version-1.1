package com.example.hmapp.AppMenu;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MainActivity_OLD;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.GPSTracker;
import com.example.hmapp.hdbLibrary.GPSTracker_New;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NewCase extends AppCompatActivity implements OnItemSelectedListener {


    int MEDIA_MAP_COUNT = 0;

    protected JSONObject jsonobject;
    public static final String KEY_STATUS = "status";

    String[] Dataentrytype = {"Select", "New Case"};
    String[] Dataentrytype_val = {"", "NW"};

    String[] Visit_Mode = {"Select", "Phone", "Visit Office", "Visit Resident", "Visit Others", "Third Party", "DSA Office"};
    String[] Visit_Mode_val = {"", "PH", "VSOF", "VSRS", "VSOTH", "THP", "DSA"};


    private ConnectionDetector cd;
    String lattitude = null, longitude = null;
    RequestDB rdb;
    String map_nw;

    Spinner spn_Product_Type;
    public Spinner spn_visit_Type;
    private String str_Product_Type;
    private String str_Data_Entry_Type;

    EditText edt_losid_no, edt_app_form_no, edt_customer_name, edt_branch_name;
    UserFunctions userFunction;
    String Upload_Date, date;


    String str_losid_no, sr_app_form_no, str_customer_name,
            str_Number_Contacted,
            str_branch_name = null, str_product = null;
    String current_date = null;
    String str_visit_mode;

    SharedPreferences pref;
    SharedPreferences pData;
    EditText edt_Number_Contacted;
    String str_user_id;
    String str_disance = "0";
    GPSTracker gps;
    Date dTS;
    String ts;
    ProgressDialog mProgressDialog;
    EditText txt_distacne;
    String str_txt_distacne;
    Spinner spn_distance_from_loc;
    String str_distance_type;
    ArrayAdapter<String> adapter_distance_type;
    String[] distance_type = {"Select", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};
    int map_captured = 0;
    ExampleDBHelper eDBHelper;

    ArrayList<String> proudct_type ;
    ArrayList<String> producttype_val;
    GPSTracker_New tracker;
    double dbl_latitude = 0;
    double dbl_longitude = 0;
    String map_address = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_case);
        String deviceMan = android.os.Build.MANUFACTURER;

        ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("NEW LOSID");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);


        tracker = new GPSTracker_New(this);

        rdb = new RequestDB(NewCase.this);
        cd = new ConnectionDetector(NewCase.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = getApplicationContext().getSharedPreferences("pData", 0);
        userFunction = new UserFunctions();
        str_user_id = pref.getString("user_id", null);
        str_branch_name = pref.getString("user_brnch", null);

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("map_img_name", null);
        peditor.putInt("is_map_captured", 0);
        peditor.commit();

        userFunction.resetData(NewCase.this);
        Calendar c = Calendar.getInstance();
        dTS = c.getTime();
        ts = getDateTime();
        date = getDateTime1();
        setLayout();

    }

    public Void setLayout() {

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putInt("map_cont", 0);
        peditor.putInt("is_map_captured", 0);
        peditor.commit();

        spn_Product_Type = (Spinner) findViewById(R.id.spn_product_type);
        spn_visit_Type = (Spinner) findViewById(R.id.spn_visitmode);

        edt_losid_no = (EditText) findViewById(R.id.edt_losid_no);
       // edt_app_form_no = (EditText) findViewById(R.id.edt_app_form_no);
        edt_customer_name = (EditText) findViewById(R.id.edt_customer_name);
        edt_branch_name = (EditText) findViewById(R.id.txt_customer_branch_name);
        edt_branch_name.setText(str_branch_name);
        edt_Number_Contacted = (EditText) findViewById(R.id.edt_Number_Contacted);
        txt_distacne=(EditText)findViewById(R.id.edt_distance);

        proudct_type=new ArrayList<>();
        producttype_val =new ArrayList<>();
        eDBHelper = new ExampleDBHelper(this);

        proudct_type=rdb.get_masters_pd();
        producttype_val=rdb.get_masters_pd_val();

        ArrayAdapter<String> adapter_state_p = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, proudct_type);
        adapter_state_p
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_Product_Type.setAdapter(adapter_state_p);
        spn_Product_Type.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state_v = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, Visit_Mode);
        adapter_state_v
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_visit_Type.setAdapter(adapter_state_v);
        spn_visit_Type.setOnItemSelectedListener(this);

        spn_distance_from_loc = (Spinner) findViewById(R.id.spn_distance_from_loc);

        adapter_distance_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, distance_type);
        adapter_distance_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);



        Button btn_locate_me = (Button) findViewById(R.id.btn_locate_me);
        btn_locate_me.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), NewCase.this);
                }
                str_losid_no = edt_losid_no.getText().toString();
                sr_app_form_no = edt_app_form_no.getText().toString();

                if (sr_app_form_no.equals("") && str_losid_no.equals("")) {
                    missingLOSID();
                } else {
                    map_captured = pref.getInt("is_map_captured", 0);

                    if (map_captured == 0) {
                        Intent img_pinch = new Intent(
                                NewCase.this, MapsActivity.class);
                        img_pinch.putExtra("str_losid_map", str_losid_no);
                        img_pinch.putExtra("str_app_form_no_map", sr_app_form_no);
                        img_pinch.putExtra("str_from_act", "NEW");
                        img_pinch.putExtra("str_ts", ts);

                        startActivity(img_pinch);
                    } else {
                        userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                                getApplicationContext());

                    }
                }

            }
        });

        Button btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
            }

        });


        Button btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {

                str_Number_Contacted = edt_Number_Contacted.getText()
                        .toString();
                str_losid_no = edt_losid_no.getText().toString();
                sr_app_form_no = edt_app_form_no.getText().toString();
                str_txt_distacne=txt_distacne.getText().toString();

                str_customer_name = edt_customer_name.getText().toString();
                str_Data_Entry_Type = "NW";


                DateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                Upload_Date = dateFormat.format(date);
                boolean ins = true;

                try {
                    if (sr_app_form_no.isEmpty() && str_losid_no.isEmpty() && ins == true) {
                        userFunction.cutomToast("Please Enter App Form no or LOSID",

                                NewCase.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_Data_Entry_Type.equals("") && ins == true) {
                        userFunction.cutomToast(
                                "Please Select Data Entry Type", NewCase.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    System.out.println("str_visit_mode : " + str_visit_mode);
                    if (str_visit_mode.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Visit Mode",
                                NewCase.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_product.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Product",
                                NewCase.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_visit_mode.equals("PH") && ins == true) {

                        if (str_Number_Contacted.equals("") == true) {
                            userFunction.cutomToast("Please Add Contact No",
                                    NewCase.this);
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_txt_distacne == null || str_txt_distacne.isEmpty() || str_txt_distacne.equals("null")) {
                        userFunction.cutomToast("Please Insert Distance",
                                NewCase.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }



                try {

                    if (str_distance_type == null && ins == true) {
                        userFunction.cutomToast("Please Select Locate-me Distance",
                                NewCase.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                long newRowId = 0;

                lattitude = pref.getString("pref_latitude", null);
                longitude = pref.getString("pref_longitude", null);


                SimpleDateFormat sdf = new SimpleDateFormat(
                        "yyyy-MM-dd");

                current_date = sdf.format(new Date());


                str_losid_no = edt_losid_no.getText().toString();
                str_customer_name = edt_customer_name.getText()
                        .toString();

                String img_new_path = pref.getString("map_img_name", null);

                if (img_new_path != null) {
                    MEDIA_MAP_COUNT = 1;
                } else {
                    MEDIA_MAP_COUNT = 0;
                }


                if (ins == true) {
                    String map_latitude = pref.getString("pref_latitude", "0");
                    String map_longitude = pref.getString("pref_longitude", "0");
                    map_address = pref.getString("map_address", null);

                    dbl_latitude = Double.parseDouble(map_latitude);
                    dbl_longitude = Double.parseDouble(map_longitude);

                    if (dbl_latitude > 0) {
                        lattitude = String.valueOf(dbl_latitude);
                        longitude = String.valueOf(dbl_longitude);

                    } else {
                        dbl_latitude = tracker.getLatitude();
                        dbl_longitude = tracker.getLongitude();
                        lattitude = String.valueOf(dbl_latitude);
                        longitude = String.valueOf(dbl_longitude);
                    }

                    insertDataSqlite();
                }

            }

        });

        return null;
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_product_type:
                // Do stuff for spinner1
                str_product = producttype_val.get(position).toString();

                System.out.println("str_product:::::"+str_product);

                if (str_product.equals("Select")) {
                    str_product = null;
                }
                break;

            case R.id.spn_visitmode:
                // Do stuff for spinner1
                str_visit_mode = Visit_Mode_val[position].toString();
                TableRow tblrow3 = (TableRow) findViewById(R.id.tableRow6);
                if (position == 1) {
                    tblrow3.setVisibility(View.VISIBLE);
                } else {
                    tblrow3.setVisibility(View.GONE);

                }
                if (position == 0) {
                    tblrow3.setVisibility(View.GONE);
                }
                break;

            case R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    private void missingLOSID() {
        userFunction.cutomToast("Please enter The Form No Or LOSID",
                getApplicationContext());
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    public void insertDataSqlite() {
        long newRowId;
        String str_status = "0";

        newRowId = rdb.insertNewCase(str_losid_no, sr_app_form_no, str_user_id,
                str_customer_name,
                str_visit_mode,
                Upload_Date, lattitude, longitude, ts, str_disance,
                str_product, str_distance_type, str_status,str_txt_distacne);

        if (newRowId > 0) {
            SQLiteDatabase dbs = rdb.getWritableDatabase();

            map_captured = pref.getInt("is_map_captured", 0);

            String map = Integer.toString(map_captured);
            String img_new_path = pref.getString("map_img_name", null);
            if (img_new_path != null) {
              //  edbHelper.insertImage(img_new_path, str_losid_no, str_app_form_no, str_user_id, ts, map, "", "0", "MAP");
                eDBHelper.insertImage(img_new_path, str_losid_no, sr_app_form_no, str_user_id, ts,map, "", "0", "MAP");

            }

            if (str_losid_no == null || str_losid_no.isEmpty() || str_losid_no.equals("null")) {
                str_losid_no = null;
            }

            if (sr_app_form_no == null || sr_app_form_no.isEmpty() || sr_app_form_no.equals("null")) {
                sr_app_form_no = null;
            }

            map_captured= pref.getInt("is_map_captured", 0);

            map_nw = String.valueOf(map_captured);
            userFunction.resetData(NewCase.this);
            Intent dashboardActivity = new Intent(
                    NewCase.this, MainActivity_OLD.class);
            dashboardActivity.putExtra("new_losid", str_losid_no);
            dashboardActivity.putExtra("new_formno", sr_app_form_no);
            dashboardActivity.putExtra("new_userid", str_user_id);
            dashboardActivity.putExtra("new_ts", ts);
            dashboardActivity.putExtra("new_map", map_nw);
            dashboardActivity.putExtra("new_last", newRowId);
            dashboardActivity.putExtra("id", "NEW");
            dashboardActivity.putExtra("lat", lattitude);
            dashboardActivity.putExtra("long", longitude);
            dashboardActivity.putExtra("ts_date", date);

            startActivity(dashboardActivity);
            finish();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cleanUpCancel();
    }

    public void cleanUpCancel() {
        spn_Product_Type.setSelection(0);
        spn_visit_Type.setSelection(0);
        edt_customer_name.setText(null);

    }

    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String format = s.format(new Date());
        return format;
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }
}
