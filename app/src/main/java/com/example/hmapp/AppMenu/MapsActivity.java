package com.example.hmapp.AppMenu;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.hdbLibrary.GPSTracker_New;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends FragmentActivity implements
        GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerDragListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "MapsActivity";
    double latitude;
    double longitude;
    GoogleMap googleMap;
    Button btn_refresh, btn_setlocation;
    SharedPreferences pref;
    String str_latitude;
    String str_longitude;
    String str_losid;
    String str_app_form_no;
    String str_ts;
    String str_user_id;
    RequestDB rdb;
    String str_from_act;
    Location location;
    UserFunctions userfunction;
    Marker mCurrLocationMarker;
    LatLng latLng;
    String addres = null;

    static  Double lat;
    static  Double lon;

    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_activity_maps);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        rdb = new RequestDB(MapsActivity.this);
        userfunction = new UserFunctions();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) MapsActivity.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        checkLocation();

        Intent asset = getIntent();
        str_losid = asset.getStringExtra("str_losid_map");
        str_app_form_no = asset.getStringExtra("str_app_form_no_map");
        str_from_act = asset.getStringExtra("str_from_act");
        str_ts = asset.getStringExtra("str_ts");



        str_user_id = pref.getString("user_id", null);

        if (str_losid.trim().length() == 0) {
            str_losid = null;
        }
        if (str_app_form_no.trim().length() == 0) {
            str_app_form_no = null;
        }

        System.out.println("str_losid_no:::" + str_losid);
        System.out.println("str_losid_no::sr_app_form_no:::" + str_app_form_no);


        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove("map_cont");
        peditor.commit();

        setlayout();

        btn_refresh = (Button) findViewById(R.id.btn_refresh);
        // mDrawerList.setAdapter(adapter);
        btn_setlocation = (Button) findViewById(R.id.btn_setlocation);


        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent img_pinch = new Intent(
                        MapsActivity.this, MapsActivity.class);
                startActivity(img_pinch);
                finish();*/

                setlayout();
            }

        });

        btn_setlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_latitude = String.valueOf(latitude);
                str_longitude = String.valueOf(longitude);

                if (str_latitude != null) {
                    CaptureMapScreen();
                } else {
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putInt("is_locate_clicked", 1);
                    peditor.commit();
                    userfunction.cutomToast("Location service is not active...", MapsActivity.this);
                }
                finish();
            }

        });
    }


    public void setlayout() {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //    googleMap.clear();
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();
            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMarkerDragListener(this);
            googleMap.setOnMapLongClickListener(MapsActivity.this);
            googleMap.setOnMapClickListener(MapsActivity.this);


            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
            GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);
            // check if location is available
            if (tracker.isLocationEnabled) {
                latitude = tracker.getLatitude();
                longitude = tracker.getLongitude();
            } else {
                // show dialog box to user to enable location
                tracker.askToOnLocation();
            }

            onLocationChanged();
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            if (longitude > 0) {
                AddmarkerforStore();
            }

        }
    }


    public void onLocationChanged() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        TextView tvLocation = (TextView) findViewById(R.id.tv_location);

        GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);
        // check if location is available
        if (tracker.isLocationEnabled()) {
            latitude = tracker.getLatitude();
            longitude = tracker.getLongitude();
            addres = tracker.getCompleteAddressString(latitude, longitude, MapsActivity.this);
        } else {
            // show dialog box to user to enable location
            tracker.askToOnLocation();
        }

        // Creating a LatLng object for the current location
        latLng = new LatLng(latitude, longitude);
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        if (latitude > 0) {
            AddmarkerforStore();
        }
        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" + latitude + ",Longitude:" + longitude + "\nAddress::" + addres);


    }


    public void AddmarkerforStore() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        // adding marker
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = googleMap.addMarker(markerOptions);

    }


    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onMarkerDrag(Marker arg0) {

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {
        LatLng dragPosition = arg0.getPosition();
        final double dragLat = dragPosition.latitude;
        final double dragLong = dragPosition.longitude;
    }

    @Override
    public void onMarkerDragStart(Marker arg0) {

    }

    @Override
    public void onMapClick(LatLng arg0) {
        //  map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
    }


    @Override
    public void onMapLongClick(LatLng arg0) {

        //create new marker when user long clicks
//        map.addMarker(new MarkerOptions()
//        .position(arg0)
//        .draggable(true));
    }

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }


    public void CaptureMapScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {

                    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), "HDB_PD");
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {

                        }
                    }
                    // Create a media file name
                    File mediaFile;
                    String new_path = mediaStorageDir.getPath() + File.separator + str_app_form_no + "_" + str_user_id + "_" + str_losid + "_"
                            + str_ts + "_" + str_from_act + "_" + "MAP_MAP.jpg";

                    System.out.println("new_path::::"+new_path);
                    mediaFile = new File(new_path);

                    FileOutputStream out = new FileOutputStream(mediaFile);
                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("map_img_name", new_path);
                    if(latitude>0) {
                        peditor.putInt("is_map_captured", 1);

                    }else{
                        peditor.putInt("is_map_captured", 0);
                    }

                    peditor.putInt("is_locate_clicked", 1);
                    peditor.putString("map_address", addres);

                    peditor.putString("pref_latitude", str_latitude);
                    peditor.putString("pref_longitude", str_longitude);
                    peditor.commit();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);
    }

    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        TextView tvLocation = (TextView) findViewById(R.id.tv_location);
        //  GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);
        if(isLocationEnabled()) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            getCompleteAddressString(lat, lon, MapsActivity.this);
            System.out.println("swdgdggggiudgiusgdigigd------");


        }
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        AddmarkerforStore();

        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" + lat + ",Longitude:" + lon);

    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public String getCompleteAddressString(double lattitude, double longitude ,Context cnt) {

        String strAdd = "";

        Geocoder geocoder = new Geocoder(cnt, Locale.getDefault());
        try {
            List<Address> addresses;
            addresses = geocoder.getFromLocation(lattitude, longitude, 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress
                            .append(returnedAddress.getAddressLine(i)).append(
                            "\n");
                }
                strAdd = strReturnedAddress.toString();
                System.out.println("swdgdggggiudgiusgdigigd"+strAdd);

                TextView tvLocation_add = (TextView) findViewById(R.id.tv_location);
                tvLocation_add.setText("Address:"+strAdd);
                Log.w(" location address", ""+ strReturnedAddress.toString());
            } else {
                Log.w(" location address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w(" location address", "Cannot get Address!");
        }
        return strAdd;
    }

}