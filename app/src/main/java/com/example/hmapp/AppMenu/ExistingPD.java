package com.example.hmapp.AppMenu;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MainActivity_OLD;

import com.example.hmapp.hdbLibrary.AppLocationService;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.ContentValues.TAG;


public class ExistingPD extends AppCompatActivity implements OnItemSelectedListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected String imgPath;
    protected JSONObject jsonobject;
    public static final String KEY_STATUS = "status";
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    EditText txt_distacne;
    String str_txt_distacne;
    EditText edt_Losid, edt_app_form_no, edt_remarks, edt_Cont_person_name, edt_link_losid, edt_vehicle_no, edt_reg_no;
    String str_Losid, str_app_form_no, str_remarks, str_Cont_person_name, str_link_losid, str_vehicle_no, str_reg_no;
    Spinner spn_Visit_Type;
    Spinner spn_Community;
    Spinner spn_Approach;
    Spinner spn_Reference;
    Spinner spn_coll_deployment;
    Spinner spn_pd_status;
    Spinner spn_coll_verfied;
    Spinner spn_contact_person;
    String asset_ver;
    String cust_status;
    Spinner spn_product, spn_asset_type;
    Spinner spn_residence_area;
    Spinner spn_residence_type;
    Spinner spn_residence_locality;

    Spinner spn_branch;
    String str_branch;
    TableRow tbl_asset_type,
            tbl_vehicle_no_1,
            tbl_vehicle_no_2;


    ArrayAdapter<String> adapter_visit_type;
    ArrayAdapter<String> adapter_community;
    ArrayAdapter<String> adapter_approach;
    ArrayAdapter<String> adapter_reference;
    ArrayAdapter<String> adapter_coll_deployment;
    ArrayAdapter<String> adapter_status;
    ArrayAdapter<String> adapter_coll_verified;
    ArrayAdapter<String> adapter_contact_person;
    ArrayAdapter<String> adapter_product;
    ArrayAdapter<String> adapter_asset_type;
    ArrayAdapter<String> adapter_residence_area;
    ArrayAdapter<String> adapter_residence_type;
    ArrayAdapter<String> adapter_residence_locality;
    String str_Visit_Type;
    String str_Community;
    String str_Approach;
    String str_Reference;
    String str_coll_deployment;
    String str_pd_status;
    String str_coll_verified;
    String str_contact_person;
    EditText edt_asset_customer_name;
    String str_coll_person_name;

    String str_product, str_asset_type, str_residence_area, str_residence_type, str_residence_locality;

    String[] visit_type = {"Select", "Residence", "Residence cum Office", "Office", "Plant", "Factory Site"};
    String[] contact_person = {"Select", "Applicant", "Co applicant", "Guarantor", "Director", "Partner", "Key Person", "Immediate Family Member", "Not Met"};
    String[] community = {"Select", "Yes", "No"};
    String[] approach = {"Select", "Easy to reach by four wheeler", "Difficult to reach by four wheeler", "Not Reachable"};
    String[] reference = {"Select", "Positive", "Negative", "Customer not known in area"};
    String[] pd_status = {"Select", "Recommended", "Not Recommended"};
    String[] coll_verified = {"Select", "Yes", "No"};
    String[] coll_deployment = {"Select", "AGRI_ACTIVITY",
            "SAND/STONE_QUARRY",
            "COAL_MINING",
            "OTHER_MINING",
            "CEMENT",
            "PETROLEUM",
            "EDIBLE_GOODS",
            "OTHER_GOODS",
            "ROAD_CONSTRUCTION",
            "BUILDING_MATERIAL",
            "PASSENGER_BUS",
            "SCHOOL/BPO",
            "OTHER"};

    String[] asset_type = {"Select", "New", "Used", "ReFinance"};
    String[] residence_type = {"Select", "Bungalow", "Hutman/Sitting Chawl", "Independent House", "Multi Tenant House", "Part of Independent House", "Row House", "Standing Chawl/Janta Flat", "Temporary shed", "Other"};
    String[] residence_locality = {"Select", "Lower Middle Class", "Middle Class", "Posh Locality", "Slums", "Upper Middle Class", "Village Area", "Other"};
    String[] residence_area = {"Select", "High Risk", "Medium Risk", "Low Risk"};
    ArrayList<String> proudct_type;
    ArrayList<String> producttype_val;
    ArrayList<String> branch_list;



    //CV
    RequestDB rdb;
    ExampleDBHelper edbHelper;
    String str_losid_no = null;
    SharedPreferences pref;
    SharedPreferences pData;
    String latitude, longitude, ts, offline_ts;
    Bitmap bmp;
    AppLocationService appLocationService;
    int is_coll_verified = 0;
    String str_user_id;
    private ConnectionDetector cd;
    String str_feedback, date;
    EditText edt_feedback;

    Spinner spn_distance_from_loc;
    String str_distance_type;

    ArrayAdapter<String> adapter_distance_type;

    String[] distance_type = {"Select", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    int MEDIA_MAP_COUNT;
    int map_captured = 0;
    TableRow tbl_9, tbl_11;
    TextView location_lat, location_long;
    String map_address = null;


    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;

    static  Double lat;
    static  Double lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pd_existing_pd);
        String deviceMan = android.os.Build.MANUFACTURER;

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("CUSTOMER PD");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

        userFunction = new UserFunctions();
        userFunction.resetData(ExistingPD.this);
        appLocationService = new AppLocationService(ExistingPD.this);
        cd = new ConnectionDetector(ExistingPD.this);
        edbHelper = new ExampleDBHelper(this);
        rdb = new RequestDB(ExistingPD.this);

        proudct_type = new ArrayList<>();
        producttype_val = new ArrayList<>();
        branch_list= new ArrayList<>();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = getApplicationContext().getSharedPreferences("pData", 0);
        str_user_id = pref.getString("user_id", null);

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("map_img_name", null);
        peditor.putInt("is_map_captured", 0);
        peditor.commit();


        Intent i = getIntent();
        str_losid_no = i.getStringExtra("pd_losid");
        str_app_form_no = i.getStringExtra("pd_form_no");
        str_product = i.getStringExtra("product_type");
        str_asset_type = i.getStringExtra("asset_type");


        tbl_asset_type = (TableRow) findViewById(R.id.tbl_0);
        tbl_vehicle_no_1 = (TableRow) findViewById(R.id.tbl_vehicle_no_1);
        tbl_vehicle_no_2 = (TableRow) findViewById(R.id.tbl_vehicle_no_2);

        edt_Losid = (EditText) findViewById(R.id.edt_losid);
        edt_app_form_no = (EditText) findViewById(R.id.edt_app_form);

        edt_Losid.setText(str_losid_no);
        edt_app_form_no.setText(str_app_form_no);


        edt_Cont_person_name = (EditText) findViewById(R.id.edt_contact_person_name);
        edt_link_losid = (EditText) findViewById(R.id.edt_link_acc);
        edt_vehicle_no = (EditText) findViewById(R.id.edt_vehicle_no);
        edt_feedback = (EditText) findViewById(R.id.edt_feedback);
        edt_reg_no = (EditText) findViewById(R.id.edt_reg_no);

        spn_Visit_Type = (Spinner) findViewById(R.id.spn_visit_type);

        spn_Approach = (Spinner) findViewById(R.id.spn_approach);
        spn_Reference = (Spinner) findViewById(R.id.spn_reference_check);
        spn_coll_deployment = (Spinner) findViewById(R.id.spn_coll_deployment);
        spn_pd_status = (Spinner) findViewById(R.id.spn_pd_status);
        spn_coll_verfied = (Spinner) findViewById(R.id.spn_coll_verfied);
        spn_contact_person = (Spinner) findViewById(R.id.spn_contact_person);
        spn_product = (Spinner) findViewById(R.id.spn_product);
        spn_asset_type = (Spinner) findViewById(R.id.spn_asset_type);
        spn_residence_area = (Spinner) findViewById(R.id.spn_residence_area);
        spn_residence_type = (Spinner) findViewById(R.id.spn_residence_type);
        spn_residence_locality = (Spinner) findViewById(R.id.spn_residence_locality);
        spn_branch = (Spinner) findViewById(R.id.spn_branch);
        txt_distacne = (EditText) findViewById(R.id.edt_distance);
        location_lat = (TextView) findViewById(R.id.location_lat);
        location_long = (TextView) findViewById(R.id.location_long);

        edt_asset_customer_name = (EditText) findViewById(R.id.edt_asset_customer_name);
        tbl_9 = (TableRow) findViewById(R.id.tbl_9);
        tbl_11 = (TableRow) findViewById(R.id.tbl_11);

        tbl_11.setVisibility(View.GONE);
        tbl_9.setVisibility(View.GONE);

        tbl_asset_type.setVisibility(View.GONE);
        tbl_vehicle_no_1.setVisibility(View.GONE);
        tbl_vehicle_no_2.setVisibility(View.GONE);

        ts = getDateTime();

        date = getDateTime1();

        proudct_type = rdb.get_masters_pd();
        System.out.println("aaaaaaaaaaaaaaaaaaaaa"+proudct_type);
        producttype_val = rdb.get_masters_pd_val();
        System.out.println("aaaaaaaaaaaaaaaaaaaaa"+producttype_val);
        branch_list = rdb.get_masters_branch();


        mGoogleApiClient = new GoogleApiClient.Builder(ExistingPD.this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) ExistingPD.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        checkLocation();




            location_lat.setText(latitude);
            location_long.setText(longitude);


        Button btn_locate_me = (Button) findViewById(R.id.btn_locate_me);
        // setImages();

        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), ExistingPD.this);
                }

                str_losid_no = edt_Losid.getText().toString();
                str_app_form_no = edt_app_form_no.getText().toString();


                if (str_app_form_no.equals("") && str_losid_no.equals("")) {
                    missingLOSID();
                } else {
                    map_captured = pref.getInt("is_map_captured", 0);

                    if (map_captured == 0) {
                        Intent img_pinch = new Intent(
                                ExistingPD.this, MapsActivity.class);
                        img_pinch.putExtra("str_losid_map", str_losid_no);
                        img_pinch.putExtra("str_app_form_no_map", str_app_form_no);
                        img_pinch.putExtra("str_from_act", "CU");
                        img_pinch.putExtra("str_ts", ts);
                        startActivity(img_pinch);
                    } else {
                        userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                                getApplicationContext());

                    }
                }

            }
        });


        adapter_visit_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, visit_type);
        adapter_visit_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn_Visit_Type.setAdapter(adapter_visit_type);
        spn_Visit_Type.setOnItemSelectedListener(this);


        adapter_approach = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, approach);
        adapter_approach
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_Approach.setAdapter(adapter_approach);
        spn_Approach.setOnItemSelectedListener(this);


        adapter_reference = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, reference);
        adapter_reference
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_Reference.setAdapter(adapter_reference);
        spn_Reference.setOnItemSelectedListener(this);


        adapter_coll_deployment = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, coll_deployment);
        adapter_coll_deployment
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_coll_deployment.setAdapter(adapter_coll_deployment);
        spn_coll_deployment.setOnItemSelectedListener(this);


        adapter_status = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, pd_status);
        adapter_status
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_pd_status.setAdapter(adapter_status);
        spn_pd_status.setOnItemSelectedListener(this);


        adapter_coll_verified = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, coll_verified);
        adapter_coll_verified
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_coll_verfied.setAdapter(adapter_coll_verified);
        spn_coll_verfied.setOnItemSelectedListener(this);


        adapter_contact_person = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, contact_person);
        adapter_contact_person
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_contact_person.setAdapter(adapter_contact_person);
        spn_contact_person.setOnItemSelectedListener(this);

/*
        adapter_product = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, product);
        adapter_contact_person
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_product.setAdapter(adapter_product);
        spn_product.setOnItemSelectedListener(this);
*/
        ArrayAdapter<String> adapter_state_p = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, proudct_type);
        adapter_state_p
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_product.setAdapter(adapter_state_p);
        spn_product.setOnItemSelectedListener(this);


        ArrayAdapter<String> adapter_state_b = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, branch_list);
        adapter_state_p
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_branch.setAdapter(adapter_state_b);
        spn_branch.setOnItemSelectedListener(this);


        adapter_asset_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, asset_type);
        adapter_asset_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_asset_type.setAdapter(adapter_asset_type);
        spn_asset_type.setOnItemSelectedListener(this);

        spn_distance_from_loc = (Spinner) findViewById(R.id.spn_distance_from_loc);

        adapter_distance_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, distance_type);
        adapter_distance_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);

        adapter_residence_area = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, residence_area);
        adapter_residence_area
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_residence_area.setAdapter(adapter_residence_area);
        spn_residence_area.setOnItemSelectedListener(this);

        adapter_residence_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, residence_type);
        adapter_residence_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_residence_type.setAdapter(adapter_residence_type);
        spn_residence_type.setOnItemSelectedListener(this);

        adapter_residence_locality = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, residence_locality);
        adapter_residence_locality
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_residence_locality.setAdapter(adapter_residence_locality);
        spn_residence_locality.setOnItemSelectedListener(this);

        if (str_losid_no != null) {
            edt_Losid.setEnabled(false);
            edt_app_form_no.setEnabled(false);

        }
        if (str_app_form_no != null) {
            edt_Losid.setEnabled(false);
            edt_app_form_no.setEnabled(false);

        }


        if (str_asset_type != null) {
            spn_asset_type.setSelection(getIndex(spn_asset_type, str_asset_type));
            // spn_asset_type.setEnabled(false);

        }


        Button btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean ins = true;
                str_txt_distacne = txt_distacne.getText().toString();

                str_losid_no = edt_Losid.getText().toString();
                str_app_form_no = edt_app_form_no.getText().toString();
                //    str_remarks = edt_remarks.getText().toString();
                str_Cont_person_name = edt_Cont_person_name.getText().toString();
                str_link_losid = edt_link_losid.getText().toString();
                str_feedback = edt_feedback.getText().toString();
                str_reg_no = edt_reg_no.getText().toString().replace(" ", "");
                str_vehicle_no = edt_vehicle_no.getText().toString().replace(" ", "");
                str_coll_person_name = edt_asset_customer_name.getText().toString();

                try {

                    if (str_losid_no.isEmpty() && str_app_form_no.isEmpty()&& ins == true) {
                        userFunction.cutomToast("Please Enter LOSID or App Form no",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_Visit_Type == null && ins == true) {
                        userFunction.cutomToast("Please Select Visit Type",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_product == null && ins == true) {
                        userFunction.cutomToast("Please Select Product Type",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_branch == null && ins == true) {
                        userFunction.cutomToast("Please Select Branch",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if (str_Visit_Type.equals("Residence") || str_Visit_Type.equals("Residence cum Office") && ins == true) {

                        if (str_residence_type == null) {

                            userFunction.cutomToast("Please Select  Type of Residence",
                                    ExistingPD.this);
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_Visit_Type.equals("Residence") || str_Visit_Type.equals("Residence cum Office") && ins == true) {
                        if (str_residence_locality == null && ins == true) {
                            userFunction.cutomToast("Please Select  Locality of Residence",
                                    ExistingPD.this);
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_Visit_Type.equals("Residence") || str_Visit_Type.equals("Residence cum Office") && ins == true) {
                        if (str_residence_area == null && ins == true) {
                            userFunction.cutomToast("Please Select Area of Residence",
                                    ExistingPD.this);
                            ins = false;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_contact_person == null && ins == true) {
                        userFunction.cutomToast("Please Select Contact Person",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_Cont_person_name.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter Contact Person Name",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    System.out.println("str_vehicle_no:::" + str_vehicle_no + ":::str_reg_no:::" + str_reg_no);
                    if (!str_vehicle_no.trim().equals(str_reg_no.trim())&& ins == true) {
                        userFunction.cutomToast("Vehicle registration No does not match with above Reg No.",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if (str_Community == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Community Dominated",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_feedback == null || str_feedback.isEmpty() || str_feedback.equals("null")&& ins == true) {
                        userFunction.cutomToast("Enter Feedback/Remarks",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_txt_distacne == null || str_txt_distacne.isEmpty() || str_txt_distacne.equals("null")&& ins == true) {
                        userFunction.cutomToast("Please Insert Distance",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_Community == "Yes" && ins == true) {
                        if (str_remarks.equals("") && ins == true) {
                            userFunction.cutomToast("Please Enter Remarks",
                                    ExistingPD.this);

                            ins = false;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_remarks.length() < 10 &&
                            ins == true) {
                        userFunction.cutomToast("Please Enter Atleast 10 characters in Remarks",
                                ExistingPD.this);
                        ins = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_Approach == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Approachability",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_Reference == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Reference Check",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_coll_deployment == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Collateral Deployment",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_pd_status == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select PD Status",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_coll_verified == "Select" && ins == true) {
                        userFunction.cutomToast("Please Select Collateral verified or not",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_distance_type == null && ins == true) {
                        userFunction.cutomToast("Please Select Locate Me Distance",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


               // String map_latitude = pref.getString("pref_latitude", "0");
               // String map_longitude = pref.getString("pref_longitude", "0");
                map_address = pref.getString("map_address", null);


                map_captured = pref.getInt("is_map_captured", 0);

                System.out.println("map_captured::::" + map_captured + "::::str_distance_type::::" + str_distance_type + "::::str_txt_distacne:::" + str_txt_distacne);
                try {
                    if (map_captured == 1 && ins == true) {

                        if (str_distance_type.equals("Map not loading") == true) {
                                userFunction.cutomToast("You already captured map... select other Distance type..",
                                        ExistingPD.this);
                                ins = false;

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (map_captured == 1 && ins == true) {
                        if ((str_distance_type.equals("Exact location")) || ((str_distance_type.equals("Map not loading")))) {
                            ins = true;
                        } else {
                            if (Integer.parseInt(str_txt_distacne) == 0) {
                                userFunction.cutomToast("Distance cant be a Zero...",
                                        ExistingPD.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


/*
                Integer int_is_locate_clicked = pref.getInt("is_locate_clicked", 0);
                try {

                    if (int_is_locate_clicked ==0) {
                        userFunction.cutomToast("select Map",
                                ExistingPD.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
*/

              //  latitude = pref.getString("pref_latitude", null);
              //  longitude = pref.getString("pref_longitude", null);

                String img_new_path = pref.getString("map_img_name", null);
                if (img_new_path != null) {
                    MEDIA_MAP_COUNT = 1;
                } else {
                    MEDIA_MAP_COUNT = 0;
                }
                if (ins == true) {


                    insertDataSqlite();
                }

            }

        });

        Button btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
            }

        });

        rdb.closeConnection();
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_visit_type:
                str_Visit_Type = visit_type[position];
                if (position == 0) {
                    str_Visit_Type = null;
                }

                TableRow tbl_31 = (TableRow) findViewById(R.id.tbl_31);
                TableRow tbl_32 = (TableRow) findViewById(R.id.tbl_32);
                TableRow tbl_33 = (TableRow) findViewById(R.id.tbl_33);
                if (position == 1 || position == 2) {
                    tbl_31.setVisibility(View.VISIBLE);
                    tbl_32.setVisibility(View.VISIBLE);
                    tbl_33.setVisibility(View.VISIBLE);
                } else {
                    tbl_31.setVisibility(View.GONE);
                    tbl_32.setVisibility(View.GONE);
                    tbl_33.setVisibility(View.GONE);
                }
                break;

            case R.id.spn_approach:
                str_Approach = approach[position];
                if (position == 0) {
                    str_Approach = null;
                }
                break;
            case R.id.spn_reference_check:

                str_Reference = reference[position];
                if (position == 0) {
                    str_Reference = null;
                }
                break;

            case R.id.spn_coll_deployment:
                str_coll_deployment = coll_deployment[position];
                if (position == 0) {
                    str_coll_deployment = null;
                }
                break;

            case R.id.spn_pd_status:
                str_pd_status = pd_status[position];
                if (position == 0) {
                    str_pd_status = null;
                }

            case R.id.spn_coll_verfied:
                str_coll_verified = coll_verified[position];
                if (position == 0) {
                    str_coll_verified = null;
                }

                if (position == 1) {
                    is_coll_verified = 1;
                } else {
                    is_coll_verified = 0;
                }

                break;
            case R.id.spn_contact_person:
                str_contact_person = contact_person[position];
                if (position == 0) {
                    str_contact_person = null;
                }
                break;

            case R.id.spn_branch:
                str_branch = branch_list.get(position).toString();
                if (position == 0) {
                    str_branch = null;
                }
                break;

            case R.id.spn_product:

                str_product = producttype_val.get(position).toString();
                System.out.println("str_product:::::" + str_product);

                if (str_product.equals("Select")) {
                    str_product = null;
                }

                if (str_product.equals("UCL") || str_product.equals("TRL") || str_product.equals("TW") || str_product.equals("CVEH")) {
                    spn_coll_deployment.setSelection(0);
                    spn_coll_deployment.setEnabled(true);
                } else {
                    spn_coll_deployment.setSelection(13);
                    spn_coll_deployment.setEnabled(false);
                }
                if (str_product.equals("UCL") || str_product.equals("TRL") || str_product.equals("TW") || str_product.equals("CVEH")) {
                    tbl_asset_type.setVisibility(View.VISIBLE);
                    tbl_vehicle_no_1.setVisibility(View.VISIBLE);
                    tbl_vehicle_no_2.setVisibility(View.VISIBLE);
                } else {
                    tbl_asset_type.setVisibility(View.GONE);
                    tbl_vehicle_no_1.setVisibility(View.GONE);
                    tbl_vehicle_no_2.setVisibility(View.GONE);
                }


                if (str_product.equals("UCL")) {
                    tbl_9.setVisibility(View.GONE);
                    tbl_11.setVisibility(View.GONE);

                } else {
                    tbl_9.setVisibility(View.VISIBLE);
                    tbl_11.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.spn_asset_type:
                str_asset_type = asset_type[position];
                if (position == 0) {
                    str_asset_type = null;
                }
                if (position == 1) {
                    tbl_vehicle_no_1.setVisibility(View.GONE);
                    tbl_vehicle_no_2.setVisibility(View.GONE);
                } else {
                    tbl_vehicle_no_1.setVisibility(View.VISIBLE);
                    tbl_vehicle_no_2.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;

            case R.id.spn_residence_type:
                str_residence_type = residence_type[position];
                if (position == 0) {
                    str_residence_type = null;
                }
                break;

            case R.id.spn_residence_locality:
                str_residence_locality = residence_locality[position];
                if (position == 0) {
                    str_residence_locality = null;
                }
                break;

            case R.id.spn_residence_area:
                str_residence_area = residence_area[position];
                if (position == 0) {
                    str_residence_area = null;
                }
                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    public void insertDataSqlite() {
        long newRowId = 0;
        cust_status = "0";

        newRowId = rdb.insertCustomers_Info(str_losid_no, str_app_form_no,str_branch,
                str_Visit_Type, str_link_losid, str_Cont_person_name,
                str_contact_person, str_Approach,
                str_Reference, str_coll_deployment, str_pd_status,
                str_coll_verified, str_remarks,
                str_user_id, latitude,
                longitude, ts, str_product, str_asset_type, str_vehicle_no, str_feedback, str_distance_type, str_residence_type, str_residence_locality, str_residence_area, str_coll_person_name, offline_ts, cust_status, str_txt_distacne, map_address);


        SQLiteDatabase dbs = rdb.getWritableDatabase();
        map_captured = pref.getInt("is_map_captured", 0);
        String map = Integer.toString(map_captured);
        String img_new_path = pref.getString("map_img_name", null);
            if (img_new_path != null) {

                edbHelper.insertImage(img_new_path, str_losid_no, str_app_form_no, str_user_id, ts, map, "", "0", "MAP");
        }


        if (newRowId > 0) {
/*
            if (is_coll_verified != 1) {
                Intent pd_form = new Intent(ExistingPD.this, Dashboard.class);
                startActivity(pd_form);
            } else {
                Intent ass_Verf = new Intent(ExistingPD.this, AssetVerified.class);
                ass_Verf.putExtra("pd_upload_by", str_user_id);
                ass_Verf.putExtra("pd_losid", str_losid_no);
                ass_Verf.putExtra("pd_form_no", str_app_form_no);
                startActivity(ass_Verf);
            }
            */
            if (str_losid_no == null || str_losid_no.isEmpty() || str_losid_no.equals("null")) {
                str_losid_no = null;
            }

            if (str_app_form_no == null || str_app_form_no.isEmpty() || str_app_form_no.equals("null")) {
                str_app_form_no = null;
            }

            Intent cust_img = new Intent(ExistingPD.this, MainActivity_OLD.class);

            cust_img.putExtra("cus_losid", str_losid_no);
            cust_img.putExtra("cus_formno", str_app_form_no);
            cust_img.putExtra("cus_userid", str_user_id);
            cust_img.putExtra("cus_ts", ts);
            cust_img.putExtra("cus_map", map);
            cust_img.putExtra("cus_last", newRowId);
            cust_img.putExtra("id", "CU");
            cust_img.putExtra("lat", latitude);
            cust_img.putExtra("long", longitude);
            cust_img.putExtra("ts_date", date);
            startActivity(cust_img);
            finish();

        } else {

            userFunction.resetData(ExistingPD.this);
            Intent dashboardActivity = new Intent(
                    ExistingPD.this, Dashboard.class);
            startActivity(dashboardActivity);
            finish();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cleanUpCancel();

    }

    public void cleanUpCancel() {


        edt_Cont_person_name.setText(null);
        edt_link_losid.setText(null);
        edt_vehicle_no.setText(null);
        edt_feedback.setText(null);
        edt_reg_no.setText(null);

        spn_Visit_Type.setSelection(0);

        spn_Approach.setSelection(0);
        spn_Reference.setSelection(0);
        spn_coll_deployment.setSelection(0);
        spn_pd_status.setSelection(0);
        spn_coll_verfied.setSelection(0);
        spn_contact_person.setSelection(0);
        spn_product.setSelection(0);
        spn_asset_type.setSelection(0);
        spn_residence_area.setSelection(0);
        spn_residence_type.setSelection(0);
        spn_residence_locality.setSelection(0);


    }


    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                        System.out.println(path + " : Deleted");
                    } else {
                        System.out.println(path + " : CANT Deleted");
                    }
                } else {
                    System.out.println(path + " : Do Not Exists");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }


    private void missingLOSID() {
        userFunction.cutomToast("Please Enter The App Form No or Losid",
                getApplicationContext());
    }


    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String format = s.format(new Date());
        return format;
    }


    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }


    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }




    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(getApplicationContext(), "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        System.out.println("LATITUDEeeeeeeeeeeeeeeeeee"+lat);
        lon = location.getLongitude();
        System.out.println("LATITUDEeeeeeeeeeeeeeeeeee"+lon);
        location_lat.setText("" + lat);
        location_long.setText("" + lon);
        latitude = String.valueOf(lat);
        longitude = String.valueOf(lon);
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new  AlertDialog.Builder (ExistingPD.this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
}