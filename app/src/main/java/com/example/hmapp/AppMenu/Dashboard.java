package com.example.hmapp.AppMenu;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.TrafficStats;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.GPSTracker;
import com.example.hmapp.hdbLibrary.LoanService;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONObject;

public class Dashboard extends AppCompatActivity {

    TextView txt_user_id, txt_user_name, txt_branch, txt_document_progress,
            txt_upload_progress;
    TextView text_error_msg;

    String str_user_id, str_user_name, str_branch;
    SharedPreferences pref;

    SharedPreferences pData;

    Button btn_dashboard, btn_data_entry;
    ImageButton btn_logout;
    UserFunctions userFunction;
    RequestDB rdbl;
    ExampleDBHelper edbHelp;
    JSONObject jsonobject = null;
    GPSTracker gps;
    Handler handler;
    private ConnectionDetector cd;
    ProgressDialog mProgressDialog;

    Button btn_asset_upload;
    Button btn_pd_upload;
    Button btn_appform_upload,btn_pic_list;
    TableRow tbl_pic_list;
    private Handler mHandler = new Handler();
    private long mStartRX = 0;
    private long mStartTX = 0;
    long bytes;
    long txbytes;
    double dts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_dshboard);
        cd = new ConnectionDetector(getApplicationContext());

        Intent in = new Intent(Dashboard.this, LoanService.class);
        startService(in);

        mStartRX = TrafficStats.getTotalRxBytes();
        mStartTX = TrafficStats.getTotalTxBytes();
        if (mStartRX == TrafficStats.UNSUPPORTED || mStartTX == TrafficStats.UNSUPPORTED) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Uh Oh!");
            alert.setMessage("Your device does not support traffic stat monitoring.");
            alert.show();
        } else {
           // mHandler.postDelayed(mRunnable, 10000);


        }

        userFunction = new UserFunctions();
        rdbl = new RequestDB(Dashboard.this);

        edbHelp = new ExampleDBHelper(Dashboard.this);
        handler = new Handler();

        //  handler.postDelayed(timedTask, 10000);

        gps = new GPSTracker(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = getApplicationContext().getSharedPreferences("pData", 0);
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        str_branch = pref.getString("user_brnch", null);

		/*
         * if (userFunction.isUserLoggedIn(getApplicationContext())) {
		 * startService(new Intent(this, GPSLocationService.class));
		 * startService(new Intent(this, LoanService.class)); SetLayout(); }
		 * else { Intent loginActivity = new Intent(Dashboard.this,
		 * LoginActivity.class); startActivity(loginActivity); finish(); }
		 */

        //userFunction.checkforLogout(pref, Dashboard.this);

        SetLayout();

        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);

        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("Upload In Progress");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

    }

    protected void SetLayout() {

        text_error_msg = (TextView) findViewById(R.id.text_error_msg);
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);

        TextView txt_pd_cnt = (TextView) findViewById(R.id.txt_pd_info_upload);
        TextView txt_asset_cnt = (TextView) findViewById(R.id.txt_asset_info_upload);
        TextView txt_form_cnt = (TextView) findViewById(R.id.txt_form_info_upload);
        TextView textView9 = (TextView) findViewById(R.id.txt_document_progress);
        TextView txt_case_progress = (TextView) findViewById(R.id.txt_cases_progress);

        btn_asset_upload = (Button) findViewById(R.id.btn_asset_upload);
        btn_pd_upload = (Button) findViewById(R.id.btn_pd_upload);
        btn_appform_upload = (Button) findViewById(R.id.btn_appform_upload);
        btn_data_entry = (Button) findViewById(R.id.btn_newcase_upload);
        btn_pic_list=(Button) findViewById(R.id.btn_pic_list);
        tbl_pic_list=(TableRow) findViewById(R.id.tbl_pic_list);
        Button btn_photo_upload = (Button) findViewById(R.id.btn_photo_upload);

        int int_cust_pending_count = 0;
        int int_asset_pending_count = 0;
        int int_app_form_pending_count = 0;
        int int_new_case_pending_count = 0;
        int PendingPhotoCoun = 0;


        int_cust_pending_count = Integer.parseInt(rdbl.getCustomerPendingCount());
        int_asset_pending_count = Integer.parseInt(rdbl.getAssetPendingCount());
        int_app_form_pending_count = Integer.parseInt(rdbl.getPD_formPendingCount());
        int_new_case_pending_count = Integer.parseInt(rdbl.getCasePendingCount());
        PendingPhotoCoun = Integer.parseInt(edbHelp.getPendingPhotoCount());


        textView9.setText(String.valueOf(PendingPhotoCoun));
        txt_form_cnt.setText(String.valueOf(int_app_form_pending_count));
        txt_asset_cnt.setText(String.valueOf(int_asset_pending_count));
        txt_pd_cnt.setText(String.valueOf(int_cust_pending_count));
        txt_case_progress.setText(String.valueOf(int_new_case_pending_count));

        if (int_cust_pending_count > 0) {
            btn_pd_upload.setBackgroundResource(R.drawable.btn_red);
        }
        if (int_asset_pending_count > 0) {
            btn_asset_upload.setBackgroundResource(R.drawable.btn_red);
        }
        if (int_app_form_pending_count > 0) {
            btn_appform_upload.setBackgroundResource(R.drawable.btn_red);
        }
        if (int_new_case_pending_count > 0) {
            btn_data_entry.setBackgroundResource(R.drawable.btn_red);
        }
        if (PendingPhotoCoun > 0) {
            btn_photo_upload.setBackgroundResource(R.drawable.btn_red);
            btn_pic_list.setVisibility(View.VISIBLE);
            textView9.setVisibility(View.VISIBLE);
        }else{
            btn_pic_list.setVisibility(View.GONE);
            textView9.setVisibility(View.GONE);

        }

        // ---set Custom Font to textview

        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);

        btn_pic_list.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(getApplicationContext(), ImagesList.class);
                    startActivity(intent);
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }

        });

        btn_data_entry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    rdbl.get_cases_request(null);
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }

        });

        btn_photo_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    new UploadPhoto().execute();
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });

        btn_pd_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    rdbl.get_Customers_request(null);

                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });

        btn_asset_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    rdbl.get_asset_request(null);
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });

        btn_appform_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    rdbl.get_app_form_request(null);
                } else {
                    userFunction.cutomToast("No internet connection...", Dashboard.this);
                }
            }
        });
    }


    public void go_home_acivity(View v) {
        // do stuff

        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    @SuppressWarnings("unused")
    private void tToast(String s) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, s, duration);
        toast.show();
    }

    public class UploadPhoto extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Dashboard.this);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                if (Integer.parseInt(edbHelp.getPendingPhotoCount()) > 0) {
                    if (Integer.parseInt(rdbl.getPD_formPendingCount()) == 0) {
                        if (Integer.parseInt(rdbl.getAssetPendingCount()) == 0) {
                            if (Integer.parseInt(rdbl.getCustomerPendingCount()) == 0) {
                                if (Integer.parseInt(rdbl.getCasePendingCount()) == 0) {

                                    // Only Starts if all the Requests are updated.
                                    String img_path = edbHelp.getphoto();
                                }
                            }
                            }

                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (Integer.parseInt(edbHelp.getPendingPhotoCount()) > 0) {
                if (Integer.parseInt(rdbl.getPD_formPendingCount()) == 0) {
                    if (Integer.parseInt(rdbl.getAssetPendingCount()) == 0) {
                        if (Integer.parseInt(rdbl.getCustomerPendingCount()) == 0) {
                            if (Integer.parseInt(rdbl.getCasePendingCount()) == 0) {


                            }
                        }else{
                            userFunction.cutomToast("first upload pending Customer PD",Dashboard.this);
                        }
                    }else{
                        userFunction.cutomToast("First upload pending Asset Info",Dashboard.this);
                    }
                }else{
                    userFunction.cutomToast("first upload pending PD Form",Dashboard.this);
                }
            }
            Intent messagingActivity = new Intent(Dashboard.this,
                    Dashboard.class);
            startActivity(messagingActivity);

            mProgressDialog.dismiss();
        }

    }



    @Override
    public void onBackPressed() {
        // your code.
        // to stop anonymous runnable use handler.removeCallbacksAndMessages(null);
        Intent intent = new Intent(getApplicationContext(), HdbHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private final Runnable mRunnable = new Runnable() {
        public void run() {
            if (cd.isConnectingToInternet()) {
                long rxBytes = TrafficStats.getTotalRxBytes() - mStartRX;
                // RX.setText(Long.toString(rxBytes));

                bytes = TrafficStats.getTotalTxBytes();
                txbytes = bytes - mStartTX;
                mStartTX = bytes;

                long lng_upload_bytes = txbytes / 1048;
                dts = (double) lng_upload_bytes;
            }
            mHandler.postDelayed(mRunnable, 10000);
        }
    };
}
