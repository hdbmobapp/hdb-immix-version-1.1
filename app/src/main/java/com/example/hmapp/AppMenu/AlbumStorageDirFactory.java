package com.example.hmapp.AppMenu;

/**
 * Created by Administrator on 07-09-2015.
 */
import java.io.File;

abstract class AlbumStorageDirFactory {
    public abstract File getAlbumStorageDir(String albumName);
}
