package com.example.hmapp.AppMenu;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MainActivity_OLD;
import com.example.hmapp.hdbLibrary.AppLocationService;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.GPSTracker_New;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.hmapp.hdbLibrary.ExampleDBHelper;


/**
 * Created by Aviansh on 21-08-2015.
 */
public class PDForm extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    SharedPreferences pref;
    SharedPreferences pData;
    private ConnectionDetector cd;
    RequestDB rdb;
    String str_losid_no = null;
    String str_app_form_no;

    protected JSONObject jsonobject;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    String latitude, longitude, ts;
    String str_user_id;
    String str_pd_uploaded_by;
    Spinner spn_distance_from_loc;
    String str_distance_type;

    String str_losid = null;
    String str_form_no = null;
    public static final String KEY_STATUS = "status";
    private static String KEY_SUCCESS = "success";
    AppLocationService appLocationService;
    String map_form, date;
    ArrayAdapter<String> adapter_distance_type;
    String[] distance_type = {"Select", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};
    ExampleDBHelper eDBHelper;

    int map_captured = 0;
    EditText txt_distacne;
    String str_txt_distacne;
    GPSTracker_New tracker;
    double dbl_latitude = 0;
    double dbl_longitude = 0;
    String map_address = null;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_form);
        String deviceMan = android.os.Build.MANUFACTURER;


        ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

        userFunction = new UserFunctions();
        userFunction.resetData(PDForm.this);
        eDBHelper = new ExampleDBHelper(this);
        tracker = new GPSTracker_New(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        pData = getApplicationContext().getSharedPreferences("pData", 0);

        str_user_id = pref.getString("user_id", null);

        Intent i = getIntent();
        str_pd_uploaded_by = i.getStringExtra("pd_upload_by");
        str_losid_no = i.getStringExtra("pd_losid");
        str_app_form_no = i.getStringExtra("pd_form_no");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("map_img_name", null);
        peditor.putInt("is_map_captured", 0);
        peditor.commit();



        spn_distance_from_loc = (Spinner) findViewById(R.id.spn_distance_from_loc);

        adapter_distance_type = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, distance_type);
        adapter_distance_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);
        txt_distacne=(EditText)findViewById(R.id.edt_distance);


        ts = getDateTime();
        date = getDateTime1();


        Button btn_locate_me = (Button) findViewById(R.id.btn_locate_me);
        // setImages();

        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(R.string.no_internet), PDForm.this);
                }
                map_captured = pref.getInt("is_map_captured", 0);

                if (map_captured == 0) {
                    Intent img_pinch = new Intent(
                            PDForm.this, MapsActivity.class);
                    img_pinch.putExtra("str_losid_map", str_losid_no);
                    img_pinch.putExtra("str_app_form_no_map", str_app_form_no);
                    img_pinch.putExtra("str_from_act", "FORM");
                    img_pinch.putExtra("str_ts", ts);

                    startActivity(img_pinch);
                } else {
                    userFunction.cutomToast(getResources().getString(R.string.map_cant_select),
                            getApplicationContext());
                }

            }
        });

        rdb = new RequestDB(PDForm.this);
        cd = new ConnectionDetector(PDForm.this);

        Button btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean ins = true;


                str_txt_distacne=txt_distacne.getText().toString();

                String img_new_path = pref.getString("map_img_name", null);


                if (ins == true) {
                    String map_latitude = pref.getString("pref_latitude", "0");
                    String map_longitude = pref.getString("pref_longitude", "0");
                    map_address = pref.getString("map_address", null);

                    dbl_latitude = Double.parseDouble(map_latitude);
                    dbl_longitude = Double.parseDouble(map_longitude);

                    if (dbl_latitude > 0) {
                        latitude = String.valueOf(dbl_latitude);
                        longitude = String.valueOf(dbl_longitude);

                    } else {
                        dbl_latitude = tracker.getLatitude();
                        dbl_longitude = tracker.getLongitude();
                        latitude = String.valueOf(dbl_latitude);
                        longitude = String.valueOf(dbl_longitude);
                    }

                    insertDataSqlite();
                }

            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
    }

    public void insertDataSqlite() {
        long newRowId = 0;

        newRowId = rdb.insertPDInfo(str_losid_no, str_app_form_no, str_user_id,
                latitude, longitude, ts, str_distance_type,str_txt_distacne);

        if (newRowId > 0) {

            SQLiteDatabase dbs = rdb.getWritableDatabase();


            String img_new_path = pref.getString("map_img_name", null);
            if (img_new_path != null) {
                eDBHelper.insertImage(img_new_path, str_losid, str_form_no, str_user_id, ts, "", "", "0", "MAP");
            }
            if (str_losid_no == null || str_losid_no.isEmpty() || str_losid_no.equals("null")) {
                str_losid_no = null;
            }

            if (str_app_form_no == null || str_app_form_no.isEmpty() || str_app_form_no.equals("null")) {
                str_app_form_no = null;
            }

            map_captured = pref.getInt("is_map_captured", 0);

            map_form = String.valueOf(map_captured);
            Intent dash = new Intent(PDForm.this, MainActivity_OLD.class);
            dash.putExtra("form_losid", str_losid_no);
            dash.putExtra("form_formno", str_app_form_no);
            dash.putExtra("form_userid", str_user_id);
            dash.putExtra("form_ts", ts);
            dash.putExtra("form_map", map_form);
            dash.putExtra("form_last", newRowId);
            dash.putExtra("id", "FORM");
            dash.putExtra("lat", latitude);
            dash.putExtra("long", longitude);
            dash.putExtra("ts_date", date);
            startActivity(dash);
            finish();

        } else {

            userFunction.resetData(PDForm.this);
            Intent dashboardActivity = new Intent(
                    PDForm.this, Dashboard.class);
            startActivity(dashboardActivity);
            finish();
        }
        System.out.println("newRowId::::newRowId:::" + newRowId);

    }

    private void missingLOSID() {
        userFunction.cutomToast("Please Enter The App Form No",
                getApplicationContext());
    }

    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String format = s.format(new Date());
        return format;
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }


    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }
}
