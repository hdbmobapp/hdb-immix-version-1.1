package com.example.hmapp.AppMenu;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.LoadMoreListView;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MyCases extends AppCompatActivity {

    // Within which the entire activity is enclosed

    // ListView represents Navigation Drawer
    ListView mList;
    public int pgno = 0;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;

    // Title of the action bar
    String mTitle = "";
    static LinearLayout drawerll;
    static String str_region;
    SharedPreferences pref;

    String str_user_id;
    String str_user_name;
    String str_losid_no;

    MyCasesListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;

    static String FORM_NO = "Form_No";
    static String LOSID = "LOSID";
    static String PD_STATUS = "PD_Status";
    static String VISIT_MODE = "Visited_At";
    static String COLL_VERIFIED = "Coll_verified";
    static String PF_PIC_COUNT = "PF_PIC_COUNT";
    static String CO_PIC_COUNT = "CO_PIC_COUNT";
    static String CU_PIC_COUNT = "CU_PIC_COUNT";
    static String PD_UPLOAD_BY = "CU_PIC_COUNT";
    static String NEW_CASE_UPLOAD_BY = "NEW_PIC_COUNT";
    static String PD_Product_type = "Product_type";
    static String PD_Asset_type = "Asset_type";
    static String PD_Customer_name = "PD_customer_name";
    static String CO_Customer_name = "CO_customer_name";


    UserFunctions userfunction;
    TextView txt_error;
    EditText edt_losid;
    Button btn_losid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_my_cases);
        mTitle = (String) getTitle();
        userFunction = new UserFunctions();
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);

        System.out.println("user_id:::" + str_user_id);

        Intent i = getIntent();
        str_region = i.getStringExtra("str_region");

        setlayout();


        cd = new ConnectionDetector(MyCases.this);

        if (cd.isConnectingToInternet()) {

            new DownloadJSON().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }
        // Setting the adapter on mDrawerList
        // mDrawerList.setAdapter(adapter);


        btn_losid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_user_id = null;
                str_losid_no = edt_losid.getText().toString();

                if (str_losid_no != null) {
                    if (cd.isConnectingToInternet()) {

                        new DownloadJSON().execute();
                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Please Select The Form No",
                            getApplicationContext());
                }

            }

        });

        ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("My Uploads");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

    }


    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        arraylist = new ArrayList<HashMap<String, String>>();

        mList = (ListView) findViewById(R.id.list);

        edt_losid = (EditText) findViewById(R.id.editText);

        btn_losid = (Button) findViewById(R.id.button);


    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(MyCases.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            jsonobject = userFunction.getCaseList(str_user_id, str_losid_no, "0");
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");
//                    txt_error.setVisibility(View.GONE);

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("LOSID", jsonobject.getString("LOSID"));
                    map.put("PD_Status",
                            jsonobject.getString("PD_Status"));
                    map.put("Visited_At", jsonobject.getString("Visited_At"));
                    map.put("CU_PIC_COUNT", jsonobject.getString("CU_PIC_COUNT"));
                    map.put("CO_PIC_COUNT", jsonobject.getString("CO_PIC_COUNT"));
                    map.put("PF_PIC_COUNT", jsonobject.getString("PF_PIC_COUNT"));
                    map.put("Form_No", jsonobject.getString("Form_No"));
                    map.put("Product_type", jsonobject.getString("Product_type"));
                    map.put("Asset_type", jsonobject.getString("Asset_type"));
                    map.put("NEW_PIC_COUNT", jsonobject.getString("NEW_PIC_COUNT"));
                    map.put("CO_customer_name", jsonobject.getString("CO_customer_name"));
                    map.put("PD_customer_name", jsonobject.getString("PD_customer_name"));


                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                adapter = new MyCasesListAdapter(MyCases.this, arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

                System.out.println("INSIDE::: post excdf:");
                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });

            }
            mProgressDialog.dismiss();

        }

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }


    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //jsonobject = userFunction.getVendorOrders(store_id,order_status,tmpgno);


            jsonobject = userFunction.getCaseList(str_user_id, str_losid_no, tmpgno);

            // Create an array
            if (jsonobject != null) try {
                jsonarray = jsonobject.getJSONArray("Data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("LOSID", jsonobject.getString("LOSID"));
                    map.put("PD_Status",
                            jsonobject.getString("PD_Status"));
                    map.put("Visited_At", jsonobject.getString("Visited_At"));
                    map.put("CU_PIC_COUNT", jsonobject.getString("CU_PIC_COUNT"));
                    map.put("CO_PIC_COUNT", jsonobject.getString("CO_PIC_COUNT"));
                    map.put("PF_PIC_COUNT", jsonobject.getString("PF_PIC_COUNT"));
                    map.put("Form_No", jsonobject.getString("Form_No"));
                    map.put("Product_type", jsonobject.getString("Product_type"));
                    map.put("Asset_type", jsonobject.getString("Asset_type"));
                    map.put("NEW_PIC_COUNT", jsonobject.getString("NEW_PIC_COUNT"));
                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }

}
