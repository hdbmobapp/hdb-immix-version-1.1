package com.example.hmapp.hdbLibrary;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.AppMenu.Dashboard;
import com.example.hmapp.AppMenu.RequestDB;

import org.json.JSONException;
import org.json.JSONObject;

public class LoanService extends Service {

    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    public static Runnable runnable_123 = null;


    GPSTracker gps;
    RequestDB requestdb;
    UserFunctions userfunction;

    private static final String IMAGE_DIRECTORY_NAME = "/HDB";

    JSONObject jsonobject;
    String GPS_Latitiude;
    String GPS_Longitude;
    SharedPreferences pref;
    String str_user_id;

    private static String KEY_SUCCESS = "success";
    private static String KEY_STATUS = "status";
    private ConnectionDetector cd;
    ExampleDBHelper dbhelper;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        cd = new ConnectionDetector(getApplicationContext());
        userfunction = new UserFunctions();
        gps = new GPSTracker(getApplicationContext());
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                try {

                    Integer int_cust_pending_count = 0;
                    Integer int_asset_pending_count = 0;
                    Integer int_app_form_pending_count = 0;
                    Integer int_new_case_pending_count = 0;

                    Integer PendingPhotoCoun = 0;

                    requestdb = new RequestDB(LoanService.this);
                    userfunction = new UserFunctions();
                    if (cd.isConnectingToInternet()) {

                        int_cust_pending_count = Integer.parseInt(requestdb.getPD_formPendingCount());

                        if (int_cust_pending_count > 0) {
                            requestdb.get_Customers_request(null);
                        }
                        int_asset_pending_count = Integer.parseInt(requestdb.getAssetPendingCount());
                        if (int_asset_pending_count > 0) {
                            requestdb.get_asset_request(null);
                        }
                        int_app_form_pending_count = Integer.parseInt(requestdb.getPD_formPendingCount());
                        if (int_app_form_pending_count > 0) {
                            requestdb.get_app_form_request(null);
                        }
                        int_new_case_pending_count = Integer.parseInt(requestdb.getCasePendingCount());
                        if (int_new_case_pending_count > 0) {
                            requestdb.get_cases_request(null);
                        }
                    }

                    try {
                        PendingPhotoCoun = Integer.parseInt(requestdb.getPendingPhotoCount());

                        if (int_cust_pending_count == 0) {
                            if (int_asset_pending_count == 0) {
                                if (int_app_form_pending_count == 0) {
                                    if (int_new_case_pending_count == 0) {
                                        if (PendingPhotoCoun > 0) {
                                            // Only Starts if all the Requests are updated.
                                            String img_path = requestdb.getphoto();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                handler.postDelayed(runnable, 900000);
            }
        };
        handler.postDelayed(runnable, 900000);


        runnable_123 = new Runnable() {
            @Override
            public void run() {
                try {
                    boolean is_notifyed = false;

                    Integer int_cust_pending_count = 0;
                    Integer int_asset_pending_count = 0;
                    Integer int_app_form_pending_count = 0;
                    Integer int_new_case_pending_count = 0;

                    Integer PendingPhotoCoun = 0;
                    dbhelper = new ExampleDBHelper(LoanService.this);
                    requestdb = new RequestDB(LoanService.this);
                    userfunction = new UserFunctions();

                    int_cust_pending_count = Integer.parseInt(requestdb.getPD_formPendingCount());

                    if (int_cust_pending_count > 0) {
                        is_notifyed = true;
                    }
                    int_asset_pending_count = Integer.parseInt(requestdb.getAssetPendingCount());
                    if (int_asset_pending_count > 0) {
                        is_notifyed = true;
                    }
                    int_app_form_pending_count = Integer.parseInt(requestdb.getPD_formPendingCount());
                    if (int_app_form_pending_count > 0) {
                        is_notifyed = true;
                    }
                    int_new_case_pending_count = Integer.parseInt(requestdb.getCasePendingCount());
                    if (int_new_case_pending_count > 0) {
                        is_notifyed = true;
                    }
                    PendingPhotoCoun = Integer.parseInt(dbhelper.getPendingPhotoCount());
                    if (PendingPhotoCoun > 0) {
                        is_notifyed = true;
                    }

                    if (is_notifyed == true) {
                        Integer my_count = int_cust_pending_count +
                                int_asset_pending_count +
                                int_app_form_pending_count +
                                int_new_case_pending_count + PendingPhotoCoun;
                        if (my_count > 0) {
                            send_notification("You have " + my_count + " Files Pending in your Mobile Click here ....", context);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                handler.postDelayed(runnable_123, 800000);
            }
        };
        handler.postDelayed(runnable_123, 800000);

    }

    @Override
    public void onDestroy() {
        /*

		 * IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE
		 * FOLLOWING LINE
		 */
        // handler.removeCallbacks(runnable);
        // Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onStart(Intent intent, int startid) {
        // Toast.makeText(this, "Service started by user.",
        // Toast.LENGTH_LONG).show();
    }


    public void send_notification(String msg, Context context) {

        Intent intent = new Intent(context, Dashboard.class);

        System.out.println("INSIDE::::notification::::");
        int icon = R.drawable.ic_launcher;
        int mNotificationId = 001;
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context);
        Notification notification = mBuilder.setSmallIcon(icon).setTicker("HDB-PD").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle("HDB-PD")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                .setContentText(msg).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notification);
    }

}