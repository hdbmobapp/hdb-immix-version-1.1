package com.example.hmapp.hdbLibrary;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;


import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

@SuppressLint("NewApi")
public class UserFunctions extends Activity {

    private JSONParser jsonParser;
    // private JsonParserService jsonParserservice;
    SharedPreferences pref;

    private static String URL = "https://hdbapp.hdbfs.com/HDB_PD_Com/HDB_PD_V4.0/index.php";
    private static String IndexURLGET = "https://hdbapp.hdbfs.com/HDB_PD_Com/HDB_PD_V4.0/index_get.php";
    Context context;
    SharedPreferences pData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //  setContentView(R.layout.setting);
    }

    // constructor
    public UserFunctions() {
        jsonParser = new JSONParser();
    }


    public JSONObject loginUser(String username, String password, String imei_no, String latitude, String longitude, String device_name, String device_man, String device_id,String imei1,String imei2) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "login");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("username", username);
            map.put("password", password);
            map.put("imei", imei_no);
            map.put("device_name", device_name);
            map.put("device_man", device_man);
            map.put("device_id", device_id);
            map.put("app_version", "HDB-PD Version 4.0");
            map.put("imei1", imei1);
            map.put("imei2", imei2);

            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);
            //Log.e("JSON", json.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject GpsLoction(String user_id, String latitude, String longitude) {
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "save");
            map.put("target", "location");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("user_id", user_id);
            map.put("app_version", "HDB-PD Version 4.0");
            json = jsonParser.makeHttpRequest_new(URL, "POST", map, UserFunctions.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getFAQList(String flag, String str_keywrd) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "faq");
            map.put("target", "loan");
            map.put("flag", flag);
            map.put("str_keyword", str_keywrd);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public void resetData(Context context) {
        pData = context.getSharedPreferences("pData", 0); // 0 -

        //String str_timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(new Date();

        DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String str_timestamp = sdf.format(new Date());

        Editor dEditor = pData.edit();

        dEditor.putString("str_DOC_1", "");
        dEditor.putString("str_DOC_2", "");
        dEditor.putString("str_DOC_3", "");
        dEditor.putString("str_DOC_4", "");
        dEditor.putString("str_DOC_5", "");
        dEditor.putString("str_DOC_6", "");
        dEditor.putString("str_DOC_7", "");
        dEditor.putString("str_DOC_8", "");
        dEditor.putString("str_DOC_9", "");
        dEditor.putString("str_DOC_10", "");
        dEditor.putString("RAW", "");
        dEditor.putString("str_timestamp", str_timestamp);

        dEditor.commit();
    }


    public JSONObject InsertData(String str_app_form_no,
                                 String str_Losid,
                                 String str_branch,
                                 String str_link_acc,
                                 String str_Visit_Type,
                                 String str_contact_person,
                                 String str_Cont_person_name,
                                 //  String str_Community,
                                 String str_remarks,
                                 String str_Approach,
                                 String str_Reference,
                                 String str_coll_deployment,
                                 String str_pd_status,
                                 String str_coll_verified,
                                 String latitude,
                                 String longitude,
                                 String ts,
                                 String UserID,
                                 String product,
                                 String asset_type,
                                 String str_vehicle_no,
                                 String str_feedback,
                                 String str_distance,
                                 String str_residence_type,
                                 String str_residence_locality,
                                 String str_residence_area,
                                 String str_pd_photo_count,
                                 String str_pd_customer_name,
                                 String cu_distance_km,
                                 String str_pd_map_address

    ) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "save");
            map.put("target", "loan");
            map.put("Lat", latitude);
            map.put("Long", longitude);
            map.put("LOSID", str_Losid);
            map.put("Form_No", str_app_form_no);
            map.put("str_branch", str_branch);

            map.put("str_link_acc", str_link_acc);
            map.put("Visited_At", str_Visit_Type);
            //   map.put("Comm_Dominated", str_Community);
            map.put("Cnt_Person", str_contact_person);
            map.put("Cnt_Person_Name", str_Cont_person_name);
            map.put("Remarks", str_remarks);
            map.put("Approach", str_Approach);
            map.put("Neighbour_Check", str_Reference);
            map.put("Coll_Deployment", str_coll_deployment);
            map.put("PD_Status", str_pd_status);
            map.put("UserID", UserID);
            map.put("Coll_verified", str_coll_verified);
            map.put("str_version", "HDB-PD Version 4.0");
            map.put("product", product);
            map.put("asset_type", asset_type);
            map.put("reg_no", str_vehicle_no);
            map.put("feedback", str_feedback);
            map.put("distance", str_distance);
            map.put("residence_type", str_residence_type);
            map.put("residence_locality", str_residence_locality);
            map.put("residence_area", str_residence_area);
            map.put("pic_count", str_pd_photo_count);
            map.put("customer_name", str_pd_customer_name);
            map.put("cu_ts", ts);
            map.put("cu_distance_km", cu_distance_km);
            map.put("map_address", str_pd_map_address);

            System.out.println("REQ PARAMS " + map.toString());
            Log.e("REQ PARAMS", map.toString());
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject InsertDataAsset(String str_losid,
                                      String app_form_no,
                                      String str_branch,
                                      String str_coll_verified_at,
                                      String str_coll_verified_with,
                                      String str_contact_person,
                                      String str_coll_person_name,
                                      String latitude,
                                      String longitude,
                                      String str_user_id,
                                      String str_product,
                                      String str_asset_type,
                                      String str_vehicle_no,
                                      String str_distance,
                                      String str_coll_photo_count,
                                      String str_coll_customer_name,
                                      String co_ts,
                                      String co_distance_km,
                                      String co_remarks,
                                      String str_collx_map_address


    ) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "update");
            map.put("target", "loan");
            map.put("LOSID", str_losid);
            map.put("Form_No", app_form_no);
            map.put("str_branch", str_branch);
            map.put("Coll_Visited_At", str_coll_verified_at);
            map.put("coll_verified_with", str_coll_verified_with);
            map.put("Cnt_Person_Name", str_coll_person_name);
            map.put("Cnt_Person_type", str_contact_person);
            map.put("pd_upload_by", str_user_id);
            map.put("Lat", latitude);
            map.put("Long", longitude);
            map.put("Coll_verified", "1");
            map.put("product", str_product);
            map.put("asset_type", str_asset_type);
            map.put("reg_no", str_vehicle_no);
            map.put("distance", str_distance);
            map.put("str_version", "HDB-PD Version 4.0");
            map.put("pic_count", str_coll_photo_count);
            map.put("customer_name", str_coll_customer_name);
            map.put("co_ts", co_ts);
            map.put("co_distance_km", co_distance_km);
            map.put("co_remarks", co_remarks);
            map.put("map_address", str_collx_map_address);


            System.out.println("REQ PARAMS " + map.toString());
            Log.e("REQ PARAMS", map.toString());
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {

            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject InsertDataForm(String str_losid,
                                     String app_form_no,
                                     String latitude,
                                     String longitude,
                                     String str_user_id,
                                     String str_distance,
                                     String str_form_photo_count,
                                     String form_ts,
                                     String distance_km

    ) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "form_insert");
            map.put("target", "loan");
            map.put("LOSID", str_losid);
            map.put("Form_No", app_form_no);
            map.put("form_upload_by", str_user_id);
            map.put("Lat", latitude);
            map.put("Long", longitude);
            map.put("str_version", "HDB-PD Version 4.0");
            map.put("distance", str_distance);
            map.put("pic_count", str_form_photo_count);
            map.put("form_ts", form_ts);
            map.put("fm_distance_km", distance_km);


            System.out.println("REQ PARAMS " + map.toString());
            Log.e("REQ PARAMS", map.toString());
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {

            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    /**
     * function make Login Request
     * @param name
     * @param email
     * @param password
     * */
    /**
     * Function get Login status
     */
    public boolean isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }


    /**
     * Function to logout user
     * Reset Database
     */
    public boolean logoutUser(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }

    public boolean logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }


    public JSONObject getCaseList(String user_id, String losid_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getCU_PDCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_pd");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);

            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getCO_PDCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_coll");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);
            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getFORM_PDCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_form");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);
            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getNEW_LOSCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_nc");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);
            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getSingleCase(String user_id, String losid_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "single_case");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public void cutomToast(String message, Context contxt) {
        // Inflate the Layout
        ViewGroup parent = null;
        LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.pd_custom_toast,
                parent, false);
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(contxt);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    //new case
    public JSONObject InsertNewCaseData(String locid, String new_app_form_no, String user_id,
                                        String customer_name,
                                        String Visit_Mode,
                                        String str_Upload_Date,
                                        String ts,
                                        String product, String lattitude,
                                        String longitude,
                                        String str_distance,
                                        String str_distance_km,
                                        String str_pic_count
    ) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "new_case");
            map.put("target", "loan");
            map.put("LOSID", locid);
            map.put("app_form_no", new_app_form_no);
            map.put("Customer_Name", customer_name);
            map.put("Visit_Mode", Visit_Mode);
            map.put("user_id", user_id);
            map.put("Upload_Date", str_Upload_Date);
            map.put("ts", ts);
            map.put("latitude", lattitude);
            map.put("longitude", longitude);
            map.put("product", product);
            map.put("distance", str_distance);
            map.put("str_distance_km", str_distance_km);
            map.put("pic_count", str_pic_count);

            System.out.println("REQ PARAMS " + map.toString());
            Log.e("REQ PARAMS", map.toString());
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public void cutomToastAct(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.pd_custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void noInternetConnection(String message, Context contxt) {
        Toast toast = null;
        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            ViewGroup parent = null;
            LayoutInflater inflater = (LayoutInflater) contxt
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.pd_toast_no_internet, parent,
                    false);
            TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
            mesg_name.setText(message);
            // Create Custom Toast
            toast = new Toast(contxt);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }

    public Void checkforLogout(SharedPreferences pref, Context contextt) {
        String str_last_date = pref.getString("current_date", null);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = sdf.format(new Date());

        Date date1 = null;
        Date date2 = null;
        try {
            if (current_date != null) {
                date1 = sdf.parse(current_date);
            }
            if (str_last_date != null) {
                date2 = sdf.parse(str_last_date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (current_date != null && str_last_date != null) {
            if (date1.after(date2)) {
                Editor editor = pref.edit();
                editor.putInt("is_login", 0);
                editor.commit();
                Intent i = new Intent(contextt, HdbHome.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                contextt.startActivity(i);
                finish();
            }
        }
        return null;
    }

    public JSONObject getRemarks(String losid) {
        JSONObject json = null;


        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("losid", losid);
            map.put("action", "search");
            map.put("target", "search_remarks");

            //  map.put("type", str_type);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
//		Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject insertRemark(String losid, String remark, String remark_by_id, String remark_by_name, String designation) {
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "save");
            map.put("target", "add_remarks");
            map.put("losid", losid);
            map.put("remark", remark);
            map.put("remark_by_id", remark_by_id);
            map.put("remark_by_name", remark_by_name);
            map.put("designation", designation);
            //  map.put("type", str_type);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
//		Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getMessageBox(String user_id, String page_no) {

        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("userid", user_id);
            map.put("target", "messages");
            map.put("action", "search");
            map.put("messages_of", "PD");

            map.put("pg", page_no);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject update_message(String str_msg_id, String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("target", "messages");
            map.put("action", "update");
            map.put("userid", str_user_id);
            map.put("msg_id", str_msg_id);
            map.put("messages_of", "PD");

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_msg_count(String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("target", "messages");
            map.put("action", "count");
            map.put("userid", str_user_id);
            map.put("messages_of", "PD");

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject update_newLos(String str_fin_losid, String str_pg_from, String str_sr_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("target", "loan");
            map.put("action", "update_los");
            map.put("sr_no", str_sr_no);
            map.put("str_fin_los", str_fin_losid);
            map.put("str_pg_from", str_pg_from);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject update_Masters(String str_userid, String str_master_tbl) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("target", "loan");
            map.put("action", "getmasters");
            map.put("userid", str_userid);
            map.put("master_id", str_master_tbl);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_masters(String id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getmasters");
            map.put("target", "loan");
            map.put("id", id);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_branch_masters(String count,String str_userid) {
        JSONObject json = null;
        try {
            System.out.println("str_userid:::"+str_userid);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_branch_masters");
            map.put("target", "loan");
            map.put("count", count);
            map.put("str_userid", str_userid);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject check_masters(String flag_dc, String flag_pm, String id_dc, String id_pm, String flag_pd, String id_pd) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "verify_masters");
            map.put("target", "loan");
            map.put("flag_dc", flag_dc);
            map.put("flag_pm", flag_pm);
            map.put("id_dc", id_dc);
            map.put("id_pm", id_pm);

            map.put("flag_pd", flag_pd);
            map.put("id_pd", id_pd);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }
}
