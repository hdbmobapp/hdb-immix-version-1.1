package com.example.hmapp.hdbLibrary;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by obaro on 02/04/2015.
 */
public class ExampleDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "sql_lit_pd_com";
    private static final int DATABASE_VERSION = 4;

    public static final String IMAGE_TABLE_NAME = "tbl_image_master_pd";
    public static final String IMAGE_COLUMN_ID = "_id";
    public static final String IMAGE_LOSID = "img_losid";
    public static final String IMAGE_FORMNO = "img_formno";
    public static final String IMAGE_USERID = "img_userid";
    public static final String IMAGE_TS = "img_ts";
    public static final String IMAGE_MAPCOUNT = "img_map_count";
    public static final String IMAGE_STATUS = "img_status";
    public static final String IMAGE_LAST = "img_lastid";
    public static final String IMAGE_PATH = "img_path";
    public static final String IMAGE_CAPTION = "img_captions";

    String urlString = "https://hdbapp.hdbfs.com/HDB_PD_Com/HDB_PD_V4.0/img_upload.php";

    //-----Masters Tables-------

    public static final String Master_product = "tbl_product_master";
    public static final String COLUMN_ID = "_id";
    public static final String NAME = "name";


    public static final String Master_contact_prsn = "tbl_contact_prsn_master";


    public static final String Master_VERIFIED_AT = "tbl_verified_at";


    public static final String KEY_STATUS = "status";
    private static String KEY_SUCCESS = "success";

    private static final String IMAGE_DIRECTORY_NAME = "/HDB";
    ConnectionDetector cd;
    String str_user_id;
    UserFunctions userFunction;
    SharedPreferences pref;
    Context context;
    private static int VERSION = 1;
    private static String DBNAME = "HDBHMCV";

    ProgressDialog dialog;
    InputStream is = null;
    JSONObject jObj = null;
    String json = null;
    public ExampleDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        userFunction = new UserFunctions();
        this.context = context;
        cd = new ConnectionDetector(context);
        pref = context.getSharedPreferences("MyPref", 0);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        userFunction = new UserFunctions();

        db.execSQL(
                "CREATE TABLE " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        IMAGE_LOSID + " TEXT, " +
                        IMAGE_FORMNO + " TEXT, " +
                        IMAGE_USERID + " TEXT, " +
                        IMAGE_TS + " TEXT, " +
                        IMAGE_MAPCOUNT + " TEXT, " +
                        IMAGE_LAST + " TEXT, " +
                        IMAGE_PATH + " TEXT, " +
                        IMAGE_CAPTION + " TEXT, " +
                        IMAGE_STATUS + " INTEGER)"
        );

        db.execSQL(
                "CREATE TABLE " + Master_product +
                        "(" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        NAME + " TEXT)"
        );

        db.execSQL(
                "CREATE TABLE " + Master_contact_prsn +
                        "(" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        NAME + " TEXT)"
        );

        db.execSQL(
                "CREATE TABLE " + Master_VERIFIED_AT +
                        "(" + COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        NAME + " TEXT)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + IMAGE_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Master_product);
        db.execSQL("DROP TABLE IF EXISTS " + Master_VERIFIED_AT);
        db.execSQL("DROP TABLE IF EXISTS " + Master_contact_prsn);
        onCreate(db);
    }

    public boolean insertMasterValue(String name, String TABLE_NAME) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        System.out.println("INside::::" + name);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }


    public boolean insertImage(String path, String losid, String form_no, String userid, String ts, String map_count, String last_id, String status, String image_cation) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);

        contentValues.put(IMAGE_FORMNO, form_no);
        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_MAPCOUNT, map_count);
        contentValues.put(IMAGE_LAST, last_id);
        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);


        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        return true;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, IMAGE_TABLE_NAME);
        return numRows;
    }

    public boolean updateImage(String ts, int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        db.update(IMAGE_TABLE_NAME, contentValues, IMAGE_TS + " = ? ", new String[]{ts});
        return true;
    }

    public Integer deleteImage(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(IMAGE_TABLE_NAME,
                IMAGE_COLUMN_ID + " = ? ",
                new String[]{Integer.toString(id)});
    }

    public Cursor getImage(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " +
                IMAGE_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }

    public Cursor getAllImages(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{ts});

        return res;
    }

    public Cursor getImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + ">? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0"});

        return res;
    }
    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + IMAGE_PATH + ") FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }


    public String getCasePhotoCount(String str_ts) {


        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.rawQuery("SELECT  COUNT ( distinct " + IMAGE_CAPTION + ") FROM " + IMAGE_TABLE_NAME +
                " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{str_ts});

        //  Cursor cursor = db.rawQuery(countQuery_pic, null);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }


    public String getphoto() {

        SQLiteDatabase db = this.getReadableDatabase();
        String image_path = null;
        Integer image_id = null;

        String countQuery_val = "SELECT " + IMAGE_COLUMN_ID + "," + IMAGE_PATH + "  FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_val, null);

        System.out.println("countQuerymlop:" + countQuery_val);

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    image_id = cursor.getInt(0);
                    image_path = cursor.getString(1);

                    System.out.println("image_path:" + image_id);
                    if (image_path != null) {
                        upload_image(image_path);
                    } else {
                        System.out.println("image_path:" + image_path);

                        deleteImage(image_id);
                    }
                    // String cmp_iomage = compressImage(image_path, image_new_path, image_latitude, image_longitude);


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            cursor.close();
            db.close();
        }
        //     closeConnection();

        return image_path;
        // System.out.println("countQuery:"+product_qnt);
    }

    public void upload_image(String img_path) {

        if (img_path != null) {
            //	Log.d("uploading File : ", img_path);
            File file1 = new File(img_path);
            if (file1.exists()) {
                // Log.d("uploading File Esists : ", "true");
                System.out.println("img_path::::" + img_path);
                if (cd.isConnectingToInternet()) {
                    uploadFile(img_path);
                } else {
                    userFunction.cutomToast("No internet connection...", context);
                }
            } else {
                this.deletepic(img_path);
                this.deletepic(img_path);

            }
        } else {

            try {
                if (Integer.parseInt(this.getPendingPhotoCount()) == 0 && !userFunction.isUserLoggedIn(context)) {


                    File mediaStorageDir;
                    String extStorageDirectory = Environment
                            .getExternalStorageDirectory()
                            .toString();

                    // External sdcard location
                    mediaStorageDir = new File(
                            extStorageDirectory
                                    + IMAGE_DIRECTORY_NAME);

                    if (mediaStorageDir.isDirectory()) {
                        try {
                            String[] children = mediaStorageDir
                                    .list();
                            for (int i = 0; i < children.length; i++) {
                                new File(mediaStorageDir, children[i]).delete();
                            }

                        } catch (Exception e) {
                            // block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
/*
    public void doFileUpload(final String path) {

        File file1 = new File(path);
        // File file2 = new File(selectedPath2);
        str_user_id = pref.getString("user_id", null);
        // str_user_name = pref.getString("user_name", null);

        String urlString = "https://hdbapp.hdbfs.com/HDB_PD_V2.3/img_upload.php";
        Log.i("doFileUpload", urlString);
        try {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlString);
            FileBody bin1 = new FileBody(file1);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("uploadedfile1", bin1);
            //  reqEntity.addPart("img_latitude", new StringBody(image_latitude));
            // reqEntity.addPart("img_longitude", new StringBody(image_longitude));
            // reqEntity.addPart("user_name", new StringBody(str_user_name));

            reqEntity.addPart("user", new StringBody("User"));

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            if (resEntity != null) {
                Log.i("Doc RES", response_str);
                try {
                    System.out.println("response_str::::" + response_str);

                    JSONObject json = new JSONObject(response_str);

                    if (json.getString(KEY_STATUS) != null) {
                        String res = json.getString(KEY_STATUS);
                        String resp_success = json.getString(KEY_SUCCESS);
                        String resp_op = json.getString("OP");
                        System.out.println("resp_op:::"+resp_op);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true") && Integer.parseInt(resp_op)>=1) {
                            deletepic(path);
                            file1.delete();

                        } else {
                            Log.i("RESPONSE MESSAGE ", json
                                    .getString("message").toString());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ConnectTimeoutException e) {
        } catch (SocketTimeoutException e) {
        } catch (IOException e) {
        } catch (Exception e) {

        }
    }
*/
    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_PATH + "=" + " '"
                    + path + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }

    public boolean Deletepicnew(String PIC_id) {

        boolean chkFlag = false;
        try {
            System.out.println("image_path:" + PIC_id);

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_COLUMN_ID + "=" + " '"
                    + PIC_id + "'", null) > 0;
            db.close();
            System.out.println("image_path:" + PIC_id);

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }

    public void delete_pd() {

        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + IMAGE_TABLE_NAME + " where " + IMAGE_STATUS + "= 0";
        db.execSQL(del_img);
    }

    public List<String> getAllLabels(String TABLE_NAME) {
        List<String> list = new ArrayList<String>();

        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);//selectQuery,selectedArguments

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(1));//adding 2nd column data
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();

        // returning lables
        return list;
    }
    public int uploadFile(final String selectedFilePath){
        File file1 = new File(selectedFilePath);

        int serverResponseCode = 0;
        String response = "";

        HttpsURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead,bytesAvailable,bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length-1];

        if (!selectedFile.isFile()){
            dialog.dismiss();


            return 0;
        }else{
            try{
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(urlString);
                connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploadedfile1",selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile1\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer,0,bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0){
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer,0,bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();
                System.out.println("serverResponseMessage::"+serverResponseMessage);


                if(serverResponseCode == 200){
                    String line;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"iso-8859-1"));
                    while ((line = bufferedReader.readLine()) != null) {
                        response += line;
                    }
                }

                if (response != null) {

                    try {

                        json = response.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    // try parse the string to a JSON object
                    try {
                        jObj = new JSONObject(json);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                } else {
                    jObj = null;
                }



                if (jObj != null) {
                    try {
                        System.out.println("response_str::::"+jObj);
                        if (jObj.getString(KEY_STATUS) != null) {
                            String res = jObj.getString(KEY_STATUS);
                            String resp_success = jObj.getString(KEY_SUCCESS);
                            if (Integer.parseInt(res) == 200
                                    && resp_success.equals("true")) {
                                deletepic(selectedFilePath);
                                file1.delete();
                            } else {
                                Log.i("RESPONSE MESSAGE ", jObj.getString("message").toString());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();



            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(context, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            //dialog.dismiss();
            return serverResponseCode;
        }

    }
}