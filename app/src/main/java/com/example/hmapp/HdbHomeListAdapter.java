package com.example.hmapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.AppMenu.RequestDB;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;

public class HdbHomeListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;

    public HdbHomeListAdapter(Activity context, String[] itemname,
                              Integer[] imgid) {
        super(context, R.layout.pd_hdb_home_list_item, itemname);

        this.context = context;
        this.itemname = itemname;
        this.imgid = imgid;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater
                .inflate(R.layout.pd_hdb_home_list_item, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView txtmsg_count = (TextView) rowView.findViewById(R.id.msg_count);
        //TextView txtmsg_count1 = (TextView) rowView.findViewById(R.id.msg_count1);
        txtmsg_count.setVisibility(View.GONE);

        RequestDB rdbl = new RequestDB(context);

        ExampleDBHelper edbHelp = new ExampleDBHelper(context);

        int int_cust_pending_count = 0;
        int int_asset_pending_count = 0;
        int int_app_form_pending_count = 0;

        int int_new_case_pending_count = 0;
        int PendingPhotoCoun = 0;


        int_cust_pending_count = Integer.parseInt(rdbl.getCustomerPendingCount());
        int_asset_pending_count = Integer.parseInt(rdbl.getAssetPendingCount());
        int_app_form_pending_count = Integer.parseInt(rdbl.getPD_formPendingCount());
        int_new_case_pending_count = Integer.parseInt(rdbl.getCasePendingCount());
        PendingPhotoCoun = Integer.parseInt(edbHelp.getPendingPhotoCount());
        rdbl.closeConnection();

        int total_int = int_cust_pending_count + int_asset_pending_count + int_app_form_pending_count +
                int_new_case_pending_count + PendingPhotoCoun;


        String str_total = String.valueOf(total_int);

        if (position ==5 ) {

            txtmsg_count.setText(str_total);
            txtmsg_count.setVisibility(View.VISIBLE);
            if (total_int == 0) {
                txtmsg_count.setVisibility(View.GONE);
            }
        }

        if (position == 4) {
            if (Integer.parseInt(HdbHome.msg_count) != 0) {
                txtmsg_count.setText(HdbHome.msg_count);
                txtmsg_count.setVisibility(View.VISIBLE);
            }

        }
        txtTitle.setText(itemname[position]);
        imageView.setImageResource(imgid[position]);

        return rowView;


    }

}
