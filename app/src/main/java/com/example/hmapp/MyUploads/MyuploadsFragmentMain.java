package com.example.hmapp.MyUploads;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MyUploads.adapter.TabsPagerAdapter;



import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyuploadsFragmentMain extends FragmentActivity implements
        ActionBar.TabListener {

    static public ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    public static String str_title = null;
    // Tab titles
    private String[] tabs = {"Cutomer PD", "Assets PD"};
    public static String str_main_losid, str_main_app_form_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);

        ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);

        Intent i = getIntent();
        str_main_losid = i.getStringExtra("losid");
        str_main_app_form_no = i.getStringExtra("app_form_no");
        str_title = i.getStringExtra("from");

         home_title.setText(str_title);

        System.out.println("LOSID:::" + str_main_losid);

        // Initilization
        viewPager = (ViewPager) findViewById(R.id.pager);
        actionBar = getActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(mAdapter);
        //actionBar.setHomeButtonEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Adding Tabs
        for (String tab_name : tabs) {
            actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
        }
/*
        viewPager.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View arg0, MotionEvent arg1) {
                return true;
            }
        });
*/
        /**
         * on swiping the viewpager make respective tab selected
         * */
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        // on tab selected
        // show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
    }

    public void settcurrentItem(int item, boolean smooothscroll) {
        viewPager.setCurrentItem(item, smooothscroll);
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }
}
