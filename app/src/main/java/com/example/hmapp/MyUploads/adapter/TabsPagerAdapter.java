package com.example.hmapp.MyUploads.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hmapp.MyUploads.AssetsuploadsFragment;
import com.example.hmapp.MyUploads.Customer_uploadsFragment;
import com.example.hmapp.MyUploads.FormuploadsFragment;
import com.example.hmapp.MyUploads.NewcaseUploadFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new Customer_uploadsFragment();
            case 1:
                return new AssetsuploadsFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }

}
