package com.example.hmapp.MyUploads;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.UserFunctions;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 24-05-2016.
 */

public class finLos extends AppCompatActivity {
    String str_losid, str_upload_date, str_form_no, str_remarks, str_cust_name, str_pic_count, str_upload_by;
    TextView txt_losid, txt_upload_date, txt_form_no, txt_remarks, txt_cust_name, txt_pic_count, txt_upload_by, txt_final_losid;
    EditText edt_fin_losid;
    String str_fin_losid;
    Button btn_submit;
    JSONObject jsonobject = null;
    public static final String KEY_STATUS = "status";
    String str_msg_id;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    String str_pg_from;
    String str_sr_no;
    ConnectionDetector cd;
    String str_final_losid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_finlos);


        ActionBar home = getSupportActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);
        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.pd_homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("FINAL LOSID");

        Button home_button = (Button) home_view.findViewById(R.id.txt_home);
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_home_acivity(v);
            }

        });

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);


        userFunction = new UserFunctions();

        Intent i = getIntent();
        str_losid = i.getStringExtra("pd_losid");
        str_upload_date = i.getStringExtra("pd_upload_date");
        str_form_no = i.getStringExtra("pd_form_no");
        str_remarks = i.getStringExtra("pd_remarks");
        str_cust_name = i.getStringExtra("pd_cust_name");
        str_pic_count = i.getStringExtra("pd_pic_count");
        str_upload_by = i.getStringExtra("pd_upload_by");
        str_sr_no = i.getStringExtra("sr_no");
        str_final_losid = i.getStringExtra("final_losid");

        Integer pg_from = i.getIntExtra("pg_from", 0);
        str_pg_from = Integer.toString(pg_from);
        txt_losid = (TextView) findViewById(R.id.txt_losid);
        txt_upload_date = (TextView) findViewById(R.id.txt_upload_time);
        txt_form_no = (TextView) findViewById(R.id.txt_app_form_no);
        txt_remarks = (TextView) findViewById(R.id.txt_upload_by);
        txt_cust_name = (TextView) findViewById(R.id.txt_pd_customer_name);
        txt_pic_count = (TextView) findViewById(R.id.txt_pic_count);
        txt_upload_by = (TextView) findViewById(R.id.tv_upload_by_new);
        txt_final_losid = (TextView) findViewById(R.id.txt_final_losid);

        txt_losid.setText(str_losid);
        txt_upload_date.setText(str_upload_date);
        txt_form_no.setText(str_form_no);
        txt_remarks.setText(str_remarks);
        txt_cust_name.setText(str_cust_name);
        txt_pic_count.setText(str_pic_count);
        txt_upload_by.setText(str_upload_by);
        txt_final_losid.setText(str_final_losid);

        edt_fin_losid = (EditText) findViewById(R.id.edt_fin_losid);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_fin_losid = edt_fin_losid.getText().toString();
                if (str_fin_losid != null) {
                    cd = new ConnectionDetector(finLos.this);
                    boolean ins = true;
                    boolean start_with_zero = startWithZeros(str_fin_losid, 1);
                    System.out.println("start_with_zero:::"+start_with_zero);
                    if (start_with_zero == true && ins==true) {
                        ins = false;
                        Toast.makeText(finLos.this, "Enter Valid LOSID.....", Toast.LENGTH_LONG)
                                .show();
                    } else {
                        ins = true;
                    }

                    if (str_fin_losid.length() == 7&&ins==true) {
                        ins = true;
                    } else {
                        Toast.makeText(finLos.this, "Enter Valid LOSID.....", Toast.LENGTH_LONG)
                                .show();
                        ins = false;
                    }

                    if (cd.isConnectingToInternet()) {
                        if (ins == true) {
                            new updateFinLos().execute();
                        }
                    } else {
                        userFunction.cutomToast(getResources().getString(R.string.no_internet), finLos.this);
                    }

                } else {
                    Toast.makeText(finLos.this, "Enter LOSID.....", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });
    }

    public class updateFinLos extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(finLos.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            jsonobject = userFunction.update_newLos(str_fin_losid, str_pg_from, str_sr_no);
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {

                if (jsonobject != null) {

                    // Log.d("jsonobject OP : " , jsonobject.toString() );
                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            userFunction.cutomToast("LOSID updated Successfully...",
                                    finLos.this);

                            Intent myuploads = new Intent(finLos.this,
                                    MyuploadsFragmentMain.class);
                            myuploads.putExtra("losid", "");
                            myuploads.putExtra("app_form_no", "");
                            myuploads.putExtra("from", "MY UPLOADS");
                            startActivity(myuploads);
                            finish();
                        } else {
                            String
                                    error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg,
                                    finLos.this);

                        }
                    }
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    boolean startWithZeros(String str, Integer count_ofzeros) {
        StringBuilder sb = new StringBuilder();
        for (Integer i = 0; i < count_ofzeros; i++)
            sb.append("0");
        return str.startsWith(sb.toString());
    }
}
