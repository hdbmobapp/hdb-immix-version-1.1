package com.example.hmapp.MyUploads;

import com.example.hdb.R;
import com.example.hmapp.HdbHome;
import com.example.hmapp.MyUploads.adapter.PDCasesListAdapter;

import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.UserFunctions;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.LoadMoreListView;

public class AssetsuploadsFragment extends Fragment {
    // Within which the entire activity is enclosed

    // ListView represents Navigation Drawer
    ListView mList;
    public int pgno = 0;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;

    // Title of the action bar
    String mTitle = "";
    static LinearLayout drawerll;
    static String str_region;
    SharedPreferences pref;

    String str_user_id;
    String str_user_name;
    String str_losid_no,str_app_form_no;

    PDCasesListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;

    public static String FORM_NO = "Form_No";
    public static String LOSID = "LOSID";
    public static String PIC_COUNT = "PIC_COUNT";
    public static String PD_Customer_name = "customer_name";
    public static String UPLOAD_BY = "upload_by";
    public static String UPLOAD_DATE = "upload_date";
    public static String SRNO = "srno";
    public static String REMARKS = "remarks";
    public static String FINAL_LOSID = "final_losid";


    UserFunctions userfunction;
    TextView txt_error;
    EditText edt_losid;
    Button btn_losid;
    View rootView;
    ProgressBar progressbar_loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pd_my_cases, container, false);

        userFunction = new UserFunctions();
        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);

        System.out.println("user_id:::" + str_user_id);


        setlayout();


        cd = new ConnectionDetector(getActivity().getApplicationContext());

        if (cd.isConnectingToInternet()) {

            new DownloadJSON().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }
        // Setting the adapter on mDrawerList
        // mDrawerList.setAdapter(adapter);


        return rootView;
    }


    public void setlayout() {
        txt_error = (TextView) rootView.findViewById(R.id.text_error_msg);
        arraylist = new ArrayList<HashMap<String, String>>();

        mList = (ListView) rootView.findViewById(R.id.list);

        edt_losid = (EditText) rootView.findViewById(R.id.editText);

        btn_losid = (Button) rootView.findViewById(R.id.button);

        progressbar_loading=(ProgressBar)rootView.findViewById(R.id.progressbar_loading);

    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            progressbar_loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;

            jsonobject = userFunction.getCO_PDCaseList(str_user_id,  MyuploadsFragmentMain.str_main_losid,MyuploadsFragmentMain.str_main_app_form_no, "0");

            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");
//                    txt_error.setVisibility(View.GONE);
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("srno", jsonobject.getString("Srno"));
                    map.put("LOSID", jsonobject.getString("LOSID"));
                    map.put("Form_No", jsonobject.getString("Form_No"));
                    map.put("PIC_COUNT", jsonobject.getString("PIC_COUNT"));
                    map.put("customer_name", jsonobject.getString("customer_name"));
                    map.put("upload_by", jsonobject.getString("upload_by"));
                    map.put("upload_date", jsonobject.getString("upload_date"));
                    map.put("remarks", jsonobject.getString("remarks"));
                    map.put("final_losid", jsonobject.getString("final_losid"));



                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getActivity().getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                adapter = new PDCasesListAdapter(getActivity().getApplicationContext(), arraylist,2);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });

            }
            progressbar_loading.setVisibility(View.GONE);

        }

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getActivity().getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        getActivity().finish();
    }


    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            jsonobject = userFunction.getCO_PDCaseList(str_user_id,  MyuploadsFragmentMain.str_main_losid,MyuploadsFragmentMain.str_main_app_form_no, tmpgno);

            // Create an array
            if (jsonobject != null) try {
                jsonarray = jsonobject.getJSONArray("Data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("srno", jsonobject.getString("Srno"));
                    map.put("LOSID", jsonobject.getString("LOSID"));
                    map.put("Form_No", jsonobject.getString("Form_No"));
                    map.put("PIC_COUNT", jsonobject.getString("PIC_COUNT"));
                    map.put("customer_name", jsonobject.getString("customer_name"));
                    map.put("upload_by", jsonobject.getString("upload_by"));
                    map.put("upload_date", jsonobject.getString("upload_date"));
                    map.put("remarks", jsonobject.getString("remarks"));
                    map.put("final_losid", jsonobject.getString("final_losid"));

                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }

}
