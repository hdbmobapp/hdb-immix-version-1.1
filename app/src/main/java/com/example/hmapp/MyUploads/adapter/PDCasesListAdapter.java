package com.example.hmapp.MyUploads.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hmapp.MyUploads.Customer_uploadsFragment;
import com.example.hmapp.MyUploads.MyuploadsFragmentMain;
import com.example.hmapp.MyUploads.finLos;
import com.example.hmapp.hdbLibrary.UserFunctions;

import java.util.ArrayList;
import java.util.HashMap;

public class PDCasesListAdapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunctions user_function;
    Integer int_pg_from;

    public PDCasesListAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist, Integer pg_from) {
        this.context = context;
        data = arraylist;
        int_pg_from = pg_from;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.pd_cases_list_item, parent,
                    false);

            holder = new ViewHolder();
            holder.tv_losid = (TextView) itemView.findViewById(R.id.txt_losid);
            holder.tv_app_form_no = (TextView) itemView
                    .findViewById(R.id.txt_app_form_no);
            holder.tv_pd_customer_name = (TextView) itemView
                    .findViewById(R.id.txt_pd_customer_name);
            holder.tv_upload_date = (TextView) itemView
                    .findViewById(R.id.txt_upload_time);
            holder.tv_coll_pics = (TextView) itemView
                    .findViewById(R.id.txt_pic_count);
            holder.tv_upload_by_new = (TextView) itemView
                    .findViewById(R.id.tv_upload_by_new);

            holder.tv_remarks = (TextView) itemView
                    .findViewById(R.id.txt_remarks);

            holder.tv_final_losid = (TextView) itemView
                    .findViewById(R.id.txt_final_losid);


            itemView.setTag(holder);
        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        holder.tv_losid.setText(resultp.get(Customer_uploadsFragment.LOSID));
        holder.tv_upload_date.setText(resultp.get(Customer_uploadsFragment.UPLOAD_DATE));
        holder.tv_app_form_no.setText(resultp.get(Customer_uploadsFragment.FORM_NO));
        holder.tv_remarks.setText(resultp.get(Customer_uploadsFragment.CU_REMARKS));
        holder.tv_pd_customer_name.setText(resultp.get(Customer_uploadsFragment.PD_Customer_name));
        holder.tv_coll_pics.setText(resultp.get(Customer_uploadsFragment.PIC_COUNT));
        holder.tv_upload_by_new.setText(resultp.get(Customer_uploadsFragment.UPLOAD_BY));
        holder.tv_remarks.setText(resultp.get(Customer_uploadsFragment.REMARKS));
        holder.tv_final_losid.setText(resultp.get(Customer_uploadsFragment.FINAL_LOSID));




        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                if (MyuploadsFragmentMain.str_title.equals("SEARCH")) {
                } else {
                    if (int_pg_from >= 1 && int_pg_from <= 2) {
                        // new InsertData().execute();
                        Intent pd_form = new Intent(context, finLos.class);
                        pd_form.putExtra("pd_losid", resultp.get(Customer_uploadsFragment.LOSID));
                        pd_form.putExtra("pd_upload_date", resultp.get(Customer_uploadsFragment.UPLOAD_DATE));
                        pd_form.putExtra("pd_form_no", resultp.get(Customer_uploadsFragment.FORM_NO));
                        pd_form.putExtra("pd_remarks", resultp.get(Customer_uploadsFragment.CU_REMARKS));
                        pd_form.putExtra("pd_cust_name", resultp.get(Customer_uploadsFragment.PD_Customer_name));
                        pd_form.putExtra("pd_pic_count", resultp.get(Customer_uploadsFragment.PIC_COUNT));
                        pd_form.putExtra("pd_upload_by", resultp.get(Customer_uploadsFragment.UPLOAD_BY));
                        pd_form.putExtra("sr_no", resultp.get(Customer_uploadsFragment.SRNO));
                        pd_form.putExtra("final_losid", resultp.get(Customer_uploadsFragment.FINAL_LOSID));
                        pd_form.putExtra("pg_from", int_pg_from);


                        pd_form.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(pd_form);
                    }
                }
            }
        });

/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}



        holder.tv_form_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, PDForm.class);
                pd_form.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));
                context.startActivity(pd_form);
            }

        });

        holder.tv_add_pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                Intent pd_form = new Intent(context, ExistingPD.class);
                pd_form.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_form.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));

                pd_form.putExtra("product_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                pd_form.putExtra("asset_type",resultp.get(Customer_uploadsFragment.FORM_NO));

                context.startActivity(pd_form);
            }

        });

        holder.tv_coll_pics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                Intent pd_coll = new Intent(context, AssetVerified.class);
               // pd_coll.putExtra("pd_upload_by",resultp.get(MyCases.PD_UPLOAD_BY));
                pd_coll.putExtra("pd_losid",resultp.get(Customer_uploadsFragment.LOSID));
                pd_coll.putExtra("pd_form_no",resultp.get(Customer_uploadsFragment.FORM_NO));

                pd_coll.putExtra("product_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                pd_coll.putExtra("asset_type",resultp.get(Customer_uploadsFragment.FORM_NO));
                context.startActivity(pd_coll);
            }

        });
*/
        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_losid;
        public TextView tv_app_form_no;
        public TextView tv_remarks;
        public TextView tv_pd_customer_name;
        public TextView tv_coll_pics;
        public TextView tv_upload_date;
        public TextView tv_upload_by_new;
        public TextView tv_final_losid;



    }

}
