package com.example.hmapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.Home_PD_COLLX;
import com.example.hdb.R;
import com.example.hmapp.AppMenu.Contact_us;
import com.example.hmapp.AppMenu.Dashboard;
import com.example.hmapp.AppMenu.FAQ;
import com.example.hmapp.AppMenu.Loansearch;
import com.example.hmapp.AppMenu.LoginActivity;
import com.example.hmapp.AppMenu.MessageBox;
import com.example.hmapp.AppMenu.Middle_Screen;
import com.example.hmapp.AppMenu.NewCase;
import com.example.hmapp.MyUploads.MyuploadsFragmentMain;
import com.example.hmapp.hdbLibrary.ConnectionDetector;
import com.example.hmapp.hdbLibrary.ExampleDBHelper;
import com.example.hmapp.hdbLibrary.GPSTracker;
import com.example.hmapp.hdbLibrary.UserFunctions;
import com.example.hmapp.hdbLibrary.ConvertMoneyToNumberMain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class HdbHome extends AppCompatActivity {
    UserFunctions userfunction;
    JSONObject jsonobject = null;
    info.hdb.libraries.UserFunctions userFunction_nw;
    public static final String KEY_STATUS = "status";
    String str_user_id, str_user_name, str_branch;
    public static String msg_count = "0";
    TextView txt_user_id, txt_user_name, txt_branch;
    SharedPreferences pref;
    ListView list;
    String[] itemname = {"Customer/Collateral Visit", "Generate New LOSID", "Loan Search", "My Uploads", "Message Box", "Upload In Progress", "FAQ", "Contact Us"};
    Integer[] imgid = {R.drawable.pd_image, R.drawable.scan1, R.drawable.loan_search, R.drawable.reprt, R.drawable.mail_box,
            R.drawable.progress12, R.drawable.faqs, R.drawable.contact_ud };

    GPSTracker gps;
    ProgressDialog mProgressDialog;
    //Typeface font;
    ConnectionDetector cd;
    HdbHomeListAdapter adapter;
    String str_designation;
    Integer is_login;
    String str_asset_verify = "abc";
    JSONArray jsonarray = null;
    ExampleDBHelper exdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pd_hdb_home);

        ConvertMoneyToNumberMain w = new ConvertMoneyToNumberMain();
        exdb =new ExampleDBHelper(HdbHome.this);
        int rupees = Integer.parseInt("123456789");
        String str1 = w.convert(rupees);

        //--- Logout button programaticaly---- start here

        ActionBar logout = getSupportActionBar();
        logout.setDisplayShowHomeEnabled(false);
        logout.setDisplayShowTitleEnabled(false);
        LayoutInflater logout_inflater = LayoutInflater.from(this);

        View logout_view = logout_inflater.inflate(R.layout.pd_action_logout, null);
        TextView home_title = (TextView) logout_view.findViewById(R.id.txt_title);
        home_title.setText("PD Commertial Home");

        ImageButton logout_button = (ImageButton) logout_view.findViewById(R.id.logout);
        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout(v);
            }

        });

        logout.setCustomView(logout_view);
        logout.setDisplayShowCustomEnabled(true);

        //------------end Here----------

        userfunction = new UserFunctions();
        cd = new ConnectionDetector(HdbHome.this);

        gps = new GPSTracker(this);
        userFunction_nw = new info.hdb.libraries.UserFunctions();

//        font = Typeface.createFromAsset(this.getAssets(),
//                "fonts/MyriadPro-Regular.otf");
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        str_branch = pref.getString("user_brnch", null);
        str_designation = pref.getString("user_designation", "NO");
        is_login = pref.getInt("is_login", 0);


        this.SetLayout();

        /*
        if (userFunction_nw.isUserLoggedIn(HdbHome.this) && is_login > 0 ) {
            if (is_login > 0) {
                this.SetLayout();
            } else {
                Intent messagingActivity = new Intent(HdbHome.this,
                        LoginActivity.class);
                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(messagingActivity);
                finish();
            }

        } else {
            Intent messagingActivity = new Intent(HdbHome.this,
                    LoginActivity.class);
            messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(messagingActivity);
            finish();
        }

*/

        if (cd.isConnectingToInternet()) {
            new GetDashboardInfo().execute();
            // new GetMasters().execute();
        }

        adapter = new HdbHomeListAdapter(this, itemname, imgid);

        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                GoTo(position);
            }
        });
    }

    protected void SetLayout() {
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);
        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);
    }

    public void GoTo(Integer i) {
        switch (i) {
            case 0:
                Intent effe = new Intent(HdbHome.this, Middle_Screen.class);
                startActivity(effe);
                break;
            case 1:
                Intent eff = new Intent(HdbHome.this, NewCase.class);
               // startActivity(eff);
                userfunction.cutomToast("This menu is not Active..",getApplicationContext());
                break;
            case 2:
                Intent loandetails = new Intent(HdbHome.this, Loansearch.class);
                loandetails.putExtra("losid", "");
                loandetails.putExtra("app_form_no", "");

                startActivity(loandetails);
                break;
            case 3:
                Intent myuploads = new Intent(HdbHome.this, MyuploadsFragmentMain.class);
                myuploads.putExtra("losid", "");
                myuploads.putExtra("app_form_no", "");
                myuploads.putExtra("from", "MY UPLOADS");
                startActivity(myuploads);
                break;
            case 4:
                Intent messagingActivity = new Intent(HdbHome.this, MessageBox.class);
                startActivity(messagingActivity);
                break;

            case 5:
                Intent dashboradActivity = new Intent(HdbHome.this, Dashboard.class);
                startActivity(dashboradActivity);
                break;

            case 6:
                Intent faq = new Intent(HdbHome.this, FAQ.class);
                faq.putExtra("int_sreen_val", 2);
                startActivity(faq);
                break;


            case 7:
                Intent contact_us = new Intent(HdbHome.this, Contact_us.class);
                startActivity(contact_us);
                break;

            /*
            case 7:
                Intent reports = new Intent(HdbHome.this, Reports.class);
                startActivity(reports);

                break;
            case 8:
                Intent mobile_usage = new Intent(HdbHome.this,
                        ExpandMainActivity.class);
                startActivity(mobile_usage);

                break;

            case 9:
                Intent remarks = new Intent(HdbHome.this,
                        Remarks.class);
                startActivity(remarks);

*/
            default:

                break;

        }

    }

    public void logout(View v) {
        // do stuff
        Intent hme = new Intent(HdbHome.this,
                Home_PD_COLLX.class);
        startActivity(hme);
       // alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                HdbHome.this.finish();
                                @SuppressWarnings("unused")
                                boolean is_logout_Boolean = false;
                                is_logout_Boolean = userfunction
                                        .logoutUser(getApplicationContext());

                                if (is_logout_Boolean = true) {
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putInt("is_login", 0);
                                    editor.commit();

                                    Intent i = new Intent(
                                            HdbHome.this, com.example.hdb.LoginActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public class GetDashboardInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            if (cd.isConnectingToInternet()) {
                jsonobject = userfunction.get_msg_count(str_user_id);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    // text_error_msg.setVisibility(View.GONE);

                    // Log.d("jsonobject OP : " , jsonobject.toString() );
                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            msg_count = jsonobject.getString("msg_count");

                            System.out.println("MSG COUNT:::" + msg_count);
                            list.setAdapter(adapter);

                        }
                    }
                }
                // mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class GetMasters extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(HdbHome.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            if (cd.isConnectingToInternet()) {
                jsonobject = userfunction.update_Masters(str_user_id,"1");
                if (jsonobject != null) {

                    // Create an array
                    try {
                        // Locate the array name in JSON
                        jsonarray = jsonobject.getJSONArray("Data");

                        for (int i = 0; i < jsonarray.length(); i++) {
                            jsonobject = jsonarray.getJSONObject(i);

                            // Retrive JSON Objects

                            exdb.insertMasterValue(jsonobject.getString("name").toString(),"tbl_contact_prsn_master");

                            // Set the JSON Objects into the array
                        }
                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

            userfunction.cutomToast("Master upload Succesfully",HdbHome.this);
                        }
                    }
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }
}