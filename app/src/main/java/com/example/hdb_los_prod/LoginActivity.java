package com.example.hdb_los_prod;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.UserFunction;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.DatabaseHandler;

public class LoginActivity extends Activity {

    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;
    LoginActivity ll;

    Integer off_rowcount;
    JSONArray jsonarray;
    ArrayList<String> master_flag;
    public  static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS=0;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunction userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    private static Typeface font;
    //String user_name = null;
    String gcm = null;
    @SuppressWarnings("unused")
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{2,14}");
    Context context;
    ProgressBar pb_progress;

    String res,resp_success;

    public  static String KEY_STATUS="status";

    public  static String KEY_SUCCESS="success";

    String
            flag_pm,
            id_pm,

    flag_pd,
            id_pd,flag_cc,id_cc,flag_cr,id_cr,flag_ccm,id_ccm,ac_flag,ac_id,bp_flag,bp_id,mp_flag,mp_id,mlp_flag,mlp_id,man_flag,man_id,app_flag,app_id;

    JSONObject jsonobject;

    ProgressDialog mProgressDialog;

    String str_dispo_id,str_dispo,str_flag;

    DatabaseHandler db;

    String reg_id;
    String  imeiNumber1,imeiNumber2;

    SharedPreferences pref;
    String lattitude = null, longitude = null;
    EditText edt_new_version_link;
    String new_version;
    TextView txt_new_version;
    String new_version_name;
    String imei1,imei2;

    DBHelper rDB;

    HashMap<String,String> user = new HashMap<String,String>();

    private String android_id;
    String check_master_flag;
    ArrayList<HashMap<String, String>> arraylist;

    int count_btn=0, count_btn_1=0, count_btn_2=0,count_btn_3=0,count_btn_4=0,count_btn_5=0,count_btn_6=0,count_btn_7=0,count_btn_8=0,count_btn_9=0,count_btn_10=0,count_btn_11=0;


    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA

    };



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ll=new LoginActivity();
        // setContentView(R.qde_layout.login);
       // font = Typeface.createFromAsset(getBaseContext().getAssets(),
         //       "fonts/Amaranth-Regular.otf");
        cd = new ConnectionDetector(getApplicationContext());

        db=new DatabaseHandler(getApplicationContext());
        userFunction = new UserFunction(this);

        arraylist= new ArrayList<>();
        master_flag=new ArrayList<String>();
        System.out.println("LOGIN REDIRECT::::");

        android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){

            TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            imei1=  mngr.getDeviceId(1);
            imei2=   mngr.getDeviceId(2);
        }

        System.out.println("imei1::::"+imei1+"::::imei2:::::"+imei2);
        rDB=new DBHelper(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        // for
        master_flag= rDB.get_master_flag();

        flag_pm= master_flag.get(2);
        id_pm= master_flag.get(3);


        flag_pd= master_flag.get(0);
        id_pd= master_flag.get(1);

        flag_cc= master_flag.get(4);
        id_cc= master_flag.get(5);

        flag_cr= master_flag.get(6);
        id_cr= master_flag.get(7);

        flag_ccm= master_flag.get(8);
        id_ccm= master_flag.get(9);

        ac_flag= master_flag.get(10);
        ac_id= master_flag.get(11);

        bp_flag= master_flag.get(12);
        bp_id= master_flag.get(13);


        mp_flag= master_flag.get(14);
        mp_id= master_flag.get(15);

        mlp_flag= master_flag.get(16);
        mlp_id= master_flag.get(17);


        man_flag= master_flag.get(18);
        man_id= master_flag.get(19);


        app_flag= master_flag.get(20);
        app_id= master_flag.get(21);

        Editor editor = pref.edit();
        editor.putString("timeout", "20000");
        editor.commit();


/*
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // qde_result of the request.
                }
            }
        }else {
            android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

*/

          //cons code
           checkPermissions();
           SetLayout();

    }

    protected void SetLayout() {
        // Check if Internet present
        setContentView(R.layout.qde_sl_login);
        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        edt_new_version_link = (EditText) findViewById(R.id.edt_new_version_link);
        txt_new_version = (TextView) findViewById(R.id.txt_new_version);
        // ---set Custom Font to textview
      //  inputEmail.setTypeface(font);

        //inputPassword.setTypeface(font);

       // btnLogin.setTypeface(font);
       // loginErrorMsg.setTypeface(font);

        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);
        //inputEmail.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

      //  inputEmail.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

       //CapsText.afterTextChanged((Editable)inputEmail);


        /*
        inputEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    inputEmail.setText(s);
                }
                inputEmail.setSelection(inputEmail.getText().length());
            }
        });
*/

        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
              //  GCMRegistrar.register(LoginActivity.this,
               //         GCMIntentService.SENDER_ID);

                TelephonyManager tm= (TelephonyManager)getSystemService(context.TELEPHONY_SERVICE);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    imeiNumber1=tm.getImei(1);
                    imeiNumber2= tm.getImei(2);

                }else {
                    imeiNumber1 = tm.getDeviceId(1);
                    imeiNumber2 = tm.getDeviceId(2);
                }

                System.out.println("reg_id:::::"+reg_id);

                email = inputEmail.getText().toString();
                password = inputPassword.getText().toString();
                // Check if Internet present
                TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);
         //       error_pwd.setTypeface(font);
           //     error_email.setTypeface(font);

                if (password.equals("") || email.equals("")) {

                    if (password.equals("")) {
                        error_pwd.setVisibility(View.VISIBLE);
                        error_pwd.setText("Please enter a password");

                    } else {
                        error_pwd.setVisibility(View.GONE);

                    }
                    if (email.equals("")) {
                        error_email.setVisibility(View.VISIBLE);
                        error_email.setText("Please enter a Employee Id");
                    } else {
                        error_email.setVisibility(View.GONE);

                    }
                } else {

                    if (cd.isConnectingToInternet()) {

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);

                        new Login().execute();

                    } else {
                        userFunction.noInternetConnection(
                                "Check your connection settings!",
                                getApplicationContext());

                        loginErrorMsg.setText("Check your connection settings!");
                        /*

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);

                       // userFunction.logoutUserclear(getApplicationContext());

                        user=   db.off_getUserDetails();

                        System.out.println("HASHMAP::::" + user);

                        String val=(String)user.get("uid");
                        String dev_val=(String)user.get("dev_id");





                        if(val!=null) {

                            if (val.equals(email)) {

                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "yyyy-MM-dd");
                                String current_date = sdf.format(new Date());

                                Editor editor = pref.edit();
                                editor.putString("current_date_off", current_date);
                                editor.putInt("off_is_login", 1);


                                editor.commit();
                                System.out.println("OFF LOGIN:::");

                                off_rowcount=db.off_getRowCount();
                                if(off_rowcount>0) {
                                    if (!dev_val.equals("")) {

                                        System.out.println("DEVICE ID VAL:::" + dev_val);

                                        if (!dev_val.equals(android_id)) {
                                            userFunction.cutomToast(
                                                    "You have changed your handset. Contact your supervisor",
                                                    getApplicationContext()

                                            );

                                            loginErrorMsg.setText("You have changed your handset. Contact your supervisor");


                                        } else {
                                            Intent messagingActivity = new Intent(
                                                    LoginActivity.this, HdbHome.class);
                                            startActivity(messagingActivity);
                                            finish();
                                        }
                                    }else {
                                        Intent messagingActivity = new Intent(
                                                LoginActivity.this, Masters.class);
                                        startActivity(messagingActivity);
                                        finish();
                                    }
                                }else {
                                    userFunction.noInternetConnection(
                                            "Check your connection settings!",
                                            getApplicationContext());

                                    loginErrorMsg.setText("Check your connection settings!");
                                }
                            } else {
                               userFunction.cutomToast(
                                        "Username or Password is incorrect",
                                        getApplicationContext());

                                loginErrorMsg.setText("Username or Password is incorrect");

                            }
                        }else {
                            userFunction.noInternetConnection(
                                    "Check your connection settings!",
                                    getApplicationContext());

                            loginErrorMsg.setText("Check your connection settings!");
                        }



                        */
                    }
                }



            }
        });

    }

    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());

                }

            }
        });
    }

/*
    private  final TextWatcher CapsText=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }


        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            String upper=charSequence.toString().toUpperCase();
            if(!charSequence.equals(upper)){


            }

        }

        @Override
        public void afterTextChanged(EditText editable) {

            editable.toString().toUpperCase();


        }


    }   ;

    */

    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            try {
                String deviceName = Build.MODEL;
                String deviceMan = Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, "",
                        lattitude, longitude, deviceName, deviceMan, android_id,imeiNumber1,imeiNumber2);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            mProgressDialog.dismiss();
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);



                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            // user successfully logged in'
                            // Store user details in SQLite Database10
                            DatabaseHandler db = new DatabaseHandler(
                                    getApplicationContext());

                            JSONArray jsn_arr;
                            JSONObject json_user = json.getJSONObject("data");
                         //   JSONObject json_user1 = json.getJSONObject("imei");

                            // Clear all previous data in database


                            if( !json_user.getString("branchid").toString().equals("null") ) {


                                if(json_user.getString("branchid")!=null ){

                                    userFunction
                                            .logoutUserclear(getApplicationContext());

                                    userFunction
                                            .off_logoutUserclear(getApplicationContext());


                                    String KEY_EMAIL = "email";
                                    String KEY_NAME = "name";
                                    String KEY_UID = "id";
                                    String KEY_deviceid = "device_id";

                                    db.addUser(json_user.getString(KEY_NAME),
                                            json_user.getString(KEY_EMAIL),
                                            json_user.getString(KEY_UID));

                                    db.off_addUser(json_user.getString(KEY_NAME),
                                            json_user.getString(KEY_EMAIL),
                                            json_user.getString(KEY_UID),
                                            json_user.getString(KEY_deviceid)
                                    );

                                    SimpleDateFormat sdf = new SimpleDateFormat(
                                            "yyyy-MM-dd");
                                    String current_date = sdf.format(new Date());

                                    Editor editor = pref.edit();
                                    editor.putString("current_date", current_date);

                                    editor.putInt("is_login", 1);
                                    editor.putString("user_id",
                                            json_user.getString(KEY_UID).toString());
                                    editor.putString("user_name",
                                            json_user.getString(KEY_NAME).toString());
                                    String KEY_BRANCH = "branch";
                                    editor.putString("user_brnch",
                                            json_user.getString(KEY_BRANCH).toString());
                                    editor.putString("user_email",
                                            json_user.getString(KEY_EMAIL).toString());
                                    editor.putString("user_designation",
                                            json_user.getString("designation").toString());
                                    editor.putString("user_designation_value",
                                            json_user.getString("user_designation_value").toString());

                                    editor.putString("avi_tel",
                                            json_user.getString("Avinash").toString());

                                    editor.putString("sach_tel",
                                            json_user.getString("Sachin").toString());

                                    editor.putString("vija_tel",
                                            json_user.getString("Vijay").toString());

                                    editor.putString("service_tel",
                                            json_user.getString("Service").toString());

                                    editor.putString("mobile",
                                            json_user.getString("mobile").toString());

                                    editor.putString("timeout",
                                            json_user.getString("timeout").toString());
                                    Boolean del_flag;


//                                    rDB. delete_IMGo(null);



/*
                                        if (jsn_arr.length() != 0) {
                                            for (int j = i-1, l = 0; j < jsn_arr.length() ; j++, l++) {





                                                //    jsonobject = jsn_arr.getJSONObject(i);x
                                                System.out.println("AL COOOOO:::" + jsn_arr.get(j).toString());
                                                rDB.insert_image_master(String.valueOf(j), jsn_arr.get(l).toString(), "C");

                                            }
                                        }*/







                                    //   System.out.println("aaaa::::" + jsn_arr.get(0));
                                   /* if(jsn_arr.length()!=0) {
                                        for (int k = i; k < jsn_arr.length()+i; k++) {

                                            //    jsonobject = jsn_arr.getJSONObject(i);x
                                            //System.out.println("AL PM:::" + al_product.get(i));
                                            rDB.insert_image_master(String.valueOf(k), jsn_arr.get(k).toString(), "G");

                                        }
                                    }*/



                                    System.out.println("BRANCH ID::::"+json_user.getString("branchid").toString());

                                    if(!json_user.getString("img_height").equals("")){
                                        editor.putInt("img_height",
                                                Integer.valueOf(json_user.getString("img_height")));
                                    }

                                    if(!json_user.getString("img_width").equals("")) {
                                        editor.putInt("img_width",
                                                Integer.valueOf(json_user.getString("img_width")));
                                    }

                                    if (!json_user.getString("img_comp_size").equals("")) {
                                        editor.putInt("img_comp_size",
                                                Integer.valueOf(json_user.getString("img_comp_size").toString()));
                                    }

                                    System.out.println("Service::::::" + json_user.getString("Service").toString());
                                    if (rememberMe.isChecked()) {
                                        editor.putString("user_username", email);
                                        editor.putString("user_password", password);
                                    } else {
                                        editor.putString("user_username", "");
                                        editor.putString("user_password", "");
                                    }


                                    System.out.println("BRR ID:::" + json_user.getString("branchid"));
                                    editor.putString("branchid",
                                            json_user.getString("branchid"));
                                    editor.commit();

                                    new Verify_Masters().execute();

                                }
                                else{
                                    callPopup_branch();
                                }
                            }else {
                                callPopup_branch();                            }


                        } else {
                            String KEY_DEVID = "imei";

                            if(!json.getString(KEY_DEVID).toString().equals("")) {
                                System.out.println("hahaahhehehehee inside");
                                db.update_deviceid(json.getString(KEY_DEVID));
                            }


                            if (new_version != null && new_version != "") {
                                edt_new_version_link.setText(new_version);
                                edt_new_version_link.setVisibility(View.VISIBLE);
                                txt_new_version.setText(new_version_name);
                                txt_new_version.setVisibility(View.VISIBLE);
                            }
                            loginErrorMsg.setText(json.getString("message")
                                    .toString());


                        }


                    }
                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb_progress.setVisibility(View.GONE);

        }

    }



    /*
     * private boolean CheckEmail(String email) {
     *
     * return EMAIL_PATTERN.matcher(email).matches(); }
     */
    private boolean CheckUsername(String username) {

        return USERNAME_PATTERN.matcher(username).matches();
    }

    private boolean CheckPassword(String password) {

        return PASSWORD_PATTERN.matcher(password).matches();
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }





    void showRationale(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("njdskfbsbfhkssf,m ?")
                .setCancelable(false)
                .setPositiveButton("Retry",

                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if (ContextCompat.checkSelfPermission(LoginActivity.this,
                                        Manifest.permission.READ_PHONE_STATE)
                                        != PackageManager.PERMISSION_GRANTED) {

                                    // Should we show an explanation?
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                                            Manifest.permission.READ_PHONE_STATE)) {

                                        // Show an expanation to the user *asynchronously* -- don't block
                                        // this thread waiting for the user's response! After the user
                                        // sees the explanation, try again to request the permission.

                                    } else {

                                        // No explanation needed, we can request the permission.

                                        ActivityCompat.requestPermissions(LoginActivity.this,
                                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                        // app-defined int constant. The callback method gets the
                                        // qde_result of the request.
                                    }
                                }

                            }
                        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void callPopup_branch() {


        // custom dialog
         AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
       // dialog.setContentView(R.qde_layout.qde_pop_up_branch);
        dialog.setTitle("Branch Not Mapped");


        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        dialog.setMessage("Branch is not mapped against your Employee ID. Contact your Supervisor");

        dialog.show();




    }

    public void go_contacts(View v){
        Intent messagingActivity = new Intent(getApplicationContext(),
                Contact_us.class);
        startActivity(messagingActivity);
        finish();

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... params) {

            String str_message;
            //     try {
            jsonobject = userFunction.check_masters();

            System.out.println("JSONOBJ::::" + jsonobject);

            if (jsonobject != null) {

                //      if (jsonobject.getString(KEY_STATUS_VM) != null) {
//                        res = jsonobject.getString(KEY_STATUS_VM);
                //                       resp_success = jsonobject.getString(KEY_SUCCESS_VM);


                //  if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                if (jsonobject != null) try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Data");
//                    txt_error.setVisibility(View.GONE);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<>();
                        jsonobject = jsonarray.getJSONObject(i);
                        map.put("sr_no", jsonobject.getString("sr_no"));
                        map.put("master_id", jsonobject.getString("master_id"));
                        map.put("master_name", jsonobject.getString("master_name"));
                        map.put("flag", jsonobject.getString("flag"));
                        map.put("QDE_flag", jsonobject.getString("QDE_flag"));
                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }


            } else {


            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            mProgressDialog.dismiss();
            if (jsonobject == null) {

//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                //  txt_error.setText(getResources().getString(R.string.error_message));
                //   txt_error.setVisibility(View.VISIBLE);
                loginErrorMsg.setVisibility(View.VISIBLE);
                loginErrorMsg.setText( getResources().getString(R.string.error_message));

            } else {/*
                ms_adapter = new MastersListAdapter(getApplicationContext().getApplicationContext(), arraylist, 1,Masters.this);
                ms_adapter.notifyDataSetChanged();
                mList.setAdapter(ms_adapter);
                */
                int i;
                for (i = 0; i <= arraylist.size() - 1; i++) {
                    String temp_flag = arraylist.get(i).get("QDE_flag");
                    System.out.println("ARRYLIST::::" + arraylist.get(i).get("master_id"));
                    switch (arraylist.get(i).get("master_id")) {
                        case "PD":

                            System.out.println("PD master chk");

                            Boolean result = userFunction.verify_master(temp_flag, flag_pd);
                            if (result == true) {

                                count_btn_1 = 1;
                                check_count();

                            }
                            break;

                        case "CONS":

                            System.out.println("CONS master chk");


                            Boolean result1 = userFunction.verify_master(temp_flag, flag_pm);

                            if (result1 == true) {

                                count_btn_2 = 1;
                                check_count();
                            }
                            break;


                        case "CR":

                            System.out.println("CR master chk");



                            System.out.println("FLG TBL cr:::" + temp_flag + ":::FLAG SQLITE cr:::" + flag_cr);

                            Boolean result3 = userFunction.verify_master(temp_flag, flag_cr);


                            System.out.println("qde_result CR:::::" + result3);
                            if (result3 == true) {

                                count_btn_3 = 1;
                                check_count();

                            }
                            break;

                        case "CCM":

                            System.out.println("CCM master chk");

                            System.out.println("FLG TBL CCM:::" + temp_flag + ":::FLAG SQLITE CCM:::" + flag_ccm);

                            Boolean result4 = userFunction.verify_master(temp_flag, flag_ccm);
                            if (result4 == true) {

                                count_btn_4 = 1;
                                check_count();
                            }
                            break;


                        case "CC":

                            System.out.println("CC master chk");


                            System.out.println("PD211::::" + temp_flag + "PD 311::::" + flag_cc);
                            Boolean result2 = userFunction.verify_master(temp_flag, flag_cc);
                            if (result2 == true) {

                                count_btn_5 = 1;
                                check_count();
                            }
                            break;


                        case "AC":

                            System.out.println("AC master chk");


                            System.out.println("FLG TBL ac:::" + temp_flag + ":::FLAG SQLITE ac:::" + ac_flag);

                            Boolean result5 = userFunction.verify_master(temp_flag, ac_flag);
                            if (result5 == true) {

                                count_btn_6 = 1;
                                check_count();

                            }
                            break;

                        case "BP":

                            System.out.println("BP master chk");


                            System.out.println("FLG TBL bp:::" + temp_flag + ":::FLAG SQLITE bp:::" + bp_flag);

                            Boolean result6 = userFunction.verify_master(temp_flag, bp_flag);
                            if (result6 == true) {

                                count_btn_7 = 1;
                                check_count();
                            }
                            break;


                        case "ML":

                            System.out.println("ML master chk");


                            System.out.println("FLG TBL::: ml" + temp_flag + ":::FLAG SQLITE ml:::" + mp_flag);

                            Boolean result7 = userFunction.verify_master(temp_flag, mp_flag);
                            if (result7 == true) {

                                count_btn_8 = 1;
                                check_count();
                            }
                            break;

                        case "MLP":
                            System.out.println("MLP master chk");


                            System.out.println("FLG TBL mlp:::" + temp_flag+ ":::FLAG SQLITE mlp:::" + mlp_flag);

                            Boolean result8 = userFunction.verify_master(temp_flag, mlp_flag);
                            if (result8 == true) {

                                count_btn_9 = 1;
                                check_count();
                            }
                            break;

                        case "APP":

                            System.out.println("APP master chk");

                            System.out.println("FLG TBL APP:::" + temp_flag + ":::FLAG SQLITE APP:::" + app_flag);

                            Boolean result10 = userFunction.verify_master(temp_flag, app_flag);


                            if (result10 == true) {

                                count_btn_11 = 1;
                                check_count();
                            }
                            break;


                        case "MM":

                            System.out.println("MM  chk");

                            System.out.println("FLG TBL man:::" + temp_flag + ":::FLAG SQLITE man:::" + man_flag);

                            Boolean result9 = userFunction.verify_master(temp_flag, man_flag);
                            if (result9 == true) {

                                count_btn_10 = 1;
                                check_count();
                            }
                            break;

                    }


                        if(count_btn==0) {
                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, Masters.class);
                           // messagingActivity.putExtra("app_id","545455");
                            startActivity(messagingActivity);
                            finish();
                        }else {
                            Intent messagingActivity = new Intent(
                                    LoginActivity.this, Masters.class);
                           // messagingActivity.putExtra("app_id","545455");
                            startActivity(messagingActivity);
                            finish();
                        }




/*
                    if(count_btn==0){
                        Intent messagingActivity = new Intent(
                                LoginActivity.this, HdbHome.class);

                        startActivity(messagingActivity);
                        finish();
                    }

                    */



                }
            }

/*


                }
                */


        }


    }

    public  void check_count(){



        count_btn=count_btn_1+count_btn_2+count_btn_3+count_btn_4+count_btn_5+count_btn_6+count_btn_7+count_btn_8+count_btn_9+count_btn_10+count_btn_11;


        Editor editor= pref.edit();
        editor.putInt("count_master", count_btn);
        editor.commit();
        System.out.println("COUNT BTN::::" + count_btn);
/*
        if(count_btn>=1){

            Intent i=new Intent(this, Masters.class);

            startActivity(i);

        }else{
            Intent messagingActivity = new Intent(
                    LoginActivity.this, HdbHome.class);

            startActivity(messagingActivity);
            finish();
        }

        */

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }
    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }



}


