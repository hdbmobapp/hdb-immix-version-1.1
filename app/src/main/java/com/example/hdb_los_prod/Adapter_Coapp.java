package com.example.hdb_los_prod;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.UserFunction;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 18-04-2017.
 */
public class Adapter_Coapp extends BaseAdapter {


    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunction userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunction user_function;
    Integer int_pg_from;
    String app_id,str_ts;


    public Adapter_Coapp(Context context,
                         ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;


        TableRow row_remraks;
        dialog = new ProgressDialog(context);
        user_function = new UserFunction(context);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);



        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.qde_coapp_list_item, parent,
                    false);


            holder = new ViewHolder();

            holder.tv_cust_name = (TextView) itemView
                    .findViewById(R.id.txt_customer_name);


            holder.tv_app_type = (TextView) itemView
                    .findViewById(R.id.txt_app_type);

            holder.tv_srno = (TextView) itemView
                    .findViewById(R.id.txt_srno);




            holder.img_upload_status = (TextView) itemView
                    .findViewById(R.id.img_upload_status);


            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        holder.tv_cust_name.setText(resultp.get(MyUploads_Coapp.cust_name));
if(resultp.get(MyUploads_Coapp.app_type).equals("C")) {
    holder.tv_app_type.setText("Co-Applicant");
}else{
    if(resultp.get(MyUploads_Coapp.app_type).equals("G")){

    holder.tv_app_type.setText("Guarantor");
    }

    else{
        holder.tv_app_type.setText("Primary Applicant");
    }
}

        holder.tv_srno.setText(resultp.get(MyUploads_Coapp.srno));

        if(!resultp.get(MyUploads_Coapp.err_code).equals("null") ) {
            if (Integer.valueOf(resultp.get(MyUploads_Coapp.err_code)) == 0) {

                holder.img_upload_status.setVisibility(View.VISIBLE);


            }
            else{
                holder.img_upload_status.setVisibility(View.GONE);

            }
        }else{
            holder.img_upload_status.setVisibility(View.GONE);

        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                // new InsertData().execute();
                //     Intent pd_form = new Intent(context, finLos.class);
                //   pd_form.putExtra("app_id_uploads",MyUploads.app_id);


                //     context.startActivity(pd_form);


            }
        });



        holder.img_upload_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                resultp = data.get(position);

                app_id = resultp.get(MyUploads_Coapp.app_id).trim().toString();

                str_ts = resultp.get(MyUploads_Coapp.ts).toString();

                Intent messagingActivity = new Intent(context,
                        Loan_Result.class);

                messagingActivity.putExtra("app_id",app_id);
                messagingActivity.putExtra("str_ts",str_ts);

                messagingActivity.putExtra("cust_type",resultp.get(MyUploads_Coapp.app_type));
                messagingActivity.putExtra("cust_name", resultp.get(MyUploads_Coapp.cust_name));

                messagingActivity.putExtra("losid", resultp.get(MyUploads_Coapp.final_losid));
                messagingActivity.putExtra("img_uplod_count", resultp.get(MyUploads_Coapp.img_upload_count));
                messagingActivity.putExtra("img_tot_cnt", resultp.get(MyUploads_Coapp.tot_img_cnt));


                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.  startActivity(messagingActivity);
            }
        });

        return itemView;
    }


    private static class ViewHolder {

        public TextView tv_cust_name;

        public TextView tv_srno;

        public TextView tv_app_type;



        public TextView  img_upload_status;



    }

}
