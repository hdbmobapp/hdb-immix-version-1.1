package com.example.hdb_los_prod;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.Home_PD_COLLX;
import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.MastersListAdapter;
import com.example.hdb_los_prod.libraries.UserFunction;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Vijay on 2/7/2017.
 */
public class Masters extends Activity {

    ProgressDialog mProgressDialog;

    JSONArray jsonarray, jsonarray1, jsonarray2, jsonarray3;


    ListView mList;
    MastersListAdapter ms_adapter;
    UserFunction userFunction;
    JSONObject jsonobject = null;
    String res, resp_success;
    public static String KEY_STATUS_VM = "status";
    public static String KEY_SUCCESS_VM = "success";



    Integer is_login,off_is_login,master_count;
    DBHelper rDB;

    ArrayList<HashMap<String, String>> arraylist;
    TextView txt_error;

    public static String srno = "srno";
    public static String master_id = "master_id";
    public static String master_name = "master_name";
    public static String flag = "flag";
    public static String QDE_flag = "QDE_flag";
    public static Button btn_next;
    SharedPreferences pref;
    private String android_id;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.print("MASTR LOAD REFRESG:::");
        setContentView(R.layout.qde_masters);
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        mList = (ListView) findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();

       // mList.addFooterView(btn_next);

        rDB=new DBHelper(this);
        userFunction = new UserFunction(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);



        is_login = pref.getInt("is_login", 0);
        is_login=1;

        off_is_login = pref.getInt("off_is_login", 0);


        master_count = pref.getInt("count_master", 0);


        System.out.println("off Login:::" + off_is_login);

  //      if (userFunction.isUserLoggedIn(Masters.this) && is_login > 0  ) {
            System.out.println("isUserLoggedIn:::"+off_is_login);

           // this.SetLayout();

            //  check_master=rDB.get_masters();

            new Verify_Masters().execute();



           /* if(check_master.get(0).equals("0") || check_master.get(1).equals("0") ){
                //  new Download_Masters().execute();

                System.out.println("MASTERS LOAD");

              //  new Download_Masters().execute();
            }
            else{*/

            //   new Verify_Masters().execute();

            // }


            /*
        } else {

                        System.out.println("redirect login:::" + off_is_login);
                        Intent messagingActivity = new Intent(Masters.this,
                                LoginActivity.class);
                        messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(messagingActivity);
                        finish();
                        // this.SetLayout();
                    }
*/







    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Masters.this);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //     try {
            jsonobject = userFunction.check_masters();

            System.out.println("JSONOBJ::::" + jsonobject);

            if (jsonobject != null) {

                //      if (jsonobject.getString(KEY_STATUS_VM) != null) {
//                        res = jsonobject.getString(KEY_STATUS_VM);
                //                       resp_success = jsonobject.getString(KEY_SUCCESS_VM);


                //  if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                if (jsonobject != null) try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Data");
//                    txt_error.setVisibility(View.GONE);
                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map =  new HashMap<>();
                        jsonobject = jsonarray.getJSONObject(i);
                        map.put("sr_no", jsonobject.getString("sr_no"));
                        map.put("master_id", jsonobject.getString("master_id"));
                        map.put("master_name", jsonobject.getString("master_name"));
                        map.put("flag", jsonobject.getString("flag"));
                        map.put("QDE_flag", jsonobject.getString("QDE_flag"));
                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }


            } else {
                // str_message = "Something Went wrong please try again...";
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            mProgressDialog.dismiss();
            if (jsonobject == null) {
//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                ms_adapter = new MastersListAdapter(getApplicationContext().getApplicationContext(), arraylist, 1,Masters.this);
                ms_adapter.notifyDataSetChanged();
                mList.setAdapter(ms_adapter);


            }
            mProgressDialog.dismiss();
        }

    }

    public void logout(View v) {
        // do stuff

        Intent hme = new Intent(com.example.hdb_los_prod.Masters.this,
                Home_PD_COLLX.class);
        startActivity(hme);


        // alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Masters.this.finish();
                                @SuppressWarnings("unused")
                                boolean islogoutBoolean=true ;

                                SharedPreferences.Editor editor = pref.edit();

                                editor.putInt("off_is_login", 0);
                                editor.commit();

                                islogoutBoolean = userFunction
                                        .logoutUser(getApplicationContext());

                               /* off_islogoutBoolean = userfunction
                                        .off_logoutUserclear(getApplicationContext());*/

                                if (islogoutBoolean == true ) {
                                    Intent i = new Intent(
                                            Masters.this, HdbHome.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


}
