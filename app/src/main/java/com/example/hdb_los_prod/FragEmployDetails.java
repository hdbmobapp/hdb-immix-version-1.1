package com.example.hdb_los_prod;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.hdb_los_prod.img_upload_retrofit.Image_List;

import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.GPSTracker_New;
import com.example.hdb_los_prod.libraries.UserFunction;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;

/**
 * Created by Vijay on 1/20/2017.
 */
public class FragEmployDetails   extends Fragment implements AdapterView.OnItemSelectedListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String TAG = "FragEmployDetails";
    DBHelper dbHelp;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");
    SharedPreferences pref,pData;

    EditText edt_das,edt_others,edt_emp_code,edt_emp_branch;

    Spinner spn_emp_name,spn_emp_type,spn_designation;

    ArrayAdapter adp_emp_name,adp_emp_type,adp_designation;

    Spinner spn_distance_from_loc;
    EditText edt_distance_km;

    String[]  emp_name,emp_type,designation;
    String  str_emp_namestr_,str_emp_type,str_designation,str_das,str_others,str_emp_code,str_emp_branch,str_ts,str_appid,str_userid,str_cust_id,str_distance_km;
    Button btn_next;

    String[] distance_type = {"Select", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};

    FragAddrDetails adr_details;

    String str_barnch,str_barnch_id,str_distance_type;

    ArrayAdapter<String> adapter_distance_type;


    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;



    static  Double lat;
    static  Double lon;

    ConnectionDetector cd;

    UserFunction userFunction;

    int map_captured = 0;

    String map_address,lattitude,longitude,app_type;

   Double dbl_longitude,dbl_latitude;

    TextView location_lat,location_long;

    GPSTracker_New appLocationService;
    Button btn_locate_me;

    UserFunction userFunctions;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qde_employement_details, container, false);



        userFunctions=new UserFunction(getContext());
        checkLocation();
        cd=new ConnectionDetector(getActivity());

        pref = getContext().getSharedPreferences("MyPref", 0); // 0 -
        dbHelp=new DBHelper(getActivity());

        adr_details=new FragAddrDetails();
        btn_locate_me = (Button) view.findViewById(R.id.btn_locate_me);

        edt_distance_km = (EditText) view.findViewById(R.id.edt_distance);
        app_type = pref.getString("app_type", null);


        spn_distance_from_loc = (Spinner) view.findViewById(R.id.spn_distance_from_loc);

        appLocationService = new GPSTracker_New(getContext());

        str_ts = pref.getString("ts", null);
        location_lat = (TextView) view.findViewById(R.id.location_lat);
        location_long = (TextView)view. findViewById(R.id.location_long);


        str_userid = pref.getString("user_id", null);


        str_cust_id = pref.getString("cust_id", null);

        str_barnch_id = pref.getString("branchid", null);
        str_barnch = pref.getString("user_brnch", null);


        edt_das=(EditText)view.findViewById(R.id.edt_DAS_Normal);

        userFunctions.edtCaps(edt_das);
        setEditMaxLenght(40, edt_das);

        btn_next=(Button)view.findViewById(R.id.btn_next);

       /* if (appLocationService.isLocationEnabled) {

            dbl_latitude = appLocationService.getLatitude();
            dbl_longitude = appLocationService.getLongitude();
            //String addres = tracker.getCompleteAddressString(dbl_latitude, dbl_longitude, EditRequest.this);
            location_lat.setText("" + dbl_latitude);
            location_long.setText("" + dbl_longitude);


        } else {
            // show dialog box to user to enable location
            appLocationService.askToOnLocation();
        }*/




        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) FragEmployDetails.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        checkLocation();

       // if (isLocationEnabled()== true) {
            System.out.println("bkjknlnlknlknklliihijkkkl");


    System.out.println("bkjknlnlknlknklliihijkkkl" + lat);
    System.out.println("bkjknlnlknlknklliihijkkkl" + lon);
    //String addres = tracker.getCompleteAddressString(dbl_latitude, dbl_longitude, EditRequest.this);
    //location_lat.setText("" + lat);
   // location_long.setText("" + lon);

       // }

        btn_locate_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!cd.isConnectingToInternet()) {
                    userFunctions.cutomToast(getResources().getString(R.string.no_internet), getContext());
                }else {

                    map_captured = pref.getInt("is_map_captured", 0);
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("is_locate_clicked", "");
                    peditor.commit();

                    Intent img_pinch = new Intent(
                            getContext(), MapsActivity.class);

                    startActivity(img_pinch);
                }

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    getdata();

                    boolean ins = true;


                    str_das = edt_das.getText().toString();

                    str_distance_km = edt_distance_km.getText().toString();


                    if (str_das.equals("") && ins == true) {
                        edt_das.setError("Enter Employer Name");
                        edt_das.requestFocus();

                        ins = false;

                    } else {
                        if (str_das.charAt(0) == ' ' && ins == true) {

                            edt_das.setError("Enter valid Employer Name");
                            edt_das.requestFocus();

                            ins = false;
                        } else {
                            edt_das.setError(null);
                        }

                    }


/*
                if (pref.getString("is_locate_clicked",null).equals("")  && ins == true) {
                    userFunctions.cutomToast("Capture MAP by clicking Locate Me Button", getContext());

                    ins = false;

                }*/

                    if (str_distance_type == null && ins == true) {
                        userFunctions.cutomToast("Select Distance Type", getContext());

                        ins = false;

                    }

                    if (str_distance_km.equals("") && ins == true) {

                        userFunctions.cutomToast("Enter Distance in KM", getContext());

                        ins = false;

                    }


                    if (ins == true) {
                        checkLocation();
                        if (isLocationEnabled() == true) {
                        String map_latitude = pref.getString("pref_latitude", "0");
                        String map_longitude = pref.getString("pref_longitude", "0");
                        map_address = pref.getString("map_address", null);


                        System.out.println("lattttttttttttttttt"+lat);

                            if (lat != null ) {
                                lattitude = Double.toString(lat);
                                longitude = Double.toString(lon);
                            }

                        System.out.println("LATITUDEeeeeeeeeeeeeeeeeee" + lat + "::::" + lattitude);

                        Boolean update = dbHelp.update_emp_details(str_das, str_emp_namestr_, str_others, str_emp_type, str_designation, str_userid, str_barnch, str_ts, str_barnch_id, lattitude, longitude, str_distance_type, str_distance_km, map_address);


                        if (update == true) {
                            if (app_type == null || app_type.equals("P")) {
                                Intent messagingActivity = new Intent(getContext(),
                                        Image_List.class);

                                messagingActivity.putExtra("app_id", str_cust_id);
                                messagingActivity.putExtra("str_ts", str_ts);
                                messagingActivity.putExtra("cust_type", app_type);
                                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                startActivity(messagingActivity);


                            } else {
                                if (cd.isConnectingToInternet() == true) {


                                    System.out.println("internet no verify");

                                    dbHelp.update_qde_off("true", str_ts, "1");

                                    callPopup_branch("on");


                                } else {
                                    System.out.println("no internet no verify");

                                    dbHelp.update_qde_off("true", str_ts, "1");


                                }
                            }

                        }

                    }
                }
            }
        });

        adapter_distance_type = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, distance_type);
        adapter_distance_type
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);

/*
        adp_emp_name = new MyAdapter(getActivity(),
                R.qde_layout.qde_sp_display_layout ,R.id.txt_visit, emp_name);

        adp_emp_name .setDropDownViewResource(R.qde_layout.qde_spinner_layout);
        spn_emp_name.setAdapter(adp_emp_name);
        spn_emp_name.setOnItemSelectedListener(this);



        adp_emp_type = new MyAdapter(getActivity(),
                R.qde_layout.qde_sp_display_layout ,R.id.txt_visit, emp_type);

        adp_emp_type .setDropDownViewResource(R.qde_layout.qde_spinner_layout);
        spn_emp_type.setAdapter(adp_emp_type);
        spn_emp_type.setOnItemSelectedListener(this);

        adp_designation = new MyAdapter(getActivity(),
                R.qde_layout.qde_sp_display_layout ,R.id.txt_visit, designation);

        adp_designation .setDropDownViewResource(R.qde_layout.qde_spinner_layout);
        spn_designation.setAdapter(adp_designation);

        spn_designation.setOnItemSelectedListener(this);
*/
        view.setBackgroundColor(Color.WHITE);
        return view;
    }


    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(getActivity(), "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        System.out.println("LATITUDEeeeeeeeeeeeeeeeeee"+lat);
        lon = location.getLongitude();
        System.out.println("LATITUDEeeeeeeeeeeeeeeeeee"+lon);
          location_lat.setText("" + lat);
          location_long.setText("" + lon);

        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(getActivity());
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }
    public  void getdata() {




     //   str_emp_branch = edt_emp_branch.getText().toString();

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;

        }

    }


    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }



    public  void setEditMaxLenght(int len,EditText edt_sample){
        InputFilter[] filter_arr=new InputFilter[1];
        filter_arr[0]=new InputFilter.LengthFilter(len);
        edt_sample.setFilters(filter_arr);
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getContext(),
                HdbHome.class);
        startActivity(home_activity);
        getActivity() .finish();
    }

    private void callPopup_branch(final String  str_entry_type1) {

        System.out.println("entr type::"+str_entry_type1);
        // custom dialog
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        // dialog.setContentView(R.qde_layout.qde_pop_up_branch);
        dialog.setTitle("Add Co-app/ Guarantor ?");
        dialog.setCancelable(false);


        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("app_type", "C");
                peditor.putString("cust_id", str_cust_id);
                // peditor.putString("ts",  pref.getString("ts", null));
                peditor.commit();
                Intent pd_form = new Intent(getActivity(),MainActivity.class);
                startActivity(pd_form);
                dialog.dismiss();

            }
        });


        dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("app_type", "P");
                peditor.commit();


                if (dbHelp.getPending_cases(str_cust_id) == true) {
                    Intent messagingActivity = new Intent(getActivity(),
                            Offine_Entries.class);
                    startActivity(messagingActivity);
                }

                dialog.dismiss();

            }
        });


        dialog.setMessage("Do you want to apply for co-app/Guarantor ?...");

        dialog.show();




    }

}
