package com.example.hdb_los_prod;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;


import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.MyAdapter;

/**
 * Created by Vijay on 1/20/2017.
 */
public class FragAddrDetails   extends Fragment implements AdapterView.OnItemSelectedListener {

    String str_addr1,str_addr2,str_addr3,str_pincode,str_phone1,str_phone2,str_mobile,str_email,str_distance_km,str_landmark,str_curr_years,str_curr_months,str_city_yrs,str_mail_addr;
    Button btn_next;

    static String str_city=null,str_state=null,str_city_off_id=null,str_state_off_id=null;

    ConnectionDetector cd;

    String str_addr1_off,str_addr2_off,str_addr3_off,str_city_off,str_state__off,str_pincode_off,str_phone1_off,str_phone2_off,str_mobile_off,str_email_off,str_distance_km_off,str_landmar_off,str_curr_years_off,str_curr_months_off,str_city_yrs_off,str_mail_addr_off,
    str_std_resi,str_std_off;

    EditText edt_addr1,edt_addr2,edt_addr3,edt_pincode,edt_city,edt_state,edt_phone1,edt_phone2,edt_mobile,edt_email,edt_distance_km,edt_landmark,edt_curr_years,edt_curr_months,edt_city_yrs,edt_mail_addr;
    EditText edt_addr1_off,edt_addr2_off,edt_addr3_off,edt_pincode_off,edt_city_off,edt_state_off,edt_phone1_off,edt_phone2_off,edt_mobile_off,edt_email_off,edt_distance_km_off,edt_landmark_pff,edt_curr_years_off,edt_curr_months_off,edt_city_yrs_off,edt_mail_addr_off,edt_std_resi,edt_std_off;


    RadioGroup addr_grp;
    int pos,addr;

    String str_cust_id;

    int adrr_type;

    RadioButton rb_resi,rb_off;



    Boolean flag_resi=false,flag_off=false;

    String resi_addr=null,off_addr=null;

    DBHelper dbHelper;

    boolean update ;
    boolean update1;

    ImageView btn_resi,btn_off;

    String str_custid,str_mail_addre_resi,str_mail_addre_off,str_ts;

    SharedPreferences pref;

    JSONObject jsonobject;

    JSONArray jsonarray;

    int resi_add,off_add;
    String addr_type;

    RelativeLayout resi_tile,off_title;
    LinearLayout lay_resi,lay_off;

    String str_city_name,str_state_name,city_name_off,state_name_off,str_userid,app_type;

    Spinner spn_mail_addr_resi,spn_mail_addr_off;

    ArrayList<String> al_mail_addr;

    ArrayAdapter adp_mail_addr,adp_mail_addr_off;

    ProgressDialog mProgressDialog;
   UserFunction userFunction;
    CheckBox cb_off;

    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");

    UserFunction userFunctions;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qde_address_details, container, false);

        pref = getContext().getSharedPreferences("MyPref", 0); // 0 -

        str_custid = pref.getString("cust_id", null);
        str_userid = pref.getString("user_id", null);

        app_type = pref.getString("app_type", null);

        str_ts = pref.getString("ts", null);


        update = false;
        update1=false;

        cd = new ConnectionDetector(getContext());

        dbHelper = new DBHelper(getActivity());



        al_mail_addr=new ArrayList<String>();

        al_mail_addr.add("Select Mailing Address");
        al_mail_addr.add("YES");
        al_mail_addr.add("NO");

        cb_off = (CheckBox) view.findViewById(R.id.chk_off);

        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_addr1 = (EditText) view.findViewById(R.id.edt_addr1);
        edt_addr2 = (EditText) view.findViewById(R.id.edt_addr2);
        edt_addr3 = (EditText) view.findViewById(R.id.edt_addr3);
        edt_city = (EditText) view.findViewById(R.id.edt_city);
        edt_pincode = (EditText) view.findViewById(R.id.edt_pincode);
        //edt_state = (EditText) view.findViewById(R.id.edt_state);
        edt_phone1 = (EditText) view.findViewById(R.id.edt_phn1);
        edt_phone2 = (EditText) view.findViewById(R.id.edt_phn2);
        edt_mobile = (EditText) view.findViewById(R.id.edt_mobile);
        edt_distance_km = (EditText) view.findViewById(R.id.edt_distance);
        edt_landmark = (EditText) view.findViewById(R.id.edt_landmark);
        edt_curr_years = (EditText) view.findViewById(R.id.edt_curr_yrs);
        edt_curr_months = (EditText) view.findViewById(R.id.edt_curr_months);
        edt_city_yrs = (EditText) view.findViewById(R.id.edt_city_years);



        edt_email_off = (EditText) view.findViewById(R.id.edt_email_off);
        edt_addr1_off = (EditText) view.findViewById(R.id.edt_addr1_off);
        edt_addr2_off = (EditText) view.findViewById(R.id.edt_addr21_off);
        edt_addr3_off = (EditText) view.findViewById(R.id.edt_addr3_off);
        edt_city_off = (EditText) view.findViewById(R.id.edt_city_off);
        edt_pincode_off = (EditText) view.findViewById(R.id.edt_pincode_off);
        edt_state_off = (EditText) view.findViewById(R.id.edt_state_off);
        edt_phone1_off = (EditText) view.findViewById(R.id.edt_phn1_off);
        edt_phone2_off = (EditText) view.findViewById(R.id.edt_phn2_off);
        edt_mobile_off = (EditText) view.findViewById(R.id.edt_mobile_off);
        edt_distance_km_off = (EditText) view.findViewById(R.id.edt_distance_off);
        edt_landmark_pff = (EditText) view.findViewById(R.id.edt_landmark_off);
        edt_curr_years_off = (EditText) view.findViewById(R.id.edt_curr_yrs_off);
        edt_curr_months_off = (EditText) view.findViewById(R.id.edt_curr_months_off);
        edt_city_yrs_off = (EditText) view.findViewById(R.id.edt_city_years_off);


        edt_std_resi = (EditText) view.findViewById(R.id.edt_std_resi);

        edt_std_off = (EditText) view.findViewById(R.id.edt_std_off);

        spn_mail_addr_off = (Spinner) view.findViewById(R.id.spn_mailing_addr_off);
        spn_mail_addr_resi = (Spinner) view.findViewById(R.id.spn_mailing_addr_resi);

        btn_next = (Button) view.findViewById(R.id.btn_next);

        lay_resi = (LinearLayout) view.findViewById(R.id.lay_resi);

        resi_tile = (RelativeLayout) view.findViewById(R.id.lay_resi_title);

        lay_off = (LinearLayout) view.findViewById(R.id.lay_off);

        off_title = (RelativeLayout) view.findViewById(R.id.off_title);

        userFunctions = new UserFunction(getContext());

        edt_pincode.addTextChangedListener(pincodeWatcher1);
        edt_pincode_off.addTextChangedListener(pincodeWatcher2);

        edt_phone1.addTextChangedListener(addressWatcher);
        edt_phone2.addTextChangedListener(addressWatcher);
        edt_mobile.addTextChangedListener(addressWatcher);

        edt_email.addTextChangedListener(addressWatcher);
        edt_city_yrs.addTextChangedListener(addressWatcher);


        btn_resi = (ImageView) view.findViewById(R.id.btn_resi);

        btn_off = (ImageView) view.findViewById(R.id.btn_off);

        view.setBackgroundColor(Color.WHITE);

        btn_resi.setTag(1);

        int i;
        EditText[]  tFields={ edt_addr1,edt_addr2,edt_addr3,edt_phone1,edt_phone2,edt_mobile,edt_email,edt_distance_km,edt_landmark,edt_curr_years,edt_curr_months,edt_city_yrs,
        edt_addr1_off,edt_addr2_off,edt_addr3_off,edt_phone1_off,edt_phone2_off,edt_mobile_off,edt_email_off,edt_distance_km_off,edt_landmark_pff,edt_curr_years_off,edt_curr_months_off,edt_city_yrs_off,edt_std_resi,edt_std_off };

        System.out.println("LENGHT ARR::::" + tFields.length);

        for(i=0;i<tFields.length;i++) {
            //  inputs[i] = new EditText(getContext());
            System.out.println("LENGHT ARR::isss::" + tFields[i]);

            userFunctions.edtCaps(tFields[i]);

        }

      //  edt_addr1.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_addr2.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_addr3.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // edt_city.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

//        edt_state.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
  //      edt_email.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    //    edt_landmark.setFilters(new InputFilter[]{new InputFilter.AllCaps()});


      //  edt_addr1_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_addr2_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_addr3_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
      //  edt_city_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //edt_state_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_email_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //edt_landmark_pff.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
/*
        edt_addr1.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));

        edt_addr1.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr1.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr1);

        edt_addr2.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));

        edt_addr2.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr2.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr2);


        edt_addr3.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_addr3.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr3.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr3);


        edt_addr1_off.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_addr1_off.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr1_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr1_off);

        edt_addr2_off.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_addr2_off.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr2_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr2_off);

        edt_addr3_off.setKeyListener(DigitsKeyListener.getInstance("0123456789#/_:-%$@*.,abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_addr3_off.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_addr3_off.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_addr3_off);

        edt_landmark.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_landmark.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_landmark.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_landmark);

        edt_landmark_pff.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "));
        edt_landmark_pff.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_landmark_pff.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(40, edt_landmark_pff);


        setEditMaxLenght(40, edt_email);
        setEditMaxLenght(40, edt_email_off);

        */

        adp_mail_addr = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout,R.id.txt_visit, al_mail_addr);

        adp_mail_addr .setDropDownViewResource(R.layout.qde_spinner_layout);

        adp_mail_addr_off = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout,R.id.txt_visit, al_mail_addr);

        adp_mail_addr_off .setDropDownViewResource(R.layout.qde_spinner_layout);

        spn_mail_addr_resi.setAdapter(adp_mail_addr);
        spn_mail_addr_resi.setOnItemSelectedListener(this);

        spn_mail_addr_off.setAdapter(adp_mail_addr_off);
        spn_mail_addr_off.setOnItemSelectedListener(this);


        cb_off.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (cb_off.isChecked()){
                    getdata();
                    set_data();

                    edt_email_off.setError(null);
                    edt_addr1_off.setError(null);
                    edt_addr2_off.setError(null);
                    edt_addr3_off .setError(null);
                    edt_city_off.setError(null);
                    edt_pincode_off.setError(null);
                    edt_state_off.setError(null);
                    edt_phone1_off  .setError(null);
                    edt_phone2_off  .setError(null);
                    edt_mobile_off.setError(null);
                    edt_distance_km_off.setError(null);
                    edt_landmark_pff.setError(null);
                    edt_curr_years_off .setError(null);
                    edt_curr_months_off.setError(null);
                    edt_city_yrs_off.setError(null);

                    edt_std_off.setText(str_std_resi);

                }else{
                    clear_text();

                    clear_data();
                }

            }

        });


                resi_tile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                if (Integer.parseInt(btn_resi.getTag().toString()) == 1) {
                    btn_resi.setBackgroundResource(R.drawable.minus);

                    btn_resi.setTag(0);
                } else {
                    btn_resi.setBackgroundResource(R.drawable.add);
                    btn_resi.setTag(1);
                }


                if (lay_resi.getVisibility() == View.VISIBLE) {
                    lay_resi.setVisibility(View.GONE);
                } else {

                    lay_resi.setVisibility(View.VISIBLE);

                }

            }
        });


        btn_off.setTag(1);
        off_title.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                 if (Integer.parseInt(btn_off.getTag().toString()) == 1) {
                    btn_off.setBackgroundResource(R.drawable.minus);

                    btn_off.setTag(0);
                } else {
                    btn_off.setBackgroundResource(R.drawable.add);
                    btn_off.setTag(1);
                }


                if (lay_off.getVisibility() == View.VISIBLE) {
                    lay_off.setVisibility(View.GONE);
                } else {

                    lay_off.setVisibility(View.VISIBLE);

                }

            }
        });





        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ArrayList<String> pincode = new ArrayList();

                // System.out.println("RB ID::::" + id);
                String resi_email, off_email;

                //    System.out.println("resi email::::" + resi_email + "off email:::" + off_email);

                boolean ins = true;
                getdata();
                getdata_off();


                if (str_addr1.equals("")   && ins == true) {
                    edt_addr1.setError("Enter Address 1 Resi");
                    edt_addr1.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(ins == true && str_addr1.charAt(0)==' ' ){
                        edt_addr1.setError("Enter valid Address 1 Resi");
                        edt_addr1.requestFocus();

                        ins = false;
                        show_resi();
                    }else {
                        if (str_addr1.length() < 5 && ins == true) {
                            edt_addr1.setError("Enter Atleast 5 Charactres in Address 1 Resi");
                            edt_addr1.requestFocus();
                            ins = false;
                            show_resi();
                        } else {
                            edt_addr1.setError(null);
                        }
                    }

                }


                if (str_std_resi.equals("")  && ins == true) {
                    edt_std_resi.setError("Enter Resi STD code");
                    edt_std_resi.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(ins==true && Integer.valueOf(str_std_resi.trim())==0  ){

                        edt_std_resi.setError("Enter valid Resi STD code");
                        edt_std_resi.requestFocus();

                        ins = false;
                        show_resi();
                    }else{
                        edt_std_resi.setError(null);
                    }


                }



                if (str_phone1.equals("") && ins == true) {
                    edt_phone1.setError("Enter Resi Phone 1");
                    edt_phone1.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(ins==true && new BigDecimal(str_phone1.trim()).compareTo(new BigDecimal("0"))==0    ){

                        edt_phone1.setError("Enter valid Resi Phone 1");
                        edt_phone1.requestFocus();

                        ins = false;
                        show_resi();
                    }else{
                        edt_phone1.setError(null);
                    }

                }


                if ((str_mobile.equals("") )&& ins == true) {
                    edt_mobile.setError("Enter Resi mobile");
                    edt_mobile.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(ins == true && new BigDecimal(str_mobile.trim()).compareTo( new BigDecimal("0"))==0  ){
                        edt_mobile.setError("Enter valid Resi mobile");
                        edt_mobile.requestFocus();

                        ins = false;
                        show_resi();
                    }else{
                        edt_mobile.setError(null);

                    }
                }


                if (!str_email.equals("") && ins == true) {
                    if (!Check_pattern(str_email) && ins == true) {

                        edt_email.setError("Enter valid  email ID");
                        edt_email.requestFocus();


                        ins = false;
                        show_resi();
                    }else {
                        edt_email.setError(null);
                    }
                }else{if(str_email.equals("") && ins == true)
                {
                    edt_email.setError("Enter  email ID");
                    edt_email.requestFocus();
                    ins = false;
                    show_resi();
                }else {
                    edt_email.setError(null);
                }
                }



                if (str_distance_km.equals("")&& ins == true) {

                    edt_distance_km.setError("Enter  Distance KM");
                    edt_distance_km.requestFocus();


                    ins = false;
                    show_resi();

                }else {
                    if(ins==true && Integer.valueOf(str_distance_km.trim())==0  ){

                        edt_distance_km.setError("Enter valid Distance KM");
                        edt_distance_km.requestFocus();

                        ins = false;
                        show_resi();
                    }else{
                        edt_distance_km.setError(null);
                    }

                }





                if (str_curr_years.equals("")  && ins == true) {
                    edt_curr_years.setError("Enter Current Years Resi");
                    edt_curr_years.requestFocus();

                    ins = false;
                    show_resi();

                }else {

                    edt_curr_years.setError(null);
                }




                if (str_curr_months.equals("")&& ins == true) {
                    edt_curr_months.setError("Enter Current months Resi");
                    edt_curr_months.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(!str_curr_months.equals("") &&  ins == true) {
                        if (Integer.valueOf(str_curr_months) > 11) {
                            edt_curr_months.setError("Enter Current months Less than 12");
                            edt_curr_months.requestFocus();
                            ins = false;
                            show_resi();
                        } else {
                            edt_curr_months.setError(null);
                        }
                    }

                }




                if (str_city_yrs.equals("")  && ins == true) {
                    edt_city_yrs.setError("Enter city years Resi");
                    edt_city_yrs.requestFocus();

                    ins = false;
                    show_resi();

                }else {
                    if(!str_curr_years.equals("") && !str_city_yrs.equals("") && ins==true){

                        if (Integer.valueOf(str_curr_years) > Integer.valueOf(str_city_yrs)) {
                            edt_city_yrs.setError("City years shoud  be more than or equal to current year");
                            edt_city_yrs.requestFocus();
                            ins = false;
                            show_resi();

                        } else {
                            edt_city_yrs.setError(null);
                        }
                    }

                }

                if((str_mail_addre_resi==null || str_mail_addre_resi.equals("") ) && ins==true) {
                    userFunctions.cutomToast("Select mailing Address Residence", getActivity());
                    ins = false;
                }


                if (str_addr1_off.equals("")  && ins == true) {
                    edt_addr1_off.setError("Enter Address 1 Office");
                    edt_addr1_off.requestFocus();

                    ins = false;
                    show_off();

                }else {
                    if(  ins == true && str_addr1_off.charAt(0)==' ' ){
                        edt_addr1_off.setError("Enter valid Address 1 Office");
                        edt_addr1_off.requestFocus();

                        ins = false;
                        show_off();
                    }else {
                        if (str_addr1_off.length() < 5 && ins == true) {
                            edt_addr1_off.setError("Enter Atleast 5 Charactres in Address 1 Office");
                            edt_addr1_off.requestFocus();
                            ins = false;
                            show_off();

                        } else {
                            edt_addr1_off.setError(null);
                        }
                    }

                }


                if (str_std_off.equals("") && ins == true) {

                    edt_std_off.setError("Enter Office STD code");
                    edt_std_off.requestFocus();

                    ins = false;
                    show_off();

                }else {
                    if(ins==true && Integer.valueOf(str_std_off.trim())==0  ){

                        edt_std_off.setError("Enter valid Office STD code");
                        edt_std_off.requestFocus();

                    ins = false;
                    show_resi();
                }else{
                        edt_std_off.setError(null);
                }


                }


                if ((str_phone1_off.equals("") )&& ins == true) {
                    edt_phone1_off.setError("Enter Office phone 1");
                    edt_phone1_off.requestFocus();


                    ins = false;
                    show_off();

                }else {
                    if( ins == true &&  new BigDecimal(str_phone1_off.trim()).compareTo(new BigDecimal("0"))==0  ){
                        edt_phone1_off.setError("Enter valid Office phone 1");
                        edt_phone1_off.requestFocus();


                        ins = false;
                        show_off();
                    }else {
                        edt_phone1_off.setError(null);

                    }
                }


                if (str_mobile_off.equals("")  && ins == true) {
                    edt_mobile_off.setError("Enter Office mobile");
                    edt_mobile_off.requestFocus();

                    ins = false;
                    show_off();

                }else {
                    if( ins==true &&  new BigDecimal(str_mobile_off.trim()).compareTo(new BigDecimal("0"))==0  ){
                        edt_mobile_off.setError("Enter valid Office mobile");
                        edt_mobile_off.requestFocus();

                        ins = false;
                        show_off();

                    }else {
                        edt_mobile_off.setError(null);
                    }

                }

                if (!str_email_off.equals("") && ins == true) {
                    if (!Check_pattern(str_email_off) && ins == true) {
                        edt_email_off.setError("Enter valid  email ID");
                        edt_email_off.requestFocus();

                        ins = false;
                        show_off();
                    }else {
                        edt_email_off.setError(null);
                    }
                }else{if(str_email_off.equals("") && ins == true)
                {
                    edt_email_off.setError("Enter  email ID");
                    edt_email_off.requestFocus();
                    ins = false;
                    show_off();
                }else {
                    edt_email_off.setError(null);
                }
                }


                if (str_distance_km_off.equals("")&& ins == true) {
                    edt_distance_km_off.setError("Enter Distance in KM office");
                    edt_distance_km_off.requestFocus();

                    ins = false;
                    show_off();

                }else {
                    if(ins==true && Integer.valueOf(str_distance_km_off.trim())==0  ){

                        edt_distance_km_off.setError("Enter valid Distance in KM office");
                        edt_distance_km_off.requestFocus();

                        ins = false;
                        show_resi();
                    }else{
                        edt_distance_km_off.setError(null);
                    }

                }


                if (str_curr_years_off.equals("") && ins == true) {
                    edt_curr_years_off.setError("Enter Current Years Office");
                    edt_curr_years_off.requestFocus();

                    ins = false;
                    show_off();

                }else {
                    edt_curr_years_off.setError(null);
                }


                if (str_curr_months_off.equals("") && ins == true) {
                    edt_curr_months_off.setError("Enter Current months office");
                    edt_curr_months_off.requestFocus();

                    ins = false;
                    show_off();


                }else {
                    if(!str_curr_months_off.equals("") && ins==true) {
                        if (Integer.valueOf(str_curr_months_off) > 11) {
                            edt_curr_months_off.setError("Enter Current months Less than 12");
                            edt_curr_months_off.requestFocus();
                            ins = false;
                            show_off();
                        } else {
                            edt_curr_months_off.setError(null);
                        }
                    }
                }

                if (str_city_yrs_off.equals("")  && ins == true) {
                    edt_city_yrs_off.setError("Enter city years Office");
                    edt_city_yrs_off.requestFocus();

                    ins = false;
                    show_off();

                }else {if( !str_city_yrs_off.equals("") && !str_curr_years_off.equals("") && ins==true) {
                    if (Integer.valueOf(str_curr_years_off) > Integer.valueOf(str_city_yrs_off)) {
                        edt_city_yrs_off.setError("City years shoud  be more than or equal to current year");
                        edt_city_yrs_off.requestFocus();
                        ins = false;
                        show_off();

                    } else {
                        edt_city_yrs_off.setError(null);
                    }
                }
                }

                if((str_mail_addre_off==null || str_mail_addre_off.equals(""))  && ins==true){
                    userFunctions.cutomToast("Select mailing Address Office", getActivity());
                    ins = false;
                }

                if (str_mail_addre_resi!=null && str_mail_addre_off != null && ins == true) {

                    System.out.println("resi addr:::" + str_mail_addre_resi + "::::office address" + str_mail_addre_off);
    if ((str_mail_addre_resi.equals("YES") && str_mail_addre_off.equals("YES"))
            ) {
        userFunctions.cutomToast("Both Residence and Office mailing Addresses cant be Yes or No", getActivity());
        ins = false;
    }

    if ((str_mail_addre_resi.equals("NO") && str_mail_addre_off.equals("NO"))
            ) {
        userFunctions.cutomToast("Both Residence and Office mailing Addresses cant be Yes or No", getActivity());
        ins = false;
    }
}else{if((str_mail_addre_resi==null || str_mail_addre_resi.equals(""))  && ins==true){
    userFunctions.cutomToast("Select mailing Address Residence", getActivity());
    ins = false;
}

    if((str_mail_addre_off==null || str_mail_addre_off.equals(""))  && ins==true){
        userFunctions.cutomToast("Select mailing Address Office", getActivity());
        ins = false;
    }



}



                if (ins == true) {
                    System.out.println("app_type:::::"+app_type);

                    if (app_type == null || app_type.equals("P")) {
                        str_cust_id =getDateTime_ts();
                    } else {

                        str_cust_id = pref.getString("cust_id", null);
                    }

                    SharedPreferences.Editor peditor = pref.edit();

                    peditor.putString("cust_id", str_cust_id);

                    peditor.commit();


                    update = dbHelper.update_qde_resi(str_addr1, str_addr2, str_addr3, str_city, str_state, str_phone1, str_phone2, str_mobile, str_email, str_distance_km, str_landmark, str_curr_years,
                            str_city_yrs, str_mail_addre_resi, str_ts, str_pincode,str_std_resi,str_cust_id);
                    // rb_off.setChecked(true);
                    update1 = dbHelper.update_qde_off(str_addr1_off, str_addr2_off, str_addr3_off, str_city_off_id, str_state_off_id, str_pincode_off, str_phone1_off, str_phone2_off, str_mobile_off, str_email_off, str_distance_km_off, str_landmar_off, str_curr_years_off, str_curr_months_off, str_city_yrs_off, str_mail_addre_off, str_pincode_off, str_ts,str_std_off);

                }


            if(update==true&&update1==true)

            {
                Fragment vehicle = new FragEmployDetails();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, vehicle);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        }
        });

        return view;

    }


  private boolean Check_pattern(String string) {

        Boolean status=false;

        status=  EMAIL_PATTERN.matcher(string).matches();

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }


    @Override

    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {
        switch (arg0.getId()) {

            case R.id.spn_mailing_addr_resi:
                if(position==0){
                    str_mail_addre_resi="";
                }else {
                    str_mail_addre_resi = al_mail_addr.get(position);
                }
                break;


            case R.id.spn_mailing_addr_off:
                    if(position==0){
                        str_mail_addre_off="";
                }else {
                    str_mail_addre_off = al_mail_addr.get(position);
                }
                break;
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }




    public  void getdata(){

       str_email= edt_email.getText().toString();
        str_addr1= edt_addr1.getText().toString();
        str_addr2= edt_addr2.getText().toString();
        str_addr3= edt_addr3.getText().toString();

        str_pincode= edt_pincode.getText().toString();
        str_phone1= edt_phone1.getText().toString();
        str_phone2= edt_phone2.getText().toString();
        str_mobile= edt_mobile.getText().toString();

        str_distance_km= edt_distance_km.getText().toString();
        str_landmark= edt_landmark.getText().toString();
        str_curr_years= edt_curr_years.getText().toString();
        str_curr_months= edt_curr_months.getText().toString();
        str_city_yrs= edt_city_yrs.getText().toString();



        str_std_resi= edt_std_resi.getText().toString();
    }

    public  void getdata_off(){

        str_email_off= edt_email_off.getText().toString();
        str_addr1_off= edt_addr1_off.getText().toString();
        str_addr2_off= edt_addr2_off.getText().toString();
        str_addr3_off= edt_addr3_off.getText().toString();
        str_city_off= edt_city_off.getText().toString();
        str_state__off= edt_state_off.getText().toString();
        str_pincode_off= edt_pincode_off.getText().toString();
        str_phone1_off= edt_phone1_off.getText().toString();
        str_phone2_off= edt_phone2_off.getText().toString();
        str_mobile_off= edt_mobile_off.getText().toString();

        str_distance_km_off= edt_distance_km_off.getText().toString();
        str_landmar_off= edt_landmark_pff.getText().toString();
        str_curr_years_off= edt_curr_years_off.getText().toString();
        str_curr_months_off= edt_curr_months_off.getText().toString();
        str_city_yrs_off= edt_city_yrs_off.getText().toString();


        str_std_off= edt_std_off.getText().toString();

    }

    public void clear_text(){

        edt_addr1_off .setText("");
        edt_addr2_off .setText("");
        edt_addr3_off .setText("");
        edt_city_off  .setText("");
        edt_pincode_off .setText("");
        edt_state_off  .setText("");

        edt_distance_km_off .setText("");
        edt_landmark_pff  .setText("");
        edt_curr_years_off  .setText("");
        edt_curr_months_off .setText("");
        edt_std_off .setText("");

    }

    public  void  clear_data(){

      clear_text();

        str_email_off="";
        str_addr1_off= "";
        str_addr2_off= "";
        str_addr3_off= "";

        str_std_off= "";
        str_pincode_off= "";

        str_distance_km_off= "";
        str_landmar_off= "";
        str_curr_years_off="";
        str_curr_months_off= "";
        str_city_yrs_off= "";


    }

    public  void cleardata(){

        clear_text();

        str_addr1_off= "";
        str_addr2_off= "";
        str_addr3_off= "";
        str_city_off= "";
        str_state__off= "";
        str_pincode_off= "";

        str_distance_km_off= "";
        str_landmar_off="";
        str_curr_years_off= "";
        str_curr_months_off= "";
        str_city_yrs_off="";

    }

    public void set_data(){
        edt_email_off .setText(str_email);
        edt_addr1_off .setText(str_addr1);
        edt_addr2_off .setText(str_addr2);
        edt_addr3_off .setText(str_addr3);
        edt_city_off.setText(str_city_name);
        edt_pincode_off.setText(str_pincode);
        edt_state_off.setText(str_state_name);
        edt_phone1_off  .setText(str_phone1);
        edt_phone2_off  .setText(str_phone2);
        edt_mobile_off.setText(str_mobile);
        edt_distance_km_off .setText(str_distance_km);
        edt_landmark_pff.setText(str_landmark);
        edt_curr_years_off  .setText(str_curr_years);
        edt_curr_months_off.setText(str_curr_months);
        edt_city_yrs_off.setText(str_city_yrs);

        edt_std_off.setText(str_std_resi);

    }

    public void set_data_roff(){
        edt_email .setText(str_email_off);
        edt_addr1 .setText(str_addr1_off);
        edt_addr2 .setText(str_addr2_off);
        edt_addr3 .setText(str_addr3_off);
        edt_city.setText(str_city_off);
        edt_pincode .setText(str_pincode_off);
        edt_state  .setText(str_state__off);
        edt_phone1  .setText(str_phone1_off);
        edt_phone2  .setText(str_phone2_off);
        edt_mobile .setText(str_mobile_off);
        edt_distance_km .setText(str_distance_km_off);
        edt_landmark  .setText(str_landmar_off);
        edt_curr_years.setText(str_curr_years_off);
        edt_curr_months .setText(str_curr_months_off);
        edt_city_yrs  .setText(str_city_yrs_off);
        edt_mail_addr .setText(str_mail_addr_off);
    }


    private  final TextWatcher pincodeWatcher1=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() ==6) {

                str_pincode = edt_pincode.getText().toString();

                str_pincode_off = edt_pincode_off.getText().toString();



                str_state=null;

                str_city=null;

                new  Download_Masters().execute("resi");


                        }
        }


    }   ;


    private  final TextWatcher pincodeWatcher2=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (editable.length() ==6) {


            //    str_pincode = edt_pincode.getText().toString();

                str_city_off_id=null;
                str_state_off_id=null;
                 str_pincode_off = edt_pincode_off.getText().toString();

                new  Download_Masters().execute("off");

            }
        }

    }   ;

    private  final TextWatcher addressWatcher=new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if(edt_phone1.getText().hashCode()==editable.hashCode()){
                edt_phone1_off.setText(edt_phone1.getText().toString());
            }


            if(edt_phone2.getText().hashCode()==editable.hashCode()){
                edt_phone2_off.setText(edt_phone2.getText().toString());
            }

            if(edt_mobile.getText().hashCode()==editable.hashCode()){
                edt_mobile_off.setText(edt_mobile.getText().toString());
            }

            if(edt_email.getText().hashCode()==editable.hashCode()){
                edt_email_off.setText(edt_email.getText().toString());
            }

            if(edt_city_yrs.getText().hashCode()==editable.hashCode()){
                edt_city_yrs_off.setText(edt_city_yrs.getText().toString());
            }
        }


    }   ;

    public class Download_Masters extends AsyncTask<String, String, String> {

        protected void onPreExecute() {


            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());

            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                 addr_type = params[0];

                if(addr_type=="resi"){
                    jsonobject = userFunction.get_pincode(str_pincode);
                }else{
                    jsonobject = userFunction.get_pincode(str_pincode_off);
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute (String args){

            String city_id,city_name,state_id,state_name;
            //System.out.println("JSONARR::::::" + jsonarray);
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getActivity().getResources().getString(R.string.error_message),
                            getActivity());
                    System.out.println("JSON OBJ NULL:::");

                    edt_city.setText("");

                    edt_state.setText("");

                    edt_city_off.setText("");

                    edt_state_off.setText("");
                    	mProgressDialog.dismiss();

                } else {


                    if(!jsonobject.getString("success").equals("false")){

                        city_id= jsonobject.getString("city_id");
                        city_name= jsonobject.getString("city_name");
                        state_id= jsonobject.getString("state_id");
                        state_name= jsonobject.getString("state_name");

                        System.out.println("city_name:::" + city_name + ":::state::::" + state_name);



                        if(addr_type.equals("resi")) {
                            str_city_name = city_name;
                            str_state_name = state_name;

                            str_city=city_id;
                            str_state=state_id;

                            edt_city.setText(str_city_name);
                            edt_state.setText(str_state_name);

                        }

                        if(addr_type.equals("off")){
                            str_city_off = city_name;
                            str_state__off = state_name;

                            str_city_off_id=city_id;
                            str_state_off_id=state_id;

                            edt_city_off.setText(str_city_off);
                            edt_state_off.setText(str_state__off);

                        }

                        mProgressDialog.dismiss();

                    }
                    else{

                        userFunction.cutomToast(jsonobject.getString("Error_msg"),getActivity());
                        mProgressDialog.dismiss();
                    }
                    //   txt_error.setVisibility(View.GONE);

                    //if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    //System.out.println("PARAM:::"+param);

/*
					} else {
						mProgressDialog.dismiss();
						SharedPreferences.Editor editor = pref.edit();
						editor.putInt("is_login", 0);
						editor.commit();
						loginErrorMsg.setText(jsonobject.getString("message").toString());
						//pd_details_layout.setVisibility(View.GONE);


						//pd_details_layout.setVisibility(View.GONE);

					}
*/
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public  void setEditMaxLenght(int len,EditText edt_sample){
        InputFilter[] filter_arr=new InputFilter[1];
        filter_arr[0]=new InputFilter.LengthFilter(len);
        edt_sample.setFilters(filter_arr);
    }


    void show_resi(){
        lay_resi.setVisibility(View.VISIBLE);
        lay_off.setVisibility(View.GONE);

    }

    void show_off(){
        lay_resi.setVisibility(View.GONE);
        lay_off.setVisibility(View.VISIBLE);

    }

    private String getDateTime_ts() {
        SimpleDateFormat s = new SimpleDateFormat("yyyyhhmmss");
        String format = s.format(new Date());

       // str_userid = str_userid.replace("HDB","");

        return str_userid+format;
    }
    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getContext(),
                HdbHome.class);
        startActivity(home_activity);
        getActivity() .finish();
    }


}
