package com.example.hdb_los_prod;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.img_upload_retrofit.Image_List;
import com.example.hdb_los_prod.libraries.UserFunction;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;

/**
 * Created by DELL on 12/8/2017.
 */

public class Loan_Result extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    UserFunction userFunction;
    JSONObject jsonobject = null;
    JSONObject json = null;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    String str_message;
    ConnectionDetector cd;
    String str_user_id, str_losid;
    SharedPreferences pref;

    String str_name, str_branch_id, str_branch_name, str_product, str_app_type, str_loan_no, str_cust_id,
    img_upld_count,img_tot_count;
    TextView txt_error, txt_message;
    Button btn_capture_pic, btn_verify;
    String str_app_no;
    String str_ts,losid,cust_name,product,cust_type;
    String str_app_nw_type;
    Integer int_is_verified;
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    Loan_Result_Adapter adapter;
    GridView mList;
    public static String img_name = "img_name";
    public static String img_data = "img_data";
    public static String img_no = "img_no";
    public static String img_count = "img_count";

    public static String dms_count = "dms_count";

    double dbl_latitude;
    double dbl_longitude;
    String lattitude = null, longitude = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.qde_loan_result);
        userFunction = new UserFunction(this);
        cd = new ConnectionDetector(Loan_Result.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        System.out.println("str_user_id::::" + str_user_id);
        arraylist = new ArrayList<HashMap<String, String>>();


        Intent i = getIntent();
        str_losid = i.getStringExtra("losid");
        str_cust_id = i.getStringExtra("app_id");
        str_ts = i.getStringExtra("str_ts");
        str_app_type = i.getStringExtra("cust_type");
        cust_name = i.getStringExtra("cust_name");
        product = i.getStringExtra("product");
        img_upld_count = i.getStringExtra("img_uplod_count");
        img_tot_count = i.getStringExtra("img_tot_cnt");


            setlayout();

            btn_capture_pic.setVisibility(View.VISIBLE);



    }


    private void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        txt_message = (TextView) findViewById(R.id.text_error_msg);
        btn_capture_pic = (Button) findViewById(R.id.btn_add_customer);

        mList = (GridView) findViewById(R.id.grid);

        Button btn_add_customer = (Button) findViewById(R.id.btn_add_customer);




        if (cd.isConnectingToInternet()) {
            // new VerifyLos().execute();
            new SearchLos().execute();

        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }

        btn_capture_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent pd_form = new Intent(getApplicationContext(), Image_List.class);
                pd_form.putExtra("app_id",str_cust_id);

                pd_form.putExtra("str_ts",str_ts);

                pd_form.putExtra("cust_type",str_app_type);

                startActivity(pd_form);

            }
        });

    }


    public class SearchLos extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Loan_Result.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            // txt_error.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            jsonobject=null;
            json = jsonobject;
            jsonobject = userFunction.
                    loan_search( str_ts,str_app_type);
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);
                        String error_msg = jsonobject.getString("message");
                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {



                            jsonarray = jsonobject.getJSONArray("Data");
                            if(jsonarray.length()>0) {
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    HashMap<String, String> map = new HashMap<>();
                                    jsonobject = jsonarray.getJSONObject(i);
                                    map.put("img_name", jsonobject.getString("img_name"));
                                    map.put("img_data", jsonobject.getString("img_data"));
                                    map.put("img_no", jsonobject.getString("img_no"));
                                    map.put("img_count", jsonobject.getString("img_count"));
                                    map.put("dms_count", jsonobject.getString("count_DMS"));


                                    // Set the JSON Objects into the array
                                    arraylist.add(map);
                                }
                            }

                        } else {
                            str_message = error_msg;
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            TextView txt_name = (TextView) findViewById(R.id.txt_cust_name);
            TextView txt_app_type = (TextView) findViewById(R.id.txt_applicant_type);
            TextView txt_loan_no = (TextView) findViewById(R.id.txt_losid);


            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                    String str_type_app = null;
                    String type_app = str_app_type;

                    switch (type_app) {
                        case "P":
                            str_type_app = "Primary Applicant";
                            break;
                        case "C":
                            str_type_app = "CO-Applicant";
                            break;
                        case "G":
                            str_type_app = "Guarantor";
                            break;

                        default:
                            str_type_app = "";
                    }

                    adapter = new Loan_Result_Adapter(getApplicationContext(), arraylist);
                    adapter.notifyDataSetChanged();
                    mList.setAdapter(adapter);

                    txt_error.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);

                    txt_name.setText(cust_name);

                    txt_app_type.setText(str_app_type);
                    txt_loan_no.setText(str_losid);


                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    // pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }


            }
            mProgressDialog.dismiss();

        }

    }



    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

}


