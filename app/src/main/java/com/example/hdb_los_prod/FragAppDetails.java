package com.example.hdb_los_prod;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.MyAdapter;
import com.example.hdb_los_prod.libraries.SelectDateFragment;
import com.example.hdb_los_prod.libraries.UserFunction;



import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by Vijay on 1/20/2017.
 */
public class FragAppDetails extends Fragment implements AdapterView.OnItemSelectedListener {

    String str_pan, str_dob, str_fname, str_mname, str_lname, str_drining_lic, str_aadhar, str_asset_manu, str_asset_model, str_asset_cost, str_margin_money, str_loan_amt, str_tenure,
            str_consent_to_call, str_passport, str_voter, str_asset_make, str_age,str_father_name,str_mother_name;
    TextView txt_date;
    EditText edt_dob, edt_pan, edt_fname, edt_mname, edt_lname, edt_drining_lic, edt_aadhar, edt_asset_cost, edt_margin, edt_loan_amt, edt_tenure,
            edt_passport, edt_voter,edt_father_name,edt_mother_name;
    Button btn_next;
    RadioGroup sex_grp;
    String sex, gender;
    Date curr_date;
    int pos;
    Spinner spn_app_type, spn_relation, spn_product, spn_catagory, spn_const, spn_asset_catg, spn_asset_manu, spn_asset_model, spn_asset_make;
    String str_app_type, str_relation, str_product, str_catagory, str_const, str_asset_catg, str_ts, str_make1, app_type;
    ArrayAdapter adp_prod, adp_catagory, adp_const, adp_asset_catg, adp_asset_manu, adp_asset_model, adp_coapp_rel, adp_cust_actg, adp_app_type, adp_asset_make;

    ArrayList<String> al_product, al_cons, al_catg, al_coapp_rel, al_manu, al_model, al_app_type, al_make, al_make_str;
    ArrayList<String> al_product_val, al_cons_val, al_catg_val, al_coapp_rel_val, al_manu_val, al_model_val, al_cust_cat, al_cust_catg_val, app_type_val, al_make_val, al_make1, al_values, al_prod_catg;


    RelativeLayout co_relation, lay_product_txt, lay_cust_catg_txt, lay_asset_catg_txt, lay_asset_man_txt, lay_asset_make_txt, lay_model_txt;
    RelativeLayout lay_product, lay_cust_catg, lay_asset_catgt, lay_asset_man, lay_asset_make, lay_model, lay_apptype;
    TextView txt_prod, txt_cust_catg, txt_asset_catg, txt_aset_man, txt_asset_model, txt_asset_make;

    EditText[] inputs;

    DatePickerDialog datePickerDialog;


    Cursor cursor;
    // lay_product.setVisibility(View.GONE);
    //   lay_cust_catg.setVisibility(View.GONE);

    // lay_asset_catg.setVisibility(View.GONE);
    //  lay_asset_man.setVisibility(View.GONE);
    //  lay_asset_make.setVisibility(View.GONE);
    //  lay_model.setVisibility(View.GONE);
    String ca_prod, ca_cust_catg, ca_asset_catg, ca_asset_man, ca_asset_make, ca_asset_model, str_userid, str_prod_catg,
            ca_asset_cost, ca_margin_money, ca_loan_amt, ca_tenure;



    DBHelper dbHelp;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");
    SharedPreferences pref, pData;


    UserFunction userFunctions;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qde_app_details, container, false);

        userFunctions = new UserFunction(getContext());


        pref = getContext().getSharedPreferences("MyPref", 0); // 0 -

        app_type = pref.getString("app_type", null);

        System.out.println("APP type::::"+app_type);

        str_userid = pref.getString("user_id", null);

        co_relation = (RelativeLayout) view.findViewById(R.id.tableRow5);

        lay_apptype = (RelativeLayout) view.findViewById(R.id.lay_app_type);


        curr_date = new Date();
        al_prod_catg = new ArrayList<String>();

        al_product = new ArrayList<String>();
        al_cons = new ArrayList<String>();
        al_catg = new ArrayList<String>();
        al_product_val = new ArrayList<String>();
        al_cons_val = new ArrayList<String>();
        al_catg_val = new ArrayList<String>();
        al_manu = new ArrayList<String>();
        al_manu_val = new ArrayList<String>();

        al_make = new ArrayList<String>();
        al_make_val = new ArrayList<String>();

        al_make1 = new ArrayList<String>();

        al_make_str = new ArrayList<String>();

        al_model = new ArrayList<String>();
        al_model_val = new ArrayList<String>();

        al_coapp_rel = new ArrayList<String>();
        al_coapp_rel_val = new ArrayList<String>();

        al_cust_cat = new ArrayList<String>();
        al_cust_catg_val = new ArrayList<String>();

        al_app_type = new ArrayList<String>();
        app_type_val = new ArrayList<String>();

        dbHelp = new DBHelper(getActivity());
        al_product.clear();

        al_product = dbHelp.get_masters_pd("PD");

        al_cons.clear();
        al_cons = dbHelp.get_masters_pd("CONS");

        //  al_catg=dbHelp.get_masters_pd("CC");
        al_coapp_rel = dbHelp.get_masters_pd("CR");

        al_coapp_rel_val = dbHelp.get_masters_pd_val("CR");

        al_product_val.clear();
        al_product_val = dbHelp.get_masters_pd_val("PD");

        al_cons_val.clear();
        al_cons_val = dbHelp.get_masters_pd_val("CONS");

        al_cust_catg_val.clear();
        al_cust_catg_val = dbHelp.get_masters_pd_val("CC");

        al_cust_cat.clear();
        al_cust_cat = dbHelp.get_masters_pd("CC");

        app_type_val.clear();

        al_app_type.clear();

        al_app_type = dbHelp.get_masters_pd_val("APP");

        app_type_val = dbHelp.get_masters_pd("APP");

        al_catg.add("Select Asset Catagory");
        al_manu.add("Select Asset Manufacturer");
        al_model.add("Select Asset Model");
        al_make_str.add("Select Asset Make");


        al_prod_catg.clear();
        al_prod_catg = dbHelp.get_prod_catg();


        System.out.println("PROD catg:::" + al_prod_catg);


        lay_product_txt = (RelativeLayout) view.findViewById(R.id.lay_product_txt);
        lay_cust_catg_txt = (RelativeLayout) view.findViewById(R.id.lay_cust_catg_txt);
        lay_asset_catg_txt = (RelativeLayout) view.findViewById(R.id.lay_asset_catg_txt);

        lay_asset_man_txt = (RelativeLayout) view.findViewById(R.id.lay_asset_man_txt);
        //lay_asset_make=(RelativeLayout) view.findViewById(R.id.lay_ass);

        lay_model_txt = (RelativeLayout) view.findViewById(R.id.lay_asset_model_txt);

        lay_asset_make_txt = (RelativeLayout) view.findViewById(R.id.lay_asset_make_txt);


        lay_product = (RelativeLayout) view.findViewById(R.id.lay_product);
        lay_cust_catg = (RelativeLayout) view.findViewById(R.id.lay_cust_catg);
        lay_asset_catgt = (RelativeLayout) view.findViewById(R.id.lay_asset_catg);

        lay_asset_man = (RelativeLayout) view.findViewById(R.id.lay_asset_man);
        //lay_asset_make=(RelativeLayout) view.findViewById(R.id.lay_ass);

        lay_model = (RelativeLayout) view.findViewById(R.id.lay_asset_model);

        lay_asset_make = (RelativeLayout) view.findViewById(R.id.lay_asset_make);


        System.out.println("App type:::" + al_app_type + "ccc" + app_type_val + ":::");

//dbHelp.deleteAll();
        txt_date = (TextView) view.findViewById(R.id.txt_age);
        edt_dob = (EditText) view.findViewById(R.id.edt_dob);
        edt_pan = (EditText) view.findViewById(R.id.edt_pan_no);
        btn_next = (Button) view.findViewById(R.id.btn_getotp);
        sex_grp = (RadioGroup) view.findViewById(R.id.sexGroup);
        spn_app_type = (Spinner) view.findViewById(R.id.spn_app_tpe);
        spn_catagory = (Spinner) view.findViewById(R.id.spn_catagory);
        spn_const = (Spinner) view.findViewById(R.id.spn_const);
        spn_product = (Spinner) view.findViewById(R.id.spn_product);
        spn_relation = (Spinner) view.findViewById(R.id.spn_relation);
        spn_asset_catg = (Spinner) view.findViewById(R.id.spn_asset_catg);
        spn_asset_manu = (Spinner) view.findViewById(R.id.spn_asset_manu);
        spn_asset_model = (Spinner) view.findViewById(R.id.spn_asset_model);
        spn_asset_make = (Spinner) view.findViewById(R.id.spn_asset_make);

        edt_fname = (EditText) view.findViewById(R.id.edt_fn);
        //edt_fname.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        //edt_fname.setAllCaps(true);
        edt_mname = (EditText) view.findViewById(R.id.edt_mn);
        edt_lname = (EditText) view.findViewById(R.id.edt_ln);
        edt_drining_lic = (EditText) view.findViewById(R.id.edt_driving_lic);
        edt_aadhar = (EditText) view.findViewById(R.id.edt_aadhar_no);

        edt_asset_cost = (EditText) view.findViewById(R.id.edt_asset_cost);
        edt_margin = (EditText) view.findViewById(R.id.edt_margin_money);
        edt_loan_amt = (EditText) view.findViewById(R.id.edt_loan_amt);
        edt_tenure = (EditText) view.findViewById(R.id.edt_tenure);

        edt_passport = (EditText) view.findViewById(R.id.edt_passport);
        edt_voter = (EditText) view.findViewById(R.id.edt_voter_id);
        edt_father_name = (EditText) view.findViewById(R.id.edt_father_name);
        edt_mother_name = (EditText) view.findViewById(R.id.edt_mothers_name);

        txt_prod = (TextView) view.findViewById(R.id.txt_product);

        txt_cust_catg = (TextView) view.findViewById(R.id.txt_cust_catg);
        txt_asset_catg = (TextView) view.findViewById(R.id.txt_asset_catg);
        txt_aset_man = (TextView) view.findViewById(R.id.txt_asset_man);
        txt_asset_model = (TextView) view.findViewById(R.id.txt_asset_model);

        txt_asset_make = (TextView) view.findViewById(R.id.txt_asset_make);

        int i;
        EditText[] tFields = {edt_fname, edt_mname, edt_lname, edt_drining_lic, edt_aadhar, edt_asset_cost, edt_margin, edt_loan_amt, edt_tenure,
                edt_passport, edt_voter,edt_father_name,edt_mother_name};

        System.out.println("LENGHT ARR::::" + tFields.length);

        for (i = 0; i < tFields.length; i++) {
            //  inputs[i] = new EditText(getContext());
            userFunctions.edtCaps(tFields[i]);
        }

        // edt_fname.addTextChangedListener(CapsText);


        if (app_type == null || app_type.equals("P")) {
            co_relation.setVisibility(View.GONE);
            //   al_app_type.remove("Guarantor");
            //  app_type_val.remove("G");

            //  al_app_type.remove("Co-Applicant");
            //  app_type_val.remove("C");

            lay_apptype.setVisibility(View.GONE);
            str_app_type = "P";

            adp_app_type = new MyAdapter(getActivity(),
                    R.layout.qde_sp_display_layout, R.id.txt_visit, al_app_type);

            adp_app_type.setDropDownViewResource(R.layout.qde_spinner_layout);
            spn_app_type.setAdapter(adp_app_type);
            spn_app_type.setOnItemSelectedListener(this);

            lay_product_txt.setVisibility(View.GONE);
            lay_cust_catg_txt.setVisibility(View.GONE);

            lay_asset_catg_txt.setVisibility(View.GONE);
            lay_asset_man_txt.setVisibility(View.GONE);
            //  lay_asset_make.setVisibility(View.GONE);
            lay_model_txt.setVisibility(View.GONE);

            lay_asset_make_txt.setVisibility(View.GONE);


        } else {

            str_prod_catg = pref.getString("prod_catg", null);


            cursor = dbHelp.get_applicant_details(pref.getString("cust_id", null));

            ca_prod = cursor.getString(cursor.getColumnIndexOrThrow("product1"));
            ca_cust_catg = cursor.getString(cursor.getColumnIndexOrThrow("catagory"));
            ca_asset_catg = cursor.getString(cursor.getColumnIndexOrThrow("asset_catg"));
            ca_asset_man = cursor.getString(cursor.getColumnIndexOrThrow("aseet_man"));
            ca_asset_model = cursor.getString(cursor.getColumnIndexOrThrow("asset_model"));
            ca_asset_make = cursor.getString(cursor.getColumnIndexOrThrow("asset_make"));

            ca_asset_cost = cursor.getString(cursor.getColumnIndexOrThrow("asset_cost"));

            ca_margin_money = cursor.getString(cursor.getColumnIndexOrThrow("margin_money"));

            ca_loan_amt = cursor.getString(cursor.getColumnIndexOrThrow("loan_amt"));
            ca_tenure = cursor.getString(cursor.getColumnIndexOrThrow("tenure_months"));

            System.out.println(dbHelp.get_values(ca_prod, ca_cust_catg, ca_asset_catg, ca_asset_man, ca_asset_model, ca_asset_make));

            al_values = dbHelp.get_values(ca_prod, ca_cust_catg, ca_asset_catg, ca_asset_man, ca_asset_model, ca_asset_make);


            System.out.println("al val::::"+al_values.size());
            if(ca_prod.length()>-2){
            txt_prod.setText("Product: " + al_values.get((0)));}
            if(ca_cust_catg.length()>-1){
            txt_cust_catg.setText("Cust Catagory: " + al_values.get(1));}

            if(al_values.size()>2) {
                if (ca_asset_catg.length() > -1) {
                    txt_asset_catg.setText("Asset Catagory: " + al_values.get((2)));
                }
                if (ca_asset_man.length() > -1) {
                    txt_aset_man.setText("Asset manufacturer: " + al_values.get((3)));
                }
                if (ca_asset_model.length() > -1) {
                    txt_asset_model.setText("Asset Model: " + al_values.get((4)));
                }
                if (ca_asset_make.length() > -1) {
                    txt_asset_make.setText("Asset Make: " + al_values.get((5)));
                }
            }else {

                lay_asset_catg_txt.setVisibility(View.GONE);
                lay_asset_man_txt.setVisibility(View.GONE);

                lay_model_txt.setVisibility(View.GONE);

                lay_asset_make_txt.setVisibility(View.GONE);
            }


            edt_asset_cost.setEnabled(false);
            edt_asset_cost.setText(ca_asset_cost);


            edt_margin.setEnabled(false);
            edt_margin.setText(ca_margin_money);

            edt_loan_amt.setEnabled(false);
            edt_loan_amt.setText(ca_loan_amt);

            edt_tenure.setEnabled(false);
            edt_tenure.setText(ca_tenure);

            lay_product.setVisibility(View.GONE);
            lay_cust_catg.setVisibility(View.GONE);

            lay_asset_catgt.setVisibility(View.GONE);
            lay_asset_man.setVisibility(View.GONE);
            //  lay_asset_make.setVisibility(View.GONE);
            lay_model.setVisibility(View.GONE);
            lay_asset_make.setVisibility(View.GONE);


            al_app_type.remove("Applicant");
            app_type_val.remove("P");

            adp_app_type = new MyAdapter(getActivity(),
                    R.layout.qde_sp_display_layout, R.id.txt_visit, al_app_type);

            adp_app_type.setDropDownViewResource(R.layout.qde_spinner_layout);
            spn_app_type.setAdapter(adp_app_type);
            spn_app_type.setOnItemSelectedListener(this);


        }

        // edt_pan.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //  edt_fname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        // edt_mname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        // edt_lname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});



       /* edt_fname.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/ "));

      //  edt_fname.setRawInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS );

        //edt_fname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        //setEditMaxLenght(50, edt_fname);

     //   edt_mname.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/ "));

      //  edt_mname.setRawInputType(InputType.TYPE_CLASS_TEXT);

       // edt_mname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
       // setEditMaxLenght(50, edt_mname);



        edt_lname.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/ "));

        edt_lname.setRawInputType(InputType.TYPE_CLASS_TEXT);

        edt_lname.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(50, edt_lname);

        edt_drining_lic.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/.- "));

        edt_drining_lic.setRawInputType(InputType.TYPE_CLASS_TEXT);



        edt_drining_lic.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(15, edt_drining_lic);

        edt_passport.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ./- "));

        edt_passport.setRawInputType(InputType.TYPE_CLASS_TEXT);

        edt_passport.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        setEditMaxLenght(15, edt_passport);


        edt_voter.setKeyListener(DigitsKeyListener.getInstance("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ./- "));

        edt_voter.setRawInputType(InputType.TYPE_CLASS_TEXT);
        edt_voter.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        setEditMaxLenght(15, edt_voter);

*/

        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calender = Calendar.getInstance();

             int  mYear=  calender.get(Calendar.YEAR);
              int mMonth=  calender.get(Calendar.MONTH);
              int mDay= calender.get(Calendar.DAY_OF_MONTH);

                datePickerDialog= new DatePickerDialog(getActivity(),android.R.style.Theme_Holo_Dialog,
                        new DatePickerDialog.OnDateSetListener(){

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                String curr_date1 = "01-03-2017";

                                if (userFunctions.dateCompare(dayOfMonth, monthOfYear + 1, year, curr_date1).equals("")) {
                                    edt_dob.setText("");
                                    txt_date.setText("");
                                    //  edt_dob.setError("Minimum Age should be 18 ");
                                    //  edt_dob.requestFocus();

                                    userFunctions.cutomToast("Minimumm Age should be 18", getActivity());
                                } else {

                                    String age = userFunctions.dateCompare(dayOfMonth, monthOfYear + 1, year, curr_date1);
                                    txt_date.setText(String.valueOf(age));

                                    String s = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                    Date df1 = null;
                                    String df2 = null;

                                    SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                                    try {

                                        df1 = formater.parse(s);
                                        df2 = formater.format(df1);

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }


                                    edt_dob.setText(df2 + "+05:30");

                                }

                            }


                },mYear,mMonth,mDay);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            datePickerDialog.show();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getdata();

                boolean ins = true;


                try {
                    if (str_fname.equals("") && ins == true) {
                        edt_fname.setError("Enter First Name");
                        edt_fname.requestFocus();
                        ins = false;
                    } else {
                        if (str_fname.charAt(0) == ' ' && ins == true) {
                            edt_fname.setError("Enter valid First Name");
                            edt_fname.requestFocus();
                            ins = false;
                        } else {

                            edt_fname.setError(null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_lname.equals("") && ins == true) {
                        edt_lname.setError("Enter Last Name");
                        edt_lname.requestFocus();
                        ins = false;
                    } else {
                        if (ins == true && str_lname.charAt(0) == ' ') {
                            edt_lname.setError("Enter valid Last Name");
                            edt_lname.requestFocus();
                            ins = false;
                        } else {
                            edt_lname.setError(null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if ((edt_father_name.getText().toString().equals("") || edt_father_name.toString().charAt(0) == ' ' || edt_father_name.getText().toString().equals(" ")) && ins == true) {
                        edt_father_name.setError("Enter Fathers Name");
                        edt_father_name.requestFocus();
                        ins = false;
                    } else {
                        if (ins == true ) {
                            int count_space=0;
                            for(int i=0;i<=edt_father_name.getText().toString().length()-1;i++) {
                                if(edt_father_name.getText().toString().charAt(i)==' ') {

                                    if(i!=edt_father_name.getText().toString().length()-1) {
                                        count_space++;
                                    }
                                }
                            }
                            if(count_space<1){
                                edt_father_name.setError("Enter valid Fathers Name");
                                edt_father_name.requestFocus();
                                ins = false;

                            }
                        } else {
                            edt_father_name.setError(null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



                try {
                    if ((edt_mother_name.getText().toString().equals("")  || edt_mother_name.toString().charAt(0) == ' ' || edt_mother_name.getText().toString().equals(" ") )&& ins == true) {
                        edt_mother_name.setError("Enter Mother's Maiden Name");
                        edt_mother_name.requestFocus();
                        ins = false;
                    } else {

                        int count_space=0;
                        for(int i=0;i<=edt_mother_name.getText().toString().length()-1;i++) {

                            if(edt_mother_name.getText().toString().charAt(i)==' ') {
                                if(i!=edt_mother_name.getText().toString().length()-1) {
                                    count_space++;
                                }
                            }
                        }
                        if(count_space>0){

                            edt_mother_name.setError("Enter valid  Mother's Maiden Name");
                            edt_mother_name.requestFocus();
                            ins = false;

                        }else{
                            edt_mother_name.setError(null);

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }




                try {
                    if ((app_type != null || !app_type.equals("P")) && ins == true) {
                        if (str_app_type == null && ins == true) {
                            userFunctions.cutomToast("Select Applicant Type", getActivity());
                            ins = false;
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                System.out.println("STR RELATION::::" + str_relation);

                try {
                    if (app_type != null || !app_type.equals("P")) {

                        if (str_relation == null && ins == true) {

                            userFunctions.cutomToast("Select Customer Relation", getActivity());
                            ins = false;
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (app_type == null || app_type.equals("P")) {
                    try {
                        if (str_product == null && ins == true) {

                            userFunctions.cutomToast("Select Product", getActivity());
                            ins = false;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (app_type == null || app_type.equals("P")) {
                    try {
                        if (str_catagory == null && ins == true) {

                            userFunctions.cutomToast("Select Customer Catagory", getActivity());
                            ins = false;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                try {
                    if (str_const == null && ins == true) {

                        userFunctions.cutomToast("Select Constituition", getActivity());
                        ins = false;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (gender == null && ins == true) {
                        userFunctions.cutomToast("Select Gender", getActivity());

                        ins = false;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_dob.equals("") && ins == true) {
                        edt_dob.setError("Select DOB");
                        edt_dob.requestFocus();


                        ins = false;
                    } else {
                        edt_dob.setError(null);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }


                if ((!str_drining_lic.equals("") || !str_pan.equals("") || !str_aadhar.equals("") || !str_passport.equals("")
                        || !str_voter.equals("")) && ins == true) {


                    if (!str_pan.equals("") && ins == true) {
                        if (!Check_pattern(str_pan.toUpperCase()) && ins == true) {
                            edt_pan.setError("Enter valid PAN No.");

                            edt_pan.requestFocus();
                            ins = false;
                        } else {
                            if (str_pan.charAt(3) != 'P' && ins == true) {
                                edt_pan.setError("Fourth characher should be 'P'");

                                edt_pan.requestFocus();
                                ins = false;
                            } else {
                                edt_pan.setError(null);
                            }
                        }
                    }


                    if (!str_aadhar.equals("") && ins == true) {
                        if (Long.parseLong(str_aadhar) < Long.parseLong("12")) {
                            edt_aadhar.setError("Enter valid aadhar No");
                            edt_aadhar.requestFocus();
                            ins = false;
                        } else {
                            edt_aadhar.setError(null);
                        }
                    }


                } else {
                    if (ins == true) {

                        userFunctions.cutomToast("Enter atleast one ID Proof", getActivity());

                        ins = false;
                    }
                }

                if (ins == true && (app_type == null || app_type.equals("P"))) {

                    try {

                        if ((ins == true && str_asset_catg == null && !str_prod_catg.equals("PERSONAL"))) {

                            userFunctions.cutomToast("Select Asset Catagory", getActivity());
                            ins = false;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (ins == true && (app_type == null || app_type.equals("P"))) {
                    try {
                        if ((ins == true && str_asset_manu == null && !str_prod_catg.equals("PERSONAL"))) {

                            userFunctions.cutomToast("Select Asset Manufacturere", getActivity());
                            ins = false;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (ins == true && (app_type == null || app_type.equals("P"))) {
                    try {
                        if ((ins == true && str_asset_make == null && !str_prod_catg.equals("PERSONAL"))) {

                            userFunctions.cutomToast("Select Asset Make", getActivity());
                            ins = false;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (ins == true && (app_type == null || app_type.equals("P"))) {
                    try {
                        if ((ins == true && str_asset_model == null && !str_prod_catg.equals("PERSONAL"))) {

                            userFunctions.cutomToast("Select Asset Model", getActivity());
                            ins = false;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                if (ins == true && !str_prod_catg.equals("PERSONAL")) {
                    try {
                        if (str_asset_cost.equals("") && ins == true) {
                            edt_asset_cost.setError("Enter asset Cost");
                            edt_asset_cost.requestFocus();
                            ins = false;
                        } else {
                            BigDecimal margint;
                            margint = new BigDecimal(str_asset_cost);
                            if (margint.compareTo(new BigDecimal("100000000")) == 1 && ins == true) {
                                edt_asset_cost.setError("Enter asset Cost Less than 100000000");
                                edt_asset_cost.requestFocus();
                                ins = false;
                            } else {
                                edt_asset_cost.setError(null);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (ins == true && !str_prod_catg.equals("PERSONAL")) {
                    try {
                        if (str_margin_money.equals("") && ins == true) {
                            edt_margin.setError("Enter margin money");
                            edt_margin.requestFocus();
                            ins = false;
                        } else {
                            BigDecimal margint;
                            margint = new BigDecimal(str_margin_money);
                            if (margint.compareTo(new BigDecimal("100000000")) == 1 && ins == true) {
                                edt_margin.setError("Enter margin money Less than 100000000");
                                ins = false;

                            } else {
                                edt_margin.setError(null);
                            }

                            if (margint.compareTo(new BigDecimal(str_asset_cost)) == 1 && ins == true) {
                                edt_margin.setError("Enter Margin Money Less than or equal to Asset Cost");
                                ins = false;

                            } else {
                                edt_margin.setError(null);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    if (str_loan_amt.equals("") && ins == true) {
                        edt_loan_amt.setError("Enter Loan Amount");
                        edt_loan_amt.requestFocus();
                        ins = false;
                    } else {
                        BigDecimal loan_amt;

                        loan_amt = new BigDecimal(str_loan_amt);
                        if ((loan_amt.compareTo(new BigDecimal("100000000")) == 1 || loan_amt.compareTo(new BigDecimal("100000")) == -1) && ins == true) {
                            edt_loan_amt.setError(" Loan Amount should be between 100000 and 100000000");
                            ins = false;
                            edt_loan_amt.requestFocus();
                        } else {
                            edt_loan_amt.setError(null);
                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if ((str_tenure.equals("") || Integer.valueOf(str_tenure) == 0) && ins == true) {
                        edt_tenure.setError("Enter Tenure in months");
                        edt_tenure.requestFocus();
                        ins = false;
                    } else {
                        if (Integer.valueOf(str_tenure) > 250 && ins == true) {
                            edt_tenure.setError("Enter Tenure less than 250");
                            edt_tenure.requestFocus();
                            ins = false;

                        } else {

                            edt_tenure.setError(null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (ins == true) {


                    str_ts = getDateTime();

                    System.out.println("TS!!!::::" + str_ts);

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("ts", str_ts);
                    peditor.commit();

                    long update = 0;

                    if (app_type == null || app_type.equals("P")) {

                        update = dbHelp.insert_app_details(str_ts, str_fname, str_mname, str_lname, str_app_type, str_relation, str_product, str_catagory,
                                str_const, gender, str_dob, str_drining_lic, str_pan, str_passport, str_aadhar, str_voter, str_asset_catg, str_asset_make, str_asset_model, str_asset_cost,
                                str_margin_money, str_loan_amt, str_tenure, str_consent_to_call, str_asset_manu, str_age,str_father_name,str_mother_name);
                    } else {
                        update = dbHelp.insert_app_details(str_ts, str_fname, str_mname, str_lname, str_app_type, str_relation, cursor.getString(cursor.getColumnIndexOrThrow("product1")), cursor.getString(cursor.getColumnIndexOrThrow("catagory")),
                                str_const, gender, str_dob, str_drining_lic, str_pan, str_passport, str_aadhar, str_voter, cursor.getString(cursor.getColumnIndexOrThrow("asset_catg")), cursor.getString(cursor.getColumnIndexOrThrow("asset_make")), cursor.getString(cursor.getColumnIndexOrThrow("asset_model")), str_asset_cost,
                                str_margin_money, str_loan_amt, str_tenure, str_consent_to_call, cursor.getString(cursor.getColumnIndexOrThrow("aseet_man")), str_age,str_father_name,str_mother_name);
                    }
                    if (update > 0) {

                        Fragment vehicle = new FragAddrDetails();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.container, vehicle);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                    }
                }

            }
        });

        sex_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                System.out.println("checked id::" + checkedId);
                pos = sex_grp.indexOfChild(group.findViewById(checkedId));





                switch (pos) {

                    case 0:
                        System.out.println("checked id::" + checkedId);
                        sex = "2";
                        gender = "M";
                        System.out.println("Gender:::" + sex);
                        break;

                    case 1:
                        sex = "1";
                        gender = "F";
                        System.out.println("Gender:::" + sex);
                        break;

                }
            }
        });


        adp_prod = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_product);

        adp_prod.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_product.setAdapter(adp_prod);
        spn_product.setOnItemSelectedListener(this);


        adp_const = new MyAdapter( getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_cons);

        adp_const.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_const.setAdapter(adp_const);
        spn_const.setOnItemSelectedListener(this);


        adp_coapp_rel = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_coapp_rel);

        adp_coapp_rel.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_relation.setAdapter(adp_coapp_rel);
        spn_relation.setOnItemSelectedListener(this);


        adp_cust_actg = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_cust_cat);

        adp_cust_actg.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_catagory.setAdapter(adp_cust_actg);
        spn_catagory.setOnItemSelectedListener(this);


        adp_asset_catg = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_catg);

        adp_asset_catg.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_asset_catg.setAdapter(adp_asset_catg);
        spn_asset_catg.setOnItemSelectedListener(this);


        adp_asset_manu = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_manu);

        adp_asset_manu.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_asset_manu.setAdapter(adp_asset_manu);
        spn_asset_manu.setOnItemSelectedListener(this);


        adp_asset_model = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_model);

        adp_asset_model.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_asset_model.setAdapter(adp_asset_model);
        spn_asset_model.setOnItemSelectedListener(this);


        adp_asset_make = new MyAdapter(getActivity(),
                R.layout.qde_sp_display_layout, R.id.txt_visit, al_make_str);

        adp_asset_make.setDropDownViewResource(R.layout.qde_spinner_layout);
        spn_asset_make.setAdapter(adp_asset_make);
        spn_asset_make.setOnItemSelectedListener(this);


        edt_margin.addTextChangedListener(pincodeWatcher1);
        edt_asset_cost.addTextChangedListener(pincodeWatcher2);

        //edt_fname.addTextChangedListener(CapsText);

//        CapsText.afterTextChanged((Editable)edt_fname);


        return view;
    }

    public void getdata() {

        str_pan = edt_pan.getText().toString();
        str_fname = edt_fname.getText().toString();

        str_mname = edt_mname.getText().toString();
        str_lname = edt_lname.getText().toString();
        str_dob = edt_dob.getText().toString();
        str_age = txt_date.getText().toString();


        str_drining_lic = edt_drining_lic.getText().toString();
        str_drining_lic = str_drining_lic.toUpperCase();
        str_pan = edt_pan.getText().toString();
        str_aadhar = edt_aadhar.getText().toString();

        str_asset_cost = edt_asset_cost.getText().toString();
        str_margin_money = edt_margin.getText().toString();
        str_loan_amt = edt_loan_amt.getText().toString();
        str_tenure = edt_tenure.getText().toString();

        str_passport = edt_passport.getText().toString();
        str_passport = str_passport.toUpperCase();


        str_voter = edt_voter.getText().toString();
        str_voter = str_voter.toUpperCase();
        str_father_name = edt_father_name.getText().toString();
        str_mother_name = edt_mother_name.getText().toString();

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {

            case R.id.spn_app_tpe:

                if (position == 0) {
                    str_app_type = null;

                } else {

                    str_app_type = app_type_val.get(position);

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("app_type", str_app_type);
                    peditor.commit();

                }
                break;
            case R.id.spn_catagory:
                if (position == 0) {
                    str_catagory = null;

                } else {

                    str_catagory = al_cust_catg_val.get(position).toString();
                }

                break;
            case R.id.spn_const:
                if (position == 0) {
                    str_const = null;
                } else {
                    str_const = al_cons_val.get(position).toString();
                }
                break;

            case R.id.spn_product:

                if (position == 0) {
                    str_product = null;

                } else {

                    al_make_str.clear();
                    al_make_str.add("Select Make");
                    al_make_val.clear();
                    al_make_val.add("");

                    adp_asset_make = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_make_str);

                    adp_asset_make.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_make.setAdapter(adp_asset_make);
                    spn_asset_make.setOnItemSelectedListener(this);

                    al_model.clear();
                    al_model_val.clear();

                    al_model.add("Select Model");
                    al_model_val.add("");

                    adp_asset_model = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_model);

                    adp_asset_model.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_model.setAdapter(adp_asset_model);
                    spn_asset_model.setOnItemSelectedListener(this);


                    spn_asset_make.setSelection(0);
                    System.out.println("POSITION PROD:" + position);

                    str_product = al_product_val.get(position).toString();

                    String catg_id, catg;

                    al_catg.clear();
                    al_catg_val.clear();


                    al_catg.add("Select Asset Catagory");
                    al_catg_val.add("");
                    Cursor cursor_catg = dbHelp.get_catagory(str_product);
                    System.out.println("cursor_catg::::::" + cursor_catg);

                    spn_asset_model.setSelection(0);
                    try {
                        if (cursor_catg != null) {

                            while (cursor_catg.moveToNext()) {
                                catg_id = cursor_catg.getString(0);
                                catg = cursor_catg.getString(1);

                                al_catg.add(catg);
                                al_catg_val.add(catg_id);
                                //  arr_pd_val.add(pd_val);
                            }

                            cursor_catg.close();

                        }

                        System.out.println("ARR CATG:" + al_catg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    adp_asset_catg = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_catg);

                    adp_asset_catg.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_catg.setAdapter(adp_asset_catg);
                    spn_asset_catg.setOnItemSelectedListener(this);

                    al_cust_cat.clear();
                    al_cust_catg_val.clear();

                    al_cust_cat.add("Select Customer Catagory");

                    al_cust_catg_val.add("");


                    Cursor cursor_cust_catg = dbHelp.get_cust_catg(str_product);
                    System.out.println("cursor_catg::::::" + cursor_cust_catg);
                    try {
                        if (cursor_cust_catg != null) {

                            while (cursor_cust_catg.moveToNext()) {
                                catg_id = cursor_cust_catg.getString(0);
                                catg = cursor_cust_catg.getString(1);

                                al_cust_cat.add(catg_id);
                                al_cust_catg_val.add(catg);
                                //  arr_pd_val.add(pd_val);
                            }

                            cursor_cust_catg.close();

                        }

                        System.out.println("ARR CATG:" + al_catg);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    adp_cust_actg = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_cust_cat);

                    adp_cust_actg.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_catagory.setAdapter(adp_cust_actg);
                    spn_catagory.setOnItemSelectedListener(this);


                    String man_id, manu;

                    al_manu.clear();
                    al_manu.add("Select Manufacturer");

                    al_manu_val.clear();
                    al_manu_val.add("");


                    Cursor cursor_manu = dbHelp.get_manufacturer(str_product);
                    System.out.println("cursor_catg::::::" + cursor_manu);
                    try {
                        if (cursor_manu != null) {

                            while (cursor_manu.moveToNext()) {
                                man_id = cursor_manu.getString(0);
                                manu = cursor_manu.getString(1);

                                al_manu.add(manu);
                                al_manu_val.add(man_id);
                                //  arr_pd_val.add(pd_val);
                            }

                            cursor_manu.close();

                        }

                        System.out.println("ARR CATG:" + al_manu);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    adp_asset_manu = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_manu);

                    adp_asset_manu.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_manu.setAdapter(adp_asset_manu);
                    spn_asset_manu.setOnItemSelectedListener(this);

                    str_prod_catg = al_prod_catg.get(position).toString();

                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("prod_catg", str_prod_catg);
                    peditor.commit();

                    System.out.println("PROD CATG:" + str_prod_catg);


                    if (str_prod_catg.equals("PERSONAL")) {
                        spn_asset_catg.setSelection(0);
                        spn_asset_manu.setSelection(0);
                        spn_asset_make.setSelection(0);
                        spn_asset_model.setSelection(0);
                        edt_asset_cost.setText("");
                        edt_margin.setText("");

                        str_asset_catg = "";
                        str_asset_manu = "";
                        str_asset_make = "";
                        str_asset_model = "";


                        lay_asset_catgt.setVisibility(View.GONE);
                        lay_asset_man.setVisibility(View.GONE);
                        lay_asset_make.setVisibility(View.GONE);
                        lay_model.setVisibility(View.GONE);
                        edt_asset_cost.setVisibility(View.GONE);
                        edt_margin.setVisibility(View.GONE);
                        edt_loan_amt.setEnabled(true);
                    } else {
                        lay_asset_catgt.setVisibility(View.VISIBLE);
                        lay_asset_man.setVisibility(View.VISIBLE);
                        lay_asset_make.setVisibility(View.VISIBLE);
                        lay_model.setVisibility(View.VISIBLE);
                        edt_asset_cost.setVisibility(View.VISIBLE);
                        edt_margin.setVisibility(View.VISIBLE);
                        edt_loan_amt.setEnabled(false);

                        str_asset_catg = null;
                        str_asset_manu = null;
                        str_asset_make = null;
                        str_asset_model = null;
                    }

                }
                break;

            case R.id.spn_relation:

                if (position == 0) {
                    str_relation = null;
                } else {
                    str_relation = al_coapp_rel_val.get(position).toString();

                }

                break;

            case R.id.spn_asset_catg:

                if (position == 0) {
                    str_asset_catg = null;
                } else {

                    str_asset_catg = al_catg_val.get(position);
                }

                break;

            case R.id.spn_asset_manu:

                if (position == 0) {
                    str_asset_manu = null;
                } else {
                    str_asset_manu = al_manu_val.get(position).toString();

                    System.out.println("AtCAT:::" + str_asset_manu);

                    al_make.clear();
                    al_make_val.clear();
                    al_make_str.clear();

                    al_make.add("Select make");
                    al_make_val.add("");
                    al_make_str.add("Select make");

                    String make_id, make, make_str;
                    Cursor cursor_make = dbHelp.get_make(str_product, str_asset_manu);
                    System.out.println("cursor_catg::::::" + cursor_make);
                    try {
                        if (cursor_make != null) {

                            while (cursor_make.moveToNext()) {
                                make_id = cursor_make.getString(0);
                                make = cursor_make.getString(1);
                                make_str = cursor_make.getString(2);

                                al_make.add(make);
                                al_make_val.add(make_id);
                                al_make_str.add(make_str);

                            }

                            cursor_make.close();

                        }

                        System.out.println("ARR CATG:" + al_make);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    adp_asset_make = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_make_str);

                    adp_asset_make.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_make.setAdapter(adp_asset_make);
                    spn_asset_make.setOnItemSelectedListener(this);


                }
                break;

            case R.id.spn_asset_model:

                if (position == 0) {
                    str_asset_model = null;
                } else {
                    str_asset_model = al_model_val.get(position).toString();


                }
                break;

            case R.id.spn_asset_make:

                if (position == 0) {
                    str_asset_make = null;
                } else {


                    str_asset_make = al_make_val.get(position).toString();


                    String Asset_make = al_make_str.get(position).toString();

                    String model_id, model;

                    al_model.clear();
                    al_model_val.clear();

                    al_model_val.add("");
                    al_model.add("Select model");


                    Cursor cursor_model = dbHelp.get_models(Asset_make, str_product, str_asset_manu);
                    System.out.println("cursor_catg::::::" + cursor_model);
                    try {
                        if (cursor_model != null) {

                            while (cursor_model.moveToNext()) {
                                model_id = cursor_model.getString(0);
                                model = cursor_model.getString(1);

                                al_model.add(model);
                                al_model_val.add(model_id);
                                //  arr_pd_val.add(pd_val);
                            }

                            cursor_model.close();

                        }

                        System.out.println("model::" + al_model + ":::model id::" + al_model_val);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    adp_asset_model = new MyAdapter(getActivity(),
                            R.layout.qde_sp_display_layout, R.id.txt_visit, al_model);

                    adp_asset_model.setDropDownViewResource(R.layout.qde_spinner_layout);
                    spn_asset_model.setAdapter(adp_asset_model);
                    spn_asset_model.setOnItemSelectedListener(this);

                }
                break;

        }

    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        Random rand =new Random();
        int new_rand=rand.nextInt(90000)+10000;
        String str_new_rand=String.valueOf(new_rand);
        return str_new_rand+format;
    }


    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    private void showDatePicker() {
        SelectDateFragment date = new SelectDateFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            Calendar c = Calendar.getInstance();

            String formattedDate1 = new String();

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            //   String formattedDate = df.parse(curr_date);

            String curr_date1 = "01-03-2017";

          /*  SimpleDateFormat df1 = new SimpleDateFormat("yyyy");
            try {
                curr_date1 = df.format(curr_date);
            }
            catch (Exception e) {
                e.printStackTrace();
            }*/

            //  String[] parts=curr_date.split("-");
            // String nsd=parts[2];

            // System.out.println("NSD:::" + nsd);

            System.out.println("YEAR:::::" + year + "month::::" + (monthOfYear + 1) + "DAY::::" + dayOfMonth);

            // Integer age=  (Integer.parseInt(nsd))-year;

            if (userFunctions.dateCompare(dayOfMonth, monthOfYear + 1, year, curr_date1).equals("")) {
                edt_dob.setText("");
                txt_date.setText("");
                //  edt_dob.setError("Minimum Age should be 18 ");
                //  edt_dob.requestFocus();

                userFunctions.cutomToast("Minimumm Age should be 18", getActivity());
            } else {

                String age = userFunctions.dateCompare(dayOfMonth, monthOfYear + 1, year, curr_date1);
                txt_date.setText(String.valueOf(age));

                String s = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                Date df1 = null;
                String df2 = null;

                SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                try {

                    df1 = formater.parse(s);
                    df2 = formater.format(df1);

                } catch (ParseException e) {
                    e.printStackTrace();
                }


                edt_dob.setText(df2 + "+05:30");

            }

        }
    };

    public void setEditMaxLenght(int len, EditText edt_sample) {
        InputFilter[] filter_arr = new InputFilter[1];
        filter_arr[0] = new InputFilter.LengthFilter(len);
        edt_sample.setFilters(filter_arr);
    }


    private boolean Check_pattern(String string) {

        Boolean status = false;

        status = PAN_PATTERN.matcher(string).matches();

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }


   /* public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edt_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
            }
        });
    }*/


    private final TextWatcher pincodeWatcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        @Override
        public void afterTextChanged(Editable editable) {


            BigDecimal minus_amt, asset_cost, loan_amt;
            if (!edt_margin.getText().toString().equals("")) {

                if (!edt_asset_cost.getText().toString().equals("")) {
                    //   if( is_self_emp.equals(true)) {
                    minus_amt = new BigDecimal(edt_margin.getText().toString());

                    asset_cost = new BigDecimal(edt_asset_cost.getText().toString());


                    loan_amt = asset_cost.subtract(minus_amt);
                    edt_loan_amt.setText(loan_amt.toString());
                } else {
                    minus_amt = new BigDecimal(edt_margin.getText().toString());

                    asset_cost = new BigDecimal("0");


                    loan_amt = asset_cost.subtract(minus_amt);
                    edt_loan_amt.setText(loan_amt.toString());
                }
                //   }
                //  has_focus();
            }

            System.out.println("TEXTCHNGED");

        }


    };

    private final TextWatcher pincodeWatcher2 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {


            BigDecimal minus_amt, asset_cost, loan_amt;
            if (!edt_asset_cost.getText().toString().equals("")) {

                if (!edt_margin.getText().toString().equals("")) {
                    //   if( is_self_emp.equals(true)) {
                    minus_amt = new BigDecimal(edt_margin.getText().toString());

                    asset_cost = new BigDecimal(edt_asset_cost.getText().toString());


                    loan_amt = asset_cost.subtract(minus_amt);
                    edt_loan_amt.setText(loan_amt.toString());
                } else {
                    asset_cost = new BigDecimal(edt_asset_cost.getText().toString());

                    minus_amt = new BigDecimal("0");


                    loan_amt = asset_cost.subtract(minus_amt);
                    edt_loan_amt.setText(loan_amt.toString());
                }
                //   }
                //  has_focus();
            }

            System.out.println("TEXTCHNGED");

        }


    };


    private final TextWatcher CapsText = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        @Override
        public void afterTextChanged(Editable editable) {


            if (edt_fname.getText().hashCode() == editable.hashCode()) {
                edt_mname.setText(edt_fname.getText().toString().toUpperCase());
            }

            if (edt_mname.getText().hashCode() == editable.hashCode()) {
                edt_mname.setText(edt_mname.getText().toString());
            }

            if (edt_lname.getText().hashCode() == editable.hashCode()) {
                edt_lname.setText(edt_lname.getText().toString());
            }


        }


    };

    public void callParentMehod() {

        getActivity().onBackPressed();
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getContext(),
                HdbHome.class);
        startActivity(home_activity);
        getActivity().finish();
    }

}
