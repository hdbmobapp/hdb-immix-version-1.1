package com.example.hdb_los_prod;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.OffEntriesAdapter;
import com.example.hdb_los_prod.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Vijay on 2/7/2017.
 */
public class Offine_Entries extends Activity {

    ProgressDialog mProgressDialog;

    JSONArray jsonarray, jsonarray1, jsonarray2, jsonarray3;


    public static String name = "name";
    public static String app_type = "app_type";
    public static String relation = "relation";
    public static String product = "product";
    public static String catagory = "catagory";
    public static String pincode = "pincode";
    public static String city = "city";
    public static String state = "state";
    public static String TS = "ts";
    public static String off_pincode = "off_pincode";
    public static String cust_id = "cust_id";


    ListView mList;
    OffEntriesAdapter ms_adapter;
    UserFunction userFunction;
    JSONObject jsonobject = null;
    String res, resp_success;
    public static String KEY_STATUS_VM = "status";
    public static String KEY_SUCCESS_VM = "success";

    ArrayList<HashMap<String, String>> arraylist;
    TextView txt_error;


    public static Button btn_next;
    Cursor off_entry;
    DBHelper DBhelp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.print("MASTR LOAD REFRESG:::");

        setContentView(R.layout.qde_off_entries);

        DBhelp=new DBHelper(getApplicationContext());

        txt_error = (TextView) findViewById(R.id.text_error_msg);
        btn_next = (Button) findViewById(R.id.btn_next);

        mList = (ListView) findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();

        userFunction = new UserFunction(this);

        off_entry=DBhelp.get_offine_entry();
       // off_entry=null;

        System.out.println("CURSOR:::" + off_entry);

        if(off_entry!=null) {

            if (off_entry.moveToFirst()) {
                txt_error.setVisibility(View.GONE);
                do {

                    HashMap<String, String> map = new HashMap<>();

                    System.out.println("rel:::" + off_entry.getString(off_entry.getColumnIndexOrThrow("app_type"))
                            + "::::prod::::" + product + "::::catag::::" + off_entry.getString(off_entry.getColumnIndexOrThrow("catagory"))
                            + ":::TS:::" + off_entry.getString(off_entry.getColumnIndexOrThrow("app_id")) + "::::CUST ID::::" + off_entry.getString(off_entry.getColumnIndexOrThrow("cust_id")));
                    map.put("name", off_entry.getString(off_entry.getColumnIndexOrThrow("fname")) + " " +
                            off_entry.getString(off_entry.getColumnIndexOrThrow("mname")) + " " + off_entry.getString(off_entry.getColumnIndexOrThrow("lname")));

                    map.put("app_type", off_entry.getString(off_entry.getColumnIndexOrThrow("app_type")));


                    map.put("relation", off_entry.getString(off_entry.getColumnIndexOrThrow("app_relation")));
                    map.put("product", off_entry.getString(off_entry.getColumnIndexOrThrow("product1")));
                    map.put("catagory", off_entry.getString(off_entry.getColumnIndexOrThrow("catagory")));

                    map.put("pincode", off_entry.getString(off_entry.getColumnIndexOrThrow("pincode")));
                    map.put("city", off_entry.getString(off_entry.getColumnIndexOrThrow("city")));
                    map.put("state", off_entry.getString(off_entry.getColumnIndexOrThrow("state")));
                    map.put("ts", off_entry.getString(off_entry.getColumnIndexOrThrow("ts")));
                    map.put("off_pincode", off_entry.getString(off_entry.getColumnIndexOrThrow("off_pincode")));
                    map.put("cust_id", off_entry.getString(off_entry.getColumnIndexOrThrow("cust_id")));

                    // Set the JSON Objects into the array
                    arraylist.add(map);


                } while (off_entry.moveToNext());


            }else {
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Nothing to Verify !");
            }

            ms_adapter = new OffEntriesAdapter(getApplicationContext(), arraylist, 1);

            ms_adapter.notifyDataSetChanged();

            mList.setAdapter(ms_adapter);
        }

    }

public void go_home(View v){
    Intent messagingActivity = new Intent(getApplicationContext(),
            HdbHome.class);
    startActivity(messagingActivity);

}

    @Override
    public void onBackPressed() {

    }


}

