
package com.example.hdb_los_prod;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.LoadMoreListView;
import com.example.hdb_los_prod.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;

public class MyUploads_Coapp extends Activity {
    // Within which the entire activity is enclosed

    // ListView represents Navigation Drawer
    ListView mList;
    public int pgno = 0;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;



    // Title of the action bar
    String mTitle = "";
    static LinearLayout drawerll;
    static String str_region;
    SharedPreferences pref;

    String str_app_id;
    String str_user_name;
    String str_losid_no;
    String str_app_form_no;

    Adapter_Coapp adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunction userFunction;

    public  static String app_id = "app_id1";
    public static String cust_name = "cust_name1";

    public  static String final_losid = "final_losid1";
    public  static String app_type = "app_type1";

    public  static String srno = "srno";

    public  static String ts = "ts";
    public  static String img_upload_count = "img_upload_count";
    public  static String tot_img_cnt = "tot_img_cnt";
    public  static String err_code = "err_code";


    UserFunction userfunction;
    TextView txt_error;
    EditText edt_losid;
    Button btn_losid;
    View rootView;
    ProgressBar progressbar_loading;

    Button btn_home;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qde_myuploads_coapp);
        userFunction = new UserFunction(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        Intent i=getIntent();

        str_app_id =  i.getStringExtra("app_id_uploads");

        System.out.println("user_id:::" + str_app_id);

        setlayout();


        cd = new ConnectionDetector(getApplicationContext());

        if (cd.isConnectingToInternet()) {

            new DownloadJSON().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }


        btn_home=(Button)findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent messagingActivity = new Intent(getApplicationContext(),
                        HdbHome.class);
                startActivity(messagingActivity);

            }
        });

        // Setting the adapter on mDrawerList
        // mDrawerList.setAdapter(adapter);


    }


    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        arraylist = new ArrayList<HashMap<String, String>>();

        mList = (ListView) findViewById(R.id.list);




        progressbar_loading=(ProgressBar)findViewById(R.id.progressbar_loading);

    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            progressbar_loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;

            jsonobject = userFunction.get_CaseList_coapp(str_app_id,"0");
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");
//                    txt_error.setVisibility(View.GONE);
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("app_id1", jsonobject.getString("app_id"));
                    map.put("cust_name1", jsonobject.getString("cust_name"));

                    map.put("final_losid1", jsonobject.getString("final_losid"));
                    map.put("app_type1", jsonobject.getString("app_type"));

                    map.put("srno", jsonobject.getString("srno"));
                    map.put("ts", jsonobject.getString("ts"));

                    map.put("err_code", jsonobject.getString("err_code"));
                    map.put("tot_img_cnt", jsonobject.getString("tot_img_cnt"));
                    map.put("img_upload_count", jsonobject.getString("img_uplod_count"));

                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                adapter = new Adapter_Coapp(getApplicationContext(), arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

                System.out.println("INSIDE::: post excdf:");
                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });

            }
            progressbar_loading.setVisibility(View.GONE);

        }

    }



    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }


    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //getActivity().getApplicationContext().viewPager.setCurrentItem(2, true);

            jsonobject = userFunction.get_CaseList_coapp(str_app_id,tmpgno);

            // Create an array
            if (jsonobject != null) try {
                jsonarray = jsonobject.getJSONArray("Data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("app_id", jsonobject.getString("app_id1"));
                    map.put("cust_name", jsonobject.getString("cust_name1"));

                    map.put("final_losid", jsonobject.getString("final_losid1"));
                    map.put("app_type", jsonobject.getString("app_type1"));
                    map.put("ts", jsonobject.getString("ts"));

                    map.put("err_code", jsonobject.getString("err_code"));

                    map.put("tot_img_cnt", jsonobject.getString("tot_img_cnt"));
                    map.put("img_upload_count", jsonobject.getString("img_uplod_count"));

                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }

}
