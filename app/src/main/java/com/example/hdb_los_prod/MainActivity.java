package com.example.hdb_los_prod;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.hdb.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qde_activity_main);

    }


    @Override
    public void onBackPressed() {
        // your code.
        // to stop anonymous runnable use handler.removeCallbacksAndMessages(null);


    }
    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

}
