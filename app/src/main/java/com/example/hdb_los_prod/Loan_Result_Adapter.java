package com.example.hdb_los_prod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DELL on 12/14/2017.
 */

public class Loan_Result_Adapter extends BaseAdapter {


    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    ArrayAdapter<String> adapter;
    ImageLoader img_loader;

    public Loan_Result_Adapter(Context context,
                               ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        TableRow row_remraks;

        img_loader = ImageLoader.getInstance();
        img_loader.init(ImageLoaderConfiguration.createDefault(context));
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }
    // Declare Variables

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);


        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.qde_loan_result_adapter, parent,
                    false);


            holder = new ViewHolder();
            holder.tv_image_name = (TextView) itemView.findViewById(R.id.tv_image_name);
            holder.tv_img_count = (TextView) itemView.findViewById(R.id.tv_img_count);
            holder.img_img_pc = (ImageView) itemView.findViewById(R.id.img_img_pc);
            holder.tv_img_upld_count = (TextView) itemView.findViewById(R.id.tv_img_upld_count);


            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }


        /*

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }
*/


        //System.out.println("PR::::" + resultp.get(MyUploads.product) + ":::err code:::" + resultp.get(MyUploads.error_code) + ":::err desc:::" + resultp.get(MyUploads.error_desc));
        // holder.img_img_pc.setImageBitmap(decodedByte);
        // holder.img_img_pc.setText(resultp.get(Loan_upload_search.app_id));

        holder.tv_image_name.setText(resultp.get(Loan_Result.img_name));
        if(Loan_Result.img_count!=null ||!Loan_Result.img_count.equals("")){
            holder.tv_img_count.setText(resultp.get(Loan_Result.img_count));


        }else {
            holder.tv_img_count.setText("0");

        }

        if(Loan_Result.dms_count!=null ||!Loan_Result.dms_count.equals("")){
            holder.tv_img_upld_count.setText(resultp.get(Loan_Result.dms_count));

        }else{

            holder.tv_img_upld_count.setText("0");

        }



        if (Integer.parseInt(resultp.get(Loan_Result.img_count).toString())>0) {

            holder.img_img_pc.setImageDrawable(context.getDrawable(R.drawable.uploaded_succ));

        }else{
            holder.img_img_pc.setImageDrawable(context.getDrawable(R.drawable.pending));


        }


        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_image_name;
        public ImageView img_img_pc;
        public TextView tv_img_count;
        public TextView tv_img_upld_count;


    }


}

