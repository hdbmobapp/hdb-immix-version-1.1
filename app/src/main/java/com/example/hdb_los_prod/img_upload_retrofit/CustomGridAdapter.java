package com.example.hdb_los_prod.img_upload_retrofit;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.hdb.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;

public class CustomGridAdapter extends BaseAdapter {
    private Context mContext;
    private final String[] web;
    private final String[] myarray_img;
    ImageLoader imageLoader;
    public DisplayImageOptions img_options;


    LayoutInflater inflater;

    public CustomGridAdapter(Context c, String[] web ,String[] myarray_img) {
        mContext = c;
        this.web = web;
        this.myarray_img = myarray_img;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        img_options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.sad)
                .showImageForEmptyUri(R.drawable.sad)
                .showImageOnFail(R.drawable.sad).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        imageLoader.clearDiskCache();
        imageLoader.clearMemoryCache();

        CustomGridAdapter.ViewHolder holder = null;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.qde_grid_single, parent,
                    false);

            holder = new ViewHolder();
            holder.textView = (TextView) itemView.findViewById(R.id.grid_text);
            holder.imageView = (ImageView) itemView.findViewById(R.id.grid_image);
            holder.textView.setText(web[position]);

            String imageUri_ll = "file:///" + myarray_img[position]; // from SD card

            File file =new File(myarray_img[position]);

            if(file.exists()) {
                imageLoader.displayImage(imageUri_ll, holder.imageView);
            }

            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (CustomGridAdapter.ViewHolder) itemView.getTag();
        }
        return itemView;
    }

    /*
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.qde_layout.qde_grid_single, null);
            TextView textView = (TextView) grid.findViewById(R.id.grid_text);
            ImageView imageView = (ImageView) grid.findViewById(R.id.grid_image);
            textView.setText(web[position]);
           // imageView.setImageResource(Imageid[position]);
        } else {
            grid = (View) convertView;
        }

        return grid;
    }
*/

    private static class ViewHolder {
        public TextView textView;
        public ImageView imageView;
        public TextView tv_img_count;


    }
}