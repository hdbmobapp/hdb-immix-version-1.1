package com.example.hdb_los_prod.img_upload_retrofit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.HdbHome;
import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;


/**
 * Created by Administrator on 20-01-2017.
 */
public class ImagesList extends AppCompatActivity {
    DBHelper dbHelper;
    Integer total_pic_count;
    static String str_ts;
    Button btn_main_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qde_imageslist);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent i = getIntent();
        str_ts = i.getStringExtra("ts");
        System.out.println("str_ts:::str_ts::::" + str_ts);
        setlistview();
    }

    public void setlistview() {
        btn_main_list = (Button) findViewById(R.id.btn_main_list);
        dbHelper = new DBHelper(this);
        TextView txt_upload = (TextView) findViewById(R.id.txt_upload);
        final Cursor cursor = dbHelper.getImagests(str_ts);
        total_pic_count = cursor.getCount();
        if (total_pic_count > 0) {
            txt_upload.setVisibility(View.GONE);
        } else {
            txt_upload.setVisibility(View.VISIBLE);
        }
        ListView lvItems = (ListView) findViewById(R.id.list);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageAdapter todoAdapter = new ImageAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        todoAdapter.notifyDataSetChanged();
        lvItems.refreshDrawableState();

        btn_main_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(ImagesList.this, HdbHome.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);

    }

    public void go_home(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

}
