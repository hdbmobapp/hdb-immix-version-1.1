package com.example.hdb_los_prod.img_upload_retrofit;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;

import com.example.hdb_los_prod.libraries.DBHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageAdapter extends CursorAdapter {
    ImageLoader imageLoader;
    public DisplayImageOptions img_options;
    String priority;

    String img_path;
    SharedPreferences pref;
    DBHelper edb;
    ProgressDialog mProgressDialog;
    Context cnt;
    boolean is_upload = false;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static final String SERVER_PATH = "https://hdbapp.hdbfs.com/";
    private Uri uri;
    int int_img_size = 0;
    File file;

    // IMAGE TABLE
    public ImageAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        img_options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.sad)
                .showImageForEmptyUri(R.drawable.sad)
                .showImageOnFail(R.drawable.sad).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        edb = new DBHelper(context);
        cnt = context;
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.qde_image_listview_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        //imageLoader.destroy();
        imageLoader.clearDiskCache();
        imageLoader.clearMemoryCache();

        pref = context.getSharedPreferences("MyPref", 0); // 0 -

        int_img_size = pref.getInt("img_size", 0);
        System.out.println("int_img_size:::" + int_img_size);
        String str_losid = null;
        String str_form_no = null;
        // Find fields to populate in inflated template
        TextView tvlosid = (TextView) view.findViewById(R.id.tvlosid);
        TextView tvform_no = (TextView) view.findViewById(R.id.tv_app_form);
        TextView tvlabel = (TextView) view.findViewById(R.id.tv_label);
        final ImageView img_upload = (ImageView) view.findViewById(R.id.img_upload_icon);
        final TextView tv_size = (TextView) view.findViewById(R.id.tv_images_size);
        ImageView img_image = (ImageView) view.findViewById(R.id.flag);
        final Button btn_upload = (Button) view.findViewById(R.id.btn_upload);
        Button btn_resize = (Button) view.findViewById(R.id.btn_resize);
        // Extract properties from cursor
        String body = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
        //  int priority = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        priority = cursor.getString(6);
        str_losid = cursor.getString(1);
        str_form_no = cursor.getString(2);
        //  String priority=pref.getString("img_name",null);
        img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));


        System.out.println("img_path::::" + img_path);
        long length = 0;
        try {
            file = new File(img_path);
            length = file.length();
            length = length / 1024;
        } catch (Exception e) {

        }

        if (length == 0) {
            //  view.setVisibility(View.GONE);
            System.out.println("rrrrrrrrrrrrrrrrrrr" + length);
            //  Toast.makeText(cnt, "Image not found in your internal storage ", Toast.LENGTH_LONG).show();
            edb.deletepic(img_path);
            Intent i = new Intent(cnt, ImagesList.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            cnt.startActivity(i);

        }


        String str_long = Long.toString(length);
        // Populate fields with extracted properties
        tvlosid.setText(str_losid);
        tvform_no.setText(str_form_no);
        tvlabel.setText(priority);
        tv_size.setText(str_long + " kb");
/*
        if (length > int_img_size) {
            btn_resize.setVisibility(View.VISIBLE);
            btn_upload.setVisibility(View.INVISIBLE);
            img_upload.setVisibility(View.GONE);
        } else {

            // new UploadPhoto().execute(img_path);
            final File file_nw = new File(img_path);
            if (file_nw.exists()) {
                final ProgressDialog progressDialog;
                progressDialog = new ProgressDialog(cnt);
                progressDialog.setMessage("Uploading Images");
                progressDialog.show();


                Call<UploadObject> fileUpload;

                Log.d(TAG, "Filename " + file_nw.getName());
                //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file_nw);
                RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file_nw);
                MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file_nw.getName(), mFile);
                RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file_nw.getName());
                RequestBody losid = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_losid);
                RequestBody ts = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_ts);
                RequestBody emp_id = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_emp_id);
                // RequestBody img_no = RequestBody.create(MediaType.parse("text/plain"), str_form_no);
                // RequestBody app_type = RequestBody.create(MediaType.parse("text/plain"), Image_List.app_type);


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(SERVER_PATH)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);

                System.out.println("fileToUpload::::::" + fileToUpload);
                System.out.println("fileToUpload::::filename:::::" + filename.toString());

                fileUpload = uploadImage.uploadFile(fileToUpload, filename, losid, ts, emp_id);
                fileUpload.enqueue(new Callback<UploadObject>() {
                    @Override
                    public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                         progressDialog.dismiss();

                        if (response.body().getStatus() == 200) {

                            if (edb.deletepic(file_nw.getPath()) == true) {
                                Toast.makeText(cnt, "Success mkmkmkm" + response.body().getSuccess(), Toast.LENGTH_LONG).show();
                                btn_upload.setVisibility(View.GONE);
                                img_upload.setVisibility(View.VISIBLE);
                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<UploadObject> call, Throwable t) {
                        Log.d(TAG, "Error " + t.getMessage());
                        progressDialog.dismiss();

                    }
                });
            }


            btn_resize.setVisibility(View.GONE);
            btn_upload.setVisibility(View.VISIBLE);

        }
*/
        //  imageLoader.displayImage(imageUri, img_image);
        String imageUri_ll = "file:///" + img_path; // from SD card

        imageLoader.displayImage(imageUri_ll, img_image);

//        imageLoader.displayImage(img_path,
//                img_image, img_options);
        btn_upload.setTag(cursor.getString(5));
        btn_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                new UploadPhoto().execute(arg0.getTag().toString());
            }
        });

      /*  btn_resize.setTag(cursor.getString(5));
        btn_resize.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                new ResizePhoto().execute(arg0.getTag().toString());
            }
        });*/

        view.setTag(cursor.getString(cursor.getColumnIndexOrThrow("img_path")));
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                File nw_file = new File(arg0.getTag().toString());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(nw_file), "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                cnt.startActivity(intent);


            }
        });

    }


    private class UploadPhoto extends AsyncTask<String, Integer, String> {
        private String str_path;
        Call<UploadObject> fileUpload;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(cnt);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            // mProgressDialog.setProgress(0);

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            mProgressDialog.setProgress(progress[0]);
        }


        @Override
        protected String doInBackground(String... params) {

            try {
                str_path = params[0];
                //edb.upload_image(str_path);

                System.out.println("filePath:::::" + str_path);
                File file = new File(str_path);
                if (file.exists()) {
                    Log.d(TAG, "Filename " + file.getName());
                    //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
                    RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
                    RequestBody losid = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_losid);
                    RequestBody ts = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_ts);
                    RequestBody emp_id = RequestBody.create(MediaType.parse("text/plain"), Image_List.str_emp_id);
                    RequestBody img_no = RequestBody.create(MediaType.parse("text/plain"), "");
                    RequestBody app_type = RequestBody.create(MediaType.parse("text/plain"), Image_List.app_type);

                   // Gson gson=new GsonBuilder().setLenient().create();
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(SERVER_PATH)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);

                    System.out.println("fileToUpload::::::" + fileToUpload);
                    System.out.println("fileToUpload::::filename:::::" + filename);

                    fileUpload = uploadImage.uploadFile(fileToUpload);

                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {
            fileUpload.enqueue(new Callback<UploadObject>() {
                @Override
                public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                    System.out.println("::::status::::" + response.body().getStatus() + ":::str_path:::" + str_path);


                    if (response.body().getStatus() == 200) {
                        if (edb.deletepic(str_path) == true) {
                            edb.deletepic(str_path);
                            System.out.println(":::str_path:::" + str_path);
                            Toast.makeText(cnt, "Response " + response.raw().message(), Toast.LENGTH_LONG).show();
                            // Toast.makeText(cnt, "Success " + response.body().getSuccess(), Toast.LENGTH_LONG).show();

                            Intent i = new Intent(cnt,
                                    ImagesList.class);
                            i.putExtra("ts", ImagesList.str_ts);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            cnt.startActivity(i);

                        }
                    }

                    is_upload = true;
                   /*
                    Intent i = new Intent(cnt,
                            ImagesList.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    cnt.startActivity(i);
                    */
                    //mProgressDialog.dismiss();
                    mProgressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<UploadObject> call, Throwable t) {
                    Log.d(TAG, "Error " + t.getMessage());
                    mProgressDialog.dismiss();


                }
            });

        }

    }

    private class ResizePhoto extends AsyncTask<String, String, String> {
        private String str_path;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(cnt);
            mProgressDialog.setMessage("Compressing ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // Get the file path from the intent object


            str_path = params[0];
            try {
                compressImage(str_path);

            } catch (Exception e) {
                e.printStackTrace();
            }

        /*
                    boolean success=false;
                //Bitmap bmp;
                str_path = params[0];
                // edb.upload_image(str_path);
                FileOutputStream out = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bmp = BitmapFactory.decodeFile(str_path, options);
               // bmp = ImageUtils.getInstant().getCompressedBitmap_new(str_path, 800, 600);

                try {
                    out = new FileOutputStream(str_path);
                    bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            } catch (Exception e1) {
                e1.printStackTrace();
            }
*/

            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {
            Intent i = new Intent(cnt,
                    ImagesList.class);
            i.putExtra("ts", ImagesList.str_ts);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            cnt.startActivity(i);
            mProgressDialog.dismiss();

        }

    }

    public String compressImage(String imageUri) {

        String filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 800.0f;
        float maxWidth = 1200.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = imageUri;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}