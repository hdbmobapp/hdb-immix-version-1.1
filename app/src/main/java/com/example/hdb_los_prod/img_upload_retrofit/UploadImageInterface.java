package com.example.hdb_los_prod.img_upload_retrofit;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface UploadImageInterface {
    @Multipart
    @POST("/HDB_QDE/HDB_QDE_v4.0/pdf_upload.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file);
}