package com.example.hdb_los_prod.img_upload_retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.hdb.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Administrator on 23-02-2016.
 */

public class ImageListAdapter extends CursorAdapter {
    ImageLoader imageLoader;
    public DisplayImageOptions img_options;

    SharedPreferences pref;

    public ImageListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        img_options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.slide_2)
                .showImageForEmptyUri(R.drawable.slide_2)
                .showImageOnFail(R.drawable.slide_2).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }


    // The newView method is used to inflate a new view and returnx it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.qde_image_list_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // Find fields to populate in inflated template
        TextView tvBody = (TextView) view.findViewById(R.id.tvBody);
        TextView tvPriority = (TextView) view.findViewById(R.id.tvPriority);
        ImageView img_image = (ImageView) view.findViewById(R.id.img_image);

       // pref = context.getSharedPreferences("MyPref", 0);


        // Extract properties from cursor
        String body = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
     //  int priority = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String priority = cursor.getString(6);
      //  String priority=pref.getString("img_name",null);
        String img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));

        // Populate fields with extracted properties
        tvBody.setText(body);
        tvPriority.setText(priority);


        //  imageLoader.displayImage(imageUri, img_image);
        String imageUri_ll = "file:///" + img_path; // from SD card

        imageLoader.displayImage(imageUri_ll, img_image);

//        imageLoader.displayImage(img_path,
//                img_image, img_options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }

        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }


        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }
}