package com.example.hdb_los_prod;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.UserFunction;


import org.json.JSONObject;

/**
 * Created by Vijay on 2/16/2017.
 */
public class PincodeScreen extends Activity {

    String ts, cust_id;

    TextView txt_pincode, txt_city, txt_state, txt_pincode_off, txt_city_off, txt_state_off, txt_err_msg;
    Button btn_verify, btn_upload, btn_verify_off;


    ProgressDialog mProgressDialog;
    UserFunction userFunction;
    JSONObject jsonobject;
    Cursor pincoddes;

    DBHelper dbHelper;
    String str_pincode, str_city, str_state, str_pincode_off, str_city_off, str_state_off, resi_flag, off_flag;

    String pincode, city, state, pincode_off, city_off, state_off;

    LinearLayout lay_resi, lay_off;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.qde_pincode_update);
        userFunction = new UserFunction(this);
        dbHelper = new DBHelper(PincodeScreen.this);

        txt_pincode = (TextView) findViewById(R.id.edt_pincode);
        txt_pincode_off = (EditText) findViewById(R.id.edt_pincode_off);
        txt_state = (TextView) findViewById(R.id.txt_state);
        txt_city = (TextView) findViewById(R.id.txt_city);

        txt_city_off = (TextView) findViewById(R.id.txt_city_off);
        txt_state_off = (TextView) findViewById(R.id.txt_state_off);
        txt_err_msg = (TextView) findViewById(R.id.text_error_msg);

        btn_verify = (Button) findViewById(R.id.button2);

        btn_verify_off = (Button) findViewById(R.id.btn_verify_off);

        lay_resi = (LinearLayout) findViewById(R.id.lay_resi);
        lay_off = (LinearLayout) findViewById(R.id.lay_off);

        Intent i = getIntent();
        ts = i.getStringExtra("ts");

        cust_id = i.getStringExtra("cust_id");

        System.out.println("PINcust_idCODES::::::" + cust_id);

        System.out.println("TSS::::::" + ts);

        pincoddes = dbHelper.get_pincodes(ts);

        try {
            if (pincoddes != null) {
                System.out.println("PINCODES not null::::::" + pincoddes);

                pincode = pincoddes.getString(0);
                city = pincoddes.getString(1);
                state = pincoddes.getString(2);
                pincode_off = pincoddes.getString(3);
                city_off = pincoddes.getString(4);
                state_off = pincoddes.getString(5);
                //  arr_pd_val.add(pd_val);

                System.out.println("PINCODES CITY::::::" + pincoddes.getString(1));
                System.out.println("PINCODES state off::::::" + pincoddes.getString(5));


                pincoddes.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((pincode == null || city == null || state == null || pincode_off == null) && (city_off == null || state_off == null)) {

        } else {
            if (pincode == null || city == null || state == null) {

                lay_off.setVisibility(View.GONE);

            } else {
                lay_resi.setVisibility(View.GONE);
            }
        }


        btn_upload = (Button) findViewById(R.id.btn_upload_off);


        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!txt_pincode.getText().toString().equals("")) {

                    off_flag = null;
                    resi_flag = "resi";
                    str_pincode = txt_pincode.getText().toString();

                    new Download_Masters().execute(str_pincode);
                }

            }
        });

        btn_verify_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!txt_pincode_off.getText().toString().equals("")) {

                    resi_flag = null;
                    off_flag = "off";

                    str_pincode_off = txt_pincode_off.getText().toString();

                    new Download_Masters().execute(str_pincode_off);
                }

            }
        });

        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println("pincode resi:::" + str_pincode + "pincode off:::" + str_pincode_off);
                if (str_pincode != null && str_pincode_off != null) {
                    dbHelper.get_QDE_request(ts, str_pincode, str_city, str_state, str_pincode_off, str_state_off,
                            str_city_off, true);

                }

            }
        });

    }


    public class Download_Masters extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(PincodeScreen.this);
            mProgressDialog.setMessage("Fetching Data...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String pincode = params[0];

            try {
                jsonobject = userFunction.get_pincode(pincode);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String args) {

            String city_id, city_name, state_id, state_name;
            //System.out.println("JSONARR::::::" + jsonarray);
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getApplicationContext().getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {


                    if (!jsonobject.getString("success").equals("false")) {

                        city_name = jsonobject.getString("city_name");

                        state_name = jsonobject.getString("state_name");

                        System.out.println("city_name:::" + city_name + ":::state::::" + state_name);

                        if (resi_flag != null) {
                            str_city = jsonobject.getString("city_id");
                            str_state = jsonobject.getString("state_id");

                            txt_city.setText(city_name);
                            txt_state.setText(state_name);
                        } else {
                            str_city_off = jsonobject.getString("city_id");
                            str_state_off = jsonobject.getString("state_id");

                            txt_city_off.setText(city_name);
                            txt_state_off.setText(state_name);
                        }
                        if (lay_resi.getVisibility() == View.VISIBLE && lay_off.getVisibility() == View.VISIBLE) {
                            if (!txt_city.getText().toString().equals("") && !txt_state.getText().toString().equals("")
                                    && !txt_city_off.getText().toString().equals("") && !txt_state_off.getText().toString().equals("")) {
                                btn_upload.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (lay_resi.getVisibility() == View.VISIBLE) {
                                if (!txt_city.getText().toString().equals("") && !txt_state.getText().toString().equals("")
                                        ) {
                                    btn_upload.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (!txt_city_off.getText().toString().equals("") && !txt_state_off.getText().toString().equals("")) {
                                    btn_upload.setVisibility(View.VISIBLE);
                                }

                            }
                        }

                        mProgressDialog.dismiss();
                    } else {

                        userFunction.cutomToast(jsonobject.getString("Error_msg"), getApplicationContext());
                        mProgressDialog.dismiss();
                    }

                    //   txt_error.setVisibility(View.GONE);

                    // if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                    // System.out.println("PARAM:::"+param);

/*
					} else {
						mProgressDialog.dismiss();
						SharedPreferences.Editor editor = pref.edit();
						editor.putInt("is_login", 0);
						editor.commit();
						loginErrorMsg.setText(jsonobject.getString("message").toString());
						//pd_details_layout.setVisibility(View.GONE);


						//pd_details_layout.setVisibility(View.GONE);

					}
*/
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void go_home(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

    @Override
    public void onBackPressed() {

    }
}
