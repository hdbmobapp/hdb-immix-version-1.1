package com.example.hdb_los_prod;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.libraries.UserFunction;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 18-04-2017.
 */
public class MyUploads_adapter extends BaseAdapter {


    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunction userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunction user_function;
    Integer int_pg_from;


    String app_id,str_ts,str_err;
    TableRow tbl_pic;


    public MyUploads_adapter(Context context,
                             ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;


        TableRow row_remraks;
        dialog = new ProgressDialog(context);
        user_function = new UserFunction(context);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);



        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.qde_uploads_list_item, parent,
                    false);



            holder = new ViewHolder();

            holder.tv_temp_losid = (TextView) itemView.findViewById(R.id.txt_temp_appid);

            holder.tv_cust_name = (TextView) itemView
                    .findViewById(R.id.txt_customer_name);

            holder.tv_cust_count = (TextView) itemView
                    .findViewById(R.id.txt_cust_count);

            holder.tv_fin_losid = (TextView) itemView
                    .findViewById(R.id.txt_final_losid);

            holder.tv_btn_coapp = (Button) itemView
                    .findViewById(R.id.btn_coapp);

            holder.tv_lay_error = (LinearLayout) itemView
                    .findViewById(R.id.lay_error);

            holder.tv_error_desc = (TextView) itemView
                    .findViewById(R.id.txt_error);

            holder.tv_req_date = (TextView) itemView
                    .findViewById(R.id.txt_req_date);

            holder.tv_product = (TextView) itemView
                    .findViewById(R.id.txt_prod);

            holder.tv_status = (TextView) itemView
                    .findViewById(R.id.txt_stataus);
            itemView.setTag(holder);


            holder.img_upload_status
                    = (TextView) itemView
                    .findViewById(R.id.img_upload_status);

        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }



        System.out.println("PR::::" + resultp.get(MyUploads.img_tot_cnt) + ":::err code:::" + resultp.get(MyUploads.img_uplod_count) +"error code::::" + resultp.get(MyUploads.error_code));

        if(!resultp.get(MyUploads.error_code).equals("null") ) {
            if (Integer.valueOf(resultp.get(MyUploads.error_code)) > 0) {
                holder.tv_lay_error.setVisibility(View.VISIBLE);
                holder.tv_error_desc.setText(resultp.get(MyUploads.error_desc));
                holder.tv_status.setText("ERROR");

                holder.img_upload_status.setVisibility(View.GONE);


            } else {


                    holder.tv_lay_error.setVisibility(View.GONE);
                    holder.tv_status.setText(resultp.get(MyUploads.error_desc));




            }
        }else{

            holder.img_upload_status.setVisibility(View.GONE);
        }


        holder.tv_temp_losid.setText(resultp.get(MyUploads.app_id));

        holder.tv_cust_name.setText(resultp.get(MyUploads.cust_name));
        holder.tv_cust_count.setText(resultp.get(MyUploads.cust_count));
        holder.tv_fin_losid.setText(resultp.get(MyUploads.final_losid));
        holder.tv_req_date.setText(resultp.get(MyUploads.req_date));

        holder.tv_product.setText(resultp.get(MyUploads.product));


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                app_id = resultp.get(MyUploads.app_id).toString();

                System.out.println("hahaha" + app_id);

                if (Integer.valueOf(resultp.get(MyUploads.img_uplod_count)) >= 1) {// new InsertData().execute();
                    Intent pd_form = new Intent(context, MyUploads_Coapp.class);
                    pd_form.putExtra("app_id_uploads", app_id);


                    pd_form.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(pd_form);
                } else {
                    if (Integer.valueOf(resultp.get(MyUploads.error_code)) == 0) {
                        user_function.cutomToast("Please upload Applicant Images", context);
                    } else {

                    }
                }


            }
        });

        holder.img_upload_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                resultp = data.get(position);

                app_id=resultp.get(MyUploads.app_id).toString();


                str_ts=resultp.get(MyUploads.ts).toString();

                Intent messagingActivity = new Intent(context,
                        Loan_Result.class);

                messagingActivity.putExtra("app_id",app_id);
                messagingActivity.putExtra("str_ts",str_ts);

                messagingActivity.putExtra("cust_type","P");
                messagingActivity.putExtra("cust_name", resultp.get(MyUploads.cust_name));

                messagingActivity.putExtra("losid", resultp.get(MyUploads.final_losid));
                messagingActivity.putExtra("product", resultp.get(MyUploads.product));
                messagingActivity.putExtra("img_uplod_count", resultp.get(MyUploads.img_uplod_count));
                messagingActivity.putExtra("img_tot_cnt", resultp.get(MyUploads.img_tot_cnt));

                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.  startActivity(messagingActivity);
            }
        });



        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_temp_losid;
        public TextView tv_cust_name;
        public TextView tv_cust_count;
        public TextView tv_fin_losid;
        public Button tv_btn_coapp;
        public TextView tv_req_date;

        public TextView tv_product;
        public TextView tv_status;
        public TextView tv_error_desc;

        public LinearLayout tv_lay_error;

        public TextView img_upload_status;
        public TableRow tbl_pic;
    }

}
