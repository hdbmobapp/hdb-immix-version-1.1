package com.example.hdb_los_prod;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.hdb_los_prod.img_upload_retrofit.UploadImageInterface;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.UserFunction;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MultipartBody;

/**
 * Created by Vijay on 4/7/2017.
 */
public class FragResult extends Activity {

    String app_id,cust_id,img_path;

    ProgressDialog mProgressDialog;
    UserFunction userFunction;

    TextView txt_succes,txt_losid,txt_error,txt_app_id;

    JSONObject jsonobject;

    private Handler mHandler = new Handler();

    String succes_msg,error_code;

    LinearLayout lay_error;
    UploadImageInterface uploadImage;

    Button btn_home,btn_send_req;

    TextView txt_err;

    boolean is_success=false;
    private static final String TAG = MainActivity.class.getSimpleName();
    DBHelper edb;
    private static  Boolean sucess=true;

    private static final String IMAGE_DIRECTORY_NAME = "/HDB_QDE";

    SharedPreferences pref;

    MultipartBody.Part fileToUpload;

    @Override



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.qde_result);

        Intent i=getIntent();
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        app_id=i.getStringExtra("app_id");
        cust_id = pref.getString("cust_id_appl", null);

        userFunction=new UserFunction(this);
        edb=new DBHelper(getApplicationContext());

        txt_succes=(TextView)findViewById(R.id.txt_success);
        txt_losid=(TextView)findViewById(R.id.txt_losid);

        txt_error=(TextView)findViewById(R.id.txt_error);

        txt_app_id=(TextView)findViewById(R.id.txt_app_id);

        txt_err=(TextView)findViewById(R.id.text_error_msg);

        btn_send_req=(Button)findViewById(R.id.btn_send_req);

        lay_error=(LinearLayout)findViewById(R.id.lay_error);
    //    mHandler.postDelayed(mRunnable, 5000);

        btn_home=(Button)findViewById(R.id.btn_home);

        new get_result().execute();

        if(is_success==true){

           // count=12

        }

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                Intent messagingActivity = new Intent(getApplicationContext(),
                        HdbHome.class);
                startActivity(messagingActivity);
                finish();

            }
        });

        btn_send_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

          new get_result().execute();

            }
        });

    }


    public class get_result extends AsyncTask<String, String, String> {

        protected void onPreExecute() {


            super.onPreExecute();

            mProgressDialog = new ProgressDialog(FragResult.this);

            mProgressDialog.setMessage("Processing your request...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                    jsonobject = userFunction.get_results(app_id);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String args) {

            try {
                if (jsonobject == null) {


                    mProgressDialog.dismiss();
                    btn_send_req.setVisibility(View.VISIBLE);

                } else {

                    if (!jsonobject.getString("success").equals("false")) {
                        succes_msg=jsonobject.getString("data1");

                        error_code=jsonobject.getString("error_code");

                        if(error_code.equals("0")) {
                            is_success=true;
                            txt_succes.setText(succes_msg);
                            txt_app_id.setText(app_id);
                            lay_error.setVisibility(View.GONE);
                            btn_send_req.setVisibility(View.GONE);

                            mProgressDialog.dismiss();


                            Cursor cursor=edb.getImages_applicant(cust_id);
                            if (cursor != null) {

                                while (cursor.moveToNext()) {
                                    img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));

                                    long length = 0;
                                    try {
                                        File  file = new File(img_path);
                                        length = file.length();
                                        length = length / 1024;
                                    } catch (Exception e) {

                                    }
                                    String str_long = Long.toString(length);
                                    // Populate fields with extracted properties


                                    if (length > 200) {
                                        String filename= compressImage(img_path);

                                        if(filename.equals(img_path)){
                                            new UploadPhoto111().execute(img_path);
                                            break;
                                        }

                                    }else {
                                        new UploadPhoto111().execute(img_path);
                                        break;
                                    }

                                }
                            }


                        }else{

                            txt_app_id.setText(app_id);
                            txt_succes.setText("ERROR");
                            mProgressDialog.dismiss();



                            Cursor cursor=edb.getImages_applicant(cust_id);
                            if (cursor != null) {

                                while (cursor.moveToNext()) {
                                    img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));

                                    edb.deletepic(img_path);
                                }

                            }

                          //  txt_error.setText(succes_msg);
                          //  lay_error.setVisibility(View.VISIBLE);
                           // new get_result().execute();




                        }
                        txt_losid.setText(jsonobject.getString("data2"));

                    } else {
                        if(jsonobject.getString("success").equals("false")){

                            txt_app_id.setText(app_id);

                            txt_error.setText(jsonobject.getString("message"));

                            lay_error.setVisibility(View.VISIBLE);

                            btn_send_req.setVisibility(View.VISIBLE);

                            Cursor cursor=edb.getImages_applicant(cust_id);
                            if (cursor != null) {

                                while (cursor.moveToNext()) {
                                    img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
                                    edb.deletepic(img_path);

                                }


                            }


                                    mProgressDialog.dismiss();

                        }else {

                            userFunction.cutomToast(jsonobject.getString("errorinfo"), getApplicationContext());
                            mProgressDialog.dismiss();
                        }
                    }
                }

            } catch (Exception e) {
            e.printStackTrace();
        }
    }

    }

    private class UploadPhoto111 extends AsyncTask<String,Integer,String> {
        private String str_path;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(FragResult.this);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            mProgressDialog.setProgress(0);

        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            mProgressDialog.setProgress(progress[0]);
        }
        @Override
        protected String doInBackground(String... params) {

            try {

                str_path=params[0];
                if (str_path != null) {
                    File file1 = new File(str_path);
                    if (file1.exists()) {
                        // Log.d("uploading File Esists : ", "true");
                        edb.doFileUpload(str_path);

                    //	Log.d("uploading File : ", img_path);
                    //  String new_img_path="/storage/emulated/0/PD/"+img_path+".jpg";
                     System.out.println("new_img_path::::::"+str_path);

                    } else {

                        edb.deletepic(str_path);


                    }
                } else {
                    try {
                        if (Integer.parseInt(edb.getPendingPhotoCount()) == 0) {

                            File mediaStorageDir;
                            String extStorageDirectory = Environment
                                    .getExternalStorageDirectory()
                                    .toString();

                            // External sdcard location
                            mediaStorageDir = new File(
                                    extStorageDirectory
                                            + IMAGE_DIRECTORY_NAME);

                            if (mediaStorageDir.isDirectory()) {
                                try {
                                    String[] children = mediaStorageDir
                                            .list();
                                    for (int i = 0; i < children.length; i++) {
                                        new File(mediaStorageDir, children[i]).delete();
                                    }

                                } catch (Exception e) {
                                    // block
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {
            edb.deletepic(str_path);


            mProgressDialog.dismiss();


            Cursor cursor=edb.getImages_applicant(cust_id);
            long length = 0;
            try {
                File  file = new File(img_path);
                length = file.length();
                length = length / 1024;
            } catch (Exception e) {

            }
            String str_long = Long.toString(length);
            // Populate fields with extracted properties


            if (cursor != null) {

                while(cursor.moveToNext()) {
                    img_path = cursor.getString(cursor.getColumnIndexOrThrow("img_path"));
                    System.out.println("IMG_path2222::::" + img_path);


                    if (length > 200) {

                       String filename= compressImage(img_path);

                        if(filename.equals(img_path)){
                            new UploadPhoto111().execute(img_path);
                            break;
                        }

                    }else {
                        new UploadPhoto111().execute(img_path);
                        break;
                    }



                }
            }

        }
    }
    public String compressImage(String imageUri) {

        String filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = imageUri;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


/*
    private final Runnable mRunnable = new Runnable() {
        public void run() {

            System.out.println("count");
            if(succes_msg==null) {
                System.out.println("success");
                new get_result().execute();
                mHandler.postDelayed(mRunnable, 1000);
            }else{
                if(!error_code.equals("0")){

                    System.out.println("error_code");
                    new get_result().execute();
                    mHandler.postDelayed(mRunnable, 1000);
                }else{
                    mHandler.removeCallbacks(mRunnable);
                }
            }
        }
    };*/

    @Override
    public void onBackPressed() {

    }

}
