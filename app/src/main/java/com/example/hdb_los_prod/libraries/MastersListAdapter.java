package com.example.hdb_los_prod.libraries;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.HdbHome;
import com.example.hdb_los_prod.Masters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MastersListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;

	ConnectionDetector cd;

	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunction userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;
	UserFunction user_function;
	Integer int_pg_from;
	DBHelper rDB;
	JSONArray jsonarray;
	ArrayList<String> master_flag;
	String
	flag_pm,
	id_pm,

	flag_pd,
	id_pd,flag_cc,id_cc,flag_cr,id_cr,flag_ccm,id_ccm,ac_flag,ac_id,bp_flag,bp_id,mp_flag,mp_id,mlp_flag,mlp_id,man_flag,man_id,app_flag,app_id,img_flag,img_id;

	ArrayList<String> arr_chk_master;

	SharedPreferences pref;

	Activity parent_act;

	Boolean btn_status=false;

	Masters master;
	  int count_btn=0, count_btn_1=0, count_btn_2=0,count_btn_3=0,count_btn_4=0,count_btn_5=0,count_btn_6=0,count_btn_7=0,count_btn_8=0,count_btn_9=0,count_btn_10=0,count_btn_11=0,count_btn_12=0;

	String param;
JSONObject jsonobject;

ArrayList<String>	al_product,al_prod_val,al_prod_flag,al_prod_id;

	public MastersListAdapter(Context context,
							  ArrayList<HashMap<String, String>> arraylist, Integer pg_from, Activity act) {
		this.context = context;
		data = arraylist;
		int_pg_from=pg_from;

		master=new Masters();
		cd=new ConnectionDetector(context);

		parent_act=act;

		TableRow row_remraks;
		dialog = new ProgressDialog(context);
		userFunction = new UserFunction(context);
		master_flag=new ArrayList<String>();
		al_product=new ArrayList<String>();
		al_prod_val=new ArrayList<String>();
		al_prod_flag=new ArrayList<String>();
		al_prod_id=new ArrayList<String>();

		arr_chk_master=new ArrayList<String>();
		pref = context.getSharedPreferences("MyPref", 0); // 0 -



		check_count();

	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}
	ViewHolder holder;
	@SuppressWarnings("unused")
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables


		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		rDB=new DBHelper(context);

		View itemView = null;
		// Get the position
		resultp = data.get(position);

		System.out.println("VIEW REFRESH::::");


		if (itemView == null) {
			// The view is not a recycled one: we have to inflate
			itemView = inflater.inflate(R.layout.qde_master_listview_item, parent,
					false);

			holder = new ViewHolder();

			holder.txt_master_name = (TextView) itemView
					.findViewById(R.id.txt_master_name);


			holder.btn_downld = (Button) itemView
					.findViewById(R.id.button2);


			itemView.setTag(holder);


		} else {
			// View recycled !
			// no need to inflate
			// no need to findViews by id
			holder = (ViewHolder) itemView.getTag();
		}

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }

int i=position;
		//for(int i=0;i<=10;i++) {
			holder.txt_master_name.setText("Master "+i);
		//}


		master_flag= rDB.get_master_flag();

		//System.out.println(" FLAG PROD:::" + master_flag.get(0)+" FLAG CONS::: "+ master_flag.get(2));

		flag_pm= master_flag.get(2);
		id_pm= master_flag.get(3);


		flag_pd= master_flag.get(0);
		id_pd= master_flag.get(1);

		flag_cc= master_flag.get(4);
		id_cc= master_flag.get(5);

		flag_cr= master_flag.get(6);
		id_cr= master_flag.get(7);

		flag_ccm= master_flag.get(8);
		id_ccm= master_flag.get(9);

		ac_flag= master_flag.get(10);
		ac_id= master_flag.get(11);

		bp_flag= master_flag.get(12);
		bp_id= master_flag.get(13);


		mp_flag= master_flag.get(14);
		mp_id= master_flag.get(15);

		mlp_flag= master_flag.get(16);
		mlp_id= master_flag.get(17);


		man_flag= master_flag.get(18);
		man_id= master_flag.get(19);


		app_flag= master_flag.get(20);
		app_id= master_flag.get(21);

		img_flag= master_flag.get(22);
		img_id= master_flag.get(23);

		System.out.println("PD1::9999::" + resultp.get(Masters.master_id));

		switch (resultp.get(Masters.master_id).toString()){

			case "PD":

				System.out.println("PD master chk");

				String temp_flag=resultp.get(Masters.QDE_flag).toString();
				System.out.println("PD1::::" + resultp.get(Masters.flag)+"PD 2::::"+flag_pd);
				Boolean result=userFunction.verify_master(temp_flag,flag_pd);
				if(result==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_1=1;
					check_count();


				}
				break;

			case "CONS":
				System.out.println("CONS master chk");


				String temp_flag_1=resultp.get(Masters.QDE_flag).toString();

				Boolean result1=userFunction.verify_master(temp_flag_1,flag_pm);
				if(result1==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_2=1;
					check_count();
				}
				break;


			case "CR":

				System.out.println("CR master chk");



				String temp_flag_3=resultp.get(Masters.QDE_flag).toString();

				System.out.println("FLG TBL cr:::" + temp_flag_3 + ":::FLAG SQLITE cr:::" + flag_cr);

				Boolean result3=userFunction.verify_master(temp_flag_3,flag_cr);


				System.out.println("qde_result CR:::::"+result3);
				if(result3==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_3=1;
					check_count();

				}
				break;

			case "CCM":

				System.out.println("CCM master chk");

				String temp_flag_4=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL CCM:::" + temp_flag_4 + ":::FLAG SQLITE CCM:::" + flag_ccm);

				Boolean result4=userFunction.verify_master(temp_flag_4,flag_ccm);
				if(result4==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_4=1;
					check_count();
				}
				break;



			case "CC":

				System.out.println("CC master chk");

				String temp_flag_2=resultp.get(Masters.QDE_flag).toString();
				System.out.println("PD211::::" + temp_flag_2+"PD 311::::"+flag_cc);
				Boolean result2=userFunction.verify_master(temp_flag_2,flag_cc);
				if(result2==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_5=1;
					check_count();
				}
				break;



			case "AC":

				System.out.println("AC master chk");

				String temp_flag_5=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL ac:::" + temp_flag_5 + ":::FLAG SQLITE ac:::" + ac_flag);

				Boolean result5=userFunction.verify_master(temp_flag_5,ac_flag);
				if(result5==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_6=1;
					check_count();

				}
				break;

			case "BP":

				System.out.println("BP master chk");

				String temp_flag_6=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL bp:::" + temp_flag_6 + ":::FLAG SQLITE bp:::" + bp_flag);

				Boolean result6=userFunction.verify_master(temp_flag_6,bp_flag);
				if(result6==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_7=1;
					check_count();
				}
				break;


			case "ML":

				System.out.println("ML master chk");

				String temp_flag_7=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL::: ml" + temp_flag_7 + ":::FLAG SQLITE ml:::" + mp_flag);

				Boolean result7=userFunction.verify_master(temp_flag_7,mp_flag);
				if(result7==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_8=1;
					check_count();
				}
				break;

			case "MLP":
				System.out.println("MLP master chk");

				String temp_flag_8=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL mlp:::" + temp_flag_8 + ":::FLAG SQLITE mlp:::" + mlp_flag);

				Boolean result8=userFunction.verify_master(temp_flag_8,mlp_flag);
				if(result8==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_9=1;
					check_count();
				}
				break;

			case "APP":

				System.out.println("APP master chk");

				String temp_flag_10=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL APP:::" + temp_flag_10 + ":::FLAG SQLITE APP:::"+app_flag  );

				Boolean result10=userFunction.verify_master(temp_flag_10,app_flag);


				if(result10==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_11=1;
					check_count();
				}
				break;


			case "LOS_QDE_v2.0":

				System.out.println("IMG  chk");
				String temp_flag_11=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL img:::" + temp_flag_11 + ":::FLAG SQLITE img:::" + img_flag);

				Boolean result11=userFunction.verify_master(temp_flag_11,img_flag);
				if(result11==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_12=1;
					check_count();
				}
				break;

			case "MM":

				System.out.println("MM  chk");
				String temp_flag_9=resultp.get(Masters.QDE_flag).toString();
				System.out.println("FLG TBL man:::" + temp_flag_9 + ":::FLAG SQLITE man:::" + man_flag);

				Boolean result9=userFunction.verify_master(temp_flag_9,man_flag);
				if(result9==false){
					holder.btn_downld.setVisibility(View.GONE);
					count_btn_10=1;
					check_count();
				}
				break;


		}
		//check_count();

		holder.btn_downld.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resultp = data.get(position);
				if (cd.isConnectingToInternet()) {
					new Download_Masters().execute(resultp.get(Masters.master_id).toString());
				}else{

					userFunction.noInternetConnection(
							"Check your connection settings!",
							context);
				}
				System.out.println("MAster ID:::" + resultp.get(Masters.master_id).toString());
				if(btn_status==true){
					holder.btn_downld.setVisibility(View.GONE);

				}
				notifyDataSetChanged();

				//context.startActivity(pd_form);
			}
		});
check_count();
		return itemView;
	}

	 public static class ViewHolder {

		public TextView txt_master_name;
		public TableRow tr_remarks;
		public Button btn_downld;

    }

public  void check_master(){

	arr_chk_master=rDB.get_master_flag();

}

	public class Download_Masters extends AsyncTask<String, String, String> {

		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(parent_act);
			mProgressDialog.setMessage("Loading Masters...Plz wait");
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

			mProgressDialog.setProgress(0);
			mProgressDialog.show();

			final int total_time = 100;
			final Thread t = new Thread() {

				@Override
				public void run() {
					int jump_time = 0;

					while (jump_time < total_time) {

						try {
							sleep(200);
							jump_time = +5;

							mProgressDialog.setProgress(jump_time );
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

				}

			};t.start();

		}

		@Override
		protected String doInBackground(String... params) {

			try {
				jsonobject=null;
				jsonobject = userFunction.get_masters(params[0]);
				param=params[0];
				if (jsonobject != null) {

						jsonarray = jsonobject.getJSONArray("Data");

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return param;
		}

		@Override

		protected void onPostExecute (String args){

			System.out.println("JSONARR::::::" + jsonarray);
			try {
				if (jsonobject == null) {
					userFunction.cutomToast(
							context.getResources().getString(R.string.error_message),
							context);
					System.out.println("JSON OBJ NULL:::");
					//	mProgressDialog.dismiss();

				} else {
					//   txt_error.setVisibility(View.GONE);

					//if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
					System.out.println("PARAM:::"+param);

					if (jsonarray != null) {
						if (param.equals("CONS")) {

							System.out.println("CONS downld");
							rDB.delete_cons_table();
							for (int i = 0; i <  jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								System.out.println(jsonobject.getString("id")+"aaaaa::::"+jsonobject.getString("value")+"::::"+ jsonobject.getString("QDE_flag")+"::::"+ "CONS");
							//	System.out.println("AL PM:::" +al_product.get(i));
								rDB.insert_prod( jsonobject.getString("value"),jsonobject.getString("id"), jsonobject.getString("QDE_flag"), "CONS",null,null,null);

							}
						count_btn=count_btn+1;

						}

						if (param.equals("PD")) {

							System.out.println("PD downld");
							rDB.delete_pd_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								rDB.insert_prod(jsonobject.getString("value"), jsonobject.getString("id"), jsonobject.getString("QDE_flag"), "PD", jsonobject.getString("prod_catg"), jsonobject.getString("start_date"), jsonobject.getString("end_date"));

							}
							count_btn=count_btn+1;

						}

						if (param.equals("CC")) {

							System.out.println("CC downld");

							rDB.delete_cc_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);
								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_prod(jsonobject.getString("value"),jsonobject.getString("id"), jsonobject.getString("QDE_flag"), "CC",null, jsonobject.getString("start_date"), jsonobject.getString("end_date"));

							}

							count_btn=count_btn+1;

						}

						if (param.equals("CR")) {

							System.out.println("CR downld");
							rDB.delete_cr_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_prod( jsonobject.getString("value"),jsonobject.getString("id"), jsonobject.getString("QDE_flag"), "CR",null, jsonobject.getString("start_date"), jsonobject.getString("end_date"));

							}
							count_btn=count_btn+1;
						}


						if (param.equals("CCM")) {

							System.out.println("CCM downld");
							rDB.delete_ccm_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL CCM:::" + al_product.get(i));
								rDB.insert_prod(jsonobject.getString("value"),jsonobject.getString("id"),   jsonobject.getString("QDE_flag"), "CCM",null,null,null);

							}
							count_btn=count_btn+1;
						}

						if (param.equals("BP")) {

							System.out.println("BP downld");
							rDB.delete_bp_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_block(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("QDE_flag"), "BP");

							}
							count_btn=count_btn+1;
						}

						if (param.equals("AC")) {

							System.out.println("AC downld");
							rDB.delete_ac_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_catg(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("catg_block_id"), jsonobject.getString("QDE_flag"), "AC");

							}
							count_btn=count_btn+1;
						}


						if (param.equals("ML")) {

							System.out.println("ML downld");
							rDB.delete_model_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_model(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("catg_id"), jsonobject.getString("make"), jsonobject.getString("make_model_flag"), jsonobject.getString("QDE_flag")

										, "ML", jsonobject.getString("man_id"), jsonobject.getString("start_date"), jsonobject.getString("end_date"));

							}
							count_btn=count_btn+1;
						}

						if (param.equals("MLP")) {

							System.out.println("MLP downld");

							rDB.delete_mlp_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_model_prod(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("QDE_flag"), "MLP");

							}
							count_btn=count_btn+1;
						}


						if (param.equals("MM")) {

							System.out.println("MM downld");
							rDB.delete_man_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_manu(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("QDE_flag"), "MM", jsonobject.getString("start_date"), jsonobject.getString("end_date"));

							}
							count_btn=count_btn+1;
						}


						if (param.equals("APP")) {

							System.out.println("APP downld");
							rDB.delete_app_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								//System.out.println("AL PM:::" + al_product.get(i));
								rDB.insert_prod(jsonobject.getString("id"), jsonobject.getString("value"), jsonobject.getString("QDE_flag"), "APP", null, null, null);

							}
							count_btn=count_btn+1;
						}

						if (param.equals("LOS_QDE_v2.0")) {

							System.out.println("APP downld");
							rDB.delete_IMG_table();
							for (int i = 0; i < jsonarray.length(); i++) {

								jsonobject = jsonarray.getJSONObject(i);

								System.out.println("doc type:::" + jsonobject);
								rDB.insert_img_data(jsonobject.getString("doc_id"), jsonobject.getString("doc_name"), jsonobject.getString("image_quality"), jsonobject.getString("img_hight"), jsonobject.getString("img_width"), "LOS_QDE_v2.0", jsonobject.getString("QDE_flag"), jsonobject.getString("doc_type"),jsonobject.getString("multiple_image"));

							}
							count_btn=count_btn+1;
						}


						mProgressDialog.dismiss();

						btn_status=true;
						notifyDataSetChanged();

						System.out.println("BTN status:::"+btn_status);
						Intent i=new Intent(context,Masters.class);


						i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						//context.startActivity(i);

						check_count();


					}
/*
					} else {
						mProgressDialog.dismiss();
						SharedPreferences.Editor editor = pref.edit();
						editor.putInt("is_login", 0);
						editor.commit();
						loginErrorMsg.setText(jsonobject.getString("message").toString());
						//pd_details_layout.setVisibility(View.GONE);


						//pd_details_layout.setVisibility(View.GONE);

					}
*/
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public  void check_count(){



		count_btn=count_btn_1+count_btn_2+count_btn_3+count_btn_4+count_btn_5+count_btn_6+count_btn_7+count_btn_8+count_btn_9+count_btn_10+count_btn_11+count_btn_12;


		SharedPreferences.Editor editor= pref.edit();
		editor.putInt("count_master",count_btn);
		editor.commit();
		System.out.println("COUNT BTN::::"+count_btn);

		if(count_btn==12){

			Intent i=new Intent(context, HdbHome.class);

			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			 context.startActivity(i);



		}

	}

}
