package com.example.hdb_los_prod.libraries;

import android.app.Activity;
import android.content.Context;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hdb.BuildConfig;
import com.example.hdb.R;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Vijay on 1/20/2017.
 */
public class UserFunction extends Activity {
    public final static String BASE_URL = "https://hdbapp.hdbfs.com";
    private static String URL = BASE_URL+"/HDB_QDE/HDB_QDE_v4.0/index.php";

    Context context;

    public UserFunction(Context context) {

        jsonParser = new JSONParser(context);

    }

    JSONParser jsonParser;

    public void cutomToast(String message, Context contxt) {
        // Inflate the Layout
        ViewGroup parent = null;
        LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.qde_custom_toast,
                parent, false);
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(contxt);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private static Age calculateAge(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        // System.out.println("CURR TIME::::"+);
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        //Get difference between months
        months = currMonth - birthMonth;
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        return new Age(days, months, years);
    }

    public String dateCompare(int day, int month, int year, String curr_date) {
        String stat;


        String[] parts = curr_date.split("-");
        String nsd = parts[2];
        String mnth = parts[1];
        String days = parts[0];


        System.out.println("YEAR:::" + nsd + "MONTHS::::" + mnth + "DAYS::::" + days);

/*

        float diff_year= Float.valueOf(nsd)-Float.valueOf(year);

        //float diff_months=diff_year*12+(Float.valueOf(mnth)-month);

        float diff_days=(diff_year*365)+(Float.valueOf(days)-day);

        float total_age=diff_days/365;

      //  System.out.println("DIFF MONTHS::::" + diff_months);

        System.out.println("DIFF DAYZZ::::" + diff_days);

        System.out.println("akjgdkhad" +String.valueOf(total_age) );

        String new_total_age=String.valueOf(total_age).replace(".","-");

        String[] parts1=String.valueOf(new_total_age).split("-");

        String cal_age=parts1[0];

        System.out.println("calc age" + cal_age);

        String select_date=day+"/"+month+"/*/
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


        String bday = day + "/" + month + "/" + year;

        Date birthDate = null;

        try {
            birthDate = sdf.parse(bday);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Yeh !! It's my date of birth :-)
        Age age = calculateAge(birthDate);
        if (age.years < 18) {
            stat = "";
        } else {
            stat = "Age : " + String.valueOf(age.years);
        }

        return stat;

    }

    public JSONObject loginUser(String username, String password, String imei_no, String latitude, String longitude, String device_name, String device_man, String android_id
    ,String imei1,String imei2) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "search");
            params.put("action", "search");
            params.put("target", "login");
            params.put("latitude", latitude);
            params.put("longitude", longitude);

            params.put("username", username);
            params.put("password", password);
            params.put("imei", imei_no);
            params.put("device_name", device_name);
            params.put("device_man", device_man);
            params.put("app_version", "QDE4.0");
            params.put("device_id", android_id);
            params.put("imei1", imei1);
            params.put("imei2", imei2);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, context);
            Log.e("JSON", json.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public void noInternetConnection(String message, Context contxt) {
        Toast toast = null;
        if (toast == null
                || toast.getView().getWindowVisibility() != View.VISIBLE) {
            ViewGroup parent = null;
            LayoutInflater inflater = (LayoutInflater) contxt
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.qde_toast_no_internet, parent,
                    false);
            TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
            mesg_name.setText(message);
            // Create Custom Toast
            toast = new Toast(contxt);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }

    public boolean logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }

    public boolean off_logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.off_resetTables();

        return true;
    }


    public JSONObject get_masters(String id) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_list_details_master");
            params.put("target", "loan");
            params.put("id", id);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject check_masters() {
        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_list_master");
            params.put("target", "loan");


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public boolean off_isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.off_getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }


    public boolean isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }

    public boolean logoutUser(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();
        //this.cutomToast("Hope you had a wonderful day! Do check back soon.",context);

        return true;
    }

    public JSONObject InsertData(
            String str_fname,
            String str_mname,
            String str_lname,
            String str_app_type,
            String str_relation,
            String str_product,
            String str_catagory,
            String str_const,
            String gender,
            String str_dob,
            String str_drining_lic,
            String str_pan,
            String str_passport,
            String str_aadhar,
            String str_voter,
            String str_asset_catg,
            String str_asset_make,
            String str_asset_model,
            String str_asset_cost,
            String str_margin_money,
            String str_loan_amt,

            String str_tenure,
            String str_consent_to_call,
            String str_addr1,
            String str_addr2,
            String str_addr3,
            String str_city,
            String str_state,
            String str_phone1,
            String str_phone2,
            String str_mobile,
            String str_email,
            String str_distance_km,
            String str_landmark,
            String str_curr_years,
            String str_city_yrs,
            String str_mail_addr,
            String str_ts,

            String str_addr1_off,
            String str_addr2_off,
            String str_addr3_off,
            String str_city_off,
            String str_state_off,
            String str_phone1_off,
            String str_phone2_off,
            String str_mobile_off,
            String str_email_off,
            String str_distance_km_off,
            String str_landmark_off,
            String str_curr_years_off,
            String str_city_yrs_off,
            String str_mail_addr_off,
            String str_das,
            String str_emp_name,
            String str_others,
            String str_emp_type,
            String str_emp_desination,
            String str_emp_code,
            String str_emp_branch,
            String app_id,
            String branch_id,
            String std_resi,
            String std_off,
            String pincode_resi,
            String pincode_off,
            String asset_man,
            String custid,
            String lats,
            String longs,
            String dist_type,
            String dist_km,
            String map_addr,
            String str_age,
            String img_count,String father_name,String mother_nam

    ) {
        // Building Parameters
        JSONObject json = null;

        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "insert_qde");
            params.put("target", "loan");
            // params.put("Lat", latitude);
            // params.put("Long", longitude);
            params.put("str_fname", str_fname);
            params.put("str_mname", str_mname);
            params.put("str_lname", str_lname);
            params.put("str_app_type", str_app_type);
            //    params.put("Comm_Dominated", str_Community);
            params.put("str_relation", str_relation);
            params.put("str_product", str_product);
            params.put("str_catagory", str_catagory);
            params.put("str_const", str_const);
            params.put("gender", gender);
            params.put("str_dob", str_dob);
            params.put("str_driving_lic", str_drining_lic);
            params.put("str_pan", str_pan);
            params.put("str_passport", str_passport);
            params.put("str_aadhar", str_aadhar);
            params.put("str_voter", str_voter);
            params.put("str_asset_catg", str_asset_catg);
            params.put("str_asset_make", str_asset_make);
            params.put("str_asset_model", str_asset_model);
            params.put("str_asset_cost", str_asset_cost);
            params.put("str_margin_money", str_margin_money);
            params.put("str_loan_amt", str_loan_amt);
            params.put("str_tenure", str_tenure);
            params.put("str_consent_to_call", str_consent_to_call);
            params.put("str_addr1", str_addr1);
            params.put("str_addr2", str_addr2);
            params.put("str_addr3", str_addr3);
            params.put("str_city", str_city);
            params.put("str_state", str_state);
            params.put("str_phone1", str_phone1);
            params.put("str_phone2", str_phone2);
            params.put("str_mobile", str_mobile);
            params.put("str_email", str_email);
            params.put("str_distance_km", str_distance_km);
            params.put("str_landmark", str_landmark);
            params.put("str_curr_years", str_curr_years);
            params.put("str_city_yrs", str_city_yrs);
            params.put("str_mail_addr", str_mail_addr);
            params.put("str_ts", str_ts);

            params.put("str_addr1_off", str_addr1_off);
            params.put("str_addr2_off", str_addr2_off);
            params.put("str_addr3_off", str_addr3_off);
            params.put("str_city_off", str_city_off);
            params.put("str_state_off", str_state_off);
            params.put("str_phone1_off", str_phone1_off);
            params.put("str_phone2_off", str_phone2_off);
            params.put("str_mobile_off", str_mobile_off);
            params.put("str_email_off", str_email_off);
            params.put("str_distance_km_off", str_distance_km_off);
            params.put("str_landmark_off", str_landmark_off);
            params.put("str_curr_years_off", str_curr_years_off);
            params.put("str_city_yrs_off", str_city_yrs_off);
            params.put("str_mail_addr_off", str_mail_addr_off);

            params.put("str_das", str_das);
            params.put("str_emp_nmae", str_emp_name);
            params.put("str_others", str_others);
            params.put("str_emp_type", str_emp_type);
            params.put("str_emp_desination", str_emp_desination);
            params.put("str_emp_code", str_emp_code);


            params.put("str_emp_branch", str_emp_branch);

            params.put("app_id", app_id);
            params.put("branch_id", branch_id);
            params.put("std_resi", std_resi);
            params.put("std_off", std_off);

            params.put("pincode_resi", pincode_resi);
            params.put("pincode_off", pincode_off);

            params.put("asset_man", asset_man);

            params.put("custid", custid);

            params.put("lats", lats);
            params.put("longs", longs);
            params.put("dist_type", dist_type);
            params.put("dist_km", dist_km);

            params.put("map_addr", map_addr);

            params.put("str_age", str_age);
            params.put("img_count", img_count);
            params.put("father_name", father_name);
            params.put("mother_nam", mother_nam);

            System.out.println("REQ params " + params.toString());
            Log.e("REQ params", params.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_pincode(
            String pincode
    ) {
        // Building Parameters
        JSONObject json = null;

        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_pincode_detail");
            params.put("target", "loan");
            params.put("pincode", pincode);


            System.out.println("REQ params " + params.toString());
            Log.e("REQ params", params.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_results(String app_id) {
        // Building Parameters
        JSONObject json = null;

        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_loan");
            params.put("target", "loan");
            params.put("app_id", app_id);
            System.out.println("REQ params " + params.toString());
            Log.e("REQ params", params.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public boolean verify_master(String master_flag, String master_flag1) {

        Boolean result = false;

        System.out.println("flag1::::" + master_flag + "flag 2::::" + master_flag1);

        if (master_flag1 != null) {
            if (!master_flag1.equals("null")) {

                if (Float.parseFloat(master_flag) > Float.parseFloat(master_flag1)) {
                    result = true;
                }
            } else {
                result = false;
            }
        } else {
            result = true;
        }

        return result;
    }


    public JSONObject get_CaseList(String user_id, String pg_no) {
        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_uploads");
            params.put("target", "loan");
            params.put("userid", user_id);
            params.put("pg", pg_no);
            System.out.println("REQ params " + params.toString());
            Log.e("REQ params", params.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_CaseList_coapp(String app_id, String pg_no) {
        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_uploads_coapp");
            params.put("target", "loan");
            params.put("app_id", app_id);
            params.put("pg", pg_no);
            System.out.println("REQ params " + params.toString());
            Log.e("REQ params", params.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getMessageBox(String user_id, String page_no) {

        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("userid", user_id);
            params.put("target", "messages");
            params.put("action", "search");
            params.put("messages_of", "PD");

            params.put("pg", page_no);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject update_message(String str_msg_id, String str_user_id) {
        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "messages");
            params.put("action", "update");
            params.put("userid", str_user_id);
            params.put("msg_id", str_msg_id);
            params.put("messages_of", "PD");


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject Update_details(String str_mail, String str_mobile, String str_uid) {
        JSONObject json = null;
        try {
              HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "login");
            params.put("action", "update");
            params.put("email", str_mail);
            params.put("mobile", str_mobile);
            params.put("userid", str_uid);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public void edtCaps(EditText edt_fname ) {
        InputFilter[] edtfilt = edt_fname.getFilters();
        InputFilter[] newfilter = new InputFilter[edtfilt.length + 1];
        System.arraycopy(edtfilt, 0, newfilter, 0, edtfilt.length);
        newfilter[edtfilt.length] = new InputFilter.AllCaps();
        edt_fname.setFilters(newfilter);
    }

    public JSONObject Update_count(String ts,String pic_count) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "loan");
            params.put("action", "update_count");
            params.put("img_count", pic_count);
            params.put("ts", ts);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_img_master(String cust_id,String app_type,String userid) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "loan");
            params.put("action", "get_img");
            params.put("cust_id", cust_id);
            params.put("app_type", app_type);
            params.put("userid", userid);



            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject loan_search(String cust_id,String app_type) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "loan");
            params.put("action", "get_img_pending");
            params.put("cust_id", cust_id);
            params.put("app_type", app_type);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("JSON", json.toString());
        return json;
    }

}