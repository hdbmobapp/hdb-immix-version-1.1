package com.example.hdb_los_prod.libraries;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.hdb_los_prod.Offine_Entries;
import com.example.hdb_los_prod.PincodeScreen;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class OffEntriesAdapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunction userFunction;

    DBHelper dbHelp;

    ViewHolder holder ;
    UserFunction user_function;
    Integer int_pg_from;
    DBHelper rDB;
    JSONArray jsonarray;

    String str_pincode,str_ts,str_cust_id;

    static  int  count_btn=0;

    String param;
    JSONObject jsonobject;


    public OffEntriesAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist, Integer pg_from) {
        this.context = context;
        data = arraylist;
        int_pg_from=pg_from;

        TableRow row_remraks;
        dialog = new ProgressDialog(context);
        userFunction = new UserFunction(context);
        dbHelp=new DBHelper(context);


    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        holder=null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);



        rDB=new DBHelper(context);

        View itemView = null;
        // Get the position
        resultp = data.get(position);

        this.notifyDataSetChanged();

        System.out.println("VIEW REFRESH::::");



        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.qde_off_entries_item, parent,
                    false);

            holder = new ViewHolder();

            holder.txt_name = (TextView) itemView
                    .findViewById(R.id.txt_name);
            holder.txt_app_type = (TextView) itemView
                    .findViewById(R.id.txt_app_type);


            holder.txt_product = (TextView) itemView
                    .findViewById(R.id.txt_product);

            holder.txt_pincode = (EditText) itemView
                    .findViewById(R.id.edt_pincode);

            holder.txt_req_id = (TextView) itemView
                    .findViewById(R.id.txt_req_id);

            holder.btn_verify = (Button) itemView
                    .findViewById(R.id.btn_coapp);

            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }

        holder.txt_name.setText(resultp.get(Offine_Entries.name));
        holder.txt_app_type.setText(resultp.get(Offine_Entries.app_type));

        holder.txt_product.setText(resultp.get(Offine_Entries.product));

        holder.txt_req_id.setText(resultp.get(Offine_Entries.cust_id)       );


        holder. btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resultp = data.get(position);

                str_ts=resultp.get(Offine_Entries.TS).toString();

                str_cust_id=resultp.get(Offine_Entries.cust_id).toString();

                System.out.println("APP TYPE::::"+resultp.get(Offine_Entries.app_type).toString());

                System.out.println("cust_id::::"+str_cust_id+"::::TS::::"+str_ts);

                Intent next=new Intent(context, PincodeScreen.class);
                next.putExtra("ts",str_ts);
                next.putExtra("cust_id", str_cust_id);
                next.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               context. startActivity(next);

            }
       });


        return itemView;
    }


    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Loading Masters...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            //	mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_pincode(str_pincode);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute (Void args){

            String city_id,city_name,state_id,state_name;
            //System.out.println("JSONARR::::::" + jsonarray);
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            context.getResources().getString(R.string.error_message),
                            context);
                    System.out.println("JSON OBJ NULL:::");
                    //	mProgressDialog.dismiss();

                } else {


                    if(!jsonobject.getString("success").equals("flase")){

                        city_id= jsonobject.getString("city_id");
                        city_name= jsonobject.getString("city_name");
                        state_id= jsonobject.getString("state_id");
                        state_name= jsonobject.getString("state_name");

                        System.out.println("city_name:::" + city_name + ":::state::::" + state_name);


                    }
                    else{

                        userFunction.cutomToast(jsonobject.getString("Error_msg"),context);
                    }
                    //   txt_error.setVisibility(View.GONE);

                    //if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    //System.out.println("PARAM:::"+param);

/*
					} else {
						mProgressDialog.dismiss();
						SharedPreferences.Editor editor = pref.edit();
						editor.putInt("is_login", 0);
						editor.commit();
						loginErrorMsg.setText(jsonobject.getString("message").toString());
						//pd_details_layout.setVisibility(View.GONE);


						//pd_details_layout.setVisibility(View.GONE);

					}
*/
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static class ViewHolder {


        public TextView txt_name;
        public TextView txt_app_type;
        public TextView txt_app_rel;
        public TextView txt_product;

        public EditText txt_pincode;

        public TextView txt_req_id;

        public Button btn_verify;

    }






}
