package com.example.hdb_los_prod.libraries;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;


import com.example.hdb_los_prod.FragResult;
import com.example.hdb_los_prod.HdbHome;
import com.example.hdb_los_prod.MainActivity;
import com.example.hdb_los_prod.Offine_Entries;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by obaro on 02/04/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    ProgressDialog dialog;

    JSONObject jObj = null;
    String json = null;
    ProgressDialog mProgressDialog;

    String str_pan,str_dob,str_fname,str_mname,str_lname,str_drining_lic,str_aadhar,str_asset_make,str_asset_model,str_asset_cost,str_margin_money,str_loan_amt,str_tenure,
            str_consent_to_call,str_passport,str_voter,str_addr1, str_addr2, str_addr3, str_city, str_state, str_phone1, str_phone2, str_mobile, str_email, str_distance_km, str_landmark, str_curr_years,
            str_city_yrs,str_mail_addr,row_id,str_app_type,str_relation,str_product,str_catagory,str_const,str_asset_catg,gender,str_entry_type,
            str_das,str_emp_name,str_others,str_emp_type,str_designation,str_emp_code,str_emp_branch,str_app_id,str_branch_id,str_asset_man,str_std_resi,str_std_off;

    String str_addr1_off,str_addr2_off,str_addr3_off,str_city_off,str_state__off,str_pincode_off,str_phone1_off,str_phone2_off,str_mobile_off,str_email_off,str_distance_km_off,str_landmar_off
            ,str_curr_years_off,str_curr_months_off,str_city_yrs_off,str_mail_addr_off,str_pincode,str_cust_id,str_age,img_count,str_father_name,str_mother_name;

    String str_dist_type,str_dist_km,str_lats,str_longs,str_map_addr;

    public static final String DATABASE_NAME = "QDE_Database.db";
    private static final int DATABASE_VERSION = 4;

    public static final String TBL_CUST = "tbl_cust_master";


    private static final String DATABASE_PROD_TABLE = "key_prod_master";


    private static final String DATABASE_QDE_TABLE = "key_qde_master";
    private static final String DATABASE_CONSTI_TABLE = "key_consti_master";


    JSONObject jsonobject;
    private static final String TAG = MainActivity.class.getSimpleName();


    //customer table

    public static final String KEY_srno= "srno";
    public static final String KEY_fname= "fname";
    public static final String KEY_mname= "mname";
    public static final String KEY_lname= "lname";
    public static final String KEY_app_type= "app_type";
    public static final String KEY_app_relation= "app_relation";
    public static final String KEY_product1= "product1";
    public static final String KEY_constn= "consti";
    public static final String KEY_gender= "gender";
    public static final String KEY_dob= "dob";
    public static final String KEY_age= "age";
    public static final String KEY_driving_lic= "driving_lic";
    public static final String KEY_pan_no= "pan_no";
    public static final String KEY_pass_no= "pass_no";
    public static final String KEY_aadhar_no= "aadhar_no";
    public static final String KEY_voterid_no= "voterid_no";
    public static final String KEY_asset_catg= "asset_catg";
    public static final String KEY_asset_make= "asset_make";
    public static final String KEY_asset_model= "asset_model";
    public static final String KEY_asset_cost= "asset_cost";
    public static final String KEY_margin_money= "margin_money";
    public static final String KEY_loan_amt= "loan_amt";
    public static final String KEY_tenure_months= "tenure_months";
    public static final String KEY_consent_tocall= "consent_tocall";
    public static final String KEY_addr_type= "addr_type";
    public static final String KEY_addr1= "addr1";
    public static final String KEY_addr2= "addr2";
    public static final String KEY_addr3= "addr3";
    public static final String KEY_pincode= "pincode";
    public static final String KEY_city= "city";
    public static final String KEY_state= "state";
    public static final String KEY_phone1= "phone1";
    public static final String KEY_phone2= "phone2";
    public static final String KEY_mobile= "mobile";
    public static final String KEY_email= "email";
    public static final String KEY_distance_km= "distance_km";
    public static final String KEY_landmark= "landmark";
    public static final String KEY_curr_years= "curr_years";
    public static final String KEY_city_years= "city_years";
    public static final String KEY_mail_addr= "mail_addr";
    public static final String KEY_CATG= "catagory";
    public static final String KEY_TS= "ts";
    public static final String KEY_FLAG= "flag";

    public static final String KEY_addr1_off= "off_addr1";
    public static final String KEY_addr2_off= "_off_addr2";
    public static final String KEY_addr3_off= "off_addr3";
    public static final String KEY_pincode_off= "off_pincode";
    public static final String KEY_city_off= "off_city";
    public static final String KEY_state_off= "off_state";
    public static final String KEY_phone1_off= "off_phone1";
    public static final String KEY_phone2_off= "off_phone2";
    public static final String KEY_mobile_off= "off_mobile";
    public static final String KEY_email_off= "off_email";
    public static final String KEY_distance_km_off= "off_distance_km";
    public static final String KEY_landmark_off= "off_landmark";
    public static final String KEY_curr_years_off= "off_curr_years";
    public static final String KEY_city_years_off= "off_city_years";
    public static final String KEY_mail_addr_off= "off_mail_addr";

    public static final String KEY_std_off= "std_off";
    public static final String KEY_std= "std";

    public static final String KEY_off= "off_entry";


    public static final String KEY_DAS= "das";
    public static final String KEY_emp_name= "emp_name";
    public static final String KEY_others= "others";
    public static final String KEY_emp_type= "emp_type";
    public static final String KEY_designation= "designation";
    public static final String KEY_emp_code= "emp_code";
    public static final String KEY_emp_branch= "emp_branch";

    public static final String KEY_emp_branch_id= "emp_branch_id";

    public static final String KEY_app_id= "app_id";


    public static final String KEY_cust_id= "cust_id";

    public static final String KEY_asset_manu= "aseet_man";

    public static final String KEY_prod_catg= "prod_catg";

    public static final String KEY_lats= "lats";
    public static final String KEY_longs= "longs";
    public static final String KEY_dist_type= "dist_type";
    public static final String KEY_distance= "dist_km";

    public static final String KEY_map_addr= "map_addr";
    public static final String KEY_img_count= "img_count";

    public static final String KEY_father_name= "father_name";
    public static final String KEY_mother_name= "mother_name";

    //image table

    //product table

    //PAY MODE TABLE


    public static final String KEY_pm_id = "pm_id";
    public static final String KEY_pay_mode = "pay_mode ";
    public static final String KEY_pm_flag = "pm_flag";
    public static final String KEY_pm_srno = "pm_srno";
    public static final String KEY_pm_val = "pm_val";


    //PRODUCT TABLE

    public static final String KEY_pd_id = "pd_id";
    public static final String KEY_product = "product";
    public static final String KEY_pd_flag = "pd_flag";
    public static final String KEY_pd_srno = "pd_srno";
    public static final String KEY_pd_val = "pd_val";

    public static final String KEY_pd_start = "pd_start";
    public static final String KEY_pd_end = "pd_end";


    //CONSTITUTION TABLE


    public static final String KEY_cons_id = "cons_id";
    public static final String KEY_cons = "cons";
    public static final String KEY_cons_flag = "cons_flag";
    public static final String KEY_cons_srno = "cons_srno";
    public static final String KEY_cons_val = "cons_val";


    //CC TABLE
    public static final String DATABASE_CC_TBL = "tbl_cc";

    public static final String KEY_cc_id = "cc_id";
    public static final String KEY_cc = "cc";
    public static final String KEY_cc_flag = "cc_flag";
    public static final String KEY_cc_srno = "cc_srno";
    public static final String KEY_cc_val = "cc_val";

    public static final String KEY_cc_start = "cc_start";
    public static final String KEY_cc_end = "cc_end";


    //CR TABLE

    public static final String DATABASE_CR_TBL = "tbl_cr";
    public static final String KEY_cr_id = "cr_id";
    public static final String KEY_cr = "cr";
    public static final String KEY_cr_flag = "cr_flag";
    public static final String KEY_cr_srno = "cr_srno";
    public static final String KEY_cr_val = "cr_val";

    public static final String KEY_cr_start = "cr_start";
    public static final String KEY_cr_end = "cr_end";


    //CCM TABLE

    public static final String DATABASE_CCM_TBL = "tbl_ccm";
    public static final String KEY_ccm_id = "ccm_id";
    public static final String KEY_ccm = "ccm";
    public static final String KEY_ccm_flag = "ccm_flag";
    public static final String KEY_ccm_srno = "ccm_srno";
    public static final String KEY_ccm_val = "ccm_val";



    // BLOCK PROD

    public static final String DATABASE_BLOCK_PROD = "tbl_block_prod";
    public static final String KEY_block_id = "block_id";
    public static final String KEY_block_product= "product";
    public static final String KEY_block_flag = "block_flag";
    public static final String KEY_block_srno = "block_srno";
    public static final String KEY_block_idn = "block_idn";


    //asset catg


    public static final String DATABASE_ASSET_CATG = "tbl_asset_catg";
    public static final String KEY_catg_id= "catg_id";
    public static final String KEY_catg= "catg";
    public static final String KEY_catg_flag= "catg_flag";
    public static final String KEY_catg_srno = "catg_srno";
    public static final String KEY_catg_idn= "catg_idn";
    public static final String KEY_catg_block_id= "catg_block_id";

    //model


    public static final String DATABASE_model = "tbl_model";
    public static final String KEY_modelid= "model_id";
    public static final String KEY_modelno= "model_no";
    public static final String KEY_make= "model_make";
    public static final String KEY_make_model_flag= "make_model_flag";
    public static final String KEY_model_catg_id= "catg_id";

    public static final String KEY_model_man_id= "model_man_id";
    public static final String KEY_model_srno= "model_srno";
    public static final String KEY_model_idn= "model_idn";
    public static final String KEY_model_flag= "model_flag";

    public static final String KEY_model_start = "model_start";
    public static final String KEY_model_end = "model_end";


    // model product


    public static final String DATABASE_model_product = "tbl_model_product";
    public static final String KEY_model_product_id= "model_id";
    public static final String KEY_model_product_code= "product_code";
    public static final String KEY_model_product_srno= "model_product_srno";

    public static final String KEY_model_product_sidn= "model_product_idn";
    public static final String KEY_model_product_flag= "model_product_flag";

    String str_ts;

    //MANU TABLE

    public static final String DATABASE_MANU_TBL = "tbl_man";
    public static final String KEY_man_id = "man_id";
    public static final String KEY_man = "man";
    public static final String KEY_man_flag = "man_flag";
    public static final String KEY_man_srno = "man_srno";
    public static final String KEY_man_val = "man_val";

    public static final String KEY_man_start = "man_start";
    public static final String KEY_man_end = "man_end";

    public static final String KEY_STATUS = "status";
    private static String KEY_SUCCESS = "success";

    private static final String IMAGE_DIRECTORY_NAME = "/HDB_QDE";
    ConnectionDetector cd;
    String str_user_id;
  //  HttpEntity resEntity;
    UserFunction userFunction;
    SharedPreferences pref;
    Context context;
    private static int VERSION = 2;
    private static String DBNAME = "HDBHMCV";
    private SQLiteDatabase mDB;

    ArrayList<String> get_masters;

    ArrayList<String> get_flag;

    ArrayList<String> arr_pd, arr_pd_val,arr_cons,arr_cons_val,arr_doc_type;

    // IMAGE TABLE


    public static final String IMAGE_TABLE_NAME = "tbl_image_master";
    public static final String IMAGE_COLUMN_ID = "_id";
    public static final String IMAGE_LOSID = "img_losid";
    public static final String IMAGE_USERID = "img_userid";
    public static final String IMAGE_TS = "img_ts";
    public static final String IMAGE_MAPCOUNT = "img_map_count";
    public static final String IMAGE_STATUS = "img_status";
    public static final String IMAGE_PATH = "img_path";
    public static final String IMAGE_CAPTION = "img_captions";

    //IMAGE MASTER
    public static final String IMAGE_SRNO = "img_srno";
    public static final String IMAGE_ID = "img_id";
    public static final String IMAGE_TITLE = "img_title";
    public static final String IMAGE_FLAG = "img_flag";
    public static final String IMAGE_width = "IMAGE_width";
    public static final String IMAGE_height = "IMAGE_height";
    public static final String IMAGE_resolution = "IMAGE_resolution";
    public static final String IMAGE_tag = "IMAGE_tag";
    public static final String IMAGE_QDE_flag = "IMAGE_QDE_flag";
    public static final String IMAGE_doc_type = "IMAGE_doc_type";
    public static final String IMAGE_multiple_page = "Image_multiple_page";



    public static final String  IMAGE_APP_TYPE= "img_app_type";
    public static final String  IMAGE_NO= "img_no";
    public static final String  IMAGE_SR_NO= "img_sr_no";
    public static final String  IMAGE_COUNT= "img_count";
    public static final String  IMAGE_TIMESTAMP= "img_timestamp";


    private static final String DATABASE_IMG_MASTER = "key_img_master";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        userFunction = new UserFunction(context);
        this.context = context;
        cd = new ConnectionDetector(context);
        this.mDB = getWritableDatabase();
        pref = context.getSharedPreferences("MyPref", 0);

        get_masters = new ArrayList<String>();

        get_flag = new ArrayList<String>();



        //   arr_pd = new ArrayList<String>();
        //   arr_pd_val = new ArrayList<String>();
        arr_cons = new ArrayList<String>();
        arr_cons_val = new ArrayList<String>();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        userFunction = new UserFunction(context);



        String sql_pd_query = "create table " + DATABASE_PROD_TABLE + " ( "
                + KEY_pd_srno + " integer primary key autoincrement , "
                + KEY_product + " text , " + KEY_pd_val + " text , " + KEY_pd_id + " text, "
                + KEY_pd_flag + " text ,"+ KEY_prod_catg + " text, "+KEY_pd_start+" text, "+KEY_pd_end+" text )";

        db.execSQL(sql_pd_query);

        String sql_block_query = "create table " + DATABASE_BLOCK_PROD + " ( "
                + KEY_block_srno + " integer primary key autoincrement , "
                + KEY_block_id + " text , " + KEY_block_product + " text , " + KEY_block_idn + " text, "
                + KEY_block_flag + " text )";

        db.execSQL(sql_block_query);

        String sql_aset_catg= "create table " + DATABASE_ASSET_CATG + " ( "
                + KEY_catg_srno + " integer primary key autoincrement , "
                + KEY_catg_id + " text , " + KEY_catg + " text , " + KEY_catg_block_id + " text, "
                + KEY_catg_flag + " text , " + KEY_catg_idn + " text ) " ;


        db.execSQL(sql_aset_catg);


        String sql_model= "create table " + DATABASE_model + " ( "
                + KEY_model_srno + " integer primary key autoincrement , "
                + KEY_modelid + " text , " + KEY_modelno + " text , " + KEY_make + " text, "
                + KEY_make_model_flag + " text , " + KEY_model_catg_id + " text , " + KEY_model_man_id + " text , "+KEY_model_flag + " text , "+KEY_model_idn+" text , "
                +KEY_model_start+" text, "+KEY_model_end+" text )";


        db.execSQL(sql_model);

        String sql_model_product= "create table " + DATABASE_model_product + " ( "
                + KEY_model_product_srno + " integer primary key autoincrement , "
                + KEY_model_product_id + " text , " + KEY_model_product_code + " text , " + KEY_model_product_flag + " text, "
                + KEY_model_product_sidn + " text )"  ;
        ;

        db.execSQL(sql_model_product);

        String sql_man_query = "create table " + DATABASE_MANU_TBL + " ( "
                + KEY_man_srno + " integer primary key autoincrement , "
                + KEY_man + " text , " + KEY_man_val + " text , " + KEY_man_id + " text, "
                + KEY_man_flag + " text , " +KEY_man_start+" text, "+KEY_man_end+" text )";

        db.execSQL(sql_man_query);

        String sql_QDE_query = "create table " + DATABASE_QDE_TABLE + " ( "
                + KEY_srno + " integer primary key autoincrement , "
                + KEY_fname + " text , " + KEY_mname + " text , " + KEY_lname + " text, "
                +  KEY_app_type + " text, "
                + KEY_app_relation + " text, " + KEY_product1 + " text, " + KEY_CATG + " text, "+ KEY_constn + " text, "
                + KEY_gender + " text, "+ KEY_dob + " text, "+ KEY_driving_lic + " text, "+ KEY_pan_no + " text, "
                + KEY_pass_no + " text, " + KEY_aadhar_no + " text, " + KEY_voterid_no + " text, " + KEY_asset_catg + " text, "+ KEY_asset_make + " text, "
                + KEY_asset_model + " text, " + KEY_asset_cost + " text, " + KEY_margin_money + " text, "+ KEY_loan_amt + " text, "+ KEY_tenure_months + " text, "
                + KEY_consent_tocall + " text, " +  KEY_addr1 + " text, " + KEY_addr2 + " text, " + KEY_addr3 + " text, "
                + KEY_city + " text, " + KEY_state + " text, " + KEY_phone1 + " text, " + KEY_phone2 + " text, " + KEY_mobile + " text, " + KEY_email + " text, "
                +KEY_distance_km + " text, " + KEY_landmark + " text, " + KEY_curr_years + " text, "
                + KEY_city_years + " text, " + KEY_mail_addr + " text, " +KEY_addr1_off + " text, " + KEY_addr2_off + " text, " + KEY_addr3_off + " text, " +KEY_city_off + " text, " +KEY_state_off + " text, "
                +KEY_phone1_off + " text, " +KEY_phone2_off + " text, " +KEY_mobile_off + " text, " +KEY_email_off + " text, " +KEY_distance_km_off + " text, "
                +KEY_landmark_off + " text, " +KEY_curr_years_off + " text, " +KEY_city_years_off + " text, " +KEY_mail_addr_off + " text, "
                +KEY_off + " text, " +KEY_TS + " text, "+ KEY_FLAG + " text, "+KEY_pincode+" text, " +KEY_pincode_off+" text, " +KEY_DAS+" text,"+KEY_emp_name+" text,"+KEY_others+" text,"
                +KEY_emp_type+" text,"+KEY_designation+" text,"+KEY_emp_code+" text,"+KEY_emp_branch+" text, "+KEY_app_id+" text,"+KEY_emp_branch_id+" text,"+ KEY_asset_manu+" text,"+ KEY_std+" text,"+KEY_std_off+" text ,"+ KEY_cust_id+" text, "+KEY_lats+" text,"
                +KEY_longs+" text, "+KEY_dist_type+" text,"+KEY_distance+" text,"+KEY_map_addr+" text,"+KEY_age+" text,"+KEY_img_count+" text,"+KEY_father_name+" text,"+KEY_mother_name+" text)";


        db.execSQL(sql_QDE_query);

        db.execSQL(
                "CREATE TABLE " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        IMAGE_LOSID + " TEXT, " +
                        IMAGE_USERID + " TEXT, " +
                        IMAGE_TS + " TEXT, " +
                        IMAGE_MAPCOUNT + " TEXT, " +
                        IMAGE_PATH + " TEXT, " +
                        IMAGE_CAPTION + " TEXT, " +
                        IMAGE_STATUS + " INTEGER , "+
                        IMAGE_APP_TYPE + " TEXT, " +
                        IMAGE_NO + " TEXT, " +
                        IMAGE_SR_NO + " TEXT, " +
                        IMAGE_COUNT + " TEXT, " +
                        IMAGE_TIMESTAMP + " TEXT )"
        );


        db.execSQL(
                "CREATE TABLE " + DATABASE_IMG_MASTER +
                        "(" + IMAGE_SRNO + " integer primary key autoincrement, " +
                        IMAGE_ID + " TEXT, " +
                        IMAGE_TITLE + " TEXT, " +
                        IMAGE_FLAG + " TEXT, " +
                        IMAGE_width + " TEXT, " +
                        IMAGE_height + " TEXT, " +
                        IMAGE_resolution + " TEXT, "
                        + IMAGE_QDE_flag + " TEXT, "
                        + IMAGE_tag + " TEXT,  "
                        + IMAGE_doc_type + " TEXT,  "
                        + IMAGE_multiple_page + " TEXT )"
        );


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_QDE_TABLE);

        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_PROD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_BLOCK_PROD);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_MANU_TBL);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_ASSET_CATG);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_model_product);

        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_model);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CC_TBL);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CCM_TBL);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CR_TBL);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_CONSTI_TABLE);

        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_IMG_MASTER);
        db.execSQL("DROP TABLE IF EXISTS " + IMAGE_TABLE_NAME);


        onCreate(db);
    }


    public ArrayList<String> get_master_flag() {

        SQLiteDatabase db = this.getReadableDatabase();
        String cons_flag = null;
        String cons_id = null;


        String pd_flag = null;
        String pd_id = null;
        String cc_flag = null;
        String cc_id = null;
        String cr_flag = null;
        String cr_id = null;
        String ccm_flag = null;
        String ccm_id = null;
        String ac_id = null;
        String ac_flag = null;
        String bp_id = null;
        String bp_flag = null;
        String model_id = null;
        String model_flag = null;
        String mp_id = null;
        String mp_flag = null;

        String man_id = null;
        String man_flag = null;

        String app_id = null;
        String app_flag = null;

        String img_id = null;
        String img_flag = null;

        String countQuery = "SELECT " + KEY_pd_flag + "," + KEY_pd_id + " FROM "
                + DATABASE_PROD_TABLE + " where "+KEY_pd_id+" = ?  LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, new String[]{"PD"});

        // System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pd_flag = cursor.getString(0);
                    pd_id = cursor.getString(1);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        SQLiteDatabase db1 = this.getReadableDatabase();

        String countQuery1 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PROD_TABLE + " where "+KEY_pd_id+" = ? LIMIT 1";
        Cursor cursor1 = db1.rawQuery(countQuery1, new String[]{"CONS"});

        //System.out.println("countQuery:" + cursor);

        try {
            if (cursor1 != null) {

                if (cursor1.moveToNext()) {
                    cons_flag = cursor1.getString(0);
                    cons_id = cursor1.getString(1);
                }
                cursor1.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }


        SQLiteDatabase db2 = this.getReadableDatabase();


        String countQuery2 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PROD_TABLE +  " where "+KEY_pd_id+" = ? LIMIT 1";

        Cursor cursor2 = db2.rawQuery(countQuery2, new String[]{"CC"});

        //   System.out.println("countQuery:" + cursor);

        try {
            if (cursor2 != null) {

                if (cursor2.moveToNext()) {
                    cc_flag = cursor2.getString(0);
                    cc_id = cursor2.getString(1);
                }
                cursor2.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }


        SQLiteDatabase db3 = this.getReadableDatabase();


        String countQuery3 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PROD_TABLE + " where "+KEY_pd_id+" = ? LIMIT 1";
        Cursor cursor3 = db3.rawQuery(countQuery3, new String[]{"CR"});
//
//
        try {
            if (cursor3 != null) {

                if (cursor3.moveToNext()) {
                    cr_flag = cursor3.getString(0);
                    cr_id = cursor3.getString(1);
                }
                cursor3.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }

        SQLiteDatabase db4 = this.getReadableDatabase();




        String countQuery4 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PROD_TABLE + " where "+KEY_pd_id+" = ? LIMIT 1";
        
        Cursor cursor4 = db4.rawQuery(countQuery4, new String[]{"CCM"});

        System.out.println("countQuery:" + countQuery4);

        try {
            if (cursor4 != null) {

                if (cursor4.moveToNext()) {

                    ccm_flag = cursor4.getString(0);
                    ccm_id = cursor4.getString(1);
                    System.out.println("ccm_flag:" + ccm_flag);


                }
                cursor4.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }


        SQLiteDatabase db5 = this.getReadableDatabase();


        String countQuery5 = "SELECT " + KEY_catg_flag + " , " + KEY_catg_idn + "  FROM "
                + DATABASE_ASSET_CATG + " where "+KEY_catg_idn+" = ? LIMIT 1";
        Cursor cursor5 = db5.rawQuery(countQuery5, new String[]{"AC"});

        // System.out.println("countQuery:" + cursor);

        try {
            if (cursor5 != null) {

                if (cursor5.moveToNext()) {
                    ac_flag = cursor5.getString(0);
                    ac_id = cursor5.getString(1);
                }
                cursor5.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db5.close();
        }


        SQLiteDatabase db6 = this.getReadableDatabase();


        String countQuery6 = "SELECT " + KEY_block_flag + " , " + KEY_block_idn + "  FROM "
                + DATABASE_BLOCK_PROD + " where "+KEY_block_idn+" = ? LIMIT 1";
        Cursor cursor6 = db6.rawQuery(countQuery6, new String[]{"BP"});


        //  System.out.println("countQuery:" + cursor);

        try {
            if (cursor6 != null) {

                if (cursor6.moveToNext()) {
                    bp_flag = cursor6.getString(0);
                    bp_id = cursor6.getString(1);
                }
                cursor5.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db6.close();
        }

        SQLiteDatabase db7 = this.getReadableDatabase();


        String countQuery7 = "SELECT " + KEY_model_flag + " , " + KEY_model_idn + "  FROM "
                + DATABASE_model + " where "+KEY_model_idn+" = ? LIMIT 1";
        Cursor cursor7 = db7.rawQuery(countQuery7, new String[]{"ML"});

        //    System.out.println("countQuery:" + cursor);

        try {
            if (cursor7 != null) {

                if (cursor7.moveToNext()) {
                    model_flag = cursor7.getString(0);
                    model_id = cursor7.getString(1);
                }
                cursor5.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db7.close();
        }

        SQLiteDatabase db8 = this.getReadableDatabase();


        String countQuery8 = "SELECT " + KEY_model_product_flag + " , " + KEY_model_product_sidn + "  FROM "
                + DATABASE_model_product + " where "+KEY_model_product_sidn+" = ? LIMIT 1";

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{"MLP"});

        //    System.out.println("countQuery:" + cursor);

        try {
            if (cursor8 != null) {

                if (cursor8.moveToNext()) {
                    mp_flag = cursor8.getString(0);
                    mp_id = cursor8.getString(1);
                }
                cursor8.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db8.close();
        }


        SQLiteDatabase db9 = this.getReadableDatabase();


        String countQuery9 = "SELECT " + KEY_man_flag + " , " + KEY_man_id + "  FROM "
                + DATABASE_MANU_TBL + " where "+KEY_man_id+" = ? LIMIT 1";

        Cursor cursor9 = db9.rawQuery(countQuery9, new String[]{"MM"});

        //    System.out.println("countQuery:" + cursor);

        try {
            if (cursor9 != null) {

                if (cursor9.moveToNext()) {
                    man_flag = cursor9.getString(0);
                    man_id = cursor9.getString(1);
                }
                cursor9.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db9.close();
        }


        SQLiteDatabase db10= this.getReadableDatabase();


        String countQuery10 = "SELECT " + KEY_pd_flag + " , " + KEY_pd_id + "  FROM "
                + DATABASE_PROD_TABLE + " where "+KEY_pd_id+" = ? LIMIT 1";

        Cursor cursor10 = db10.rawQuery(countQuery10, new String[]{"APP"});

        //    System.out.println("countQuery:" + cursor);

        try {
            if (cursor10 != null) {

                if (cursor10.moveToNext()) {
                    app_flag = cursor10.getString(0);
                    app_id = cursor10.getString(1);
                }
                cursor10.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db10.close();
        }

        SQLiteDatabase db11= this.getReadableDatabase();


        String countQuery11 = "SELECT " + IMAGE_QDE_flag + " , " + IMAGE_tag + "  FROM "

                + DATABASE_IMG_MASTER + " where "+IMAGE_tag+" = ? LIMIT 1";

        Cursor cursor11 = db11.rawQuery(countQuery11, new String[]{"LOS_QDE_v2.0"});

        //    System.out.println("countQuery:" + cursor);

        try {
            if (cursor11 != null) {

                if (cursor11.moveToNext()) {
                    img_flag = cursor11.getString(0);
                    img_id = cursor11.getString(1);
                    System.out.println("flag:" + img_flag+"::::id"+img_id);
                }
                cursor11.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db11.close();
        }

        get_flag.add(pd_flag);
        get_flag.add(pd_id);
        get_flag.add(cons_flag);
        get_flag.add(cons_id);
        get_flag.add(cc_flag);
        get_flag.add(cc_id);
        get_flag.add(cr_flag);
        get_flag.add(cr_id);
        get_flag.add(ccm_flag);
        get_flag.add(ccm_id);
        get_flag.add(ac_flag);
        get_flag.add(ac_id);
        get_flag.add(bp_flag);
        get_flag.add(bp_id);
        get_flag.add(model_flag);
        get_flag.add(model_id);

        get_flag.add(mp_flag);
        get_flag.add(mp_id);

        get_flag.add(man_flag);
        get_flag.add(man_id);

        get_flag.add(app_flag);
        get_flag.add(app_id);
        get_flag.add(img_flag);
        get_flag.add(img_id);

        return get_flag;
        // System.out.println("countQuery:"+product_qnt);
    }

    public Cursor get_catagory( String product) {
        SQLiteDatabase db8 = this.getReadableDatabase();

        String countQuery8 = "SELECT " + KEY_catg_id + " , " + KEY_catg + "  FROM "
                + DATABASE_ASSET_CATG + " where "+KEY_catg_block_id+" in ( select " +KEY_block_id+" from "+DATABASE_BLOCK_PROD+
                " where "+KEY_block_product+" = ? ) order by "+KEY_catg;

        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";

        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        //Cursor cursor8 = db8.rawQuery(countQuery8, null);

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{product});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"});

        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));


        return cursor8;

    }


    public Cursor get_manufacturer( String catg_id) {
        SQLiteDatabase db8 = this.getReadableDatabase();

        String countQuery8 = "SELECT " + KEY_man_val + " , " + KEY_man + "  FROM "
                + DATABASE_MANU_TBL + " where "+KEY_man_val+" in ( select distinct " +KEY_model_man_id+" from "+DATABASE_model+" a join "
                +DATABASE_model_product+" b on a."+KEY_modelid+" = b."+KEY_model_product_id+
                " where b."+KEY_model_product_code+" = ? ) and "+KEY_man_start+" < datetime() and "+KEY_man_end+" > datetime() order by "+KEY_man;



        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";


        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        // Cursor cursor8 = db8.rawQuery(countQuery8, null);

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{catg_id});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"});

        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));


        return cursor8;

    }

    public Cursor get_make( String prod ,String manu) {
        SQLiteDatabase db8 = this.getReadableDatabase();

        String countQuery8 = "SELECT " + KEY_modelid + ", "+KEY_modelno+" , "+KEY_make+" FROM "
                + DATABASE_model + " where "+KEY_make_model_flag+" in ('MA')  and " +
                KEY_model_man_id+" = ? and "+KEY_modelid+" in ( select "+KEY_modelid+" from "+DATABASE_model_product+" where "+KEY_model_product_code+" = ? )";

        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";

        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        // Cursor cursor8 = db8.rawQuery(countQuery8, null);

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{manu,prod});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"}


        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));


        return cursor8;

    }

    public Cursor get_models( String make,String prod,String man_id) {
        SQLiteDatabase db8 = this.getReadableDatabase();

        //  prod="CVEH";
        System.out.println("man_id:" + make);
        String countQuery8 ="select "+KEY_modelid +" , "+KEY_modelno+" from "+DATABASE_model+" where "+KEY_make+" = ? "
                +"and "+KEY_make_model_flag+" = ? AND " +KEY_model_man_id +" = ? and "+KEY_model_start+" < datetime() and "+KEY_model_end+" > datetime() order by "+KEY_modelno ;

        System.out.println("QUERY model::::" + countQuery8);

        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";

        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        // Cursor cursor8 = db8.rawQuery(countQuery8, null);

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{make,"MO",man_id});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"});

        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));

        return cursor8;
    }


    public Cursor get_cust_catg( String prod) {
        SQLiteDatabase db8 = this.getReadableDatabase();


        System.out.println("man_id:" + prod);
        String countQuery8 = "SELECT  a. "+KEY_product + " , a."+KEY_pd_val + "  FROM (select * from "+
                DATABASE_PROD_TABLE+ " where "+ KEY_pd_id+ "= ? ) a  join ( select * from "+DATABASE_PROD_TABLE + " where "+ KEY_pd_id+ "= ? ) b on a."+KEY_pd_val+" =b."+KEY_pd_val+ " where b."+KEY_product+" = ? order by a."+ KEY_product;



        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";

        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        // Cursor cursor8 = db8.rawQuery(countQuery8, null);



        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{"CC","CCM",prod});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"});

        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));


        return cursor8;

    }



    public void delete_pd_table(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows


        db.delete(DATABASE_PROD_TABLE, KEY_pd_id + " = ? ", new String[]{"PD"});
        db.close();
    }

    public void delete_cons_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows

        db.delete(DATABASE_PROD_TABLE, KEY_pd_id + " = ? ", new String[]{"CONS"});


        // db.delete(DATABASE_CONSTI_TABLE, null, null);
        db.close();
    }

    public void delete_cc_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        //    db.delete(DATABASE_CC_TBL, null, null);
        db.delete(DATABASE_PROD_TABLE, KEY_pd_id + " = ? ", new String[]{"CC"});

        db.close();
    }

    public void delete_cr_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        //    db.delete(DATABASE_CR_TBL, null, null);
        db.delete(DATABASE_PROD_TABLE,KEY_pd_id+ " = ? ",new String[]{"CR"});
        db.close();
    }

    public void delete_ccm_table() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_PROD_TABLE,KEY_pd_id+ " = ? ",new String[]{"CCM"});
        // Delete All Rows
        //  db.delete(DATABASE_CCM_TBL, null, null);

        db.close();
    }

    public void delete_ac_table() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_ASSET_CATG,null,null);
        // Delete All Rows
        //  db.delete(DATABASE_CCM_TBL, null, null);

        db.close();
    }

    public void delete_bp_table() {
        SQLiteDatabase db = this.getWritableDatabase();


        // Delete All Rows
        db.delete(DATABASE_BLOCK_PROD, null, null);

        db.close();
    }

    public void delete_model_table() {
        SQLiteDatabase db = this.getWritableDatabase();


        // Delete All Rows
        db.delete(DATABASE_model, null, null);

        db.close();
    }

    public void delete_mlp_table() {
        SQLiteDatabase db = this.getWritableDatabase();


        // Delete All Rows
        db.delete(DATABASE_model_product, null, null);

        db.close();
    }

    public void delete_app_table() {
        SQLiteDatabase db = this.getWritableDatabase();


        // Delete All Rows
        db.delete(DATABASE_PROD_TABLE, KEY_pd_id + " = ? ", new String[]{"APP"});

        db.close();
    }
    public void delete_IMG_table() {
        SQLiteDatabase db = this.getWritableDatabase();


        // Delete All Rows
        db.delete(DATABASE_IMG_MASTER,IMAGE_tag + " = ? ", new String[]{"LOS_QDE_v2.0"});

        db.close();
    }

    public void delete_man_table() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(DATABASE_MANU_TBL, null, null);
        // Delete All Rows
        //  db.delete(DATABASE_CCM_TBL, null, null);

        db.close();
    }

    public long insert_prod(String prod, String pd_val, String pd_flag, String pd_id, String prod_catg,String start_date ,String end_date) {

        System.out.print("PROD:::" + prod);
        SQLiteDatabase db = this.getWritableDatabase();

        if (prod != "" && pd_val != "") {
            ContentValues values = new ContentValues();

            values.put(KEY_product, prod);
            values.put(KEY_pd_val, pd_val);
            values.put(KEY_pd_flag, pd_flag);
            values.put(KEY_pd_id, pd_id);
            values.put(KEY_pd_start, start_date);
            values.put(KEY_pd_end, end_date);

            values.put(KEY_prod_catg, prod_catg);
            long lid = db.insert(DATABASE_PROD_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_img_data(String img_id, String img_val, String img_quality, String img_ht, String img_wd,String img_ip,String img_qde_flag,String doc_type,String multiple_page) {

        System.out.print("PROD:::" + doc_type);
        SQLiteDatabase db = this.getWritableDatabase();

        if (img_id != "" && img_val != "") {
            ContentValues values = new ContentValues();

            values.put(IMAGE_ID, img_id);
            values.put(IMAGE_TITLE, img_val);
            values.put(IMAGE_resolution, img_quality);
            values.put(IMAGE_height, img_ht);
            values.put(IMAGE_width, img_wd);
            values.put(IMAGE_height, img_ht);
            values.put(IMAGE_tag, img_ip);
            values.put(IMAGE_QDE_flag, img_qde_flag);
            values.put(IMAGE_doc_type, doc_type);
            values.put(IMAGE_multiple_page, multiple_page);




            long lid = db.insert(DATABASE_IMG_MASTER, null, values);

            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_manu(String man_val, String man, String man_flag, String man_id,String start_date,String end_date) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (man != "" && man_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_man, man);
            values.put(KEY_man_val, man_val);
            values.put(KEY_man_flag, man_flag);
            values.put(KEY_man_id, man_id);
            values.put(KEY_man_start, start_date);
            values.put(KEY_man_end, end_date);


            long lid = db.insert(DATABASE_MANU_TBL, null, values);
            //db.close();
            // Inserting Row

            return lid;
        } else {
            return 0;
        }

    }

    public long insert_block(String id, String product, String block_flag, String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (id != "" && product != "") {

            System.out.println("zbcmzd::::" + id);
            ContentValues values = new ContentValues();
            values.put(KEY_block_id, id);
            values.put(KEY_block_product, product);
            values.put(KEY_block_flag, block_flag);
            values.put(KEY_block_idn, pd_id);
            long lid = db.insert(DATABASE_BLOCK_PROD, null, values);
            //db.close();
            // Inserting Row

            // System.out.println("zhbcjhz"+lid);

            return lid;
        } else {
            return 0;
        }

    }


    public long insert_catg(String id, String catg, String catg_block_id,String catg_flag, String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (id != "" && catg != "") {

            System.out.println("CATG BLK ID:::" + catg_block_id);
            ContentValues values = new ContentValues();
            values.put(KEY_catg_id, id);
            values.put(KEY_catg, catg);
            values.put(KEY_catg_block_id, catg_block_id);
            values.put(KEY_catg_flag, catg_flag);
            values.put(KEY_catg_idn, pd_id);
            long lid = db.insert(DATABASE_ASSET_CATG, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_model(String model_id, String model_no, String catg_id,String make, String make_model_flag,String model_flag,String idn,String man_id,String start_date,String end_date) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (model_id != "" && model_no != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_modelid, model_id);
            values.put(KEY_modelno, model_no);
            values.put(KEY_model_catg_id, catg_id);
            values.put(KEY_make, make);

            values.put(KEY_make_model_flag, make_model_flag);
            values.put(KEY_model_idn, idn);
            values.put(KEY_model_flag, model_flag);
            values.put(KEY_model_man_id, man_id);

            values.put(KEY_model_start, start_date);
            values.put(KEY_model_end, end_date);

            long lid = db.insert(DATABASE_model, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_model_prod(String model_id, String prod_code, String flag,String idn) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (model_id != "" && prod_code != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_model_product_id, model_id);
            values.put(KEY_model_product_code, prod_code);
            values.put(KEY_model_product_flag, flag);

            values.put(KEY_model_product_sidn, idn);

            long lid = db.insert(DATABASE_model_product, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }




    public long insert_cons(String cons, String cons_val, String cons_flag, String cons_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (cons != "" && cons_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_cons, cons);
            values.put(KEY_cons_val, cons_val);
            values.put(KEY_cons_flag, cons_flag);
            values.put(KEY_cons_id, cons_id);
            long lid = db.insert(DATABASE_CONSTI_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_cc(String cc, String cc_val, String cc_flag, String cc_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (cc != "" && cc_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_cc, cc);
            values.put(KEY_cc_val, cc_val);
            values.put(KEY_cc_flag, cc_flag);
            values.put(KEY_cc_id, cc_id);
            long lid = db.insert(DATABASE_CC_TBL, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_cr(String cr, String cr_val, String cr_flag, String cr_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (cr != "" && cr_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_cr, cr);
            values.put(KEY_cr_val, cr_val);
            values.put(KEY_cr_flag, cr_flag);
            values.put(KEY_cr_id, cr_id);
            long lid = db.insert(DATABASE_CR_TBL, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long insert_ccm(String ccm, String ccm_val, String ccm_flag, String ccm_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (ccm != "" && ccm_val != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_ccm, ccm);
            values.put(KEY_ccm_val, ccm_val);
            values.put(KEY_ccm_flag, ccm_flag);
            values.put(KEY_ccm_id, ccm_id);
            long lid = db.insert(DATABASE_CCM_TBL, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }


    public ArrayList<String> get_masters_pd( String master) {

        System.out.println("0th pos::::" + master);

        SQLiteDatabase db = this.getReadableDatabase();

        String product = null;
        arr_pd = new ArrayList<String>();
        //arr_pd.clear();
        switch(master){


            case "PD":
                System.out.println("inside pd::::" + master);
                // arr_pd.clear();
                arr_pd.add("Select Product");
                break;


            case "CONS":
                System.out.println("inside cons::::" + master);
                //arr_pd.clear();
                arr_pd.add("Select Constitution");
                break;


            case "CC":

                System.out.println("inside cc::::" + master);
                //arr_pd.clear();
                arr_pd.add("Select Customer Catagory");
                break;


            case "CR":
                //arr_pd.clear();
                arr_pd.add("Select Customer Co-App Relation");
                break;

            case "APP":
                //arr_pd.clear();
                arr_pd.add("");
                break;

        }

        Cursor cursor;
        if(master.equals("CC") || master.equals("CR")) {

            String countQuery = "SELECT " + KEY_product + " FROM "
                    + DATABASE_PROD_TABLE + " where " + KEY_pd_id + " = ?  and " + KEY_pd_start + " < datetime() and " + KEY_pd_end + " > datetime() order by " + KEY_product;
            cursor = db.rawQuery(countQuery, new String[]{master});
        }else{
            String countQuery = "SELECT " + KEY_product + " FROM "
                    + DATABASE_PROD_TABLE + " where " + KEY_pd_id + " = ?  order by " + KEY_product;
            cursor = db.rawQuery(countQuery, new String[]{master});
        }


        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    product = cursor.getString(0);
                    //  pd_val = cursor.getString(0);

                    arr_pd.add(product);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        System.out.println("ARRLIST:::1111" + arr_pd + ":::");
        return arr_pd;
        // return arr_pd_val;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_prod_catg() {

        SQLiteDatabase db = this.getReadableDatabase();

        arr_pd_val = new ArrayList<String>();
        String pd_catg = null;
        arr_pd_val.clear();
        arr_pd_val.add("");

        String countQuery = "SELECT " + KEY_prod_catg +" , "+KEY_product+ " FROM "
                + DATABASE_PROD_TABLE  +" where "+KEY_pd_id+" = ? order by "+KEY_product;

        Cursor cursor = db.rawQuery(countQuery, new String[]{"PD"});


        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_catg = cursor.getString(0);
                    //  pd_val = cursor.getString(0);

                    arr_pd_val.add(pd_catg);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_pd_val;
    }

    public ArrayList<String> get_masters_pd_val(String master) {

        SQLiteDatabase db = this.getReadableDatabase();

        arr_pd_val = new ArrayList<String>();
        String pd_val = null;

        switch(master){


            case "PD":
                System.out.println("inside pd::::" + master);
                // arr_pd.clear();
                arr_pd_val.add("");
                break;


            case "CONS":
                System.out.println("inside cons::::" + master);
                //arr_pd.clear();
                arr_pd_val.add("");
                break;


            case "CC":

                System.out.println("inside cc::::" + master);
                //arr_pd.clear();
                arr_pd_val.add("");
                break;


            case "CR":
                //arr_pd.clear();
                arr_pd_val.add("");
                break;


            case "APP":
                //arr_pd.clear();
                arr_pd_val.add("Select Applicant Type");
                break;
        }


        Cursor cursor;


        if( master.equals("CC") || master.equals("CR")){
            String countQuery = "SELECT " + KEY_pd_val +","+KEY_product+ " FROM "
                    + DATABASE_PROD_TABLE+ " where "+KEY_pd_id+" = ?  and " + KEY_pd_start + " < datetime() and " + KEY_pd_end + " > datetime() order by "+KEY_product ;
            cursor = db.rawQuery(countQuery,new String[]{master});



        }else{
            String countQuery = "SELECT " + KEY_pd_val +","+KEY_product+ " FROM "
                    + DATABASE_PROD_TABLE+ " where "+KEY_pd_id+" = ? order by "+KEY_product;
            cursor = db.rawQuery(countQuery,new String[]{master});


        }



        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    //  product = cursor.getString(0);
                    pd_val = cursor.getString(0);
                    arr_pd_val.add(pd_val);
                    //  arr_pd_val.add(pd_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        System.out.println("ARRLIST:::00" + arr_pd_val + ":::");
        return arr_pd_val;
        // return arr_pd_val;
        // System.out.println("countQuery:"+product_qnt);
    }

    public Cursor get_offine_entry() {

        SQLiteDatabase db = this.getReadableDatabase();
        String cons = null;
        //    arr_cons.add("Select Constituition");

        String countQuery = "SELECT * FROM "
                + DATABASE_QDE_TABLE+" where "+KEY_FLAG+"= 1 order by "+KEY_srno+" desc ";
        Cursor cursor = db.rawQuery(countQuery,null);

        System.out.println("countQuery:" + cursor);

        //  db.close();

        return cursor;
        // System.out.println("countQuery:"+product_qnt);
    }

    public boolean deletegridpic(String ts) {

        boolean chkFlag = false;



        SQLiteDatabase db = this.getWritableDatabase();
        chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_TIMESTAMP + "=" + " '"
                + ts + "'", null) > 0;
        db.close();




        // return true;
        return chkFlag;
    }
    public ArrayList<String> get_masters_cons_val() {

        SQLiteDatabase db = this.getReadableDatabase();
        String cons_val = null;
        arr_cons_val.add("");
        String countQuery = "SELECT " + KEY_cons_val + " FROM "
                + DATABASE_CONSTI_TABLE;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    cons_val = cursor.getString(0);

                    arr_cons_val.add(cons_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_cons_val;

    }

    public long insert_app_details(String str_ts,String str_fname,String str_mname,String str_lname,String str_app_type,String str_relation,String str_product,String str_catagory,
                                   String str_const,String gender,String str_dob,String str_drining_lic,String str_pan,String str_passport,String str_aadhar,String str_voter
            ,String str_asset_catg,String str_asset_make,String str_asset_model,String str_asset_cost,
                                   String str_margin_money,String str_loan_amt,String str_tenure, String str_consent_to_call,String asset_man,String str_age,String father_name,String mother_name){

        SQLiteDatabase db = this.getWritableDatabase();

        System.out.println("TS INSERT::::" + str_ts);
        ContentValues values = new ContentValues();
        values.put(KEY_TS, str_ts);
        values.put(KEY_fname, str_fname);
        values.put(KEY_mname, str_mname);
        values.put(KEY_lname, str_lname);
        values.put(KEY_app_type, str_app_type);
        values.put(KEY_app_relation, str_relation);
        values.put(KEY_product1, str_product);
        values.put(KEY_CATG, str_catagory);
        values.put(KEY_constn, str_const);
        values.put(KEY_gender, gender);
        values.put(KEY_dob, str_dob);
        values.put(KEY_driving_lic, str_drining_lic);
        values.put(KEY_pan_no, str_pan);
        values.put(KEY_pass_no, str_passport);
        values.put(KEY_aadhar_no, str_aadhar);
        values.put(KEY_voterid_no, str_voter);
        values.put(KEY_asset_catg, str_asset_catg);
        values.put(KEY_asset_make, str_asset_make);
        values.put(KEY_asset_model, str_asset_model);
        values.put(KEY_asset_cost, str_asset_cost);
        values.put(KEY_margin_money, str_margin_money);
        values.put(KEY_loan_amt, str_loan_amt);
        values.put(KEY_tenure_months, str_tenure);
        values.put(KEY_consent_tocall, str_consent_to_call);
        values.put(KEY_age, str_age);


        values.put(KEY_asset_manu, asset_man);

        values.put(KEY_father_name, father_name);

        values.put(KEY_mother_name, mother_name);

        values.put(KEY_FLAG, "0");

        long lid = db.insert(DATABASE_QDE_TABLE, null, values);
        //db.close();
        // Inserting Row
        return lid;

    }

    public boolean update_qde_off(String str_addr1_off,String str_addr2_off,String str_addr3_off,String str_city_off,String str_state__off,String str_pincode_off,String str_phone1_off,String str_phone2_off,String mobile_off1,String str_email_off,String str_distance_km_off,String str_landmar_off,String str_curr_years_off,String str_curr_months_off,String str_city_yrs_off,String str_mail_addr_off,String str_pincode_off1,String ts
            ,String str_std) {

        System.out.println("QDE TS ::::" + ts);
        mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();


        args.put(KEY_addr1_off, str_addr1_off);
        args.put(KEY_addr2_off, str_addr2_off);
        args.put(KEY_addr3_off, str_addr3_off);
        args.put(KEY_city_off, str_city_off);
        args.put(KEY_state_off, str_state__off);
        args.put(KEY_phone1_off, str_phone1_off);
        args.put(KEY_phone2_off, str_phone2_off);
        args.put(KEY_mobile_off, mobile_off1);
        args.put(KEY_email_off, str_email_off);
        args.put(KEY_distance_km_off, str_distance_km_off);
        args.put(KEY_landmark_off, str_landmar_off);
        args.put(KEY_curr_years_off, str_curr_years_off);
        args.put(KEY_city_years_off, str_city_yrs_off);
        args.put(KEY_mail_addr_off, str_mail_addr_off);
        args.put(KEY_pincode_off, str_pincode_off1);
        args.put(KEY_TS, ts);


        args.put(KEY_std_off, str_std);


        Boolean res = false;

        res = mDB.update(DATABASE_QDE_TABLE, args, KEY_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income:::" + res);

        return res;
    }


    public boolean update_qde_resi(String str_addr1,String str_addr2,String str_addr3,String str_city,String str_state,String str_phone1,String str_phone2,
                                   String str_mobile,String str_email,String str_distance_km,String str_landmark,String str_curr_years,
                                   String str_city_yrs,String str_mail_addr,String ts,String str_pincode,String str_std,String cust_id) {

        System.out.println("QDE TS ::::" + str_ts);
        mDB = this.getWritableDatabase();
        ContentValues args = new ContentValues();

        args.put(KEY_addr1, str_addr1);
        args.put(KEY_addr2, str_addr2);
        args.put(KEY_addr3, str_addr3);
        args.put(KEY_city, str_city);
        args.put(KEY_state, str_state);
        args.put(KEY_phone1, str_phone1);
        args.put(KEY_phone2, str_phone2);
        args.put(KEY_mobile, str_mobile);
        args.put(KEY_email, str_email);
        args.put(KEY_distance_km, str_distance_km);
        args.put(KEY_landmark, str_landmark);
        args.put(KEY_curr_years, str_curr_years);
        args.put(KEY_city_years, str_city_yrs);
        args.put(KEY_mail_addr, str_mail_addr);
        args.put(KEY_pincode, str_pincode);
        args.put(KEY_std,str_std);


        args.put(KEY_cust_id, cust_id);

        Boolean res = false;

        res = mDB.update(DATABASE_QDE_TABLE, args, KEY_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income:::" + res);

        return res;
    }

    public boolean update_qde_off(String off_entry,String ts, String flag) {

        System.out.println("QDE TS1111 ::::" + str_ts);
        mDB = this.getWritableDatabase();

        ContentValues args = new ContentValues();

        args.put(KEY_off, off_entry);
        args.put(KEY_FLAG, flag);


        Boolean res = false;

        res = mDB.update(DATABASE_QDE_TABLE, args, KEY_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income111:::" + res);

        return true;
    }

    public Cursor getImages_new(String img_sr_no,String img_ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_SR_NO + "= ? AND " + IMAGE_TIMESTAMP + " = ?  order by " + IMAGE_COLUMN_ID + " desc limit 1", new String[]{img_sr_no,img_ts});

        return res;
    }

    public Cursor getImages_new_ts(String img_ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT  * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TIMESTAMP +"=?", new String[]{img_ts});
        return res;
    }
    public boolean update_emp_details(String das,String emp_name, String others,String emp_type,String designation,String emp_code,
                                      String emp_branch,String ts,String branch_id,String lat,String longs,String dist_type,String dist_km,String map_addr) {


        mDB = this.getWritableDatabase();

        ContentValues args = new ContentValues();

        args.put(KEY_DAS, das);

        args.put(KEY_emp_name, emp_name);
        args.put(KEY_others, others);
        args.put(KEY_emp_type, emp_type);
        args.put(KEY_designation, designation);
        args.put(KEY_emp_code, emp_code);
        args.put(KEY_emp_branch, emp_branch);
        args.put(KEY_emp_branch_id, branch_id);

        args.put(KEY_lats, lat);
        args.put(KEY_longs, longs);
        args.put(KEY_dist_type, dist_type);
        args.put(KEY_distance, dist_km);

        args.put(KEY_map_addr, map_addr);


        Boolean res = false;

        res = mDB.update(DATABASE_QDE_TABLE, args, KEY_TS + " = ? ", new String[]{ts}) > 0;

        System.out.println("income111:::" + res);

        return true;
    }


    public String get_QDE_request(String ts,String pincode,String city,String state,String pincode_off,String state_off,String city_off,Boolean dash) {

        //System.out.println("TS CUST::::" + ts);
        Cursor cursor;
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;
        if (ts != null) {

            countQuery = "SELECT *  FROM " + DATABASE_QDE_TABLE + " WHERE " + KEY_FLAG + " = 1  AND " + KEY_TS + " = ? LIMIT 1" ;
            cursor = db.rawQuery(countQuery,  new String[]{ts});
        } else {
            System.out.println("TS NULL:::");


            countQuery = "SELECT *  FROM " + DATABASE_QDE_TABLE + " WHERE "
                    + KEY_FLAG + "= 1 LIMIT 1";
            cursor = db.rawQuery(countQuery,null);
        }





        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    row_id = cursor.getString(0);
                    str_fname = cursor.getString(1);
                    str_mname = cursor.getString(2);
                    str_lname = cursor.getString(3);
                    str_app_type = cursor.getString(4);
                    str_relation= cursor.getString(5);
                    str_product = cursor.getString(6);
                    str_catagory = cursor.getString(7);
                    str_const = cursor.getString(8);
                    gender = cursor.getString(9);
                    str_dob = cursor.getString(10);
                    str_drining_lic = cursor.getString(11);
                    str_pan = cursor.getString(12);
                    str_passport = cursor.getString(13);
                    str_aadhar = cursor.getString(14);
                    str_voter = cursor.getString(15);
                    str_asset_catg = cursor.getString(16);
                    str_asset_make = cursor.getString(17);
                    str_asset_model = cursor.getString(18);
                    str_asset_cost = cursor.getString(19);
                    str_margin_money = cursor.getString(20);
                    str_loan_amt = cursor.getString(21);
                    str_tenure = cursor.getString(22);
                    str_consent_to_call = cursor.getString(23);
                    str_addr1 = cursor.getString(24);
                    str_addr2 = cursor.getString(25);
                    str_addr3 = cursor.getString(26);

                    str_phone1 = cursor.getString(29);
                    str_phone2 = cursor.getString(30);
                    str_mobile = cursor.getString(31);
                    str_email = cursor.getString(32);
                    str_distance_km = cursor.getString(33);
                    str_landmark = cursor.getString(34);
                    str_curr_years = cursor.getString(35);
                    str_city_yrs = cursor.getString(36);
                    str_mail_addr = cursor.getString(37);

                    str_addr1_off = cursor.getString(38);
                    str_addr2_off = cursor.getString(39);
                    str_addr3_off = cursor.getString(40);

                    str_phone1_off = cursor.getString(43);
                    str_phone2_off = cursor.getString(44);
                    str_mobile_off = cursor.getString(45);
                    str_email_off = cursor.getString(46);
                    str_distance_km_off = cursor.getString(47);
                    str_landmar_off= cursor.getString(48);
                    str_curr_years_off = cursor.getString(49);
                    str_city_yrs_off = cursor.getString(50);
                    str_mail_addr_off = cursor.getString(51);
                    str_entry_type= cursor.getString(52);
                    str_ts = cursor.getString(53);

                    str_das= cursor.getString(57);
                    str_emp_name= cursor.getString(58);
                    str_others= cursor.getString(59);
                    str_emp_type= cursor.getString(60);
                    str_designation= cursor.getString(61);
                    str_emp_code= cursor.getString(62);
                    str_emp_branch= cursor.getString(63);


                    str_app_id= cursor.getString(64);
                    str_branch_id= cursor.getString(65);
                    str_asset_man= cursor.getString(66);
                    str_std_resi= cursor.getString(67);
                    str_std_off= cursor.getString(68);

                    str_cust_id= cursor.getString(69);

                    str_lats= cursor.getString(70);
                    str_longs= cursor.getString(71);
                    str_dist_type= cursor.getString(72);
                    str_dist_km= cursor.getString(73);

                    str_map_addr= cursor.getString(74);

                    str_age= cursor.getString(75);
                    img_count= cursor.getString(76);
                    str_father_name= cursor.getString(77);
                    str_mother_name= cursor.getString(78);

                }

                if(city!=null && city_off!=null ){

                    str_city =city;
                    str_state = state;
                    str_pincode = pincode;

                    str_pincode_off=pincode_off;
                    str_city_off=city_off;
                    str_state__off=state_off;
                }else {

                    str_city = cursor.getString(27);
                    str_state = cursor.getString(28);

                    str_city_off=cursor.getString(41);
                    str_state__off=cursor.getString(42);

                    str_pincode=cursor.getString(55);
                    str_pincode_off=cursor.getString(56);
                }


                cursor.close();

                System.out.println("ROW ID ::::" + row_id + "TSS::::" + str_ts+":::PAN:::"+str_pan);

                if (str_ts == null || str_ts.equals("") || str_ts.equals("null")) {

                }else {
                    if (!cd.isConnectingToInternet()) {
                        userFunction.cutomToast("No internet connection...", context);
                    }
                    else {
                        new InsertQDEData().execute(dash);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public class InsertQDEData extends AsyncTask<Boolean, Boolean, Boolean> {

        Boolean param;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }


        @Override
        protected Boolean doInBackground(Boolean... params) {
            // then do your work
            //   String str_pic_count = DBHelper.getCasePhotoCount(cu_ts);

            param=params[0];
            jsonobject = userFunction.InsertData(str_fname ,
                    str_mname ,
                    str_lname ,
                    str_app_type ,
                    str_relation,
                    str_product,
                    str_catagory ,
                    str_const,
                    gender,
                    str_dob,
                    str_drining_lic ,
                    str_pan ,
                    str_passport,
                    str_aadhar ,
                    str_voter ,
                    str_asset_catg ,
                    str_asset_make ,
                    str_asset_model,
                    str_asset_cost,
                    str_margin_money ,
                    str_loan_amt ,

                    str_tenure ,
                    str_consent_to_call ,
                    str_addr1 ,
                    str_addr2 ,
                    str_addr3,
                    str_city ,
                    str_state ,
                    str_phone1 ,
                    str_phone2 ,
                    str_mobile,
                    str_email ,
                    str_distance_km ,
                    str_landmark,
                    str_curr_years,
                    str_city_yrs,
                    str_mail_addr ,
                    str_ts ,
                    str_addr1_off,str_addr2_off,str_addr3_off,str_city_off,str_state__off,str_phone1_off,str_phone2_off,str_mobile_off,str_email_off,str_distance_km_off,str_landmar_off,str_curr_years_off,str_city_yrs_off,str_mail_addr_off,str_das,str_emp_name,str_others,str_emp_type,str_designation,str_emp_code,str_emp_branch,
                    str_app_id,str_branch_id,str_std_resi,str_std_off,str_pincode,str_pincode_off,str_asset_man,str_cust_id,str_lats,str_longs,str_dist_type,str_dist_km,str_map_addr,str_age,img_count,str_father_name,str_mother_name);

            return null;
        }

        @Override
        protected void onPostExecute(Boolean args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200


                                && resp_success.equals("true")) {

                            deleteCustomerInfo(row_id);
                            userFunction
                                    .cutomToast("QDE Data Successfully uploaded..", context);

                            SharedPreferences.Editor peditor = pref.edit();

                            int int_cust_pending_count;
                            int_cust_pending_count = Integer.parseInt(getCustomerPendingCount());

                            peditor.putString("qde_off_count", String.valueOf(int_cust_pending_count));

                            if (param == true) {
                                if(str_app_type.equals("P")){
                                    Boolean status_co = update_status(str_ts, 2 , null);
                                    if (status_co == true) {

                                        SharedPreferences.Editor peditor1 = pref.edit();
                                        peditor1.putString("cust_id_appl", str_ts);

                                        // peditor.putString("ts",  pref.getString("ts", null));
                                        peditor1.commit();
                                    }

                                }


                                if (getPending_cases(str_cust_id)==true) {
                                    Intent messagingActivity = new Intent(context,
                                            Offine_Entries.class);
                                    context.startActivity(messagingActivity);
                                } else {



                                    Intent messagingActivity = new Intent(context,
                                            FragResult.class);

                                    messagingActivity.putExtra("app_id", str_cust_id);
                                    messagingActivity.putExtra("cust_id", str_ts);

                                    context.startActivity(messagingActivity);

                                }

                            }

                            // peditor.putString("ts",  pref.getString("ts", null));
                            peditor.commit();

                            mProgressDialog.dismiss();
                            //  callPopup_branch();




                        }
                    }
                } else {

                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteCustomerInfo(String key_row_id) {
        Log.d("DELETING", key_row_id);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_QDE_TABLE, KEY_srno + "=" + " '" + key_row_id
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public boolean deleteAll() {

        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_QDE_TABLE,null,null ) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }
    public String getPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_TS + ") FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_FLAG + "= 1 LIMIT 1" ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }

            System.out.println("request countt::::" + pending_request_count);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }


    public Boolean getPending_cases(String ts) {

        Boolean case_pending=false;

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_TS + ") FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_cust_id + "= ? LIMIT 1" ;
        Cursor cursor = db.rawQuery(countQuery,new  String[]{ts});

        System.out.println("countQuery:" + cursor);
        String pending_case_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_case_count = cursor.getString(0);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }

            Integer case_pending_int= Integer.parseInt(pending_case_count);

            if(case_pending_int>0){
                case_pending=true;
            }else {
                case_pending=false;
            }

            System.out.println("request countt::::" + case_pending_int);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return case_pending;
    }


    public ArrayList get_address_details( String ts) {

        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<String> adrr=new ArrayList<String>();

        String countQuery = "SELECT "  + KEY_city + " , "+KEY_city_off+" FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_TS + "= ? LIMIT 1" ;
        Cursor cursor = db.rawQuery(countQuery,new String[] {ts});

        System.out.println("countQuery:" + cursor);
        String pincode = "";
        String pincode_off = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pincode = cursor.getString(0);
                    pincode_off = cursor.getString(1);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }

            adrr.add(pincode);
            adrr.add(pincode_off);
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return
                adrr;
    }


    public String get_resi_info( String ts) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT "+ KEY_email + " FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_TS + "= ? " ;
        Cursor cursor = db.rawQuery(countQuery, new String[] {ts});

        System.out.println("countQuery:" + cursor);
        String resi_email = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    resi_email = cursor.getString(0);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }

            System.out.println("request countt::::" + resi_email);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return resi_email;
    }


    public String get_off_info( String ts) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT "+ KEY_email_off + " FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_TS + "= ? " ;
        Cursor cursor = db.rawQuery(countQuery, new String[]{ts});

        System.out.println("countQuery:" + cursor);
        String off_email = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    off_email = cursor.getString(0);
                    //  return cursor.getString(27);
                }
                cursor.close();

            }

            System.out.println("request countt::::" + off_email);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return off_email;
    }



    private void callPopup_branch() {


        // custom dialog
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        // dialog.setContentView(R.layout.qde_pop_up_branch);
        dialog.setTitle("Add Co-app/ Guarantor ?");


        dialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                System.out.println("APP TYPE::::" + str_app_type);
                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("app_type", "C");

                // peditor.putString("ts",  pref.getString("ts", null));
                peditor.commit();
                Intent pd_form = new Intent(context,MainActivity.class);
                context.startActivity(pd_form);
                dialog.dismiss();

            }
        });


        dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("app_type", "P");
                peditor.commit();

                if (str_entry_type.equals("true")) {
                    Intent messagingActivity = new Intent(context,
                            Offine_Entries.class);
                    context.startActivity(messagingActivity);
                } else {
                    Intent messagingActivity = new Intent(context,
                            HdbHome.class);
                    context.startActivity(messagingActivity);
                }
                dialog.dismiss();

            }
        });


        dialog.setMessage("Do you want to apply for co-app/Guarantor ?...");

        dialog.show();




    }


    public String getCustomerPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_app_id + ") FROM "
                + DATABASE_QDE_TABLE + " WHERE " + KEY_FLAG + "= 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }


    public Cursor get_pincodes( String ts) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT "+ KEY_pincode +","+KEY_city+","+KEY_state+","+KEY_pincode_off+","+KEY_city_off+","+KEY_state_off+ " FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_TS + "= ? " ;
        Cursor cursor = db.rawQuery(countQuery, new String[] {ts});

        System.out.println("countQuery:" + cursor);
        String resi_pincode,resi_city = "",resi_state="",off_pincode,off_city = "",off_state="";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {

                    //  return cursor.getString(27);
                }


            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            db.close();
        }
        return cursor;
    }



    public Cursor get_applicant_details( String cust_id) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT "+ KEY_product1 +","+KEY_CATG+","+KEY_asset_catg +","+KEY_asset_manu+","+KEY_asset_make +","+KEY_asset_model +","+KEY_asset_make+" , "+KEY_asset_cost+","+KEY_margin_money+","+KEY_loan_amt+","+KEY_tenure_months +" FROM " + DATABASE_QDE_TABLE +" WHERE " + KEY_cust_id + "= ? " ;
        Cursor cursor = db.rawQuery(countQuery, new String[] {cust_id});

        System.out.println("countQuery:" + cursor);
        String resi_pincode,resi_city = "",resi_state="",off_pincode,off_city = "",off_state="";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {

                    //  return cursor.getString(27);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            db.close();
        }
        return cursor;
    }


    public ArrayList get_values( String prod,String cust_catgt,String asset_catg,String asset_man,String asset_model,String aset_make) {


        ArrayList<String> arr_values;
        arr_values=new ArrayList<>();


        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT "+ KEY_product + " FROM " + DATABASE_PROD_TABLE +" WHERE " + KEY_pd_val + "= ? and "+KEY_pd_id +"=?";
        Cursor cursor = db.rawQuery(countQuery, new String[] {prod,"PD"});


        try {

            if (cursor != null) {

                if (cursor.moveToNext()) {

                    arr_values.add(cursor.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        SQLiteDatabase db1 = this.getReadableDatabase();

        String countQuery1 = "SELECT "+ KEY_product + " FROM " + DATABASE_PROD_TABLE +" WHERE " + KEY_pd_val + "= ? and "+KEY_pd_id +"=?" ;
        Cursor cursor1 = db1.rawQuery(countQuery1, new String[] {cust_catgt,"CC"});



        try {

            if (cursor1 != null) {

                if (cursor1.moveToNext()) {

                    arr_values.add(cursor1.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }


        SQLiteDatabase db2 = this.getReadableDatabase();

        String countQuery2 = "SELECT "+ KEY_catg + " FROM " + DATABASE_ASSET_CATG +" WHERE " + KEY_catg_id + "= ? " ;
        Cursor cursor2 = db2.rawQuery(countQuery2, new String[] {asset_catg});


        try {

            if (cursor2 != null) {

                if (cursor2.moveToNext()) {

                    arr_values.add(cursor2.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }


        SQLiteDatabase db3 = this.getReadableDatabase();

        String countQuery3 = "SELECT "+ KEY_man + " FROM " + DATABASE_MANU_TBL +" WHERE " + KEY_man_val + "= ? " ;
        Cursor cursor3 = db3.rawQuery(countQuery3, new String[] {asset_man});


        try {

            if (cursor3 != null) {

                if (cursor3.moveToNext()) {

                    arr_values.add(cursor3.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        SQLiteDatabase db4 = this.getReadableDatabase();

        String countQuery4 = "SELECT "+ KEY_modelno + " FROM " + DATABASE_model +" WHERE " + KEY_modelid + "= ? " ;
        Cursor cursor4 = db4.rawQuery(countQuery4, new String[] {asset_model});


        try {

            if (cursor4 != null) {

                if (cursor4.moveToNext()) {

                    arr_values.add(cursor4.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        SQLiteDatabase db5 = this.getReadableDatabase();

        String countQuery5 = "SELECT "+ KEY_make + " FROM " + DATABASE_model +" WHERE " + KEY_modelid + "= ? " ;
        Cursor cursor5 = db5.rawQuery(countQuery5, new String[] {asset_model});


        try {

            if (cursor5 != null) {

                if (cursor5.moveToNext()) {

                    arr_values.add(cursor5.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        SQLiteDatabase db6 = this.getReadableDatabase();

        String countQuery6 = "SELECT "+ KEY_make + " FROM " + DATABASE_model +" WHERE " + KEY_modelid + "= ? " ;
        Cursor cursor6 = db6.rawQuery(countQuery6, new String[] {aset_make});


        try {

            if (cursor6 != null) {

                if (cursor6.moveToNext()) {

                    arr_values.add(cursor6.getString(0));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }

        return arr_values;
    }



    public boolean insertImage(String path, String losid, String userid, String ts, String status, String image_cation,String image_app_type,String image_no,String image_sr_no,String image_count,String image_timestamp) {
        System.out.println("UPDT IMG TSpath:1::" + path);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        System.out.println("UPDT IMG TSpath:2::" + path);

        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);
        contentValues.put(IMAGE_APP_TYPE, image_app_type);
        contentValues.put(IMAGE_NO, image_no);
        contentValues.put(IMAGE_SR_NO, image_sr_no);
        contentValues.put(IMAGE_COUNT, image_count);
        contentValues.put(IMAGE_TIMESTAMP, image_timestamp);


        System.out.println("UPDT IMG TSpath:3::" + path);

        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        System.out.println("UPDT IMG TSpath::4:" + path);

        return true;
    }


    public boolean insertImage_new(String path, String losid, String userid, String ts, String status, String image_cation,String image_app_type,String image_no,String image_sr_no,String image_count,String image_timestamp) {
        System.out.println("UPDT IMG TSpath:1::" + path);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        System.out.println("UPDT IMG TSpath:2::" + path);

        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);
        contentValues.put(IMAGE_APP_TYPE, image_app_type);
        contentValues.put(IMAGE_NO, image_no);
        contentValues.put(IMAGE_SR_NO, image_sr_no);
        contentValues.put(IMAGE_COUNT, image_count);
        contentValues.put(IMAGE_TIMESTAMP, image_timestamp);


        System.out.println("UPDT IMG TSpath:3::" + path);

        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        System.out.println("UPDT IMG TSpath::4:" + path);

        return true;
    }


    public Cursor getImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"2"});

        return res;
    }

    public Cursor getImages_applicant(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + ">? and "+IMAGE_TS+"=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0",ts});

        return res;
    }
    public Cursor getImagests(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + ">? AND  "+ IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0",ts});

        return res;
    }


    /*
    public int upload_image(String img_path) {
        int resp_code=0;

        System.out.println("img_path::1212121::::" + img_path);
        if (img_path != null) {
            //	Log.d("uploading File : ", img_path);
            //  String new_img_path="/storage/emulated/0/PD/"+img_path+".jpg";
            // System.out.println("new_img_path::::::"+new_img_path);
            File file1 = new File(img_path);
            if (file1.exists()) {
                // Log.d("uploading File Esists : ", "true");
                System.out.println("img_path::::11222" + img_path);
                if (cd.isConnectingToInternet()) {

                    String BASE_URL = BuildConfig.SERVER_URL;
                   // BASE_URL=BASE_URL+"/HDB_QDE_UAT/index_get.php?target=img&action=upload";

                    Log.d(TAG, "Filename " + file1.getName());
                    //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file1);
                    MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file1.getName(), mFile);
                    RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file1.getName());
                    RequestBody target = RequestBody.create(MediaType.parse("text/plain"), "img");
                    RequestBody action = RequestBody.create(MediaType.parse("text/plain"), "upload");
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
                    System.out.println("fileToUpload::::::"+fileToUpload);
                    System.out.println("fileToUpload::::filename:::::"+filename);
                    Call<UploadObject > fileUpload = uploadImage.uploadFile(fileToUpload,"sad","tst","res","test");


                    fileUpload.enqueue(new Callback<UploadObject>() {
                        @Override
                        public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                            System.out.println("success!!!!" +response.body().getSuccess());
                            System.out.println("success!!status!!" +response.body().getSthhatus());

                            //  Toast.makeText(this, "Response " + response.raw().message(), Toast.LENGTH_LONG).show();
                            // Toast.makeText(DBHelper.this, "Success " + response.body().getSuccess(), Toast.LENGTH_LONG).show();
                        }
                        @Override
                        public void onFailure(Call<UploadObject> call, Throwable t) {

                             Log.d(TAG, "Error " + t.getMessage());
                            System.out.println("failure!!!!");
                        }

                    });

                } else {
                    userFunction.cutomToast("No internet connection...", context);
                }
            } else {
                //  System.out.println("img_path::::8877" + new_img_path);

                this.deletepic(img_path);
                this.deletepic(img_path);

            }
        } else {
            try {
                if (Integer.parseInt(this.getPendingPhotoCount()) == 0) {

                    File mediaStorageDir;
                    String extStorageDirectory = Environment
                            .getExternalStorageDirectory()
                            .toString();

                    // External sdcard location
                    mediaStorageDir = new File(
                            extStorageDirectory
                                    + IMAGE_DIRECTORY_NAME);

                    if (mediaStorageDir.isDirectory()) {
                        try {
                            String[] children = mediaStorageDir
                                    .list();
                            for (int i = 0; i < children.length; i++) {
                                new File(mediaStorageDir, children[i]).delete();
                            }

                        } catch (Exception e) {
                            // block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resp_code;
    }
*/
    public int doFileUpload(final String selectedFilePath) {
        File file1 = new File(selectedFilePath);
        String BASE_URL = "https://hdbapp.hdbfs.com";
        //private static String URL_POST = "https://hdbapp.hdbfs.com/HDB_QDE/index_post.php";
        int serverResponseCode = 0;
        String response = "";

        HttpsURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length - 1];

        if (!selectedFile.isFile()) {
            dialog.dismiss();

            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(BASE_URL+"/HDB_QDE_v2.6/uploadimg.php");
                connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploadedfile1", selectedFilePath);


                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile1\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0) {
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();
                System.out.println("serverResponseMessage::" + serverResponseMessage);


                if (serverResponseCode == 200) {
                    String line;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "iso-8859-1"));
                    while ((line = bufferedReader.readLine()) != null) {
                        response += line;
                    }
                }

                if (response != null) {

                    try {

                        json = response.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting qde_result " + e.toString());
                    }
                    // try parse the string to a JSON object
                    try {
                        jObj = new JSONObject(json);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                } else {
                    jObj = null;
                }


                if (jObj != null) {
                    try {
                        System.out.println("response_str::::" + jObj);
                        if (jObj.getString(KEY_STATUS) != null) {
                            String res = jObj.getString(KEY_STATUS);
                            String resp_success = jObj.getString(KEY_SUCCESS);
                            if (Integer.parseInt(res) == 200
                                    && resp_success.equals("true")) {
                                deletepic(selectedFilePath);
                                file1.delete();
                            } else {
                                if(Integer.parseInt(res) == 200
                                        && resp_success.equals("false")){


                                }else {
                                    serverResponseCode=3;

                                    Log.i("RESPONSE MESSAGE ", jObj.getString("message").toString());
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();


            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(context, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            //dialog.dismiss();
            return serverResponseCode;
        }
    }

    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_PATH + "=" + " '"
                    + path + "'", null) > 0;
            db.close();


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }

    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + IMAGE_PATH + ") FROM "
                + IMAGE_TABLE_NAME ;
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }



    public ArrayList<String> get_img_list_app_id(String app_type) {

        SQLiteDatabase db = this.getReadableDatabase();

        arr_pd_val = new ArrayList<String>();
        String pd_catg = null;
        arr_pd_val.clear();
        arr_pd_val.add("");
        String countQuery;
        Cursor cursor;

             countQuery = "SELECT  " + IMAGE_ID + " FROM "
                    + DATABASE_IMG_MASTER ;
             cursor = db.rawQuery(countQuery,null);




        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_catg = cursor.getString(0);
                    //  pd_val = cursor.getString(0);

                    arr_pd_val.add(pd_catg);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_pd_val;
    }


    public ArrayList<String> get_img_list_app_val(String app_type) {

        SQLiteDatabase db = this.getReadableDatabase();

        arr_pd_val = new ArrayList<String>();
        String pd_catg = null;
        arr_pd_val.clear();
        arr_pd_val.add("Select Doc Type ");
        String countQuery;
        Cursor cursor;


             countQuery = "SELECT  " + IMAGE_TITLE + " FROM "
                    + DATABASE_IMG_MASTER ;

             cursor = db.rawQuery(countQuery,  null);





        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_catg = cursor.getString(0);
                    //  pd_val = cursor.getString(0);
                    System.out.println("AL items:::" + pd_catg);

                    arr_pd_val.add(pd_catg);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_pd_val;
    }

    public ArrayList<String> get_img_doc_type(String app_type) {

        SQLiteDatabase db = this.getReadableDatabase();

        arr_doc_type = new ArrayList<String>();
        String pd_catg = null;
        arr_doc_type.clear();
        arr_doc_type.add("");
        String countQuery;
        Cursor cursor;


        countQuery = "SELECT  " + IMAGE_doc_type + " FROM "
                + DATABASE_IMG_MASTER ;

        cursor = db.rawQuery(countQuery,  null);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_catg = cursor.getString(0);
                    //  pd_val = cursor.getString(0);
                    System.out.println("AL items:::" + pd_catg);

                    arr_doc_type.add(pd_catg);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_doc_type;
    }

    public HashMap<String, ArrayList<String>> get_img_details(String app_type) {
        HashMap<String,ArrayList<String> > params = new HashMap<String,ArrayList<String>>();

        SQLiteDatabase db = this.getReadableDatabase();


        ArrayList<String> al_ht;
        ArrayList<String> al_wd;
        ArrayList<String> al_rsl;
        ArrayList<String> al_multiple;


        al_ht=new ArrayList<>();
        al_wd=new ArrayList<>();
        al_rsl=new ArrayList<>();

        al_multiple=new ArrayList<>();

        String ht = null,wd = null,rsl = null,str_multiple=null;

        String countQuery;
        Cursor cursor;


        countQuery = "SELECT  " + IMAGE_height +","+IMAGE_width+","+IMAGE_resolution+","+IMAGE_multiple_page+ " FROM "
                + DATABASE_IMG_MASTER ;

        cursor = db.rawQuery(countQuery,  null);





        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    ht = cursor.getString(0);
                    wd = cursor.getString(1);
                    rsl = cursor.getString(2);
                    str_multiple= cursor.getString(3);
                    //  pd_val = cursor.getString(0);

                    System.out.println("ht::::"+ht+":::::wd:::"+wd+"::::rsl::::"+rsl+":::::str_multiple::::"+str_multiple);

                    al_ht.add(ht);
                    al_wd.add(wd);
                    al_rsl.add(rsl);
                    al_multiple.add(str_multiple);
                    //  arr_pd_val.add(pd_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        params.put("al_ht",al_ht);
        params.put("al_wd",al_wd);
        params.put("al_rsl",al_rsl);
        params.put("al_multiple",al_multiple);

        return params;
    }



    public Cursor getAllImages(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{ts});

        return res;
    }

    public Cursor getAllImages_new(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        // Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{ts});
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + "=? AND  "+ IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0",ts});

        return res;
    }


    public Cursor get_img_app( String img_flag) {
        SQLiteDatabase db8 = this.getReadableDatabase();

        String countQuery8 = "SELECT " + IMAGE_ID + " , " + IMAGE_TITLE + "  FROM "
                + DATABASE_IMG_MASTER + " where "+IMAGE_FLAG+"=? ";



        //   String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + "  FROM "
        //           + DATABASE_BLOCK_PROD + " where "+KEY_block_id+"= ?";


        //  String countQuery8 = "SELECT " + KEY_block_product + " , " + KEY_block_id + " FROM " + DATABASE_BLOCK_PROD +" WHERE " + KEY_block_id + "= 1.0 " ;
        // Cursor cursor8 = db8.rawQuery(countQuery8, null);

        Cursor cursor8 = db8.rawQuery(countQuery8, new String[]{img_flag});
        //Cursor cursor8 = db8.rawQuery(countQuery8,new String[]{"1.0"});

        System.out.println("countQuery:" + countQuery8);
        //   System.out.println("countQuery::::" + cursor8.getString(0));


        return cursor8;

    }

    public boolean delete_IMGo(String ts) {
        System.out.println("tsssss1111::::" + ts);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            if(ts!=null) {
                if (!ts.equals(null)) {
                    Log.d("DELETING IMG", ts);

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_TS + "=" + " '" + ts
                            + "'", null) > 0;
                    db.close();
                } else {

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                    db.close();

                }
            }else {

                SQLiteDatabase db = this.getWritableDatabase();
                chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                db.close();

            }



        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
System.out.println("sttus::::"+chkFlag);
        // return true;
        return chkFlag;
    }


    public boolean delete_master() {

        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");


                    Log.d("DELETING IMG MASTER","ms");

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(DATABASE_IMG_MASTER, null, null) > 0;
                    db.close();





        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("sttus::::"+chkFlag);
        // return true;
        return chkFlag;
    }

    public boolean update_status(String ts, int status, String pic_count) {
        mDB = this.getReadableDatabase();
        ContentValues args = new ContentValues();
        args.put(IMAGE_STATUS, status);
        Boolean res = false;

        res = mDB.update(IMAGE_TABLE_NAME, args,  IMAGE_TS + " = ? ", new String[]{ts}) > 0;


        return res;
        /*
        String update_query="Update "+DATABASE_CUSTOMER_TABLE+ " set "+KEY_STATUS+" =1  where "+KEY_TS+ " = "+ts;
       return mDB.rawQuery(update_query,null);
        System.out.println("Update query::::"+update_query);
        */

    }


}

