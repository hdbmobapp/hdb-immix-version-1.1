package com.example.hdb_los_prod;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.Home_PD_COLLX;
import com.example.hdb.R;
import com.example.hdb_los_prod.img_upload_retrofit.ImageListAll;
import com.example.hdb_los_prod.libraries.DBHelper;
import com.example.hdb_los_prod.libraries.MastersListAdapter;
import com.example.hdb_los_prod.libraries.UserFunction;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.DatabaseHandler;


public class HdbHome extends Activity {

    JSONObject jsonobject = null;

    ListView mList;
    MastersListAdapter ms_adapter;

    UserFunction userFunction;
    public static final String KEY_STATUS = "status";
    String str_user_id, str_user_name, str_branch;
    public static String msg_count = "0";
    TextView txt_user_id, txt_user_name, txt_branch;
    SharedPreferences pref;
    ListView list;
    String[] itemname = {"Data Entry", "Pincode Verification", "My Uploads", "Image Upload","Message Box","Update Contact", "Contact Us"};
    Integer[] imgid = {R.drawable.data_entry, R.drawable.upload, R.drawable.efficiancy, R.drawable.img_upld, R.drawable.mail_box, R.drawable.user_details_icon, R.drawable.contact_ud};

    String[] itemname_co = {"Data Entry", "Pincode Verification", "My Uploads", "Image Upload","Message Box","Update Contact", "Contact Us"};
    Integer[] imgid_co = {R.drawable.data_entry, R.drawable.upload, R.drawable.efficiancy,  R.drawable.img_upld,R.drawable.mail_box, R.drawable.user_details_icon, R.drawable.contact_ud};

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";


    ProgressDialog mProgressDialog;
    Typeface font;
    ConnectionDetector cd;
    HdbHomeListAdapter adapter;
    String str_designation;
    Integer is_login, off_is_login;
    String str_designation_val;
    public static String str_total;
    DBHelper rDB;

    String res, resp_success;

    String flag_cons, id_cons, flag_pd, id_pd;

    int total_int = 0;

    DatabaseHandler db;
    HashMap<String, String> user = new HashMap<String, String>();
    private String android_id;

    Integer off_rowcount;

    ArrayList<String> check_master;

    ArrayList<String> master_flag;

    ArrayList<String> al_cons, al_cons_val, al_cons_flag, al_cons_id,
            al_product, al_prod_val, al_prod_flag, al_prod_id;

    JSONArray jsonarray, jsonarray1, jsonarray2, jsonarray3;

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("ON START HOME::::::::");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qde_hdb_home);

        cd = new ConnectionDetector(HdbHome.this);
        rDB = new DBHelper(this);
        SharedPreferences settings = getSharedPreferences("prefs", 0);
        boolean firstRun = settings.getBoolean("firstRun", true);

        if (firstRun) {
            // here run your first-time instructions, for example :
            Intent iHomeScreen = new Intent(HdbHome.this,
                    User_Details.class);
            startActivity(iHomeScreen);
            finish();
        }

        userFunction = new UserFunction(this);

        ActivityCompat.requestPermissions(HdbHome.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_READ_CONTACTS);


        db = new DatabaseHandler(this);

        al_cons = new ArrayList<String>();
        al_cons_val = new ArrayList<String>();
        al_cons_flag = new ArrayList<String>();
        al_cons_id = new ArrayList<String>();

        check_master = new ArrayList<String>();
        master_flag = new ArrayList<String>();

        //product
        al_product = new ArrayList<String>();
        al_prod_val = new ArrayList<String>();
        al_prod_flag = new ArrayList<String>();
        al_prod_id = new ArrayList<String>();


//        font = Typeface.createFromAsset(this.getAssets(),
        //              "fonts/MyriadPro-Regular.otf");
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        str_branch = pref.getString("user_brnch", null);
        str_designation = pref.getString("user_designation", "NO");
        str_designation_val = pref.getString("user_designation_value", "0");

        //  userFunction.checkforLogout(pref, HdbHome.this);

        is_login = pref.getInt("is_login", 0);
        is_login=1;

         off_is_login = pref.getInt("off_is_login", 0);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        System.out.println("ON CREaTE HOME::::::::");

        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.putString("app_type", null);
        peditor.commit();

        SetLayout();

        // System.out.println("HEHHUHU");

        //   RequestDB rdb = new RequestDB(HdbHome.this);

        int int_new_caset_pending_count = 0;
        int Pending_Photo_Count = 0;

/*
        int_new_caset_pending_count = Integer.parseInt(rdb.getPendingCount());
        Pending_Photo_Count = Integer.parseInt(eDB.getPendingPhotoCount());

        total_int = int_new_caset_pending_count  + Pending_Photo_Count;
        str_total = String.valueOf(total_int);


        if (cd.isConnectingToInternet()) {
            new GetDashboardInfo().execute();
        }
*/

        if (str_designation.equals("0")) {
            adapter = new HdbHomeListAdapter(this, itemname_co, imgid_co);
        } else {
            adapter = new HdbHomeListAdapter(this, itemname, imgid);
        }

        System.out.println("off Login HEHE:::" + off_is_login);

        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (is_login > 0 || off_is_login > 0) {
                    System.out.println("IF::::");

                    GoTo(position);
                } else {
                    System.out.println("ELSE::::");
                    Intent messagingActivity = new Intent(HdbHome.this,
                            LoginActivity.class);
                    messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(messagingActivity);
                    finish();

                }
            }
        });
    }

    protected void SetLayout() {
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);
        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);




        // txt_user_id.setTypeface(font);
        // txt_user_n)ame.setTypeface(font);
        // txt_branch.setTypeface(font);

    }

    public void GoTo(Integer i) {
        if (total_int > 0) {
            userFunction.cutomToast("Please upload data in Upload in progress tab ....Pending Count=" + str_total, HdbHome.this);
        }
        switch (i) {

            case 0:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity = new Intent(HdbHome.this, MainActivity.class);
                startActivity(messagingActivity);

                break;

            case 1:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity1 = new Intent(HdbHome.this, Offine_Entries.class);
                startActivity(messagingActivity1);

                break;

            case 2:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity2 = new Intent(HdbHome.this, MyUploads.class);
                startActivity(messagingActivity2);

                break;

            case 3:

                System.out.println("CASE 1 ::::");
                Intent messagingActivity25 = new Intent(HdbHome.this, ImageListAll.class);
                startActivity(messagingActivity25);

                break;

            case 4:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity3 = new Intent(HdbHome.this, MessageBox.class);

                startActivity(messagingActivity3);

                break;
            case 5:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity5 = new Intent(HdbHome.this, User_Details.class);
                startActivity(messagingActivity5);

                break;

            case 6:


                System.out.println("CASE 1 ::::");
                Intent messagingActivity4 = new Intent(HdbHome.this, Contact_us.class);
                startActivity(messagingActivity4);


                break;


            default:

                break;

        }

    }

    public void logout(View v) {
        // do stuff
        Intent hme = new Intent(com.example.hdb_los_prod.HdbHome.this,
                Home_PD_COLLX.class);
        startActivity(hme);

      //  alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                HdbHome.this.finish();
                                @SuppressWarnings("unused")
                                boolean islogoutBoolean = true;

                                SharedPreferences.Editor editor = pref.edit();

                                editor.putInt("off_is_login", 0);
                                editor.commit();

                                islogoutBoolean = userFunction
                                        .logoutUser(getApplicationContext());

                               /* off_islogoutBoolean = userfunction
                                        .off_logoutUserclear(getApplicationContext());*/

                                if (islogoutBoolean == true) {
                                    Intent i = new Intent(
                                            HdbHome.this, LoginActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {

    }


}