package com.example.MyUploads.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.MyUploads.E_receipt_frag2;
import com.example.MyUploads.E_receipt_frag_gene1;
import com.example.MyUploads.E_receipt_frag_genr2;
import com.example.MyUploads.E_reciept_frag1;
import com.example.MyUploads.E_reciept_genrate;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        if(E_reciept_genrate.str_module.contains("genrr")) {
            switch (index) {
                case 0:
                    return new E_receipt_frag2();
                case 1:
                    return new E_reciept_frag1();
            }
        }else{
            switch (index) {
                case 0:
                    return new E_receipt_frag_gene1();
                case 1:
                    return new E_receipt_frag_genr2();
            }

        }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }

}
