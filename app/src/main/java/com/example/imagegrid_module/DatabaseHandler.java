
package com.example.imagegrid_module;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.util.HashMap;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "android_api";

	// Login table name
	private static final String TABLE_LOGIN = "login";

	private static final String TABLE_OFFLOGIN = "off_login";

	// Login Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_EMAIL = "email";

	private static final String KEY_UID = "uid";
	private static final String KEY_CREATED_AT = "created_at";
	//private static final String KEY_DESIGNATION = "designation";

	// Off Login Table Columns names
	private static final String KEY_OFF_ID = "off_id";
	private static final String KEY_OFF_NAME = "off_name";
	private static final String KEY_OFF_EMAIL = "off_email";
	private static final String KEY_OFF_UID = "off_uid";
	private static final String KEY_OFF_CREATED_AT = "off_created_at";
	private static final String KEY_OFF_DEVIDEID = "off_device_id";

	public boolean chkDBExist = false;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		 File dbFile = context.getDatabasePath(DATABASE_NAME);
		 chkDBExist = dbFile.exists(); 
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," 
				+ KEY_NAME + " TEXT,"
				+ KEY_EMAIL + " TEXT UNIQUE,"
				+ KEY_UID + " TEXT,"
				+ KEY_CREATED_AT + " TEXT" + ")";
		db.execSQL(CREATE_LOGIN_TABLE);

		String CREATE_OFF_LOGIN_TABLE = "CREATE TABLE " + TABLE_OFFLOGIN + "("
				+ KEY_OFF_ID + " INTEGER PRIMARY KEY,"
				+ KEY_OFF_NAME + " TEXT,"
				+ KEY_OFF_EMAIL + " TEXT UNIQUE,"
				+ KEY_OFF_UID + " TEXT,"
				+KEY_OFF_DEVIDEID+ " TEXT,"
				+ KEY_OFF_CREATED_AT + " TEXT" + ")";
		db.execSQL(CREATE_OFF_LOGIN_TABLE);
	}
	
	
	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFLOGIN);
		// Create tables again
		onCreate(db);
	}

	/**
	 * Storing user details in database
	 * */
	public void addUser(String name, String email, String uid) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name); // Name
		values.put(KEY_EMAIL, email); // Email
		values.put(KEY_UID, uid); // Email
		//values.put(KEY_CREATED_AT, created_at); // Created At

		// Inserting Row
		db.insert(TABLE_LOGIN, null, values);
		db.close(); // Closing database connection
	}

	public void off_addUser(String name, String email, String uid, String imei) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_OFF_NAME, name); // Name
		values.put(KEY_OFF_EMAIL, email); // Email
		values.put(KEY_OFF_UID, uid); // Email
		values.put(KEY_OFF_DEVIDEID, imei);

		//values.put(KEY_CREATED_AT, created_at); // Created At

		// Inserting Row
		db.insert(TABLE_OFFLOGIN, null, values);
		db.close(); // Closing database connection
	}
	
	/**
	 * Getting user data from database
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String,String> user = new HashMap<String,String>();
		String selectQuery = "SELECT * FROM " + TABLE_LOGIN;
		 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
        	//user.put("name", cursor.getString(1));
        	//user.put("email", cursor.getString(2));
        	user.put("uid", cursor.getString(3));
        	//user.put("created_at", cursor.getString(4));
        }
        cursor.close();
        db.close();
		// return user
		return user;
	}

	public HashMap<String, String> off_getUserDetails(){
		HashMap<String,String> user = new HashMap<String,String>();
		String selectQuery = "SELECT * FROM " + TABLE_OFFLOGIN;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if(cursor.getCount() > 0){
			//user.put("name", cursor.getString(1));
			//user.put("email", cursor.getString(2));
			user.put("uid", cursor.getString(3));
			user.put("dev_id", cursor.getString(4));

		//	System.out.println("UID:" + cursor.getString(3)+"DEVID:"+cursor.getString(4));

			//user.put("created_at", cursor.getString(4));
		}
		cursor.close();
		db.close();
		// return user
		return user;
	}


	public boolean update_deviceid(String dev_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_OFF_DEVIDEID, dev_id);
		db.update(TABLE_OFFLOGIN, contentValues,null,null);
		return true;
	}

	/**
	 * Getting user login status
	 * return true if rows are there in table
	 * */
	public int getRowCount() {
		String countQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();
		db.close();
		cursor.close();

		// return row count
		return rowCount;
	}

	public int off_getRowCount() {
		String countQuery = "SELECT  * FROM " + TABLE_OFFLOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();
		db.close();
		cursor.close();

		// return row count
		return rowCount;
	}

	/**
	 * Re crate database
	 * Delete all tables and create them again
	 * */
	public void resetTables(){
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_LOGIN, null, null);
		db.close();

	}



	public void off_resetTables(){
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_OFFLOGIN, null, null);
		db.close();
	}

}
