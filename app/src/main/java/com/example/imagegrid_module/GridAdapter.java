package com.example.imagegrid_module;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hdb.R;

import java.util.ArrayList;
import java.util.HashMap;


public class GridAdapter  extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions user_function;
    String extension = "";

    //ViewHolder holder = null;

    //View itemView;
    public GridAdapter(Context context,
                                 ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        View itemView =null ;
        ViewHolder holder;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.dms_imageview_gridmodule_item_list, parent,
                    false);

            holder=new ViewHolder();
            holder.simpleTextView_con = (TextView) itemView
                    .findViewById(R.id.simpleTextView);
            holder.simpleTextView1 = (TextView) itemView
                    .findViewById(R.id.simpleTextView1);
            holder.image = (ImageView) itemView
                    .findViewById(R.id.image);

            itemView.setTag(holder);
        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }
        if (position % 2 == 1) {
           // itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
           // itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        String uri = resultp.get(GridviewModule.doc_name);




        int i = uri.lastIndexOf('.');
        if (i >= 0) {
            extension = uri.substring(i+1);
        }

        System.out.println(":::::extension:::::"+extension);
        if(extension.contains("pdf")){
            System.out.println(":::::extension:::::"+extension);
            holder.image.setBackgroundResource(R.drawable.pdf_icon_n);
        }
        else {
            holder.image.setBackgroundResource(R.drawable.app_icon);
        }

        holder.simpleTextView_con.setText(Html.fromHtml(resultp.get(GridviewModule.doc_name)));
        holder.simpleTextView1.setText(Html.fromHtml(resultp.get(GridviewModule.doc_id)));


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                Intent pd_form = new Intent(context, API_activity.class);
                pd_form.putExtra("doc_id", resultp.get(GridviewModule.doc_id));
                pd_form.putExtra("extension",resultp.get(GridviewModule.doc_name) );
                context.startActivity(pd_form);

            }
        });
        return itemView;
    }

    private static class ViewHolder {
        public TextView simpleTextView_con,simpleTextView1;
        public ImageView image;



    }


}

