package com.example.imagegrid_module;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import com.example.hdb.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences pref;



    //String fisrt_run = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_imageview_activity_splash);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
      //  TrustScoreHelper.getInstance(getApplicationContext()).initialize();

        Boolean is_xiomi=isXiaomiPhone();
        if(is_xiomi==true){
            showServiceSmsPermissionSettings();

        }else{

            int SPLASH_SCREEN_TIME = 2000;

            new Handler().postDelayed(new Runnable() {

                SharedPreferences settings = getSharedPreferences("prefs", 0);
                boolean firstRun = settings.getBoolean("firstRun", true);

                @Override
                public void run() {
                    // This is method will be executed when SPLASH_SCREEN_TIME is
                    // over, Now you can call your Home Screen
                    SharedPreferences settings = getSharedPreferences("prefs", 0);
                    boolean firstRun = settings.getBoolean("firstRun", true);

                    if (firstRun) {
                        // here run your first-time instructions, for example :
                        // startActivityForResult( new Intent(SplashScreen.this,
                        // InstructionsActivity.class),INSTRUCTIONS_CODE);
                        Intent iHomeScreen = new Intent(SplashScreen.this,
                                Login_act.class);
                        startActivity(iHomeScreen);
                    } else {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        Intent i = new Intent(SplashScreen.this, Login_act.class);
                        startActivity(i);
                    }

                    // Finish Current Splash Screen, as it should be visible only
                    // once when application start
                    finish();
                }
            }, SPLASH_SCREEN_TIME);
        }

    }

    //rDB.delete_data();

    // checkforLogout();





    public Void checkforLogout() {
        String str_last_date = pref.getString("current_date", null);

        //	String str_last_date="2016-08-15";

        System.out.println("LAST DATE::::"+str_last_date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = sdf.format(new Date());

        Date date1 = null;
        Date date2 = null;

        try {
            if (current_date != null) {
                date1 = sdf.parse(current_date);
            }
            if (str_last_date != null) {
                date2 = sdf.parse(str_last_date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (current_date != null && str_last_date != null) {
            if (date1.after(date2)) {
                System.out.println("Date1 is after Date2");
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("is_login", 0);
                editor.putInt("off_is_login", 0);
                editor.commit();
            }
        }
        return null;
    }



    private static final int RC_SERVICE_SMS_SETTINGS = 1222;

    /**
     *   Method to check if current manufacturer is Xiaomi
     */
    boolean isXiaomiPhone() {
        return "xiaomi".equalsIgnoreCase(Build.MANUFACTURER);
    }


    /**
     *  Shows a new window where user has to manually check "Service SMS".
     *  When user closes that window, onActivityResult is invoked with resultcode = RC_SERVICE_SMS_SETTINGS
     */
    void showServiceSmsPermissionSettings() {
        try {
            // MIUI 8
            Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
            localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.PermissionsEditorActivity");
            localIntent.putExtra("extra_pkgname", getPackageName());
            startActivityForResult(localIntent, RC_SERVICE_SMS_SETTINGS);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                // MIUI 5/6/7
                Intent localIntent = new Intent("miui.intent.action.APP_PERM_EDITOR");
                localIntent.setClassName("com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity");
                localIntent.putExtra("extra_pkgname", getPackageName());
                startActivityForResult(localIntent, RC_SERVICE_SMS_SETTINGS);
            } catch (Exception e1) {
                e1.printStackTrace();
                // Otherwise jump to application details
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, RC_SERVICE_SMS_SETTINGS);
            }
        }
    }


    public void onServiceSmsResponseReceived(){
        // Continue further work in app

        int SPLASH_SCREEN_TIME = 2000;

        new Handler().postDelayed(new Runnable() {

            SharedPreferences settings = getSharedPreferences("prefs", 0);
            boolean firstRun = settings.getBoolean("firstRun", true);

            @Override
            public void run() {
                // This is method will be executed when SPLASH_SCREEN_TIME is
                // over, Now you can call your Home Screen
                SharedPreferences settings = getSharedPreferences("prefs", 0);
                boolean firstRun = settings.getBoolean("firstRun", true);

                if (firstRun) {
                    // here run your first-time instructions, for example :
                    // startActivityForResult( new Intent(SplashScreen.this,
                    // InstructionsActivity.class),INSTRUCTIONS_CODE);
                    Intent iHomeScreen = new Intent(SplashScreen.this,
                            Login_act.class);
                    startActivity(iHomeScreen);
                } else {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    Intent i = new Intent(SplashScreen.this, Login_act.class);
                    startActivity(i);
                }

                // Finish Current Splash Screen, as it should be visible only
                // once when application start
                finish();
            }
        }, SPLASH_SCREEN_TIME);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SERVICE_SMS_SETTINGS) {
            System.out.println("request_code++++++++++++"+requestCode);
            onServiceSmsResponseReceived();
        }
    }





}
