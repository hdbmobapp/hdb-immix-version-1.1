package com.example.imagegrid_module;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class GridviewModule extends AppCompatActivity {
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;
    GridView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;


    static String doc_id = "doc_id";
    static String doc_name = "doc_name";


    GridAdapter adapter;
    ProgressBar pb_progress;
    TextView txt_error;
    SharedPreferences pref;
    String str_user_id = null;
    EditText edt_loan_search;
    String str_loan_search;
    Button btn_submit;
    LinearLayout pd_details_layout;
    String str_message;
    String KEY_STATUS = "status_code";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    LinearLayout lin_new_pd;
    public int pgno = 0;

    ImageView img_app_usage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dms_imageview_gridview_module_act);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        setlayout();


    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_loan_search = (EditText) findViewById(R.id.edt_search);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        list = (GridView) findViewById(R.id.list);
        pb_progress = (ProgressBar) findViewById(R.id.pb_progress);
        pd_details_layout = (LinearLayout) findViewById(R.id.pd_details_layout);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arraylist = new ArrayList<HashMap<String, String>>();
                str_loan_search = edt_loan_search.getText().toString();

                if (str_loan_search != null && str_loan_search != "" && !str_loan_search.isEmpty()) {
                    if (cd.isConnectingToInternet()) {
                        pgno = 0;
                        new DownloadLoanTrails().execute();
                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Please Enter LOSID", getApplicationContext());
                }

            }

        });




    }

    public class DownloadLoanTrails extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_dms_images(str_loan_search);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            // JSONObject jobj = jsonobject.getJSONObject("Data");
                            jsonarray = jsonobject.getJSONArray("data");

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);

                                map.put("doc_id", jsonobject.getString("doc_id"));
                                map.put("doc_name", jsonobject.getString("doc_name"));

                                // Set the JSON Objects into the array
                                arraylist.add(map);
                            }
                        } else {
                            str_message = "No Details found ";
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            TextView txt_message = (TextView) findViewById(R.id.txt_message);

            if (jsonobject == null) {
                userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    pd_details_layout.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    adapter = new GridAdapter(GridviewModule.this, arraylist);
                    adapter.notifyDataSetChanged();
                    list.setAdapter(adapter);
                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }


            }
              pb_progress.setVisibility(View.GONE);
        }
    }


}
