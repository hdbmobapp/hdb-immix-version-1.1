package com.example.imagegrid_module;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;

import org.json.JSONObject;

import java.util.HashMap;


@SuppressLint("NewApi")
public class UserFunctions extends Activity {

    private JSONParser jsonParser;
    SharedPreferences pref;


    private static String URL = "https://hdbapp.hdbfs.com/API/HDB_DMS_Images/index.php";

    private static String URL_POST = "https://hdbapp.hdbfs.com/API/HDB_DMS_Images/get_all_images.php";

    private static String URL_GET_IMAGE = "https://hdbapp.hdbfs.com/API/HDB_DMS_Images/get_single_image.php";


    Context context;

    SharedPreferences pData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //  setContentView(R.layout.setting);
    }

    // constructor
    public UserFunctions() {
        jsonParser = new JSONParser();

    }




    public void cutomToast(String message, Context contxt) {
        // Inflate the Layout
        ViewGroup parent = null;
        LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dms_imageview_custom_toast,
                parent, false);
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(contxt);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    public void noInternetConnection(String message, Context contxt) {
        Toast toast = null;
        if (toast == null
                || toast.getView().getWindowVisibility() != View.VISIBLE) {
            ViewGroup parent = null;
            LayoutInflater inflater = (LayoutInflater) contxt
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.dms_imageview_toast_no_internet, parent,
                    false);
            TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
            mesg_name.setText(message);
            // Create Custom Toast
            toast = new Toast(contxt);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }


    public boolean isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }

    public boolean off_isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.off_getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }

    public boolean logoutUser(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();
        //this.cutomToast("Hope you had a wonderful day! Do check back soon.",context);

        return true;
    }

    public boolean logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }

    public boolean off_logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.off_resetTables();

        return true;
    }




    public JSONObject loginUser(String username, String password, String imeiNumber1,String imeiNumber2, String latitude, String longitude, String device_name, String device_man, String android_id,String str_api_level) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "login");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("username", username);
            map.put("password", password);
            map.put("imei1", imeiNumber1);
            map.put("imei2", imeiNumber2);
            map.put("device_name", device_name);
            map.put("device_man", device_man);
            map.put("app_version", "Version 4.2");
            map.put("device_id", android_id);
            map.put("api_level", str_api_level);



            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);

           // Log.e("JSON", json.toString());
            System.out.println("JSON"+json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public JSONObject get_dms_images(String str_loan_search) {

        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("LOSID", str_loan_search);
            json = jsonParser.makeHttpRequest_new(URL_POST, "POST", map, context);


        } catch (Exception e) {

            e.printStackTrace();
        }
      //   Log.e("JSON", json.toString());
        return json;
    }

    public JSONObject get_dms_image(String str_doc_id) {

        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("doc_id", str_doc_id);
            json = jsonParser.makeHttpRequest_new(URL_GET_IMAGE, "POST", map, context);


        } catch (Exception e) {

            e.printStackTrace();
        }
      //  Log.e("JSON", json.toString());
        return json;
    }

    }

