package com.example.imagegrid_module;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;


public class Login_act extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private static final String TAG = "Login_act";

    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunctions userFunction;
    JSONObject json;
    String reg_id;


    private ConnectionDetector cd;
    // private static Typeface font;
    //String user_name = null;
    String gcm = null;
    @SuppressWarnings("unused")
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{2,14}");
    Context context;
    ProgressBar pb_progress;

    String res, resp_success;

    public static String KEY_STATUS = "status";

    public static String KEY_SUCCESS = "success";

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";

    String imeiNumber1, imeiNumber2,str_api_level;
    Integer api_level;



    SharedPreferences pref;
    String lattitude = null, longitude = null;
    //EditText edt_new_version_link;
    String new_version;
    TextView txt_new_version;
    String new_version_name;

    HashMap<String, String> user = new HashMap<String, String>();


    private String android_id;
    String check_master_flag;

    double dbl_latitude = 0;
    double dbl_longitude = 0;
    static Double lat;
    static Double lon;
    DatabaseHandler db;
    Integer off_rowcount;

    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;


    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();



        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        db = new DatabaseHandler(getApplicationContext());


        checkPermissions();
        SetLayout();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) Login_act.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        checkLocation();
    }


    protected void SetLayout() {
        // Check if Internet present
        setContentView(R.layout.dms_imageview_activity_login);
        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        // edt_new_version_link = (EditText) findViewById(R.id.edt_new_version_link);
        txt_new_version = (TextView) findViewById(R.id.txt_new_version);
        // ---set Custom Font to textview
        //inputEmail.setTypeface(font);

        // inputPassword.setTypeface(font);

        // btnLogin.setTypeface(font);
        // loginErrorMsg.setTypeface(font);

        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                System.out.println("jnnfgnhnnhnfhbonbhfgoh");

                api_level = Build.VERSION.SDK_INT;
                str_api_level= Integer.toString(api_level);
                TelephonyManager tm = (TelephonyManager) getSystemService(context.TELEPHONY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    imeiNumber1 = tm.getImei(1);
                    imeiNumber2 = tm.getImei(2);

                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    imeiNumber1 = tm.getDeviceId(1);
                    imeiNumber2 = tm.getDeviceId(2);
                    System.out.println("imeiNumber1" + imeiNumber1);
                }

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                    android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                }

                checkLocation();
                if (isLocationEnabled() == true) {

                    email = inputEmail.getText().toString();
                    password = inputPassword.getText().toString();
                    // Check if Internet present
                    TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                    TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);
                    // error_pwd.setTypeface(font);
                    //  error_email.setTypeface(font);

                    if (password.equals("") || email.equals("")) {

                        if (password.equals("")) {
                            error_pwd.setVisibility(View.VISIBLE);
                            error_pwd.setText("Please enter a password");

                        } else {
                            error_pwd.setVisibility(View.GONE);

                        }
                        if (email.equals("")) {
                            error_email.setVisibility(View.VISIBLE);
                            error_email.setText("Please enter a Employee Id");
                        } else {
                            error_email.setVisibility(View.GONE);

                        }
                    } else {

                        if (cd.isConnectingToInternet()) {

                            error_pwd.setVisibility(View.GONE);
                            error_email.setVisibility(View.GONE);
                      /*  Location nwLocation = appLocationService
                                .getLocation(LocationManager.NETWORK_PROVIDER);
                        System.out.println("nwLocation:::" + nwLocation);

                        if (nwLocation == null) {
                            userFunction.cutomToast("Please start Location Service..", LoginActivity.this);
                        } else {
                            double ltt = nwLocation.getLatitude();
                            double lott = nwLocation.getLongitude();


                            longitude = String.valueOf(lott);
                        }*/

                            System.out.println("lattitude:  lat::" + lat);

                            if (lat != null) {
                                lattitude = Double.toString(lat);
                                System.out.println("lattitude:::" + lattitude);
                                longitude = Double.toString(lon);
                            }
                            System.out.println("longitude:::" + longitude);
                            new Login().execute();

                        } else {

                            error_pwd.setVisibility(View.GONE);
                            error_email.setVisibility(View.GONE);

                            //   dbl_latitude = tracker.getLatitude();
                            //   dbl_longitude = tracker.getLongitude();
                            //  lattitude=String.valueOf(dbl_latitude);
                            //  longitude=String.valueOf(dbl_longitude);


                            // userFunction.logoutUserclear(getApplicationContext());

                            user = db.off_getUserDetails();

                            System.out.println("HASHMAP::::" + user);

                            String val = (String) user.get("uid");
                            String dev_val = (String) user.get("dev_id");

                            //   String userid=pref.getString("user_id", null);

                            System.out.println("USERID::::" + email);

                            System.out.println("DEVICE ID::::::::" + dev_val);

                            if (val != null) {

                                if (val.equals(email)) {

                                    SimpleDateFormat sdf = new SimpleDateFormat(
                                            "yyyy-MM-dd");
                                    String current_date = sdf.format(new Date());

                                    SharedPreferences.Editor editor = pref.edit();

                                    editor.putInt("off_is_login", 1);

                                    editor.putString("current_date_off", current_date);
                                    editor.commit();
                                    System.out.println("OFF LOGIN:::");

                                    off_rowcount = db.off_getRowCount();
                                    if (off_rowcount > 0) {
                                        if (!dev_val.equals("")) {

                                            System.out.println("DEVICE ID VAL:::" + dev_val);

                                            if (!dev_val.equals(android_id)) {
                                                userFunction.cutomToast(
                                                        "You have changed your handset. Contact your supervisor",
                                                        getApplicationContext()

                                                );

                                                loginErrorMsg.setText("You have changed your handset. Contact your supervisor");


                                            } else {
                                                Intent messagingActivity = new Intent(
                                                        Login_act.this, HdbHome.class);
                                                startActivity(messagingActivity);
                                                finish();
                                            }
                                        } else {
                                            Intent messagingActivity = new Intent(
                                                    Login_act.this, HdbHome.class);
                                            startActivity(messagingActivity);
                                            finish();
                                        }
                                    } else {
                                        userFunction.noInternetConnection(
                                                "Check your connection settings!",
                                                getApplicationContext());

                                        loginErrorMsg.setText("Check your connection settings!");
                                    }
                                } else {
                                    userFunction.cutomToast(
                                            "Username or Password is incorrect",
                                            getApplicationContext());

                                    loginErrorMsg.setText("Username or Password is incorrect");
                                }
                            } else {
                                userFunction.noInternetConnection(
                                        "Check your connection settings!",
                                        getApplicationContext());

                                loginErrorMsg.setText("Check your connection settings!");
                            }

                       /* userFunction.noInternetConnection(
                                "Check your connection settings!",
                                getApplicationContext());*/
                        }
                    }

                }
            }
        });

    }




    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat = location.getLatitude();
        System.out.println("adffddf"+lat);
        lon = location.getLongitude();


        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }



    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());

                }

            }
        });
    }


    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            // String str_imei = getIMEI(LoginActivity.this);
            String str_imei ="imei";
            try {
                String deviceName = Build.MODEL;
                String deviceMan = Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, imeiNumber1, imeiNumber1,
                        lattitude, longitude, deviceName, deviceMan, android_id,str_api_level);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);


                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            // user successfully logged in'
                            // Store user details in SQLite Database10
                            DatabaseHandler db = new DatabaseHandler(
                                    getApplicationContext());
                            JSONObject json_user = json.getJSONObject("data");
                            //   JSONObject json_user1 = json.getJSONObject("imei");

                            // Clear all previous data in database
                            userFunction
                                    .logoutUserclear(getApplicationContext());

                            userFunction
                                    .off_logoutUserclear(getApplicationContext());


                            String KEY_EMAIL = "email";
                            String KEY_NAME = "name";
                            String KEY_UID = "id";
                            String KEY_deviceid = "imei";

                            db.addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID));

                            db.off_addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID),
                                    json.getString(KEY_deviceid)
                            );

                            SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd");
                            String current_date = sdf.format(new Date());

                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("current_date", current_date);

                            editor.putInt("is_login", 1);
                            editor.putString("user_id",
                                    json_user.getString(KEY_UID).toString());
                            editor.putString("user_name",
                                    json_user.getString(KEY_NAME).toString());
                            String KEY_BRANCH = "branch";
                            editor.putString("user_brnch",
                                    json_user.getString(KEY_BRANCH).toString());
                            editor.putString("user_email",
                                    json_user.getString(KEY_EMAIL).toString());
                            editor.putString("user_designation",
                                    json_user.getString("designation").toString());
                            editor.putString("user_designation_value",
                                    json_user.getString("user_designation_value").toString());

                            editor.putString("avi_tel",
                                    json_user.getString("Avinash").toString());
                            editor.putString("sach_tel",
                                    json_user.getString("Sachin").toString());
                            editor.putString("vija_tel",
                                    json_user.getString("Vijay").toString());
                            editor.putString("service_tel",
                                    json_user.getString("Service").toString());

                            if(!json_user.getString("img_height").equals("")){
                                editor.putInt("img_height",
                                        Integer.valueOf(json_user.getString("img_height")));
                            }

                            if(!json_user.getString("img_width").equals("")) {
                                editor.putInt("img_width",
                                        Integer.valueOf(json_user.getString("img_width")));
                            }

                            if(!json_user.getString("img_comp_size").equals("")) {
                                editor.putInt("img_comp_size",
                                        Integer.valueOf(json_user.getString("img_comp_size").toString()));
                            }

                            System.out.println("Service::::::"+json_user.getString("Service").toString());
                            if (rememberMe.isChecked()) {
                                editor.putString("user_username", email);
                                editor.putString("user_password", password);
                            } else {
                                editor.putString("user_username", "");
                                editor.putString("user_password", "");
                            }
                            String loan_count = json_user.getString("loan_count");


                            editor.commit();


                            Intent messagingActivity = new Intent(
                                    Login_act.this, HdbHome.class);
                            startActivity(messagingActivity);
                            finish();


                        } else {
                            String KEY_DEVID = "imei";

                            if(!json.getString(KEY_DEVID).toString().equals("")) {
                                db.update_deviceid(json.getString(KEY_DEVID));
                            }

                            if (new_version != null && new_version != "") {
                               // edt_new_version_link.setText(new_version);
                               // edt_new_version_link.setVisibility(View.VISIBLE);
                                txt_new_version.setText(new_version_name);
                                txt_new_version.setVisibility(View.VISIBLE);
                            }
                            loginErrorMsg.setText(json.getString("message")
                                    .toString());
                        }



                    }
                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb_progress.setVisibility(View.GONE);

        }

    }


    public void contact_us(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                Contact_us.class);
        startActivity(home_activity);

    }


}

