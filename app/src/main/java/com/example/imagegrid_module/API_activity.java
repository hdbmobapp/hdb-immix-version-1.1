package com.example.imagegrid_module;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.imagegrid_module.info.hdb.lib.ZoomageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class API_activity extends AppCompatActivity {
    TextView txt_img_name;
    String str_message;
    String KEY_STATUS = "status_code";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    ProgressBar pb_progress;
    TextView txt_error;
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;
    SharedPreferences pref;
    String str_user_id = null;
    String str_doc_name;
    String str_doc_id,doc_id;
    String imageString,extension;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    private ZoomageView mImageView;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_imageview_api_activity);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mImageView=(ZoomageView)findViewById(R.id.myZoomageView);
        txt_img_name=(TextView) findViewById(R.id.txt_img_name);
        pb_progress=(ProgressBar)  findViewById(R.id.pb_progress);
        txt_error = (TextView) findViewById(R.id.text_error_msg);

       // mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        Intent i = getIntent();
        str_doc_id = i.getStringExtra("doc_id");
        extension = i.getStringExtra("extension");

        String uri = extension;




        int ii = uri.lastIndexOf('.');
        if (ii >= 0) {
            extension = uri.substring(ii+1);
        }


        new DownloadLoanTrails().execute();


    }


    public class DownloadLoanTrails extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_dms_image(str_doc_id);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            // JSONObject jobj = jsonobject.getJSONObject("Data");
                            doc_id = jsonobject.getString("document_id");
                            System.out.println("vgjjjjjjjjjjvjgggggggggg" + doc_id);
                            imageString = jsonobject.getString("document");
                            System.out.println("vgjjjjjjjjjjvjgggggggggg" + imageString);

                        } else {
                            str_message = "No Details found ";
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            TextView txt_message = (TextView) findViewById(R.id.txt_message);
            if (jsonobject == null) {
                userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    txt_message.setVisibility(View.GONE);
                    txt_img_name.setText(str_doc_name);



                    if (extension.contains("jpeg") || extension.contains("jpg")) {
                        byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);

                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        mImageView.setImageBitmap(decodedByte);

                    } else {
                        //PDF

                        File mediaStorageDir;

                        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "HDB_COLLX");
                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {

                            }
                        }
                        // Create a media file name


                        // File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/"+"Base64.pdf");

                        String new_path = mediaStorageDir.getPath() + File.separator + str_user_id + "MAP.pdf";
                        final File mediaFile;
                        mediaFile = new File(new_path);


                        try {

                            FileOutputStream fos = new FileOutputStream(mediaFile);
                            fos.write(Base64.decode(imageString, Base64.DEFAULT));
                            fos.close();

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(mediaFile), "application/pdf");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(intent);
                            finish();

                        } catch (IOException e) {
                            // Log.d(tag, "File.toByteArray() error");
                            e.printStackTrace();
                        }


                    }


                    //
                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    txt_message.setText(str_message);
                }


            }
            pb_progress.setVisibility(View.GONE);

        }
    }

  /*  private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f,
                    Math.min(mScaleFactor, 10.0f));
            mImageView.setScaleX(mScaleFactor);
            mImageView.setScaleY(mScaleFactor);
            return true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }*/

}
