package com.example.hdb;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import info.hdb.libraries.ExampleDBHelper;


/**
 * Created by Administrator on 20-01-2017.
 */
public class ImagesList extends Activity {
    ExampleDBHelper dbHelper;
    Integer total_pic_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageslist);
        setlistview();
    }

    public void setlistview() {
        dbHelper = new ExampleDBHelper(this);
        TextView txt_upload =(TextView)findViewById(R.id.txt_upload);
        final Cursor cursor = dbHelper.getImages();
        total_pic_count = cursor.getCount();
        if(total_pic_count>0){
            txt_upload.setVisibility(View.GONE);
        }else{
            txt_upload.setVisibility(View.VISIBLE);
        }
        ListView lvItems = (ListView) findViewById(R.id.list);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageAdapter todoAdapter = new ImageAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        todoAdapter.notifyDataSetChanged();
        lvItems.refreshDrawableState();

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(com.example.hdb.ImagesList.this, Dashboard.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }
}
