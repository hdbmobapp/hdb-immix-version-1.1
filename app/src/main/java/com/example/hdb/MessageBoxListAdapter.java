package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

public class MessageBoxListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;

	UserFunctions user_function;

	public MessageBoxListAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView txt_msg_from, txt_msg, txt_created_date,txt_msg_subject;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.message_box_list_item,
				parent, false);
		// Get the position
		resultp = data.get(position);
		final RelativeLayout rel_lay_msg=(RelativeLayout)itemView.findViewById(R.id.rel_lay_msg);
		
		if(resultp.get(MessageBox.MESSAGE_STATUS).equals("0")==true){
			rel_lay_msg.setBackgroundColor(context.getResources().getColor(R.color.list_color_1));
		}else{
			rel_lay_msg.setBackgroundColor(context.getResources().getColor(R.color.default_screen_bg));

		}

		txt_msg_from = (TextView) itemView.findViewById(R.id.msg_from);
		txt_msg = (TextView) itemView.findViewById(R.id.txt_msg);
		txt_created_date = (TextView) itemView
				.findViewById(R.id.txt_created_date);
		txt_msg_subject = (TextView) itemView
				.findViewById(R.id.txt_msg_subject);

		txt_msg.setVisibility(View.GONE);
		txt_msg_from.setText("From : "
				+ resultp.get(MessageBox.MESSAGE_SENDER_NAME));
		txt_msg_subject.setText("Subject - "+resultp.get(MessageBox.MESSAGE_SUBJECT));
		txt_created_date.setText(resultp.get(MessageBox.MESSAGE_CREATED_DATE));

		userFunction = new UserFunctions();

		itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);

				new Thread(new Runnable() {
					public void run() {
						// Get the position
					//	rel_lay_msg.setBackgroundColor(context.getResources().getColor(R.color.default_screen_bg));

						resultp = data.get(position);
						Intent messagingActivity = new Intent(context,
								SingleMessage.class);
						messagingActivity.putExtra("msg_from",
								resultp.get(MessageBox.MESSAGE_SENDER_NAME));
						messagingActivity.putExtra("msg",
								resultp.get(MessageBox.MESSAGE_CONTENT));
						messagingActivity.putExtra("created_date",
								resultp.get(MessageBox.MESSAGE_CREATED_DATE));
						messagingActivity.putExtra("msg_id",
								resultp.get(MessageBox.MESSAGE_ID));
						messagingActivity.putExtra("msg_subject",
								resultp.get(MessageBox.MESSAGE_SUBJECT));
						context.startActivity(messagingActivity);
					}
				}).start();
			}

		});

		return itemView;
	}
}
