package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

/**
 * * Created by ${Avinash} on ${30-07-2015}.
 */

public class EffReportAdapter extends BaseAdapter {
    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;

    UserFunctions user_function;

    public EffReportAdapter(Context context,
    ArrayList<HashMap<String, String>> arraylist) {

        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView txt_1,txt_2,txt_3,txt_4,txt_5,txt_6,txt_7,txt_8,txt_9,txt_10;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.detail_list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        txt_1 =  itemView.findViewById(R.id.txt_1);
        txt_2 =  itemView.findViewById(R.id.txt_2);
        txt_3 =  itemView.findViewById(R.id.txt_3);
        txt_4 =  itemView.findViewById(R.id.txt_4);
        txt_5 =  itemView.findViewById(R.id.txt_5);
        txt_6 =  itemView.findViewById(R.id.txt_6);
        txt_7 =  itemView.findViewById(R.id.txt_7);
        txt_8 =  itemView.findViewById(R.id.txt_8);
        txt_9 =  itemView.findViewById(R.id.txt_9);
        txt_10 =  itemView.findViewById(R.id.txt_10);


        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.list_back_color));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.list_color_1));
        }


        if (!resultp.get(TodaysDetails.GRID_REPORT1).toString().equals("null")) {
            txt_1.setText(resultp.get(TodaysDetails.GRID_REPORT1));
            txt_1.setVisibility(View.GONE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT2).toString().equals("null")) {

            txt_2.setText(resultp.get(TodaysDetails.GRID_REPORT2));
            txt_2.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT3).toString().equals("null")) {

            txt_3.setText(resultp.get(TodaysDetails.GRID_REPORT3));
            txt_3.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT4).toString().equals("null")) {

            txt_4.setText(resultp.get(TodaysDetails.GRID_REPORT4));
            txt_4.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT5).toString().equals("null")) {
            txt_5.setText(resultp.get(TodaysDetails.GRID_REPORT5));
            txt_5.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT6).toString().equals("null")) {
            txt_6.setText(resultp.get(TodaysDetails.GRID_REPORT6));
            txt_6.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT7).toString().equals("null")) {
            txt_7.setText(resultp.get(TodaysDetails.GRID_REPORT7));
            txt_7.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT8).toString().equals("null")) {
            txt_8.setText(resultp.get(TodaysDetails.GRID_REPORT8));
            txt_8.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT9).toString().equals("null")) {
            txt_9.setText(resultp.get(TodaysDetails.GRID_REPORT10));
            txt_9.setVisibility(View.VISIBLE);
        }

        if (!resultp.get(TodaysDetails.GRID_REPORT10).toString().equals("null")) {
            txt_10.setText(resultp.get(TodaysDetails.GRID_REPORT10));
            txt_10.setVisibility(View.VISIBLE);
        }

        userFunction = new UserFunctions();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                arg0.setSelected(true);

                new Thread(new Runnable() {
                    public void run() {
                        // Get the position
                        resultp = data.get(position);
/*
						Intent messagingActivity = new Intent(context,
								ACMActivity.class);
						messagingActivity.putExtra("emp_id",
								resultp.get(SecondActivity.ACM_USER_ID));
						messagingActivity.putExtra("emp_name",
								resultp.get(SecondActivity.txt_1));
						messagingActivity.putExtra("str_region",
								SecondActivity.str_region);

						messagingActivity.putExtra("acmjsonobject",
								acmjsonobject.toString());

						messagingActivity
								.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						context.startActivity(messagingActivity);

						/*
						 *
						 * // Creating a fragment object DashboardFragment
						 * rFragment = new DashboardFragment();
						 *
						 * // Creating a Bundle object Bundle data = new
						 * Bundle();
						 *
						 * // Setting the index of the currently selected item
						 * of // mDrawerList
						 *
						 * data.putString("emp_id",
						 * resultp.get(SecondActivity.ACM_USER_ID));
						 * data.putString("emp_name",
						 * resultp.get(SecondActivity.txt_1));
						 *
						 * // Setting the position to the fragment
						 * rFragment.setArguments(data);
						 *
						 * // Getting reference to the FragmentManager
						 * FragmentManager fragmentManager = ((Activity)
						 * context).getFragmentManager();
						 *
						 * // Creating a fragment transaction
						 * FragmentTransaction ft =
						 * fragmentManager.beginTransaction();
						 *
						 * // Adding a fragment to the fragment transaction
						 * ft.replace(R.id.content_frame, rFragment);
						 *
						 * // Committing the transaction ft.commit();
						 *
						 * SecondActivity.mDrawerLayout.closeDrawer(SecondActivity
						 * .drawerll);
						 */
                    }
                }).start();
            }

        });

        return itemView;
    }
}
