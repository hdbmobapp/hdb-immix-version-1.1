package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;


public class New_Remark extends Activity {

    Button btn_home;
    Button btn_dataentry;
    EditText edit_new_remark;
    Button btn_submit_remark;
    String str_new_remark;
    ProgressDialog mProgressDialog;
    String str_user_name;
    String resp_success;
    String losid;
    String parent_id;


    JSONObject jsonobject;
    private ConnectionDetector cd;
    SharedPreferences pref;
    public ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    String str_designation;
    String str_designation_val;
    String str_user_id;
    String str_parent_id;
    String str_page;
    String str_parent_id1;


    UserFunctions userFunction = new UserFunctions();
    private static final Pattern REMARK_PATTERN = Pattern
            .compile("[a-zA-Z0-9 ]{1,100}");

    static  String PARENT_ID="Parent_Id";

    private boolean CheckRemark(String remark) {
        return REMARK_PATTERN.matcher(remark).matches();
    }


    public void onCreate(Bundle savedInstanceState) {


        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        str_designation = pref.getString("user_designation", "NO");
        str_user_name = pref.getString("user_name", null);
        str_user_id = pref.getString("user_id", null);


        cd = new ConnectionDetector(com.example.hdb.New_Remark.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_remark);

        Intent iii=getIntent();
        losid = iii.getStringExtra("losid");
        str_parent_id = iii.getStringExtra("parentid");

        //str_parent_id1 = iii.getStringExtra("parentid");

       // str_parent_id1 = iii.getStringExtra("parentid");

        str_page = iii.getStringExtra("pageid");

        this.SetLayout();

        btn_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent home = new Intent(com.example.hdb.New_Remark.this,
                        HdbHome.class);
                startActivity(home);
            }
        });

        btn_dataentry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent dataentry = new Intent(com.example.hdb.New_Remark.this,
                        EditRequest.class);
                startActivity(dataentry);
            }
        });


        btn_submit_remark.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                str_new_remark = null;
                if (cd.isConnectingToInternet()) {
                    str_new_remark = null;
                    str_new_remark = edit_new_remark.getText().toString();

                    if (str_new_remark.equals("")) {
                        userFunction.cutomToast("Please enter remark",
                                getApplicationContext());
                    }
                    else {
                        if (!CheckRemark(str_new_remark)) {
                            userFunction.cutomToast("Please enter a valid remark",
                                    getApplicationContext());
                        } else {
                            new AddRemarks().execute();

                            System.out.println("Page id:::::"+str_page);
                            System.out.println("LOS id:::::"+losid);
                            System.out.println("Parent id:::::" +str_parent_id1);

                            Intent get_losid=new Intent(com.example.hdb.New_Remark.this,Remarks_Display.class);

                            get_losid.putExtra("losid1",losid);
                            startActivity(get_losid);

                            // System.out.println("AAAAA:::" + losid);

                        }
                    }

                } else {

                    userFunction.cutomToast("No Internet Connection...",
                            getApplicationContext());
                }
            }
        });


    }

    protected void SetLayout() {
        btn_submit_remark = (Button) findViewById(R.id.btn_submit);
        btn_home = (Button) findViewById(R.id.txt_home);
        btn_dataentry = (Button) findViewById(R.id.txt_data_entry);
        edit_new_remark = (EditText) findViewById(R.id.edit_new_remark);
    }

    public class AddRemarks extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.New_Remark.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

//fetching extra data passed with intents in a Bundle type variable

            System.out.println("LLOSIDLLL:::::" + losid);
            boolean bool_sucess = false;

            System.out.println("PARENTID"+str_parent_id);


                jsonobject = userFunction.insertRemark(losid, str_new_remark, str_user_id, str_user_name, "CO", str_parent_id);

            if (jsonobject != null) {
                // Create an array
                try {
                    String KEY_STATUS = "status";
                    // Locate the array name in JSON
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        resp_success = jsonobject.getString(KEY_SUCCESS);


                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                }
                //e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {

                userFunction.cutomToast("Something went wrong. Please try after sometime",
                        getApplicationContext());

                finish();
            } else {
                System.out.println("LOSID:::::" + losid);

                    userFunction.cutomToast("New remark submitted!",
                            getApplicationContext());

                  //  System.out.println( );

            }


            mProgressDialog.dismiss();
        }


    }


}
