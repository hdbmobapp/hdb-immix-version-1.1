package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

public class TodaysDetailsListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;

	UserFunctions user_function;

	public TodaysDetailsListAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView txt_details_tag, txt_details_count, txt_details_sum;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.todays_details_list_adapter,
				parent, false);
		// Get the position
		resultp = data.get(position);

		txt_details_tag = (TextView) itemView.findViewById(R.id.txt_tag);
		txt_details_count = (TextView) itemView
				.findViewById(R.id.txt_total_count);
		txt_details_sum = (TextView) itemView.findViewById(R.id.txt_total_sum);

		// Float f=
		// Float.parseFloat(resultp.get(TodaysDetails.TODAYS_TOTAL_SUM));
		// int int_total_sum = (int)Math.round(f);

		// Float float_count=
		// Float.parseFloat(resultp.get(TodaysDetails.TODAYS_TOTAL_COUNT));
		// int int_total_count = (int)Math.round(float_count);

		if (position == 0) {
			txt_details_sum.setVisibility(View.GONE);

		}

		if (position == 1) {
			txt_details_sum.setVisibility(View.GONE);

		}
		if (position == 2) {
			txt_details_sum.setVisibility(View.GONE);

		}

		if (position != 7) {

			txt_details_sum.setText(" Rs."
					+ resultp.get(TodaysDetails.TODAYS_TOTAL_SUM));
			txt_details_count.setText(resultp.get(TodaysDetails.TODAYS_TOTAL_COUNT));
			txt_details_tag.setText(resultp.get(TodaysDetails.TODAYS_TAG));
		}
		userFunction = new UserFunctions();

		itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);

				new Thread(new Runnable() {
					public void run() {
						// Get the position
						resultp = data.get(position);
						/*
						 * Intent messagingActivity = new Intent(context,
						 * ACMActivity.class);
						 * messagingActivity.putExtra("emp_id",
						 * resultp.get(SecondActivity.ACM_USER_ID));
						 * messagingActivity.putExtra("emp_name",
						 * resultp.get(SecondActivity.txt_1));
						 * messagingActivity.putExtra("str_region",
						 * SecondActivity.str_region);
						 * 
						 * messagingActivity.putExtra("acmjsonobject",
						 * acmjsonobject.toString());
						 * 
						 * messagingActivity
						 * .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						 * context.startActivity(messagingActivity);
						 * 
						 * /*
						 * 
						 * // Creating a fragment object DashboardFragment
						 * rFragment = new DashboardFragment();
						 * 
						 * // Creating a Bundle object Bundle data = new
						 * Bundle();
						 * 
						 * // Setting the index of the currently selected item
						 * of // mDrawerList
						 * 
						 * data.putString("emp_id",
						 * resultp.get(SecondActivity.ACM_USER_ID));
						 * data.putString("emp_name",
						 * resultp.get(SecondActivity.txt_1));
						 * 
						 * // Setting the position to the fragment
						 * rFragment.setArguments(data);
						 * 
						 * // Getting reference to the FragmentManager
						 * FragmentManager fragmentManager = ((Activity)
						 * context).getFragmentManager();
						 * 
						 * // Creating a fragment transaction
						 * FragmentTransaction ft =
						 * fragmentManager.beginTransaction();
						 * 
						 * // Adding a fragment to the fragment transaction
						 * ft.replace(R.id.content_frame, rFragment);
						 * 
						 * // Committing the transaction ft.commit();
						 * 
						 * SecondActivity.mDrawerLayout.closeDrawer(SecondActivity
						 * .drawerll);
						 */
					}
				}).start();
			}

		});

		return itemView;
	}
}
