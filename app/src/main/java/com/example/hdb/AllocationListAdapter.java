package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

/**
 * Created by Avinash on 01-07-2015
 */
public class AllocationListAdapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunctions user_function;

    //ViewHolder holder = null;

    //View itemView;
    public AllocationListAdapter(Context context,
                                 ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        View itemView = null;
        ViewHolder holder;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.allocationlist_item, parent,
                    false);

            holder = new ViewHolder();
            holder.tv_name = (TextView) itemView
                    .findViewById(R.id.txt_1);
            holder.tv_resolv_flag = (TextView) itemView
                    .findViewById(R.id.txt_2);
            holder.tv_updated_date = (TextView) itemView
                    .findViewById(R.id.txt_3);
            holder.tv_is_date = (TextView) itemView
                    .findViewById(R.id.txt_4);

            itemView.setTag(holder);
        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }
        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }
        String new_date = resultp.get(AllocationList.GRID_IS_TODAY).toString();

        if (new_date.equals("Y")) {
            // itemView.setBackgroundColor(context.getResources().getColor(R.color.list_color_2));
            holder.tv_is_date.setText("New  Allocation");
            holder.tv_is_date.setVisibility(View.VISIBLE);
        }

        if (resultp.get(AllocationList.GRID_IS_NEW_ALLOC).toString().equals("NA")) {
            // itemView.setBackgroundColor(context.getResources().getColor(R.color.list_color_2));
            itemView.setBackgroundColor(context.getResources().getColor(R.color.list_color_5));

        }

        if (resultp.get(AllocationList.GRID_REPORT1).toString() != null) {
            holder.tv_name.setText(resultp.get(AllocationList.GRID_REPORT1));
            holder.tv_name.setVisibility(View.VISIBLE);
        }
        if (resultp.get(AllocationList.GRID_ME_STATUS_WEEK).toString() != "null") {
            holder.tv_resolv_flag.setText(resultp.get(AllocationList.GRID_ME_STATUS_WEEK));
            holder.tv_resolv_flag.setVisibility(View.VISIBLE);
        }

        if (resultp.get(AllocationList.GRID_DATE).toString() != "null") {
            holder.tv_updated_date.setText(resultp.get(AllocationList.GRID_DATE));
            holder.tv_updated_date.setVisibility(View.VISIBLE);
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                arg0.setSelected(true);

                new Thread(new Runnable() {
                    public void run() {
                        resultp = data.get(position);
                        Intent messagingActivity = new Intent(context,
                                AllocationEdit.class);
                        messagingActivity.putExtra("los_id",
                                resultp.get(AllocationList.GRID_LOAN_NO));
                        messagingActivity.putExtra("me_status",
                                resultp.get(AllocationList.GRID_ME_STATUS));
                        messagingActivity.putExtra("me_week",
                                resultp.get(AllocationList.GRID_ME_WEEK));
                        messagingActivity.putExtra("user_id",
                                resultp.get(AllocationList.GRID_USER_ID));
                        messagingActivity.putExtra("resi_address",
                                resultp.get(AllocationList.GRID_RESI_ADDRESS));
                        messagingActivity.putExtra("resi_contact",
                                resultp.get(AllocationList.GRID_RESI_CONTACT));
                        messagingActivity.putExtra("ofc_address",
                                resultp.get(AllocationList.GRID_OFC_ADDRESS));
                        messagingActivity.putExtra("ofc_contact",
                                resultp.get(AllocationList.GRID_OFC_CONTACT));
                        messagingActivity.putExtra("cust_name",
                                resultp.get(AllocationList.GRID_CUST_NAME));
                        messagingActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(messagingActivity);
                    }
                }).start();

            }

        });


        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_name;
        public TextView tv_resolv_flag;
        public TextView tv_updated_date;
        public TextView tv_is_date;


    }
}

