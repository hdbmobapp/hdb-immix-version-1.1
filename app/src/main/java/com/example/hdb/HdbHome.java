package com.example.hdb;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.MyUploads.E_reciept_genrate;
import com.example.hdb.My_PickupType.pickup_type_main;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.DatabaseHandler;
import info.hdb.libraries.ExampleDBHelper;
import info.hdb.libraries.UserFunctions;

public class HdbHome extends AppCompatActivity {
    UserFunctions userfunction;
    JSONObject jsonobject = null;
    UserFunctions userFunction;
    public static final String KEY_STATUS = "status";
    String str_user_id, str_user_name, str_branch;
    public static String msg_count = "0";
    TextView txt_user_id, txt_user_name, txt_branch;
    SharedPreferences pref;
    ListView list;
    public static String  str_paynimo;

    String[] itemname = {"Data Entry", "My Uploads","Upload In Progress", "Allocations",
            "Message Box","Contact Us","FAQ" };
    Integer[] imgid = { R.drawable.data_entry,  R.drawable.efficiancy, R.drawable.upload,
            R.drawable.alloc, R.drawable.mail_box, R.drawable.contact_ud, R.drawable.faqs};

    String[] itemname_co = {"Data Entry", "My Uploads","Upload In Progress", "Allocations",
            "Message Box",  "Efficiency Report", "Search last update Details",
            "Search New Contact Details","Search LOS Details","Reports","Online Payment","Online Payment Status","Schedule Pickup","E-Reciept"," My Upload Locc ","My Upload  Genr ","Contact Update","Contact Us","FAQ"};
    Integer[] imgid_co = {R.drawable.data_entry,  R.drawable.efficiancy, R.drawable.upload,
            R.drawable.alloc, R.drawable.mail_box, R.drawable.reprt,
            R.drawable.search_los, R.drawable.contact_search, R.drawable.search_los,
            R.drawable.reprt, R.drawable.online_pay, R.drawable.payment_status, R.drawable.pickup,R.drawable.e_receipt, R.drawable.invoice_paid, R.drawable.invoice_paid,R.drawable.update_contact, R.drawable.contact_ud, R.drawable.faqs};


    ProgressDialog mProgressDialog;

    ConnectionDetector cd;
    HdbHomeListAdapter adapter;
    String str_designation;
    Integer is_login,off_is_login;
    String str_designation_val;
    public static String str_total;
    RequestDB rdb;
    int total_int=0;
    ExampleDBHelper eDB;
    DatabaseHandler db;
    HashMap<String,String> user = new HashMap<String,String>();
    private String android_id;

    Integer off_rowcount;

    @Override
    protected void onStart(){
        super.onStart();
        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.commit();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hdb_home);
        userfunction = new UserFunctions();
        cd = new ConnectionDetector(com.example.hdb.HdbHome.this);

        rdb=new RequestDB(getApplicationContext());
        userFunction = new UserFunctions();

        eDB=new ExampleDBHelper(this);

        db=new DatabaseHandler(this);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        str_branch = pref.getString("user_brnch", null);
        str_designation= pref.getString("user_designation", "NO");
        str_designation_val= pref.getString("user_designation_value", "0");
        str_paynimo= pref.getString("paynimo", "0");

      //  userFunction.checkforLogout(pref, HdbHome.this);

        is_login = pref.getInt("is_login", 0);

        off_is_login = pref.getInt("off_is_login", 0);

        android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);





            SharedPreferences.Editor peditor = pref.edit();
            peditor.putString("is_locate_clicked", "");
            peditor.commit();



        if (userfunction.isUserLoggedIn(com.example.hdb.HdbHome.this) && is_login > 0  ) {
            System.out.println("isUserLoggedIn:::"+off_is_login);

            SharedPreferences settings = getSharedPreferences("prefs", 0);
            boolean firstRun = settings.getBoolean("firstRun", true);

            if (firstRun) {
                // here run your first-time instructions, for example :
                // startActivityForResult( new Intent(SplashScreen.this,
                // InstructionsActivity.class),INSTRUCTIONS_CODE);
                Intent iHomeScreen = new Intent(HdbHome.this,
                        Update_contact.class);
                startActivity(iHomeScreen);
            }
            this.SetLayout();
        } else {

            user=   db.off_getUserDetails();


            String val=(String)user.get("uid");
            String dev_val=(String)user.get("dev_id");

            off_rowcount=db.off_getRowCount();
            if(off_rowcount>0) {



                if (!dev_val.equals("")) {
                    if (userfunction.off_isUserLoggedIn(com.example.hdb.HdbHome.this) && off_is_login > 0 && dev_val.equals(android_id))

                    {
                        System.out.println("off_isUserLoggedIn:::" + off_is_login);
                        this.SetLayout();

                    } else {
                        System.out.println("redirect login:::" + off_is_login);
                        Intent messagingActivity = new Intent(com.example.hdb.HdbHome.this,
                                LoginActivity.class);
                        messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(messagingActivity);
                        finish();
                        // this.SetLayout();
                    }
                }else{
                    if (userfunction.off_isUserLoggedIn(com.example.hdb.HdbHome.this) && off_is_login > 0) {

                        System.out.println("off_isUserLoggedIn:::" + off_is_login);
                        this.SetLayout();
                    }else {
                        System.out.println("redirect login vcvbv:::" + off_is_login);
                        Intent messagingActivity = new Intent(com.example.hdb.HdbHome.this,
                                LoginActivity.class);
                        messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(messagingActivity);
                        finish();
                    }
                }

            }else{
                System.out.println("redirect login:::" + off_is_login);
                Intent messagingActivity = new Intent(com.example.hdb.HdbHome.this,
                        LoginActivity.class);
                messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(messagingActivity);
                finish();
            }
        }

           // System.out.println("HEHHUHU");

        RequestDB rdb = new RequestDB(com.example.hdb.HdbHome.this);

        int int_new_caset_pending_count = 0;
        int Pending_Photo_Count = 0;


        int_new_caset_pending_count = Integer.parseInt(rdb.getPendingCount());
        Pending_Photo_Count = Integer.parseInt(eDB.getPendingPhotoCount());

        total_int = int_new_caset_pending_count  + Pending_Photo_Count;
        str_total = String.valueOf(total_int);


        if (cd.isConnectingToInternet()) {
            new GetDashboardInfo().execute();
        }


        if(str_designation.equals("agency")){
            adapter = new HdbHomeListAdapter(this, itemname, imgid);

        }else {
            adapter = new HdbHomeListAdapter(this, itemname_co, imgid_co);

        }

        System.out.println("off Login HEHE:::"+off_is_login);

        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (is_login > 0  || off_is_login>0) {
                    System.out.println("IF::::");

                    GoTo(position);
                } else {
                    System.out.println("ELSE::::");
                    Intent messagingActivity = new Intent(com.example.hdb.HdbHome.this,
                            LoginActivity.class);
                    messagingActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(messagingActivity);
                    finish();

                }
            }
        });
    }

    protected void SetLayout() {
        txt_user_id = (TextView) findViewById(R.id.txt_user_id);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_branch = (TextView) findViewById(R.id.txt_branch);
        txt_user_id.setText(str_user_id);
        txt_user_name.setText(str_user_name);
        txt_branch.setText(str_branch);

        // txt_user_id.setTypeface(font);
        // txt_user_n)ame.setTypeface(font);
        // txt_branch.setTypeface(font);

    }

    public void GoTo(Integer i) {
        if(total_int>0){
            userFunction.cutomToast("Please upload data in Upload in progress tab ....Pending Count="+str_total, com.example.hdb.HdbHome.this);
        }
        switch (i) {

            case 0:
                System.out.println("CASE 1 ::::");
                Intent messagingActivity = new Intent(com.example.hdb.HdbHome.this, EditRequest.class);
                startActivity(messagingActivity);

                break;

            case 1:
               // Intent effe = new Intent(com.example.hdb.HdbHome.this, MyCases.class);
                Intent effe = new Intent(com.example.hdb.HdbHome.this, com.example.hdb.MyCases.class);
                startActivity(effe);
                //userfunction.cutomToast("To be updated..", HdbHome.this);
                break;

            case 2:
                Intent details = new Intent(com.example.hdb.HdbHome.this, Dashboard.class);
                details.putExtra("user_id", str_user_id);
                startActivity(details);
                break;

            case 3:
                Intent loandetails = new Intent(com.example.hdb.HdbHome.this, AllocationList.class);
                startActivity(loandetails);
                //userfunction.cutomToast("To be updated..", HdbHome.this);
                break;

            case 4:
                Intent messagebox = new Intent(com.example.hdb.HdbHome.this, MessageBox.class);
                startActivity(messagebox);

                break;

            case 5:
                Intent todyosdetails_activity;
                if(str_designation.equals("agency")){
                     todyosdetails_activity = new Intent(com.example.hdb.HdbHome.this, Contact_us.class);
                }else {
                     todyosdetails_activity = new Intent(com.example.hdb.HdbHome.this, TodaysDetails.class);
                    todyosdetails_activity.putExtra("user_id", str_user_id);
                }
                startActivity(todyosdetails_activity);

                break;

            case 6:
                Intent reports;
                if(str_designation.equals("agency")){
                    reports = new Intent(com.example.hdb.HdbHome.this, FAQ.class);
                }else {
                    reports = new Intent(com.example.hdb.HdbHome.this, Loan_Trails.class);
                }
                startActivity(reports);

                break;


            case 7:
                Intent loan_search = new Intent(com.example.hdb.HdbHome.this,
                        NewContactList.class);
                startActivity(loan_search);

                break;

            case 8:
                Intent los_detailsearch = new Intent(com.example.hdb.HdbHome.this,
                        Loan_Search_Details.class);
                startActivity(los_detailsearch);
                break;

            case 9:
                Intent mobile_usage = new Intent(com.example.hdb.HdbHome.this,
                        Reports.class);
                startActivity(mobile_usage);
                break;

            case 10:
                Intent ol_payment = new Intent(com.example.hdb.HdbHome.this, Online_payment.class);
                ol_payment.putExtra("module_g", "genrr");
                ol_payment.putExtra("module_l", "locc");
                startActivity(ol_payment);

                break;

            case 11:
                Intent ol_payment_st = new Intent(com.example.hdb.HdbHome.this, Online_payment_status.class);
                startActivity(ol_payment_st);

                break;


            case 12:
                Intent s = new Intent(com.example.hdb.HdbHome.this, pickup_type_main.class);
                s.putExtra("pickup_type","Schedule");
                startActivity(s);
                break;

            /*case 13:
                Intent a = new Intent(com.example.hdb.HdbHome.this, pickup_type_main.class);
                a.putExtra("pickup_type","Assign");
                startActivity(a);
                break;*/

            case 13:
                if (cd.isConnectingToInternet()) {
                  Intent ercpt = new Intent(com.example.hdb.HdbHome.this, Ercpt_activity_main.class);
                    ercpt.putExtra("from","home");
                    ercpt.putExtra("str_id","000");
                  startActivity(ercpt);
                }else
                {
                    Toast.makeText(com.example.hdb.HdbHome.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case 14:
                Intent ercpt_gen = new Intent(com.example.hdb.HdbHome.this,
                        E_reciept_genrate.class);
                ercpt_gen.putExtra("module", "genrr");
                startActivity(ercpt_gen);
                break;

            case 15:
                Intent ercpt_locc = new Intent(com.example.hdb.HdbHome.this,
                        E_reciept_genrate.class);
                ercpt_locc.putExtra("module", "locc");
                startActivity(ercpt_locc);
                break;

            case 16:
                Intent contact_update = new Intent(com.example.hdb.HdbHome.this,
                        Update_contact.class);
                startActivity(contact_update);
                break;

            case 17:
                Intent contact_us = new Intent(com.example.hdb.HdbHome.this,
                    Contact_us.class);
                startActivity(contact_us);
                break;

            case 18:
                Intent faq = new Intent(com.example.hdb.HdbHome.this,
                        FAQ.class);
                startActivity(faq);
                break;

            default:

                break;

        }

    }

    public void logout(View v) {
        // do stuff

        Intent hme = new Intent(com.example.hdb.HdbHome.this,
                Home_PD_COLLX.class);
        startActivity(hme);

       // alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                com.example.hdb.HdbHome.this.finish();
                                @SuppressWarnings("unused")
                                boolean islogoutBoolean=true ;



                                SharedPreferences.Editor editor = pref.edit();

                                editor.putInt("off_is_login", 0);
                                editor.commit();

                               islogoutBoolean = userfunction
                                      .logoutUser(getApplicationContext());

                               /* off_islogoutBoolean = userfunction
                                        .off_logoutUserclear(getApplicationContext());*/

                                if (islogoutBoolean == true ) {
                                    Intent i = new Intent(
                                            com.example.hdb.HdbHome.this, LoginActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                }
                            }
                        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public class GetDashboardInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            if (cd.isConnectingToInternet()) {
                jsonobject = userFunction.get_msg_count(str_user_id);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    // text_error_msg.setVisibility(View.GONE);

                    // Log.d("jsonobject OP : " , jsonobject.toString() );
                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            msg_count = jsonobject.getString("msg_count");
                            list.setAdapter(adapter);

                        }
                    }
                }
                // mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }
}