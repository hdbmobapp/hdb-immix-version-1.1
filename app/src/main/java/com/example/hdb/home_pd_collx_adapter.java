package com.example.hdb;

import android.widget.ArrayAdapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class home_pd_collx_adapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] app;
    private final Integer[] imageId;

    public home_pd_collx_adapter(Activity context, String[] app, Integer[] imageId) {
        super(context, R.layout.home_pd_collx_itemlist, app);
        this.context = context;
        this.app = app;
        this.imageId = imageId;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.home_pd_collx_itemlist, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imgview);
        txtTitle.setText(app[position]);

        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}