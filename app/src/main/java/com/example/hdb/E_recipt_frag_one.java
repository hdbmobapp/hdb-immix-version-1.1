package com.example.hdb;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.MyAdapter;
import info.hdb.libraries.UserFunctions;


/**
 * Created by Administrator on 20-07-2018.
 */
public class E_recipt_frag_one extends Fragment {

    ProgressDialog mProgressDialog;

    Button Next, btn_search, txt_home,btn_reset;
    EditText edt_loan_no, edt_cust_name, edt_product, edt_branchid, edt_branch, edt_contact_no, edt_uniqueid,
            edt_total_emi_due, edt_pickup_type,edt_brch_gst_sc,edt_branch_state,
            edt_cust_gst_sc,edt_cust_state,edt_gstn,edt_cust_id;
    Spinner spinner_payment_mode,spinner_loc_gen;
    LinearLayout lay_main,tableRow_br_gst,tableRow_br_state,tableRow_cust_gst,tableRow_cust_state,
                 tableRow_gstn,tableRow_cust_id;
    private ProgressBar progress_bar;


    String Paymode_arraylist[] = {"Select Payment Mode", "Cash", "Cheque", "DD"};
    String loc_gen_arraylist[] = {"Select E-receipt Module", "Locc", "Genr"};

    String str_pay_mode,str_loc_gen,str_product,str_loan_no,str_cust_name,str_branch_id,str_branch,
            str_contact_no,str_unique_id,str_total_emi_due,str_pickup_type,str_brch_gst_sc,
            str_branch_state,str_cust_gst_sc,str_cust_state,str_gstn,str_cust_id,user_id;
    String str_lat,str_long;
    Double  longitude ,latitude;
    Boolean ins_new = true;
    JSONObject jsonobject =null;
    ArrayList<String> module_arraylist;

    Boolean ins = true;
    ConnectionDetector cd;
    public boolean isLocationEnabled=false;

    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    static UserFunctions userFunction;
    SharedPreferences pData,pref;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.e_recipt_frag_one, container, false);
        userFunction = new UserFunctions();
        cd = new ConnectionDetector(getActivity().getApplicationContext());
        isLocationEnabled=((Ercpt_activity_main)getActivity()).checkLocation();
        module_arraylist = new ArrayList<String>();

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 -
        user_id = pref.getString("user_id", null);
        System.out.println("hghjghjghjgh"+user_id);

        /* Button*/
        Next =  view.findViewById(R.id.btn_next);
        btn_search =  view.findViewById(R.id.btn_search);
        txt_home =  view.findViewById(R.id.txt_home);
        btn_reset =  view.findViewById(R.id.btn_reset);

        /* EditText*/
        edt_loan_no =  view.findViewById(R.id.edt_loan_no);
        edt_cust_name =  view.findViewById(R.id.edt_cust_name);
        edt_product =  view.findViewById(R.id.edt_product);
        edt_branchid =  view.findViewById(R.id.edt_branchid);
        edt_branch =  view.findViewById(R.id.edt_branch);
        edt_contact_no =  view.findViewById(R.id.edt_contact_no);
        edt_uniqueid =  view.findViewById(R.id.edt_uniqueid);
        edt_total_emi_due =  view.findViewById(R.id.edt_total_emi_due);
        edt_pickup_type =  view.findViewById(R.id.edt_pickup_type);
        edt_brch_gst_sc =  view.findViewById(R.id.edt_brch_gst_sc);
        edt_branch_state =  view.findViewById(R.id.edt_branch_state);
        edt_cust_gst_sc =  view.findViewById(R.id.edt_cust_gst_sc);
        edt_cust_state =  view.findViewById(R.id.edt_cust_state);
        edt_gstn =  view.findViewById(R.id.edt_gstn);
        edt_cust_id =  view.findViewById(R.id.edt_cust_id);


        /* Linerr layout*/
        lay_main =  view.findViewById(R.id.lay_main);
        tableRow_br_gst =  view.findViewById(R.id.tableRow_br_gst);
        tableRow_br_state =  view.findViewById(R.id.tableRow_br_state);
        tableRow_cust_gst =  view.findViewById(R.id.tableRow_cust_gst);
        tableRow_cust_state =  view.findViewById(R.id.tableRow_cust_state);
        tableRow_gstn =  view.findViewById(R.id.tableRow_gstn);
        tableRow_cust_id =  view.findViewById(R.id.tableRow_cust_id);



        /* Spinner*/
        spinner_payment_mode =  view.findViewById(R.id.spinner_payment_mode);
        spinner_loc_gen =  view.findViewById(R.id.spinner_loc_gen);
        progress_bar = view. findViewById(R.id.progress_bar);


        edt_pickup_type.setEnabled(false);

        edt_cust_name.setEnabled(false);
        edt_product.setEnabled(false);
        edt_branchid.setEnabled(false);
        edt_branch.setEnabled(false);
        edt_contact_no.setEnabled(false);
        edt_uniqueid.setEnabled(false);
        edt_total_emi_due.setEnabled(false);
        edt_brch_gst_sc.setEnabled(false);
        edt_branch_state.setEnabled(false);
        edt_cust_gst_sc.setEnabled(false);
        edt_cust_state.setEnabled(false);
        edt_gstn.setEnabled(false);
        edt_cust_id.setEnabled(false);


        isLocationEnabled=((Ercpt_activity_main)getActivity()).checkLocation();


        if(isLocationEnabled==true) {

            longitude = Ercpt_activity_main.lat;
            latitude = Ercpt_activity_main.lon;
            if (latitude != null) {
                str_lat = Double.toString(latitude);
                str_long = Double.toString(longitude);
            }
        }





        ArrayAdapter<String> spinnerArrayAdapteter_loc_gen = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit,loc_gen_arraylist);
        spinnerArrayAdapteter_loc_gen.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_loc_gen.setAdapter(spinnerArrayAdapteter_loc_gen);
        spinner_loc_gen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_loc_gen.getSelectedItemPosition() == 0) {
                    str_loc_gen = null;
                    if (str_loc_gen == null) {
                        cleardata();
                        lay_main.setVisibility(View.GONE);

                    }
                } else {
                    edt_loan_no.setText("");
                    String str_from =Ercpt_activity_main.str_from.toString();
                    System.out.println("str_from:str_fromstr_from:str_from"+str_from);
                    if(str_from.equals("pickup")){

                        str_loan_no = Ercpt_activity_main.str_losid;

                        //  cleardata();
                        edt_loan_no.setText(str_loan_no);

                        if (cd.isConnectingToInternet()) {
                            if (ins == true) {
                                str_loan_no = edt_loan_no.getText().toString();
                                if (str_loan_no.isEmpty()) {
                                    Toast.makeText(getContext(), "Enter Your loan No", Toast.LENGTH_SHORT).show();
                                } else {
                                    new download_locc().execute();
                                    // System.out.println("succesfull:::::::::::::::::::+++");
                                }

                            }
                        } else {
                            userFunction.cutomToast("No internet Connection  ", getContext());

                        }

                    }

                    //  cleardata();

                    str_loc_gen = spinner_loc_gen.getItemAtPosition(spinner_loc_gen.getSelectedItemPosition()).toString();
                    if (str_loc_gen.contains("Locc")) {
                        lay_main.setVisibility(View.VISIBLE);
                        tableRow_br_gst.setVisibility(View.GONE);
                        tableRow_br_state.setVisibility(View.GONE);
                        tableRow_cust_gst.setVisibility(View.GONE);
                        tableRow_cust_state.setVisibility(View.GONE);
                        tableRow_gstn.setVisibility(View.GONE);
                        tableRow_cust_id.setVisibility(View.GONE);

                    }
                    if (str_loc_gen.contains("Genr")) {
                        lay_main.setVisibility(View.VISIBLE);
                        tableRow_br_gst.setVisibility(View.VISIBLE);
                        tableRow_br_state.setVisibility(View.VISIBLE);
                        tableRow_cust_gst.setVisibility(View.VISIBLE);
                        tableRow_cust_state.setVisibility(View.VISIBLE);
                        tableRow_gstn.setVisibility(View.VISIBLE);
                        tableRow_cust_id.setVisibility(View.VISIBLE);

                    }
                    //Toast.makeText(getContext(), str_pay_mode, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        String str_from1 =Ercpt_activity_main.str_from.toString();
        if(str_from1.equals("pickup")) {
            spinner_loc_gen.setSelection(1);
        }



        ArrayAdapter<String> spinnerArrayAdapteterpaymode = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, Paymode_arraylist);
        spinnerArrayAdapteterpaymode.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_payment_mode.setAdapter(spinnerArrayAdapteterpaymode);
        spinner_payment_mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_payment_mode.getSelectedItemPosition() == 0) {
                    str_pay_mode = null;
                } else {
                    str_pay_mode = spinner_payment_mode.getItemAtPosition(spinner_payment_mode.getSelectedItemPosition()).toString();
                    //Toast.makeText(getContext(), str_pay_mode, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cleardata();
                if (cd.isConnectingToInternet()) {
                    if (ins == true) {
                        str_loan_no = edt_loan_no.getText().toString();

                            if (str_loan_no.isEmpty()) {
                                Toast.makeText(getContext(), "Enter Your loan No", Toast.LENGTH_SHORT).show();
                            } else {
                                new download_locc().execute();
                               // System.out.println("succesfull:::::::::::::::::::+++");
                            }

                    }
                } else {
                    userFunction.cutomToast("No internet Connection  ", getContext());

                }

            }
        });

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt_loan_no.setText("");
                cleardata();


            }
        });
        if (cd.isConnectingToInternet()) {
            Next.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            str_cust_name = edt_cust_name.getText().toString();
                                         if(str_cust_name!=null) {
                                            // System.out.println("dasfsdfsdf"+str_cust_name);
                                             new movedata().execute();
                                         }else{
                                            // System.out.println("dasfsdfsdf"+str_cust_name);
                                             userFunction.cutomToast("Invalid Loan No Please Enter Valid Number ", getContext());
                                         }
                                        }

                                    });

        }else {
            userFunction.cutomToast("No internet Connection  ", getContext());
        }

        return view;
    }

    public class download_locc extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonobject=null;
            System.out.println("str_loan_nos::::::::::::::::::::" + str_loan_no);
            if (str_loc_gen.contains("Locc")) {
                try {
                    jsonobject= Ercpt_activity_main.get_lms_loccc_data(str_loan_no,user_id,str_lat,str_long);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                try {
                    jsonobject= Ercpt_activity_main.get_los_genrr_e_data(str_loan_no,user_id,str_lat,str_long);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            try {
                if (jsonobject != null) {
                    String KEY_STATUS = "status";
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            if (str_loc_gen.contains("Locc")) {

                                edt_cust_name.setText(Ercpt_activity_main.cust_name);
                                edt_product.setText(Ercpt_activity_main.prod);
                                edt_branchid.setText(Ercpt_activity_main.branchid);
                                edt_branch.setText(Ercpt_activity_main.branch);
                                edt_contact_no.setText(Ercpt_activity_main.contact_no);
                                edt_uniqueid.setText(Ercpt_activity_main.unqid);
                                edt_total_emi_due.setText(Ercpt_activity_main.total_emi_due);
                                edt_pickup_type.setText(Ercpt_activity_main.pickuptype);
                            }

                            if (str_loc_gen.contains("Genr")) {
                                edt_cust_name.setText(Ercpt_activity_main.cust_name);
                                edt_product.setText(Ercpt_activity_main.prod);
                                edt_branchid.setText(Ercpt_activity_main.branchid);
                                edt_branch.setText(Ercpt_activity_main.branch);
                                edt_contact_no.setText(Ercpt_activity_main.contact_no);
                                edt_uniqueid.setText(Ercpt_activity_main.unqid);
                                edt_total_emi_due.setText(Ercpt_activity_main.total_emi_due);

                                edt_brch_gst_sc.setText(Ercpt_activity_main.branch_gst_sc);
                                edt_branch_state.setText(Ercpt_activity_main.branch_state);
                                edt_cust_gst_sc.setText(Ercpt_activity_main.cust_gst_sc);
                                edt_cust_state.setText(Ercpt_activity_main.cust_state);
                                edt_gstn.setText(Ercpt_activity_main.gstn);
                                edt_cust_id.setText(Ercpt_activity_main.cust_id);
                                edt_pickup_type.setText(Ercpt_activity_main.pickuptype);
                            }

                        } else {
                            userFunction.cutomToast(jsonobject.getString("message"), getActivity().getApplicationContext());
                        }


                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            edt_loan_no.setEnabled(false);
            mProgressDialog.dismiss();

    }
    }



    protected void cleardata(){
        edt_loan_no.setEnabled(true);

        edt_cust_name.setText("");
        edt_product.setText("");
        edt_branchid.setText("");
        edt_branch.setText("");
        edt_contact_no.setText("");
        edt_uniqueid.setText("");
        edt_total_emi_due.setText("");

        edt_brch_gst_sc.setText("");
        edt_branch_state.setText("");
        edt_cust_gst_sc.setText("");
        edt_cust_state.setText("");
        edt_gstn.setText("");
        edt_cust_id.setText("");



    }


    public class movedata extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage("Please Wait.....");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

         }

        @Override
        protected Void doInBackground(Void... params) {

            ins_new = true;
            str_loan_no = edt_loan_no.getText().toString();
            str_product = edt_product.getText().toString();
            str_cust_name = edt_cust_name.getText().toString();
            str_branch_id = edt_branchid.getText().toString();
            str_branch = edt_branch.getText().toString();
            str_contact_no = edt_contact_no.getText().toString();
            str_unique_id = edt_uniqueid.getText().toString();
            str_total_emi_due = edt_total_emi_due.getText().toString();
            str_pickup_type = edt_pickup_type.getText().toString();
            str_brch_gst_sc = edt_brch_gst_sc.getText().toString();
            str_branch_state = edt_branch_state.getText().toString();
            str_cust_gst_sc = edt_cust_gst_sc.getText().toString();
            str_cust_state = edt_cust_state.getText().toString();
            str_gstn = edt_gstn.getText().toString();
            str_cust_id = edt_cust_id.getText().toString();

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {


            if(str_loc_gen.contains("Locc")) {

                if (str_loan_no.equals("") && ins_new == true) {
                    ins_new = false;
                    edt_loan_no.setError("Please Search Your Loan No.");
                    edt_loan_no.requestFocus();
                }

               if(str_cust_name == null && ins_new == true){
                   Toast.makeText(getContext(), "Invalid Loan No", Toast.LENGTH_SHORT).show();
                   ins_new = false;
               }
            }


            if(str_loc_gen.contains("Genr")) {

                if (str_loan_no.equals("") && ins_new == true) {
                    ins_new = false;
                    edt_loan_no.setError("Please Search Your Loan No.");
                    edt_loan_no.requestFocus();
                }

                if(str_cust_name == null && ins_new == true){
                    Toast.makeText(getContext(), "Invalid Loan No", Toast.LENGTH_SHORT).show();
                    ins_new = false;
                }

            }

            if (str_pay_mode == null && ins_new == true) {
                ins_new = false;
                Toast.makeText(getContext(), "You did not Select Payment Mode", Toast.LENGTH_SHORT).show();
            }


            if (cd.isConnectingToInternet()) {
                if (ins_new == true) {
                    Fragment f = new E_recipt_frag_two();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    Bundle arg = new Bundle();

                    arg.putString("str_loc_gen", str_loc_gen);

                    if (str_loc_gen.contains("Locc")) {
                        arg.putString("str_pay_mode", str_pay_mode);
                        arg.putString("str_loan_no", str_loan_no);
                        arg.putString("str_product", str_product);
                        arg.putString("str_product_name", Ercpt_activity_main.prod_name);
                        arg.putString("str_cust_name", str_cust_name);
                        arg.putString("str_branch_id", str_branch_id);
                        arg.putString("str_branch", str_branch);
                        arg.putString("str_contact_no", str_contact_no);
                        arg.putString("str_unique_id", str_unique_id);
                        arg.putString("str_total_emi_due", str_total_emi_due);
                        arg.putString("str_pickup_type", str_pickup_type);
                        System.out.println("str_total_emi_due::::::::::::::::::" + str_total_emi_due);

                    }
                    if (str_loc_gen.contains("Genr")) {
                        arg.putString("str_pay_mode", str_pay_mode);
                        arg.putString("str_loan_no", str_loan_no);
                        arg.putString("str_product", str_product);
                        arg.putString("str_product_name", Ercpt_activity_main.prod_name);
                        arg.putString("str_cust_name", str_cust_name);
                        arg.putString("str_branch_id", str_branch_id);
                        arg.putString("str_branch", str_branch);
                        arg.putString("str_contact_no", str_contact_no);
                        arg.putString("str_unique_id", str_unique_id);
                        arg.putString("str_total_emi_due", str_total_emi_due);
                        arg.putString("str_pickup_type", str_pickup_type);

                        arg.putString("str_brch_gst_sc", str_brch_gst_sc);
                        arg.putString("str_branch_state", str_branch_state);
                        arg.putString("str_cust_gst_sc", str_cust_gst_sc);
                        arg.putString("str_cust_state", str_cust_state);
                        arg.putString("str_gstn", str_gstn);
                        arg.putString("str_cust_id", str_cust_id);

                        System.out.println("str_total_emi_due:fbfdbdbdb" + str_total_emi_due);

                    }
                    f.setArguments(arg);
                    fragmentTransaction.replace(R.id.container, f);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            } else {
                userFunction.cutomToast("No internet Connection  ", getContext());
            }
            mProgressDialog.dismiss();
           }
       }
    }







