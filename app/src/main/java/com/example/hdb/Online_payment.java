package com.example.hdb;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 06-08-2018.
 */
public class Online_payment extends AppCompatActivity {


    EditText edt_loan_no, edt_cust_name, edt_amt, edt_contact_no, edt_emp_con_no, edt_prod, edt_amount_new,edt_contact_no_new;
    Spinner spinner_prod;
    Button btn_submit, btn_search;

    ArrayList<String> prod_arraylist, producttype_val;
    String loc_gen_arraylist[] = {"Locc", "Genrr"};

    String str_prod, str_loan_no, str_cust_name, str_amt, str_cust_cont_no, str_emp_cont_no, str_new_amt,str_contact_no_new;
    String str_user_id, str_user_name;

    UserFunctions userFunction;
    RequestDB rdb;
    String str_module_g, str_module_l;

    ProgressBar progress_bar;
    JSONObject json;
    Boolean ins = true;
    ConnectionDetector cd;

    JSONObject jsonobject = null;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.online_payment);

        System.out.println("str_module_g::::-------------");

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        System.out.println("str_module_g::::-------------123");

        Intent i = getIntent();
        str_module_g = i.getStringExtra("module_g");
        System.out.println("str_module_g::::-------------" + str_module_g);
        str_module_l = i.getStringExtra("module_l");

        prod_arraylist = new ArrayList<>();
        producttype_val = new ArrayList<>();
        userFunction = new UserFunctions();
        rdb = new RequestDB(com.example.hdb.Online_payment.this);
        cd = new ConnectionDetector(com.example.hdb.Online_payment.this);

        prod_arraylist = rdb.get_masters_pd();

        producttype_val = rdb.get_masters_pd_val();
        btn_submit = (Button) findViewById(R.id.btn_submit);

        edt_loan_no = (EditText) findViewById(R.id.edt_loan_no);
        edt_cust_name = (EditText) findViewById(R.id.edt_cust_name);
        edt_amt = (EditText) findViewById(R.id.edt_amt);
        edt_contact_no = (EditText) findViewById(R.id.edt_contact_no);
        edt_emp_con_no = (EditText) findViewById(R.id.edt_emp_con_no);
        edt_prod = (EditText) findViewById(R.id.edt_prod);
        edt_amount_new = (EditText) findViewById(R.id.edt_amount_new);
        edt_contact_no_new = (EditText) findViewById(R.id.edt_contact_no_new);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);
        spinner_prod = (Spinner) findViewById(R.id.spinner_prod);
        btn_submit.setVisibility(View.GONE);

       /* ArrayAdapter<String> spinnerArrayAdapter = new MyAdapter(Online_payment.this, R.layout.sp_display_layout, R.id.txt_visit, prod_arraylist);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_prod.setAdapter(spinnerArrayAdapter);
        spinner_prod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_prod.getSelectedItemPosition() == 0) {
                    str_prod = null;
                } else {
                    str_prod = spinner_prod.getItemAtPosition(spinner_prod.getSelectedItemPosition()).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });*/

        btn_search = (Button) findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                str_loan_no = edt_loan_no.getText().toString();
                System.out.println("str_loan_no++++++++++::::::::::::::::::::" + str_loan_no);
                if (cd.isConnectingToInternet()) {
                    if (ins == true) {
                        System.out.println("str_loan_nos::::::::::::::::::::" + str_loan_no);
                        if (str_loan_no.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Enter Your loan No", Toast.LENGTH_SHORT).show();
                        } else {
                            new download_locc().execute();
                            System.out.println("succesfull:::::::::::::::::::+++");
                        }

                    }
                } else {
                    userFunction.cutomToast("No internet Connection  ", getApplicationContext());

                }

            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                str_loan_no = edt_loan_no.getText().toString();
                str_cust_name = edt_cust_name.getText().toString();
                str_amt = edt_amt.getText().toString();
                str_cust_cont_no = edt_contact_no.getText().toString();
                str_emp_cont_no = edt_emp_con_no.getText().toString();
                str_prod = edt_prod.getText().toString();
                str_new_amt = edt_amount_new.getText().toString();
                str_contact_no_new= edt_contact_no_new.getText().toString();

                if (cd.isConnectingToInternet()) {

                    ins = true;

                    if (str_loan_no.equals("") && ins == true) {
                        edt_loan_no.setError("Please Enter Loan No.");
                        edt_loan_no.requestFocus();
                        ins = false;
                    }

                    if (str_cust_name.equals("") && ins == true) {
                        edt_cust_name.setError("Please Enter Customer Name.");
                        edt_cust_name.requestFocus();
                        ins = false;
                    }

                    if (str_amt.equals("") && ins == true) {
                        edt_amt.setError("Please Enter Amount.");
                        edt_amt.requestFocus();
                        ins = false;
                    }

                    if (str_cust_cont_no.equals("") && ins == true) {
                        edt_contact_no.setError("Please Enter Customer Contact no.");
                        edt_contact_no.requestFocus();
                        ins = false;
                    }

                    if (str_emp_cont_no.equals("") && ins == true) {
                        edt_emp_con_no.setError("Please Enter Contact No.");
                        edt_emp_con_no.requestFocus();
                        ins = false;
                    }

                    if (str_prod.equals("") && ins == true) {
                        edt_prod.setError("Please Enter Product.");
                        edt_prod.requestFocus();
                        ins = false;
                    }

                    if (str_new_amt.equals("") && ins == true) {
                        edt_amount_new.setError("Please Enter Amount.");
                        edt_amount_new.requestFocus();
                        ins = false;
                    }

                    if (str_contact_no_new.equals("") && ins == true) {
                        edt_contact_no_new.setError("Please Enter Customer Contact no.");
                        edt_contact_no_new.requestFocus();
                        ins = false;
                    }


                    if (ins == true) {
                        new SubmitData().execute();

                    }
                } else {
                    userFunction.cutomToast("No internet Connection", com.example.hdb.Online_payment.this);

                }
            }

        });


    }

    public class SubmitData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            jsonobject = userFunction.submit_data(str_prod, str_loan_no, str_cust_name,
                    str_amt, str_cust_cont_no, str_emp_cont_no, str_user_id, str_new_amt,str_contact_no_new);


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            String succ_msg = jsonobject.getString("message");

                            userFunction.cutomToast(succ_msg,
                                    com.example.hdb.Online_payment.this);
                            Intent messagingActivity = new Intent(
                                    com.example.hdb.Online_payment.this, HdbHome.class);
                            startActivity(messagingActivity);

                        } else {
                            String error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg,
                                    com.example.hdb.Online_payment.this);

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            progress_bar.setVisibility(View.GONE);

        }
    }

    public class download_locc extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            progress_bar.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonobject = null;
            if (str_module_l.contains("locc")) {
                jsonobject = userFunction.get_lms_Locc_data(str_loan_no,str_user_id,"","");


            } else {
                jsonobject = userFunction.get_los_genrr_data(str_loan_no,str_user_id,"","");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {


                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    // txt_error.setText(getResources().getString(R.string.error_message));
                    // txt_error.setVisibility(View.VISIBLE);
                } else {
                    btn_submit.setVisibility(View.VISIBLE);
                    if (str_module_l.contains("locc")) {

                        edt_cust_name.setText(jsonobject.getString("CUSTOMER_NAME"));
                        edt_prod.setText(jsonobject.getString("PRODUCT"));
                        edt_contact_no.setText(jsonobject.getString("RESI_MOBILE"));
                        edt_amt.setText(jsonobject.getString("Amt_Overdue"));
                        edt_contact_no_new.setText(jsonobject.getString("RESI_MOBILE"));


                    }

                    if (str_module_g.contains("genrr")) {
                        edt_cust_name.setText(jsonobject.getString("CUSTOMER_NAME"));
                        edt_prod.setText(jsonobject.getString("PRODUCT"));
                        edt_contact_no.setText(jsonobject.getString("RESI_MOBILE"));
                        edt_amt.setText(jsonobject.getString("Amt_Overdue"));
                        edt_contact_no_new.setText(jsonobject.getString("RESI_MOBILE"));


                    }
                }

                progress_bar.setVisibility(View.GONE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }

}


