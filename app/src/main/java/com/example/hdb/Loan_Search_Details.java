package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 12-10-2015.
 */
public class Loan_Search_Details extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;

    RadioGroup search_grp;
    RadioButton rb_los,rb_reg;
    int pos;

    String losid,reg_id="";


    static String LOSID = "losid";
    static String CUSTOMER_NAME = "name";
    static String EMI = "emi";
    static String PRODUCT = "product";
    static String LEGAL = "legal";
    static String MOBILE = "mob";
    static String BOUNCE = "bounce";
    static String ASSET = "asset";
    static String BONCE_REASON = "bounce_reason";
    static String TENOR = "tenor";
    static String CHASIS = "chasis";
    static String REG_NO = "reg_no";
    static String Los_details = "Los_details";


    ImageView img_app_usage;
    Loan_Search_Adapter adapter;
    ProgressDialog mProgressDialog;
    TextView txt_error;
    SharedPreferences pref;
    String str_user_id = null;
    String str_table_name = null;
    EditText edt_loan_search;
    String str_loan_search;
    Button btn_submit;
    LinearLayout pd_details_layout;
    String str_message;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    LinearLayout lin_new_pd;
    public int pgno = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loan_search);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        userFunction.checkforLogout(pref, com.example.hdb.Loan_Search_Details.this);

        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_loan_search = (EditText) findViewById(R.id.edt_los_appform);
        btn_submit = (Button) findViewById(R.id.btn_search);

        search_grp = (RadioGroup) findViewById(R.id.search_group);

        list = (ListView) findViewById(R.id.list);
        pd_details_layout = (LinearLayout) findViewById(R.id.pd_details_layout);



        search_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                System.out.println("checked id::" + checkedId);
                pos = search_grp.indexOfChild(group.findViewById(checkedId));
if(String.valueOf(pos)!=null ||!String.valueOf(pos).isEmpty()) {
    switch (pos) {

        case 1:
            //    System.out.println("checked id::" + checkedId);
            losid = edt_loan_search.getText().toString();
            reg_id = null;

            System.out.println("losid click!!");
            System.out.println("losid :::" + losid + "reg_no" + reg_id);
            break;

        case 2:
            losid = null;
            reg_id = edt_loan_search.getText().toString();
            System.out.println("reg_id click!!");
            System.out.println("losid :::" + losid + "reg_no" + reg_id);
            break;

    }
}

            }
        });



        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arraylist = new ArrayList<HashMap<String, String>>();
                 str_loan_search = edt_loan_search.getText().toString();



                    if (cd.isConnectingToInternet()) {

                        if (!str_loan_search.isEmpty()) {

                            System.out.println("inside EDT:::::"+str_loan_search);

                            if(losid!=null && reg_id!=null || losid!="" && reg_id!=""){

                        switch (pos) {


                            case 1:
                                //    System.out.println("checked id::" + checkedId);
                                losid = edt_loan_search.getText().toString();
                                reg_id = null;

                                System.out.println("losid click!!");
                                System.out.println("losid :::" + losid + "reg_no" + reg_id);
                                break;

                            case 2:
                                losid = null;
                                reg_id = edt_loan_search.getText().toString();
                                System.out.println("reg_id click!!");
                                System.out.println("losid :::" + losid + "reg_no" + reg_id);
                                break;

                        }


                        new DownloadLoanTrails().execute();
                    }
                            else {
                                userFunction.cutomToast("Select LOSID or Reg No to Search ...",
                                        getApplicationContext());
                            }
                } else {
                    userFunction.cutomToast("Enter LOSID ...",
                            getApplicationContext());
                }

            }
                    else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }

        }
        });

    }



    public class DownloadLoanTrails extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Loan_Search_Details.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.getLoanDetails(losid, reg_id,"0");
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                            JSONObject jobj = jsonobject.getJSONObject("data");
                            jsonarray = jobj.getJSONArray("Data");
                            arraylist = new ArrayList<HashMap<String, String>>();

                            System.out.println("JSON arr len::"+jsonarray.length());

                            if(jsonarray.length()!=0) {


                                for (int i = 0; i < jsonarray.length(); i++) {
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    jsonobject = jsonarray.getJSONObject(i);

                                    map.put("Los_details", jsonobject.getString("Los_details"));


                                    // Set the JSON Objects into the array
                                    arraylist.add(map);
                                }
                            } else {

                                str_message = "No Loan trails found ...";

                            }
                        }
                    }

                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            if (jsonobject == null) {

                System.out.println("JSON OBJ NULL:::");
                userFunction.cutomToast(
                        str_message,
                        getApplicationContext());
                txt_error.setText(str_message);
                txt_error.setVisibility(View.VISIBLE);
                pd_details_layout.setVisibility(View.GONE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                    if(jsonarray.length()!=0) {

                        pd_details_layout.setVisibility(View.VISIBLE);

                        adapter = new Loan_Search_Adapter(com.example.hdb.Loan_Search_Details.this, arraylist);
                        adapter.notifyDataSetChanged();
                        list.setAdapter(adapter);
                        ((LoadMoreListView) list)
                                .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                                    public void onLoadMore() {
                                        // Do the work to load more items at the end of list
                                        // here
                                        new LoadDataTask().execute();
                                    }
                                });

                    }else {
                        System.out.println("JSON ARR NULL");
                        pd_details_layout.setVisibility(View.GONE);


                        userFunction.cutomToast(
                                str_message,
                                getApplicationContext());
                        txt_error.setText(str_message);
                        txt_error.setVisibility(View.VISIBLE);

                    }

                } else {

                    pd_details_layout.setVisibility(View.GONE);

                }

            }

            mProgressDialog.dismiss();
        }

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }

    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            jsonobject = userFunction.getLoanDetails(losid, reg_id, tmpgno);

            // Create an array
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                            JSONObject jobj = jsonobject.getJSONObject("data");
                            jsonarray = jobj.getJSONArray("Data");
                            arraylist = new ArrayList<HashMap<String, String>>();

                            System.out.println("JSON arr len::"+jsonarray.length());

                            if(jsonarray.length()!=0) {


                                for (int i = 0; i < jsonarray.length(); i++) {
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    jsonobject = jsonarray.getJSONObject(i);

                                    map.put("Los_details", jsonobject.getString("Los_details"));


                                    System.out.println(":::::::123654");
                                    // Set the JSON Objects into the array
                                    arraylist.add(map);
                                }
                            } else {

                                str_message = "No Loan trails found ...";

                            }
                        }
                    }

                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) list).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) list).onLoadMoreComplete();
        }
    }


}