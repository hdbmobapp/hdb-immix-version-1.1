package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.ExampleDBHelper;
import info.hdb.libraries.UserFunctions;

public class Dashboard extends Activity {

	TextView txt_user_id, txt_user_name, txt_branch, txt_document_progress,
			txt_upload_progress;
	TextView text_error_msg;

	String str_user_id, str_user_name, str_branch;
	SharedPreferences pref;

	SharedPreferences pData;

	ImageButton btn_logout;
	UserFunctions userFunction;
	RequestDB rdb;
	JSONObject jsonobject=null;
    Handler handler;
    private ConnectionDetector cd;
    private static final String IMAGE_DIRECTORY_NAME = "/HDB";
    ProgressDialog mProgressDialog;
    TableRow tbl_pic_list;

    ExampleDBHelper eDBHelp;

    private Handler mHandler = new Handler();
    private long mStartRX = 0;
    private long mStartTX = 0;
    Button btn_pic_list;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dshboard);
        cd = new ConnectionDetector(getApplicationContext());


		userFunction = new UserFunctions();
		rdb = new RequestDB(com.example.hdb.Dashboard.this);

        eDBHelp=new ExampleDBHelper(this);

		//.handler = new Handler();


		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		pData = getApplicationContext().getSharedPreferences("pData", 0);
		// for
		// private
		// mode
        SharedPreferences.Editor peditor = pref.edit();
        peditor.putString("is_locate_clicked", "");
        peditor.commit();

		str_user_id = pref.getString("user_id", null);
		str_user_name = pref.getString("user_name", null);
		str_branch = pref.getString("user_brnch", null);


        System.out.println("USERID::DASH::" + str_user_id);

		/*
		 * if (userFunction.isUserLoggedIn(getApplicationContext())) {
		 * startService(new Intent(this, GPSLocationService.class));
		 * startService(new Intent(this, LoanService.class)); SetLayout(); }
		 * else { Intent loginActivity = new Intent(Dashboard.this,
		 * LoginActivity.class); startActivity(loginActivity); finish(); }
		 */

		userFunction.checkforLogout(pref, com.example.hdb.Dashboard.this);

		SetLayout();

	}

	protected void SetLayout() {
		text_error_msg= (TextView) findViewById(R.id.text_error_msg);
		txt_user_id = (TextView) findViewById(R.id.txt_user_id);
		txt_user_name = (TextView) findViewById(R.id.txt_user_name);
		txt_branch = (TextView) findViewById(R.id.txt_branch);
		txt_upload_progress = (TextView) findViewById(R.id.txt_upload_progress);
		txt_document_progress = (TextView) findViewById(R.id.txt_document_progress);


        int PendingPhotoCoun=0;
		// ---set Custom Font to textview

		txt_user_id.setText(str_user_id);
		txt_user_name.setText(str_user_name);
		txt_branch.setText(str_branch);
		txt_upload_progress.setText(rdb.getPendingCount());
        PendingPhotoCoun = Integer.parseInt(eDBHelp.getPendingPhotoCount());
        btn_pic_list=(Button) findViewById(R.id.btn_pic_list);
        tbl_pic_list=(TableRow) findViewById(R.id.tbl_pic_list);

		btn_logout = (ImageButton) findViewById(R.id.img_btn_logout);
        System.out.println("pending_count:PendingPhotoCoun:::" + PendingPhotoCoun);


       Button btn_upload = (Button) findViewById(R.id.btn_upload);
        Button btn_photo_upload = (Button) findViewById(R.id.btn_photo_upload);

        if (PendingPhotoCoun > 0) {
            btn_photo_upload.setBackgroundResource(R.drawable.btn_shadow_parent);
            btn_pic_list.setVisibility(View.VISIBLE);
            txt_document_progress.setText(String.valueOf(PendingPhotoCoun));
            txt_document_progress.setVisibility(View.VISIBLE);

        } else{
            btn_pic_list.setVisibility(View.GONE);
            txt_document_progress.setVisibility(View.GONE);


        }
        btn_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    rdb.getrequestForDashboard(null, "OFF");

                }else{
                    userFunction.cutomToast("No internet connection...", com.example.hdb.Dashboard.this);
                }
            }

        });

        btn_photo_upload.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    new UploadPhoto().execute();

                }else{
                    userFunction.cutomToast("No internet connection...", com.example.hdb.Dashboard.this);
                }
            }
        });
        btn_pic_list.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(getApplicationContext(), ImagesList.class);
                    startActivity(intent);
                } else {
                    userFunction.cutomToast("No internet connection...", com.example.hdb.Dashboard.this);
                }
            }

        });

	/*	btn_dashboard = (Button) findViewById(R.id.btn_dshboard);
		btn_dashboard.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent messagingActivity = new Intent(Dashboard.this,
						Dashboard.class);
				startActivity(messagingActivity);
				finish();
			}

		});*/

	/*	btn_dataentry = (Button) findViewById(R.id.btn_data_entry);
		btn_dataentry.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				userFunction.resetData(Dashboard.this);

				Intent messagingActivity = new Intent(Dashboard.this,
						EditRequest.class);
				startActivity(messagingActivity);
				finish();
			}

		});*/

	}


	public void go_home_acivity(View v) {
		// do stuff

		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);


	}

	public void go_dataEntry_acivity(View v) {
		// do stuff

		Intent home_activity = new Intent(getApplicationContext(),
				EditRequest.class);
		startActivity(home_activity);

	}


@SuppressWarnings("unused")
private void tToast(String s) {
    Context context = getApplicationContext();
    int duration = Toast.LENGTH_LONG;
    Toast toast = Toast.makeText(context, s, duration);
    toast.show();
}


	@Override
	public void onBackPressed() {
	    // your code.
		   // to stop anonymous runnable use handler.removeCallbacksAndMessages(null);
        Intent intent = new Intent(getApplicationContext(), HdbHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

	}

    public class UploadPhoto extends AsyncTask<Void, Void, Void>
    {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Dashboard.this);
            mProgressDialog.setMessage("Uploading Please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                if (Integer.parseInt(eDBHelp.getPendingPhotoCount()) > 0) {

                 if(Integer.parseInt( rdb.getPendingCount())==0) {
                     String img_path = eDBHelp.getphoto();
                 }
                }


            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            Intent messagingActivity = new Intent(com.example.hdb.Dashboard.this,
                    com.example.hdb.Dashboard.class);
            startActivity(messagingActivity);

            mProgressDialog.dismiss();
            finish();


        }

    }




}
