package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 04-08-2018.
 */
public class Update_contact extends AppCompatActivity {

    String str_mail, str_mobile, str_userid;


    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    JSONObject jsonobject;

    Button btn_upld;
    EditText edt_email, edt_mobile;
    SharedPreferences settings;

    SharedPreferences pref;
    boolean firstRun;
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_contact_activity);

        userFunction = new UserFunctions();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        settings = getSharedPreferences("prefs", 0);
        firstRun = settings.getBoolean("firstRun", true);



        btn_upld = (Button) findViewById(R.id.btn_getotp);


        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);



        if (!pref.getString("user_email", null).equals("null")) {
            edt_email.setText(pref.getString("user_email", null));

        }

        if (!pref.getString("mobile", null).equals("null")) {
            edt_mobile.setText(pref.getString("mobile", null));
        }


        str_userid = pref.getString("user_id", null);

        btn_upld = (Button) findViewById(R.id.btn_getotp);

        btn_upld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                str_mail = edt_email.getText().toString();
                str_mobile = edt_mobile.getText().toString();

                Boolean ins = true;
                try {
                    if ((str_mobile.equals("")) && ins == true) {
                        edt_mobile.setError("Enter Resi mobile");
                        edt_mobile.requestFocus();

                        ins = false;


                    } else {
                        if (ins == true && (new BigDecimal(str_mobile.trim()).compareTo(new BigDecimal("0")) == 0) ||
                                str_mobile.trim().length() < 10) {
                            edt_mobile.setError("Enter valid Resi mobile");
                            edt_mobile.requestFocus();

                            ins = false;

                        } else {
                            edt_mobile.setError(null);

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("EMAIL::::" + str_mail);

                try {
                    if (!str_mail.equals("") && ins == true) {
                        if (!Check_pattern(str_mail) && ins == true) {

                            edt_email.setError("Enter valid  email ID");
                            edt_email.requestFocus();


                            ins = false;

                        } else {
                            edt_email.setError(null);
                        }
                    } else {
                        if (str_mail.equals("") && ins == true) {
                            edt_email.setError("Enter  email ID");
                            edt_email.requestFocus();
                            ins = false;

                        } else {
                            edt_email.setError(null);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (ins == true) {
                    new Upload_details().execute();
                }

            }
        });

    }


    public class Upload_details extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Update_contact.this);
            mProgressDialog.setMessage("Updating Details...Plz wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {


            try {
                jsonobject = userFunction.Update_details(str_mail, str_mobile, str_userid);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String args) {

            String city_id, city_name, state_id, state_name;
            //System.out.println("JSONARR::::::" + jsonarray);
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getApplicationContext().getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");

                } else {


                    if (!jsonobject.getString("success").equals("false")) {

                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("user_email", str_mail);
                        editor.putString("mobile", str_mobile);
                        editor.commit();

                        userFunction.cutomToast("Contact Details Updated Successfully", getApplicationContext());

                        Intent messagingActivity = new Intent(getApplicationContext(),
                                HdbHome.class);
                        startActivity(messagingActivity);
                        finish();
                        SharedPreferences.Editor editr=settings.edit();
                        editr.putBoolean("firstRun",false);
                        editr.commit();
                    } else {

                        userFunction.cutomToast(jsonobject.getString("Error_msg"), getApplicationContext());

                    }

                }
                mProgressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private boolean Check_pattern(String string) {

        Boolean status = false;

        status = EMAIL_PATTERN.matcher(string).matches();

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }

    public void go_home(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

    @Override
    public void onBackPressed() {

    }
}

