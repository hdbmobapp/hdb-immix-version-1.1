package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hm_collx.expndablelist.ExpandableACMactivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

public class ACMTypeListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;
	UserFunctions user_function;
	JSONObject acmjsonobject;

	public ACMTypeListAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.group_item, parent, false);
		// Get the position
		resultp = data.get(position);

		TextView tv_name = (TextView) itemView.findViewById(R.id.group_name);
		TextView tv_total_users = (TextView) itemView
				.findViewById(R.id.txt_total_users_cnt);
		TextView tv_active_users = (TextView) itemView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) itemView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) itemView
				.findViewById(R.id.txt_total_pay);
		TextView tv_locate_me = (TextView) itemView
				.findViewById(R.id.txt_locate);

		TextView tv_exp = (TextView) itemView.findViewById(R.id.indicator_icon);
		tv_exp.setVisibility(View.INVISIBLE);
		tv_name.setTypeface(null, Typeface.NORMAL);

		if (resultp.get(ACMActivity.ACM_FLAG).compareTo("C") == 0) {
			tv_locate_me.setVisibility(View.VISIBLE);
		}
		System.out.println("ALLLL:::" + resultp.get(ACMTypes.ACM_TOTAL_USERS));
		tv_name.setText(resultp.get(ACMTypes.ACM_NAME));
		tv_total_users.setText("Users  " + System.getProperty("line.separator")
				+ resultp.get(ACMTypes.ACM_TOTAL_USERS));
		tv_active_users.setText("A Users  "
				+ System.getProperty("line.separator")
				+ resultp.get(ACMTypes.ACM_ACTIVE_USERS));
		tv_total_punches.setText("Punches "
				+ System.getProperty("line.separator")
				+ resultp.get(ACMTypes.ACM_TOTAL_PUNCHES));
		tv_total_pay.setText("Pay " + System.getProperty("line.separator")
				+ resultp.get(ACMActivity.ACM_TOTAL_PAY));

		userFunction = new UserFunctions();

		itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);

				new Thread(new Runnable() {
					public void run() {
						// Get the position
						resultp = data.get(position);

						Intent intent = new Intent(context,
								ExpandableACMactivity.class);
						intent.putExtra("str_region", ACMTypes.str_region);
						intent.putExtra("from", "expand");
						intent.putExtra("total_users",
								resultp.get(ACMTypes.ACM_TOTAL_USERS));
						intent.putExtra("active_users",
								resultp.get(ACMTypes.ACM_ACTIVE_USERS));
						intent.putExtra("total_punches",
								resultp.get(ACMTypes.ACM_TOTAL_PUNCHES));
						intent.putExtra("total_pay",
								resultp.get(ACMTypes.ACM_TOTAL_PAY));
						intent.putExtra("name",
								resultp.get(ACMTypes.ACM_REGION));
						intent.putExtra("type", resultp.get(ACMTypes.ACM_NAME));

						context.startActivity(intent);
					}
				}).start();
			}

		});

		return itemView;
	}

	public void show() {
		Toast.makeText(context, "cant find ....", Toast.LENGTH_LONG).show();

	}
}
