package com.example.hdb;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hmapp.AppMenu.SplashScreen;
import com.example.hmapp.HdbHome;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.DatabaseHandler;
import info.hdb.libraries.UserFunctions;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final String TAG = "LoginActivity";
    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;

    String flag_dc, flag_pm, id_dc, id_pm, flag_pd, id_pd;
    Integer off_rowcount;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunctions userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    // private static Typeface font;
    //String user_name = null;
    String gcm = null;
    @SuppressWarnings("unused")
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{2,14}");
    Context context;
    ProgressBar pb_progress;

    String res, resp_success;

    public static String KEY_STATUS = "status";

    public static String KEY_SUCCESS = "success";

    public static String KEY_STATUS_VM = "status";

    public static String KEY_SUCCESS_VM = "success";
    public static String KEY_STATUS_GM = "status";

    public static String KEY_SUCCESS_GM = "success";

    JSONObject jsonobject;
    JSONArray jsonarray, jsonarray1, jsonarray2, jsonarray3;
    ProgressDialog mProgressDialog;

    ArrayList<String> al_dispo_code, al_dispo_value, al_dispo_flag, al_pm, al_pm_val, al_pm_flag, al_dispo_id, al_pm_id,
            al_product, al_prod_val, al_prod_flag, al_prod_id, al_remark;

    String str_dispo_id, str_dispo, str_flag;
    String imeiNumber1, imeiNumber2,str_api_level;
    Integer api_level;


    ArrayList<String> check_master;

    ArrayList<String> master_flag;

    DatabaseHandler db;

    String reg_id;

    SharedPreferences pref;
    String lattitude = null, longitude = null;
    //EditText edt_new_version_link;
    String new_version;
    TextView txt_new_version;
    String new_version_name;
    RequestDB rDB;
    HashMap<String, String> user = new HashMap<String, String>();


    private String android_id;
    String check_master_flag;
    GPSTracker_New tracker;
    double dbl_latitude = 0;
    double dbl_longitude = 0;
    static Double lat;
    static Double lon;

    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;


    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,

    };

    Integer is_login,off_is_login;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.login);
      /*  font = Typeface.createFromAsset(getBaseContext().getAssets(),
                "fonts/Amaranth-Regular.otf");*/
        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();

        rDB = new RequestDB(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        // for

        db = new DatabaseHandler(getApplicationContext());


        //dispo code
        al_dispo_code = new ArrayList<String>();
        al_dispo_value = new ArrayList<String>();
        al_dispo_flag = new ArrayList<String>();
        al_dispo_id = new ArrayList<String>();

        check_master = new ArrayList<String>();
        master_flag = new ArrayList<String>();


        //pay mode
        al_pm = new ArrayList<String>();
        al_pm_val = new ArrayList<String>();
        al_pm_id = new ArrayList<String>();
        al_pm_flag = new ArrayList<String>();


        //product
        al_product = new ArrayList<String>();
        al_prod_val = new ArrayList<String>();
        al_prod_flag = new ArrayList<String>();
        al_prod_id = new ArrayList<String>();

        //Remark_Reason
        al_remark = new ArrayList<String>();


        SetLayout();
        checkLocation();
        checkPermissions();

        check_master = rDB.get_masters();
        System.out.println("MASTERS ARRAY:::1 " + check_master.get(0) + "MASTERS ARRAY:::1 " + check_master.get(1));

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) com.example.hdb.LoginActivity.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

    }

    protected void SetLayout() {


        is_login = pref.getInt("is_login", 0);

        off_is_login = pref.getInt("off_is_login", 0);

        if (userFunction.isUserLoggedIn(LoginActivity.this) && is_login > 0  ) {

            Intent iHomeScreen = new Intent(LoginActivity.this,
                    Home_PD_COLLX.class);
            startActivity(iHomeScreen);
            finish();
        }

            // Check if Internet present
        setContentView(R.layout.sl_login);
        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
       // edt_new_version_link = (EditText) findViewById(R.id.edt_new_version_link);
        txt_new_version = (TextView) findViewById(R.id.txt_new_version);
        // ---set Custom Font to textview
        //inputEmail.setTypeface(font);

        // inputPassword.setTypeface(font);

        // btnLogin.setTypeface(font);
        // loginErrorMsg.setTypeface(font);

        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);

        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
               // System.out.println("jnnfgnhnnhnfhbonbhfgoh");

                api_level = Build.VERSION.SDK_INT;
                str_api_level= Integer.toString(api_level);
                TelephonyManager tm = (TelephonyManager) getSystemService(context.TELEPHONY_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    imeiNumber1 = tm.getImei(1);
                    imeiNumber2 = tm.getImei(2);

                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    imeiNumber1 = tm.getDeviceId(1);
                    imeiNumber2 = tm.getDeviceId(2);
                    System.out.println("imeiNumber1" + imeiNumber1);
                }

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                    android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                }

                    checkLocation();
                if (isLocationEnabled() == true) {

                    GCMRegistrar.register(com.example.hdb.LoginActivity.this,
                            GCMIntentService.SENDER_ID);
                    reg_id = GCMRegistrar
                            .getRegistrationId(com.example.hdb.LoginActivity.this);

                    System.out.println("reg_id:::::" + reg_id);

                    email = inputEmail.getText().toString();
                    password = inputPassword.getText().toString();
                    // Check if Internet present
                    TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                    TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);
                    // error_pwd.setTypeface(font);
                    //  error_email.setTypeface(font);

                    if (password.equals("") || email.equals("")) {

                        if (password.equals("")) {
                            error_pwd.setVisibility(View.VISIBLE);
                            error_pwd.setText("Please enter a password");

                        } else {
                            error_pwd.setVisibility(View.GONE);

                        }
                        if (email.equals("")) {
                            error_email.setVisibility(View.VISIBLE);
                            error_email.setText("Please enter a Employee Id");
                        } else {
                            error_email.setVisibility(View.GONE);

                        }
                    } else {

                        if (cd.isConnectingToInternet()) {

                            error_pwd.setVisibility(View.GONE);
                            error_email.setVisibility(View.GONE);
                      /*  Location nwLocation = appLocationService
                                .getLocation(LocationManager.NETWORK_PROVIDER);
                        System.out.println("nwLocation:::" + nwLocation);

                        if (nwLocation == null) {
                            userFunction.cutomToast("Please start Location Service..", LoginActivity.this);
                        } else {
                            double ltt = nwLocation.getLatitude();
                            double lott = nwLocation.getLongitude();


                            longitude = String.valueOf(lott);
                        }*/

                            System.out.println("lattitude:  lat::" + lat);

                            if (lat != null) {
                                lattitude = Double.toString(lat);
                                System.out.println("lattitude:::" + lattitude);
                                longitude = Double.toString(lon);
                            }
                            System.out.println("longitude:::" + longitude);
                            new Login().execute();

                        } else {

                            error_pwd.setVisibility(View.GONE);
                            error_email.setVisibility(View.GONE);

                            //   dbl_latitude = tracker.getLatitude();
                            //   dbl_longitude = tracker.getLongitude();
                            //  lattitude=String.valueOf(dbl_latitude);
                            //  longitude=String.valueOf(dbl_longitude);


                            // userFunction.logoutUserclear(getApplicationContext());

                            user = db.off_getUserDetails();

                            System.out.println("HASHMAP::::" + user);

                            String val = (String) user.get("uid");
                            String dev_val = (String) user.get("dev_id");

                            //   String userid=pref.getString("user_id", null);

                            System.out.println("USERID::::" + email);

                            System.out.println("DEVICE ID::::::::" + dev_val);

                            if (val != null) {

                                if (val.equals(email)) {

                                    SimpleDateFormat sdf = new SimpleDateFormat(
                                            "yyyy-MM-dd");
                                    String current_date = sdf.format(new Date());

                                    Editor editor = pref.edit();

                                    editor.putInt("off_is_login", 1);

                                    editor.putString("current_date_off", current_date);
                                    editor.commit();
                                    System.out.println("OFF LOGIN:::");

                                    off_rowcount = db.off_getRowCount();
                                    if (off_rowcount > 0) {
                                        if (!dev_val.equals("")) {

                                            System.out.println("DEVICE ID VAL:::" + dev_val);

                                            if (!dev_val.equals(android_id)) {
                                                userFunction.cutomToast(
                                                        "You have changed your handset. Contact your supervisor",
                                                        getApplicationContext()

                                                );

                                                loginErrorMsg.setText("You have changed your handset. Contact your supervisor");


                                            } else {
                                                Intent messagingActivity = new Intent(
                                                        com.example.hdb.LoginActivity.this, Home_PD_COLLX.class);
                                                startActivity(messagingActivity);
                                                finish();
                                            }
                                        } else {
                                            Intent messagingActivity = new Intent(
                                                    com.example.hdb.LoginActivity.this, Home_PD_COLLX.class);
                                            startActivity(messagingActivity);
                                            finish();
                                        }
                                    } else {
                                        userFunction.noInternetConnection(
                                                "Check your connection settings!",
                                                getApplicationContext());

                                        loginErrorMsg.setText("Check your connection settings!");
                                    }
                                } else {
                                    userFunction.cutomToast(
                                            "Username or Password is incorrect",
                                            getApplicationContext());

                                    loginErrorMsg.setText("Username or Password is incorrect");
                                }
                            } else {
                                userFunction.noInternetConnection(
                                        "Check your connection settings!",
                                        getApplicationContext());

                                loginErrorMsg.setText("Check your connection settings!");
                            }

                       /* userFunction.noInternetConnection(
                                "Check your connection settings!",
                                getApplicationContext());*/
                        }
                    }

                }
            }
        });

    }


    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat = location.getLatitude();

        lon = location.getLongitude();


        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());

                }

            }
        });
    }

    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            // String str_imei = getIMEI(LoginActivity.this);

            try {
                String deviceName = Build.MODEL;
                String deviceMan = Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, imeiNumber1, imeiNumber1,
                        lattitude, longitude, deviceName, deviceMan, android_id,str_api_level);
                System.out.println("json" + json);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);

                        new_version = json.getString("new_version_link").toString();
                        new_version_name = json.getString("new_version_name").toString();

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            // user successfully logged in'
                            // Store user details in SQLite Database10
                            DatabaseHandler db = new DatabaseHandler(
                                    getApplicationContext());
                            JSONObject json_user = json.getJSONObject("data");
                            //   JSONObject json_user1 = json.getJSONObject("imei");

                            // Clear all previous data in database
                            userFunction
                                    .logoutUserclear(getApplicationContext());

                            userFunction
                                    .off_logoutUserclear(getApplicationContext());


                            String KEY_EMAIL = "email";
                            String KEY_NAME = "name";
                            String KEY_UID = "id";
                            String KEY_deviceid = "imei";

                            db.addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID));

                            db.off_addUser(json_user.getString(KEY_NAME),
                                    json_user.getString(KEY_EMAIL),
                                    json_user.getString(KEY_UID),
                                    json.getString(KEY_deviceid)
                            );

                            SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd");
                            String current_date = sdf.format(new Date());

                            Editor editor = pref.edit();
                            editor.putString("current_date", current_date);

                            editor.putInt("is_login", 1);
                            editor.putString("user_id",
                                    json_user.getString(KEY_UID).toString());
                            editor.putString("user_name",
                                    json_user.getString(KEY_NAME).toString());
                            String KEY_BRANCH = "branch";
                            editor.putString("user_brnch",
                                    json_user.getString(KEY_BRANCH).toString());
                            editor.putString("mobile",
                                    json_user.getString("mobile_no").toString());


                            editor.putString("user_email",
                                    json_user.getString(KEY_EMAIL).toString());
                            editor.putString("user_designation",
                                    json_user.getString("designation").toString());
                            editor.putString("user_designation_value",
                                    json_user.getString("user_designation_value").toString());

                            editor.putString("avi_tel",
                                    json_user.getString("Avinash").toString());
                            editor.putString("sach_tel",
                                    json_user.getString("Sachin").toString());
                            editor.putString("vija_tel",
                                    json_user.getString("Vijay").toString());
                            editor.putString("service_tel",
                                    json_user.getString("Service").toString());
                            editor.putString("paynimo",
                                    json_user.getString("paynimo").toString());

                            editor.putString("collx_app",
                                    json_user.getString("collx_app").toString());
                            editor.putString("pd_com_app",
                                    json_user.getString("pd_com_app").toString());
                            editor.putString("pd_lend_app",
                                    json_user.getString("pd_lend_app").toString());

                            editor.putString("dms_app",
                                    json_user.getString("dms_app").toString());

                            editor.putString("los_qde_app",
                                    json_user.getString("los_qde_app").toString());
                            editor.putString("dms_imageview_app",
                                    json_user.getString("dms_imageview_app").toString());


                            System.out.println("Service::::::" + json_user.getString("Service").toString());
                            if (rememberMe.isChecked()) {
                                editor.putString("user_username", email);
                                editor.putString("user_password", password);
                            } else {
                                editor.putString("user_username", "");
                                editor.putString("user_password", "");
                            }
                            String loan_count = json_user.getString("loan_count");
                            int int_loan_count = Integer.parseInt(loan_count);
                            if (int_loan_count > 0) {
                                userFunction.send_Alloc_notification("You have " + loan_count + " not attempt cases click here to view ", com.example.hdb.LoginActivity.this);

                            }


                            editor.commit();

                            if (check_master.get(0).equals("0") || check_master.get(1).equals("0") || check_master.get(2).equals("0")) {
                                //  new Download_Masters().execute();

                                System.out.println("MASTERS LOAD");

                                new Download_Masters().execute();
                            } else {
                                master_flag = rDB.get_master_flag();

                                System.out.println(" FLAG DC:::" + master_flag.get(0) + " FLAG PM::: " + master_flag.get(2));

                                flag_dc = master_flag.get(0);
                                id_dc = master_flag.get(1);
                                flag_pm = master_flag.get(2);
                                id_pm = master_flag.get(3);

                                flag_pd = master_flag.get(4);
                                id_pd = master_flag.get(5);

                                new Verify_Masters().execute();

                            }

                        } else {
                            String KEY_DEVID = "imei";

                            if (!json.getString(KEY_DEVID).toString().equals("")) {
                                db.update_deviceid(json.getString(KEY_DEVID));
                            }

                            if (new_version != null && new_version != "") {
                              //  edt_new_version_link.setText(new_version);
                              //  edt_new_version_link.setVisibility(View.VISIBLE);
                                txt_new_version.setText(new_version_name);
                                txt_new_version.setVisibility(View.VISIBLE);
                            }
                            loginErrorMsg.setText(json.getString("message")
                                    .toString());
                        }
                    }
                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pb_progress.setVisibility(View.GONE);

        }

    }

    public class Download_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.LoginActivity.this);
            mProgressDialog.setMessage("Please wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.get_masters(null);
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_GM) != null) {
                        res = jsonobject.getString(KEY_STATUS_GM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_GM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                            JSONObject jobj = jsonobject.getJSONObject("data");

                            JSONObject jobj1 = jsonobject.getJSONObject("data1");

                            JSONObject jobj2 = jsonobject.getJSONObject("data2");


                            jsonarray = jobj.getJSONArray("dispo_code");

                            jsonarray1 = jobj1.getJSONArray("pay_mode");

                            jsonarray2 = jobj2.getJSONArray("product");


                            if (jsonarray != null && jsonarray1 != null && jsonarray2 != null) {


                                for (int i = 0; i < jsonarray.length(); i++) {

                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_dispo_code.add(jsonobject.getString("dispo_code"));
                                    al_dispo_value.add(jsonobject.getString("dispo_code_val"));
                                    al_dispo_flag.add(jsonobject.getString("dispo_flag"));
                                    al_dispo_id.add(jsonobject.getString("dispo_id"));

                                }

                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_pm.add(jsonobject.getString("pay_mode"));
                                    al_pm_val.add(jsonobject.getString("pay_mode_val"));
                                    al_pm_flag.add(jsonobject.getString("pay_mode_flag"));
                                    al_pm_id.add(jsonobject.getString("pay_mode_id"));

                                }


                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }

                            }

                        } else {

                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        if (jsonarray != null && jsonarray1 != null && jsonarray2 != null) {

                            rDB.delete_dispo_table();
                            rDB.delete_pm_table();
                            rDB.delete_pd_table();

                            for (int i = 0; i < al_dispo_code.size(); i++) {

                                rDB.insert_dispo_codes(al_dispo_code.get(i), al_dispo_value.get(i), al_dispo_flag.get(i), al_dispo_id.get(i));

                            }

                            for (int i = 0; i < al_pm.size(); i++) {

                                System.out.println("AL PM:::" + al_pm.get(i));
                                rDB.insert_pm(al_pm.get(i), al_pm_val.get(i), al_pm_flag.get(i), al_pm_id.get(i));

                            }

                            for (int i = 0; i < al_product.size(); i++) {

                                System.out.println("AL PD:::" + al_product.get(i));
                                System.out.println("AL PD:::" + al_prod_val.get(i));
                                System.out.println("AL PD:::" + al_prod_flag.get(i));
                                System.out.println("AL PD:::" + al_prod_id.get(i));

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));
                            }

                            mProgressDialog.dismiss();

                            Intent messagingActivity = new Intent(
                                    com.example.hdb.LoginActivity.this, Home_PD_COLLX.class);
                            startActivity(messagingActivity);
                            finish();

                        }

                    } else {
                        mProgressDialog.dismiss();
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        loginErrorMsg.setText(jsonobject.getString("message").toString());
                        //pd_details_layout.setVisibility(View.GONE);


                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class Verify_Masters extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.LoginActivity.this);
            mProgressDialog.setMessage("Please wait");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                jsonobject = userFunction.check_masters(flag_dc, flag_pm, id_dc, id_pm, flag_pd, id_pd);

                System.out.println("JSONOBJ::::" + jsonobject);

                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS_VM) != null) {
                        res = jsonobject.getString(KEY_STATUS_VM);
                        resp_success = jsonobject.getString(KEY_SUCCESS_VM);


                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            if (!jsonobject.isNull("data")) {
                                JSONObject jobj2 = jsonobject.getJSONObject("data");
                                jsonarray = jobj2.getJSONArray("arr_dispo_code");
                            }

                            if (!jsonobject.isNull("data1")) {
                                JSONObject jobj3 = jsonobject.getJSONObject("data1");
                                jsonarray1 = jobj3.getJSONArray("arr_pay_mode");
                            }

                            if (!jsonobject.isNull("data2")) {
                                JSONObject jobj4 = jsonobject.getJSONObject("data2");
                                jsonarray2 = jobj4.getJSONArray("arr_product");
                            }


                            if (jsonarray != null) {

                                System.out.println("DISPO Arr::");
                                for (int i = 0; i < jsonarray.length(); i++) {

                                    jsonobject = jsonarray.getJSONObject(i);
                                    al_dispo_code.add(jsonobject.getString("dispo_code"));
                                    al_dispo_value.add(jsonobject.getString("dispo_code_val"));
                                    al_dispo_flag.add(jsonobject.getString("dispo_flag"));
                                    al_dispo_id.add(jsonobject.getString("dispo_id"));

                                }
                            }

                            if (jsonarray1 != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray1.length(); i++) {

                                    jsonobject = jsonarray1.getJSONObject(i);
                                    al_pm.add(jsonobject.getString("pay_mode"));
                                    al_pm_val.add(jsonobject.getString("pay_mode_val"));
                                    al_pm_flag.add(jsonobject.getString("pay_mode_flag"));
                                    al_pm_id.add(jsonobject.getString("pay_mode_id"));

                                }
                            }

                            if (jsonarray2 != null) {
                                System.out.println("PM Arr::");
                                for (int i = 0; i < jsonarray2.length(); i++) {

                                    jsonobject = jsonarray2.getJSONObject(i);
                                    al_product.add(jsonobject.getString("product"));
                                    al_prod_val.add(jsonobject.getString("product_val"));
                                    al_prod_flag.add(jsonobject.getString("flag"));
                                    al_prod_id.add(jsonobject.getString("product_id1"));

                                }
                            }

                        } else {
                            mProgressDialog.dismiss();
                            Editor edt = pref.edit();
                            edt.putInt("is_login", 0);

                            edt.commit();
                        }
                    }


                } else {
                    // str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject == null) {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                    System.out.println("JSON OBJ NULL:::");
                    mProgressDialog.dismiss();

                } else {
                    //   txt_error.setVisibility(View.GONE);

                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {


                        System.out.println("JSON ARR:::" + jsonarray + "J ARR 1" + jsonarray1);


                        if (jsonarray != null) {


                            System.out.println("INSIDE DISPO CODE:::");
                            rDB.delete_dispo_table();

                            for (int i = 0; i < al_dispo_code.size(); i++) {


                                rDB.insert_dispo_codes(al_dispo_code.get(i), al_dispo_value.get(i), al_dispo_flag.get(i), al_dispo_id.get(i));

                            }
                        }

                        if (jsonarray1 != null) {

                            rDB.delete_pm_table();

                            for (int i = 0; i < al_pm.size(); i++) {

                                rDB.insert_pm(al_pm.get(i), al_pm_val.get(i), al_pm_flag.get(i), al_pm_id.get(i));

                            }
                        }

                        if (jsonarray2 != null) {

                            rDB.delete_pd_table();

                            for (int i = 0; i < al_product.size(); i++) {

                                rDB.insert_prod(al_product.get(i), al_prod_val.get(i), al_prod_flag.get(i), al_prod_id.get(i));

                            }
                        }

                        mProgressDialog.dismiss();

                        Intent messagingActivity = new Intent(
                                com.example.hdb.LoginActivity.this, Home_PD_COLLX.class);
                        startActivity(messagingActivity);
                        finish();


                    } else {

                        mProgressDialog.dismiss();
                        Editor editor = pref.edit();
                        editor.putInt("is_login", 0);
                        editor.commit();
                        loginErrorMsg.setText(jsonobject.getString("message").toString());
                        //pd_details_layout.setVisibility(View.GONE);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    public String getIMEI(Context context) {

        TelephonyManager mngr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }

    /*
     * private boolean CheckEmail(String email) {
     *
     * return EMAIL_PATTERN.matcher(email).matches(); }
     */
 /*   private boolean CheckUsername(String username) {

        return USERNAME_PATTERN.matcher(username).matches();
    }

    private boolean CheckPassword(String password) {

        return PASSWORD_PATTERN.matcher(password).matches();
    }*/

    public void getRegId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        GCMRegistrar.register(com.example.hdb.LoginActivity.this,
                                GCMIntentService.SENDER_ID);
                        reg_id = GCMRegistrar
                                .getRegistrationId(com.example.hdb.LoginActivity.this);

                        System.out.println("REG ID:::" + reg_id);
                    }
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                // reg_id;
            }
        }.execute(null, null, null);
    }

    /*
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
*/

    public void contact_us(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                Contact_us.class);
        startActivity(home_activity);

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }


}
