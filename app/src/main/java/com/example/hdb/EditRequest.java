package com.example.hdb;

import android.Manifest;
import android.R;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.ExampleDBHelper;
import info.hdb.libraries.MainActivity_OLD;
import info.hdb.libraries.MyAdapter;
import info.hdb.libraries.UserFunctions;


public class EditRequest extends AppCompatActivity implements OnItemSelectedListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
        //,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String TAG = "EditRequest";

    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "/HDB";


    ArrayAdapter<String> adapter_state16;

    int map_captured = 0;

    int curr_DOC_no = 0; // Current Processing Document Number.
    String ts;
    Uri imageUri = null;
    String str_disance = "0";
    File file;
    String current_date = null;
    String losid;

    /************
     * IMAGE END
     *************/

    // final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    UserFunctions userFunction;
    com.example.hdb.RequestDB rdb;
    // Uri imageUri = null;
    com.example.hdb.EditRequest CameraActivity = null;
    Button btn_dashboard, btn_dataentry, btn_search;
    JSONObject jsonobject;
    final private int CAPTURE_IMAGE = 2;
    private String imgPath;
    public static String KEY_SUCCESS = "success";
    ProgressDialog mProgressDialog;
    private int year, month, day;
    public static String KEY_STATUS = "status";
    boolean datafound = false;
    String Upload_Date;
    String[] Dataentrytype = {"Collection"};
    String[] Dataentrytype_val = {"CO"};

    String pic_count = "0";

    TableRow tb0;

    String str_date;

    String entry_status, str_txt_distacne;


    String[] Visit_Mode = {"Select Visit Mode *", "Phone", "Visit Office", "Visit Residence", "Visit Others", "Visit Third Party"};

    String[] Visit_Mode_val = {"", "PH", "VSOF", "VSRS", "VSOTH", "VSTHP"};

    String[] Bucket_type = {"Select", "Live", "Repo Recovery"};

    String[] Bucket_type_val = {"", "LI", "LO"};

    String[] Met_Spoke_type = {"Select Met/Spoke *", "Customer", "Staff", "Family Member",
            "Coapplicant", "Guarantor", "Others", "No One"};

    String[] Met_Spoke_type_val = {"", "CU", "ST", "FM", "CO", "GU",
            "OT", "NO"};

    ArrayList<String> DispoCode_type;

    ArrayList<String> DispoCode_type_val;

    ArrayList<String> Mode_type;

    ArrayList<String> Mode_type_val;

    ArrayList<String> proudct_type;
    ArrayList<String> producttype_val;

    String[] Form60_type = {"Select Form60 Type", "Yes", "No"};
    String[] Sighted_type = {"Select Sighted * ", "Yes", "No"};
    String[] running_con_array= {"Select Running Condition * ", "Yes", "No"};
    String[] vehicle_repo_double = {"Select Vehicle Repo Double *", "Yes", "No"};
    String[] with_third_party_array = {"Select With 3rd Party", "Yes", "No"};
    String[] Cou_hand_loan_array = {"Select Customer Has Hand Loans  *", "Yes", "No"};

    String[] Form60_type_val = {"", "Y", "N"};

    String[] BIZ_type_val = {"Select BIZ *", "GOOD BIZ", "SAME BIZ LOW SCALE", "NO BIZ BUT FIX INC", "ALTERNATE/NEW BIZ", "NO BIZ AND NO INCOME"};
    String[] PROPERTY_type_val = {"Select Property *", "GOOD QLTY GOOD LQDTY", "POOR QLTY GOOD LQDTY", "GOOD QLTY POOR LQDTY", "POOR QLTY POOR LQDTY"};
    String[] Current_BIZ_type_val = {"Select Current Biz From Same Place *", "Yes", "New - good Premise", "New - inferior Premise", "Residence", "No BIZ"};
    String[] Not_able_pay_type_val = {"Select Not Able To Pay", "No Delay", "Current Mnth", "2 Mnths", "3 Mnths", "Cannot Pay"};
    String[] Coll_Occupant_type_value = {"Select Collateral Occupant", "Self", "Tenant/Relative", "Vacant", "Buyer", "Illegal Possession"};
    String[] distance_type = {"Select Distance", "Exact location", "Within 500 mtr", "Within 1Km", "More than 1 KM", "Map not loading"};


    String str_BIZ_type_val, str_PROPERTY_type_val, str_Current_BIZ_type_val, str_Not_able_pay_type_val, str_Coll_Occupant_type_value, str_cust_has_loan;


    Spinner spn_BIZ_type_val, spn_PROPERTY_type_val, spn_Current_BIZ_type_val, spn_Not_able_pay_type_val, spn_Coll_Occupant_type_value, spn_cust_has_loan;

    Spinner spn_Product_Type;
  //  Spinner spn_  Data_Entry_Type;
    Spinner spn_visit_mode;
    Spinner spn_bucket_type;
    Spinner spn_met_spoke;
    Spinner spn_dispoCode;
    Spinner spn_Mode;
    Spinner spn_formsixty;
    Spinner spn_sighted;
    Spinner spn_running_condition;
    Spinner spn_third_Party;
    Spinner spn_repo_doable;
    Spinner spn_distance_from_loc;

    ArrayAdapter adapter_distance_type;

    String str_Product_Type;
    String str_Data_Entry_Type;
    String str_visit_mode;
    String str_bucket_type;
    String str_met_spoke;
    String str_dispoCode;
    String str_Mode;
    String str_sighted;
    String str_running_condition;
    String str_third_Party;
    String str_repo_doable;
    String str_EMI_Amount_data;
    Integer int_dist_count = 0;
    String is_locate_clicked = "";

    String str_data_entry = "ON";

    String str_label = "";

    String vis_mode = null;

    String str_distance_type;

    private ConnectionDetector cd;
    String lattitude = null, longitude = null;
    String map_address = null;
    EditText edt_losid_no, edt_customer_name, edt_Account_Status,
            edt_Number_Contacted, edt_Next_Follow_Date,
            edt_Remarks_if_any, edt_New_Contact_Number,
            edt_New_Contact_Address, edt_New_Contact_Email, edt_Receipt_Number,
            edt_Total_Amount_Rs, edt_EMI_Amount_Rs, edt_Other_Amount_Rs,
            edt_Cheque_Num, edt_PAN_no, edt_third_Party_Name,
            edt_third_Party_Address, edt_emi_amount_ser, edt_max_emi_amt_capble, edt_hand_loane_value, edt_loan_mnth_emi, edt_ssn
         ;
    ExampleDBHelper dbHelper;
    TextView  text_error_msg, location_lat, location_long, simpleTextView,text_error_comntacted;

    String str_losid_no, str_customer_name, str_Account_Status, str_PDD_Status,
            str_Number_Contacted, str_Next_Follow_Date, str_Remarks_if_any,
            str_New_Contact_Number, str_New_Contact_Address,
            str_New_Contact_Email, str_Receipt_Number, str_Total_Amount_Rs,
            str_EMI_Amount_Rs, str_Other_Amount_Rs, str_Cheque_Num, str_PAN_no = null,str_formsixty,
            str_third_Party_Name, str_third_Party_Address,
            str_branch_name = null, str_bom_bkt = null, str_risk_bkt = null, str_cbc_due = null, str_lpp_due = null, str_product = null, str_EMI_Amount_Rs_ser, str_max_emi_amt_capble, str_hand_loane_value, str_loan_mnth_emi,
            str_ssn, str_ACH_status, str_contact_1, str_contact_2,str_virtual_acc_no,str_text;

    LinearLayout lay_collateral_detalis, lay_collateral_detalis1;
    String alloc_losid;
    EditText txt_distacne;

    EditText edt_PDD_Status, edt_ACH_status,edt_virtual_acc,txt_branch_name, txt_bom_bkt, txt_risk_bkt, txt_cbc_due, txt_lpp_due;
    SharedPreferences pref;
    SharedPreferences pData;
    String str_user_id;
    private Calendar calendar;
    String str_designation;
    String str_designation_val;
    LinearLayout lin_quality_fbck_amt;
    Date dTS;
    TableRow tbl_ssn, tbl_next_date;
    Button btn_locate_me;

    private static final Pattern PANNO_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,10}");


    private static final Pattern REMARKS_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,25}");

    char zero = '0';
    String contacts_flag = "0";
    com.example.hdb.GPSTracker_New tracker;
    double dbl_latitude = 0;
    double dbl_longitude = 0;

    static  Double lat;
    static  Double lon;

    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;



    @Override
    public void onPause() {
        super.onPause();

        if ((mProgressDialog != null) && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        mProgressDialog = null;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.hdb.R.layout.editrequest);
        String deviceMan = android.os.Build.MANUFACTURER;



        //  tracker = new GPSTracker_New(EditRequest.this);
        DispoCode_type = new ArrayList<>();
        DispoCode_type_val = new ArrayList<>();

        Mode_type = new ArrayList<>();
        Mode_type_val = new ArrayList<>();

        proudct_type = new ArrayList<>();
        producttype_val = new ArrayList<>();

        tbl_next_date = (TableRow) findViewById(com.example.hdb.R.id.tableRow10);

        txt_distacne = (EditText) findViewById(com.example.hdb.R.id.edt_distance);

        tbl_next_date.setVisibility(View.GONE);


        userFunction = new UserFunctions();
        rdb = new com.example.hdb.RequestDB(com.example.hdb.EditRequest.this);
        cd = new ConnectionDetector(com.example.hdb.EditRequest.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = getApplicationContext().getSharedPreferences("pData", 0);
        str_designation = pref.getString("user_designation", "NO");
        str_designation_val = pref.getString("user_designation_value", "0");
        dbHelper = new ExampleDBHelper(this);

        DispoCode_type = rdb.get_masters_dispo();

        DispoCode_type_val = rdb.get_masters_dispo_val();

        Mode_type = rdb.get_masters_pm();

        Mode_type_val = rdb.get_masters_pm_val();

        proudct_type = rdb.get_masters_pd();

        producttype_val = rdb.get_masters_pd_val();



        Intent i = getIntent();
        String alloc_losid_new = i.getStringExtra("los_alloc");
        System.out.println("aaaaaaaaaaaaa"+alloc_losid_new);
        if(alloc_losid_new!= null)
        {
            System.out.println("aaaaaaaaaaaaa"+alloc_losid_new);
            long alloc_los = Math.round(Double.parseDouble(alloc_losid_new));
            alloc_losid = String.valueOf(alloc_los);
        }





        Editor pedit = pref.edit();

        ts = getDateTime();


        pedit.putInt("is_map_captured", 0);

        pedit.putString("is_locate_clicked", "");

        pedit.commit();

//        cleanUpCancel();
        userFunction.resetData(com.example.hdb.EditRequest.this);
        userFunction.checkforLogout(pref, com.example.hdb.EditRequest.this);


        // 0 -
        // for
        // private
        // mode
        str_user_id = pref.getString("user_id", null);


        Calendar c = Calendar.getInstance();
        dTS = c.getTime();

        lay_collateral_detalis = (LinearLayout) findViewById(com.example.hdb.R.id.lay_collateral_detalis);
        lay_collateral_detalis1 = (LinearLayout) findViewById(com.example.hdb.R.id.lay_collateral_detalis1);
        checkLocation();
        setLayout();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) com.example.hdb.EditRequest.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }




    public Void setLayout() {
        checkLocation();
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        // showDate(year, month+1, day);

        if (alloc_losid != null) {
            losid = alloc_losid.trim();

            if (cd.isConnectingToInternet()) {
                new DownloadJSON().execute();
            } else {
                userFunction.noInternetConnection(getResources().getString(com.example.hdb.R.string.no_internet), com.example.hdb.EditRequest.this);
            }
        }

        Button btn_clear = (Button) findViewById(com.example.hdb.R.id.btn_clear);
        btn_clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
                userFunction.resetData(com.example.hdb.EditRequest.this);
                Intent thisactivity = new Intent(com.example.hdb.EditRequest.this,
                        com.example.hdb.EditRequest.class);
                //thisactivity.putExtra("from", 1);
                startActivity(thisactivity);
                finish();
            }

        });

        btn_search = (Button) findViewById(com.example.hdb.R.id.btn_search);
        btn_search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    edt_customer_name.setText("");
                    edt_Account_Status.setText("");
                    edt_PDD_Status.setText("");

                    edt_ACH_status.setText("");

                    edt_customer_name.setEnabled(true);
                    edt_Account_Status.setEnabled(true);
                    edt_PDD_Status.setEnabled(true);
                    edt_ACH_status.setEnabled(true);

                    losid = edt_losid_no.getText().toString();

                    new DownloadJSON().execute();

                } else {
                    userFunction.cutomToast("No Internet Connection...",
                            com.example.hdb.EditRequest.this);
                }
            }

        });

        LinearLayout lay_quality_feedback =  findViewById(com.example.hdb.R.id.lay_quality_feedback);

        if (Integer.parseInt(str_designation_val) == 0) {
            lay_quality_feedback.setVisibility(View.GONE);
        } else {
            lay_quality_feedback.setVisibility(View.VISIBLE);

        }

        tbl_ssn =  findViewById(com.example.hdb.R.id.tableRow100);

        spn_Product_Type =  findViewById(com.example.hdb.R.id.spn_product_type);
        spn_visit_mode =  findViewById(com.example.hdb.R.id.spn_visitmode);
       // spn_bucket_type = (Spinner) findViewById(R.id.spn_Bucket_Type);
        spn_met_spoke =  findViewById(com.example.hdb.R.id.spin_Met_Spoke);
        spn_dispoCode =  findViewById(com.example.hdb.R.id.spn_DispoCode);
        spn_Mode =  findViewById(com.example.hdb.R.id.spn_Mode);
        spn_formsixty =  findViewById(com.example.hdb.R.id.spn_Form_60_collected);
        spn_sighted =  findViewById(com.example.hdb.R.id.spn_Sighted);
        spn_running_condition =  findViewById(com.example.hdb.R.id.spn_Running_Condition);
        spn_third_Party =  findViewById(com.example.hdb.R.id.spn_With_third_Party);
        spn_repo_doable =  findViewById(com.example.hdb.R.id.spn_Repo_Doable);

        spn_BIZ_type_val =  findViewById(com.example.hdb.R.id.spn_biz);
        spn_PROPERTY_type_val =  findViewById(com.example.hdb.R.id.spn_property);
        spn_Current_BIZ_type_val =  findViewById(com.example.hdb.R.id.spn_Current_BIZ);
        spn_Not_able_pay_type_val =  findViewById(com.example.hdb.R.id.spn_Not_able_to_pay);
        spn_Coll_Occupant_type_value =  findViewById(com.example.hdb.R.id.spn_Collateral_Occupant);
        spn_cust_has_loan =  findViewById(com.example.hdb.R.id.spn_Customer_has_hand_loans);


        edt_losid_no =  findViewById(com.example.hdb.R.id.edt_losid_no);
        edt_customer_name =  findViewById(com.example.hdb.R.id.edt_customer_name);
        edt_Account_Status =  findViewById(com.example.hdb.R.id.edt_Account_Status);
        edt_PDD_Status =  findViewById(com.example.hdb.R.id.edt_PDD_Status);
        edt_Number_Contacted =  findViewById(com.example.hdb.R.id.edt_Number_Contacted);
        txt_branch_name =  findViewById(com.example.hdb.R.id.txt_customer_branch_name);
        txt_bom_bkt =  findViewById(com.example.hdb.R.id.txt_bom_bkt);
        txt_risk_bkt =  findViewById(com.example.hdb.R.id.txt_risk_bkt);
        txt_lpp_due =  findViewById(com.example.hdb.R.id.txt_lpp_due);
        txt_cbc_due =  findViewById(com.example.hdb.R.id.txt_cbc_due);
        text_error_msg =  findViewById(com.example.hdb.R.id.text_error_msg);
        edt_Next_Follow_Date =  findViewById(com.example.hdb.R.id.edt_Next_Follow_Date);
        edt_Remarks_if_any =  findViewById(com.example.hdb.R.id.edt_Remarks_if_any);
        edt_New_Contact_Number =  findViewById(com.example.hdb.R.id.edt_New_Contact_Number);
        edt_New_Contact_Address =  findViewById(com.example.hdb.R.id.edt_New_Contact_Address);
        edt_New_Contact_Email =  findViewById(com.example.hdb.R.id.edt_New_Contact_Email);
        edt_Receipt_Number =  findViewById(com.example.hdb.R.id.edt_Receipt_Number);
        edt_Total_Amount_Rs =  findViewById(com.example.hdb.R.id.edt_Total_Amount_Rs_);
        location_lat =  findViewById(com.example.hdb.R.id.location_lat);
        location_long =  findViewById(com.example.hdb.R.id.location_long);

        edt_EMI_Amount_Rs =  findViewById(com.example.hdb.R.id.edt_EMI_Amount_Rs);
        edt_emi_amount_ser =  findViewById(com.example.hdb.R.id.edt_emi_amount);
        edt_Other_Amount_Rs =  findViewById(com.example.hdb.R.id.edt_Other_Amount_Rs);
        edt_Cheque_Num =  findViewById(com.example.hdb.R.id.edt_Cheque_Num);
        edt_PAN_no =  findViewById(com.example.hdb.R.id.edt_PAN_no);
        edt_third_Party_Name =  findViewById(com.example.hdb.R.id.edt_third_Party_Name);
        edt_third_Party_Address =  findViewById(com.example.hdb.R.id.edt_third_Party_Address);
        edt_max_emi_amt_capble =  findViewById(com.example.hdb.R.id.edt_max_emi_capable);
        edt_hand_loane_value =  findViewById(com.example.hdb.R.id.edt_hand_loan_value);
        edt_loan_mnth_emi =  findViewById(com.example.hdb.R.id.edt_hand_loan_emi);
        edt_ssn =  findViewById(com.example.hdb.R.id.edt_ssn);
        edt_ACH_status =  findViewById(com.example.hdb.R.id.edt_ACH_Status);
        edt_virtual_acc= findViewById(com.example.hdb.R.id.edt_virtual_acc);
        btn_locate_me =  findViewById(com.example.hdb.R.id.btn_locate_me);
        tb0 =  findViewById(com.example.hdb.R.id.tableRow0);

        simpleTextView =  findViewById(com.example.hdb.R.id.simpleTextView);
        text_error_comntacted =  findViewById(com.example.hdb.R.id.text_error_comntacted);



        if (alloc_losid != null) {
            edt_losid_no.setText(alloc_losid);
            str_losid_no = alloc_losid;

        }

       /*    System.out.println("lattttttttttttttttttttt" + lat);
         if (dbl_latitude >= 0) {
                lattitude = String.valueOf(dbl_latitude);
                longitude = String.valueOf(dbl_longitude);
                location_lat.setText(lattitude);
                location_long.setText(longitude);
            }*/


        lin_quality_fbck_amt =  findViewById(com.example.hdb.R.id.lin_quality_fbck_amt);
        lin_quality_fbck_amt.setVisibility(View.GONE);


        /*
        ArrayAdapter<String> spinnerArrayAdapteterComAvaSelection = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, Competitor_Avalabity_selction_arraylist);
        spinnerArrayAdapteterComAvaSelection.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spin_com_avalability_selection.setAdapter(spinnerArrayAdapteterComAvaSelection);
        spin_com_avalability_selection.setOnItemSelectedListener(this);
*/
        ArrayAdapter<String> adapter_state_p = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, proudct_type);
        adapter_state_p.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_Product_Type.setAdapter(adapter_state_p);
        spn_Product_Type.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state1 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Visit_Mode);
        adapter_state1.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_visit_mode.setAdapter(adapter_state1);
        spn_visit_mode.setOnItemSelectedListener(this);

      /*  ArrayAdapter<String> adapter_state2 = new MyAdapter(this, R.layout.sp_display_layout,R.id.txt_visit, Bucket_type);
        adapter_state2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_bucket_type.setAdapter(adapter_state2);
        spn_bucket_type.setOnItemSelectedListener(this);*/

        ArrayAdapter<String> adapter_state3 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Met_Spoke_type);
        adapter_state3.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_met_spoke.setAdapter(adapter_state3);
        spn_met_spoke.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state4 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, DispoCode_type);
        adapter_state4.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_dispoCode.setAdapter(adapter_state4);
        spn_dispoCode.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state5 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Mode_type);
        adapter_state5.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_Mode.setAdapter(adapter_state5);
        spn_Mode.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state6 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Form60_type);
        adapter_state6.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_formsixty.setAdapter(adapter_state6);
        spn_formsixty.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state7 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Sighted_type);
        adapter_state7.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_sighted.setAdapter(adapter_state7);
        spn_sighted.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state8 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, running_con_array);
        adapter_state8.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_running_condition.setAdapter(adapter_state8);
        spn_running_condition.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state9 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, with_third_party_array);
        adapter_state9.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_third_Party.setAdapter(adapter_state9);
        spn_third_Party.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state10 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, vehicle_repo_double);
        adapter_state10.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_repo_doable.setAdapter(adapter_state10);
        spn_repo_doable.setOnItemSelectedListener(this);


        ArrayAdapter<String> adapter_state11 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, BIZ_type_val);
        adapter_state11.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_BIZ_type_val.setAdapter(adapter_state11);
        spn_BIZ_type_val.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state12 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, PROPERTY_type_val);
        adapter_state12.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_PROPERTY_type_val.setAdapter(adapter_state12);
        spn_PROPERTY_type_val.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state13 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Current_BIZ_type_val);
        adapter_state13.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_Current_BIZ_type_val.setAdapter(adapter_state13);
        spn_Current_BIZ_type_val.setOnItemSelectedListener(this);


        ArrayAdapter<String> adapter_state14 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Not_able_pay_type_val);
        adapter_state14.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_Not_able_pay_type_val.setAdapter(adapter_state14);
        spn_Not_able_pay_type_val.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_state15 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Coll_Occupant_type_value);
        adapter_state15.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_Coll_Occupant_type_value.setAdapter(adapter_state15);
        spn_Coll_Occupant_type_value.setOnItemSelectedListener(this);

        adapter_state16 = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, Cou_hand_loan_array);
        adapter_state16.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_cust_has_loan.setAdapter(adapter_state16);
        spn_cust_has_loan.setOnItemSelectedListener(this);


        spn_distance_from_loc =  findViewById(com.example.hdb.R.id.spn_distance_from_loc);

        adapter_distance_type = new MyAdapter(this, com.example.hdb.R.layout.sp_display_layout, com.example.hdb.R.id.txt_visit, distance_type);
        adapter_distance_type.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spn_distance_from_loc.setAdapter(adapter_distance_type);
        spn_distance_from_loc.setOnItemSelectedListener(this);

        CameraActivity = this;
        edt_New_Contact_Number.addTextChangedListener(watch);


        Button btn_submit =  findViewById(com.example.hdb.R.id.btn_submit);
        btn_submit.setOnClickListener(new OnClickListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onClick(View v) {

                if (alloc_losid == null) {
                    str_losid_no = edt_losid_no.getText().toString();
                }
                str_customer_name = edt_customer_name.getText().toString();
                str_Account_Status = edt_Account_Status.getText().toString();
                str_PDD_Status = edt_PDD_Status.getText().toString();
               str_Number_Contacted = edt_Number_Contacted.getText()
                        .toString();
                str_Next_Follow_Date = edt_Next_Follow_Date.getText()
                        .toString();
                str_Remarks_if_any = edt_Remarks_if_any.getText().toString();
                str_New_Contact_Number = edt_New_Contact_Number.getText()
                        .toString();
                str_New_Contact_Address = edt_New_Contact_Address.getText()
                        .toString();
                str_New_Contact_Email = edt_New_Contact_Email.getText()
                        .toString();
                str_Receipt_Number = edt_Receipt_Number.getText().toString();
                str_Total_Amount_Rs = edt_Total_Amount_Rs.getText().toString();
                str_EMI_Amount_Rs = edt_EMI_Amount_Rs.getText().toString();
                str_Other_Amount_Rs = edt_Other_Amount_Rs.getText().toString();
                str_Cheque_Num = edt_Cheque_Num.getText().toString();
                str_PAN_no = edt_PAN_no.getText().toString();
                str_third_Party_Name = edt_third_Party_Name.getText()
                        .toString();
                str_third_Party_Address = edt_third_Party_Address.getText()
                        .toString();

                str_ssn = edt_ssn.getText()
                        .toString();

                str_ACH_status = edt_ACH_status.getText()
                        .toString();

                str_txt_distacne = txt_distacne.getText().toString();

                //  ts=getDateTime();

                //   ts = pData.getString("str_timestamp", null);


                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                Upload_Date = dateFormat.format(date);
                boolean ins = true;

                str_Data_Entry_Type = "CO";

                try {
                    if (str_losid_no.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select LOSID",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_customer_name.equals("") && ins == true) {
                        userFunction.cutomToast("Please Enter customer name",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_product.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Product",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_Data_Entry_Type.equals("") && ins == true) {
                        userFunction.cutomToast(
                                "Please Select Data Entry Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_visit_mode.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Visit Mode",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_met_spoke.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Met/Spoke",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_dispoCode.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Dispo Code",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }




                try {
                    if (str_Remarks_if_any.equals("")
                            && str_Data_Entry_Type.equals("CO") && ins == true) {
                        userFunction.cutomToast("Please Enter Remarks..",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_dispoCode != null && !str_dispoCode.isEmpty() && ins == true) {

                        if ((str_dispoCode.contains("PAY") || str_dispoCode.contains("PAY-SE")) && ins == true) {
                            if (str_Receipt_Number.equals("") || str_Total_Amount_Rs.equals("") || str_EMI_Amount_Rs.equals("") || str_Other_Amount_Rs.equals("") && ins == true) {
                                userFunction.cutomToast("Please Enter complete Payment Details",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }

                            if( str_Mode.isEmpty() && str_Mode.equals("") && ins == true) {
                                userFunction.cutomToast("Please Select Mode of Payment",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }










                try {
                    if (str_visit_mode.equals("PH") && ins == true) {

                        if (str_Number_Contacted.equals("") == true) {
                            userFunction.cutomToast("Please Add Contact No",
                                    com.example.hdb.EditRequest.this);
                            ins = false;
                        }

                        if (!str_Number_Contacted.isEmpty()) {
                            Long con_no = Long.parseLong(str_Number_Contacted);
                            if (con_no < 10) {
                                userFunction.cutomToast("Please Enter Valid Number",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*
                System.out.println("str_bucket_type::::" + str_bucket_type);
                try {
                    if (str_bucket_type.equals("") && ins == true) {
                        if (str_Data_Entry_Type.equals("CO")) {
                            userFunction.cutomToast("Please Select Bucket Type",
                                    EditRequest.this);
                            ins = false;

                        } else {
                            ins = true;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

*/


                try {
                    if (str_dispoCode.equals("REDEP")
                            && str_Next_Follow_Date.equals("") && ins == true) {
                        userFunction.cutomToast("Please Select Next Follow date..",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }





                try {
                    if (str_product.equals("GOLD") || str_product.equals("LASH") ||
                            str_product.equals("LAG") || str_product.equals("P") || str_product.equals("") && ins == true) {

                    } else {
                        if (str_sighted.equals("") && ins == true) {

                            userFunction.cutomToast(
                                    "Please Select Collateral Sighted",
                                    com.example.hdb.EditRequest.this);
                            ins = false;

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_product.equals("GOLD") || str_product.equals("LASH") ||
                            str_product.equals("LAG") || str_product.equals("P") || str_product.equals("")) {
                        //  ins = true;
                    } else {
                        if (str_product.equals("TRL") || str_product.equals("UCL") ||
                                str_product.equals("TW") || str_product.equals("CVEH") || str_product.equals("CE")) {
                            if (str_running_condition.equals("")
                                    && str_sighted.equals("Y") && ins == true) {
                                userFunction.cutomToast(
                                        "Please Select Running Condition",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_third_Party.equals("") && ins == true) {
                        if (str_Data_Entry_Type.equals("NW") == true) {
                            //  ins = true;

                        }
                    } else {
                        if (str_third_Party.equals("Y") && ins == true) {
                            if (str_third_Party_Name.equals("") == true) {

                                userFunction.cutomToast("Please Fill  3rd Party Name",
                                        com.example.hdb.EditRequest.this);
                                if (str_third_Party_Address.equals("") == true) {

                                    userFunction.cutomToast("Please Fill  3rd Party Address",
                                            com.example.hdb.EditRequest.this);
                                }
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_Data_Entry_Type.equals("CO")) {
                        if (str_dispoCode.equals("PAY")) {

                            if (str_PAN_no != null) {
                                if (!CheckPanNO(str_PAN_no)) {
                                    if (Integer.parseInt(str_Total_Amount_Rs) >= 50000) {
                                        if (str_Mode.equals("CH")) {
                                            Toast.makeText(com.example.hdb.EditRequest.this, "ENTER VALID PAN NO..",
                                                    Toast.LENGTH_LONG).show();
                                            ins = false;
                                        }
                                    } else {
                                        //  ins = true;
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_Data_Entry_Type.equals("CO")) {
                        if (str_dispoCode.equals("PAY-SE")) {

                            if (str_PAN_no != null) {
                                if (!CheckPanNO(str_PAN_no)) {
                                    if (Integer.parseInt(str_Total_Amount_Rs) >= 50000) {
                                        if (str_Mode.equals("CH")) {
                                            Toast.makeText(com.example.hdb.EditRequest.this, "ENTER VALID PAN NO..",
                                                    Toast.LENGTH_LONG).show();
                                            ins = false;
                                        }

                                    } else {
                                        // ins = true;
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (str_Data_Entry_Type.equals("CO")) {
                        if (str_dispoCode.equals("PAY")) {
                        // System.out.println("bbbjhhhhbbbbjhhhhh");
                            if (str_Mode.equals("CQ")) {
                             //   System.out.println("aaaaaaaaaaaaaaaaaaa");
                                if ( str_Cheque_Num.isEmpty() && str_Cheque_Num.equals("")) {
                                //    System.out.println("ccccccccccccccccc");
                                    edt_Cheque_Num.setError("ENTER VALID CHEQUE  NO.");
                                    edt_Cheque_Num.requestFocus();
                                   // Toast.makeText(com.example.hdb.EditRequest.this, "ENTER VALID CHEQUE  NO..",Toast.LENGTH_LONG).show();
                                    ins = false;
                                } else {
                                    // ins = true;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_Data_Entry_Type.equals("CO")) {
                        if (str_dispoCode.equals("PAY-SE")) {
                            if (str_Mode.equals("CQ")) {
                                if ( str_Cheque_Num.isEmpty() && str_Cheque_Num.equals("")) {
                                    edt_Cheque_Num.setError("ENTER VALID CHEQUE  NO.");
                                    edt_Cheque_Num.requestFocus();
                                    ins = false;
                                } else {
                                    // ins = true;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                ////---------------  str_label Feedback Options  starts here  -------------

                str_max_emi_amt_capble = edt_max_emi_amt_capble.getText().toString();
                str_hand_loane_value = edt_hand_loane_value.getText().toString();
                str_loan_mnth_emi = edt_loan_mnth_emi.getText().toString();


                try {
                    if ((str_BIZ_type_val.equals("") || str_BIZ_type_val.equals("null")) && ins == true) {
                        userFunction.cutomToast("Please Select business Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if ((str_PROPERTY_type_val.equals("") || str_PROPERTY_type_val.equals("null")) && (str_product.equals("Lap") &&
                            ins == true)) {
                        userFunction.cutomToast("Please Select Property Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }

                try {
                    if ((str_Current_BIZ_type_val.equals("") || str_Current_BIZ_type_val.equals("null")) && ins == true) {
                        userFunction.cutomToast("Please Current business Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if ((str_Not_able_pay_type_val.equals("") || str_Not_able_pay_type_val.equals("null")) && (str_dispoCode.equals("FB") &&
                            ins == true)) {
                        userFunction.cutomToast("Please Select Not able to pay Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if ((str_Coll_Occupant_type_value.equals("") || str_Coll_Occupant_type_value.equals("null")) && (str_product.equals("Lap") &&
                            ins == true)) {
                        userFunction.cutomToast("Please Select Coll Occupant Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if ((str_cust_has_loan.equals("") || str_cust_has_loan.equals("null")) &&
                            ins == true) {
                        userFunction.cutomToast("Please Select Customer has loan Type",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_max_emi_amt_capble.equals("") && ins == true) {

                        userFunction.cutomToast("Enter Max Emi Amt Capable ",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    } else {
                        try {

                            if ((str_max_emi_amt_capble.equals("0") || str_max_emi_amt_capble.equals("0.0")) && ins == true) {
                                userFunction.cutomToast("Please Enter non zero max EMI capable",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_cust_has_loan.equals("Y") == true) {

                        if (str_hand_loane_value.equals("") && ins == true) {

                            userFunction.cutomToast("Enter Hand loan Amt ",
                                    com.example.hdb.EditRequest.this);
                            ins = false;
                        } else {
                            try {

                                if ((str_hand_loane_value.equals("0") || str_hand_loane_value.equals("0.0")) && ins == true) {
                                    userFunction.cutomToast("Please Enter non zero hand loan value",
                                            com.example.hdb.EditRequest.this);
                                    ins = false;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        if (str_loan_mnth_emi.equals("") && ins == true) {

                            userFunction.cutomToast("Enter Loan Month Emi Amt ",
                                    com.example.hdb.EditRequest.this);
                            ins = false;
                        } else {
                            try {

                                if ((str_loan_mnth_emi.equals("0") || str_loan_mnth_emi.equals("0.0")) && ins == true) {
                                    userFunction.cutomToast("Please Enter non zero hand loan EMI",
                                            com.example.hdb.EditRequest.this);
                                    ins = false;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_ssn.equals("") && str_dispoCode.equals("FB") &&
                            ins == true) {
                        userFunction.cutomToast("Please Enter Settlement Shot No.",
                                com.example.hdb.EditRequest.this);
                        ins = false;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if ((str_dispoCode.equals("PTP") || str_dispoCode.equals("CB")) && (str_Next_Follow_Date.equals("") &&
                            ins == true)) {
                        userFunction.cutomToast("Please Enter Next Follow up Date",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (str_Remarks_if_any.length() < 10 &&
                            ins == true) {
                        userFunction.cutomToast("Please Enter Atleast 10 characters in Remarks",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    is_locate_clicked = pref.getString("is_locate_clicked", "");

                    if (is_locate_clicked.equals("") &&
                            ins == true) {
                        userFunction.cutomToast("Please set your Location by clicking Locate me Button",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    if ((str_txt_distacne == null || str_txt_distacne.isEmpty() || str_txt_distacne.equals("null")) && ins == true) {
                        userFunction.cutomToast("Please Insert Distance",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if (str_distance_type == null && ins == true) {
                        userFunction.cutomToast("Please Select Locate Me Distance",
                                com.example.hdb.EditRequest.this);
                        ins = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    if ((!str_New_Contact_Number.equals("") && ins == true)) {

                        if (str_New_Contact_Number.length() != 10) {
                            userFunction.cutomToast("Please enter Valid contact number",
                                    com.example.hdb.EditRequest.this);

                            ins = false;
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                ////---------------  Quality  Feedback Options  ends  here  -------------
              //  String map_latitude = pref.getString("pref_latitude", "0");
               // String map_longitude = pref.getString("pref_longitude", "0");
                map_address = pref.getString("map_address", null);


                lattitude = location_lat.getText().toString().trim();
                longitude = location_long.getText().toString().trim();


                map_captured = pref.getInt("is_map_captured", 0);

                try {
                    if (map_captured == 1 && ins == true) {

                        if (str_distance_type.equals("Map not loading") == true) {
                            if (dbl_latitude > 0) {
                                userFunction.cutomToast("You already captured map... select other Distance type..",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (map_captured == 1 && ins == true) {
                        if ((str_distance_type.equals("Exact location")) || ((str_distance_type.equals("Map not loading")))) {
                            ins = true;
                        } else {
                            if (Integer.parseInt(str_txt_distacne) == 0) {
                                userFunction.cutomToast("Distance cant be a Zero...",
                                        com.example.hdb.EditRequest.this);
                                ins = false;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (ins == true) {


                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    current_date = sdf.format(new Date());
                    int_dist_count = rdb.getDistanceCount(current_date);


                    if (int_dist_count > 0) {
                        String strlatituder = rdb.get_Lat_Long(current_date);

                        String[] separated = strlatituder.split(",");
                        double double_distsance = distance(dbl_latitude,
                                dbl_longitude, dbl_latitude, dbl_longitude,
                                "K");
                        str_disance = Double.toString(double_distsance);

                    } else {
                        rdb.delete_dist_All();

                    }

                    if (str_visit_mode.equals("PH")) {
                        pic_count = "1";
                    }

                    String img_new_path = pref.getString("map_img_name", null);

                    insertDataSqlite();


                }

            }

        });

        btn_locate_me.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!cd.isConnectingToInternet()) {
                    userFunction.cutomToast(getResources().getString(com.example.hdb.R.string.no_internet), com.example.hdb.EditRequest.this);
                }
                checkLocation();

               // if(isLocationEnabled()== true) {
                    str_losid_no = edt_losid_no.getText().toString();

                    if (str_losid_no.equals("")) {
                        missingLOSID();
                    } else {


                       // if (map_captured == 0) {

                            map_captured = pref.getInt("is_map_captured", 0);
                            Editor peditor = pref.edit();
                            peditor.putString("is_locate_clicked", "");
                            peditor.commit();

                            Intent img_pinch = new Intent(
                                    com.example.hdb.EditRequest.this, com.example.hdb.MapsActivity.class);
                            img_pinch.putExtra("str_losid_map", str_losid_no);
                            img_pinch.putExtra("str_ts", ts);
                            startActivity(img_pinch);

                       // } else {
                        //    userFunction.cutomToast(getResources().getString(com.example.hdb.R.string.map_cant_select),
                        //            getApplicationContext());
                       // }
                    }
               // }
            }
        });


        btn_dashboard =  findViewById(com.example.hdb.R.id.btn_dshboard);
        btn_dashboard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
                userFunction.resetData(com.example.hdb.EditRequest.this);
                Intent messagingActivity = new Intent(com.example.hdb.EditRequest.this,
                        com.example.hdb.Dashboard.class);
                startActivity(messagingActivity);
                finish();
            }

        });

        btn_dataentry =  findViewById(com.example.hdb.R.id.btn_data_entry);
        btn_dataentry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanUpCancel();
                userFunction.resetData(com.example.hdb.EditRequest.this);
                Intent messagingActivity = new Intent(com.example.hdb.EditRequest.this,
                        com.example.hdb.EditRequest.class);
                startActivity(messagingActivity);
                finish();
            }

        });
        return null;
    }




    TextWatcher watch = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            // text_error_msg.setText(s);
            if (a == 9) {
                if (cd.isConnectingToInternet()) {
                    if (contacts_flag.equals("1")) {
                        new checkcontactdetails().execute();
                    }
                }
                // Toast.makeText(getApplicationContext(), "Maximum Limit Reached", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onBackPressed() {
        // your code.
        cleanUpCancel();
        userFunction.resetData(com.example.hdb.EditRequest.this);
        finish();
    }

    private void missingLOSID() {
        userFunction.cutomToast("Please Enter The LOSID",
                getApplicationContext());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        switch (parent.getId()) {
            case com.example.hdb.R.id.spn_data_entry_typee:
                // Do stuff for spinner1
                // selState = (String) sp.getSelectedItem();
                TableRow tblrow =  findViewById(com.example.hdb.R.id.tableRow9);
                TableRow tblrow1 =  findViewById(com.example.hdb.R.id.tableRow6);
                TableRow tableRow10 =  findViewById(com.example.hdb.R.id.tableRow10);
                TableRow tableRow7 =  findViewById(com.example.hdb.R.id.tableRow7);
                TableRow tableRow3 =  findViewById(com.example.hdb.R.id.tableRow3);
                TableRow tableRow2 =  findViewById(com.example.hdb.R.id.tableRow2);

                TableRow tblrow32 =  findViewById(com.example.hdb.R.id.tableRow32);
                TableRow tblrow33 =  findViewById(com.example.hdb.R.id.tableRow33);
                TableRow tblrow34 =  findViewById(com.example.hdb.R.id.tableRow34);
                TableRow tblrow35 =  findViewById(com.example.hdb.R.id.tableRow35);


                LinearLayout lay_new_cont_detalis =  findViewById(com.example.hdb.R.id.lay_new_cont_detalis);


                LinearLayout lay_bom =  findViewById(com.example.hdb.R.id.lay_bom);

                TextView txt_collateral_1 =  findViewById(com.example.hdb.R.id.txt_collateral_1);
                TextView txt_collateral_2 =  findViewById(com.example.hdb.R.id.txt_collateral_2);
                TextView txt_losid =  findViewById(com.example.hdb.R.id.lbl_losid);

                if (position == 0) {
                    tblrow.setVisibility(View.VISIBLE);
                    tb0.setVisibility(View.VISIBLE);
                    tblrow1.setVisibility(View.GONE);
                    tblrow32.setVisibility(View.GONE);
                    tblrow33.setVisibility(View.GONE);
                    tblrow34.setVisibility(View.GONE);
                    tblrow35.setVisibility(View.GONE);

                    lay_bom.setVisibility(View.VISIBLE);
                    txt_losid.setText("LOSID");
                    btn_search.setVisibility(View.VISIBLE);

                    tableRow7.setVisibility(View.VISIBLE);
                    lay_new_cont_detalis.setVisibility(View.VISIBLE);

                    lay_collateral_detalis.setVisibility(View.GONE);
                    lay_collateral_detalis1.setVisibility(View.GONE);

                    tableRow2.setVisibility(View.VISIBLE);
                    tableRow3.setVisibility(View.VISIBLE);
                    tableRow10.setVisibility(View.VISIBLE);
                }

            /*
            else {
				tblrow.setVisibility(View.GONE);
				tblrow1.setVisibility(View.GONE);
				tblrow32.setVisibility(View.GONE);
				tblrow33.setVisibility(View.GONE);
				tblrow34.setVisibility(View.GONE);
				tblrow35.setVisibility(View.GONE);
				tableRow10.setVisibility(View.GONE);
				lay_losid.setVisibility(View.VISIBLE);
				txt_losid.setText("LOSID");
				btn_search.setVisibility(View.GONE);
				lay_collateral_detalis.setVisibility(View.VISIBLE);
				lay_new_cont_detalis.setVisibility(View.GONE);
				lay_photos.setVisibility(View.VISIBLE);
				tableRow7.setVisibility(View.GONE);
				tableRow2.setVisibility(View.GONE);
				tableRow3.setVisibility(View.GONE);
				lay_bom.setVisibility(View.GONE);
                txt_collateral_3.setText("Others");

			}

			if (position == 4) {
				tblrow32.setVisibility(View.VISIBLE);
				tblrow33.setVisibility(View.VISIBLE);
				tblrow34.setVisibility(View.VISIBLE);
				tblrow35.setVisibility(View.VISIBLE);
				lay_losid.setVisibility(View.VISIBLE);
				txt_losid.setText("App Form");
				btn_search.setVisibility(View.GONE);
				lay_collateral_detalis.setVisibility(View.GONE);
				lay_photos.setVisibility(View.VISIBLE);
				tblrow1.setVisibility(View.GONE);
				lay_new_cont_detalis.setVisibility(View.GONE);
				lay_bom.setVisibility(View.GONE);

				txt_collateral_1.setText("Others");
				txt_collateral_2.setText("Others");
                txt_collateral_3.setText("Others");
			}
			if (position == 0) {
				lay_losid.setVisibility(View.GONE);
				lay_collateral_detalis.setVisibility(View.GONE);
				lay_new_cont_detalis.setVisibility(View.GONE);
				lay_photos.setVisibility(View.GONE);
				tblrow1.setVisibility(View.GONE);
				lay_collateral_detalis.setVisibility(View.GONE);
				tblrow1.setVisibility(View.GONE);
				lay_bom.setVisibility(View.GONE);
                txt_collateral_3.setText("Others");

			}
*/

                // Toast.makeText(getApplicationContext(), "selected::" +
                // Dataentrytype_val[position], Toast.LENGTH_SHORT).show();
                break;
            case com.example.hdb.R.id.spn_visitmode:
                // Do stuff for spinner2
                // selState = (String) spnr1.getSelectedItem();
                str_visit_mode = Visit_Mode_val[position].toString();

                if (str_visit_mode.equals("VSOF")) {

                    vis_mode = "Office";

                }
                if (str_visit_mode.equals("VSRS")) {

                    vis_mode = "Residence";

                }
                if (str_visit_mode.equals("PH")) {

                    vis_mode = "";


                }
                if (str_visit_mode.equals("VSOTH")) {

                    vis_mode = "Others";

                }
                if (str_visit_mode.equals("VSTHP")) {

                    vis_mode = "Third Party";

                }

              /*  TableRow tblrow3 = (TableRow) findViewById(R.id.tableRow6);
                if (position == 1) {
                    tblrow3.setVisibility(View.VISIBLE);
                } else {
                    tblrow3.setVisibility(View.GONE);

                }
                if (position == 0) {
                    tblrow3.setVisibility(View.GONE);
                }*/

                break;

           /* case R.id.spn_Bucket_Type:
                // Do stuff for spinner2
                // selState = (String) spnr2.getSelectedItem();
                str_bucket_type = Bucket_type_val[position].toString();

                // Toast.makeText(getApplicationContext(), "selected::" +
                // Bucket_type_val[position], Toast.LENGTH_SHORT).show();
                break;*/

            case com.example.hdb.R.id.spin_Met_Spoke:
                // Do stuff for spinner1
                // selState = (String) spnr.getSelectedItem();
                str_met_spoke = Met_Spoke_type_val[position].toString();

                // Toast.makeText(getApplicationContext(), "selected::" +
                // Met_Spoke_type_val[position], Toast.LENGTH_SHORT).show();
                break;
            case com.example.hdb.R.id.spn_product_type:
                // Do stuff for spinner1
                str_product = producttype_val.get(position).toString();

                TableRow tbl_property =  findViewById(com.example.hdb.R.id.tbl_property);
                TableRow tbl_coll_ocupied =  findViewById(com.example.hdb.R.id.tbl_coll_ocupied);


                if (producttype_val.get(position).equals("Lap")) {
                    tbl_property.setVisibility(View.VISIBLE);
                    tbl_coll_ocupied.setVisibility(View.VISIBLE);
                } else {
                    tbl_property.setVisibility(View.GONE);
                    tbl_coll_ocupied.setVisibility(View.GONE);
                }


                if (producttype_val.get(position).equals("GOLD") || producttype_val.get(position).equals("LASH") ||
                        producttype_val.get(position).equals("LAG") || producttype_val.get(position).equals("P")
                        || producttype_val.get(position).equals("")) {
                    lay_collateral_detalis.setVisibility(View.GONE);

                } else {
                    if (producttype_val.get(position).equals("TRL") || producttype_val.get(position).equals("UCL") ||
                            producttype_val.get(position).equals("TW") || producttype_val.get(position).equals("CVEH") ||
                            producttype_val.get(position).equals("CE")) {
                        lay_collateral_detalis.setVisibility(View.VISIBLE);
                        lay_collateral_detalis1.setVisibility(View.VISIBLE);
                    } else {
                        lay_collateral_detalis.setVisibility(View.VISIBLE);
                        lay_collateral_detalis1.setVisibility(View.GONE);
                    }
                }


                break;
            case com.example.hdb.R.id.spn_DispoCode:
                // Do stuff for spinner1
                // selState = (String) spnr.getSelectedItem();
                str_dispoCode = DispoCode_type_val.get(position).toString();
                TableRow tblrow15 =  findViewById(com.example.hdb.R.id.tableRow16);

                TableRow tblrow5 =  findViewById(com.example.hdb.R.id.tableRow17);
                TableRow tblrow6 =  findViewById(com.example.hdb.R.id.tableRow18);
                TableRow tblrow7 =  findViewById(com.example.hdb.R.id.tableRow19);
                TableRow tblrow8 =  findViewById(com.example.hdb.R.id.tableRow20);
                TableRow tblrow9 =  findViewById(com.example.hdb.R.id.tableRow21);
                TableRow tblrow10 =  findViewById(com.example.hdb.R.id.tableRow22);
                TableRow tblrow11 =  findViewById(com.example.hdb.R.id.tableRow23);
                TableRow tblrow12 =  findViewById(com.example.hdb.R.id.tableRow24);
                TableRow tableRow222 =  findViewById(com.example.hdb.R.id.tableRow22);


                if (DispoCode_type_val.get(position).equals("PAY")) {
                    tblrow5.setVisibility(View.VISIBLE);
                    tblrow6.setVisibility(View.VISIBLE);
                    tblrow7.setVisibility(View.VISIBLE);
                    tblrow8.setVisibility(View.VISIBLE);
                    tblrow9.setVisibility(View.VISIBLE);
                    tblrow10.setVisibility(View.VISIBLE);
                    tblrow11.setVisibility(View.VISIBLE);
                    tblrow12.setVisibility(View.VISIBLE);
                    tblrow15.setVisibility(View.VISIBLE);
                    tableRow222.setVisibility(View.GONE);

                } else if (DispoCode_type_val.get(position).equals("PAY-SE")) {

                    tblrow5.setVisibility(View.VISIBLE);
                    tblrow6.setVisibility(View.VISIBLE);
                    tblrow7.setVisibility(View.VISIBLE);
                    tblrow8.setVisibility(View.VISIBLE);
                    tblrow9.setVisibility(View.VISIBLE);
                    tblrow10.setVisibility(View.VISIBLE);
                    tblrow11.setVisibility(View.VISIBLE);
                    tblrow12.setVisibility(View.VISIBLE);
                    tblrow15.setVisibility(View.VISIBLE);
                    tableRow222.setVisibility(View.GONE);

                } else {
                    tblrow5.setVisibility(View.GONE);
                    tblrow6.setVisibility(View.GONE);
                    tblrow7.setVisibility(View.GONE);
                    tblrow8.setVisibility(View.GONE);
                    tblrow9.setVisibility(View.GONE);
                    tblrow10.setVisibility(View.GONE);
                    tblrow11.setVisibility(View.GONE);
                    tblrow12.setVisibility(View.GONE);
                    tblrow15.setVisibility(View.GONE);
                }

                if (position == 0) {
                    tblrow5.setVisibility(View.GONE);
                    tblrow6.setVisibility(View.GONE);
                    tblrow7.setVisibility(View.GONE);
                    tblrow8.setVisibility(View.GONE);
                    tblrow9.setVisibility(View.GONE);
                    tblrow10.setVisibility(View.GONE);
                    tblrow11.setVisibility(View.GONE);
                    tblrow12.setVisibility(View.GONE);
                    tblrow15.setVisibility(View.GONE);
                }


                TableRow tbl_nt_able_to_pay =  findViewById(com.example.hdb.R.id.tbl_nt_able_to_pay);
                if (position == 7) {
                    tbl_nt_able_to_pay.setVisibility(View.VISIBLE);
                } else {
                    tbl_nt_able_to_pay.setVisibility(View.GONE);
                }

                if (DispoCode_type_val.get(position).equals("SETTLEMENT")) {
                    tbl_ssn.setVisibility(View.VISIBLE);
                } else {
                    tbl_ssn.setVisibility(View.GONE);
                }


                if (DispoCode_type_val.get(position).equals("PTP") || DispoCode_type_val.get(position).equals("CB")
                        || DispoCode_type_val.get(position).equals("REDEP")) {

                    tbl_next_date.setVisibility(View.VISIBLE);
                } else {
                    tbl_next_date.setVisibility(View.GONE);
                }


                break;
            case com.example.hdb.R.id.spn_Mode:
                // Do stuff for spinner1
                str_Mode = Mode_type_val.get(position).toString();
                TableRow tableRow22 = (TableRow) findViewById(com.example.hdb.R.id.tableRow22);
                if (Mode_type_val.get(position).equals("CQ")) {
                    tableRow22.setVisibility(View.VISIBLE);
                } else {
                    tableRow22.setVisibility(View.GONE);
                }
                break;

            case com.example.hdb.R.id.spn_Sighted:
                str_sighted = Form60_type_val[position].toString();

                TableRow tblrow27 = (TableRow) findViewById(com.example.hdb.R.id.tableRow27);


                if (position == 1) {
                    tblrow27.setVisibility(View.VISIBLE);

                    Editor editor = pref.edit();
                    editor.putString("coll_sighted", "yes");

                    editor.commit();


                } else {

                    Editor editor = pref.edit();
                    editor.putString("coll_sighted", "no");

                    editor.commit();

                    tblrow27.setVisibility(View.GONE);

                }

                break;
            case com.example.hdb.R.id.spn_Running_Condition:
                str_running_condition = Form60_type_val[position].toString();
                break;
            case com.example.hdb.R.id.spn_With_third_Party:

                str_third_Party = Form60_type_val[position].toString();
                TableRow tblrow29 =  findViewById(com.example.hdb.R.id.tableRow29);
                TableRow tblrow30 =  findViewById(com.example.hdb.R.id.tableRow30);

                if (position == 0) {
                    tblrow29.setVisibility(View.GONE);
                    tblrow30.setVisibility(View.GONE);
                }
                if (position == 2) {
                    tblrow29.setVisibility(View.GONE);
                    tblrow30.setVisibility(View.GONE);
                }
                if (position == 1) {
                    tblrow29.setVisibility(View.VISIBLE);
                    tblrow30.setVisibility(View.VISIBLE);
                }
                break;


            case com.example.hdb.R.id.spn_biz:
                str_BIZ_type_val = BIZ_type_val[position].toString();
                if (position == 0) {
                    str_BIZ_type_val = "";
                }
                break;

            case com.example.hdb.R.id.spn_property:
                str_PROPERTY_type_val = PROPERTY_type_val[position].toString();
                if (position == 0) {
                    str_PROPERTY_type_val = "";
                }
                break;


            case com.example.hdb.R.id.spn_Current_BIZ:
                str_Current_BIZ_type_val = Current_BIZ_type_val[position].toString();
                if (position == 0) {
                    str_Current_BIZ_type_val = "";
                }
                break;

            case com.example.hdb.R.id.spn_Not_able_to_pay:
                str_Not_able_pay_type_val = Not_able_pay_type_val[position].toString();
                if (position == 0) {
                    str_Not_able_pay_type_val = "";
                }
                break;

            case com.example.hdb.R.id.spn_Collateral_Occupant:
                str_Coll_Occupant_type_value = Coll_Occupant_type_value[position].toString();
                if (position == 0) {
                    str_Coll_Occupant_type_value = "";
                }
                break;
            case com.example.hdb.R.id.spn_Customer_has_hand_loans:
                str_cust_has_loan = Form60_type_val[position].toString();
                if (position == 0) {
                    lin_quality_fbck_amt.setVisibility(View.GONE);
                }
                if (position == 2) {
                    lin_quality_fbck_amt.setVisibility(View.GONE);
                }

                if (position == 1) {
                    lin_quality_fbck_amt.setVisibility(View.VISIBLE);
                }
                break;

            case com.example.hdb.R.id.spn_distance_from_loc:
                str_distance_type = distance_type[position];
                if (position == 0) {
                    str_distance_type = null;
                }
                break;


        }

    }

    private Uri getOutputMediaFile(int type) {
        File mediaStorageDir = null;

        try {
            String extStorageDirectory = Environment
                    .getExternalStorageDirectory().toString();

            // External sdcard location
            mediaStorageDir = new File(extStorageDirectory
                    + IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // Create a media file name

        Uri imgUri = null;
        try {
            String user_id = pref.getString("user_id", null);
            String curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));
            String losid_no = pData.getString("str_losid_no", null);
            String timestamp = pData.getString("str_timestamp", null);

            if (type == MEDIA_TYPE_IMAGE) {
                file = new File(mediaStorageDir, user_id + "_" + losid_no + "_"
                        + timestamp + "_" + curr_no + ".jpg");

                if (file.exists()) {
                    file.delete();
                }
                imgUri = Uri.fromFile(file);
                this.imgPath = file.getAbsolutePath();

                Editor editor = pData.edit();

                switch (Integer.parseInt(curr_no)) {
                    case 1:
                        editor.putString("str_DOC_1", this.imgPath);

                        break;
                    case 2:
                        editor.putString("str_DOC_2", this.imgPath);
                        break;
                    case 3:
                        editor.putString("str_DOC_3", this.imgPath);
                        break;
                    case 4:
                        editor.putString("str_DOC_4", this.imgPath);
                        break;
                    case 5:
                        editor.putString("str_DOC_5", this.imgPath);
                        break;
                    case 6:
                        editor.putString("str_DOC_6", this.imgPath);
                        break;
                    case 7:
                        editor.putString("str_DOC_7", this.imgPath);
                        break;
                    case 8:
                        editor.putString("str_DOC_8", this.imgPath);
                        break;
                    case 9:
                        editor.putString("str_DOC_9", this.imgPath);
                        break;
                    case 10:
                        editor.putString("str_DOC_10", this.imgPath);
                        break;
                    default:
                        break;

                }
                editor.commit();

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return imgUri;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // DownloadJSON AsyncTask
    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(com.example.hdb.EditRequest.this);
            mProgressDialog.setMessage(getString(com.example.hdb.R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            /*
            if (str_designation.equals("CO")) {
                str_designation_val = "0";
            } else if (str_designation.equals("CA")) {
                str_designation_val = "0";

            } else if (str_designation.equals("BCM")) {
                str_designation_val = "0";

            } else if (str_designation.equals("RM")) {
                str_designation_val = "0";

            } else if (str_designation.equals("BOM")) {
                str_designation_val = "0";

            } else if (str_designation.equals("EXE-GL")) {
                str_designation_val = "0";

            } else {
                str_designation_val = "1";
            }
*/

            lattitude = location_lat.getText().toString().trim();
            longitude = location_long.getText().toString().trim();

            jsonobject = userFunction.getLoan(losid, lattitude, longitude, str_user_id, str_designation_val, str_designation);
            if (jsonobject != null) {
                // Create an array
                try {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            JSONObject json = jsonobject.getJSONObject("data");

                            datafound = true;
                            str_text = json.getString("test");
                            str_customer_name = json.getString("name");
                            str_Account_Status = json.getString("acc_status");
                            str_PDD_Status = json.getString("pdd_status");
                            str_branch_name = json.getString("branch_name");
                            str_bom_bkt = json.getString("bom_bkt");
                            str_risk_bkt = json.getString("risk_bkt");
                            str_cbc_due = json.getString("CBC_DUE");
                            str_lpp_due = json.getString("LPP_DUE");

                            str_product = json.getString("product");
                            str_EMI_Amount_Rs_ser = json.getString("EMI");
                            str_ACH_status = json.getString("ach_status");
                            str_virtual_acc_no= json.getString("virtual_acc_no");

                            str_contact_1 = json.getString("Contact_1");
                            str_contact_2 = json.getString("Contact_2");
                            //quality feedback

                            str_BIZ_type_val = json.getString("biz");
                            str_PROPERTY_type_val = json.getString("property");
                            str_max_emi_amt_capble = json.getString("max_emi_amt");
                            str_Current_BIZ_type_val = json.getString("cur_biz_from_same_place");
                            str_Not_able_pay_type_val = json.getString("unable_to_pay_period");
                            str_Coll_Occupant_type_value = json.getString("coll_occupant");
                            str_cust_has_loan = json.getString("cust_has_hand_loans");
                            str_hand_loane_value = json.getString("hand_loan_value");
                            str_loan_mnth_emi = json.getString("hand_loan_emi_amt");
                            pic_count = json.getString("pic_cnt");
                            contacts_flag = json.getString("contacts_flag");
                            if (str_max_emi_amt_capble.equals("null") || str_max_emi_amt_capble.equals("0.0")) {
                                str_max_emi_amt_capble = "";
                            }

                            if (str_hand_loane_value.equals("null") || str_hand_loane_value.equals("0.0")) {
                                str_hand_loane_value = "";
                            }

                            if (str_loan_mnth_emi.equals("null") || str_loan_mnth_emi.equals("0.0")) {
                                str_loan_mnth_emi = "";
                            }

                        } else {
                            datafound = false;
                            str_customer_name = "";
                            str_Account_Status = "";
                            str_PDD_Status = "";
                        }
                    }

                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {

                if (jsonobject != null) {
                    String KEY_STATUS = "status";
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            edt_customer_name.setText(str_customer_name);
                            edt_Account_Status.setText(str_Account_Status);
                            if (str_PDD_Status.equals("Required")) {
                                edt_PDD_Status.setTextColor(Color.RED);
                            } else {
                                edt_PDD_Status.setText("");
                            }
                            simpleTextView.setText(str_text);
                            edt_PDD_Status.setText(str_PDD_Status);
                            txt_branch_name.setText(str_branch_name);
                            txt_branch_name.setEnabled(false);
                            edt_customer_name.setEnabled(false);
                            edt_Account_Status.setEnabled(false);
                            edt_PDD_Status.setEnabled(false);
                            txt_bom_bkt.setText(str_bom_bkt);
                            txt_bom_bkt.setEnabled(false);
                            txt_risk_bkt.setText(str_risk_bkt);
                            txt_risk_bkt.setEnabled(false);
                            txt_lpp_due.setText(str_lpp_due);
                            txt_lpp_due.setEnabled(false);
                            txt_cbc_due.setText(str_cbc_due);
                            txt_cbc_due.setEnabled(false);
                            edt_emi_amount_ser.setText(str_EMI_Amount_Rs_ser);
                            edt_emi_amount_ser.setEnabled(false);
                            edt_ACH_status.setText(str_ACH_status);
                            edt_virtual_acc.setText(str_virtual_acc_no);
                            edt_virtual_acc.setEnabled(false);
                            edt_ACH_status.setEnabled(false);
                            edt_ACH_status.setEnabled(false);

                            spn_Product_Type.setSelection(producttype_val.indexOf(str_product));
                            spn_Product_Type.setEnabled(false);

                            spn_BIZ_type_val.setSelection(getIndex(spn_BIZ_type_val, str_BIZ_type_val));
                            spn_PROPERTY_type_val.setSelection(getIndex(spn_PROPERTY_type_val, str_PROPERTY_type_val));

                            edt_max_emi_amt_capble.setText(str_max_emi_amt_capble);


                            spn_Current_BIZ_type_val.setSelection(getIndex(spn_Current_BIZ_type_val, str_Current_BIZ_type_val));
                            spn_Coll_Occupant_type_value.setSelection(getIndex(spn_Coll_Occupant_type_value, str_Coll_Occupant_type_value));
                            spn_cust_has_loan.setSelection(Arrays.asList(Form60_type_val).indexOf(str_cust_has_loan));
                            spn_Not_able_pay_type_val.setSelection(getIndex(spn_Not_able_pay_type_val, str_Not_able_pay_type_val));


                            edt_hand_loane_value.setText(str_hand_loane_value);

                            edt_loan_mnth_emi.setText(str_loan_mnth_emi);

                            if (contacts_flag.equals("1")) {
                                text_error_msg.setText("Fields marked by (*) are mandatory");

                            } else {
                                text_error_comntacted.setText("Customer is not contactable on phone, please capture new contact number of customer.");

                            }


                        } else {
                            if (datafound == false) {
                                edt_customer_name.setText("");
                                edt_Account_Status.setText("");
                                edt_PDD_Status.setText("");

                                txt_branch_name.setText("");
                                txt_bom_bkt.setText("");
                                txt_risk_bkt.setText("");
                                txt_lpp_due.setText("");
                                txt_cbc_due.setText("");
                                edt_emi_amount_ser.setText("");

                                edt_customer_name.setEnabled(true);
                                edt_Account_Status.setEnabled(true);
                                edt_PDD_Status.setEnabled(true);
                            }

                            userFunction.cutomToast(
                                    jsonobject.getString("message")
                                            .toString(),
                                    getApplicationContext());
                        }
                    }

                } else {
                    userFunction.cutomToast(
                            getResources().getString(com.example.hdb.R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
/*
           // --- old code
			if (jsonobject == null) {
				userFunction.cutomToast(
						getResources().getString(R.string.error_message),
						getApplicationContext());
			}else{

			if (datafound == false) {
				userFunction.cutomToast("LOSID not present", EditRequest.this);
				edt_customer_name.setText("");
				edt_Account_Status.setText("");
				edt_PDD_Status.setText("");

				str_customer_name = "";
				str_Account_Status = "";
				str_PDD_Status = "";

				edt_customer_name.setEnabled(true);
				edt_Account_Status.setEnabled(true);
				edt_PDD_Status.setEnabled(true);
			} else {

				edt_customer_name.setText(str_customer_name);
				edt_Account_Status.setText(str_Account_Status);
                if(str_PDD_Status.equals("Required")){
                    edt_PDD_Status.setTextColor(Color.RED);
                }else{
                    edt_PDD_Status.setText("");

                }

				edt_PDD_Status.setText(str_PDD_Status);
				txt_branch_name.setText(str_branch_name);
				edt_customer_name.setEnabled(false);
				edt_Account_Status.setEnabled(false);
				edt_PDD_Status.setEnabled(false);
				txt_bom_bkt.setText(str_bom_bkt);
				txt_risk_bkt.setText(str_risk_bkt);
				txt_lpp_due.setText(str_lpp_due);
				txt_cbc_due.setText(str_cbc_due);
                edt_emi_amount_ser.setText(str_EMI_Amount_Rs_ser);
                edt_emi_amount_ser.setEnabled(false);

                spn_Product_Type.setSelection(getIndex(spn_Product_Type, str_product));
				spn_Product_Type.setEnabled(false);


			}
			}
			*/

            if ((mProgressDialog != null) && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

        }

    }

    public void cleanUpCancel() {
        fileDelete(pData.getString("str_DOC_1", null));
        fileDelete(pData.getString("str_DOC_2", null));
        fileDelete(pData.getString("str_DOC_3", null));
        fileDelete(pData.getString("str_DOC_4", null));
        fileDelete(pData.getString("str_DOC_5", null));
        fileDelete(pData.getString("str_DOC_6", null));
        fileDelete(pData.getString("str_DOC_7", null));
        fileDelete(pData.getString("str_DOC_8", null));
        fileDelete(pData.getString("str_DOC_9", null));
        fileDelete(pData.getString("str_DOC_10", null));
    }

    public boolean fileDelete(String path) {
        boolean op = false;
        try {
            if (!path.equals("")) {
                File f = new File(path);
                if (f.exists()) {
                    if (f.delete()) {
                        op = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return op;
    }


    @SuppressWarnings("deprecation")
    public void setDate(View view) {

        showDialog(999);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        if (userFunction.dateCompare(day + "/" + month + "/" + year)) {
            edt_Next_Follow_Date.setText(new StringBuilder().append(year)
                    .append("-").append(month).append("-").append(day));
        } else {

            userFunction.cutomToast("Follow date can't be less than today",
                    getApplicationContext());
        }
    }


    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null,
                null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    //---- Function for calculating distance   ---------------------

    private double distance(double lat1, double lon1, double lat2, double lon2, String string) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (string == "K") {
            dist = dist * 1.609344;
        } else if (string == "N") {
            dist = dist * 0.8684;
        }
        return (dist);

    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
        /*::  This function converts decimal degrees to radians             :*/
        /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
        /*::  This function converts radians to decimal degrees             :*/
        /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                com.example.hdb.HdbHome.class);
        startActivity(home_activity);

    }


    private boolean CheckPanNO(String pan_no) {

        return PANNO_PATTERN.matcher(pan_no).matches();
    }

    public class checkcontactdetails extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
/*
            mProgressDialog = new ProgressDialog(EditRequest.this);
            mProgressDialog.setMessage("Uploading your data please wait...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            */
            str_losid_no = edt_losid_no.getText().toString();
            str_New_Contact_Number = edt_New_Contact_Number.getText().toString();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work


            // jsonobject = userFunction.checkContactdetails(str_losid_no, str_New_Contact_Number);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (str_New_Contact_Number.equals(str_contact_1) || str_New_Contact_Number.equals(str_contact_2)) {
                text_error_comntacted.setText("The number captured is not contactable , capture new number");
            } else
                text_error_msg.setText("Fields marked by (*) are mandatory");
/*
                if (str_New_Contact_Number.equals(str_contact_2)) {
                    text_error_msg.setText("The number captured is not contactable , capture new number");
                }
            text_error_msg.setText("The number captured is not contactable , capture new number");
*/
        }
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    public void insertDataSqlite() {
        long newRowId = 0;

        entry_status = "0";


        newRowId = rdb.insert(str_losid_no, str_customer_name,
                str_Account_Status, str_PDD_Status,
                str_Data_Entry_Type, str_visit_mode,
                str_Number_Contacted, str_bucket_type,
                str_met_spoke, str_dispoCode, str_Next_Follow_Date,
                str_Remarks_if_any, str_New_Contact_Number,
                str_New_Contact_Address, str_New_Contact_Email,
                str_Receipt_Number, str_Total_Amount_Rs,
                str_EMI_Amount_Rs, str_Other_Amount_Rs, str_Mode,
                str_Cheque_Num = null, str_PAN_no, str_formsixty = null,
                str_sighted, str_running_condition,
                str_third_Party, str_third_Party_Name,
                str_third_Party_Address, str_repo_doable, str_user_id, Upload_Date,
                lattitude, longitude, ts, str_disance,
                str_product, str_bom_bkt, str_risk_bkt, str_cbc_due, str_lpp_due,
                str_BIZ_type_val, str_PROPERTY_type_val, str_Current_BIZ_type_val,
                str_Not_able_pay_type_val, str_Coll_Occupant_type_value,
                str_cust_has_loan, str_max_emi_amt_capble, str_hand_loane_value,
                str_loan_mnth_emi, str_ssn, entry_status, str_ACH_status, str_txt_distacne, str_distance_type, map_address);


        if (newRowId > 0) {
/*
            if (int_dist_count > 0) {
a                rdb.delete_dist_All();
                rdb.insert_distance(lattitude, longitude, str_disance, current_date);


                //rdb.update_by_date(current_date,Double.toString(gps.getLatitude()),Double.toString(gps.getLongitude()),str_disance,current_date);
            } else {
                rdb.delete_dist_All();
                rdb.insert_distance(lattitude, longitude, str_disance, current_date);

            }
*/          // String img_new_path = pref.getString("map_img_name", null);

            String map_new_path = pref.getString("map_img_name", null);
            if (map_new_path != null && dbl_latitude > 0) {
              //  dbHelper.insertImage(map_new_path, losid, str_user_id, ts, "0", "MAP");
            }

            str_date = getDateTime1();
            map_captured = pref.getInt("is_map_captured", 1);
            String map_coll = Integer.toString(map_captured);
            Intent cust_img = new Intent(com.example.hdb.EditRequest.this, MainActivity_OLD.class);
            cust_img.putExtra("cus_losid", str_losid_no);
            cust_img.putExtra("cus_pic_count", pic_count);
            cust_img.putExtra("cus_userid", str_user_id);
            cust_img.putExtra("cus_ts", ts);
            cust_img.putExtra("visit_mode", vis_mode);
            cust_img.putExtra("coll_map", map_coll);
            //  cust_img.putExtra("cus_last", newRowId);
            cust_img.putExtra("id", "COLLX");
            cust_img.putExtra("str_date", str_date);
            cust_img.putExtra("lattitude", lattitude);
            cust_img.putExtra("longitude", longitude);
            startActivity(cust_img);
            finish();


            userFunction.resetData(com.example.hdb.EditRequest.this);

/*
            userFunction
                    .cutomToast(
                            "Data has been stored locally and will be uploaded in a short while",
                            EditRequest.this);*/
        } else {
            userFunction
                    .cutomToast(
                            "Something gets Wrong please try again",
                            com.example.hdb.EditRequest.this);
        }

    }

    private String getDateTime1() {
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String format = s.format(new Date());
        return format;
    }

    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        lon = location.getLongitude();
        if(lat!=null) {
            lattitude = Double.toString(lat);
            //  System.out.println("lat+++++++++titude"+lat);
            longitude = Double.toString(lon);
            //  System.out.println("lat+++++++++"+lon);
        }

        location_lat.setText(lattitude);
        location_long.setText(longitude);
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


}
