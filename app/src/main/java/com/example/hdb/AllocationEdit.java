package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Avinash on 02-07-2015
 */

public class AllocationEdit extends Activity {

    String[] month_e_status = {"Select", "RS",
            "RB",
            "SB",
            "FW"};

    String[] month_e_status_val = {"", "RS",
            "RB",
            "SB",
            "FW"};

    String[] month_status_week = {"Select", "W1",
            "W2",
            "W3",
            "W4",
            "W5"
    };

    String[] month_status_week_val = {"", "W1",
            "W2",
            "W3",
            "W4",
    "W5"};

    String str_resolve_flag = null;
    String str_resolve_week = null;

    String Str_resolve_flag_val = null;
    String Str_resolv_week_val = null;

    String str_user_id;
    SharedPreferences pref;
    ConnectionDetector cd;
    UserFunctions userFunction;

    TextView txt_error;


    Button btn_submit;
    JSONObject jsonobject = null;
    public static final String KEY_STATUS = "status";
    String str_msg_id;
    ProgressDialog mProgressDialog;
    String str_los_id;
    String cust_name;

    private RadioGroup radioGroup, radioGroup_status;
    private RadioButton week1;
    private RadioButton week2;
    private RadioButton week3;
    private RadioButton week4;
    private RadioButton week5;

    private RadioButton rbt_rs;
    private RadioButton rbt_rb;
    private RadioButton rbt_st;


    String Str_resolve_flag, Str_resolv_week;
    private Button button;

    TextView textView, txt_loan_no,txt_cust_name;
    TextView txt_resi_address,txt_resi_contact,txt_office_address,txt_office_contact;
    String str_resi_address=null,str_resi_contact=null,str_office_address=null,str_office_contact=null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.allocation_edit);



        Intent i = getIntent();
        str_resolve_flag = i.getStringExtra("me_status").trim();
        str_resolve_week = i.getStringExtra("me_week").trim();
        str_los_id = i.getStringExtra("los_id");
        str_user_id = i.getStringExtra("user_id");

        str_resi_address=i.getStringExtra("resi_address");
        str_resi_contact=i.getStringExtra("resi_contact");
        str_office_address=i.getStringExtra("ofc_address");
        str_office_contact=i.getStringExtra("ofc_contact");
        cust_name=i.getStringExtra("cust_name");
        // str_resolve_week="W1"; = i.getStringExtra("los_id");
        //str_resolve_flag="RS";

        if(str_user_id=="" || str_user_id== null) {
            pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
            str_user_id = pref.getString("user_id", null);
        }


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        setlayout();
    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        textView = (TextView) findViewById(R.id.btn_submit);
        radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);

        txt_resi_address =(TextView) findViewById(R.id.txt_resi_address);
        txt_resi_contact=(TextView) findViewById(R.id.txt_resi_contact);
        txt_office_address=(TextView) findViewById(R.id.txt_office_address);
        txt_office_contact=(TextView) findViewById(R.id.txt_office_contact);
        txt_loan_no=(TextView) findViewById(R.id.txt_loan_no);
        txt_cust_name=(TextView) findViewById(R.id.txt_customer_name);

        txt_resi_address.setText(str_resi_address);
        txt_resi_contact.setText(str_resi_contact);
        txt_office_address.setText(str_office_address);
        txt_office_contact.setText(str_office_contact);
        txt_loan_no.setText("Loan No - "+str_los_id);
        txt_cust_name.setText("Customer Name - "+cust_name);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.w1) {

                } else if (checkedId == R.id.w2) {

                } else if (checkedId == R.id.w3) {

                } else if (checkedId == R.id.w4) {

                }else if (checkedId == R.id.w5) {

                }

            }

        });

        radioGroup_status = (RadioGroup) findViewById(R.id.myStatus_RadioGroup);

        radioGroup_status.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected

                if (checkedId == R.id.rs) {

                    radioGroup.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.rb) {
                    radioGroup.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.st) {

                    radioGroup.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.fw){

                    radioGroup.setVisibility(View.GONE);

            }
            }

        });

        week1 = (RadioButton) findViewById(R.id.w1);
        week2 = (RadioButton) findViewById(R.id.w2);
        week3 = (RadioButton) findViewById(R.id.w3);
        week4 = (RadioButton) findViewById(R.id.w4);
        week5 = (RadioButton) findViewById(R.id.w5);


        rbt_rs = (RadioButton) findViewById(R.id.rs);
        rbt_rb = (RadioButton) findViewById(R.id.rb);
        rbt_st = (RadioButton) findViewById(R.id.st);
        final RadioButton rbt_fw = (RadioButton) findViewById(R.id.fw);


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();

                if (selectedId == week1.getId()) {
                    Str_resolv_week_val = "W1";
                } else if (selectedId == week2.getId()) {
                    Str_resolv_week_val = "W2";
                } else if (selectedId == week3.getId()) {
                    Str_resolv_week_val = "W3";
                }
                else if (selectedId == week4.getId()) {
                    Str_resolv_week_val = "W4";
                } else if (selectedId == week5.getId()){
                    Str_resolv_week_val = "W5";
                }

                int selected_status_Id = radioGroup_status.getCheckedRadioButtonId();

                if (selected_status_Id == rbt_rs.getId()) {
                    Str_resolve_flag_val = "RS";
                } else if (selected_status_Id == rbt_rb.getId()) {
                    Str_resolve_flag_val = "RB";
                } else if (selected_status_Id == rbt_st.getId()) {
                    Str_resolve_flag_val = "ST";
                } else if(selected_status_Id == rbt_fw.getId()){
                    Str_resolve_flag_val = "FW";
                    Str_resolv_week_val="";
                }

                if (cd.isConnectingToInternet()) {
                    new updateAllocation().execute();
                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);
                }

            }

        });


        if (str_resolve_flag.equals("ST")) {
            radioGroup_status.check(rbt_st.getId());
        }

        if (str_resolve_flag.equals("RS")) {
            radioGroup_status.check(rbt_rs.getId());
        }
        if (str_resolve_flag.equals("RB")) {
            radioGroup_status.check(rbt_rb.getId());
        }

        if (str_resolve_flag.equals("FW")) {
            radioGroup_status.check(rbt_fw.getId());
        }

        if (str_resolve_week.equals("W1")) {
            radioGroup.check(week1.getId());
        }

        if (str_resolve_week.equals("W2")) {
            radioGroup.check(week2.getId());
        }
        if (str_resolve_week.equals("W3")) {
            radioGroup.check(week3.getId());
        }

        if (str_resolve_week.equals("W4")) {
            radioGroup.check(week4.getId());
        }
        if (str_resolve_week.equals("W5")) {
            radioGroup.check(week5.getId());
        }
    }

    public class updateAllocation extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(com.example.hdb.AllocationEdit.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            if (Str_resolv_week_val != null && Str_resolve_flag_val != null) {
                jsonobject = userFunction.update_allocation(str_los_id, str_user_id, Str_resolve_flag_val, Str_resolv_week_val);
            }

            if (Str_resolve_flag_val != null && !Str_resolve_flag_val.isEmpty() && !Str_resolve_flag_val.equals("null")){

                if (Str_resolve_flag_val.equals("FW")) {
                    jsonobject = userFunction.update_allocation(str_los_id, str_user_id, Str_resolve_flag_val, Str_resolv_week_val);

                }
        }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {


                if(Str_resolve_flag_val!= null && !Str_resolve_flag_val.isEmpty() && !Str_resolve_flag_val.equals("null"))
                {
                    if (Str_resolve_flag_val.equals("FW")) {

                    }else{
                        if(Str_resolve_flag_val== null && Str_resolve_flag_val.isEmpty() && Str_resolve_flag_val.equals("null")) {
                            userFunction.cutomToast(" Select Collection Date Range...", getApplicationContext());
                        }
                    }

                }

                if (jsonobject != null) {
                    txt_error.setVisibility(View.GONE);

                    // Log.d("jsonobject OP : " , jsonobject.toString() );
                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            Intent messagingActivity = new Intent(com.example.hdb.AllocationEdit.this,
                                    AllocationList.class);
                            startActivity(messagingActivity);
                            finish();
                        } else {
                            String
                                    error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg,
                                    com.example.hdb.AllocationEdit.this);

                        }
                    }
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        home_activity.putExtra("los_alloc",str_los_id);

        startActivity(home_activity);

    }
}
