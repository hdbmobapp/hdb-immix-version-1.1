package com.example.hdb.My_PickupType;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hdb.R;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;


public class schedule_pick_adapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;

    UserFunctions user_function;

    public schedule_pick_adapter(Context context,
                                 ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.act_schedule_item, parent, false);
        // Get the position
        resultp = data.get(position);
        TextView  txt_loan =  itemView.findViewById(R.id.txt_loan);
        TextView  txt_cust_name =  itemView.findViewById(R.id.txt_cust_name);
        TextView txt_mode =  itemView.findViewById(R.id.txt_mode);
        TextView txt_amt =  itemView.findViewById(R.id.txt_amt);
        TextView  txt_time =  itemView.findViewById(R.id.txt_time);
        TextView  txt_mob =  itemView.findViewById(R.id.txt_mob);
        TextView  txt_status =  itemView.findViewById(R.id.txt_status);
        TextView  txt_rmks =  itemView.findViewById(R.id.txt_rmks);
        TextView  txt_ptp_type =  itemView.findViewById(R.id.txt_ptp_type);
        TextView  txt_ptp_date =  itemView.findViewById(R.id.txt_ptp_date);
        TextView  txt_add =  itemView.findViewById(R.id.txt_add);
        TextView  txt_pincode =  itemView.findViewById(R.id.txt_pincode);

        RelativeLayout rel_status =  itemView.findViewById(R.id.rel_status);
        RelativeLayout  rel_rmk =  itemView.findViewById(R.id.rel_rmk);

       //Fragment1 feilds
        txt_loan.setText(resultp.get(pickup_type_frag1.KEY_LOAN_NO));
        txt_cust_name.setText(resultp.get(pickup_type_frag1.KEY_CUST_NAME));
        txt_mode.setText(resultp.get(pickup_type_frag1.KEY_MODE));
        txt_amt.setText(resultp.get(pickup_type_frag1.KEY_AMT));
        txt_time.setText(resultp.get(pickup_type_frag1.KEY_TIME));
        txt_mob.setText(resultp.get(pickup_type_frag1.KEY_MOBILE));
        txt_ptp_type.setText(resultp.get(pickup_type_frag1.KEY_PTPTYPE));
        txt_ptp_date.setText(resultp.get(pickup_type_frag1.KEY_PTPDATE));
        txt_add.setText(resultp.get(pickup_type_frag1.KEY_PTPADD));
        txt_pincode.setText(resultp.get(pickup_type_frag1.KEY_PIN));


        //Fragment2 feilds
        txt_status.setText(resultp.get(pickup_type_frag2.KEY_STATUS_PICKUP));
        txt_rmks.setText(resultp.get(pickup_type_frag2.KEY_REMARKS));

        String Status_pickup =resultp.get(pickup_type_frag2.KEY_STATUS_PICKUP);
        String Remarks =resultp.get(pickup_type_frag2.KEY_REMARKS);


        if( Status_pickup!= null){

            System.out.println("+++++++++++++"+Status_pickup);
            rel_status.setVisibility(View.VISIBLE);
            rel_rmk.setVisibility(View.VISIBLE);
         }else{
            System.out.println("-----------"+Status_pickup);
            rel_status.setVisibility(View.GONE);
            rel_rmk.setVisibility(View.GONE);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    arg0.setSelected(true);

                    new Thread(new Runnable() {
                        public void run() {
                            // Get the position
                            //	rel_lay_msg.setBackgroundColor(context.getResources().getColor(R.color.default_screen_bg));

                            resultp = data.get(position);
                            Intent p = new Intent(context, pickup_submit.class);
                             p.putExtra("key_loan", resultp.get(pickup_type_frag1.KEY_LOAN_NO));
                             p.putExtra("key_cust_name", resultp.get(pickup_type_frag1.KEY_CUST_NAME));
                             p.putExtra("key_mode", resultp.get(pickup_type_frag1.KEY_MODE));
                             p.putExtra("key_amt", resultp.get(pickup_type_frag1.KEY_AMT));
                             p.putExtra("key_time", resultp.get(pickup_type_frag1.KEY_TIME));
                             p.putExtra("key_mob", resultp.get(pickup_type_frag1.KEY_MOBILE));
                             p.putExtra("key_product", resultp.get(pickup_type_frag1.KEY_PRODUCT));
                             p.putExtra("key_ptptype", resultp.get(pickup_type_frag1.KEY_PTPTYPE));
                            p.putExtra("key_ptpdate", resultp.get(pickup_type_frag1.KEY_PTPDATE));
                            p.putExtra("key_id", resultp.get(pickup_type_frag1.KEY_ID));
                            context.startActivity(p);

                        }
                    }).start();
                }

            });
        }

        userFunction = new UserFunctions();



        return itemView;
    }
}
