package com.example.hdb.My_PickupType;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hdb.R;

import info.hdb.libraries.ConnectionDetector;

public class Non_Assign_frag1 extends Fragment {



    SharedPreferences pref;
    String str_user_id,str_user_name;
    private ProgressBar progress_bar;

    WebView webView;
    ConnectionDetector cd;


    View rootView;

    private Handler handler = new Handler(){
        public void handleMessage(Message message){
            switch (message.what){
                case 1:{
                    webViewGoBack();
                }
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.act_assign_pickup, container, false);

        cd = new ConnectionDetector(getActivity());

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);
        System.out.println("str_user_id"+str_user_id+"str_user_name"+str_user_name);


        // mProgressBar = (ProgressBar)rootView.findViewById(R.id.pb);
        progress_bar = rootView.findViewById(R.id.progress_bar);
        webView = rootView.findViewById(R.id.webview);

        //
        webView.getSettings().setJavaScriptEnabled(true);
        // webView.getSettings().sets(true);
        String url="https://hdbapp.hdbfs.com/Web_Pages/Collx_App/agency_pickup_assign.php?vendor_code="+str_user_id+"&pickup_type=non_assign";
        if (cd.isConnectingToInternet()) {
            renderWebPage(url);
            // webView.loadUrl(url);

            webView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(keyCode== KeyEvent.KEYCODE_BACK && event.getAction()== MotionEvent.ACTION_UP && webView.canGoBack()){
                        handler.sendEmptyMessage(1);
                        return true;
                    }
                    return false;
                }
            });
        }else
        {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

        }

        return rootView;
    }

    private void webViewGoBack() {
        webView.goBack();
    }


    // Custom method to render a web page
    protected void renderWebPage(String urlToRender){
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                // Do something on page loading started
                // Visible the progressbar
                // mProgressBar.setVisibility(View.VISIBLE);
                progress_bar.setVisibility(View.VISIBLE);


            }

            @Override
            public void onPageFinished(WebView view, String url) {
                // Do something when page loading finished
                //Toast.makeText(getActivity(), "Page Loaded.", Toast.LENGTH_SHORT).show();
            }

        });

        /*
            WebView
                A View that displays web pages. This class is the basis upon which you can roll your
                own web browser or simply display some online content within your Activity. It uses
                the WebKit rendering engine to display web pages and includes methods to navigate
                forward and backward through a history, zoom in and out, perform text searches and more.

            WebChromeClient
                 WebChromeClient is called when something that might impact a browser UI happens,
                 for instance, progress updates and JavaScript alerts are sent here.
        */
        webView.setWebChromeClient(new WebChromeClient() {
            /*
                public void onProgressChanged (WebView view, int newProgress)
                    Tell the host application the current progress of loading a page.

                Parameters
                    view : The WebView that initiated the callback.
                    newProgress : Current page loading progress, represented by an integer
                        between 0 and 100.
            */
            public void onProgressChanged(WebView view, int newProgress) {
                // Update the progress bar with page loading progress
                //  mProgressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    // Hide the progressbar
                    //  mProgressBar.setVisibility(View.GONE);
                    progress_bar.setVisibility(View.GONE);
                }
            }
        });

        // Enable the javascript
        webView.getSettings().setJavaScriptEnabled(true);
        // Render the web page
        webView.loadUrl(urlToRender);
    }

}




