package com.example.hdb.My_PickupType;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hdb.Ercpt_activity_main;
import com.example.hdb.HdbHome;
import com.example.hdb.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.MyAdapter;
import info.hdb.libraries.UserFunctions;

public class pickup_submit extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {


    private static final String TAG = "pickup_submit";


    EditText  edt_loan,edt_cust_name,edt_product,edt_ptptype,edt_mode,edt_amt,edt_time,edt_mob,edt_remarks,edt_ptpdate;
    Button btn_submit;
    Spinner spn_status;

    ProgressDialog mProgressDialog;
    ProgressBar pb_progress;
    SharedPreferences pref;
    String str_user_id = null;
    String str_status,str_loan_no,str_cust_name,str_product,str_ptptype,str_mode,str_amt,str_time,str_mob,str_rmk,str_ptpdate,str_id;
    public int pgno = 0;

    static ArrayList<String> pickup_status_arraylist;

    String KEY_STATUS = "status";
    UserFunctions userFunction;
    JSONObject jsonobject = null;
    private ConnectionDetector cd;


    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;
    static Double lat;
    static Double lon;
    String str_lat,str_long;

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pickup_submit);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);

        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        pickup_status_arraylist = new ArrayList<String>();

        Intent i = getIntent();
        str_loan_no = i.getStringExtra("key_loan");
        str_cust_name = i.getStringExtra("key_cust_name");
        str_mode = i.getStringExtra("key_mode");
        str_amt = i.getStringExtra("key_amt");
        str_time = i.getStringExtra("key_time");
        str_mob = i.getStringExtra("key_mob");
        str_product = i.getStringExtra("key_product");
        str_ptptype = i.getStringExtra("key_ptptype");
        str_ptpdate = i.getStringExtra("key_ptpdate");
        str_id = i.getStringExtra("key_id");
        System.out.println("link:::::::::::::::++++++::::::::"+str_loan_no);


        spn_status= findViewById(R.id.spn_status);
        edt_loan= findViewById(R.id.edt_loan);
        edt_loan.setEnabled(false);
        edt_loan.setText(str_loan_no);

        edt_cust_name= findViewById(R.id.edt_cust_name);
        edt_cust_name.setEnabled(false);
        edt_cust_name.setText(str_cust_name);

        edt_product= findViewById(R.id.edt_product);
        edt_product.setEnabled(false);
        edt_product.setText(str_product);


        edt_ptptype= findViewById(R.id.edt_ptptype);
        edt_ptptype.setEnabled(false);
        edt_ptptype.setText(str_ptptype);


        edt_mode= findViewById(R.id.edt_mode);
        edt_mode.setEnabled(false);
        edt_mode.setText(str_mode);

        edt_amt= findViewById(R.id.edt_amt);
        edt_amt.setEnabled(false);
        edt_amt.setText(str_amt);

        edt_time= findViewById(R.id.edt_time);
        edt_time.setEnabled(false);
        edt_time.setText(str_time);



        edt_ptpdate= findViewById(R.id.edt_date);
        edt_ptpdate.setEnabled(false);
        edt_ptpdate.setText(str_ptpdate);

        edt_mob= findViewById(R.id.edt_mob);
        edt_mob.setEnabled(false);
        edt_mob.setText(str_mob);

        edt_remarks= findViewById(R.id.edt_remarks);
        btn_submit= findViewById(R.id.btn_submit);
        pb_progress = findViewById(R.id.progress_bar);

        if (cd.isConnectingToInternet()) {
            new load_pickup_SpinnerData().execute();
         } else {
        userFunction.cutomToast("No internet Connection  ", getApplicationContext());
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks)pickup_submit.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        checkLocation();



        spn_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spn_status.getSelectedItemPosition() == 0) {
                    str_status = null;
                } else {
                    str_status = spn_status.getItemAtPosition(spn_status.getSelectedItemPosition()).toString();

                    //Toast.makeText(getContext(), str_pay_mode, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                checkLocation();

                str_rmk = edt_remarks.getText().toString();
                str_loan_no = edt_loan.getText().toString();
                str_cust_name = edt_cust_name.getText().toString();
                str_amt = edt_amt.getText().toString();
                str_ptptype = edt_ptptype.getText().toString();
                str_mob = edt_mob.getText().toString();
                str_mode = edt_mode.getText().toString();
                str_product = edt_product.getText().toString();
                str_time = edt_time.getText().toString();

                Boolean ins = true;

                if (str_rmk.equals("") && ins == true) {
                    ins = false;
                    Toast.makeText(getApplicationContext(), "Please enter Remarks", Toast.LENGTH_SHORT).show();
                }

                if(str_status == null && ins == true){
                    Toast.makeText(getApplicationContext(), "Please Select Status ", Toast.LENGTH_SHORT).show();
                    ins = false;
                }

                if(lat!=null){
                    System.out.println("vghvvjhvhjhjhjhj"+str_lat+"sacafafaf"+str_long);
                    str_lat = Double.toString(lat);
                    str_long =  Double.toString(lon);

                }



                     if(ins==true) {
                            if (cd.isConnectingToInternet()) {
                                System.out.println("vghvvjhvhjhjhjhj"+str_lat+"sacafafaf"+str_long);
                              new submit_pickup_data().execute();
                              } else {
                                userFunction.cutomToast("No internet Connection  ", getApplicationContext());
                              }
                     }

            }
        });

    }




    public class load_pickup_SpinnerData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            mProgressDialog = new ProgressDialog(pickup_submit.this);
            mProgressDialog.setMessage("Please Wait.....");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            jsonobject = userFunction.get_pickup_status();
            System.out.println("jsonobject::::::::::::::::::::" + jsonobject);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {


                    JSONObject json_rem = jsonobject.getJSONObject("pickup_status");
                    JSONArray jsonarray_rem = json_rem.getJSONArray("PickupStatus");
                    pickup_status_arraylist.add("Select Pickup Status");
                    for (int i = 0; i < jsonarray_rem.length(); i++) {
                        json_rem = jsonarray_rem.getJSONObject(i);
                        String status = json_rem.getString("value");
                        pickup_status_arraylist.add(status);
                        //System.out.println("remark_reason_arraylist::::::::::::::::::::" + pickup_status_arraylist);

                    }

                    ArrayAdapter<String> spinnerArrayAdapteterpaymode = new MyAdapter(pickup_submit.this, R.layout.sp_display_layout, R.id.txt_visit, pickup_status_arraylist);
                    spinnerArrayAdapteterpaymode.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
                    spn_status.setAdapter(spinnerArrayAdapteterpaymode);



                } else {

                    userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());

                    Intent messagingActivity = new Intent(getApplicationContext(), HdbHome.class);
                    startActivity(messagingActivity);
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }


    }


    public class submit_pickup_data extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btn_submit.setEnabled(false);
            pb_progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {


            // then do your work

            jsonobject = userFunction.insertpickupdata(str_status,str_rmk,str_lat,str_long,str_id,str_user_id);



            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        String message=jsonobject.getString("message");

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                            userFunction.cutomToast(message, getApplicationContext());

                            if(str_status.contains("PAYMENT COLLECTED")){
                                Intent i = new Intent(getApplicationContext(), Ercpt_activity_main.class);
                                i.putExtra("loan_no",str_loan_no);
                                i.putExtra("from","pickup");
                                i.putExtra("str_id",str_id);
                                startActivity(i);
                            }else {
                                Intent i = new Intent(getApplicationContext(), HdbHome.class);
                                startActivity(i);
                            }

                        } else {
                            btn_submit.setEnabled(false);

                            String   error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg, getApplicationContext());

                        }
                    }

                }else{
                    btn_submit.setEnabled(false);

                }
                pb_progress.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            pb_progress.setVisibility(View.GONE);

        }
    }




    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            //  Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setInterval(UPDATE_INTERVAL).setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " + Double.toString(location.getLatitude()) + "," + Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat = location.getLatitude();
       // System.out.println("lat::::mmmmmmmmmmm:::::::::::ppppppp"+lat);
        lon = location.getLongitude();

        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled()) showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location").setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app").setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(), HdbHome.class);
        startActivity(home_activity);
        finish();
    }


}
