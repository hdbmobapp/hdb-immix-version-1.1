package com.example.hdb.My_PickupType;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class pickup_type_frag2 extends Fragment {



    UserFunctions userFunction;
    JSONObject jsonobject = null;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;

    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";

    static String KEY_LOAN_NO = "str_loan_no";
    static String KEY_CUST_NAME = "str_cust_name";
    static String KEY_MODE = "str_mode";
    static String KEY_TIME = "str_time";
    static String KEY_MOBILE= "str_mob_no";
    static String KEY_AMT= "str_amount";
    static String KEY_STATUS_PICKUP = "Status_pickup";
    static String KEY_REMARKS = "Remarks";
    static String KEY_PTPTYPE= "str_ptptype";
    static String KEY_PTPDATE= "str_ptpdate";
    static String KEY_ID= "str_id";


    TextView txt_error;
    schedule_pick_adapter adapter;
    ProgressDialog mProgressDialog;
    SharedPreferences pref;
    String str_user_id = null;
    public int pgno = 0;
    View rootView;

    String str_schedule="Closed";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.act_schedule_picktype, container, false);

        pref = getActivity().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        System.out.println("fghfghffgj"+str_user_id);

        cd = new ConnectionDetector(getContext());
        userFunction = new UserFunctions();
        setlayout();

        return rootView;
    }


    public void setlayout() {
        txt_error = (TextView)rootView.findViewById(R.id.text_error_msg);

        list = (ListView)rootView.findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();
        if (cd.isConnectingToInternet()) {
            new Download_pickups().execute();
        } else {

            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);


        }
    }



    public class Download_pickups extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            jsonobject = userFunction.get_pickup_data(str_user_id,str_schedule);
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        String message=jsonobject.getString("message");

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject json_rem = jsonobject.getJSONObject("data_pickup");
                            jsonarray = json_rem.getJSONArray("DataPickup");

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);

                                // Retrive JSON Objects
                                map.put("str_loan_no", jsonobject.getString("Loan_no"));
                                map.put("str_cust_name", jsonobject.getString("Cust_name"));
                                map.put("str_mode", jsonobject.getString("Mode"));
                                map.put("str_amount", jsonobject.getString("Amount"));
                                map.put("str_time", jsonobject.getString("Time"));
                                map.put("str_mob_no", jsonobject.getString("Mobile"));
                                map.put("str_product", jsonobject.getString("Product"));
                                map.put("str_ptptype", jsonobject.getString("PTPType"));
                                map.put("str_ptpdate", jsonobject.getString("PTP_DATE"));
                                map.put("str_id", jsonobject.getString("id"));


                                map.put("Status_pickup", jsonobject.getString("Status"));
                                map.put("Remarks", jsonobject.getString("Remarks"));



                                // Set the JSON Objects into the array
                                arraylist.add(map);
                            }

                            adapter = new schedule_pick_adapter(getActivity(), arraylist);
                            list.setAdapter(adapter);
                            mProgressDialog.dismiss();



                        } else {
                            String   error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg, getContext());

                        }
                    }

                }else{

                    userFunction.cutomToast("Server Connection Timeout..Please Try Later", getContext());
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }
    }


}




