package com.example.hdb.My_PickupType.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hdb.My_PickupType.Assign_frag2;
import com.example.hdb.My_PickupType.Non_Assign_frag1;
import com.example.hdb.My_PickupType.pickup_type_frag1;
import com.example.hdb.My_PickupType.pickup_type_frag2;
import com.example.hdb.My_PickupType.pickup_type_main;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        if(pickup_type_main.str_pickup_type.contains("Schedule")) {
        switch (index) {
            case 0:
                return new pickup_type_frag1();
            case 1:
                return new pickup_type_frag2();
        }
        }else{
            switch (index) {
                case 0:
                    return new Non_Assign_frag1();
                case 1:
                    return new Assign_frag2();
            }

    }
        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }

}
