package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 01-10-2015.
 */

public class FAQ extends Activity {
    ListView mList;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;
    FAQListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    TextView txt_error;
    ProgressBar progressbar_loading;

    static String FAQ_question = "FAQ_quest";
    static String FAQ_answer = "FAQ_ans";
    static String FAQ_ID = "id";

    EditText edt_faq_search;
    Button btn_search;
    int int_from_act;
    String str_keyword;
    public int pgno = 0;

    private MediaPlayer mediaPlayer;

    @Override
    public void onBackPressed()
    {
        System.out.println("BACK PRESS:::");
        if(  mediaPlayer.isPlaying()){

            mediaPlayer.stop();
             finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq);
        userFunction = new UserFunctions();
        mediaPlayer = new MediaPlayer();

        cd = new ConnectionDetector(com.example.hdb.FAQ.this);
        set_layout();

        if (cd.isConnectingToInternet()) {
            new DownloadFAQ().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }






        edt_faq_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }
                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    if(str_keyword.length()>3) {
                        new DownloadFAQ().execute();
                    }                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }
            }

            @Override
            protected Object clone() throws CloneNotSupportedException {
                return super.clone();
            }
        });
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_keyword=edt_faq_search.getText().toString();
                if (cd.isConnectingToInternet()) {
                    new DownloadFAQ().execute();
                } else {
                    txt_error.setText(getResources().getString(R.string.no_internet));
                    txt_error.setVisibility(View.VISIBLE);

                }

            }
        });
    }

    public void set_layout() {
        Intent i = getIntent();
        int_from_act = i.getIntExtra("int_sreen_val", 0);

        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_faq_search = (EditText) findViewById(R.id.edt_faq_search);
        btn_search = (Button) findViewById(R.id.btn_search);
        mList = (ListView) findViewById(R.id.list);
        progressbar_loading=(ProgressBar)findViewById(R.id.progressbar_loading);

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }


    public class DownloadFAQ extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            pgno = 0;
            progressbar_loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            if (int_from_act == 1) {
                jsonobject = userFunction.getFAQList("1",str_keyword,"0");
            } else {
                jsonobject = userFunction.getFAQList("2",str_keyword,"0");
            }
            // Create an array
            if (jsonobject != null)
                try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");

                arraylist = new ArrayList<>();

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("FAQ_quest", jsonobject.getString("FAQ_quest"));
                    map.put("FAQ_ans", jsonobject.getString("FAQ_ans"));
                    map.put("id", jsonobject.getString("id"));

                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                adapter = new FAQListAdapter(com.example.hdb.FAQ.this, arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);
                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });
            }
            progressbar_loading.setVisibility(View.GONE);

        }

    }

    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            if (int_from_act == 1) {
                jsonobject = userFunction.getFAQList("1",str_keyword,tmpgno);
            } else {
                jsonobject = userFunction.getFAQList("2",str_keyword,tmpgno);
            }
            // Create an array
            try {
                if (jsonobject != null)
                        // Locate the array name in JSON
                        jsonarray = jsonobject.getJSONArray("Data");

                        arraylist = new ArrayList<>();

                        for (int i = 0; i < jsonarray.length(); i++) {
                            HashMap<String, String> map = new HashMap<>();
                            jsonobject = jsonarray.getJSONObject(i);
                            map.put("FAQ_quest", jsonobject.getString("FAQ_quest"));
                            map.put("FAQ_ans", jsonobject.getString("FAQ_ans"));
                            map.put("id", jsonobject.getString("id"));

                            // Set the JSON Objects into the array
                            arraylist.add(map);
                        }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }


}
