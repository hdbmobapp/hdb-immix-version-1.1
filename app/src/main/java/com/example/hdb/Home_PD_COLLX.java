package com.example.hdb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hmapp.AppMenu.SplashScreen;

import java.util.ArrayList;

import info.hdb.libraries.UserFunctions;

public class Home_PD_COLLX extends AppCompatActivity {
    SharedPreferences pref;
    UserFunctions userfunction;
    public static ArrayList<String> arraylist;
    TextView txt_error_msg;

    String collx_app, pd_com_app, pd_len_app, dms_app, los_qde_app, dms_imageview_app;


    String[] app = {"LOS QDE", "PD Commercial", "PD Lending", "DMS App", "Collection", "DMS Image View", "Additional menus"

    };
    Integer[] imageId = {R.drawable.qde_app, R.drawable.pd_collateal, R.drawable.pd_lending, R.drawable.dms_upload, R.drawable.collection, R.drawable.dms_view, R.drawable.additional};


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        arraylist = new ArrayList<String>();

        txt_error_msg = findViewById(R.id.txt_error_msg);

        userfunction = new UserFunctions();

        System.out.println("INSIDE:::::HDB_PD_COLLX");


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        collx_app = pref.getString("collx_app", null);
        pd_com_app = pref.getString("pd_com_app", null);
        pd_len_app = pref.getString("pd_lend_app", null);
        dms_app = pref.getString("dms_app", null);
        los_qde_app = pref.getString("los_qde_app", null);
        dms_imageview_app = pref.getString("dms_imageview_app", null);

        final home_pd_collx_adapter adapter = new home_pd_collx_adapter(Home_PD_COLLX.this, app, imageId);

        txt_error_msg.setText("");
        GridView grid = findViewById(R.id.grid);
        grid.setAdapter(adapter);


     /*   arraylist.add("LOS QDE");
        arraylist.add("PD Commercial");
        arraylist.add("PD Lending");
        arraylist.add("DMS App");
        arraylist.add("Collection");
        arraylist.add("DMS Image View");
        arraylist.add("Additional menus");*/


        // final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.home_pd_collx_itemlist, R.id.txt, arraylist);
        // grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String value = adapter.getItem(i);


                switch (value) {
                    case "LOS QDE":
                        if (los_qde_app.contains("1")) {
                            Intent Homeact_qde = new Intent(Home_PD_COLLX.this, com.example.hdb_los_prod.SplashScreen.class);
                            startActivity(Homeact_qde);
                        } else {
                            txt_error_msg.setText("LOS QDE App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(Home_PD_COLLX.this, "LOS QDE App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();

                        }


                        break;

                    case "PD Commercial":
                        if (pd_com_app.contains("1")) {
                            Intent Homeact_pd_com = new Intent(Home_PD_COLLX.this, SplashScreen.class);
                            startActivity(Homeact_pd_com);
                        } else {
                            txt_error_msg.setText("PD Commercial App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(Home_PD_COLLX.this, "PD Commercial App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();

                        }


                        break;
                    case "PD Lending":
                        if (pd_len_app.contains("1")) {
                            Intent Homeact_pd_len = new Intent(Home_PD_COLLX.this, com.example.vijay.finone.SplashScreen.class);
                            startActivity(Homeact_pd_len);
                        } else {
                            txt_error_msg.setText("PD Lending App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(Home_PD_COLLX.this, "PD Lending App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();

                        }

                        break;

                    case "DMS App":
                        if (dms_app.contains("1")) {
                            Intent Homeact_dms = new Intent(Home_PD_COLLX.this, com.example.losimg.MainModule.SplashScreen.class);
                            startActivity(Homeact_dms);
                        } else {
                            txt_error_msg.setText("DMS App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(Home_PD_COLLX.this, "DMS  App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();

                        }

                        break;

                    case "Collection":
                        if (collx_app.contains("1")) {
                            Intent Homeact_collx = new Intent(Home_PD_COLLX.this, HdbHome.class);
                            startActivity(Homeact_collx);
                        } else {
                            txt_error_msg.setText("Collection App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(getApplicationContext(), "Collection App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();

                        }


                        break;

                    case "DMS Image View":
                        if (dms_imageview_app.contains("1")) {
                            Intent Homeact_dms_imgview = new Intent(Home_PD_COLLX.this, com.example.imagegrid_module.SplashScreen.class);
                            startActivity(Homeact_dms_imgview);
                        } else {
                            txt_error_msg.setText("DMS ImageView App Is Inactive for you, please check Additional menus for the more details.");
                            Toast.makeText(getApplicationContext(), "DMS ImageView App Is Inactive for you, please check Additional menus for the more details.", Toast.LENGTH_LONG).show();
                        }

                        break;

                    case "Additional menus":
                        Intent Homeact = new Intent(Home_PD_COLLX.this, Addition_menus.class);
                        startActivity(Homeact);
                        break;

                }
            }
        });



     /*   LinearLayout lt_collx_app=(LinearLayout)findViewById(R.id.lin_pd_collx_app);
          lt_collx_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, HdbHome.class);
                startActivity(Homeact);
            }
        });

        LinearLayout lin_pd_com_app=(LinearLayout)findViewById(R.id.lin_pd_com_app);
        lin_pd_com_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, SplashScreen.class);
                startActivity(Homeact);
            }
        });


        LinearLayout lin_pd_lend_app=(LinearLayout)findViewById(R.id.lin_pd_lend_app);
        lin_pd_lend_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, com.example.vijay.finone.SplashScreen.class);
                startActivity(Homeact);
            }
        });


        LinearLayout lin_dms_app=(LinearLayout)findViewById(R.id.lin_dms_app);
        lin_dms_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, com.example.losimg.MainModule.SplashScreen.class);
                startActivity(Homeact);
            }
        });



        LinearLayout lin_test=(LinearLayout)findViewById(R.id.lin_test);
        lin_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, Addition_menus.class);
                startActivity(Homeact);
            }
        });

        LinearLayout lin_qde=(LinearLayout)findViewById(R.id.lin_qde);
        lin_qde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Homeact = new Intent(Home_PD_COLLX.this, com.example.hdb_los_prod.SplashScreen.class);
                startActivity(Homeact);
            }
        });
*/


    }


    public void total_logout(View v) {
        // do stuff
        System.out.println(":::::::logouttt:::");
        alertForlogout();

    }

    public void alertForlogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Home_PD_COLLX.this.finish();
                @SuppressWarnings("unused") boolean islogoutBoolean = true;


                SharedPreferences.Editor editor = pref.edit();

                editor.putInt("off_is_login", 0);
                editor.commit();

                islogoutBoolean = userfunction.logoutUser(getApplicationContext());

                               /* off_islogoutBoolean = userfunction
                                        .off_logoutUserclear(getApplicationContext());*/

                if (islogoutBoolean == true) {
                    Intent i = new Intent(Home_PD_COLLX.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }


    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_LONG).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }
}
