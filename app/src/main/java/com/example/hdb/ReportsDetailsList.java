package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class ReportsDetailsList extends Activity {
	UserFunctions userFunction;
	JSONObject jsonobject;
	private ConnectionDetector cd;
	ListView list;
	ArrayList<HashMap<String, String>> arraylist;
	JSONArray jsonarray = null;
	TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7,txt8,txt9,txt10;
	String str_txt1, str_txt2, str_txt3, str_txt4, str_txt5, str_txt6,
			str_txt7,str_txt8,str_txt9,str_txt10;

	static String GRID_REPORT1 = "report1";
	static String GRID_REPORT2 = "report2";
	static String GRID_REPORT3 = "report3";
	static String GRID_REPORT4 = "report4";
	static String GRID_REPORT5 = "report5";
	static String GRID_REPORT6 = "report6";
	static String GRID_REPORT7 = "report7";
	static String GRID_REPORT8 = "report8";
	static String GRID_REPORT9 = "report9";
	static String GRID_REPORT10 = "report10";

	String report_no;

	ReportsDetailListAdapter adapter;
	ProgressDialog mProgressDialog;
	TextView txt_error;
	SharedPreferences pref;
	String str_user_id=null;
	String str_table_name=null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.detail_list);

		Intent i = getIntent();
		report_no = i.getStringExtra("report_no");
		str_table_name=i.getStringExtra("table_name");
		
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		str_user_id = pref.getString("user_id", null);

		
		System.out.println("report_no::::" + report_no);
		cd = new ConnectionDetector(getApplicationContext());
		userFunction = new UserFunctions();
		userFunction.checkforLogout(pref, com.example.hdb.ReportsDetailsList.this);
		
		setlayout();
	}

	public void setlayout() {
		txt_error=(TextView)findViewById(R.id.text_error_msg);
		list = (ListView) findViewById(R.id.list);
		
		txt1 = (TextView) findViewById(R.id.txtno_1);
		txt2 = (TextView) findViewById(R.id.txt_2);
		txt3 = (TextView) findViewById(R.id.txt_3);
		txt4 = (TextView) findViewById(R.id.txt_4);
		txt5 = (TextView) findViewById(R.id.txt_5);
		txt6 = (TextView) findViewById(R.id.txt_6);
		txt7 = (TextView) findViewById(R.id.txt_7);
		txt8 = (TextView) findViewById(R.id.txt_8);
		txt9 = (TextView) findViewById(R.id.txt_9);
		txt10 = (TextView) findViewById(R.id.txt_10);

		arraylist = new ArrayList<HashMap<String, String>>();
		if (cd.isConnectingToInternet()) {
			new DownloadGrid().execute();
		} else {
			// toast for internet conncection
			txt_error.setText(getResources().getString(R.string.no_internet));
			txt_error.setVisibility(View.VISIBLE);
		}

	}

	public class DownloadGrid extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(com.example.hdb.ReportsDetailsList.this);
			mProgressDialog.setMessage(getString(R.string.plz_wait));
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
			txt_error.setVisibility(View.GONE);

		}

		@Override
		protected Void doInBackground(Void... params) {

			jsonobject = userFunction.getReportsGrid(report_no,str_user_id,str_table_name);
			if(jsonobject!=null){


			// Create an array
			try {
				// Locate the array name in JSON
				jsonarray = jsonobject.getJSONArray("Report");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();

					jsonobject = jsonarray.getJSONObject(i);

					map.put("report1", jsonobject.getString("report1"));
					str_txt1 = jsonobject.getString("report1_header");
					map.put("report2", jsonobject.getString("report2"));
					str_txt2 = jsonobject.getString("report2_header");

					map.put("report3", jsonobject.getString("report3"));
					str_txt3 = jsonobject.getString("report3_header");

					map.put("report4", jsonobject.getString("report4"));
					str_txt4 = jsonobject.getString("report4_header");

					map.put("report5", jsonobject.getString("report5"));
					str_txt5 = jsonobject.getString("report5_header");

					map.put("report6", jsonobject.getString("report6"));
					str_txt6 = jsonobject.getString("report6_header");

					map.put("report7", jsonobject.getString("report7"));
					str_txt7 = jsonobject.getString("report7_header");

					map.put("report7", jsonobject.getString("report7"));
					str_txt7 = jsonobject.getString("report7_header");
					
					map.put("report8", jsonobject.getString("report8"));
					str_txt8 = jsonobject.getString("report8_header");
					
					
					map.put("report9", jsonobject.getString("report9"));
					str_txt9 = jsonobject.getString("report9_header");
					
					map.put("report10", jsonobject.getString("report10"));
					str_txt10 = jsonobject.getString("report10_header");
					// Set the JSON Objects into the array
					arraylist.add(map);
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			}else{
				userFunction.cutomToast(getResources().getString(R.string.error_message),getApplicationContext());
		}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			
			if(str_txt1!=null){
				txt1.setVisibility(View.GONE);
				txt1.setText(str_txt1);
			}
			if(str_txt2!=null){
				txt2.setVisibility(View.VISIBLE);
				txt2.setText(str_txt2);
			}
			if(str_txt3!=null){
				txt3.setVisibility(View.VISIBLE);
				txt3.setText(str_txt3);
			}
			if(str_txt4!=null){
				txt4.setVisibility(View.VISIBLE);
				txt4.setText(str_txt4);
			}
			if(str_txt5!=null){
				txt5.setVisibility(View.VISIBLE);
				txt5.setText(str_txt5);
			}
			if(str_txt6!=null){
				txt6.setVisibility(View.VISIBLE);
				txt6.setText(str_txt6);
			}
			if(str_txt7!=null){
				txt7.setVisibility(View.VISIBLE);
				txt7.setText(str_txt7);
			}
			if(str_txt8!=null){
				txt8.setVisibility(View.VISIBLE);
				txt8.setText(str_txt8);
			}

			
			if (jsonobject == null) {
				userFunction.cutomToast(
						getResources().getString(R.string.error_message),
						getApplicationContext());
				txt_error.setText(getResources().getString(R.string.error_message));
				txt_error.setVisibility(View.VISIBLE);
			}
			
			adapter = new ReportsDetailListAdapter(com.example.hdb.ReportsDetailsList.this, arraylist);
			list.setAdapter(adapter);
			mProgressDialog.dismiss();
			
			
		}

	}

	public void go_home_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);

	}
	
	public void go_dataEntry_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				EditRequest.class);
		startActivity(home_activity);

	}
}