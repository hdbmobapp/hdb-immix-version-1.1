package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class ACMTypes extends Activity {

	// Within which the entire activity is enclosed

	// ListView represents Navigation Drawer
	ListView mDrawerList;

	// ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
	// action bar
	ArrayList<HashMap<String, String>> arraylist;
	JSONArray jsonarray = null;
	ConnectionDetector cd;

	// Title of the action bar
	String mTitle = "";
	static LinearLayout drawerll;
	static String str_region;
	SharedPreferences pref;

	String str_user_id;
	String str_user_name;

	ACMTypeListAdapter adapter;
	JSONObject jsonobject = null;
	JSONObject json = null;
	ProgressDialog mProgressDialog;
	UserFunctions userFunction;

	static String ACM_USER_ID = "emp_id";
	static String ACM_NAME = "user_name";
	static String ACM_TOTAL_USERS = "total_users";
	static String ACM_ACTIVE_USERS = "active_users";
	static String ACM_TOTAL_PAY = "total_pay";
	static String ACM_TOTAL_PUNCHES = "total_punches";
	static String ACM_FLAG = "flag";
	static String ACM_LATITUDE = "latitude";
	static String ACM_LONGITUDE = "longitude";
	static String ACM_DATETIME = "datetime";
	static String ACM_REGION = "region";

	UserFunctions userfunction;
	String acm_id;
	String acm_name;
	String acmjsonobject;
	String str_from;
	String str_supervisor_id = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTitle = (String) getTitle();
		userFunction = new UserFunctions();
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		userfunction = new UserFunctions();

		str_user_id = pref.getString("user_id", null);
		str_user_name = pref.getString("user_name", null);


		Intent i = getIntent();
		str_region = i.getStringExtra("str_region");

		setlayout();

		arraylist = new ArrayList<HashMap<String, String>>();

		cd = new ConnectionDetector(com.example.hdb.ACMTypes.this);

		if (cd.isConnectingToInternet()) {

			new DownloadJSON().execute();
		} else {
			Toast.makeText(this, "No Internet Connection....",
					Toast.LENGTH_LONG).show();
		}
		// Setting the adapter on mDrawerList
		// mDrawerList.setAdapter(adapter);

	}

	public void setlayout() {
		mDrawerList = (ListView) findViewById(R.id.list);
	}

	public class DownloadJSON extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(com.example.hdb.ACMTypes.this);
			mProgressDialog.setMessage(getString(R.string.plz_wait));
			mProgressDialog.setCancelable(true);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			json = jsonobject;
			jsonobject = userFunction.getAcmTypeList(str_region, str_user_id);

			System.out.println("jsonobject:::::" + jsonobject);
			// Create an array
			try {
				// Locate the array name in JSON
				jsonarray = jsonobject.getJSONArray("Data");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();

					jsonobject = jsonarray.getJSONObject(i);
					// Retrive JSON Objects
					map.put("user_name", jsonobject.getString("SUP_DEP"));
					map.put("active_users",
							jsonobject.getString("active_users"));
					map.put("total_users", jsonobject.getString("total_users"));
					map.put("total_punches", jsonobject.getString("Punches"));
					map.put("total_pay", jsonobject.getString("pay"));
					map.put("region", jsonobject.getString("region"));
					map.put("flag", jsonobject.getString("flag"));

					// Set the JSON Objects into the array
					arraylist.add(map);
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			adapter = new ACMTypeListAdapter(com.example.hdb.ACMTypes.this, arraylist);
			mDrawerList.setAdapter(adapter);
			mProgressDialog.dismiss();
		}

	}

	public void go_home_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);

	}

	public void go_dataEntry_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				EditRequest.class);
		startActivity(home_activity);

	}
}
