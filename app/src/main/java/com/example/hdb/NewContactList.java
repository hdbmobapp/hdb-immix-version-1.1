package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;


/**
 * Created by Administrator on 16-10-2015.
 */
public class NewContactList extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;



    static String New_Details = "New_Details";



    NewContactListAdapter adapter;
    ProgressDialog mProgressDialog;
    TextView txt_error;
    SharedPreferences pref;
    String str_user_id = null;
    String str_table_name = null;
    EditText edt_loan_search;
    String str_loan_search;
    Button btn_submit;
    LinearLayout pd_details_layout;
    String str_message;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    LinearLayout lin_new_pd;
    RequestDB rdb;
    public int pgno = 0;

    ImageView img_app_usage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loan_trails);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);


        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        userFunction.checkforLogout(pref, com.example.hdb.NewContactList.this);



        setlayout();


    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        edt_loan_search = (EditText) findViewById(R.id.edt_search);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        img_app_usage=(ImageView) findViewById(R.id.img_app_usage);
        list = (ListView) findViewById(R.id.list);
        pd_details_layout = (LinearLayout) findViewById(R.id.pd_details_layout);
        img_app_usage.setBackgroundResource(R.drawable.contact_search_white);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arraylist = new ArrayList<HashMap<String, String>>();
                str_loan_search = edt_loan_search.getText().toString();

                if (str_loan_search != null && str_loan_search!=""  && !str_loan_search.isEmpty()) {
                    if (cd.isConnectingToInternet()) {
                         pgno = 0;
                        new DownloadLoanTrails().execute();
                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Please Enter LOSID",
                            getApplicationContext());
                }

            }

        });


    }

    public class DownloadLoanTrails extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.NewContactList.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                jsonobject = userFunction.getnewContactList(str_loan_search,"0");
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("Data");
                            jsonarray = jobj.getJSONArray("Data");

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);

                                map.put("New_Details", jsonobject.getString("New_Details"));

                                // Set the JSON Objects into the array
                                arraylist.add(map);
                            }
                        } else {
                            str_message = "No New Contact Details found for this LOSID...";
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            TextView txt_message = (TextView) findViewById(R.id.txt_message);

            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    pd_details_layout.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    adapter = new NewContactListAdapter(com.example.hdb.NewContactList.this, arraylist);
                    adapter.notifyDataSetChanged();
                    list.setAdapter(adapter);
                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }
                ((LoadMoreListView) list)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });

            }
            mProgressDialog.dismiss();
        }
    }
    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }

    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);

            jsonobject = userFunction.getnewContactList(str_loan_search, tmpgno);


            // Create an array
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            JSONObject jobj = jsonobject.getJSONObject("Data");
                            jsonarray = jobj.getJSONArray("Data");

                            for (int i = 0; i < jsonarray.length(); i++) {
                                HashMap<String, String> map = new HashMap<String, String>();
                                jsonobject = jsonarray.getJSONObject(i);
                                map.put("New_Details", jsonobject.getString("New_Details"));


                                // Set the JSON Objects into the array
                                arraylist.add(map);
                            }
                        } else {
                            str_message = "No New Contact Details found for this LOSID...";
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) list).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) list).onLoadMoreComplete();
        }
    }
}
