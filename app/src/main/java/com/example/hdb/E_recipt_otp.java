package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.MyAdapter;
import info.hdb.libraries.PinView;
import info.hdb.libraries.PinViewSettings;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 24-07-2018.
 */
public class E_recipt_otp extends Fragment {

    EditText edt_payee_name,edt_Contact_no,edt_otp,edt_if_other,edt_email,edt_ercpt_no;
    TextView txt_otp;
    LinearLayout tableRow_if,tableRow_ercpt_no;

    Button btn_generate_otp;
    Button btn_submit,btn_Validate_otp;

    String  strs_emp_code,strs_emp_name,strs_chq_no,strs_bank_name,strs_chq_date,
            strs_rcpt_date,strs_emi_amt, strs_lpp_amt,strs_bcc_amt,
            strs_coll_oth_chrg,strs_total,strs_remarks,strs_pan_no,str_rm_rs,str_pan,
            strs_rcpt_amt,str_brch_gst_sc,str_branch_state,str_cust_gst_sc,str_cust_state,
            str_gstn,str_cust_id,str_rm_rs_gen,str_form60;

    String  str_pay_mode,str_product,str_loc_gen,str_product_name,
            str_loan_no,str_cust_name,str_branch_id,str_branch,str_contact_no,
            str_unique_id,str_pickup_type,str_pan1,str_pan2, str_total_emi_due;

    String str_lat,str_long, str_payee_name,str_otp_contact_no,str_if_other,str_email;
    String  str_transcation_id,str_ercpt_no;
    String str_otp,str_otp_mode;
    Double  longitude ,latitude;

    Spinner spinner_rel_to_cust;
    String rand_otp,str_phone_no,str_rel,id;
    private ProgressBar progress_bar;
    int click_count;
    private PinView pinView;
    UserFunctions userFunction;
    Boolean otp_verified = false;
    ProgressDialog mProgressDialog;
    JSONObject json;
    Boolean ins = true;
    ConnectionDetector cd;
    public boolean isLocationEnabled=false;
    JSONObject jsonobject = null;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");

    String str_from,str_id;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.e_recipt_otp, container, false);

        str_from =Ercpt_activity_main.str_from.toString();
        System.out.println("kkkkkkkkkkkkkkkk::::::::::"+str_from);
        str_id  =Ercpt_activity_main.str_id.toString();


        str_loc_gen =getArguments().getString("str_loc_gen");

        str_pay_mode = getArguments().getString("str_pay_mode");
        str_loan_no = getArguments().getString("str_loan_no");
        str_cust_name = getArguments().getString("str_cust_name");
        str_product = getArguments().getString("str_product");
        str_product_name = getArguments().getString("str_product_name");

        str_branch_id = getArguments().getString("str_branch_id");
        str_branch = getArguments().getString("str_branch");
        str_contact_no = getArguments().getString("str_contact_no");
        str_unique_id = getArguments().getString("str_unique_id");
        str_total_emi_due = getArguments().getString("str_total_emi_due");
        System.out.println("str_total_emi_due::::::::::::::::::hjg" + str_total_emi_due);
        str_pickup_type = getArguments().getString("str_pickup_type");



        strs_emp_code = getArguments().getString("strs_emp_code");
        strs_emp_name = getArguments().getString("strs_emp_name");
        strs_chq_no = getArguments().getString("strs_chq_no");
        strs_bank_name = getArguments().getString("strs_bank_name");
        strs_chq_date = getArguments().getString("strs_chq_date");
        strs_rcpt_date = getArguments().getString("strs_rcpt_date");
        strs_emi_amt = getArguments().getString("strs_emi_amt");
        strs_lpp_amt = getArguments().getString("strs_lpp_amt");
        strs_bcc_amt = getArguments().getString("strs_bcc_amt");
        strs_coll_oth_chrg = getArguments().getString("strs_coll_oth_chrg");
        strs_total = getArguments().getString("strs_total");
        strs_remarks = getArguments().getString("strs_remarks");
        strs_pan_no = getArguments().getString("strs_pan_no");
        str_rm_rs = getArguments().getString("str_rm_rs");
        str_rm_rs_gen =getArguments().getString("str_rm_rs_gen");
        str_pan = getArguments().getString("str_pan");
        str_form60 = getArguments().getString("str_form60");

        strs_rcpt_amt = getArguments().getString("strs_rcpt_amt");
        str_brch_gst_sc = getArguments().getString("str_brch_gst_sc");
        str_branch_state = getArguments().getString("str_branch_state");
        str_cust_gst_sc = getArguments().getString("str_cust_gst_sc");
        str_cust_state = getArguments().getString("str_cust_state");
        str_gstn = getArguments().getString("str_gstn");
        str_cust_id = getArguments().getString("str_cust_id");

        System.out.println("str_pickup_type::::::::::::::::::"+str_pickup_type);

        userFunction = new UserFunctions();
        cd = new ConnectionDetector(getActivity().getApplicationContext());

        isLocationEnabled=((Ercpt_activity_main)getActivity()).checkLocation();


        if(isLocationEnabled==true) {

            longitude = Ercpt_activity_main.lat;
            latitude = Ercpt_activity_main.lon;
            if (latitude != null) {
                str_lat = Double.toString(latitude);
                str_long = Double.toString(longitude);
            }
        }


        System.out.println("lati+++++++"+str_lat);
        System.out.println("long------------"+str_long);

        /* Button*/
        btn_generate_otp = (Button) view.findViewById(R.id.btn_generate_otp);
        btn_submit = (Button) view.findViewById(R.id.btn_submit);
        btn_Validate_otp = (Button) view.findViewById(R.id.btn_Validate_otp);

        /* EditText*/
        edt_payee_name = (EditText) view.findViewById(R.id.edt_payee_name);
        edt_Contact_no = (EditText) view.findViewById(R.id.edt_Contact_no);
        edt_otp = (EditText) view.findViewById(R.id.edt_otp);
        edt_if_other = (EditText) view.findViewById(R.id.edt_if_other);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_ercpt_no = (EditText) view.findViewById(R.id.edt_ercpt_no);
        edt_ercpt_no.setEnabled(false);

        /* TextView*/
        txt_otp = (TextView) view.findViewById(R.id.txt_otp);

        /*LinearLayout*/
        tableRow_if =  (LinearLayout) view.findViewById(R.id.tableRow_if);
        tableRow_ercpt_no =  (LinearLayout) view.findViewById(R.id.tableRow_ercpt_no);

        /* Spinner*/
        spinner_rel_to_cust = (Spinner) view.findViewById(R.id.spinner_rel_to_cust);
        pinView = (PinView) view.findViewById(R.id.pinView);
        progress_bar = (ProgressBar)view. findViewById(R.id.progress_bar);

        // new newShortAsync().execute();


        ArrayAdapter<String> spinnerArrayAdapteter = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, Ercpt_activity_main.al_rel);
        spinnerArrayAdapteter.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_rel_to_cust.setAdapter(spinnerArrayAdapteter);
        spinner_rel_to_cust.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_rel_to_cust.getSelectedItemPosition() == 0) {
                    str_rel = null;
                } else {
                    str_rel = spinner_rel_to_cust.getItemAtPosition(spinner_rel_to_cust.getSelectedItemPosition()).toString();
                    if(str_rel.contains("Others")){
                        tableRow_if.setVisibility(View.VISIBLE);
                    }else{
                        tableRow_if.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        PinViewSettings pinViewSettings = new PinViewSettings.Builder()
                // .withPinTitles(titlesAux)
                .withMaskPassword(false)
                .withDeleteOnClick(true)
                .withKeyboardMandatory(false)
                .withSplit(null)
                .withNumberPinBoxes(4)
                .withNativePinBox(false)
                .build();

        pinView.setSettings(pinViewSettings);


        pinView.setOnCompleteListener(new PinView.OnCompleteListener() {
            @Override
            public void onComplete(boolean completed, final String pinResults) {
                if (completed) {
                    btn_Validate_otp.setVisibility(View.VISIBLE);
                    str_otp = pinResults;



                }
            }
        });

        btn_Validate_otp.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                //  Toast.makeText(getContext(), "str_otp"+str_otp, Toast.LENGTH_SHORT).show();

                if (str_otp.equals(rand_otp)) {
                    // Toast.makeText(getContext(), "str_otp"+str_otp, Toast.LENGTH_SHORT).show();

                    if (cd.isConnectingToInternet()) {
                        new ercpt_generate().execute();
                        // System.out.println("aaaaaaaaaaaaabbbbbbbbbbbb");

                    }else{
                        Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                    tableRow_ercpt_no.setVisibility(View.VISIBLE);


                    btn_generate_otp.setVisibility(View.GONE);
                    pinView.setVisibility(View.GONE);
                    pinView.setEnabled(false);
                    //  sb.setVisibility(View.GONE);
                    txt_otp.setVisibility(View.GONE);
                    otp_verified = true;

                } else {
                    userFunction.cutomToast("OTP is invalid  ", getActivity().getApplicationContext());
                    txt_otp.setText("");

                    btn_generate_otp.setText("Re-generate OTP");

                    txt_otp.setVisibility(View.VISIBLE);

                }

            }
        });

        btn_generate_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ins = true;
                String mobile_no = edt_Contact_no.getText().toString();

                str_payee_name = edt_payee_name.getText().toString();
                str_otp_contact_no = edt_Contact_no.getText().toString();
                str_if_other = edt_if_other.getText().toString();
                str_email = edt_email.getText().toString();


                if (str_rel == null && ins == true) {
                    ins = false;
                    Toast.makeText(getContext(), "You did not Select Relation", Toast.LENGTH_SHORT).show();
                }


                if (str_rel != null && !str_rel.isEmpty() && ins == true) {
                    if (str_rel.contains("Others")){
                        if (str_if_other.equals("") && ins == true) {
                            edt_if_other.setError("You did not Specify Your Relation.");
                            edt_if_other.requestFocus();
                            ins = false;
                        }
                    }
                }

                if (str_payee_name.equals("") && ins == true) {
                    edt_payee_name.setError("Please Enter Payee Name.");
                    edt_payee_name.requestFocus();
                    ins = false;
                }

                if (str_otp_contact_no.equals("") && ins == true) {
                    edt_Contact_no.setError("Please Enter Contact No.");
                    edt_Contact_no.requestFocus();
                    ins = false;
                }

                if (!str_otp_contact_no.isEmpty() && ins == true) {
                    System.out.println("str_otp_contact_no"+str_otp_contact_no);
                    if (str_otp_contact_no.length()!=10 && ins == true) {
                        System.out.println("con_no::"+str_otp_contact_no);
                        edt_Contact_no.setError("Please valid Contact No.");
                        edt_Contact_no.requestFocus();
                        ins = false;
                    }
                }


                if (str_email.equals("") && ins == true) {
                    //edt_email.setError("Please Enter Contact No.");
                    //  edt_email.requestFocus();
                    ins = true;
                }

                if (!str_email.equals("") && ins == true) {
                    if ( CheckEmail(str_email)) {
                        ins = true;
                    }else {
                        edt_email.setError("Enter valid Email");
                        edt_email.requestFocus();
                        ins = false;
                    }
                }


                if (cd.isConnectingToInternet()) {
                    if (ins == true) {

                        click_count = click_count + 1;
                        if (click_count > 3) {
                            btn_generate_otp.setEnabled(false);
                            //  btn_otp.setBackground(R.color.default_gray);
                            txt_otp.setText("Maximum tries to generate One Time Password(OTP) has exceeded , wait for OTP or skip the OTP. ");
                            userFunction.cutomToast("Maximum tries to generate One Time Password(OTP) has exceeded , wait for OTP or skip the OTP. ", getActivity().getApplicationContext());
                            btn_generate_otp.setText("Regenerate OTP");
                            btn_generate_otp.setBackgroundResource(R.drawable.image_btn_selector_gray);
                            txt_otp.setVisibility(View.VISIBLE);
                        }

                        if (click_count <= 3) {
                            // sb.setVisibility(View.GONE);
                            System.out.println("aaaaaaaaaaaaaaa"+click_count);

                            if((click_count % 2) == 0){
                                str_otp_mode = "tata";
                                System.out.println("++++++++++++++:::::::::"+str_otp_mode);
                            }else{

                                str_otp_mode = "mgage";
                                System.out.println("============:::::::::::"+str_otp_mode);
                            }
                            new Get_OTP().execute(mobile_no,str_otp_mode);
                        }

                    }
                } else {
                    userFunction.cutomToast("No internet Connection  ", getContext());

                }

            }


        });



        btn_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                if (cd.isConnectingToInternet()) {

                    if (ins == true) {

                        isLocationEnabled=((Ercpt_activity_main)getActivity()).checkLocation();
                        System.out.println("service location:::::::::::::::::::::::"+isLocationEnabled);

                        if(isLocationEnabled==true){

                            longitude = Ercpt_activity_main.lat;
                            System.out.println("longitude:::::::::::::::::::"+longitude);
                            latitude = Ercpt_activity_main.lon;
                            if (latitude != null) {
                                str_lat = Double.toString(latitude);
                                System.out.println("str_lat:::::::::::::::::::" + str_lat);
                                str_long = Double.toString(longitude);
                            }

                            new InssertData_Ercpt().execute();
                            // new newShortAsync().execute();
                            System.out.println("succesfull:::::::::::::::::::+++");

                        }
                    }
                } else {
                    userFunction.cutomToast("No internet Connection  ", getContext());

                }
            }
        });


        return view;
    }
    public  class Get_OTP extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {


            try {


                String mobile_no = params[0];
                String otp_mode  = params[1];
                rand_otp = "" + ((int) (Math.random() * 9000) + 1000);
                json = userFunction.get_otp(mobile_no, rand_otp,otp_mode);
                str_transcation_id = json.getString("transaction_id");
                System.out.println("str_transcation_id+++++++::::::::"+str_transcation_id);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);
                        str_phone_no = edt_Contact_no.getText().toString();
                        final String str_phone_no_nw = str_phone_no.substring(str_phone_no.length() - 4);
                        pinView.clear();

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            new CountDownTimer(30000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    txt_otp.setText("One Time Password(OTP) has been sent on ******" + str_phone_no_nw + " , please enter the same here to proceed or wait to re-generate OTP after " + millisUntilFinished / 1000 + " seconds ");
                                    btn_generate_otp.setEnabled(false);
                                    btn_generate_otp.setBackgroundResource(R.drawable.image_btn_selector_gray);
                                }

                                public void onFinish() {
                                    txt_otp.setText("");
                                    btn_generate_otp.setEnabled(true);
                                    btn_generate_otp.setText("Regenerate OTP");
                                    btn_generate_otp.setBackgroundResource(R.drawable.image_btn_selector);
                                }
                            }.start();

                            userFunction.cutomToast(
                                    "OTP Sent Successfully on mobile",
                                    getActivity().getApplicationContext());
                            pinView.setVisibility(View.VISIBLE);
                            //  sb.setVisibility(View.VISIBLE);
                            btn_generate_otp.setText("Regenerate OTP");


                        } else {
                            userFunction.cutomToast(
                                    json.getString("response_code_desc"),
                                    getActivity().getApplicationContext());

                        }
                    }


                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getActivity().getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mProgressDialog.dismiss();

        }

    }

    public class InssertData_Ercpt extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btn_submit.setEnabled(false);
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.data_uploading));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            jsonobject = userFunction.InsertData_Ert(str_pay_mode, str_loan_no, str_cust_name, str_product, str_branch_id, str_branch, str_contact_no,
                    str_unique_id, str_total_emi_due, str_pickup_type, strs_emp_code, strs_emp_name,
                    strs_chq_no, strs_bank_name, strs_chq_date,
                    strs_rcpt_date, strs_emi_amt, strs_lpp_amt,
                    strs_bcc_amt, strs_coll_oth_chrg, strs_total, strs_remarks, strs_pan_no, str_rm_rs,str_lat,str_long,
                    str_pan,strs_rcpt_amt,str_brch_gst_sc,str_branch_state,str_cust_gst_sc,str_cust_state,str_gstn,str_cust_id,str_loc_gen,str_rm_rs_gen,str_rel,
                    str_otp_contact_no,str_payee_name,str_form60,str_if_other,str_email,str_transcation_id,str_ercpt_no,str_product_name,str_from,str_id);



            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        String message=jsonobject.getString("message");

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            userFunction.cutomToast(message,
                                    getContext());
                            Intent messagingActivity = new Intent(
                                    getContext(), HdbHome.class);
                            startActivity(messagingActivity);

                        } else {
                            btn_submit.setEnabled(false);

                            String   error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg,
                                    getContext());

                        } }

                }else{
                    btn_submit.setEnabled(false);

                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }
    }

    public class ercpt_generate extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonobject = userFunction.rcpt_generate(str_loc_gen,strs_emp_name, str_loan_no,strs_emp_code);



            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {

                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            str_ercpt_no =  jsonobject.getString("e_rcpt_no");
                            System.out.println("str_ercpt_no::"+str_ercpt_no);

                            userFunction.cutomToast("OTP validated Successfully....",
                                    getContext());
                            if(str_ercpt_no!=null) {

                                edt_ercpt_no.setText(str_ercpt_no);
                                btn_Validate_otp.setVisibility(View.GONE);
                                btn_submit.setVisibility(View.VISIBLE);
                                edt_payee_name.setEnabled(false);
                                edt_Contact_no.setEnabled(false);
                            }
                            else{
                                btn_Validate_otp.setVisibility(View.VISIBLE);
                            }
                        } else {
                            String
                                    error_msg = jsonobject.getString("message");
                            userFunction.cutomToast(error_msg,
                                    getContext());

                        }
                    }
                }else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getActivity(). getApplicationContext());

                }

                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }
    }

    private boolean CheckEmail(String email) {

        return EMAIL_PATTERN.matcher(email).matches(); }

    public class newShortAsync extends AsyncTask<Void,Void,String> {

        String longUrl="https://hdbapp.hdbfs.com/Web_Pages/mob/index.php";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress_bar.setVisibility(View.VISIBLE);
        }



        @Override
        protected String doInBackground(Void... params) {
            BufferedReader reader;
            StringBuffer buffer;
            String res=null;
            String json = "{\"longUrl\": \""+longUrl+"\"}";
            try {
                URL url = new
                        URL("https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAFf6w2OG44pLdqJklBKfY0t_39IgAaryI");
                HttpURLConnection con = (HttpURLConnection)
                        url.openConnection();
                con.setReadTimeout(40000);
                con.setConnectTimeout(40000);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));

                writer.write(json);
                writer.flush();
                writer.close();
                os.close();

                int status=con.getResponseCode();
                InputStream inputStream;
                if(status==HttpURLConnection.HTTP_OK)
                    inputStream=con.getInputStream();
                else
                    inputStream = con.getErrorStream();

                reader= new BufferedReader(new
                        InputStreamReader(inputStream));

                buffer= new StringBuffer();

                String line="";
                while((line=reader.readLine())!=null)
                {
                    buffer.append(line);
                }

                res= buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return res;




        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress_bar.setVisibility(View.GONE);
            System.out.println("JSON RESP:" + s);
            String response=s;
            try {
                JSONObject jsonObject=new JSONObject(response);
                id =jsonObject.getString("id");
                System.out.println("ID:"+id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}


