package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

public class Addition_menus extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject = null;
    private ConnectionDetector cd;
    GridView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    static String MESSAGE_KEY = "link";
    static String MESSAGE_VALUE = "title";


    TextView txt_error;
    Addition_menus_adapter adapter;
    ProgressDialog mProgressDialog;
    SharedPreferences pref;
    String str_user_id = null;
    public int pgno = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.additional_menus);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);

        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        setlayout();
    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        list = (GridView) findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();
        if (cd.isConnectingToInternet()) {
            new DownloadAdditionalmenus().execute();
        } else {

            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);


        }

    }

    public class DownloadAdditionalmenus extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Addition_menus.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // jsonobject = userFunction.getDashboard();

            jsonobject = userFunction.getAdditonalmenus(str_user_id);
            if (jsonobject != null) {

                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Data");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        jsonobject = jsonarray.getJSONObject(i);

                        // Retrive JSON Objects
                        map.put("title", jsonobject.getString("title"));
                        map.put("link", jsonobject.getString("link"));


                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(getResources().getString(R.string.error_message), getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            }
            adapter = new Addition_menus_adapter(com.example.hdb.Addition_menus.this, arraylist);
            list.setAdapter(adapter);
            mProgressDialog.dismiss();


        }

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(), HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(), EditRequest.class);
        startActivity(home_activity);
        finish();
    }


}