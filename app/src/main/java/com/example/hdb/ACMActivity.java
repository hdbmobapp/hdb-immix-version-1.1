package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class ACMActivity extends Activity {

	// Within which the entire activity is enclosed

	// ListView represents Navigation Drawer
	ListView mDrawerList;

	// ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
	// action bar
	ArrayList<HashMap<String, String>> arraylist;
	JSONArray jsonarray = null;
	ConnectionDetector cd;

	// Title of the action bar
	String mTitle = "";
	String str_region;
	SharedPreferences pref;

	String str_user_id;
	String str_user_name;

	ACMTypeListAdapter adapter;
	JSONObject jsonobject = null;
	JSONObject json = null;
	ProgressDialog mProgressDialog;
	UserFunctions userFunction;

	static String ACM_USER_ID = "emp_id";
	static String ACM_NAME = "user_name";
	static String ACM_TOTAL_USERS = "total_users";
	static String ACM_ACTIVE_USERS = "active_users";
	static String ACM_TOTAL_PAY = "total_pay";
	static String ACM_TOTAL_PUNCHES = "total_punches";
	static String ACM_FLAG = "flag";
	static String ACM_LATITUDE = "latitude";
	static String ACM_LONGITUDE = "longitude";
	static String ACM_DATETIME = "datetime";
	static String ACM_REGION = "region";

	UserFunctions userfunction;
	String str_from;
	String str_supervisor_id = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mTitle = (String) getTitle();
		userFunction = new UserFunctions();
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		userfunction = new UserFunctions();
		userFunction.checkforLogout(pref, com.example.hdb.ACMActivity.this);

		str_user_id = pref.getString("user_id", null);
		str_user_name = pref.getString("user_name", null);

		System.out.println("user_id:::" + str_user_id);
		System.out.println("user_id:::str_user_name:::" + str_user_name);

		Intent i = getIntent();
		str_from = i.getStringExtra("from");
		str_region = i.getStringExtra("str_region");
		if (str_from.compareTo("acm") == 0) {
			str_supervisor_id = i.getStringExtra("supervisor_id");
		}
		setlayout();

		arraylist = new ArrayList<HashMap<String, String>>();

		cd = new ConnectionDetector(com.example.hdb.ACMActivity.this);

		if (cd.isConnectingToInternet()) {

			new DownloadJSON().execute();
		} else {
			Toast.makeText(this, "No Internet Connection....",
					Toast.LENGTH_LONG).show();
		}
		// Setting the adapter on mDrawerList
		// mDrawerList.setAdapter(adapter);

	}

	public void setlayout() {

		mDrawerList = (ListView) findViewById(R.id.list);

	}

	public class DownloadJSON extends AsyncTask<Void, Void, Void> {

		protected void onPreExecute() {
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(com.example.hdb.ACMActivity.this);
			mProgressDialog.setMessage(getString(R.string.plz_wait));
			mProgressDialog.setCancelable(true);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			json = jsonobject;
			if (str_supervisor_id == null)
				jsonobject = userFunction.getACMList(str_region, "A","");
			else	
				jsonobject = userFunction.getCAlist(str_region,
						str_supervisor_id, "C","");

			System.out.println("jsonobject:::::" + jsonobject);
			// Create an array
			try {
				// Locate the array name in JSON
				jsonarray = jsonobject.getJSONArray("ACMList");

				for (int i = 0; i < jsonarray.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();

					jsonobject = jsonarray.getJSONObject(i);
					// Retrive JSON Objects
					map.put("emp_id", jsonobject.getString("emp_id"));
					map.put("user_name", jsonobject.getString("user_name"));
					map.put("active_users",
							jsonobject.getString("active_users"));
					map.put("total_users", jsonobject.getString("total_users"));
					map.put("total_punches",
							jsonobject.getString("total_punches"));
					map.put("total_pay", jsonobject.getString("total_pay"));
					map.put("flag", jsonobject.getString("flag"));
					map.put("latitude", jsonobject.getString("latitude"));
					map.put("longitude", jsonobject.getString("longitude"));
					map.put("datetime", jsonobject.getString("datetime"));
					map.put("region", jsonobject.getString("region"));

					
					// Set the JSON Objects into the array
					arraylist.add(map);
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			adapter = new ACMTypeListAdapter(com.example.hdb.ACMActivity.this, arraylist);
			mDrawerList.setAdapter(adapter);
			mProgressDialog.dismiss();
		}

	}

}
