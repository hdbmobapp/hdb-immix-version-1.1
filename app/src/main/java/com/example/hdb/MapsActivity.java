package com.example.hdb;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.ExampleDBHelper;
import info.hdb.libraries.UserFunctions;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Locale;


/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class MapsActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final String TAG = "MapsMarkerActivity";

    static Double lat;
    static Double lon;
    LatLng latLng;
    GoogleMap mMap;
    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;
    Button btn_refresh, btn_setlocation;
    SharedPreferences pref;
    UserFunctions userfunction;
    String addres = null;
    RequestDB rdb;
    String str_losid;
    String str_app_form_no;
    String str_ts;
    String str_user_id;
    String str_from_act;
    String str_latitude;
    String str_longitude;
    ExampleDBHelper dbHelper;
    TextView tvLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        checkLocation();
        dbHelper = new ExampleDBHelper(this);
        setContentView(R.layout.activity_maps);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) MapsActivity.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        rdb = new RequestDB(MapsActivity.this);
        userfunction = new UserFunctions();



        tvLocation = (TextView) findViewById(R.id.tv_location);

        Intent asset = getIntent();
        str_losid = asset.getStringExtra("str_losid_map");
        str_from_act = "COLLX";
        str_ts = asset.getStringExtra("str_ts");


        System.out.println("TS MAP::::" + str_ts);


        str_user_id = pref.getString("user_id", null);

        if (str_losid.trim().length() == 0) {
            str_losid = null;
        }

        System.out.println("str_losid_no:::" + str_losid);


        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove("map_cont");
        peditor.commit();


        btn_refresh = (Button) findViewById(R.id.btn_refresh);
        // mDrawerList.setAdapter(adapter);
        btn_setlocation = (Button) findViewById(R.id.btn_setlocation);


        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent img_pinch = new Intent(
                        MapsActivity.this, MapsActivity.class);
                startActivity(img_pinch);
                finish();*/

                checkLocation();
            }

        });

        btn_setlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("nnnnnnnnnnnnnnjjjjjjj");
                if (lat != null) {
                    str_latitude = String.valueOf(lat);
                    str_longitude = String.valueOf(lon);
                }
                if (str_latitude != null) {
                    System.out.println("nnnnnnnnnnnnnnjjjjjjj");
                    CaptureMapScreen();

                } else {
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putInt("is_locate_clicked", 1);
                    peditor.commit();
                    userfunction.cutomToast("Location service is not active...", MapsActivity.this);
                }
                finish();
            }

        });

    }


    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(getApplicationContext(), "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat = location.getLatitude();
        System.out.println("aaaaaaaaaaabbbbbbbb"+lat);
        lon = location.getLongitude();
        if (lat != null) {
            str_latitude = String.valueOf(lat);
            str_longitude = String.valueOf(lon);
        }
        System.out.println("lat::::" + lat + "lon:::::KK:::" + lon);
        mMap.clear();
        LatLng sydney = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(sydney)
                .title("My location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        // location_lat.setText("" + lat);
        // location_long.setText("" + lon);
        // latitude = String.valueOf(lat);
        // longitude = String.valueOf(lon);
        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps

      // GPSTracker_New tracker = new GPSTracker_New(MapsActivity.this);

        if(isLocationEnabled()){
            lat = location.getLatitude();
            lon = location.getLongitude();
            getCompleteAddressString(lat, lon, MapsActivity.this);
            System.out.println("aaaaaaaaabbbbbbbbbbbcccccccccc");
        }

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        addres = getCompleteAddressString(lat, lon, MapsActivity.this);

        tvLocation.setText("Latitude:" + lat + ",Longitude:" + lon + "\nAddress::" + addres);
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MapsActivity.this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);


    }

    public void CaptureMapScreen() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                bitmap = snapshot;
                try {

                    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES), "HDB_COLLX");
                    if (!mediaStorageDir.exists()) {
                        if (!mediaStorageDir.mkdirs()) {

                        }
                    }
                    // Create a media file name
                    File mediaFile;

                    String new_path = mediaStorageDir.getPath() + File.separator + str_user_id + "_" + str_losid + "_"
                            + str_ts + "_" + "COLLX" + "_" + "MAP.jpg";

                    mediaFile = new File(new_path);

                    FileOutputStream out = new FileOutputStream(mediaFile);
                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);
                    SharedPreferences.Editor peditor = pref.edit();
                    peditor.putString("map_img_name", new_path);

                     peditor.putInt("is_map_captured", 1);


                    peditor.putInt("is_locate_clicked", 1);
                    peditor.putString("map_address", addres);

                    peditor.putString("pref_latitude", str_latitude);
                    peditor.putString("pref_longitude", str_longitude);
                    peditor.commit();
                    dbHelper.insertImage(new_path, str_losid, str_user_id, str_ts, "0", "map");


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        mMap.snapshot(callback);
    }

    public String getCompleteAddressString(double LATITUDE, double LONGITUDE ,Context cnt) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(cnt, Locale.getDefault());
        try {
            List<Address> addresses = geocoder
                    .getFromLocation(LATITUDE, LONGITUDE, 1);

            strAdd=addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.w(" location address", "Cannot get Address!");
        }
        return strAdd;
    }
}