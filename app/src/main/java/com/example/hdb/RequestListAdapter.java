package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 07-08-2015.
 */
public class RequestListAdapter extends BaseAdapter {


    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;

    String remark_by_id;
    static RemarksListAdapter INSTANCE;




    //ViewHolder holder = null;

    //View itemView;
    public RequestListAdapter(Context context,
                              ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);



    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        View itemView = null;
        ViewHolder holder;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.request_list_item, parent,
                    false);

            holder = new ViewHolder();
            holder.txt_remarks = (TextView) itemView
                    .findViewById(R.id.txt_remarks);
            holder.txt_remark_by = (TextView) itemView
                    .findViewById(R.id.txt_remark_by);
            holder.txt_date = (TextView) itemView
                    .findViewById(R.id.txt_date);
            holder.txt_remark_by_id = (TextView) itemView
                    .findViewById(R.id.txt_remark_by_id);

            itemView.setTag(holder);
        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }
        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }

        if (resultp.get(Remarks_Display.REMARK).toString() != null && !resultp.get(Remarks_Display.REMARK).toString().isEmpty() && !resultp.get(Remarks_Display.REMARK).toString().equals("null")) {

            holder.txt_remarks.setText(resultp.get(Remarks_Display.REMARK));
            holder.txt_remarks.setVisibility(View.VISIBLE);
            holder.txt_remark_by.setText(resultp.get(Remarks_Display.REMARK_BY));
            holder.txt_remark_by.setVisibility(View.VISIBLE);
            holder.txt_date.setText(resultp.get(Remarks_Display.REMARK_DATE));
            holder.txt_date.setVisibility(View.VISIBLE);
            holder.txt_remark_by_id.setText(resultp.get(Remarks_Display.REMARK_BY_ID));
            holder.txt_remark_by_id.setVisibility(View.VISIBLE);

        }

        itemView.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View arg0) {
                arg0.setSelected(true);
                // Get the position
                resultp = data.get(position);

                remark_by_id=resultp.get(Remarks_Display.REMARK_BY_ID).toString();


                Intent remark_activity = new Intent(context,
                        Request_Display.class);
                remark_activity.putExtra("remark_by_id",remark_by_id);
                context.startActivity(remark_activity);



            }
        });


//
        return itemView;
    }

    private static class ViewHolder {
        public TextView txt_remarks;
        public TextView txt_remark_by;
        public TextView txt_date;
        public TextView txt_remark_by_id;


    }


}
