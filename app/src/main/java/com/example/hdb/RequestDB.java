package com.example.hdb;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class RequestDB extends SQLiteOpenHelper implements Runnable {
    JSONObject jsonobject;
    UserFunctions userFunction;

    ArrayList<String> get_masters;

    ArrayList<String> get_flag;

    ArrayList<String> arr_dispo_code,arr_dispo_val,arr_pm,arr_pm_val,arr_pd,arr_pd_val;


    /**
     * Database name
     */
    private static String DBNAME = "sqlite_pd_com_app";
    private static String KEY_SUCCESS = "success";
    private ConnectionDetector cd;
    ProgressDialog mProgressDialog;

    /**
     * Version number of the database
     */
    private static int VERSION = 1;

    public static final String KEY_ROW_ID = "_id";
    public static final String KEY_LOSID = "losid";
    public static final String KEY_Customer_Name = "customer_Name";
    public static final String KEY_Account_Status = "acc_status";
    public static final String KEY_PDD_Status = "pdd_status";
    public static final String KEY_Data_Entry = "data_entry";
    public static final String KEY_Visit_Mode = "visit_mode";
    public static final String KEY_Number_Contacted = "number_contacted";
    public static final String KEY_Bucket_Type = "bucket_type";
    public static final String KEY_Met_Spoke = "met_spoke";
    public static final String KEY_DispoCode = "dispoCode";
    public static final String KEY_Next_Follow_Date = "next_follow_date";
    public static final String KEY_Remarks = "remarks";
    public static final String KEY_New_Contact_Number = "new_contact_number";
    public static final String KEY_New_Contact_Address = "new_contact_address";
    public static final String KEY_New_Contact_Email = "new_contact_email";
    public static final String KEY_Receipt_Number = "receipt_number";
    public static final String KEY_Total_Amount = "total_amount";
    public static final String KEY_EMI_Amount = "emi_amount";
    public static final String KEY_Other_Amount = "other_amount";
    public static final String KEY_Mode = "mode";
    public static final String KEY_Cheque_Num = "Cheque_Num";
    public static final String KEY_PAN_no = "pan_no";
    public static final String KEY_Form_sixty = "form_sixty";
    public static final String KEY_Coll_Sighted = "coll_sighted";
    public static final String KEY_Coll_Condition = "coll_condition";
    public static final String KEY_3rd_Party = "third_party";
    public static final String KEY_3rd_Party_Name = "third_party_name";
    public static final String KEY_3rd_Party_Address = "third_party_address";
    public static final String KEY_Repo_Doable = "repo_doable";
    public static final String KEY_Photo1 = "photo_one";
    public static final String KEY_Photo2 = "Phototwo";
    public static final String KEY_Photo3 = "photo_three";
    public static final String KEY_Photo4 = "photo_four";
    public static final String KEY_Photo5 = "photo_five";
    public static final String KEY_Photo6 = "photo_six";
    public static final String KEY_Photo7 = "photo_seven";
    public static final String KEY_Photo8 = "photo_eight";
    public static final String KEY_Photo9 = "photo_nine";
    public static final String KEY_Photo10 = "photo_ten";
    public static final String KEY_UserID = "userid";
    public static final String KEY_Upload_Date = "upload_date";
    public static final String KEY_GPS_Latitiude = "gps_latitiude";
    public static final String KEY_GPS_Longitude = "gps_longitude";
    public static final String KEY_STATUS = "status";
    public static final String KEY_TS = "ts";
    public static final String KEY_LOAN_DISTANCE = "distance";
    public static final String KEY_BRANCH_NAME = "branch_name";
    public static final String KEY_PRODUCT = "product";
    public static final String KEY_BOM_BKT = "bom_bkt";
    public static final String KEY_RISK_BKT = "risk_bkt";
    public static final String KEY_CBC_DUE = "cbc_due";
    public static final String KEY_LPP_DUE = "lpp_due";

    public static final String KEY_LATITUDE_DIST = "dis_latitude";
    public static final String KEY_LONGITUDE_DIST = "dis_logitude";
    public static final String KEY_DATETIME_DIST = "dis_datetime";
    public static final String KEY_DIST_KM = "dis_km";

    public static final String KEY_MONTH_E_STATUS = "me_status";
    public static final String KEY_MONTH_E_WEEK = "me_week";


    public static final String KEY_BIZ_val = "biz_value";
    public static final String KEY_PROPERTY_val = "property_value";
    public static final String KEY_Current_BIZ_val = "current_biz_value";
    public static final String KEY_Not_able_pay_type_val = "not_able_pay_val";
    public static final String KEY_Coll_Occupant_type_value = "coll_occupant";
    public static final String KEY_cust_has_loan = "cust_has_loan";
    public static final String KEY_max_emi_amt_capble = "max_emi_amt";
    public static final String KEY_hand_loane_value = "hand_loan_value";
    public static final String KEY_loan_mnth_emi = "loan_mntn_emi";

    public static final String KEY_settle_amt = "settle_amt";

    public static final String KEY_STATUS1 = "key_status1";
    public static final String KEY_pic_count ="pic_count";

    public static final String KEY_ACH_status ="ach_status";
    public static final String KEY_distance_km = "distance_in_km";
    public static final String KEY_distance_type = "distance_type";
    public static final String KEY_map_address = "map_address";



    //DISPO TABLE

    public static final String KEY_dispo_id = "dispo_id";
    public static final String KEY_dispo_code = "dispo_code ";
    public static final String KEY_dispo_flag = "dispo_flag";
    public static final String KEY_dispo_srno = "dispo_srno";
    public static final String KEY_dispo_val = "dispo_code_val";

    //PAY MODE TABLE

    public static final String KEY_pm_id = "pm_id";
    public static final String KEY_pay_mode = "pay_mode ";
    public static final String KEY_pm_flag = "pm_flag";
    public static final String KEY_pm_srno = "pm_srno";
    public static final String KEY_pm_val = "pm_val";

    //PRODUCT TABLE

    public static final String KEY_pd_id = "pd_id";
    public static final String KEY_product = "product";
    public static final String KEY_pd_flag = "pd_flag";
    public static final String KEY_pd_srno = "pd_srno";
    public static final String KEY_pd_val = "pd_val";



    String str_BIZ_type_val = "", str_PROPERTY_type_val = "", str_Current_BIZ_type_val = "",
            str_Not_able_pay_type_val = "", str_Coll_Occupant_type_value = "", str_cust_has_loan = "",
            str_max_emi_amt_capble = "", str_hand_loane_value = "", str_loan_mnth_emi = "",str_settle_amt="";

    String locid = "";
    String customer_name = "";
    String acc_status = "";
    String PDD_Status = "";
    String Data_Entry = "";
    String Visit_Mode = "";
    String Number_Contacted = "";
    String Bucket_Type = "";
    String Met_Spoke = "";
    String DispoCode = "";
    String Next_Follow_Date = "";
    String Remarks = "";
    String New_Contact_Number = "";
    String New_Contact_Address = "";
    String New_Contact_Email = "";
    String Receipt_Number = "";
    String Total_Amount = "";
    String EMI_Amount = "";
    String Other_Amount = "";
    String Mode = "";
    String Cheque_Num = "";
    String PAN_no = "";
    String Form_60 = "";
    String Coll_Sighted = "";
    String Coll_Condition = "";
    String third_Party = "";
    String third_Party_Name = "";
    String third_Party_Address = "";
    String Repo_Doable = "";
    String Photo1 = "";
    String Photo2 = "";
    String Photo3 = "";
    String Photo4 = "";
    String Photo5 = "";
    String Photo6 = "";
    String Photo7 = "";
    String Photo8 = "";
    String Photo9 = "";
    String Photo10 = "";
    String UserID = "";
    String Upload_Date = "";
    String GPS_Latitiude = "";
    String GPS_Longitude = "";
    String ts = "";
    String loan_distance = "";

    String product = "";
    String bom_bkt = "";
    String risk_bkt = "";
    String cbc_due = "";
    String lpp_due = "";

    String str_data_entry="";

    String str_pic_count="";

    String str_ach_status="";

    String str_distance_km="";

    String str_distance_type="";

    String month_e_status = "";
    String month_e_week = "";
String str_map_address="";

    /**
     * A constant, stores the the table name
     */
    private static final String DATABASE_TABLE = "key_master";
    private static final String DATABASE_PICTURE_TABLE = "key_pic_master";
    private static final String DATABASE_DISTANCE_TABLE = "key_distance_master";

    private static final String DATABASE_DISPO_TABLE = "key_dispo_master";
    private static final String DATABASE_PM_TABLE = "key_pm_master";
    private static final String DATABASE_PD_TABLE = "key_pd_master";

    /**
     * An instance variable for SQLiteDatabase
     */
    private SQLiteDatabase mDB;
    Context context;

    /**
     * Constructor
     */
    public RequestDB(Context context) {
        super(context, DBNAME, null, VERSION);
        this.mDB = getWritableDatabase();
        userFunction = new UserFunctions();
        this.context = context;
        cd = new ConnectionDetector(context);

        get_masters=new ArrayList<String>();

        get_flag=new ArrayList<String>();
       arr_dispo_code=new ArrayList<String>();
        arr_dispo_val=new ArrayList<String>();

        arr_pm=new ArrayList<String>();
        arr_pm_val=new ArrayList<String>();



                arr_pd=new ArrayList<String>();
        arr_pd_val=new ArrayList<String>();
    }

    /**
     * This is a callback method, invoked when the method getReadableDatabase()
     * / getWritableDatabase() is called provided the database does not exists

     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE IF NOT EXISTS  " + DATABASE_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key AUTOINCREMENT , "
                + KEY_LOSID + " text, " + KEY_Customer_Name + "  text, "
                + KEY_Account_Status + "  text, " + KEY_PDD_Status + "  text,"
                + KEY_Data_Entry + "  text," + KEY_Visit_Mode + "  text,"
                + KEY_Number_Contacted + "  text," + KEY_Bucket_Type
                + "  text," + KEY_Met_Spoke + "  text," + KEY_DispoCode
                + "  text," + KEY_Next_Follow_Date + "  text," + KEY_Remarks
                + "  text," + KEY_New_Contact_Number + "  text,"
                + KEY_New_Contact_Address + "  text," + KEY_New_Contact_Email
                + "  text," + KEY_Receipt_Number + "  text," + KEY_Total_Amount
                + "  text," + KEY_EMI_Amount + "  text," + KEY_Other_Amount
                + "  text," + KEY_Mode + "  text," + KEY_Cheque_Num + "  text,"
                + KEY_PAN_no + "  text," + KEY_Form_sixty + "  text,"
                + KEY_Coll_Sighted + "  text," + KEY_Coll_Condition + "  text,"
                + KEY_3rd_Party + "  text," + KEY_3rd_Party_Name + "  text,"
                + KEY_3rd_Party_Address + "  text," + KEY_Repo_Doable
                + "  text," + KEY_Photo1 + "  text," + KEY_Photo2 + "  text,"
                + KEY_Photo3 + "  text," + KEY_Photo4 + "  text," + KEY_Photo5
                + "  text," + KEY_Photo6 + "  text" + "," + KEY_Photo7
                + "  text," + KEY_Photo8 + "  text," + KEY_Photo9 + "  text,"
                + KEY_Photo10 + "  text," + KEY_UserID + "  text,"
                + KEY_Upload_Date + "  text," + KEY_GPS_Latitiude + "  text,"
                + KEY_GPS_Longitude + "  text , " + KEY_STATUS + "  text, "
                + KEY_TS + " text," + KEY_LOAN_DISTANCE + " text," + KEY_PRODUCT + " text," +
                KEY_BOM_BKT + " text," + KEY_RISK_BKT + " text," + KEY_LPP_DUE + " text,"
                + KEY_CBC_DUE + " text,"
                + KEY_BIZ_val + " text,"
                + KEY_PROPERTY_val + " text,"
                + KEY_Current_BIZ_val + " text,"
                + KEY_Not_able_pay_type_val + " text,"
                + KEY_Coll_Occupant_type_value + " text,"
                + KEY_cust_has_loan + " text,"
                + KEY_max_emi_amt_capble + " text,"
                + KEY_hand_loane_value + " text,"
                + KEY_loan_mnth_emi + " text, "
                + KEY_settle_amt + " text, "
                + KEY_STATUS1 +" text, "
                +KEY_pic_count+" ,"
                +KEY_ACH_status+" text, "
                +KEY_distance_km+" text,"
                +KEY_distance_type+" text,"
                +KEY_map_address+" )";


        db.execSQL(sql);

        String sql_query = "create table " + DATABASE_PICTURE_TABLE + " ( "
                + KEY_ROW_ID + " integer primary key autoincrement , "
                + KEY_Photo1 + " text , " + KEY_STATUS + " text) ";

        db.execSQL(sql_query);

        String sql_query_new = "create table " + DATABASE_DISTANCE_TABLE
                + " ( " + KEY_ROW_ID + " integer primary key autoincrement , "
                + KEY_LATITUDE_DIST + " text , " + KEY_LONGITUDE_DIST
                + " text, " + KEY_DATETIME_DIST + " text , " + KEY_DIST_KM
                + " text) ";

        System.out.println("sql:::" + sql_query);
        db.execSQL(sql_query_new);

        String sql_dispo_query = "create table " + DATABASE_DISPO_TABLE + " ( "
                + KEY_dispo_srno + " integer primary key autoincrement , "
                + KEY_dispo_code + " text , " + KEY_dispo_val + " text , " +KEY_dispo_id+ " text ,"
                + KEY_dispo_flag+ " text )";

        db.execSQL(sql_dispo_query);

        String sql_pm_query = "create table " + DATABASE_PM_TABLE + " ( "
                + KEY_pm_srno + " integer primary key autoincrement , "
                + KEY_pay_mode + " text , " + KEY_pm_val + " text , "+ KEY_pm_id+ " text, "
                +KEY_pm_flag+ " text )";

        db.execSQL(sql_pm_query);

        String sql_pd_query = "create table " + DATABASE_PD_TABLE + " ( "
                + KEY_pd_srno + " integer primary key autoincrement , "
                + KEY_product + " text , " + KEY_pd_val + " text , "+ KEY_pd_id+ " text, "
                +KEY_pd_flag+ " text )";

        db.execSQL(sql_pd_query);

        //db.close();
    }

    public void closeConnection() {
        mDB.close();
    }

    public long insert_photo(String path, SQLiteDatabase db) {

        if (path != "") {
            ContentValues values = new ContentValues();
            values.put(KEY_Photo1, path);
            values.put(KEY_STATUS, "0");
            long lid = db.insert(DATABASE_PICTURE_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public void insert_distance(String lattitude, String longitude,
                                String distance, String date) {
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            if (distance != "") {
                String sql = "insert into " + DATABASE_DISTANCE_TABLE + " ( "
                        + KEY_LATITUDE_DIST + "," + KEY_LONGITUDE_DIST + ","
                        + KEY_DATETIME_DIST + "," + KEY_DIST_KM + " ) " + " "
                        + "values ('" + lattitude + "','" + longitude + "','"
                        + date + "','" + distance + "')";
                System.out.println("sqldfdfdf:::" + sql);
                db.execSQL(sql);
                db.close();
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }

    }

    public String getphoto() {

        SQLiteDatabase db = this.getReadableDatabase();
        String image_path = null;

        String countQuery = "SELECT " + KEY_Photo1 + "  FROM "
                + DATABASE_PICTURE_TABLE + " WHERE " + KEY_STATUS
                + "= 0 LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    image_path = cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }
        return image_path;
        // System.out.println("countQuery:"+product_qnt);
    }

    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_Photo1 + ") FROM "
                + DATABASE_PICTURE_TABLE + " WHERE " + KEY_STATUS
                + "= 1 LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }


    public ArrayList<String> get_masters() {

        SQLiteDatabase db = this.getReadableDatabase();

        String count_dispo = "SELECT COUNT (" + KEY_dispo_code + ") FROM "
                + DATABASE_DISPO_TABLE ;
        Cursor cursor = db.rawQuery(count_dispo, null);

        String count_pm = "SELECT COUNT (" + KEY_pay_mode + ") FROM "
                + DATABASE_PM_TABLE ;
        Cursor cursor1 = db.rawQuery(count_pm, null);

        String count_pd = "SELECT COUNT (" + KEY_product + ") FROM "
                + DATABASE_PD_TABLE ;
        Cursor cursor2 = db.rawQuery(count_pd, null);

        System.out.println("countQuery:" + cursor);
        String dispo_count = "";
        String pm_count = "";
        String pd_count = "";

        try {
            if (cursor != null || cursor1!=null || cursor2!=null) {

                if (cursor.moveToNext()) {
                    dispo_count = cursor.getString(0);

                }
                cursor.close();

                if (cursor1.moveToNext()) {
                    pm_count = cursor1.getString(0);

                }
                cursor1.close();

                if (cursor2.moveToNext()) {
                    pd_count = cursor2.getString(0);

                }
                cursor2.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

       get_masters.add(dispo_count);
        get_masters.add(pm_count);
        get_masters.add(pd_count);


        System.out.println("DISPO CNT:: " + dispo_count + " PM COUNT::" + pm_count);


        return get_masters;
    }

    public void delete_dist_All() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_DISTANCE_TABLE, null, null);
        db.close();
    }

    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_PICTURE_TABLE, KEY_Photo1 + "=" + " '"
                    + path + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public boolean deleterequest(String losid) {
        Log.d("DELETING", losid);
        boolean chkFlag = false;
        try {

            System.out.println("DELETED LOS::::");
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(DATABASE_TABLE, KEY_LOSID + "=" + " '" + losid
                    + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        // return true;
        return chkFlag;
    }

    public long insert(String locid, String customer_name,
                       String acc_status, String PDD_Status, String Data_Entry,
                       String Visit_Mode, String Number_Contacted, String Bucket_Type,
                       String Met_Spoke, String DispoCode, String Next_Follow_Date,
                       String Remarks, String New_Contact_Number,
                       String New_Contact_Address, String New_Contact_Email,
                       String Receipt_Number, String Total_Amount, String EMI_Amount,
                       String Other_Amount, String Mode, String Cheque_Num, String PAN_no,
                       String Form_60, String Coll_Sighted, String Coll_Condition,
                       String third_Party, String third_Party_Name,
                       String third_Party_Address, String Repo_Doable, String UserID, String Upload_Date,
                       String GPS_Latitiude, String GPS_Longitude, String ts,
                       String loan_distance,
                       String product, String bom_bkt, String risk_bkt, String cbc_due, String lpp_due,
                       String str_BIZ_type_val, String str_PROPERTY_type_val, String str_Current_BIZ_type_val,
                       String str_Not_able_pay_type_val, String str_Coll_Occupant_type_value,
                       String str_cust_has_loan, String str_max_emi_amt_capble, String str_hand_loane_value,
                       String str_loan_mnth_emi,String str_settle_amt, String entry_status,String str_ach_status,String str_distance, String str_distance_type,String map_address) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_LOSID, locid);
        contentValues.put(KEY_Customer_Name, customer_name);
        contentValues.put(KEY_Account_Status, acc_status);
        contentValues.put(KEY_PDD_Status, PDD_Status);
        contentValues.put(KEY_Data_Entry, Data_Entry);
        contentValues.put(KEY_Visit_Mode, Visit_Mode);
        contentValues.put(KEY_Number_Contacted, Number_Contacted);
        contentValues.put(KEY_Bucket_Type, Bucket_Type);
        contentValues.put(KEY_Met_Spoke, Met_Spoke);
        contentValues.put(KEY_DispoCode, DispoCode);
        contentValues.put(KEY_Next_Follow_Date, Next_Follow_Date);
        contentValues.put(KEY_Remarks, Remarks);
        contentValues.put(KEY_New_Contact_Number, New_Contact_Number);
        contentValues.put(KEY_New_Contact_Address, New_Contact_Address);
        contentValues.put(KEY_New_Contact_Email, New_Contact_Email);
        contentValues.put(KEY_Receipt_Number, Receipt_Number);
        contentValues.put(KEY_Total_Amount, Total_Amount);
        contentValues.put(KEY_EMI_Amount, EMI_Amount);
        contentValues.put(KEY_Other_Amount, Other_Amount);
        contentValues.put(KEY_Mode, Mode);
        contentValues.put(KEY_Cheque_Num, Cheque_Num);
        contentValues.put(KEY_PAN_no, PAN_no);
        contentValues.put(KEY_Form_sixty, Form_60);
        contentValues.put(KEY_Coll_Sighted, Coll_Sighted);
        contentValues.put(KEY_Coll_Condition, Coll_Condition);
        contentValues.put(KEY_3rd_Party, third_Party);
        contentValues.put(KEY_3rd_Party_Name, third_Party_Name);
        contentValues.put(KEY_3rd_Party_Address, third_Party_Address);
        contentValues.put(KEY_Repo_Doable, Repo_Doable);
        contentValues.put(KEY_UserID, UserID);
        contentValues.put(KEY_Upload_Date, Upload_Date);
        contentValues.put(KEY_GPS_Latitiude, GPS_Latitiude);
        contentValues.put(KEY_GPS_Longitude, GPS_Longitude);
        contentValues.put(KEY_TS, ts);
        contentValues.put(KEY_LOAN_DISTANCE, loan_distance);
        contentValues.put(KEY_Photo1, Photo1);
        contentValues.put(KEY_Photo2, Photo2);
        contentValues.put(KEY_Photo3, Photo3);
        contentValues.put(KEY_Photo4, Photo4);
        contentValues.put(KEY_Photo5, Photo5);
        contentValues.put(KEY_Photo6, Photo6);
        contentValues.put(KEY_Photo7, Photo7);
        contentValues.put(KEY_Photo8, Photo8);
        contentValues.put(KEY_Photo9, Photo9);
        contentValues.put(KEY_Photo10, Photo10);
        contentValues.put(KEY_STATUS, "0");

        contentValues.put(KEY_PRODUCT, product);
        contentValues.put(KEY_BOM_BKT, bom_bkt);
        contentValues.put(KEY_RISK_BKT, risk_bkt);
        contentValues.put(KEY_CBC_DUE, cbc_due);
        contentValues.put(KEY_LPP_DUE, lpp_due);

        contentValues.put(KEY_BIZ_val, str_BIZ_type_val);


        contentValues.put(KEY_PROPERTY_val, str_PROPERTY_type_val);
        contentValues.put(KEY_Current_BIZ_val, str_Current_BIZ_type_val);
        contentValues.put(KEY_Not_able_pay_type_val, str_Not_able_pay_type_val);
        contentValues.put(KEY_Coll_Occupant_type_value, str_Coll_Occupant_type_value);
        contentValues.put(KEY_cust_has_loan, str_cust_has_loan);
        contentValues.put(KEY_max_emi_amt_capble, str_max_emi_amt_capble);
        contentValues.put(KEY_hand_loane_value, str_hand_loane_value);
        contentValues.put(KEY_loan_mnth_emi, str_loan_mnth_emi);
        contentValues.put(KEY_settle_amt, str_settle_amt);

        contentValues.put(KEY_STATUS1, entry_status);

        contentValues.put(KEY_ACH_status, str_ach_status);

        contentValues.put(KEY_distance_km, str_distance);

        contentValues.put(KEY_distance_type, str_distance_type);
        contentValues.put(KEY_map_address, map_address);



        if (mDB.isOpen()) {
            mDB.close();
        } else {
            mDB = this.getWritableDatabase();

        }



        return mDB.insert(DATABASE_TABLE, null, contentValues);
    }

    public String getPendingCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_LOSID + ") FROM "
                + DATABASE_TABLE + " WHERE " + KEY_STATUS + "= 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);
        String pending_request_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getString(0);
                    return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_request_count;
    }

    public String getrequest() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT *  FROM " + DATABASE_TABLE + " WHERE "
                + KEY_STATUS + "= 0 LIMIT 1";
        Cursor cursor = db.rawQuery(countQuery, null);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    locid = cursor.getString(1);
                    customer_name = cursor.getString(2);
                    acc_status = cursor.getString(3);
                    PDD_Status = cursor.getString(4);
                    Data_Entry = cursor.getString(5);
                    Visit_Mode = cursor.getString(6);
                    Number_Contacted = cursor.getString(7);
                    Bucket_Type = cursor.getString(8);
                    Met_Spoke = cursor.getString(9);
                    DispoCode = cursor.getString(10);
                    Next_Follow_Date = cursor.getString(11);
                    Remarks = cursor.getString(12);
                    New_Contact_Number = cursor.getString(13);
                    New_Contact_Address = cursor.getString(14);
                    New_Contact_Email = cursor.getString(15);
                    Receipt_Number = cursor.getString(16);
                    Total_Amount = cursor.getString(17);
                    EMI_Amount = cursor.getString(18);
                    Other_Amount = cursor.getString(19);
                    Mode = cursor.getString(20);
                    Cheque_Num = cursor.getString(21);
                    PAN_no = cursor.getString(22);
                    Form_60 = cursor.getString(23);
                    Coll_Sighted = cursor.getString(24);
                    Coll_Condition = cursor.getString(25);
                    third_Party = cursor.getString(26);
                    third_Party_Name = cursor.getString(27);
                    third_Party_Address = cursor.getString(28);
                    Repo_Doable = cursor.getString(29);
                    UserID = cursor.getString(40);
                    Upload_Date = cursor.getString(41);
                    GPS_Latitiude = cursor.getString(42);
                    GPS_Longitude = cursor.getString(43);
                    ts = cursor.getString(45);
                    loan_distance = cursor.getString(46);

                    product = cursor.getString(47);
                    bom_bkt = cursor.getString(48);
                    risk_bkt = cursor.getString(49);
                    lpp_due = cursor.getString(50);
                    cbc_due = cursor.getString(51);


                    str_BIZ_type_val = cursor.getString(52);
                    str_PROPERTY_type_val = cursor.getString(53);
                    str_Current_BIZ_type_val = cursor.getString(54);
                    str_Not_able_pay_type_val = cursor.getString(55);
                    str_Coll_Occupant_type_value = cursor.getString(56);
                    str_cust_has_loan = cursor.getString(57);
                    str_max_emi_amt_capble = cursor.getString(58);
                    str_hand_loane_value = cursor.getString(59);
                    str_loan_mnth_emi = cursor.getString(60);
                    str_map_address= cursor.getString(61);
                }
                cursor.close();

                if (locid.equals("")) {
                } else {
                    if (cd.isConnectingToInternet()) {
                        new InsertData().execute();
                    } else {
                        //userFunction.cutomToast("you have poor data network....  ", context);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    @Override
    public void onUpgrade(SQLiteDatabase DB, int arg1, int arg2) {
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_PICTURE_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_DISTANCE_TABLE);
        DB.execSQL("DROP TABLE IF EXISTS " + DATABASE_DISPO_TABLE);
        // Create tables again
        onCreate(DB);

    }

    public Cursor getAllCustomers() {
        String sql = "select * FROM " + DATABASE_TABLE;
        System.out.println("sql:FG::::::::" + sql);

        ArrayList<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
//		String selectQuery = "SELECT  " + KEY_ROW_ID + "," + KEY_Data_Entry
//				+ "," + KEY_LOSID + "  FROM  " + DATABASE_TABLE;

        String selectQuery = "SELECT  " + KEY_ROW_ID + "," + KEY_DIST_KM
                + "," + KEY_LATITUDE_DIST + "," + KEY_LONGITUDE_DIST + "  FROM  " + DATABASE_DISTANCE_TABLE;
        System.out.println("Query::::" + selectQuery);


        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = null;
        try {
            db.beginTransaction();
            c = db.rawQuery(selectQuery, null);
            // Move to first row
            c.moveToFirst();
            Log.d("Events", String.valueOf(c.getCount()));
            if (c.getCount() > 0) {
                do {
                    HashMap<String, String> rowList = new HashMap<String, String>();
                    rowList.put(KEY_ROW_ID, c.getString(0));
                    rowList.put(KEY_PDD_Status, c.getString(1));
                    rowList.put(KEY_LOSID, c.getString(2));
                    dataList.add(rowList);
                } while (c.moveToNext());

            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            // c.close();
            // db.close();
        }

        return mDB.query(DATABASE_TABLE, new String[]{KEY_ROW_ID, KEY_LOSID,
                        KEY_PDD_Status, KEY_PDD_Status, KEY_PDD_Status}, null, null,
                null, null, KEY_ROW_ID + " asc ");

    }

    public class InsertData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            jsonobject = userFunction.InsertData(locid, customer_name,
                    acc_status, PDD_Status, Data_Entry, Visit_Mode,
                    Number_Contacted, Bucket_Type, Met_Spoke, DispoCode,
                    Next_Follow_Date, Remarks, New_Contact_Number,
                    New_Contact_Address, New_Contact_Email, Receipt_Number,
                    Total_Amount, EMI_Amount, Other_Amount, Mode, Cheque_Num,
                    PAN_no, Form_60, Coll_Sighted, Coll_Condition, third_Party,
                    third_Party_Name, third_Party_Address, Repo_Doable, Photo1,
                    Photo2, Photo3, Photo4, Photo5, Photo6, Photo7, Photo8,
                    Photo9, Photo10, UserID, Upload_Date, GPS_Latitiude,
                    GPS_Longitude, ts, loan_distance,
                    product, bom_bkt, risk_bkt, lpp_due, cbc_due,
                    str_BIZ_type_val, str_PROPERTY_type_val,
                    str_Current_BIZ_type_val, str_Not_able_pay_type_val,
                    str_Coll_Occupant_type_value, str_cust_has_loan,
                    str_max_emi_amt_capble, str_hand_loane_value, str_loan_mnth_emi,str_settle_amt,"ON",str_pic_count,str_ach_status,str_distance_km,str_distance_type,str_map_address);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            System.out.println("SDSDSDSD:::" + res);
                            deleterequest(locid);
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }



    public Integer getDistanceCount(String currentdate) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT COUNT (" + KEY_DIST_KM + ") FROM "
                + DATABASE_DISTANCE_TABLE + " WHERE " + KEY_DATETIME_DIST
                + "= '" + currentdate + "' ";
        Cursor cursor = db.rawQuery(countQuery, null);

        Integer pending_request_count = 0;
        System.out.println("countQueryuui:" + countQuery);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_request_count = cursor.getInt(0);
                    // return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        System.out.println("countQueryuuierer:" + pending_request_count);

        return pending_request_count;

    }

    public void update_by_date(String datetime, String str_lat,
                               String str_long, String str_distance, String current_date) {
        SQLiteDatabase db = this.getReadableDatabase();

        String sqll = "UPDATE " + DATABASE_DISTANCE_TABLE + " SET " + KEY_DIST_KM + "= '" + str_distance + "' , " + KEY_LATITUDE_DIST + "= '" + str_lat + "', " + KEY_LONGITUDE_DIST + "= '" + str_long + "' " +
                "" + " WHERE " + KEY_DATETIME_DIST + "= '" + current_date + "'  ";

        Cursor cursor = db.rawQuery(sqll, null);

        System.out.println("countQueryuui:" + sqll);


        try {
            if (cursor != null) {


                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

    }

    public void insert_distance() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_DISTANCE_TABLE, null, null);
        db.close();
    }


    public String get_Lat_Long(String currentdate) {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery = "SELECT " + KEY_LATITUDE_DIST + ","
                + KEY_LONGITUDE_DIST + " FROM " + DATABASE_DISTANCE_TABLE
                + " WHERE " + KEY_DATETIME_DIST + "= '" + currentdate + "' ORDER BY " + KEY_DATETIME_DIST + " DESC LIMIT 1";

        System.out.println("countQueryuui:" + countQuery);

        Cursor cursor = db.rawQuery(countQuery, null);

        String strlatitude = null;
        String strlongitude = null;


        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    strlatitude = cursor.getString(0);
                    strlongitude = cursor.getString(1);

                    // return cursor.getString(0);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        System.out.println("countQueryuuierer:" + strlatitude
                + ":::strlongitude:::" + strlongitude);
        strlatitude = strlatitude + "," + strlongitude;

        return strlatitude;

    }

    @Override
    public void run() {

    }

    public String getrequestForDashboard(String ts1,String entry_type ) {

        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery;

        if(ts1!=null) {

            countQuery = "SELECT *  FROM " + DATABASE_TABLE + " WHERE "
                    + KEY_STATUS + "= 1  AND "+KEY_TS+ " = "+"'"+ts1+"' LIMIT 1";
        }
        else{


             countQuery = "SELECT *  FROM " + DATABASE_TABLE + " WHERE "
                    + KEY_STATUS + "= 1 LIMIT 1";

        }

        Cursor cursor = db.rawQuery(countQuery, null);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    locid = cursor.getString(1);
                    customer_name = cursor.getString(2);
                    acc_status = cursor.getString(3);
                    PDD_Status = cursor.getString(4);
                    Data_Entry = cursor.getString(5);
                    Visit_Mode = cursor.getString(6);
                    Number_Contacted = cursor.getString(7);
                    Bucket_Type = cursor.getString(8);
                    Met_Spoke = cursor.getString(9);
                    DispoCode = cursor.getString(10);
                    Next_Follow_Date = cursor.getString(11);
                    Remarks = cursor.getString(12);
                    New_Contact_Number = cursor.getString(13);
                    New_Contact_Address = cursor.getString(14);
                    New_Contact_Email = cursor.getString(15);
                    Receipt_Number = cursor.getString(16);
                    Total_Amount = cursor.getString(17);
                    EMI_Amount = cursor.getString(18);
                    Other_Amount = cursor.getString(19);
                    Mode = cursor.getString(20);
                    Cheque_Num = cursor.getString(21);
                    PAN_no = cursor.getString(22);
                    Form_60 = cursor.getString(23);
                    Coll_Sighted = cursor.getString(24);
                    Coll_Condition = cursor.getString(25);
                    third_Party = cursor.getString(26);
                    third_Party_Name = cursor.getString(27);
                    third_Party_Address = cursor.getString(28);
                    Repo_Doable = cursor.getString(29);
                    UserID = cursor.getString(40);
                    Upload_Date = cursor.getString(41);
                    GPS_Latitiude = cursor.getString(42);
                    GPS_Longitude = cursor.getString(43);
                    ts = cursor.getString(45);
                    loan_distance = cursor.getString(46);
                    product = cursor.getString(47);
                    bom_bkt = cursor.getString(48);
                    risk_bkt = cursor.getString(49);
                    lpp_due = cursor.getString(50);
                    cbc_due = cursor.getString(51);

                    str_BIZ_type_val = cursor.getString(52);
                    str_PROPERTY_type_val = cursor.getString(53);
                    str_Current_BIZ_type_val = cursor.getString(54);
                    str_Not_able_pay_type_val = cursor.getString(55);
                    str_Coll_Occupant_type_value = cursor.getString(56);
                    str_cust_has_loan = cursor.getString(57);
                    str_max_emi_amt_capble = cursor.getString(58);
                    str_hand_loane_value = cursor.getString(59);
                    str_loan_mnth_emi = cursor.getString(60);
                    str_settle_amt = cursor.getString(61);

                    str_pic_count = cursor.getString(63);

                    str_data_entry=entry_type;

                    str_ach_status=cursor.getString(64);

                    str_distance_km=cursor.getString(65);

                    str_distance_type=cursor.getString(66);
                    str_map_address=cursor.getString(67);
                }
                cursor.close();

                if (locid.equals("")) {
                } else {
                    if (cd.isConnectingToInternet()) {
                        new InsertDataForDashboard().execute(str_data_entry,str_pic_count);

                    } else {
                        userFunction.cutomToast("you have poor data network....  ", context);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return null;
        // System.out.println("countQuery:"+product_qnt);
    }

    public long insert_dispo_codes(String dispo_code,String dispo_code_val,String dispo_flag,String id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (dispo_code != "" && dispo_code_val!= "" && dispo_flag!="") {
            ContentValues values = new ContentValues();
            values.put(KEY_dispo_code, dispo_code);
            values.put(KEY_dispo_val, dispo_code_val);
            values.put(KEY_dispo_id,id );
            values.put(KEY_dispo_flag, dispo_flag);
            long lid = db.insert(DATABASE_DISPO_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public ArrayList<String> get_master_flag() {

        SQLiteDatabase db = this.getReadableDatabase();
        String dispo_flag = null;
        String dispo_id=null;

        String pm_flag = null;
        String pm_id = null;

        String pd_flag = null;
        String pd_id = null;

        String countQuery = "SELECT " + KEY_dispo_flag +","+KEY_dispo_id +" FROM "
                + DATABASE_DISPO_TABLE+ " LIMIT 1" ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    dispo_flag = cursor.getString(0);
                    dispo_id = cursor.getString(1);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        SQLiteDatabase db1 = this.getReadableDatabase();

        String countQuery1 = "SELECT " + KEY_pm_flag + " , "+KEY_pm_id+ "  FROM "
                + DATABASE_PM_TABLE+" LIMIT 1" ;
        Cursor cursor1 = db1.rawQuery(countQuery1, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor1 != null) {

                if (cursor1.moveToNext()) {
                    pm_flag = cursor1.getString(0);
                    pm_id = cursor1.getString(1);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db1.close();
        }

        SQLiteDatabase db2 = this.getReadableDatabase();

        String countQuery2 = "SELECT " + KEY_pd_flag + " , "+KEY_pd_id+ "  FROM "
                + DATABASE_PD_TABLE+" LIMIT 1" ;
        Cursor cursor2 = db2.rawQuery(countQuery2, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor2 != null) {

                if (cursor2.moveToNext()) {
                    pd_flag = cursor2.getString(0);
                    pd_id = cursor2.getString(1);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db2.close();
        }

        get_flag.add(dispo_flag);
        get_flag.add(dispo_id);
        get_flag.add(pm_flag);
        get_flag.add(pm_id);
        get_flag.add(pd_flag);
        get_flag.add(pd_id);

        return get_flag;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_dispo() {

        SQLiteDatabase db = this.getReadableDatabase();
        String dispo_code = null;
        String dispo_val=null;

        arr_dispo_code.add("Select DispoCode *");


        String countQuery = "SELECT " + KEY_dispo_code +" FROM "
                + DATABASE_DISPO_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

            while(  cursor.moveToNext()) {
                    dispo_code = cursor.getString(0);

                    arr_dispo_code.add(dispo_code);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_dispo_code;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_dispo_val() {

        SQLiteDatabase db = this.getReadableDatabase();

        String dispo_val=null;

        arr_dispo_val.add("");

        String countQuery = "SELECT " + KEY_dispo_val +" FROM "
                    + DATABASE_DISPO_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                 while(cursor.moveToNext()) {
                    dispo_val = cursor.getString(0);


                    arr_dispo_val.add(dispo_val);
                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        return arr_dispo_val;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_pm() {

        SQLiteDatabase db = this.getReadableDatabase();



        String pay_mode = null;


        arr_pm.add("Select Mode *");

        String countQuery = "SELECT " + KEY_pay_mode  +" FROM "
                + DATABASE_PM_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pay_mode = cursor.getString(0);

                    arr_pm.add(pay_mode);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_pm;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_pm_val() {

        SQLiteDatabase db = this.getReadableDatabase();



        String pay_mode_val = null;

        arr_pm_val.add("");

        String countQuery = "SELECT " + KEY_pm_val  +" FROM "
                + DATABASE_PM_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pay_mode_val = cursor.getString(0);

                    arr_pm_val.add(pay_mode_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }

        System.out.println("PM val::"+arr_pm_val);

        return arr_pm_val;
        // System.out.println("countQuery:"+product_qnt);
    }


    public ArrayList<String> get_masters_pd() {

        SQLiteDatabase db = this.getReadableDatabase();

        String product = null;


        arr_pd.add("Select Product *");

        String countQuery = "SELECT " + KEY_product  +" FROM "
                + DATABASE_PD_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    product = cursor.getString(0);

                    arr_pd.add(product);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_pd;
        // System.out.println("countQuery:"+product_qnt);
    }

    public ArrayList<String> get_masters_pd_val() {

        SQLiteDatabase db = this.getReadableDatabase();



        String pd_val = null;

        arr_pd_val.add("");

        String countQuery = "SELECT " + KEY_pd_val  +" FROM "
                + DATABASE_PD_TABLE ;
        Cursor cursor = db.rawQuery(countQuery, null);

        System.out.println("countQuery:" + cursor);

        try {
            if (cursor != null) {

                while (cursor.moveToNext()) {
                    pd_val = cursor.getString(0);

                    arr_pd_val.add(pd_val);

                }
                cursor.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            db.close();
        }


        return arr_pd_val;
        // System.out.println("countQuery:"+product_qnt);
    }


    public long insert_pm(String pm,String pm_val,String flag,String id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (pm != "" && pm_val!= "" ) {
            ContentValues values = new ContentValues();
            values.put(KEY_pay_mode, pm);
            values.put(KEY_pm_val, pm_val);
            values.put(KEY_pm_id, id);
            values.put(KEY_pm_flag, flag);
            long lid = db.insert(DATABASE_PM_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;

        }

    }

    public long insert_prod(String prod,String pd_val,String pd_flag,String pd_id) {

        SQLiteDatabase db = this.getWritableDatabase();

        if (prod != "" && pd_val!= "" ) {
            ContentValues values = new ContentValues();
            values.put(KEY_product, prod);
            values.put(KEY_pd_val, pd_val);
            values.put(KEY_pd_flag, pd_flag);
            values.put(KEY_pd_id, pd_id);
            long lid = db.insert(DATABASE_PD_TABLE, null, values);
            //db.close();
            // Inserting Row
            return lid;
        } else {
            return 0;
        }

    }

    public long update_status(String status,String pic_count,String ts1) {
        long res;

/*
        mDB = this.getReadableDatabase();
        ContentValues args=new ContentValues();
        args.put(KEY_STATUS,status);
        args.put(KEY_pic_count,pic_count);
        Boolean res=false;

        res=  mDB.update(DATABASE_TABLE, args, null, null) > 0;

        return  res;

*/

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_STATUS, status);
        contentValues.put(KEY_pic_count, pic_count);
        res= db.update(DATABASE_TABLE, contentValues, KEY_TS + " = ? ", new String[]{ts1});
        return res;


    }


    public void delete_dispo_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_DISPO_TABLE, null, null);
        db.close();
    }

    public void delete_pm_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_PM_TABLE, null, null);
        db.close();
    }

    public void delete_pd_table() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(DATABASE_PD_TABLE, null, null);
        db.close();
    }

    public class InsertDataForDashboard extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected String  doInBackground(String... params) {
            // then do your work
            String str_entry_type = params[0];
            String str_pic_count = params[1];

            jsonobject = userFunction.InsertData(locid, customer_name,
                    acc_status, PDD_Status, Data_Entry, Visit_Mode,
                    Number_Contacted, Bucket_Type, Met_Spoke, DispoCode,
                    Next_Follow_Date, Remarks, New_Contact_Number,
                    New_Contact_Address, New_Contact_Email, Receipt_Number,
                    Total_Amount, EMI_Amount, Other_Amount, Mode, Cheque_Num,
                    PAN_no, Form_60, Coll_Sighted, Coll_Condition, third_Party,
                    third_Party_Name, third_Party_Address, Repo_Doable, Photo1,
                    Photo2, Photo3, Photo4, Photo5, Photo6, Photo7, Photo8,
                    Photo9, Photo10, UserID, Upload_Date, GPS_Latitiude,
                    GPS_Longitude, ts, loan_distance,
                    product, bom_bkt, risk_bkt, lpp_due, cbc_due,

                    str_BIZ_type_val, str_PROPERTY_type_val,
                    str_Current_BIZ_type_val, str_Not_able_pay_type_val,
                    str_Coll_Occupant_type_value, str_cust_has_loan,
                    str_max_emi_amt_capble, str_hand_loane_value, str_loan_mnth_emi,str_settle_amt,str_entry_type,str_pic_count,str_ach_status,str_distance_km,str_distance_type,str_map_address);

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            System.out.print("HEHEHEH");

                            deleterequest(locid);

                            Intent messagingActivity = new Intent(context,
                                    Dashboard.class);
                            context.startActivity(messagingActivity);

                        }
                    }
                }else{
                    mProgressDialog.dismiss();

                    userFunction
                            .cutomToast("Something gets wrong with connectivity..", context);

                    Intent dashActivity = new Intent(context,
                            Dashboard.class);

                    context.startActivity(dashActivity);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class DeleteDashboard extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage("Uploading please wait..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work

            jsonobject = userFunction.InsertData(locid, customer_name,
                    acc_status, PDD_Status, Data_Entry, Visit_Mode,
                    Number_Contacted, Bucket_Type, Met_Spoke, DispoCode,
                    Next_Follow_Date, Remarks, New_Contact_Number,
                    New_Contact_Address, New_Contact_Email, Receipt_Number,
                    Total_Amount, EMI_Amount, Other_Amount, Mode, Cheque_Num,
                    PAN_no, Form_60, Coll_Sighted, Coll_Condition, third_Party,
                    third_Party_Name, third_Party_Address, Repo_Doable, Photo1,
                    Photo2, Photo3, Photo4, Photo5, Photo6, Photo7, Photo8,
                    Photo9, Photo10, UserID, Upload_Date, GPS_Latitiude,
                    GPS_Longitude, ts, loan_distance,
                    product, bom_bkt, risk_bkt, lpp_due, cbc_due,
                    str_BIZ_type_val, str_PROPERTY_type_val,
                    str_Current_BIZ_type_val, str_Not_able_pay_type_val,
                    str_Coll_Occupant_type_value, str_cust_has_loan,
                    str_max_emi_amt_capble, str_hand_loane_value, str_loan_mnth_emi,str_settle_amt,str_data_entry,str_pic_count,str_ach_status,str_distance_km,str_distance_type,str_distance_type);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            deleterequest(locid);

                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            mProgressDialog.dismiss();
        }
    }

    public void delete_data() {


        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + DATABASE_TABLE + " where " + KEY_STATUS1 + "= 0";
        System.out.println("status_co::::del_img:::"+del_img);

        db.execSQL(del_img);

        db.close();

    }

}
