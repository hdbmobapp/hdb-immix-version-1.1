package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;


public class Remarks_Display extends Activity {

    Button btn_submit;
    Button btn_home;
    Button btn_dataentry;
    EditText edit_search;
    ListView lv;
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;

    public ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    TextView txt1, txt2, txt3, txt_losid, txt_customer_name, txt_branch_name;

    String str_txt1, str_txt2, str_txt3, str_customer_name, str_branch_name;
    static String REMARK = "Remark";
    static String REMARK_BY = "Remark_By";
    static String REMARK_DATE = "Remark_Date";
    static  String REMARK_BY_ID="Remark_By_ID";
    static  String PARENT_ID="Parent_Id";
    static String REMARK_ID="Remark_Id";

    AllocationListAdapter adapter;
    ProgressDialog mProgressDialog;
    TextView txt_error;
    SharedPreferences pref;
    String str_user_id = null;
    String str_table_name = null;
    static String str_losid=null;
    String str_parent_id;
    String page="a";
    String str_losid1;
    @Override

    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.remarks_display);
        userFunction = new UserFunctions();

        Intent ii = getIntent();
        str_losid = ii.getStringExtra("losid");
        str_parent_id = ii.getStringExtra("parent_id");
        str_losid1=ii.getStringExtra("losid1");



        System.out.println("str_losid::::"+str_losid);




        SetLayout();
        // txt_losid.setText(str_losid);





        /*Intent remark_adp = new Intent(this,
                RemarksListAdapter.class);
        remark_adp.putExtra("losid", str_losid);
        startActivity(remark_adp);*/

        System.out.println("MNMNM::::" + str_losid);

        btn_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent home = new Intent(com.example.hdb.Remarks_Display.this,
                        HdbHome.class);
                startActivity(home);
            }
        });

        btn_dataentry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent dataentry = new Intent(com.example.hdb.Remarks_Display.this,
                        EditRequest.class);
                startActivity(dataentry);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent new_remark = new Intent(com.example.hdb.Remarks_Display.this,
                        New_Remark.class);
                new_remark.putExtra("losid", str_losid);

                new_remark.putExtra("parentid", str_parent_id);

                new_remark.putExtra("pageid",page);

                 startActivity(new_remark);
            }
        });


}



    protected void SetLayout() {
        btn_submit = (Button) findViewById(R.id.btn_new_remark);
        btn_home = (Button) findViewById(R.id.txt_home);
        btn_dataentry = (Button) findViewById(R.id.txt_data_entry);
        txt_losid = (TextView) findViewById(R.id.txt_losid);
        txt_customer_name = (TextView) findViewById(R.id.txt_customer_name);
        txt_branch_name = (TextView) findViewById(R.id.txt_branch_name);

        lv = (ListView) findViewById(R.id.listView);

        arraylist = new ArrayList<HashMap<String, String>>();

        if(str_losid1==null) {
            new Downloadremarks().execute();

        }
        if ((str_losid==null)) {
            new Downloadremarks1().execute();

        }


       /* if (cd.isConnectingToInternet()) {
            new Remarks_Display.execute();
        } else {
            // toast for internet conncection
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);
        }*/

        //txt_user_id.setText(str_user_id);
        //txt_user_name.setText(str_user_name);
        //txt_branch.setText(str_branch);

        // txt_user_id.setTypeface(font);
        // txt_user_n)ame.setTypeface(font);
        // txt_branch.setTypeface(font);

    }

    public class Downloadremarks extends AsyncTask<Void, Void, Void>
    {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Remarks_Display.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {

    //fetching extra data passed with intents in a Bundle type variable

            if (str_losid.toString().equals("null")) {

                userFunction.cutomToast("Please enter LOSID...",
                        getApplicationContext());
            } else
            {


                System.out.println("LOSID="+str_losid+"PARENTID="+str_parent_id);
                jsonobject = userFunction.getRemarks(str_losid, str_parent_id);

                if (jsonobject != null) {
                    try {
                        // Locate the array name in JSON
                        jsonarray = jsonobject.getJSONArray("Remarks");
//                    txt_error.setVisibility(View.GONE);

                        for (int i = 0; i < jsonarray.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();

                            jsonobject = jsonarray.getJSONObject(i);

                            map.put("Remark", jsonobject.getString("Remark"));
                            map.put("Remark_By", jsonobject.getString("Emp_name"));
                            map.put("Remark_Date", jsonobject.getString("Remark_Date"));
                            map.put("Remark_By_ID",jsonobject.getString("Remark_By"));
                            str_losid = jsonobject.getString("Loan_No");
                            str_customer_name = jsonobject.getString("Customer_Name");
                            str_branch_name = jsonobject.getString("Branch_Name");
                          //  map.put("Parent_Id",jsonobject.getString("Parent_Id"));
                            //map.put("Remark_Id",jsonobject.getString("Remark_Id"));

                            // Set the JSON Objects into the array
                            arraylist.add(map);

                        }
                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                    }
                    //e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast("Something went wrong. Please try after sometime",
                        getApplicationContext());
                finish();
            }
           /* else {
                if (jsonarray.length() <= 0) {
                    userFunction.cutomToast("Enter Valid LOSID",
                            getApplicationContext());
                    finish();

                } */
            else {
                    txt_losid.setText(str_losid);
                    txt_customer_name.setText(str_customer_name);
                    txt_branch_name.setText(str_branch_name);

                    RemarksListAdapter adapter = new RemarksListAdapter(com.example.hdb.Remarks_Display.this, arraylist);
                    lv.setAdapter(adapter);

                    mProgressDialog.dismiss();
              }



        }
    }

    public class Downloadremarks1 extends AsyncTask<Void, Void, Void>
    {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Remarks_Display.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params)
        {

            //fetching extra data passed with intents in a Bundle type variable



                jsonobject = userFunction.getRemarks(str_losid1, str_parent_id);


                if (jsonobject != null) {
                    try {
                        // Locate the array name in JSON
                        jsonarray = jsonobject.getJSONArray("Remarks");
//                    txt_error.setVisibility(View.GONE);

                        for (int i = 0; i < jsonarray.length(); i++) {
                            HashMap<String, String> map = new HashMap<String, String>();

                            jsonobject = jsonarray.getJSONObject(i);

                            map.put("Remark", jsonobject.getString("Remark"));
                            map.put("Remark_By", jsonobject.getString("Emp_name"));
                            map.put("Remark_Date", jsonobject.getString("Remark_Date"));
                            map.put("Remark_By_ID",jsonobject.getString("Remark_By"));
                            str_losid = jsonobject.getString("Loan_No");
                            str_customer_name = jsonobject.getString("Customer_Name");
                            str_branch_name = jsonobject.getString("Branch_Name");
                            map.put("Parent_Id",jsonobject.getString("Parent_Id"));
                            map.put("Remark_Id",jsonobject.getString("Remark_Id"));





                            // Set the JSON Objects into the array
                            arraylist.add(map);

                        }
                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                    }
                    //e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast("Something went wrong. Please try after sometime",
                        getApplicationContext());
                finish();
            }
            else {
                if (jsonarray.length() <= 0) {
                    userFunction.cutomToast("Enter Valid LOSID",
                            getApplicationContext());
                    finish();

                } else {
                    txt_losid.setText(str_losid);
                    txt_customer_name.setText(str_customer_name);
                    txt_branch_name.setText(str_branch_name);

                    RemarksListAdapter adapter = new RemarksListAdapter(com.example.hdb.Remarks_Display.this, arraylist);
                    lv.setAdapter(adapter);

                    mProgressDialog.dismiss();
                }
            }


        }
    }
}
