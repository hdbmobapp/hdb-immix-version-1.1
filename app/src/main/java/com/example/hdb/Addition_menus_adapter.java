package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;



public class Addition_menus_adapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;

    UserFunctions user_function;

    public Addition_menus_adapter(Context context,
                                  ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView txt_msg_value;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.additional_menu_item, parent, false);
        // Get the position
        resultp = data.get(position);
        txt_msg_value = (TextView) itemView.findViewById(R.id.txt_msg_value);
        txt_msg_value.setText(resultp.get(Addition_menus.MESSAGE_VALUE));

        userFunction = new UserFunctions();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                arg0.setSelected(true);

                new Thread(new Runnable() {
                    public void run() {
                        // Get the position
                        //	rel_lay_msg.setBackgroundColor(context.getResources().getColor(R.color.default_screen_bg));

                        resultp = data.get(position);
                        Intent messagingActivity = new Intent(context,
                                WebClass.class);
                        messagingActivity.putExtra("msg_key",
                                resultp.get(Addition_menus.MESSAGE_KEY));
                        messagingActivity.putExtra("msg_value",
                                resultp.get(Addition_menus.MESSAGE_VALUE));

                        context.startActivity(messagingActivity);
                    }
                }).start();
            }

        });

        return itemView;
    }
}
