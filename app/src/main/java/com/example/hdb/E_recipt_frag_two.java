package com.example.hdb;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.MyAdapter;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 20-07-2018.
 */
public class E_recipt_frag_two extends Fragment {


    Button  Next,btn_back,txt_home;
    TextView txt_emi_amt,txt_lpp_amt,txt_bcc_amt,txt_coll_oth_chrg,txt_total;
    EditText edt_emp_code,edt_emp_name,edt_chq_no,edt_bank_name,edt_chq_date,edt_rcpt_date,edt_emi_amt,
             edt_lpp_amt,edt_bcc_amt,edt_coll_oth_chrg,edt_total,edt_remarks,edt_pan_no,edt_rcpt_amt;
    Spinner spinner_remark_reason,spinner_pan_aval,spinner_remark_reason_genr,spinner_form60;

    LinearLayout tableRow3,tableRow4,tableRow5,tableRow14,tableRow_remark_resgenr,
            tableRow_remark_res,tableRow7,tableRow8,tableRow9,
            tableRow11,tableRow12,tableRow_ramt,tableRowform ;
   RelativeLayout rel_due_delts;

    String Pan_ava_arraylist[] = {" Pan No Availble", "Yes", "No"};
    String form60_arraylist[] = {"Form 60", "Yes", "No"};
    ArrayList<String> remark_reason_genr_arraylist;

    String  strs_emp_code,strs_emp_name,strs_chq_no,strs_bank_name,strs_chq_date,
            strs_rcpt_date,strs_emi_amt, strs_lpp_amt,strs_bcc_amt,
            strs_coll_oth_chrg,strs_total,strs_remarks,strs_pan_no;


    String  str_pay_mode,str_product,str_product_name,
            str_loan_no,str_cust_name,str_branch_id,str_branch,str_contact_no,
            str_unique_id,str_pickup_type,str_pan1,str_pan2, str_total_emi_due,str_brch_gst_sc
            ,str_branch_state,str_cust_gst_sc,str_cust_state,str_gstn,str_cust_id ,str_loc_gen,strs_rcpt_amt,user_id;

    String  str_user_id,str_user_name,str_rm_rs_gen,str_pan,str_pan_no,str_rm_rs,str_form60;

    Double emi_cal = 0.0;
    Double lpp_cal =0.0;
    Double bcc_cal=0.0;
    Double other_cal=0.0;

    String emi ="0" ;
    String lpp ="0" ;
    String bcc ="0" ;
    String other ="0" ;

    Double Result = 0.0;


    ProgressDialog mProgressDialog;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    DatePickerDialog datePickerDialog;
    UserFunctions userFunction;
    Boolean ins = true;
    JSONObject jsonobject;
    SharedPreferences pref;
    ConnectionDetector cd;

    ArrayAdapter<String> spinnerArrayAdapteterremark_genr;

    private static final Pattern PAN_PATTERN = Pattern
            .compile("[A-Z]{5}"
                    + "[0-9]{4}" +
                    "[A-Z]{1}");


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.e_recipt_frag_two, container, false);

        int SDK_INT = Build.VERSION.SDK_INT;
        if(SDK_INT>8){
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }

        cd = new ConnectionDetector(getActivity().getApplicationContext());
        remark_reason_genr_arraylist = new ArrayList<String>();




        str_loc_gen = getArguments().getString("str_loc_gen");

        str_pay_mode = getArguments().getString("str_pay_mode");
        str_loan_no = getArguments().getString("str_loan_no");
        str_cust_name = getArguments().getString("str_cust_name");
        str_product = getArguments().getString("str_product");
        str_product_name  = getArguments().getString("str_product_name");
        str_branch_id = getArguments().getString("str_branch_id");
        str_branch = getArguments().getString("str_branch");
        str_contact_no = getArguments().getString("str_contact_no");
        str_unique_id = getArguments().getString("str_unique_id");
        str_total_emi_due = getArguments().getString("str_total_emi_due");
        str_pickup_type = getArguments().getString("str_pickup_type");

        str_brch_gst_sc = getArguments().getString("str_brch_gst_sc");
        str_branch_state = getArguments().getString("str_branch_state");
        str_cust_gst_sc = getArguments().getString("str_cust_gst_sc");
        str_cust_state = getArguments().getString("str_cust_state");
        str_gstn = getArguments().getString("str_gstn");
        str_cust_id = getArguments().getString("str_cust_id");


        userFunction = new UserFunctions();


        pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);



           /* Button*/
        Next =  view.findViewById(R.id.btn_next);
        btn_back =  view.findViewById(R.id.btn_back);
        txt_home =  view.findViewById(R.id.txt_home);

         /* Textview*/
        txt_emi_amt = view.findViewById(R.id.txt_emi_amt);
        txt_lpp_amt = view.findViewById(R.id.txt_lpp_amt);
        txt_bcc_amt = view.findViewById(R.id.txt_bcc_amt);
        txt_coll_oth_chrg = view.findViewById(R.id.txt_coll_oth_chrg);
        txt_total = view.findViewById(R.id.txt_total);

        /* EditText*/
        edt_emp_code =  view.findViewById(R.id.edt_emp_code);
        edt_emp_code.setEnabled(false);
        edt_emp_name =  view.findViewById(R.id.edt_emp_name);
        edt_emp_name.setEnabled(false);
        edt_chq_no =  view.findViewById(R.id.edt_chq_no);
        edt_bank_name =  view.findViewById(R.id.edt_bank_name);
        edt_chq_date =  view.findViewById(R.id.edt_chq_date);
        edt_rcpt_date =  view.findViewById(R.id.edt_rcpt_date);
        edt_rcpt_date.setEnabled(false);
        edt_emi_amt =  view.findViewById(R.id.edt_emi_amt);
        edt_lpp_amt =  view.findViewById(R.id.edt_lpp_amt);
        edt_bcc_amt =  view.findViewById(R.id.edt_bcc_amt);
        edt_coll_oth_chrg =  view.findViewById(R.id.edt_coll_oth_chrg);
        edt_total =  view.findViewById(R.id.edt_total);
        edt_total.setEnabled(false);
        edt_remarks =  view.findViewById(R.id.edt_remarks);
        edt_pan_no = view.findViewById(R.id.edt_pan_no);
        edt_pan_no.setAllCaps(true);
        edt_rcpt_amt = view.findViewById(R.id.edt_rcpt_amt);

        /* Linerr layout*/
        tableRow3 =  view.findViewById(R.id.tableRow3);
        tableRow4 =  view.findViewById(R.id.tableRow4);
        tableRow5 =  view.findViewById(R.id.tableRow5);
        tableRow14 =  view.findViewById(R.id.tableRow14);
        tableRow_remark_res = view.findViewById(R.id.tableRow_remark_res);
        tableRow_remark_resgenr = view.findViewById(R.id.tableRow_remark_resgenr);
        rel_due_delts =  view.findViewById(R.id.rel_due_delts);
        tableRow7 =  view.findViewById(R.id.tableRow7);
        tableRow8 =  view.findViewById(R.id.tableRow8);
        tableRow9 =  view.findViewById(R.id.tableRow9);
        tableRow11  =  view.findViewById(R.id.tableRow11);
        tableRow12  =  view.findViewById(R.id.tableRow12);
        tableRow_ramt = view.findViewById(R.id.tableRow_ramt);
        tableRowform = view.findViewById(R.id.tableRowform);
          /* Spinner*/
        spinner_remark_reason =  view.findViewById(R.id.spinner_remark_reason);
        spinner_pan_aval =  view.findViewById(R.id.spinner_pan_aval);
        spinner_remark_reason_genr =  view.findViewById(R.id.spinner_remark_reason_genr);
        spinner_form60  =  view.findViewById(R.id.spinner_form60);

        edt_emp_code.setText(str_user_id);
        edt_emp_name.setText(str_user_name);

        setvisibility();
        setdate();





        try {
            Ercpt_activity_main.get_lms_loccc_data(str_loan_no,str_user_id,"","");
        } catch (JSONException e) {
            e.printStackTrace();
        }



        edt_emi_amt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                emi = edt_emi_amt.getText().toString();
                if (emi.length() == 0) {
                    emi_cal = 0.0;
                }
                if (!emi.equals("")) {
                    char  first_char = emi.charAt(0);
                    String str_frst_char = Character.toString(first_char);
                    if (!str_frst_char.startsWith(".")) {
                        emi_cal = Double.parseDouble(emi);
                    }else{
                        edt_emi_amt.setError("please enter valid number");
                        edt_emi_amt.requestFocus();
                    }

                }
                try {
                    Calculate_amt();
                    System.out.println("emi_cal::::::" + emi_cal);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        edt_lpp_amt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                lpp = edt_lpp_amt.getText().toString();
                if (lpp.length() == 0) {
                    lpp_cal = 0.0;
                }
                if (!lpp.equals("")) {

                 char  first_char = lpp.charAt(0);
                    String str_frst_char = Character.toString(first_char);
                    if (!str_frst_char.startsWith(".")) {
                        lpp_cal = Double.parseDouble(lpp);
                    }else{
                        edt_lpp_amt.setError("please enter valid number");
                        edt_lpp_amt.requestFocus();
                    }
                }

                try {
                    Calculate_amt();
                    System.out.println("lpp_cal::::::" + lpp_cal);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        edt_bcc_amt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                bcc = edt_bcc_amt.getText().toString();
                if (bcc.length() == 0) {
                    bcc_cal = 0.0;
                }
                if (!bcc.equals("")) {

                    char  first_char = bcc.charAt(0);
                    String str_frst_char = Character.toString(first_char);
                    if (!str_frst_char.startsWith(".")) {
                        bcc_cal = Double.parseDouble(bcc);
                    }else{
                        edt_bcc_amt.setError("please enter valid number");
                        edt_bcc_amt.requestFocus();
                    }



                }

                try {
                    Calculate_amt();
                    System.out.println("bcc::::::" + bcc_cal);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        edt_coll_oth_chrg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                other = edt_coll_oth_chrg.getText().toString();
                if (other.length() == 0) {
                    other_cal = 0.0;
                }
                if (!other.equals("")) {

                    char  first_char = other.charAt(0);
                    String str_frst_char = Character.toString(first_char);
                    if (!str_frst_char.startsWith(".")) {
                        other_cal = Double.parseDouble(other);
                    }else{
                        edt_coll_oth_chrg.setError("please enter valid number");
                        edt_coll_oth_chrg.requestFocus();
                    }

                }

                try {
                    Calculate_amt();
                    System.out.println("other_cal::::::" + other_cal);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });





        ArrayAdapter<String> spinnerArrayAdapteterremark = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, Ercpt_activity_main.remark_reason_arraylist);
        spinnerArrayAdapteterremark.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_remark_reason.setAdapter(spinnerArrayAdapteterremark);
        spinner_remark_reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_remark_reason.getSelectedItemPosition() == 0) {
                    str_rm_rs = null;
                } else {
                    str_rm_rs = spinner_remark_reason.getItemAtPosition(spinner_remark_reason.getSelectedItemPosition()).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });



        spinnerArrayAdapteterremark_genr = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit,remark_reason_genr_arraylist);
        spinnerArrayAdapteterremark_genr.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        new loadSpinnerData_gen().execute();

        spinner_remark_reason_genr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_remark_reason_genr.getSelectedItemPosition() == 0) {
                    str_rm_rs_gen = null;
                } else {
                    str_rm_rs_gen = spinner_remark_reason_genr.getItemAtPosition(spinner_remark_reason_genr.getSelectedItemPosition()).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });




        ArrayAdapter<String> spinnerArrayAdapteterpan = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, Pan_ava_arraylist);
        spinnerArrayAdapteterpan.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_pan_aval.setAdapter(spinnerArrayAdapteterpan);
        spinner_pan_aval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_pan_aval.getSelectedItemPosition() == 0) {
                    str_pan = null;
                } else {
                    str_pan = spinner_pan_aval.getItemAtPosition(spinner_pan_aval.getSelectedItemPosition()).toString();
                    if (str_pan.contains("Yes")) {
                        tableRowform.setVisibility(View.GONE);
                        tableRow14.setVisibility(View.VISIBLE);
                        str_pan1 = Ercpt_activity_main.pan1;
                        System.out.println("str_pan1:::::::::::" + str_pan1);

                        if (!str_pan1.isEmpty()) {

                            edt_pan_no.setEnabled(false);
                            edt_pan_no.setText(str_pan1);
                        } else {

                            edt_pan_no.setEnabled(true);
                        }


                    } else {
                        tableRow14.setVisibility(View.GONE);

                        if(str_loc_gen.contains("Genr")) {

                            tableRowform.setVisibility(View.GONE);
                        }else {
                            tableRowform.setVisibility(View.VISIBLE);
                        }

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


        ArrayAdapter<String> spinnerArrayAdapteterform60 = new MyAdapter(getActivity(), R.layout.sp_display_layout, R.id.txt_visit, form60_arraylist);
        spinnerArrayAdapteterform60.setDropDownViewResource(R.layout.spinner_layout); // The drop down view
        spinner_form60.setAdapter(spinnerArrayAdapteterform60);
        spinner_form60.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spinner_form60.getSelectedItemPosition() == 0) {
                    str_form60 = null;
                } else {
                    str_form60 = spinner_form60.getItemAtPosition(spinner_form60.getSelectedItemPosition()).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });




        Next.setOnClickListener(new View.OnClickListener() {
                                    @Override

                                    public void onClick(View v) {
                                        ins = true;


                                        strs_emp_code = edt_emp_code.getText().toString();
                                        strs_emp_name = edt_emp_name.getText().toString();
                                        strs_chq_no = edt_chq_no.getText().toString();
                                        strs_bank_name = edt_bank_name.getText().toString();
                                        strs_chq_date = edt_chq_date.getText().toString();
                                        strs_rcpt_date = edt_rcpt_date.getText().toString();
                                        strs_emi_amt = edt_emi_amt.getText().toString();
                                        strs_lpp_amt = edt_lpp_amt.getText().toString();
                                        strs_bcc_amt = edt_bcc_amt.getText().toString();
                                        strs_coll_oth_chrg = edt_coll_oth_chrg.getText().toString();
                                        strs_total = edt_total.getText().toString();
                                        strs_remarks = edt_remarks.getText().toString();
                                        strs_pan_no = edt_pan_no.getText().toString();
                                        strs_rcpt_amt = edt_rcpt_amt.getText().toString();


                                        if (str_pay_mode != null && !str_pay_mode.isEmpty() && ins == true) {
                                            if (str_pay_mode.contains("Cheque") || str_pay_mode.contains("DD")) {
                                                if (strs_chq_no.equals("") && ins == true) {
                                                    System.out.println("strs_chq_no:::" + strs_chq_no);
                                                    edt_chq_no.setError("Please Enter Cheque No.");
                                                    edt_chq_no.requestFocus();
                                                    ins = false;
                                                }

                                                if (strs_bank_name.equals("") && ins == true) {
                                                    System.out.println("strs_bank_name:::" + strs_bank_name);
                                                    edt_bank_name.setError("Please Enter Bank Name.");
                                                    edt_bank_name.requestFocus();
                                                    ins = false;
                                                }

                                                if (strs_chq_date.equals("") && ins == true) {
                                                    System.out.println("strs_bank_name:::" + strs_bank_name);
                                                    Toast.makeText(getContext(), "Please Select Cheque Date", Toast.LENGTH_SHORT).show();
                                                    ins = false;
                                                }

                                            }
                                        }

                                        if (str_loc_gen.contains("Genr")) {

                                            if (strs_rcpt_amt.equals("") && ins == true) {
                                                System.out.println("strs_bank_name:::" + strs_bank_name);
                                                edt_rcpt_amt.setError("Please Enter Receipt Amount.");
                                                edt_rcpt_amt.requestFocus();
                                                ins = false;
                                            }

                                            if (str_rm_rs_gen == null && ins == true) {
                                                ins = false;
                                                Toast.makeText(getContext(), "You did not Select Remarks/Reasons", Toast.LENGTH_SHORT).show();
                                            }
                                        }


                                        if (str_loc_gen.contains("Locc")) {

                                            if (str_rm_rs == null && ins == true) {
                                                ins = false;
                                                Toast.makeText(getContext(), "You did not Select Remarks/Reasons", Toast.LENGTH_SHORT).show();
                                            }

                                            if (strs_total.equals("") && ins == true) {
                                                edt_total.setError("Please calculate your Amount.");
                                                edt_total.requestFocus();
                                                ins = false;
                                            }
                                            if(str_pay_mode.contains("Cash")) {
                                                if (!strs_total.isEmpty() && ins == true) {
                                                    Double ta_amt = Double.parseDouble(strs_total);
                                                    if (ta_amt > 150000 && ins == true) {
                                                        edt_total.setError("Your Total Amount Exceeded,It Should Not  Above than 150000");
                                                        edt_total.requestFocus();
                                                        ins = false;
                                                    }

                                                }
                                            }

                                            }


                                        if (str_pan == null && ins == true) {
                                            ins = false;
                                            Toast.makeText(getContext(), "You did not Select Pan Avalability", Toast.LENGTH_SHORT).show();
                                        }

                                        if (str_pan != null && !str_pan.isEmpty() && ins == true) {
                                            if (str_pan.contains("Yes")) {

                                                str_pan_no = edt_pan_no.getText().toString();

                                                if (str_pan_no.equals("") && ins == true) {
                                                    System.out.println("str_pan:::" + str_pan);
                                                    edt_pan_no.setError("Please Enter Pan No.");
                                                    edt_pan_no.requestFocus();
                                                    ins = false;
                                                }
                                                if (!str_pan_no.equals("") && ins == true) {
                                                    if (!Check_pattern(str_pan_no.toUpperCase()) && ins == true) {
                                                        edt_pan_no.setError("Enter valid PAN No.");
                                                        edt_pan_no.requestFocus();
                                                        ins = false;
                                                    }
                                                    if (str_pan_no.charAt(3) != 'P' && ins == true) {
                                                        edt_pan_no.setError("Fourth character should be 'P'");
                                                        edt_pan_no.requestFocus();
                                                        ins = false;
                                                    }

                                                }
                                            }


                                        }

                                        if (str_loc_gen.contains("Locc")) {
                                            if (str_pan != null && !str_pan.isEmpty() && ins == true) {
                                                if (str_pan.contains("No")) {
                                                    if (!strs_total.isEmpty() && ins == true) {
                                                        Double ta_amt = Double.parseDouble(strs_total);
                                                        if (ta_amt > 50000 && ins == true) {
                                                            if (str_form60 == null && ins == true) {
                                                                ins = false;
                                                                Toast.makeText(getContext(), "You did not Select form60 ", Toast.LENGTH_SHORT).show();
                                                            }
                                                            if (str_form60 != null && !str_form60.isEmpty() && ins == true) {
                                                                if (str_form60.contains("No") && ins == true) {
                                                                    ins = false;
                                                                    Toast.makeText(getContext(), "Please Collect Form 60 ", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                        }



                                        if (strs_remarks.equals("") && ins == true) {
                                            System.out.println("str_pan:::" + str_pan);
                                            edt_remarks.setError("Please Enter Remark.");
                                            edt_remarks.requestFocus();
                                            ins = false;
                                        }


                                        if (cd.isConnectingToInternet()) {

                                            if (ins == true) {

                                                Fragment f = new E_recipt_otp();
                                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                Bundle args = new Bundle();

                                                args.putString("str_loc_gen", str_loc_gen);

                                                if (str_loc_gen.contains("Locc")) {
                                                    args.putString("str_pay_mode", str_pay_mode);
                                                    args.putString("str_loan_no", str_loan_no);
                                                    args.putString("str_cust_name", str_cust_name);
                                                    args.putString("str_product", str_product);
                                                    args.putString("str_product_name", str_product_name);

                                                    args.putString("str_branch_id", str_branch_id);
                                                    args.putString("str_branch", str_branch);
                                                    args.putString("str_contact_no", str_contact_no);
                                                    args.putString("str_unique_id", str_unique_id);
                                                    args.putString("str_total_emi_due", str_total_emi_due);
                                                    args.putString("str_pickup_type", str_pickup_type);


                                                    args.putString("strs_emp_code", strs_emp_code);
                                                    args.putString("strs_emp_name", strs_emp_name);
                                                    args.putString("strs_chq_no", strs_chq_no);
                                                    args.putString("strs_bank_name", strs_bank_name);
                                                    args.putString("strs_chq_date", strs_chq_date);
                                                    args.putString("strs_rcpt_date", strs_rcpt_date);
                                                    args.putString("strs_emi_amt", strs_emi_amt);
                                                    args.putString("strs_lpp_amt", strs_lpp_amt);
                                                    args.putString("strs_bcc_amt", strs_bcc_amt);
                                                    args.putString("strs_coll_oth_chrg", strs_coll_oth_chrg);
                                                    args.putString("strs_total", strs_total);
                                                    args.putString("strs_remarks", strs_remarks);
                                                    args.putString("strs_pan_no", strs_pan_no);
                                                    args.putString("str_rm_rs", str_rm_rs);
                                                    args.putString("str_pan", str_pan);
                                                    args.putString("str_form60", str_form60);


                                                    System.out.println("str_total_emi_due::::::::::::::::::" + str_total_emi_due);
                                                    System.out.println("strs_pan_no::::::::::::::::::" + strs_pan_no);
                                                }

                                                if (str_loc_gen.contains("Genr")) {
                                                    args.putString("str_pay_mode", str_pay_mode);
                                                    args.putString("str_loan_no", str_loan_no);
                                                    args.putString("str_cust_name", str_cust_name);
                                                    args.putString("str_product", str_product);
                                                    args.putString("str_product_name", str_product_name);

                                                    args.putString("str_branch_id", str_branch_id);
                                                    args.putString("str_branch", str_branch);
                                                    args.putString("str_contact_no", str_contact_no);
                                                    args.putString("str_unique_id", str_unique_id);
                                                    args.putString("str_total_emi_due", str_total_emi_due);
                                                    args.putString("str_pickup_type", str_pickup_type);
                                                    args.putString("strs_emp_code", strs_emp_code);
                                                    args.putString("strs_emp_name", strs_emp_name);
                                                    args.putString("strs_pan_no", strs_pan_no);
                                                    args.putString("str_pan", str_pan);

                                                    args.putString("strs_chq_no", strs_chq_no);
                                                    args.putString("strs_bank_name", strs_bank_name);
                                                    args.putString("strs_chq_date", strs_chq_date);
                                                    args.putString("strs_rcpt_date", strs_rcpt_date);

                                                    args.putString("strs_rcpt_amt", strs_rcpt_amt);
                                                    args.putString("str_brch_gst_sc", str_brch_gst_sc);
                                                    args.putString("str_branch_state", str_branch_state);
                                                    args.putString("str_cust_gst_sc", str_cust_gst_sc);
                                                    args.putString("str_cust_state", str_cust_state);
                                                    args.putString("str_gstn", str_gstn);
                                                    args.putString("str_cust_id", str_cust_id);
                                                    args.putString("strs_remarks", strs_remarks);
                                                    args.putString("str_rm_rs_gen", str_rm_rs_gen);
                                                    args.putString("str_form60", str_form60);

                                                    System.out.println("str_total_emi_due::::::::::::::::::" + str_total_emi_due);
                                                }
                                                f.setArguments(args);
                                                fragmentTransaction.replace(R.id.container, f);
                                                fragmentTransaction.addToBackStack(null);
                                                fragmentTransaction.commit();
                                            }
                                        }else {
                                            userFunction.cutomToast("No internet Connection  ", getContext());

                                        }
                                    }

                                }
        );



       edt_chq_date.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Calendar calender = Calendar.getInstance();

               int mYear = calender.get(Calendar.YEAR);
               int mMonth = calender.get(Calendar.MONTH);
               int mDay = calender.get(Calendar.DAY_OF_MONTH);

               datePickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog,
                       new DatePickerDialog.OnDateSetListener() {

                           @Override
                           public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                 int dayOfMonth) {
                               String curr_date1 = "01-03-2017";

                               String age = userFunction.dateCompare(dayOfMonth, monthOfYear + 1, year, curr_date1);
                               //txt_date.setText(String.valueOf(age));

                               String s = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                               Date df1 = null;
                               String df2 = null;

                               SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                               try {

                                   df1 = formater.parse(s);
                                   df2 = formater.format(df1);

                               } catch (ParseException e) {
                                   e.printStackTrace();
                               }

                               edt_chq_date.setText(df2 + "+05:30");

                           }
                       }, mYear, mMonth, mDay);
               datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

               datePickerDialog.show();
           }
       });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().replace(R.id.container, new E_recipt_frag_one()).commit();

            }
        });




        return view;

    }

  /*  private void loadSpinnerData_gen() throws JSONException {

        remark_reason_genr_arraylist.clear();
        //String  str_product_f = Ercpt_activity_main.prodf;
        jsonobject = userFunction.get_rem_data_gen(str_product);

        JSONObject json_rem = jsonobject.getJSONObject("data_rem_reas_prod");
        JSONArray jsonarray_rem = json_rem.getJSONArray("remark_prod");
        remark_reason_genr_arraylist.add("Select Remark/Reasons");

        for (int i = 0; i < jsonarray_rem.length(); i++) {
            json_rem = jsonarray_rem.getJSONObject(i);
            String data = json_rem.getString("Remarks_Reason");
            remark_reason_genr_arraylist.add(data);

            System.out.println("remark_reason_arraylist::::::::::::::::::::" + remark_reason_genr_arraylist);

        }
    }*/



    public class loadSpinnerData_gen extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage("Please Wait.....");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            remark_reason_genr_arraylist.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            jsonobject = userFunction.get_rem_data_gen(str_product);
            System.out.println("jsonobject::::::::::::::::::::" +jsonobject);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {

                    JSONObject json_rem = jsonobject.getJSONObject("data_rem_reas_prod");
                    JSONArray jsonarray_rem = json_rem.getJSONArray("remark_prod");
                    remark_reason_genr_arraylist.add("Select Remark/Reasons");

                    for (int i = 0; i < jsonarray_rem.length(); i++) {
                        json_rem = jsonarray_rem.getJSONObject(i);
                        String data = json_rem.getString("Remarks_Reason");
                        remark_reason_genr_arraylist.add(data);

                        //System.out.println("remark_reason_arraylist::::::::::::::::::::" + remark_reason_genr_arraylist);

                    }
                    spinner_remark_reason_genr.setAdapter(spinnerArrayAdapteterremark_genr);


                }else {

                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                           getActivity(). getApplicationContext());

                    Intent messagingActivity = new Intent(
                          getActivity().  getApplicationContext(), HdbHome.class);
                    startActivity(messagingActivity);


                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }


    }



    private boolean Check_pattern(String string) {

        Boolean status = false;

        status = PAN_PATTERN.matcher(string).matches();

        System.out.println("PATTEN_PAN::::" + status);
        return status;
    }
    private void Calculate_amt() throws JSONException {

        Result=0.0;
        Result= emi_cal + lpp_cal + bcc_cal + other_cal;
        System.out.println("Result:::::::::::"+Result);

        String str_result = Double.toString(Result);
        edt_total.setText(str_result);
    }

    public void setdate(){
        Calendar calender = Calendar.getInstance();

        int mYear = calender.get(Calendar.YEAR);
        int mMonth = calender.get(Calendar.MONTH);
        int mDay = calender.get(Calendar.DAY_OF_MONTH);
        String s = mYear + "-" + (mMonth + 1) + "-" + mDay;

        Date df1 = null;
        String df2 = null;

        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
        try {

            df1 = formater.parse(s);
            df2 = formater.format(df1);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        edt_rcpt_date.setText(df2 + "+05:30");
    }

    public void setvisibility(){
        if(str_pay_mode.contains("Cheque") ||str_pay_mode.contains("DD")){
            tableRow3.setVisibility(View.VISIBLE);
            tableRow4.setVisibility(View.VISIBLE);
            tableRow5.setVisibility(View.VISIBLE);

        }

        if(str_loc_gen.contains("Locc") ){
            tableRow_remark_res.setVisibility(View.VISIBLE);
            tableRow_remark_resgenr.setVisibility(View.GONE);
            rel_due_delts.setVisibility(View.VISIBLE);
            tableRow7.setVisibility(View.VISIBLE);
            tableRow8.setVisibility(View.VISIBLE);
            tableRow9.setVisibility(View.VISIBLE);
            tableRow11.setVisibility(View.VISIBLE);
            tableRow12.setVisibility(View.VISIBLE);

            txt_emi_amt.setText(Ercpt_activity_main.emi);
            txt_lpp_amt.setText(Ercpt_activity_main.LPP);
            txt_bcc_amt.setText(Ercpt_activity_main.BCC);
            txt_coll_oth_chrg.setText(Ercpt_activity_main.other_charg);
            txt_total.setText(Ercpt_activity_main.total_amt);


        }
        if(str_loc_gen.contains("Genr") ){
            tableRow_remark_res.setVisibility(View.GONE);
            tableRow_ramt.setVisibility(View.VISIBLE);
            tableRow_remark_resgenr.setVisibility(View.VISIBLE);
            rel_due_delts.setVisibility(View.GONE);
            tableRow7.setVisibility(View.GONE);
            tableRow8.setVisibility(View.GONE);
            tableRow9.setVisibility(View.GONE);
            tableRow11.setVisibility(View.GONE);
            tableRow12.setVisibility(View.GONE);
            tableRowform.setVisibility(View.GONE);

        }

    }


    public void setlayout(){

    }
}
