package com.example.hdb;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 01-10-2015.
 */

public class FAQListAdapter extends BaseAdapter {
    String str_id=null;
    // Declare Variables
    ProgressDialog dialog = null;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    String str_product_id;
    UserFunctions user_function;
    Uri myUri;
    MediaPlayer mediaPlayer;
    static String FAQ_lang = "FAQ_lang";
    static String FAQ_audio_link = "FAQ_audio_link";
    EditText edt_faq_search;
    Button btn_search;
    int int_from_act;
    String str_keyword;
    String faq_id;
    ListView mList;


    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;
    FAQ_Lang_master_Adapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;
    TextView txt_error;
    ProgressBar progressbar_loading;

    public FAQListAdapter(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
        mediaPlayer = new MediaPlayer();
        cd = new ConnectionDetector(context);
    }
    public int getCount() {
        return data.size();
    }
    public Object getItem(int position) {
        return null;
    }
    public long getItemId(int position) {
        return position;
    }
    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        ViewHolder holder = null;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = null;
        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.my_faq_list_item, parent,
                    false);

            holder = new ViewHolder();
// --            holder.tv_faq_ans= new ViewHolder();
            holder.tv_faq_quest = (TextView) itemView
                    .findViewById(R.id.txt_faq_quest);
            holder.tv_faq_ans = (TextView) itemView.findViewById(R.id.txt_faq_ans);
        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        holder.tv_faq_quest.setText(Html.fromHtml(resultp.get(FAQ.FAQ_question)) + resultp.get(FAQ.FAQ_ID).toString());
        holder.tv_faq_ans.setClickable(true);
        holder.tv_faq_ans.setMovementMethod(LinkMovementMethod.getInstance());
        String text =resultp.get(FAQ.FAQ_answer).toString();
        holder.tv_faq_ans.setText(Html.fromHtml(text));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                str_id=resultp.get(FAQ.FAQ_ID).toString();
                System.out.println("str_id::::"+str_id);

                callPopup(str_id);

            }
        });


        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_faq_quest;
        public TextView tv_faq_ans;


    }

    private void callPopup(String str_faq_id) {


        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.popup);
        dialog.setTitle("Select Your Choice...");
        System.out.print("str_faq_id:::"+str_faq_id);
        mList = (ListView) dialog.findViewById(R.id.list);
        cd = new ConnectionDetector(context);
        userFunction =new UserFunctions();

        if (cd.isConnectingToInternet()) {
            new DownloadFAQ().execute();
        } else {
            // txt_error.setText(context.getResources().getString(R.string.no_internet));
            //txt_error.setVisibility(View.VISIBLE);

        }


        dialog.show();


    }

    public class DownloadFAQ extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            // progressbar_loading.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            jsonobject = userFunction.getFAQ_LangList(str_id);
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");

                arraylist = new ArrayList<>();

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("FAQ_lang", jsonobject.getString("faq_lang"));
                    map.put("FAQ_audio_link", jsonobject.getString("filepath"));
                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        context.getResources().getString(R.string.error_message),
                        context);
                txt_error.setText(context.getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                adapter = new FAQ_Lang_master_Adapter(context, arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

            }
            // progressbar_loading.setVisibility(View.GONE);

        }

    }
}