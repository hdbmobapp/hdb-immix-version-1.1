package com.example.hdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 01-08-2016.
 */
public class FAQ_Lang_master_Adapter extends BaseAdapter {

    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    UserFunctions userFunction;
    String str_product_id;
    ArrayAdapter<String> adapter;
    UserFunctions user_function;
    Uri myUri;
    MediaPlayer mediaPlayer;
    ViewHolder holder = null;




    public FAQ_Lang_master_Adapter(Context context,  ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        dialog = new ProgressDialog(context);
        user_function = new UserFunctions();
        mediaPlayer = new MediaPlayer();

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);

        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.faq_audeo_list_item, parent,
                    false);

            holder = new ViewHolder();
// --            holder.tv_faq_ans= new ViewHolder();
            holder.tv_faq_quest = (TextView) itemView
                    .findViewById(R.id.txt_faq_quest);
            holder.tv_faq_ans = (TextView) itemView.findViewById(R.id.txt_faq_ans);
            holder.btn_play=(ImageView)itemView.findViewById(R.id.btn_play);
            holder.btn_stop=(ImageView)itemView.findViewById(R.id.btn_stop);

        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        holder.tv_faq_quest.setText(Html.fromHtml(resultp.get(FAQ_Lang_master.FAQ_lang)));
        holder.tv_faq_ans.setText(Html.fromHtml(resultp.get(FAQ_Lang_master.FAQ_audio_link)));


        itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                Intent i = new Intent(
                        context, FAQ_AudioPlayer.class);
                i.putExtra("faq_link", resultp.get(FAQ_Lang_master.FAQ_audio_link));
               // context.startActivity(i);

            }
        });

        holder.btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                Toast.makeText(context, "Playing sound", Toast.LENGTH_SHORT).show();
               String faq_link = "https://hdbapp.hdbfs.com/" + resultp.get(FAQ_Lang_master.FAQ_audio_link).toString();


                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                try {
                    mediaPlayer.setDataSource(faq_link);
                } catch (IllegalArgumentException e) {
                    Toast.makeText(context, "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (SecurityException e) {
                    Toast.makeText(context, "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IllegalStateException e) {
                    Toast.makeText(context, "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    mediaPlayer.prepare();
                } catch (IllegalStateException e) {
                    Toast.makeText(context, "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    Toast.makeText(context, "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
                }
                mediaPlayer.start();
                //holder.btn_play.setEnabled(false);
            }
        });

        holder.btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Pausing sound", Toast.LENGTH_SHORT).show();
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                //holder.btn_stop.setEnabled(false);
               // holder.btn_play.setEnabled(true);

            }
        });
/*
        if (resultp.get(MyCases.REMARKS) != null && !resultp.get(MyCases.REMARKS).isEmpty() && !resultp.get(MyCases.REMARKS).equals("null"))
        {
			holder.tv_remarks.setText(resultp.get(MyCases.REMARKS));
			holder.tbl_remarks.setVisibility(View.VISIBLE);
		} else {
			holder.tbl_remarks.setVisibility(View.GONE);
		}
*/
        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_faq_quest;

        public TextView tv_faq_ans;
        public ImageView btn_play;
        public ImageView btn_stop;


    }

private void setenable(){
    holder.btn_play.setEnabled(false);

}
}