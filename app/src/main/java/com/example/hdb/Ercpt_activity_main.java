package com.example.hdb;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 20-07-2018.
 */
public class Ercpt_activity_main extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
private static final String TAG = "Ercpt_activity_main";

    static JSONObject jsonobject;
    static  UserFunctions userFunction;
    static ArrayList<String> remark_reason_arraylist,al_rel,module_arraylist;
    String losid;

    static String id,prodf,prod,branchid,branch,cust_name,contact_no,total_amt,
            emi,BCC,LPP,unqid,other_charg,pan1,pan2,is_verified,branch_gst_sc
            ,cust_gst_sc,branch_state,cust_state,gstn,cust_id,total_emi_due,prod_name,pickuptype,product_s,msg
            , custname_s ,branch_s;
    static  Double lat;
    static  Double lon;

    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;
    ConnectionDetector cd;
static String [] str_arr_list;

    ProgressDialog mProgressDialog;
    JSONObject json;

    static String str_losid,str_from,str_id;

    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    SharedPreferences pref;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        str_from = i.getStringExtra("from");
        str_id = i.getStringExtra("str_id");
        System.out.println("++++++++++++++++++"+str_from);
        if(str_from.equals("pickup")){
            str_losid=i.getStringExtra("loan_no");
        }

        setContentView(R.layout.activity_e_rcpt_main);



        int SDK_INT = Build.VERSION.SDK_INT;
        if(SDK_INT>8){
            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        userFunction = new UserFunctions();

        remark_reason_arraylist = new ArrayList<String>();
        module_arraylist = new ArrayList<String>();
        al_rel=new ArrayList<String>();
        cd = new ConnectionDetector(com.example.hdb.Ercpt_activity_main.this);



            new loadSpinnerData().execute();
        //    new download_locc_gen_data().execute();




        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) com.example.hdb.Ercpt_activity_main.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        checkLocation();



    }

    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
          //  Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest, (LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        //System.out.println("lat::::"+lat);
        lon = location.getLongitude();

        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static JSONObject get_lms_loccc_data(String losid_n,String user_id ,String str_lat,String str_long) throws JSONException {
        jsonobject = userFunction.get_lms_Locc_data(losid_n,user_id,str_lat,str_long);

        try {
            if (jsonobject != null) {
                String KEY_STATUS = "status";
                if (jsonobject.getString(KEY_STATUS) != null) {
                    String res = jsonobject.getString(KEY_STATUS);
                    String KEY_SUCCESS = "success";
                    String resp_success = jsonobject.getString(KEY_SUCCESS);
                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
         id = jsonobject.getString("APPLID");
         prod = jsonobject.getString("PRODUCTF");
         prod_name = jsonobject.getString("PRODUCT");
         branchid = jsonobject.getString("BRANCHID");
         branch = jsonobject.getString("BRANCH");
         cust_name = jsonobject.getString("CUSTOMER_NAME");
         contact_no = jsonobject.getString("RESI_MOBILE");
         total_emi_due = jsonobject.getString("emi");
         total_amt = jsonobject.getString("Amt_Overdue");
         emi = jsonobject.getString("Emi_Due");
         BCC = jsonobject.getString("CBC_Amt_Overdue");
         LPP = jsonobject.getString("Lpp_Charges_Overdue");
         unqid = jsonobject.getString("Uniqueid");
         other_charg = jsonobject.getString("Other_Charges");
         pan1 = jsonobject.getString("PAN_NO");
         pickuptype = jsonobject.getString("pickup_type");

         is_verified = jsonobject.getString("is_verified");

         System.out.println("APPID::::::::::::::::::::" +id);
                    } else {
                        msg = jsonobject.getString("message");
                        System.out.println("mmmmmmmmmmmmmmmmmmmmm"+msg);
                    }


                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonobject;
    }

    public static JSONObject get_los_genrr_e_data(String losid_n,String user_id,String str_lat,String str_long) throws JSONException {

        jsonobject = userFunction.get_los_genrr_data(losid_n,user_id,str_lat,str_long);

        try {
            if (jsonobject != null) {
                String KEY_STATUS = "status";
                if (jsonobject.getString(KEY_STATUS) != null) {
                    String res = jsonobject.getString(KEY_STATUS);
                    String KEY_SUCCESS = "success";
                    String resp_success = jsonobject.getString(KEY_SUCCESS);
                    if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

        id = jsonobject.getString("APPLID");
        prod = jsonobject.getString("PRODUCTF");
        prod_name = jsonobject.getString("PRODUCT");
        branchid = jsonobject.getString("BRANCHID");
        branch = jsonobject.getString("BRANCH");
        cust_name = jsonobject.getString("CUSTOMER_NAME");
        contact_no = jsonobject.getString("RESI_MOBILE");
        total_emi_due = jsonobject.getString("emi");
        branch_gst_sc = jsonobject.getString("BRANCH_GST_STATECODE");
        branch_state = jsonobject.getString("BRANCH_STATE");
        cust_gst_sc= jsonobject.getString("CUST_GST_STATECODE");
        cust_state = jsonobject.getString("CUST_STATE");
        unqid = jsonobject.getString("Uniqueid");
        gstn = jsonobject.getString("GSTN");
        cust_id = jsonobject.getString("CUST_ID");
        pan1 = jsonobject.getString("PAN_NO");
        pickuptype = jsonobject.getString("pickup_type");
        is_verified = jsonobject.getString("is_verified");
        System.out.println("APPID::::::::::::::::::::" +id);
                    } else {
                        msg = jsonobject.getString("message");
                        System.out.println("mmmmmmmmmmmmmmmmmmmmm"+msg);
                    }


                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonobject;
    }




    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }





    public class loadSpinnerData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            mProgressDialog = new ProgressDialog(com.example.hdb.Ercpt_activity_main.this);
            mProgressDialog.setMessage("Please Wait.....");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            jsonobject = userFunction.get_rem_data();
            System.out.println("jsonobject::::::::::::::::::::" +jsonobject);

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {


                            JSONObject json_rem = jsonobject.getJSONObject("data_rem_reas");
                            JSONArray jsonarray_rem = json_rem.getJSONArray("remark");
                            remark_reason_arraylist.add("Select Remark/Reasons");
                            for (int i = 0; i < jsonarray_rem.length(); i++) {
                                json_rem = jsonarray_rem.getJSONObject(i);
                                String data = json_rem.getString("Remarks_Reason");
                                remark_reason_arraylist.add(data);

                                //System.out.println("remark_reason_arraylist::::::::::::::::::::" + remark_reason_arraylist);

                            }


                            JSONObject json_rel = jsonobject.getJSONObject("data_rel");
                            JSONArray jsonarray_rel = json_rel.getJSONArray("relation");
                            al_rel.add("Select Relation");
                            for (int i = 0; i < jsonarray_rel.length(); i++) {
                                json_rel = jsonarray_rel.getJSONObject(i);
                                String relation = json_rel.getString("Relation");
                                al_rel.add(relation);

                                //   System.out.println("al_rel::::::::::::::::::::" + al_rel);

                            }



                    JSONObject json_mod = jsonobject.getJSONObject("data_new");
                    JSONArray jsonarray_mod = json_mod.getJSONArray("data_module");
                    module_arraylist.add("select E-receipt");
                    for (int i = 0; i < jsonarray_mod.length(); i++) {
                        json_mod = jsonarray_mod.getJSONObject(i);
                        String mod = json_mod.getString("module");
                        module_arraylist.add(mod);
                        System.out.println("module_arraylist::::::::::::::::::::" + module_arraylist);

                    }



                }else {

                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());

                    Intent messagingActivity = new Intent(
                            getApplicationContext(), HdbHome.class);
                    startActivity(messagingActivity);
                }
                mProgressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mProgressDialog.dismiss();

        }


    }



    @Override
    public void onBackPressed(){
       // super.onBackPressed();
    }

}






