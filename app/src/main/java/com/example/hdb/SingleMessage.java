package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import info.hdb.libraries.UserFunctions;

public class SingleMessage extends Activity {
	UserFunctions userFunction;
	SharedPreferences pref;
	String str_user_id = null;
	TextView txt_msg_from, txt_msg, txt_created_date,txt_message_subject;
	String str_msg_from, str_msg, str_created_Date;
	JSONObject jsonobject = null;
	public static final String KEY_STATUS = "status";
    String str_msg_id;
	ProgressDialog mProgressDialog;
	String str_msg_subject=null;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.message_box_list_item);
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		str_user_id = pref.getString("user_id", null);
		mProgressDialog=new ProgressDialog(com.example.hdb.SingleMessage.this);
		userFunction = new UserFunctions();

		Intent i = getIntent();
		str_msg_from = i.getStringExtra("msg_from");
		str_msg = i.getStringExtra("msg");
		str_created_Date = i.getStringExtra("created_date");
		str_created_Date = i.getStringExtra("created_date");
		str_msg_id= i.getStringExtra("msg_id");
		
		str_msg_subject=i.getStringExtra("msg_subject");
		new updateMsg().execute();
	}

	public void setlayout() {
		txt_msg_from = (TextView) findViewById(R.id.msg_from);
		txt_msg = (TextView) findViewById(R.id.txt_msg);
		txt_created_date = (TextView) findViewById(R.id.txt_created_date);
		txt_message_subject = (TextView) findViewById(R.id.txt_msg_subject);

		txt_msg_from.setText("From - " + str_msg_from);
		txt_msg.setText(Html.fromHtml(str_msg));
		txt_msg.setMovementMethod(LinkMovementMethod.getInstance());
		txt_created_date.setText(str_created_Date);
		txt_message_subject.setText("Subject- "+str_msg_subject);
	}
	public class updateMsg extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(com.example.hdb.SingleMessage.this);
			mProgressDialog.setMessage(getString(R.string.plz_wait));
			mProgressDialog.setCancelable(false);
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// then do your work
			jsonobject = userFunction.update_message(str_msg_id,str_user_id);
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			try {
				if (jsonobject != null) {
					// text_error_msg.setVisibility(View.GONE);

					// Log.d("jsonobject OP : " , jsonobject.toString() );
					if (jsonobject.getString(KEY_STATUS) != null) {

						String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

						if (Integer.parseInt(res) == 200
								&& resp_success.equals("true")) {
							setlayout();
						} else {
							 String
							 error_msg= jsonobject.getString("message");
							 userFunction.cutomToast(error_msg,
							 com.example.hdb.SingleMessage.this);

						}
					}
				}
				mProgressDialog.dismiss();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			

		}
	}
}