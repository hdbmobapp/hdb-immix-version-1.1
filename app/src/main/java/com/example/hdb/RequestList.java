package com.example.hdb;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import info.hdb.libraries.UserFunctions;

public class RequestList extends FragmentActivity implements LoaderCallbacks<Cursor> {

	SimpleCursorAdapter mAdapter;
	ListView mListView;
	Button btn_logout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_list);


        mListView = (ListView) findViewById(R.id.listview);

		mAdapter = new SimpleCursorAdapter(getBaseContext(),
                R.layout.listview_item_layout,
                null,
                new String[] { RequestDB.KEY_DIST_KM, RequestDB.KEY_LATITUDE_DIST, RequestDB.KEY_LONGITUDE_DIST},
                new int[] { R.id.code , R.id.txt_one, R.id.txt_two }, 0);

		mListView.setAdapter(mAdapter);

		/** Creating a loader for populating listview from sqlite database */
		/** This statement, invokes the method onCreatedLoader() */
		getSupportLoaderManager().initLoader(0, null, this);

		btn_logout=(Button)findViewById(R.id.btn_logout);

		btn_logout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				UserFunctions userfunction = new UserFunctions();
				@SuppressWarnings("unused")
				boolean islogoutBoolean=false;
				islogoutBoolean=userfunction.logoutUser(getApplicationContext());

				if (islogoutBoolean=true){
				Intent messagingActivity = new Intent(com.example.hdb.RequestList.this,
						LoginActivity.class);
				startActivity(messagingActivity);
				}
			}

		});


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	/** A callback method invoked by the loader when initLoader() is called */
	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		Uri uri = Request.CONTENT_URI;
		return new CursorLoader(this, uri, null, null, null, null);
	}

	/** A callback method, invoked after the requested content provider returned all the data */
	                  @Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		mAdapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		mAdapter.swapCursor(null);
	}
}
