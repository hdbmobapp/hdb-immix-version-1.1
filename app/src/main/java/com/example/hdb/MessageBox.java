package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

public class MessageBox extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject = null;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    static String MESSAGE_SENDER_NAME = "emp_name_from";
    static String MESSAGE_CREATED_DATE = "created_date";
    static String MESSAGE_CONTENT = "message";
    static String MESSAGE_STATUS = "status";
    static String MESSAGE_SUBJECT = "msg_subject";

    static String MESSAGE_ID = "srno";
    TextView txt_error;
    MessageBoxListAdapter adapter;
    ProgressDialog mProgressDialog;
    SharedPreferences pref;
    String str_user_id = null;
    public int pgno = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_box);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);

        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        setlayout();
    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        list = (ListView) findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();
        if (cd.isConnectingToInternet()) {
            new DownloadMessageBox().execute();
        } else {

            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);


        }

    }

    public class DownloadMessageBox extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.MessageBox.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            // jsonobject = userFunction.getDashboard();

            jsonobject = userFunction.getMessageBox(str_user_id,"0");
            if (jsonobject != null) {

                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("MessageBox");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        jsonobject = jsonarray.getJSONObject(i);

                        // Retrive JSON Objects
                        map.put("emp_id_from",
                                jsonobject.getString("emp_id_from"));
                        map.put("emp_id_to", jsonobject.getString("emp_id_to"));
                        map.put("emp_name_to",
                                jsonobject.getString("emp_name_to"));
                        map.put("emp_name_from",
                                jsonobject.getString("emp_name_from"));
                        map.put("created_date",
                                jsonobject.getString("created_date"));
                        map.put("message", jsonobject.getString("message"));
                        map.put("srno", jsonobject.getString("srno"));
                        map.put("msg_subject", jsonobject.getString("msg_subject"));
                        map.put("status", jsonobject.getString("status"));


                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            }
            adapter = new MessageBoxListAdapter(com.example.hdb.MessageBox.this, arraylist);
            list.setAdapter(adapter);
            mProgressDialog.dismiss();

            ((LoadMoreListView) list)
                    .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                        public void onLoadMore() {
                            // Do the work to load more items at the end of list
                            // here
                            new LoadDataTask().execute();
                        }
                    });
        }

    }

    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //jsonobject = userFunction.getVendorOrders(store_id,order_status,tmpgno);


            jsonobject = userFunction.getMessageBox(str_user_id, tmpgno);

            if (jsonobject != null) {
                // Create an array
                try {
                    jsonarray = jsonobject.getJSONArray("MessageBox");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        jsonobject = jsonarray.getJSONObject(i);

                        // Retrive JSON Objects
                        map.put("emp_id_from",
                                jsonobject.getString("emp_id_from"));
                        map.put("emp_id_to", jsonobject.getString("emp_id_to"));
                        map.put("emp_name_to",
                                jsonobject.getString("emp_name_to"));
                        map.put("emp_name_from",
                                jsonobject.getString("emp_name_from"));
                        map.put("created_date",
                                jsonobject.getString("created_date"));
                        map.put("message", jsonobject.getString("message"));
                        map.put("srno", jsonobject.getString("srno"));
                        map.put("msg_subject", jsonobject.getString("msg_subject"));
                        map.put("status", jsonobject.getString("status"));


                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }

                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) list).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) list).onLoadMoreComplete();
        }




    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);
        finish();
    }

    @Override
    public void onBackPressed() {
        // your code.
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }
}