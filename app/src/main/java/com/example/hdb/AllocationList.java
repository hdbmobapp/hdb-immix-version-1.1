package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Avinash on 01-07-2015
 */
public class AllocationList extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10;
    String str_txt1, str_txt2, str_txt3, str_txt4, str_txt5, str_txt6,
            str_txt7, str_txt8, str_txt9, str_txt10;

    static String GRID_REPORT1 = "report1";
    static String GRID_ME_STATUS = "ME_status";
    static String GRID_ME_WEEK = "ME_week";
    static String GRID_LOAN_NO = "Loan_no";
    static String GRID_ME_STATUS_WEEK = "ME_status_week";
    static String GRID_DATE = "updated_date";

    static String GRID_RESI_ADDRESS = "resi_address";
    static String GRID_RESI_CONTACT = "resi_contact";
    static String GRID_OFC_ADDRESS = "ofc_address";
    static String GRID_OFC_CONTACT = "ofc_contact";
    static String GRID_CUST_NAME = "Customer_Name";
    static String GRID_USER_ID = "user_id";
    static String GRID_IS_TODAY = "is_today";
    static String GRID_IS_NEW_ALLOC = "is_new_alloc";

    String report_no;

    AllocationListAdapter adapter;
    ProgressDialog mProgressDialog;
    TextView txt_error;
    SharedPreferences pref;
    static String str_user_id ;
    String str_table_name = null;

    public int pgno = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.allocation_list);

        Intent i = getIntent();
        report_no = i.getStringExtra("report_no");
        str_table_name = i.getStringExtra("table_name");

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        //str_user_id="1219865";

        System.out.println("report_no::::" + report_no);
        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        userFunction.checkforLogout(pref, com.example.hdb.AllocationList.this);

        setlayout();
    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        list = (ListView) findViewById(R.id.list);

        txt1 = (TextView) findViewById(R.id.txtno_1);
        txt2 = (TextView) findViewById(R.id.txtno_2);
        txt3 = (TextView) findViewById(R.id.txtno_3);
        txt4 = (TextView) findViewById(R.id.txt_4);

        arraylist = new ArrayList<HashMap<String, String>>();
        if (cd.isConnectingToInternet()) {
            new DownloadAllocations().execute();
        } else {
            // toast for internet conncection
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);
        }

    }

    public class DownloadAllocations extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.AllocationList.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            txt_error.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonobject = userFunction.getAllocation(str_user_id,"0");
            if (jsonobject != null) {
                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Report");
                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        jsonobject = jsonarray.getJSONObject(i);

                        map.put("report1", jsonobject.getString("report1"));
                        str_txt1 = jsonobject.getString("report1_header");
                        map.put("Loan_no", jsonobject.getString("Loan_no"));
                        map.put("ME_status_week", jsonobject.getString("ME_status_week"));
                        map.put("ME_status", jsonobject.getString("ME_status"));
                        map.put("ME_week", jsonobject.getString("ME_week"));
                        map.put("updated_date", jsonobject.getString("updated_date"));
                        map.put("resi_address", jsonobject.getString("resi_address"));
                        map.put("resi_contact", jsonobject.getString("resi_contact"));
                        map.put("ofc_address", jsonobject.getString("ofc_address"));
                        map.put("ofc_contact", jsonobject.getString("ofc_contact"));
                        map.put("Customer_Name", jsonobject.getString("Customer_Name"));
                        map.put("user_id", jsonobject.getString("user_id"));
                        map.put("is_today", jsonobject.getString("is_today"));
                        map.put("is_new_alloc", jsonobject.getString("is_new_alloc"));


                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
                mProgressDialog.dismiss();
            } else {

                if (str_txt1 != null) {
                    txt1.setVisibility(View.VISIBLE);
                    txt1.setText(str_txt1);
                }
                if (str_txt2 != null) {
                    txt2.setVisibility(View.VISIBLE);
                    txt2.setText(str_txt2);
                }
                if (str_txt3 != null) {
                    txt3.setVisibility(View.VISIBLE);
                    txt3.setText(str_txt3);
                }
                if (str_txt4 != null) {
                    txt4.setVisibility(View.VISIBLE);
                    txt4.setText(str_txt4);
                }
                adapter = new AllocationListAdapter(com.example.hdb.AllocationList.this, arraylist);
                list.setAdapter(adapter);
                mProgressDialog.dismiss();

                ((LoadMoreListView) list)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                new LoadDataTask().execute();
                            }
                        });

            }


        }

    }

    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //jsonobject = userFunction.getVendorOrders(store_id,order_status,tmpgno);


            jsonobject = userFunction.getAllocation(str_user_id, tmpgno);
            if (jsonobject != null) {
                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Report");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();

                        jsonobject = jsonarray.getJSONObject(i);

                        map.put("report1", jsonobject.getString("report1"));
                        str_txt1 = jsonobject.getString("report1_header");
                        map.put("Loan_no", jsonobject.getString("Loan_no"));
                        map.put("ME_status_week", jsonobject.getString("ME_status_week"));
                        map.put("ME_status", jsonobject.getString("ME_status"));
                        map.put("ME_week", jsonobject.getString("ME_week"));
                        map.put("updated_date", jsonobject.getString("updated_date"));
                        map.put("resi_address", jsonobject.getString("resi_address"));
                        map.put("resi_contact", jsonobject.getString("resi_contact"));
                        map.put("ofc_address", jsonobject.getString("ofc_address"));
                        map.put("ofc_contact", jsonobject.getString("ofc_contact"));
                        map.put("Customer_Name", jsonobject.getString("Customer_Name"));
                        map.put("is_today", jsonobject.getString("is_today"));
                        map.put("user_id", jsonobject.getString("user_id"));
                        map.put("is_new_alloc", jsonobject.getString("is_new_alloc"));

                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) list).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) list).onLoadMoreComplete();
        }
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }
}