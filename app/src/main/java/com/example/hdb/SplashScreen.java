package com.example.hdb;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import info.hdb.libraries.ExampleDBHelper;

public class SplashScreen extends Activity {
	SharedPreferences pref;

	ExampleDBHelper eDB;

	RequestDB rDB;
	//String fisrt_run = null;

    /** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

		eDB=new ExampleDBHelper(this);

		rDB=new RequestDB(this);

		eDB.delete_pic();
		//rDB.delete_data();

		checkforLogout();




	    int SPLASH_SCREEN_TIME = 2000;

        new Handler().postDelayed(new Runnable() {

			SharedPreferences settings = getSharedPreferences("prefs", 0);
			boolean firstRun = settings.getBoolean("firstRun", true);

			@Override
			public void run() {
				// This is method will be executed when SPLASH_SCREEN_TIME is
				// over, Now you can call your Home Screen
				SharedPreferences settings = getSharedPreferences("prefs", 0);
				boolean firstRun = settings.getBoolean("firstRun", true);

				if (firstRun) {
					// here run your first-time instructions, for example :
					// startActivityForResult( new Intent(SplashScreen.this,
					// InstructionsActivity.class),INSTRUCTIONS_CODE);
					Intent iHomeScreen = new Intent(com.example.hdb.SplashScreen.this,
							LoginActivity.class);
					startActivity(iHomeScreen);
				} else {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent i = new Intent(com.example.hdb.SplashScreen.this, LoginActivity.class);
					startActivity(i);
				}

				// Finish Current Splash Screen, as it should be visible only
				// once when application start
				finish();
			}
		}, SPLASH_SCREEN_TIME);
	}

	public Void checkforLogout() {
		String str_last_date = pref.getString("current_date", null);

	//	String str_last_date="2016-08-15";

		System.out.println("LAST DATE::::"+str_last_date);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String current_date = sdf.format(new Date());

		Date date1 = null;
		Date date2 = null;

		try {
			if (current_date != null) {
				date1 = sdf.parse(current_date);
			}
			if (str_last_date != null) {
				date2 = sdf.parse(str_last_date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (current_date != null && str_last_date != null) {
			if (date1.after(date2)) {
				System.out.println("Date1 is after Date2");
				Editor editor = pref.edit();
				editor.putInt("is_login", 0);
				editor.putInt("off_is_login", 0);
				editor.commit();
			}
		}
		return null;
	}

}