package com.example.hdb;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;


public class Remarks extends Activity {

    Button btn_search;
    Button btn_home;
    Button btn_dataentry;
    EditText edit_search;
    TextView txt_losid;
    String str_losid;
    private ConnectionDetector cd;
    UserFunctions userFunction;
    String str_designation,str_user_name;
    SharedPreferences pref;

    private static final Pattern LOSID_PATTERN = Pattern
            .compile("[0-9]{1,100}");
    private boolean CheckLOSID(String losid)
    {
        return LOSID_PATTERN.matcher(losid).matches();
    }


    public void onCreate(Bundle savedInstanceState)
    {
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        str_designation= pref.getString("user_designation", "NO");
        str_user_name=pref.getString("user_username", null);

        System.out.println(str_designation);
        System.out.println(str_user_name);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remarks);
        cd = new ConnectionDetector(com.example.hdb.Remarks.this);
        userFunction = new UserFunctions();
        SetLayout();
        btn_home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent home = new Intent(com.example.hdb.Remarks.this,
                        HdbHome.class);
                startActivity(home);
            }
        });




        btn_dataentry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent dataentry = new Intent(com.example.hdb.Remarks.this,
                        EditRequest.class);
                startActivity(dataentry);
            }
        });


        btn_search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    str_losid = null;
                    str_losid = edit_search.getText().toString();

                    if (str_losid.equals(""))
                    {
                        userFunction.cutomToast("LOSID can't be Empty",
                                getApplicationContext());
                    }
                    else {
                        if (!CheckLOSID(str_losid))
                        {
                            userFunction.cutomToast("Plese enter LOSID",
                                    getApplicationContext());
                        }
                        else {
                            Intent i = new Intent(com.example.hdb.Remarks.this, Remarks_Display.class);

                            i.putExtra("losid", str_losid);
                            i.putExtra("parent_id","0");
                           startActivity(i);
                        }
                    }
                }
                else {

                    userFunction.cutomToast("No Internet Connection...",
                            getApplicationContext());
                }

            }
        });

    }
    protected void SetLayout() {
        btn_search = (Button) findViewById(R.id.btn_search);
        btn_home = (Button)findViewById(R.id.txt_home);
        btn_dataentry = (Button) findViewById(R.id.txt_data_entry);
        edit_search=(EditText) findViewById(R.id.edit_search);

    }
}
