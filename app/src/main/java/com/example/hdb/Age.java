package com.example.hdb;

/**
 * Created by Administrator on 21-07-2018.
 */
public class Age {
    public int days;
    public int months;
    public int years;

    private Age()
    {
        //Prevent default constructor
    }

    public Age(int days, int months, int years)
    {
        this.days = days;
        this.months = months;
        this.years = years;
    }

    public int getDays()
    {
        return this.days;
    }

    public int getMonths()
    {
        return this.months;
    }

    public int getYears()
    {
        return this.years;
    }

    @Override
    public String toString()
    {
        return years + " Years, " + months + " Months, " + days + " Days";
    }
}
