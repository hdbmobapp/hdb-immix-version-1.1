package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

public class TodaysDetails extends Activity {
    UserFunctions userFunction;
    JSONObject jsonobject = null;
    private ConnectionDetector cd;
    ListView list;
    ArrayList<HashMap<String, String>> arraylist;
    ArrayList<HashMap<String, String>> todays_arraylist;

    JSONArray jsonarray = null;

    RequestDB rdb;
    String Total_dist_travel = "null";

    TextView txt_todays_date, txt_dist_travel;
    String str_todays_date = null;
    EffReportAdapter adapter;
    ProgressDialog mProgressDialog;
    SharedPreferences pref;
    String str_user_id = null;
    TextView txt_error;

    TextView txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9, txt10;
    String str_txt1 = null, str_txt2 = null, str_txt3 = null, str_txt4 = null, str_txt5 = null, str_txt6 = null,
            str_txt7 = null, str_txt8 = null, str_txt9 = null, str_txt10 = null;

    static String GRID_REPORT1 = "report1";
    static String GRID_REPORT2 = "report2";
    static String GRID_REPORT3 = "report3";
    static String GRID_REPORT4 = "report4";
    static String GRID_REPORT5 = "report5";
    static String GRID_REPORT6 = "report6";
    static String GRID_REPORT7 = "report7";
    static String GRID_REPORT8 = "report8";
    static String GRID_REPORT9 = "report9";
    static String GRID_REPORT10 = "report10";

    static String TODAYS_TOTAL_SUM = "total_sum";
    static String TODAYS_TOTAL_COUNT = "total";
    static String TODAYS_TAG = "tag";


    TodaysDetailsListAdapter tadapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.todays_details);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
      //  str_user_id = pref.getString("user_id", null);

        Intent myIntent = getIntent();
        str_user_id = myIntent.getStringExtra("user_id");

        cd = new ConnectionDetector(getApplicationContext());
        userFunction = new UserFunctions();
        userFunction.checkforLogout(pref, com.example.hdb.TodaysDetails.this);

        setlayout();
    }

    public void setlayout() {
        txt_todays_date = (TextView) findViewById(R.id.text_todays_date);
        txt_dist_travel = (TextView) findViewById(R.id.text_dist_travel);
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        txt1 = (TextView) findViewById(R.id.txtno_1);
        txt2 = (TextView) findViewById(R.id.txt_2);
        txt3 = (TextView) findViewById(R.id.txt_3);
        txt4 = (TextView) findViewById(R.id.txt_4);
        txt5 = (TextView) findViewById(R.id.txt_5);
        txt6 = (TextView) findViewById(R.id.txt_6);
        txt7 = (TextView) findViewById(R.id.txt_7);
        txt8 = (TextView) findViewById(R.id.txt_8);
        txt9 = (TextView) findViewById(R.id.txt_9);
        txt10 = (TextView) findViewById(R.id.txt_10);


        list = (ListView) findViewById(R.id.list);
        arraylist = new ArrayList<HashMap<String, String>>();
        todays_arraylist = new ArrayList<HashMap<String, String>>();

        if (cd.isConnectingToInternet()) {
           runMultipleAsyncTask();
        } else {
            // toast for Internet connection
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }

    }


    public class DownloadGrid extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.TodaysDetails.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonobject = userFunction.getTodaysReport(str_user_id);
            if (jsonobject != null) {


                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Report");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map;
                        map = new HashMap<String, String>();

                        jsonobject = jsonarray.getJSONObject(i);

                        map.put("report1", jsonobject.getString("report1"));
                        str_txt1 = jsonobject.getString("report1_header");
                        map.put("report2", jsonobject.getString("report2"));
                        str_txt2 = jsonobject.getString("report2_header");

                        map.put("report3", jsonobject.getString("report3"));
                        str_txt3 = jsonobject.getString("report3_header");

                        map.put("report4", jsonobject.getString("report4"));
                        str_txt4 = jsonobject.getString("report4_header");

                        map.put("report5", jsonobject.getString("report5"));
                        str_txt5 = jsonobject.getString("report5_header");

                        map.put("report6", jsonobject.getString("report6"));
                        str_txt6 = jsonobject.getString("report6_header");

                        map.put("report7", jsonobject.getString("report7"));
                        str_txt7 = jsonobject.getString("report7_header");

                        map.put("report7", jsonobject.getString("report7"));
                        str_txt7 = jsonobject.getString("report7_header");

                        map.put("report8", jsonobject.getString("report8"));
                        str_txt8 = jsonobject.getString("report8_header");


                        map.put("report9", jsonobject.getString("report9"));
                        str_txt9 = jsonobject.getString("report9_header");

                        map.put("report10", jsonobject.getString("report10"));
                        str_txt10 = jsonobject.getString("report10_header");
                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            txt_error.setVisibility(View.GONE);

            if(!arraylist.isEmpty()) {

        if (str_txt1.equals("null")) {
            txt1.setVisibility(View.GONE);
            txt1.setText(str_txt1);
        }
        if (!str_txt2.equals("null")) {
            txt1.setVisibility(View.VISIBLE);
            txt2.setText(str_txt2);
        }
        if (!str_txt3.equals("null")) {
            txt3.setVisibility(View.VISIBLE);
            txt3.setText(str_txt3);
        }
        if (!str_txt4.equals("null")) {
            txt4.setVisibility(View.VISIBLE);
            txt4.setText(str_txt4);
        }
        if (!str_txt5.equals("null")) {
            txt5.setVisibility(View.VISIBLE);
            txt5.setText(str_txt5);
        }
        if (!str_txt6.equals("null")) {
            txt6.setVisibility(View.VISIBLE);
            txt6.setText(str_txt6);
        }
        if (!str_txt7.equals("null")) {
            txt7.setVisibility(View.VISIBLE);
            txt7.setText(str_txt7);
        }
        if (!str_txt8.equals("null")) {
            txt8.setVisibility(View.VISIBLE);
            txt8.setText(str_txt8);
            System.out.println("str_txt_8:::" + str_txt8);
        }
        if (!str_txt9.equals("null")) {
            txt9.setVisibility(View.VISIBLE);
            txt9.setText(str_txt9);
        }
        if (!str_txt10.equals("null")) {
            txt10.setVisibility(View.VISIBLE);
            txt10.setText(str_txt10);
        }
        adapter = new EffReportAdapter(com.example.hdb.TodaysDetails.this, arraylist);
        list.setAdapter(adapter);
        ListUtils.setDynamicHeight(list);
    }
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            }



            mProgressDialog.dismiss();


        }

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }


    public class DownloadTodaysDetails extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
//                mProgressDialog = new ProgressDialog(TodaysDetails.this);
//                mProgressDialog.setMessage(getString(R.string.plz_wait));
//                mProgressDialog.setCancelable(true);
//                mProgressDialog.setIndeterminate(true);
//                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            jsonobject = userFunction.getTodaysReportInfo(str_user_id);
            // Create an array
            if (jsonobject != null) try {

                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("Data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map;
                    map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);

                    // Retrive JSON Objects
                    map.put("total", jsonobject.getString("total"));
                    map.put("tag", jsonobject.getString("tag"));
                    map.put("total_sum", jsonobject.getString("total_sum"));


                    str_todays_date = jsonobject.getString("today_date");
                    Total_dist_travel = jsonobject.getString("total_sum");
                    // Set the JSON Objects into the array
                    todays_arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            ListView listview = (ListView) findViewById(R.id.tlistview);
            txt_error.setVisibility(View.GONE);

            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(
                        R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            }else {

                txt_todays_date.setText(str_todays_date);
                double dbl_dist_travl;

                if (Total_dist_travel== null && Total_dist_travel.isEmpty() && Total_dist_travel.equals("null")) {
                    dbl_dist_travl = 0;
                } else {
                    dbl_dist_travl = Double.parseDouble(Total_dist_travel);
                }

                /*
                Location nwLocation = appLocationService
                        .getLocation(LocationManager.NETWORK_PROVIDER);

                if (nwLocation != null) {
                    dbl_dist_travl = Double.parseDouble(Total_dist_travel);
                } else {
                    dbl_dist_travl = 0;
                }
                */

              /*  Total_dist_travel = String.format("%.2f", dbl_dist_travl);
                txt_dist_travel.setText("Distance Travel - " + Total_dist_travel
                        + "Km");*/


                tadapter = new TodaysDetailsListAdapter(com.example.hdb.TodaysDetails.this,
                        todays_arraylist);
                listview.setAdapter(tadapter);
                ListUtils.setDynamicHeight(listview);
            }
        }

    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = MeasureSpec.makeMeasureSpec(mListView.getWidth(), MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    private void runMultipleAsyncTask() // Run Multiple Async Task
    {
        DownloadGrid asyncTask = new DownloadGrid(); // First
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) // Above Api Level 13
        {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else // Below Api Level 13
        {
            asyncTask.execute();
        }
        DownloadTodaysDetails asyncTask2 = new DownloadTodaysDetails(); // Second
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)// Above Api Level 13
        {
            asyncTask2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else // Below Api Level 13
        {
            asyncTask2.execute();
        }
    }
}
