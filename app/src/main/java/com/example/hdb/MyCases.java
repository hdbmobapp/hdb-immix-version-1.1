package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.LoadMoreListView;
import info.hdb.libraries.UserFunctions;

public class MyCases extends Activity {

    // Within which the entire activity is enclosed

    // ListView represents Navigation Drawer
    ListView mList;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;

    // Title of the action bar
    String mTitle = "";
    static LinearLayout drawerll;
    static String str_region;
    SharedPreferences pref;

    String str_user_id;
    String str_user_name;

    MyCasesListAdapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunctions userFunction;

    static String CaseList= "CaseList";

    UserFunctions userfunction;
    String acm_id;
    String acm_name;
    String acmjsonobject;
    String str_from;
    String str_supervisor_id = null;
    TextView txt_error;
    public int pgno = 0;


    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_cases);

        textView = (TextView) findViewById(R.id.simpleTextView);
        // set Text in TextView using fromHtml() method with version check
        //textView.setText(Html.fromHtml(content));

        mTitle = (String) getTitle();
        userFunction = new UserFunctions();
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        userfunction = new UserFunctions();

        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);

        System.out.println("user_id:::" + str_user_id);

        Intent i = getIntent();
        str_region = i.getStringExtra("str_region");

        setlayout();

        arraylist = new ArrayList<HashMap<String, String>>();

        cd = new ConnectionDetector(com.example.hdb.MyCases.this);

        if (cd.isConnectingToInternet()) {

            new DownloadJSON().execute();
        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }
        // Setting the adapter on mDrawerList
        // mDrawerList.setAdapter(adapter);

    }

    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        mList = (ListView) findViewById(R.id.list);

    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.MyCases.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            txt_error.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            jsonobject = userFunction.getCaseList(str_user_id,"0");
            if (jsonobject != null) {
                // Create an array
                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Data");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();
                        jsonobject = jsonarray.getJSONObject(i);
                        map.put("CaseList", jsonobject.getString("CaseList"));




                        // Set the JSON Objects into the array
                        arraylist.add(map);
                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                adapter = new MyCasesListAdapter(com.example.hdb.MyCases.this, arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

                System.out.println("INSIDE::: post excdf:");
                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });
            }
            mProgressDialog.dismiss();

        }

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);

    }

    public void go_dataEntry_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                EditRequest.class);
        startActivity(home_activity);

    }
    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //jsonobject = userFunction.getVendorOrders(store_id,order_status,tmpgno);
            jsonobject = userFunction.getCaseList(str_user_id,tmpgno);


            // Create an array
            if (jsonobject != null) try {
                jsonarray = jsonobject.getJSONArray("Data");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("CaseList", jsonobject.getString("CaseList"));



                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }
}
