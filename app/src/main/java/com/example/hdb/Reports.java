package com.example.hdb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import info.hdb.libraries.ConnectionDetector;

/**
 * Created by Administrator on 07-08-2018.
 */
public class Reports extends AppCompatActivity {

	ConnectionDetector cd;
	SharedPreferences pref;
	String str_user_id,str_user_name;
	private ProgressBar mProgressBar,progress_bar;
	WebView webView;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reports);
		cd = new ConnectionDetector(com.example.hdb.Reports.this);

		pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
		str_user_id = pref.getString("user_id", null);
		str_user_name = pref.getString("user_name", null);
		System.out.println("str_user_id"+str_user_id+"str_user_name"+str_user_name);
		webView = (WebView)findViewById(R.id.webview);
		mProgressBar = (ProgressBar) findViewById(R.id.pb);
		progress_bar = (ProgressBar) findViewById(R.id.progress_bar);


		webView.getSettings().setJavaScriptEnabled(true);
		// webView.getSettings().sets(true);

		String url="https://hdbapp.hdbfs.com/Web_Pages/Collx_App/reports.php?user_id="+str_user_id;
		if (cd.isConnectingToInternet()) {
			renderWebPage(url);
			// webView.loadUrl(url);
		}else
		{
			Toast.makeText(com.example.hdb.Reports.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

		}

	}



	// Custom method to render a web page
	protected void renderWebPage(String urlToRender){
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// Do something on page loading started
				// Visible the progressbar
				mProgressBar.setVisibility(View.VISIBLE);
				progress_bar.setVisibility(View.VISIBLE);


			}

			@Override
			public void onPageFinished(WebView view, String url) {
				// Do something when page loading finished
				Toast.makeText(getApplicationContext(), "Page Loaded.", Toast.LENGTH_SHORT).show();
			}

		});

        /*
            WebView
                A View that displays web pages. This class is the basis upon which you can roll your
                own web browser or simply display some online content within your Activity. It uses
                the WebKit rendering engine to display web pages and includes methods to navigate
                forward and backward through a history, zoom in and out, perform text searches and more.

            WebChromeClient
                 WebChromeClient is called when something that might impact a browser UI happens,
                 for instance, progress updates and JavaScript alerts are sent here.
        */
		webView.setWebChromeClient(new WebChromeClient() {
			/*
                public void onProgressChanged (WebView view, int newProgress)
                    Tell the host application the current progress of loading a page.

                Parameters
                    view : The WebView that initiated the callback.
                    newProgress : Current page loading progress, represented by an integer
                        between 0 and 100.
            */
			public void onProgressChanged(WebView view, int newProgress) {
				// Update the progress bar with page loading progress
				mProgressBar.setProgress(newProgress);
				if (newProgress == 100) {
					// Hide the progressbar
					mProgressBar.setVisibility(View.GONE);
					progress_bar.setVisibility(View.GONE);
				}
			}
		});

		// Enable the javascript
		webView.getSettings().setJavaScriptEnabled(true);
		// Render the web page
		webView.loadUrl(urlToRender);
	}

	public void go_home_acivity(View v) {
		// do stuff
		Intent home_activity = new Intent(getApplicationContext(),
				HdbHome.class);
		startActivity(home_activity);
		finish();

	}
}
