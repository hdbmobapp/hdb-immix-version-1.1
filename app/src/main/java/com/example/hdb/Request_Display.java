package com.example.hdb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.ConnectionDetector;
import info.hdb.libraries.UserFunctions;

/**
 * Created by Administrator on 17-08-2015.
 */
public class Request_Display extends Activity {

    TextView txt_losid;
    TextView txt_remark;
    TextView txt_remark_date;
    ImageButton btn_reply_remark;
    String str_losid;

    String str_designation;
    String str_user_name;
    String str_user_id;
    String str_remark_by_id;
    String str_remark;
    String str_remark_date;
    String str_parent_id;
    String str_remark_id;
    String page="b";




    UserFunctions user_function;
    AllocationListAdapter adapter;
    ProgressDialog mProgressDialog;
    TextView txt_error;
    SharedPreferences pref;
    String newString;
    JSONObject jsonobject;
    private ConnectionDetector cd;

    public ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;

    static String REMARK = "Remark";
    static String REMARK_BY = "Remark_By";
    static String REMARK_DATE = "Remark_Date";
    static String REMARK_BY_ID = "Remark_By_ID";

    ListView list;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_display);

        UserFunctions user_function = new UserFunctions();

        Intent get_losid = getIntent();
        str_losid = get_losid.getStringExtra("losid");



        str_remark_by_id = get_losid.getStringExtra("remark_by_id");
        str_remark_id = get_losid.getStringExtra("remark_id");
       //str_remark_id= get_losid.getStringExtra("parentid1");

        System.out.println("remark is=======" + str_remark_id);

        System.out.println("AVS" + str_losid + str_parent_id + str_remark_by_id);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        str_designation = pref.getString("user_designation", "NO");
        str_user_name = pref.getString("user_name", null);
        str_user_id = pref.getString("user_id", null);
        setlayout();

        user_function.cutomToast("User ID:" + str_remark_by_id,
                getApplicationContext());
        btn_reply_remark = (ImageButton) findViewById(R.id.reply_remark);

        btn_reply_remark.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent new_remark = new Intent(com.example.hdb.Request_Display.this,
                        New_Remark.class);
                new_remark.putExtra("parentid1", str_remark_id);
                new_remark.putExtra("losid", str_losid);

                new_remark.putExtra("pageid",page);

                startActivity(new_remark);
            }
        });

    }


    public void setlayout() {
        txt_losid = (TextView) findViewById(R.id.txt_losid);
        txt_remark = (TextView) findViewById(R.id.txt_remark);
        txt_remark_date = (TextView) findViewById(R.id.txt_remark_date);
        list = (ListView) findViewById(R.id.listView2);
        arraylist = new ArrayList<HashMap<String, String>>();


        Intent ii = getIntent();
        // newString = ii.getStringExtra("parentid");


        txt_losid.setText(str_losid);

        user_function = new UserFunctions();
        new Downloadremarks().execute();

    }

    public class Downloadremarks extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(com.example.hdb.Request_Display.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //fetching extra data passed with intents in a Bundle type variable

            System.out.println("REMARKID::::" + str_remark_id);

            jsonobject = user_function.getRemarks(str_losid, str_remark_id);


            if (jsonobject != null) {

                try {
                    // Locate the array name in JSON
                    jsonarray = jsonobject.getJSONArray("Remarks");
//                    txt_error.setVisibility(View.GONE);

                    for (int i = 0; i < jsonarray.length(); i++) {
                        HashMap<String, String> map = new HashMap<String, String>();

                        jsonobject = jsonarray.getJSONObject(i);
                        System.out.println("json object::::" + jsonobject);

                        map.put("Remark", jsonobject.getString("Remark"));
                        map.put("Remark_By", jsonobject.getString("Emp_name"));
                        map.put("Remark_Date", jsonobject.getString("Remark_Date"));
                        map.put("Remark_By_ID", jsonobject.getString("Remark_By"));
                        str_losid = jsonobject.getString("Loan_No");
                        str_remark = jsonobject.getString("Loan_No");
                        str_remark_date = jsonobject.getString("Loan_No");


                        // Set the JSON Objects into the array
                        arraylist.add(map);

                    }
                } catch (JSONException e) {
                    Log.e("Error", e.getMessage());
                }
                //e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
                user_function.cutomToast("Something went wrong. Please try after sometime",
                        getApplicationContext());
                finish();
            } else {

                txt_losid.setText(str_losid);
                txt_remark.setText(str_remark);
                txt_remark_date.setText(str_remark_date);


                RequestListAdapter adapter = new RequestListAdapter(com.example.hdb.Request_Display.this, arraylist);
                list.setAdapter(adapter);
                mProgressDialog.dismiss();

            }


        }
    }
}
