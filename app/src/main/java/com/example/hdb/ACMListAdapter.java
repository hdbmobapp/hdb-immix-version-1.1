package com.example.hdb;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import info.hdb.libraries.UserFunctions;

public class ACMListAdapter extends BaseAdapter {

	// Declare Variables
	ProgressDialog dialog = null;
	ProgressDialog mProgressDialog;
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	UserFunctions userFunction;
	String str_product_id;
	ArrayAdapter<String> adapter;

	UserFunctions user_function;
	JSONObject acmjsonobject;

	public ACMListAdapter(Context context,
			ArrayList<HashMap<String, String>> arraylist, JSONObject jsonobject) {
		this.context = context;
		data = arraylist;
		dialog = new ProgressDialog(context);
		user_function = new UserFunctions();
		acmjsonobject = jsonobject;
	}

	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.group_item, parent, false);
		// Get the position
		resultp = data.get(position);

		TextView tv_name = (TextView) itemView.findViewById(R.id.group_name);
		TextView tv_total_users = (TextView) itemView
				.findViewById(R.id.txt_total_users_cnt);
		TextView tv_active_users = (TextView) itemView
				.findViewById(R.id.txt_acive_users);
		TextView tv_total_punches = (TextView) itemView
				.findViewById(R.id.txt_total_punches);
		TextView tv_total_pay = (TextView) itemView
				.findViewById(R.id.txt_total_pay);
		TextView tv_locate_me = (TextView) itemView
				.findViewById(R.id.txt_locate);

		TextView tv_exp = (TextView) itemView.findViewById(R.id.indicator_icon);
		tv_exp.setVisibility(View.GONE);
		tv_name.setTypeface(null, Typeface.NORMAL);

//		if (resultp.get(ACMActivity.ACM_FLAG).compareTo("A") == 0) {
//			tv_locate_me.setVisibility(View.GONE);
//		}
		System.out.println("ALLLL:::"
				+ resultp.get(ACMActivity.ACM_TOTAL_USERS));
		tv_name.setText(resultp.get(ACMActivity.ACM_NAME));
		tv_total_users.setText("Users  "
				+ resultp.get(ACMActivity.ACM_TOTAL_USERS));
		tv_active_users.setText("A Users  "
				+ resultp.get(ACMActivity.ACM_ACTIVE_USERS));
		tv_total_punches.setText("Punches "
				+ resultp.get(ACMActivity.ACM_TOTAL_PUNCHES));
		tv_total_pay.setText("Pay " + resultp.get(ACMActivity.ACM_TOTAL_PAY));

		userFunction = new UserFunctions();

		tv_locate_me.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);
				resultp = data.get(position);

				
				final double latitude = Double.parseDouble(resultp.get(ACMActivity.ACM_LATITUDE).toString());
				final double longitude =Double.parseDouble(resultp.get(ACMActivity.ACM_LONGITUDE).toString());
				final String label = resultp.get(ACMActivity.ACM_DATETIME).toString();
				if(latitude==0){
					show();
				}

				new Thread(new Runnable() {
					public void run() {
						// Get the position
						
						
						String uriBegin = "geo:" + latitude + "," + longitude;
						String query = latitude + "," + longitude + "(" + label
								+ ")";
						String encodedQuery = Uri.encode(query);
						String uriString = uriBegin + "?q=" + encodedQuery
								+ "&z=16";
						Uri uri = Uri.parse(uriString);
                if(latitude>0){
						Intent intent = new Intent(
								Intent.ACTION_VIEW, uri);

						try {
							context.startActivity(intent);
						} catch (ActivityNotFoundException ex) {
							try {
								Intent unrestrictedIntent = new Intent(
										Intent.ACTION_VIEW, uri);
								context.startActivity(unrestrictedIntent);
							} catch (ActivityNotFoundException innerEx) {
								Toast.makeText(context,
										"Please install a maps application",
										Toast.LENGTH_LONG).show();
							}
						}
}

					}
				}).start();
			}

		});
		itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				arg0.setSelected(true);

				new Thread(new Runnable() {
					public void run() {
						// Get the position
						resultp = data.get(position);
						Intent intent = new Intent(context, ACMActivity.class);
						if (resultp.get(ACMActivity.ACM_FLAG).compareTo("A") == 0) {
							intent.putExtra("from", "sds");
							intent.putExtra("supervisor_id",
									resultp.get(ACMActivity.ACM_USER_ID));
							intent.putExtra("str_region",
									resultp.get(ACMActivity.ACM_REGION));
							context.startActivity(intent);
						}
					}
				}).start();
			}

		});

		return itemView;
	}
	public void show() {
		Toast.makeText(context, "cant find ....",
				Toast.LENGTH_LONG).show();
		
	}
}
