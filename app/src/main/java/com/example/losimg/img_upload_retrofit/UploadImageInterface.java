package com.example.losimg.img_upload_retrofit;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageInterface {
    @Multipart
    @POST("/HDB_DMS_UPLD/HDB_DMS_UPLD_V4.5/pdf_upload.php")
    Call<UploadObject> uploadFile(@Part MultipartBody.Part file, @Part("name") RequestBody name, @Part("losid") RequestBody losid, @Part("ts") RequestBody ts, @Part("emp_id") RequestBody empid);
}