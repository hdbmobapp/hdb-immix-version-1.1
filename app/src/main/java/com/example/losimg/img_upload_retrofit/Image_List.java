package com.example.losimg.img_upload_retrofit;

/**
 * Created by Administrator on 12-06-2017.
 */


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.example.losimg.MainModule.HdbHome;
import com.example.losimg.MainModule.LoginActivity;

import com.example.losimg.libraries.ConnectionDetector;
import com.example.losimg.libraries.DBHelper;
import com.example.losimg.libraries.GPSTracker_New;
import com.example.losimg.libraries.MyAdapter;
import com.example.losimg.libraries.UserFunction;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by DELL on 6/3/2017.
 */

public class Image_List extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    File file;
    int REQUEST_CAMERA = 100;
    private String imgPath;
    //private static final String IMAGE_DIRECTORY_NAME = "/DEALER";
    int pic_position = 0;
    SharedPreferences pData;

    Integer img_width, img_height, img_com_size;
    ProgressDialog mProgressDialog;

    String str_pic_type;

    static String str_full_structure_1, str_full_structure_2, str_full_structure_3, str_full_structure_4, str_full_structure_5, str_full_structure_6;

    String str_total_pic_count, str_pic_name;
    Bitmap my_bitmp = null;
    DBHelper dbHelper;
    Spinner spn_pic_catpure;
    Button btn_submit;
    String str_position;
    ArrayList<String> cu_pic_values, pic_list, pic_values, pic_type;
    ArrayList<String> cu_pic_names, pic_hight, pic_width, pic_size, pic_pages;


    int curr_DOC_no = 0;

    private Uri url_file;

    File new_file = null;

    DBHelper dbHelp;
    Integer total_pic_count;

    static String user_id = null, str_ts = null, str_losid = null, str_emp_id = null, app_type, str_timestamp;

    String doc_type, str_doc_Type;
    JSONObject jsonobject;
    String str_uri;
    static String ssstr_img_path;
    ConnectionDetector cd;

    UserFunction userfunction;


    protected ImageLoader img_loader;
    Boolean update;
    ArrayList<String> list;
    ArrayList<String> list1;
    String str_cust_id, str_barnch_id, str_barnch;

    Double dbl_longitude, dbl_latitude;
    String str_pic_catpure_list = null;
    String str_pic_catpure_list1 = null;
    ArrayAdapter<String> adapter_pic_catpure;
    TextView txt_img_title;
    String str_app_img_type;
    SharedPreferences pref;
    String app_type_nw;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelp = new DBHelper(this);
        setContentView(R.layout.dms_activity_main_old);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        img_loader = ImageLoader.getInstance();
        img_loader.init(ImageLoaderConfiguration.createDefault(this));
        cd = new ConnectionDetector(this);

        userfunction = new UserFunction(this);

        GPSTracker_New tracker = new GPSTracker_New(Image_List.this);


        // check if location is available

        if (tracker.isLocationEnabled()) {
            dbl_latitude = tracker.getLatitude();
            dbl_longitude = tracker.getLongitude();

        } else {
            // show dialog box to user to enable location
            tracker.askToOnLocation();
        }

        txt_img_title = (TextView) findViewById(R.id.txt_img_title);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_emp_id = pref.getString("user_id", null);


        Intent i = getIntent();
        str_losid = i.getStringExtra("str_losid");
        app_type = i.getStringExtra("app_type");
        str_cust_id = i.getStringExtra("str_cust_id");
        app_type_nw = i.getStringExtra("app_type_nw");
        str_ts = str_cust_id;

        System.out.println("str_losid::::"+str_losid+":::app_type:::"+app_type+"::::str_cust_id:::"+str_cust_id+":::::"+app_type_nw);
        cu_pic_values = new ArrayList<String>();
        cu_pic_names = new ArrayList<String>();
        pic_type = new ArrayList<String>();
        pic_hight = new ArrayList<String>();
        pic_width = new ArrayList<String>();
        pic_size = new ArrayList<String>();
        pic_pages = new ArrayList<String>();


        cu_pic_values.add("");

        cu_pic_names.add("Select Doc Type ");

        pic_type.add("");
        pic_hight.add("0");
        pic_width.add("0");
        pic_size.add("0");
        pic_pages.add("0");

        new Get_img_master().execute();

        System.out.println("cust typ::::" + app_type);


        // cu_pic_values=dbHelp.get_img_list_app_id(app_type);
        // cu_pic_names=dbHelp.get_img_list_app_val(app_type);


        System.out.println("values::::" + cu_pic_values);

        pic_list = cu_pic_names;
        pic_values = cu_pic_values;
        list = pic_values;

        list1 = pic_list;
        new ArrayList<String>();
        System.out.println("tsssss::::" + str_ts);
        //  user_id = i.getStringExtra("user_id");
        //dbHelp.delete_IMGo(str_ts);
        pData = this.getSharedPreferences("pData", 0);
        pref = this.getSharedPreferences("MyPref", 0);
        str_cust_id = pref.getString("cust_id", null);
        str_barnch_id = pref.getString("branchid", null);
        str_barnch = pref.getString("user_brnch", null);
        user_id = pref.getString("user_id", null);

        if (app_type.equals("P")) {
            str_app_img_type = "Primary Applicant";

        } else {
            if (app_type.equals("C")) {
                str_app_img_type = "Co-Applicant";
            } else {
                str_app_img_type = "Guarantor";
            }


        }

        txt_img_title.setText("Image Upload");
        str_full_structure_1 = "";
        str_full_structure_2 = "";
        str_full_structure_3 = "";
        str_full_structure_4 = "";


        ImageView btnSelectPhoto = (ImageView) findViewById(R.id.btnSelectPhoto);
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_pic_catpure_list != null) {
                    str_timestamp = getDateTime();

                    takePicture();
/*
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            getOutputMediaFile(MEDIA_TYPE_IMAGE));
                    startActivityForResult(intent, REQUEST_CAMERA);
                    */
                } else {
                 //   Toast.makeText(Image_List.this, "Select Image category.....", Toast.LENGTH_LONG)
                 //           .show();
                }
            }

        });


        btn_submit = (Button) findViewById(R.id.btnsubmitpic);


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ins = true;
                String img_cnt = String.valueOf(total_pic_count);


                if (ins == true) {

                    // new Update_count().execute();
                    Boolean status_co = dbHelp.update_status(str_ts, 1, str_total_pic_count);
                    if (status_co == true) {

                        System.out.println("str_ts::::" + str_ts);
                        Intent cust = new Intent(Image_List.this, ImagesList.class);
                        cust.putExtra("ts", str_ts);
                        startActivity(cust);
                        finish();
                    } else {

                    }


                }
            }


        });


        spn_pic_catpure = (Spinner) findViewById(R.id.spn_pic_capture);

        //  ivImage = (ImageView) findViewById(R.id.ivImage);

        adapter_pic_catpure = new MyAdapter(this,
                R.layout.sp_display_layout, R.id.txt_visit, list1);
        adapter_pic_catpure
                .setDropDownViewResource(R.layout.spinner_layout);
        spn_pic_catpure.setAdapter(adapter_pic_catpure);
        spn_pic_catpure.setOnItemSelectedListener(this);

        setlistview();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == REQUEST_CAMERA) {
                if (resultCode == Activity.RESULT_OK) {
                    SharedPreferences.Editor editor = pData.edit();


                    str_uri = pref.getString("str_uri", null);
                    url_file = Uri.parse(str_uri);

                    String str_img_path = url_file.toString();
                    if (url_file != null) {

                        File new_img_file = new File(url_file.toString());
                        str_img_path = new_img_file.getAbsolutePath();

                        try {
                            str_img_path = str_img_path.replace("/file:", "");

                            System.out.println("img_height:::" + img_height + ":::img_width::::" + img_width + "::::img_com_size::::" + img_com_size);
                            Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(str_img_path, img_height, img_width, user_id, str_losid, str_ts, img_com_size, dbl_latitude, dbl_longitude, getDateTime());
                            System.out.println("img_height:::" + img_height + ":::img_width::::" + img_width + "::::str_pic_catpure_list::::" + str_pic_catpure_list);

                            ssstr_img_path = storeImage(bitmap, user_id, str_losid, str_ts, str_pic_catpure_list, str_doc_Type, img_com_size);

                        } catch (Exception e) {
                            ssstr_img_path = new_img_file.getAbsolutePath();
                            ssstr_img_path = ssstr_img_path.replace("/file:", "");
                        }


                    }

                    if (str_uri != null) {
                        // show the result here.
                        if (dbHelper.insertImage(ssstr_img_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list,"1","1",str_timestamp)) {
                            //  Toast.makeText(getApplicationContext(), "iMAGE Inserted", Toast.LENGTH_SHORT).show();

                            boolean ss = list.contains(str_pic_catpure_list);
                            if (ss == true) {
                                /*
                                list.remove(str_pic_catpure_list);
                                list1.remove(str_pic_catpure_list1);
                                pic_type.remove(Integer.parseInt(doc_type));
                                pic_hight.remove(Integer.parseInt(doc_type));
                                pic_width.remove(Integer.parseInt(doc_type));
                                pic_size.remove(Integer.parseInt(doc_type));


                                adapter_pic_catpure = new ArrayAdapter<>(this,
                                        android.R.layout.simple_spinner_dropdown_item, list1);
                                adapter_pic_catpure
                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spn_pic_catpure.setAdapter(adapter_pic_catpure);
                                spn_pic_catpure.setOnItemSelectedListener(this);
*/

                                // Toast.makeText(getApplicationContext(), "IMAGE", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Could not Insert IMAGE", Toast.LENGTH_SHORT).show();
                        }
                        setlistview();
                    }

                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Delete the cancelled image

                try {
                   // deleteLastPic();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }


                // user cancelled Image capture
                Toast.makeText(this,
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(this,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_pic_capture:

                System.out.println("::::multiple_image:::" + pic_pages.get(position));

                // str_pic_catpure_list = pic_values.get(pic_list.indexOf(list1.get(position)));
                str_pic_catpure_list = list.get(position);
                str_pic_catpure_list1 = list1.get(position);

                doc_type = String.valueOf(position);
                str_doc_Type = pic_type.get(position);

                System.out.println("str_pic_catpure_list:::::" + str_pic_catpure_list + "::::str_pic_catpure_list1" + str_pic_catpure_list + "doc_type::::" + doc_type + ":::str_doc_Type::::" + str_doc_Type);

                img_width = Integer.parseInt(pic_width.get(position));
                img_height = Integer.parseInt(pic_hight.get(position));
                img_com_size = Integer.parseInt(pic_size.get(position));

                str_position = String.valueOf(position);
                str_pic_name = list1.get(position);

                pic_position = Integer.parseInt(str_position);
                System.out.println("position:::::" + list1.get(position));
                if (position == 0) {
                    str_pic_catpure_list = null;
                }

                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("str_pic_name", str_pic_name);
                peditor.putString("str_pic_catpure_list", str_pic_catpure_list);

                peditor.commit();
                Integer i_count = Integer.parseInt(pic_pages.get(position).toString().trim());
                System.out.println("i_count:::::" + i_count);
                if (i_count >= 1) {

                    // Create custom dialog object
                    final Dialog dialog = new Dialog(Image_List.this);
                    // Include dialog.xml file
                    dialog.setContentView(R.layout.dms_image_dialog);
                    // Set dialog title
                    dialog.setTitle("Custom Dialog");

                    // set values for custom dialog components - text, image and button
                    final EditText edt_no_of_images = (EditText) dialog.findViewById(R.id.edt_no_of_img);

                    dialog.show();

                    Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
                    // if decline button is clicked, close the custom dialog
                    declineButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Close dialog

                            if (!TextUtils.isEmpty(edt_no_of_images.getText().toString())) {

                                if (Integer.parseInt(edt_no_of_images.getText().toString()) > 0 && Integer.parseInt(edt_no_of_images.getText().toString())<25 ) {
                                    dialog.dismiss();

                                    Intent grid_act = new Intent(getApplicationContext(),
                                            Image_grid.class);
                                    grid_act.putExtra("no_of_img",edt_no_of_images.getText().toString());
                                    grid_act.putExtra("user_id",user_id);
                                    grid_act.putExtra("str_losid",str_losid);
                                    grid_act.putExtra("str_ts",str_ts);
                                    grid_act.putExtra("str_pic_catpure_list",str_pic_catpure_list);
                                    grid_act.putExtra("str_doc_Type",doc_type);
                                    grid_act.putExtra("str_doc_name",str_pic_name);
                                    grid_act.putExtra("img_com_size",img_com_size);
                                    grid_act.putExtra("img_width",img_width);
                                    grid_act.putExtra("img_height",img_height);
                                    grid_act.putExtra("app_type",app_type);


                                    grid_act.putExtra("no_of_img",edt_no_of_images.getText().toString());

                                    startActivity(grid_act);
                                } else {
                                    userfunction.cutomToast("Enter valid documents count", Image_List.this);

                                }
                            } else {

                                userfunction.cutomToast("No of Documents Cant be Empty ", Image_List.this);
                            }

                        }
                    });
                }


                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    public void setlistview() {
        dbHelper = new DBHelper(this);



        final Cursor cursor = dbHelper.getAllImages_new(str_ts);
        total_pic_count = cursor.getCount();
        System.out.println("str_ts:::::::"+str_ts+":::total_pic_count::::"+total_pic_count);

        GridView lvItems = (GridView) findViewById(R.id.listView1);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageListAdapter todoAdapter = new ImageListAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
       /* Integer int_map_count = Integer.parseInt(map_count);
        int_total_pic = total_pic_count;
        if (int_map_count > 0) {
            total_pic_count = int_total_pic - 1;
        }*/
        str_total_pic_count = String.valueOf(total_pic_count);
    }


    public void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File new_file3 = getOutputMediaFile2(user_id, str_losid, str_ts, str_pic_catpure_list, str_doc_Type);
        System.out.println(":::::111111" + url_file);
        url_file = Uri.fromFile(new_file3);
        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove(str_uri);
        peditor.commit();
        str_uri = url_file.toString();
        peditor.putString("str_uri", str_uri);
        peditor.commit();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        startActivityForResult(intent, 100);


    }

    private File getOutputMediaFile1235() {

        String curr_no = null;
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
        /*
        File mediaStorageDir = new File(Environment.getExternallStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
                 */
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }

        Uri imgUri = null;
        try {

            curr_no = Integer.toString(pData.getInt("curr_DOC_no", 0));

            file = new File(mediaStorageDir, str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + str_pic_catpure_list + ".jpeg");

            if (file.exists()) {
                file.delete();
            }
            imgUri = Uri.fromFile(file);
            this.imgPath = file.getAbsolutePath();
            System.out.println(":::::imgPath::::::/.avia:::" + imgPath);
            SharedPreferences.Editor editor = pData.edit();

            editor.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }


        new_file = new File(mediaStorageDir.getPath() + File.separator + str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + curr_no + ".JPEG");
        return new_file;
    }

    private String storeImage(Bitmap image, String userid, String losid, String ts, String str_pic_name, String doc_type, Integer img_com_size) {
        String new_strMyImagePath = null;
        String pic_id = str_pic_name.toString();
        File pictureFile = getOutputMediaFile2(userid, losid, ts, pic_id, doc_type);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {

                if (img_com_size > 0) {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    image.compress(Bitmap.CompressFormat.JPEG, img_com_size, fos);
                    fos.close();
                }
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

        }

        return new_strMyImagePath;
    }

    private File getOutputMediaFile2(String userid, String str_losid, String ts, String str_pic_name, String doc_type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = str_losid + "_" + userid + "_" + ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + doc_type + "_" + str_pic_name + "_0_0.JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    public class Get_img_master extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Image_List.this);
            mProgressDialog.setMessage(getApplicationContext().getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            jsonobject = null;
            jsonobject = userfunction.get_img_master(str_ts, app_type_nw, str_emp_id);
            // Create an array

            return null;
        }


        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString("status") != null) {
                        System.out.println("jsonobject:::" + jsonobject);

                        String res = jsonobject.getString("status");
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            JSONArray jsonarray = jsonobject.getJSONArray("Data");

                            for (int i = 0; i < jsonarray.length(); i++) {

                                jsonobject = jsonarray.getJSONObject(i);
                                cu_pic_values.add(jsonobject.getString("doc_id"));
                                cu_pic_names.add(jsonobject.getString("doc_name"));
                                pic_type.add(jsonobject.getString("doc_type"));
                                pic_hight.add(jsonobject.getString("img_hight"));
                                pic_width.add(jsonobject.getString("img_width"));
                                pic_size.add(jsonobject.getString("image_quality"));
                                pic_pages.add(jsonobject.getString("multiple_page"));


                            }

                            mProgressDialog.dismiss();
                        }

                    }
                } else {

                    mProgressDialog.dismiss();

                    userfunction
                            .cutomToast("Something gets wrong with connectivity..", getApplicationContext());
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void go_home(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());

        // str_userid = str_userid.replace("HDB","");

        return format;
    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }



}
