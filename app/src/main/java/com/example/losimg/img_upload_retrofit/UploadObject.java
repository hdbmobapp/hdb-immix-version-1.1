package com.example.losimg.img_upload_retrofit;

/**
 * Created by DELL on 11/22/2017.
 */

public class UploadObject {
    private String success;
    private Integer status;

    public UploadObject(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }

    public Integer getStatus() {
        return status;
    }
}