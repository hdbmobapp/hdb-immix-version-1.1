package com.example.losimg.img_upload_retrofit;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;


import com.example.losimg.libraries.DBHelper;
import com.example.losimg.libraries.GPSTracker_New;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import retrofit2.Call;


/**
 * Created by DELL on 4/13/2018.
 */

public class Image_grid extends AppCompatActivity  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {
    private static final String TAG = "Image_grid";
    private Uri url_file;
    String str_uri, str_timestamp, str_img_sr_no;
    SharedPreferences pref;
    String user_id, str_losid, str_ts, str_pic_catpure_list, str_doc_Type, str_pic_name, no_of_img, app_type;
    Integer img_width, img_height, img_com_size;
    SharedPreferences pData;
    Double lat, lon;
    String[] myarray_img;
    List<String> ls_img_no, ls_img_path;
    GridView grid;
    int REQUEST_CAMERA = 100;
    private String imgPath;
    static String ssstr_img_path;
    DBHelper dbHelper;
    Integer adp_cnt;
    UserFunction userfucntion;
    public String[] IMAGES;
    String new_arr[];
    ProgressDialog mProgressDialog;
    Boolean status_co = false;
    JSONObject jsonobject;
    UserFunction userfunction;

    ArrayList<String> cu_pic_values, pic_list, pic_values, pic_type;
    ArrayList<String> cu_pic_names, pic_hight, pic_width, pic_size, pic_pages;


    public GoogleApiClient mGoogleApiClient;
    public Location mLocation;
    public LocationManager mLocationManager;
    public LocationRequest mLocationRequest;
    public com.google.android.gms.location.LocationListener listener;
    public long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    public long FASTEST_INTERVAL = 2000; /* 2 sec */
    public LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_image_grid);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        dbHelper = new DBHelper(this);
        userfucntion = new UserFunction(this);
        ls_img_no = new ArrayList<String>();
        ls_img_path = new ArrayList<String>();


        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        pData = this.getSharedPreferences("pData", 0);

        TextView tr_title1 = (TextView) findViewById(R.id.txt_img_title);
        str_timestamp = getDateTime();


        Intent i = getIntent();
        no_of_img = i.getStringExtra("no_of_img");
        str_losid = i.getStringExtra("str_losid");
        user_id = i.getStringExtra("user_id");
        str_ts = i.getStringExtra("str_ts");
        str_pic_catpure_list = i.getStringExtra("str_pic_catpure_list");
        str_doc_Type = "KYC";
        str_pic_name = i.getStringExtra("str_doc_name");
        img_com_size = i.getIntExtra("img_com_size", 0);
        img_width = i.getIntExtra("img_width", 0);
        img_height = i.getIntExtra("img_height", 0);
        app_type = i.getStringExtra("app_type");


        tr_title1.setText(str_pic_name);
        System.out.println("no_of_img:::" + no_of_img + ":::str_pic_name:::" + str_pic_name + ":::str_pic_catpure_list:::" + str_pic_catpure_list
                + ":::str_doc_Type::::" + str_doc_Type + "::::str_pic_name:::::" + str_pic_name +
                ":::::img_com_size:::::" + img_com_size + ":::img_width::::" + img_width + "::img_height:::::" + img_height);

        userfunction = new UserFunction(this);

        cu_pic_values = new ArrayList<String>();
        cu_pic_names = new ArrayList<String>();
        pic_type = new ArrayList<String>();
        pic_hight = new ArrayList<String>();
        pic_width = new ArrayList<String>();
        pic_size = new ArrayList<String>();
        pic_pages = new ArrayList<String>();


        cu_pic_values.add("");

        cu_pic_names.add("Select Doc Type ");

        pic_type.add("");
        pic_hight.add("0");
        pic_width.add("0");
        pic_size.add("0");
        pic_pages.add("0");

        System.out.println("::::inside get masyer");

       // new Get_img_master().execute();

       // GPSTracker_New tracker = new GPSTracker_New(Image_grid.this);

        Button btn_submit = (Button) findViewById(R.id.btn_add);



        String num = null;
        for (int im = 1; im <= Integer.parseInt(no_of_img); im++) {

            ls_img_no.add(str_pic_name + " " + (im));
            final Cursor cursor = dbHelper.getImages_new(Integer.toString(im), str_timestamp);


            Integer total_pic_count = cursor.getCount();


            if (total_pic_count >= 1) {

                System.out.println("total_pic_count:NNN::" + total_pic_count);
                System.out.println("total_pic_count:NNN:LLL::::" + num);

                if (cursor != null && cursor.moveToFirst()) {
                    num = cursor.getString(cursor.getColumnIndex("img_path"));
                    cursor.close();
                }

                ls_img_path.add(num);
                System.out.println("total_pic_count:NNN:LLL::::" + im + ":::::" + num);
                // System.out.println("ls_img_path:NNN::" + ls_img_path);

            } else {
                ls_img_path.add("KPKKP");

            }
        }

        //ls_img_no.clear();
        final String[] myarray = new String[ls_img_no.size()];
        ls_img_no.toArray(myarray);

        // ls_img_path.clear();
        myarray_img = new String[ls_img_path.size()];
        ls_img_path.toArray(myarray_img);

        System.out.println("myarray:::" + myarray);
        CustomGridAdapter adapter = new CustomGridAdapter(Image_grid.this, myarray, myarray_img);
        grid = (GridView) findViewById(R.id.grid);
        grid.setAdapter(adapter);
        // adp_cnt=adapter.getCount();

        adapter.notifyDataSetChanged();

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
              //  Toast.makeText(Image_grid.this, "You Clicked at " + myarray[position], Toast.LENGTH_SHORT).show();

                checkLocation();
                if(isLocationEnabled()) {
                    takePicture((position + 1));
                }
            }
        });

        /*
        ImageView btnSelectPhoto = (ImageView) findViewById(R.id.btnSelectPhoto);
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_timestamp = getDateTime();

                takePicture();

            }

        });
        */


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CreatePDf().execute();
                /*
                Boolean status_co = false;

                final Cursor cursor = dbHelper.getImages_new_ts(str_timestamp);
                cursor.moveToNext();
                adp_cnt = cursor.getCount();
                // adp_cnt=cursor.getInt(0);

                //  String  str_adp_cnt  = cursor.getString(cursor.getColumnCount());

                // String  str_adp_cnt  = cursor.getString(0);

                // String  str_adp_cnt = cursor.getString(cursor.getColumnIndexOrThrow("grd_cnt"));
                System.out.println(":::str_adp_cnt::::" + adp_cnt + "::::no_of_img::::" + no_of_img + ":::::str_timestamp::::" + str_timestamp);
                File mediaFile = null;

                if (adp_cnt > 10) {

                    try {
                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
                        // This location works best if you want the created images to be shared
                        // between applications and persist after your app has been uninstalled.

                        // Create the storage directory if it does not exist
                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                            }
                        }
                        Integer i = 0;
                        Integer int_Adp = adp_cnt / 10;
                        if ((adp_cnt % 10) == 0) {
                            System.out.println("is even_ or odd:::::::EVEN" + int_Adp);
                        } else {
                            System.out.println("is even_ or odd:::::::ODD" + int_Adp);
                            int_Adp = int_Adp + 1;

                        }

                        // 2

                        for (i = 1; i <= int_Adp; i++) {
                            // Create a media file name
                            String pdf_name = str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + str_doc_Type + "_" + str_pic_catpure_list + "_" + (i) + ".pdf";
                            mediaFile = new File(mediaStorageDir.getPath() + File.separator + pdf_name);


                            switch (i) {

                                case 1:
                                    Integer i_num = 10;
                                    if (adp_cnt <= i_num) {
                                        i_num = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (0), i_num);
                                    break;
                                case 2:
                                    Integer i_num_1 = 20;
                                    if (adp_cnt <= i_num_1) {
                                        i_num_1 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (10), i_num_1);
                                    break;
                                case 3:
                                    Integer i_num_2 = 30;
                                    if (adp_cnt <= i_num_2) {
                                        i_num_2 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (20), i_num_2);
                                    break;

                                default:

                                    break;
                            }

                            System.out.println("new_arr::::" + i + "::::ARRAY:::" + Arrays.toString(new_arr));

                            Image img = Image.getInstance(myarray_img[i]);
                            Document document = new Document(img);
                            PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                            document.open();


                            //  System.out.println("new_arr::::::"+(i+2)+"::::::ARRAY:::"+Arrays.toString(myarray_img));
                            // new_arr= Arrays.copyOfRange(myarray_img,(0),(2));

                            for (String image : new_arr) {
                                img = Image.getInstance(image);
                                document.setPageSize(img);
                                document.newPage();
                                img.setAbsolutePosition(0, 0);
                                document.add(img);
                            }
                            document.close();

                            String abs_path = mediaFile.getAbsolutePath();
                            status_co = dbHelper.insertImage_new(abs_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list, str_img_sr_no, no_of_img, str_timestamp);

                            System.out.println("abs_path::::" + abs_path);
                        }

                    } catch (Exception e) {

                        System.out.println("Errorr:::" + e.getMessage());
                    }


                } else {
                    try {
                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
                        // This location works best if you want the created images to be shared
                        // between applications and persist after your app has been uninstalled.

                        // Create the storage directory if it does not exist
                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                            }
                        }
                        // Create a media file name

                        String pdf_name = str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + str_doc_Type + "_" + str_pic_catpure_list + "_" + "1" + ".pdf";
                        mediaFile = new File(mediaStorageDir.getPath() + File.separator + pdf_name);

                        Image img = Image.getInstance(myarray_img[0]);
                        Document document = new Document(img);
                        PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                        //PdfWriter writer=PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                        // writer.setEncryption("".get(),"".getBytes(),PdfWriter.ALLOW_PRINTING,PdfWriter.ENCRYPTION_AES_128);
                        // writer.setEncryption("".getBytes(),"".getBytes(),PdfWriter.ALLOW_PRINTING,PdfWriter.ENCRYPTION_AES_128);

                        document.open();
                        for (String image : myarray_img) {
                            img = Image.getInstance(image);
                            document.setPageSize(img);
                            document.newPage();
                            img.setAbsolutePosition(0, 0);
                            document.add(img);

                        }
                        document.close();
                        String abs_path = mediaFile.getAbsolutePath();
                        status_co = dbHelper.insertImage_new(abs_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list, str_img_sr_no, no_of_img, str_timestamp);

                    } catch (Exception e) {

                        System.out.println("Errorr:::" + e.getMessage());
                    }


                }



                if (Integer.parseInt(no_of_img) <= adp_cnt) {

                    for (int j = 0; j < myarray_img.length; j++) {
                        dbHelper.deletepic(myarray_img[j]);
                    }

                    System.out.println("status_co::::" + status_co);
                    if (status_co == true) {

                        Intent i = new Intent(Image_grid.this,
                                Image_List.class);
                        i.putExtra("str_losid", str_losid);
                        i.putExtra("app_type", app_type);
                        i.putExtra("str_cust_id", str_ts);
                        i.putExtra("app_type_nw", app_type);
                        startActivity(i);
                        finish();
                    } else {

                        //toast
                    }

                } else {

                    userfucntion.cutomToast("Some of the Pics are Pending. thCapture all images..", Image_grid.this);
                }

                // new Update_count().execute();

*/
            }


        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) Image_grid.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        checkLocation();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == REQUEST_CAMERA) {
                if (resultCode == Activity.RESULT_OK) {
                    SharedPreferences.Editor editor = pData.edit();


                    str_uri = pref.getString("str_uri", null);
                    str_img_sr_no = pref.getString("str_position", null);

                    url_file = Uri.parse(str_uri);

                    String str_img_path = url_file.toString();
                    if (url_file != null) {

                        File new_img_file = new File(url_file.toString());
                        str_img_path = new_img_file.getAbsolutePath();

                        try {
                            str_img_path = str_img_path.replace("/file:", "");

                            System.out.println("img_height:::" + img_height + ":::img_width::::" + img_width + "::::img_com_size::::" + img_com_size);
                            Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(str_img_path, img_height, img_width, user_id, str_losid, str_ts, img_com_size, lat, lon, getDateTime());
                            System.out.println("img_height:::" + img_height + ":::img_width::::" + img_width + "::::str_pic_catpure_list::::" + str_pic_catpure_list);

                            ssstr_img_path = storeImage(bitmap, user_id, str_losid, str_ts, str_pic_catpure_list, str_doc_Type, img_com_size, str_img_sr_no);

                        } catch (Exception e) {
                            ssstr_img_path = new_img_file.getAbsolutePath();
                            ssstr_img_path = ssstr_img_path.replace("/file:", "");
                        }


                    }

                    if (str_uri != null) {
                        System.out.println("str_img_sr_no::::::" + str_img_sr_no);
                        // show the result here.
                        if (dbHelper.insertImage_new(ssstr_img_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list, str_img_sr_no, no_of_img, str_timestamp)) {
                            //  Toast.makeText(getApplicationContext(), "iMAGE Inserted", Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(getApplicationContext(), "Could not Insert IMAGE", Toast.LENGTH_SHORT).show();
                        }
                        //setlistview();
                        ls_img_no.clear();
                        ls_img_path.clear();
                        String num = null;

                        for (int im = 1; im <= Integer.parseInt(no_of_img); im++) {

                            ls_img_no.add(str_pic_name + " " + (im));

                            final Cursor cursor = dbHelper.getImages_new(Integer.toString(im), str_timestamp);


                            Integer total_pic_count = cursor.getCount();

                            System.out.println("im:::::" + im + "::::tmestamp:::" + str_timestamp + "::total_pic_count::::" + total_pic_count);


                            if (total_pic_count >= 1) {

                                if (cursor != null && cursor.moveToFirst()) {
                                    num = cursor.getString(cursor.getColumnIndex("img_path"));
                                    cursor.close();
                                }

                                ls_img_path.add(num);


                            } else {
                                ls_img_path.add("");

                            }
                        }


                        final String[] myarray = new String[ls_img_no.size()];
                        ls_img_no.toArray(myarray);


                        myarray_img = new String[ls_img_path.size()];
                        ls_img_path.toArray(myarray_img);

                        System.out.println("myarray:::" + myarray);
                        CustomGridAdapter adapter = new CustomGridAdapter(Image_grid.this, myarray, myarray_img);
                        grid = (GridView) findViewById(R.id.grid);
                        grid.setAdapter(adapter);
                        // adp_cnt=adapter.getCount();
                        adapter.notifyDataSetChanged();


                    }

                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // Delete the cancelled image

                try {
                    //deleteLastPic();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }


                // user cancelled Image capture
                Toast.makeText(this,
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(this,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    public void createImageCell(String path) {
        Image img = Image.getInstance(path);
        PdfPCell cell = new PdfPCell(img, true);
        return cell;
    }
    */
    public void takePicture(Integer position) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String str_position = String.valueOf(position);
        System.out.println(":::::str_position:::::111:::::" + str_position);

        File new_file3 = getOutputMediaFile2(user_id, str_losid, str_ts, str_pic_catpure_list, str_doc_Type, str_position);
        System.out.println(":::::111111" + url_file);
        url_file = Uri.fromFile(new_file3);
        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove(str_uri);
        peditor.commit();
        str_uri = url_file.toString();
        peditor.putString("str_uri", str_uri);
        peditor.putString("str_position", str_position);


        peditor.commit();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);


        startActivityForResult(intent, 100);
    }


    private String storeImage(Bitmap image, String userid, String losid, String ts, String str_pic_name, String doc_type, Integer img_com_size, String str_img_sr_no_n) {
        String new_strMyImagePath = null;
        String pic_id = str_pic_name.toString();
        System.out.println(":::::str_position:::::22222:::::" + str_img_sr_no_n);

        File pictureFile = getOutputMediaFile2(userid, losid, ts, pic_id, doc_type, str_img_sr_no_n);


        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {

                if (img_com_size > 0) {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    image.compress(Bitmap.CompressFormat.JPEG, img_com_size, fos);
                    fos.close();
                }
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

        }

        return new_strMyImagePath;
    }

    private File getOutputMediaFile2(String userid, String str_losid, String ts, String str_pic_name, String doc_type, String str_img_sr_no_n_n) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        System.out.println(":::::str_position:::::333:::::" + str_img_sr_no_n_n);

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = str_losid + "_" + userid + "_" + ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + doc_type + "_" + str_pic_name + "_" + str_img_sr_no_n_n + "_" + no_of_img + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());

        // str_userid = str_userid.replace("HDB","");

        return format;
    }

    @Override
    public void onBackPressed() {

        Boolean delete_grid_pics = dbHelper.deletegridpic(str_timestamp);
        System.out.println("deletegridpic::" + delete_grid_pics);
        finish();
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Image img = Image.getInstance(IMAGES[0]);
        Document document = new Document(img);
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        for (String image : IMAGES) {
            img = Image.getInstance(image);
            document.setPageSize(img);
            document.newPage();
            img.setAbsolutePosition(0, 0);
            document.add(img);
        }
        document.close();
    }


    private class CreatePDf extends AsyncTask<String, Integer, String> {
        private String str_path;
        Call<UploadObject> fileUpload;

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Image_grid.this);
            mProgressDialog.setMessage("Uploading ");
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            // mProgressDialog.setProgress(0);

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            mProgressDialog.setProgress(progress[0]);
        }


        @Override
        protected String doInBackground(String... params) {

            try {


                final Cursor cursor = dbHelper.getImages_new_ts(str_timestamp);
                cursor.moveToNext();
                adp_cnt = cursor.getCount();
                // String  str_adp_cnt = cursor.getString(cursor.getColumnIndexOrThrow("grd_cnt"));
                System.out.println(":::str_adp_cnt::::" + adp_cnt + "::::no_of_img::::" + no_of_img + ":::::str_timestamp::::" + str_timestamp);
                File mediaFile = null;

                if (adp_cnt > 5) {

                    try {
                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
                        // This location works best if you want the created images to be shared
                        // between applications and persist after your app has been uninstalled.

                        // Create the storage directory if it does not exist
                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                            }
                        }
                        Integer i = 0;
                        Integer int_Adp = adp_cnt / 5;
                        if ((adp_cnt % 5) == 0) {
                            System.out.println("is even_ or odd:::::::EVEN" + int_Adp);
                        } else {
                            System.out.println("is even_ or odd:::::::ODD" + int_Adp);
                            int_Adp = int_Adp + 1;

                        }

                        // 2

                        for (i = 1; i <= int_Adp; i++) {
                            // Create a media file name
                            String pdf_name = str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + str_doc_Type + "_" + str_pic_catpure_list + "_" + (i) + ".pdf";
                            mediaFile = new File(mediaStorageDir.getPath() + File.separator + pdf_name);


                            switch (i) {

                                case 1:
                                    Integer i_num = 5;
                                    if (adp_cnt <= i_num) {
                                        i_num = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (0), i_num);
                                    break;
                                case 2:
                                    Integer i_num_1 = 10;
                                    if (adp_cnt <= i_num_1) {
                                        i_num_1 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (5), i_num_1);
                                    break;
                                case 3:
                                    Integer i_num_2 = 15;
                                    if (adp_cnt <= i_num_2) {
                                        i_num_2 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (10), i_num_2);
                                    break;

                                case 4:
                                    Integer i_num_3 = 20;
                                    if (adp_cnt <= i_num_3) {
                                        i_num_3 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (15), i_num_3);
                                    break;
                                case 5:
                                    Integer i_num_4 = 25;
                                    if (adp_cnt <= i_num_4) {
                                        i_num_4 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (20), i_num_4);
                                    break;

                                case 6:
                                    Integer i_num_5 = 30;
                                    if (adp_cnt <= i_num_5) {
                                        i_num_5 = adp_cnt;
                                    }
                                    new_arr = Arrays.copyOfRange(myarray_img, (25), i_num_5);
                                    break;
                                default:

                                    break;
                            }

                            System.out.println("new_arr::::" + i + "::::ARRAY:::" + Arrays.toString(new_arr));

                            Image img = Image.getInstance(myarray_img[i]);
                            Document document = new Document(img);
                            PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                            document.open();


                            //  System.out.println("new_arr::::::"+(i+2)+"::::::ARRAY:::"+Arrays.toString(myarray_img));
                            // new_arr= Arrays.copyOfRange(myarray_img,(0),(2));

                            for (String image : new_arr) {
                                img = Image.getInstance(image);
                                document.setPageSize(img);
                                document.newPage();
                                img.setAbsolutePosition(0, 0);
                                document.add(img);
                            }
                            document.close();

                            String abs_path = mediaFile.getAbsolutePath();
                            status_co = dbHelper.insertImage_new(abs_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list, str_img_sr_no, no_of_img, str_timestamp);

                            System.out.println("abs_path::::" + abs_path);
                        }

                    } catch (Exception e) {

                        System.out.println("Errorr:::" + e.getMessage());
                    }


                } else {
                    try {
                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), "HDB-LOS_IMG");
                        // This location works best if you want the created images to be shared
                        // between applications and persist after your app has been uninstalled.

                        // Create the storage directory if it does not exist
                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                            }
                        }
                        // Create a media file name

                        String pdf_name = str_losid + "_" + user_id + "_" + str_ts + "_" + str_timestamp + "_" + "DMS_" + app_type + "_" + str_doc_Type + "_" + str_pic_catpure_list + "_" + "1" + ".pdf";
                        mediaFile = new File(mediaStorageDir.getPath() + File.separator + pdf_name);

                        Image img = Image.getInstance(myarray_img[0]);
                        Document document = new Document(img);
                        PdfWriter writer=  PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                        //PdfWriter writer=PdfWriter.getInstance(document, new FileOutputStream(mediaFile));
                        // writer.setEncryption("".get(),"".getBytes(),PdfWriter.ALLOW_PRINTING,PdfWriter.ENCRYPTION_AES_128);
                        // writer.setEncryption("".getBytes(),"".getBytes(),PdfWriter.ALLOW_PRINTING,PdfWriter.ENCRYPTION_AES_128);
                       // writer.setFullCompression();
                        document.open();
                        for (String image : myarray_img) {
                            img = Image.getInstance(image);
                            document.setPageSize(img);
                            document.newPage();
                            img.setAbsolutePosition(0, 0);
                            document.add(img);

                        }
                        document.close();
                        String abs_path = mediaFile.getAbsolutePath();
                        status_co = dbHelper.insertImage_new(abs_path, str_losid, user_id, str_ts, "0", str_pic_name, app_type, str_pic_catpure_list, str_img_sr_no, no_of_img, str_timestamp);

                    } catch (Exception e) {

                        System.out.println("Errorr:::" + e.getMessage());
                    }


                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return str_path;
        }

        @Override
        protected void onPostExecute(String args) {

            if (Integer.parseInt(no_of_img) <= adp_cnt) {

                for (int j = 0; j < myarray_img.length; j++) {
                    dbHelper.deletepic(myarray_img[j]);
                }

                System.out.println("status_co::::" + status_co);
                if (status_co == true) {

                    Intent i = new Intent(Image_grid.this,
                            Image_List.class);
                    i.putExtra("str_losid", str_losid);
                    i.putExtra("app_type", app_type);
                    i.putExtra("str_cust_id", str_ts);
                    i.putExtra("app_type_nw", app_type);
                    startActivity(i);
                    finish();
                } else {

                    //toast
                }

            } else {

                userfucntion.cutomToast("Some of the Pics are Pending. thCapture all images..", Image_grid.this);
            }


        }

    }
    public class Get_img_master extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Image_grid.this);
            mProgressDialog.setMessage(getApplicationContext().getString(R.string.plz_wait));
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            jsonobject = null;
            jsonobject = userfunction.get_img_master(str_ts, app_type, user_id);
            // Create an array

            return null;
        }


        @Override
        protected void onPostExecute(Void args) {
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString("status") != null) {
                        System.out.println("jsonobject:::" + jsonobject);

                        String res = jsonobject.getString("status");
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            JSONArray jsonarray = jsonobject.getJSONArray("Data");

                            for (int i = 0; i < jsonarray.length(); i++) {

                                jsonobject = jsonarray.getJSONObject(i);
                                cu_pic_values.add(jsonobject.getString("doc_id"));
                                cu_pic_names.add(jsonobject.getString("doc_name"));
                                pic_type.add(jsonobject.getString("doc_type"));
                                pic_hight.add(jsonobject.getString("img_hight"));
                                pic_width.add(jsonobject.getString("img_width"));
                                pic_size.add(jsonobject.getString("image_quality"));
                                pic_pages.add(jsonobject.getString("multiple_page"));


                            }

                            mProgressDialog.dismiss();
                        }

                    }
                } else {

                    mProgressDialog.dismiss();

                    userfunction
                            .cutomToast("Something gets wrong with connectivity..", getApplicationContext());
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startLocationUpdates();
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation == null) {
            startLocationUpdates();
        }
        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }


    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    public void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        Log.d("reque", "--->>>>");
    }

    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        // mLatitudeTextView.setText(String.valueOf(location.getLatitude()));
        // mLongitudeTextView.setText(String.valueOf(location.getLongitude()));
        lat =location.getLatitude();
        lon = location.getLongitude();

        // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    public boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    public void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

}

