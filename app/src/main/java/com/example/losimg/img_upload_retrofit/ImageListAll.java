package com.example.losimg.img_upload_retrofit;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.losimg.MainModule.HdbHome;

import com.example.losimg.libraries.DBHelper;

/**
 * Created by DELL on 12/2/2017.
 */

public class ImageListAll extends AppCompatActivity {
    DBHelper dbHelper;
    Integer total_pic_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_imageslist);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setlistview();
    }

    public void setlistview() {
        dbHelper = new DBHelper(this);
        TextView txt_upload = (TextView) findViewById(R.id.txt_upload);
        final Cursor cursor = dbHelper.getImages();
        total_pic_count = cursor.getCount();
        if (total_pic_count > 0) {
            txt_upload.setVisibility(View.GONE);
        } else {
            txt_upload.setVisibility(View.VISIBLE);
        }
        ListView lvItems = (ListView) findViewById(R.id.list);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageAdapterAll todoAdapter = new ImageAdapterAll(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        todoAdapter.notifyDataSetChanged();
        lvItems.refreshDrawableState();

    }

    @Override
    public void onBackPressed() {
        Intent home = new Intent(ImageListAll.this, HdbHome.class);
        home.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home);

    }

    public void go_home(View v){
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

}

