package com.example.losimg.img_upload_retrofit;


import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface InsertAPI {
    @FormUrlEncoded
    @POST("/Retrofit9999Example/insert.php")
    public void insertUser(@Field("name") String name, @Field("username") String username, @Field("password") String password, @Field("email") String email, Callback<Response> callback);
}