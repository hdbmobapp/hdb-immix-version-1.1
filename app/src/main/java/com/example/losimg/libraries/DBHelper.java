package com.example.losimg.libraries;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;




import com.example.losimg.img_upload_retrofit.MainActivity;
import com.example.losimg.img_upload_retrofit.UploadImageInterface;
import com.example.losimg.img_upload_retrofit.UploadObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by obaro on 02/04/2015.
 */
public class DBHelper extends SQLiteOpenHelper{
    ProgressDialog dialog;
    JSONObject jObj = null;
    String json = null;
    ProgressDialog mProgressDialog;
    public static final String DATABASE_NAME = "dms_app.db";
    private static final int DATABASE_VERSION = 4;
    UserFunction userFunction;
    Context context;
    ConnectionDetector cd;
    private SQLiteDatabase mDB;
    SharedPreferences pref;
    private static final String IMAGE_DIRECTORY_NAME = "/HDB_QDE";

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    private static final String SERVER_PATH = "https://hdbapp.hdbfs.com/";
    private Uri uri;
    // IMAGE TABLE


    public static final String IMAGE_TABLE_NAME = "tbl_image_master";
    public static final String IMAGE_TABLE_NAME_NEW = "tbl_image_master_new";


    public static final String IMAGE_COLUMN_ID = "_id";
    public static final String IMAGE_LOSID = "img_losid";
    public static final String IMAGE_USERID = "img_userid";
    public static final String IMAGE_TS = "img_ts";
    public static final String IMAGE_MAPCOUNT = "img_map_count";
    public static final String IMAGE_STATUS = "img_status";
    public static final String IMAGE_PATH = "img_path";
    public static final String IMAGE_CAPTION = "img_captions";
    public static final String  IMAGE_APP_TYPE= "img_app_type";
    public static final String  IMAGE_NO= "img_no";
    public static final String  IMAGE_SR_NO= "img_sr_no";
    public static final String  IMAGE_COUNT= "img_count";
    public static final String  IMAGE_TIMESTAMP= "img_timestamp";




    //IMAGE MASTER
    public static final String IMAGE_SRNO = "img_srno";
    public static final String IMAGE_ID = "img_id";
    public static final String IMAGE_TITLE = "img_title";
    public static final String IMAGE_FLAG = "img_flag";
    private static final String DATABASE_IMG_MASTER = "key_img_master";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        userFunction = new UserFunction(context);
        this.context = context;
        cd = new ConnectionDetector(context);
        this.mDB = getWritableDatabase();
        pref = context.getSharedPreferences("MyPref", 0);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        userFunction = new UserFunction(context);


        db.execSQL(
                "CREATE TABLE " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        IMAGE_LOSID + " TEXT, " +
                        IMAGE_USERID + " TEXT, " +
                        IMAGE_TS + " TEXT, " +
                        IMAGE_MAPCOUNT + " TEXT, " +
                        IMAGE_PATH + " TEXT, " +
                        IMAGE_CAPTION + " TEXT, " +
                        IMAGE_STATUS + " INTEGER , "+
                        IMAGE_APP_TYPE + " TEXT, " +
                        IMAGE_NO + " TEXT, " +
                        IMAGE_SR_NO + " TEXT, " +
                        IMAGE_COUNT + " TEXT, " +
                        IMAGE_TIMESTAMP + " TEXT )"
        );


        db.execSQL(
                "CREATE TABLE " + DATABASE_IMG_MASTER +
                        "(" + IMAGE_SRNO + " integer primary key autoincrement, " +
                        IMAGE_ID + " TEXT, " +
                        IMAGE_TITLE + " TEXT, " +
                        IMAGE_FLAG + " TEXT )"
        );


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_IMG_MASTER);


        onCreate(db);
    }


    public boolean insertImage(String path, String losid, String userid, String ts, String status, String image_cation,String image_app_type,String image_no,String image_sr_no,String image_count,String image_timestamp) {
        System.out.println("UPDT IMG TSpath:1::" + path);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        System.out.println("UPDT IMG TSpath:2::" + path);

        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);
        contentValues.put(IMAGE_APP_TYPE, image_app_type);
        contentValues.put(IMAGE_NO, image_no);
        contentValues.put(IMAGE_SR_NO, image_sr_no);
        contentValues.put(IMAGE_COUNT, image_count);
        contentValues.put(IMAGE_TIMESTAMP, image_timestamp);


        System.out.println("UPDT IMG TSpath:3::" + path);

        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        System.out.println("UPDT IMG TSpath::4:" + path);

        return true;
    }


    public boolean insertImage_new(String path, String losid, String userid, String ts, String status, String image_cation,String image_app_type,String image_no,String image_sr_no,String image_count,String image_timestamp) {
        System.out.println("UPDT IMG TSpath:1::" + path);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        System.out.println("UPDT IMG TSpath:2::" + path);

        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);
        contentValues.put(IMAGE_PATH, path);
        contentValues.put(IMAGE_CAPTION, image_cation);
        contentValues.put(IMAGE_APP_TYPE, image_app_type);
        contentValues.put(IMAGE_NO, image_no);
        contentValues.put(IMAGE_SR_NO, image_sr_no);
        contentValues.put(IMAGE_COUNT, image_count);
        contentValues.put(IMAGE_TIMESTAMP, image_timestamp);


        System.out.println("UPDT IMG TSpath:3::" + path);

        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        System.out.println("UPDT IMG TSpath::4:" + path);

        return true;
    }


    public Cursor getImages_new(String img_sr_no,String img_ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_SR_NO + "= ? AND " + IMAGE_TIMESTAMP + " = ?  order by " + IMAGE_COLUMN_ID + " desc limit 1", new String[]{img_sr_no,img_ts});

        return res;
    }

    public Cursor getImages_new_ts(String img_ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT  * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TIMESTAMP +"=?", new String[]{img_ts});
        return res;
    }
    public boolean deletegridpic(String ts) {

        boolean chkFlag = false;



            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_TIMESTAMP + "=" + " '"
                    + ts + "'", null) > 0;
            db.close();




        // return true;
        return chkFlag;
    }


    public Cursor getImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + ">? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0"});

        return res;
    }

    public Cursor getImagests(String ts) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + ">? AND  "+ IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0",ts});

        return res;
    }

    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + IMAGE_PATH + ") FROM "
                + IMAGE_TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }

    public int upload_image(String img_path) {
        int resp_code = 0;

        System.out.println("img_path::1212121::::" + img_path);
        if (img_path != null) {
            //	Log.d("uploading File : ", img_path);
            //  String new_img_path="/storage/emulated/0/PD/"+img_path+".jpg";
            // System.out.println("new_img_path::::::"+new_img_path);
            File file1 = new File(img_path);
            if (file1.exists()) {
                // Log.d("uploading File Esists : ", "true");
                System.out.println("img_path::::11222" + img_path);
                if (cd.isConnectingToInternet()) {
                    //resp_code = doFileUpload(img_path);
                     //UploadPic(img_path);


                } else {
                    userFunction.cutomToast("No internet connection...", context);
                }
            } else {
                //  System.out.println("img_path::::8877" + new_img_path);

                this.deletepic(img_path);
                this.deletepic(img_path);

            }
        } else {
            try {
                if (Integer.parseInt(this.getPendingPhotoCount()) == 0) {

                    File mediaStorageDir;
                    String extStorageDirectory = Environment
                            .getExternalStorageDirectory()
                            .toString();

                    // External sdcard location
                    mediaStorageDir = new File(
                            extStorageDirectory
                                    + IMAGE_DIRECTORY_NAME);

                    if (mediaStorageDir.isDirectory()) {
                        try {
                            String[] children = mediaStorageDir
                                    .list();
                            for (int i = 0; i < children.length; i++) {
                                new File(mediaStorageDir, children[i]).delete();
                            }

                        } catch (Exception e) {
                            // block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resp_code;
    }
/*
    public void UploadPic(String filePath) {
       // String filePath = getRealPathFromURIPath(uri, MainActivity.this);
        System.out.println("filePath:::::"+filePath);
        File file = new File(filePath);
        Log.d(TAG, "Filename " + file.getName());
        //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_PATH)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);

        System.out.println("fileToUpload::::::" + fileToUpload);
        System.out.println("fileToUpload::::filename:::::" + filename);

        Call<UploadObject> fileUpload = uploadImage.uploadFile(fileToUpload, filename);
        fileUpload.enqueue(new Callback<UploadObject>() {
            @Override
            public void onResponse(Call<UploadObject> call, Response<UploadObject> response) {
                Toast.makeText(context, "Response " + response.raw().message(), Toast.LENGTH_LONG).show();
                Toast.makeText(context, "Success " + response.body().getSuccess(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UploadObject> call, Throwable t) {
                Log.d(TAG, "Error " + t.getMessage());
            }
        });
    }
*/



    /*
        public int doFileUpload(final String selectedFilePath) {
            File file1 = new File(selectedFilePath);
            String BASE_URL = BuildConfig.SERVER_URL;
            //private static String URL_POST = "https://hdbapp.hdbfs.com/HDB_QDE/index_post.php";
            int serverResponseCode = 0;
            String response = "";

            HttpsURLConnection connection;
            DataOutputStream dataOutputStream;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";


            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File selectedFile = new File(selectedFilePath);


            String[] parts = selectedFilePath.split("/");
            final String fileName = parts[parts.length - 1];

            if (!selectedFile.isFile()) {
                dialog.dismiss();

                return 0;
            } else {
                try {
                    FileInputStream fileInputStream = new FileInputStream(selectedFile);
                    URL url = new URL(BASE_URL + "/HDB_QDE_v2.0/index_get.php?target=img&action=upload");
                    connection = (HttpsURLConnection) url.openConnection();
                    connection.setDoInput(true);//Allow Inputs
                    connection.setDoOutput(true);//Allow Outputs
                    connection.setUseCaches(false);//Don't use a cached Copy
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    connection.setRequestProperty("uploadedfile1", selectedFilePath);


                    //creating new dataoutputstream
                    dataOutputStream = new DataOutputStream(connection.getOutputStream());

                    //writing bytes to data outputstream
                    dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile1\";filename=\""
                            + selectedFilePath + "\"" + lineEnd);

                    dataOutputStream.writeBytes(lineEnd);

                    //returns no. of bytes present in fileInputStream
                    bytesAvailable = fileInputStream.available();
                    //selecting the buffer size as minimum of available bytes or 1 MB
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    //setting the buffer as byte array of size of bufferSize
                    buffer = new byte[bufferSize];

                    //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                    while (bytesRead > 0) {
                        //write the bytes read from inputstream
                        dataOutputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    dataOutputStream.writeBytes(lineEnd);
                    dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();
                    System.out.println("serverResponseMessage::" + serverResponseMessage);


                    if (serverResponseCode == 200) {
                        String line;
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "iso-8859-1"));
                        while ((line = bufferedReader.readLine()) != null) {
                            response += line;
                        }
                    }

                    if (response != null) {

                        try {

                            json = response.toString();
                        } catch (Exception e) {
                            Log.e("Buffer Error", "Error converting result " + e.toString());
                        }
                        // try parse the string to a JSON object
                        try {
                            jObj = new JSONObject(json);
                        } catch (JSONException e) {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                    } else {
                        jObj = null;
                    }


                    if (jObj != null) {
                        try {
                            System.out.println("response_str::::" + jObj);
                            if (jObj.getString(KEY_STATUS) != null) {
                                String res = jObj.getString(KEY_STATUS);
                                String resp_success = jObj.getString(KEY_SUCCESS);
                                if (Integer.parseInt(res) == 200
                                        && resp_success.equals("true")) {
                                    deletepic(selectedFilePath);
                                    file1.delete();
                                } else {
                                    if (Integer.parseInt(res) == 200
                                            && resp_success.equals("false")) {


                                    } else {
                                        serverResponseCode = 3;


                                        Log.i("RESPONSE MESSAGE ", jObj.getString("message").toString());
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    //closing the input and output streams
                    fileInputStream.close();
                    dataOutputStream.flush();
                    dataOutputStream.close();


                } catch (FileNotFoundException e) {
                    e.printStackTrace();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "URL error!", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
                }
                //dialog.dismiss();
                return serverResponseCode;
            }
        }
    */
    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }


            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_PATH + "=" + " '"
                    + path + "'", null) > 0;
            db.close();


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }


    public Cursor getAllImages(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{ts});

        return res;
    }

    public Cursor getAllImages_new(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
       // Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{ts});
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS + "=? AND  "+ IMAGE_TS + "=? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0",ts});

        return res;
    }


    public boolean delete_IMGo(String ts) {
        System.out.println("tsssss1111::::" + ts);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            if (ts != null) {
                if (!ts.equals(null)) {
                    Log.d("DELETING IMG", ts);

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_TS + "=" + " '" + ts
                            + "'", null) > 0;
                    db.close();
                } else {

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                    db.close();

                }
            } else {

                SQLiteDatabase db = this.getWritableDatabase();
                chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                db.close();

            }


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("sttus::::" + chkFlag);
        // return true;
        return chkFlag;
    }

    public boolean delete_IMGo_status(String ts) {
        System.out.println("tsssss1111::::" + ts);
        boolean chkFlag = false;
        try {
            // mDB.execSQL(" DELETE FROM "+ DATABASE_TABLE +
            // " where  KEY_LOSID='"+losid+"'");
            if (ts != null) {
                if (!ts.equals(null)) {
                    Log.d("DELETING IMG", ts);

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_STATUS + "=" + " '" + ts
                            + "'", null) > 0;
                    db.close();
                } else {

                    SQLiteDatabase db = this.getWritableDatabase();
                    chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                    db.close();

                }
            } else {

                SQLiteDatabase db = this.getWritableDatabase();
                chkFlag = db.delete(IMAGE_TABLE_NAME, null, null) > 0;
                db.close();

            }


        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("sttus::::" + chkFlag);
        // return true;
        return chkFlag;
    }

    public boolean update_status(String ts, int status, String pic_count) {
        mDB = this.getReadableDatabase();
        ContentValues args = new ContentValues();
        args.put(IMAGE_STATUS, status);
        Boolean res = false;
        res = mDB.update(IMAGE_TABLE_NAME, args, IMAGE_TS + " = ? ", new String[]{ts}) > 0;


        return res;
        /*
        String update_query="Update "+DATABASE_CUSTOMER_TABLE+ " set "+KEY_STATUS+" =1  where "+KEY_TS+ " = "+ts;
       return mDB.rawQuery(update_query,null);
        System.out.println("Update query::::"+update_query);
        */

    }


    public boolean update_status_new(String ts, int status, String timestamp) {
        mDB = this.getReadableDatabase();
        ContentValues args = new ContentValues();
        args.put(IMAGE_STATUS, status);
        Boolean res = false;
        res = mDB.update(IMAGE_TABLE_NAME, args, IMAGE_TS + " = ?  AND " + IMAGE_TIMESTAMP + " = ? ", new String[]{ts,timestamp}) > 0;


        return res;
        /*
        String update_query="Update "+DATABASE_CUSTOMER_TABLE+ " set "+KEY_STATUS+" =1  where "+KEY_TS+ " = "+ts;
       return mDB.rawQuery(update_query,null);
        System.out.println("Update query::::"+update_query);
        */

    }

}

