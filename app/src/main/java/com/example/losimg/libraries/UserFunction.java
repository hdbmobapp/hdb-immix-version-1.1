package com.example.losimg.libraries;

import android.app.Activity;
import android.content.Context;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.hdb.BuildConfig;
import com.example.hdb.R;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Vijay on 1/20/2017.
 */

public class UserFunction extends Activity {
    public final static String BASE_URL ="https://hdbapp.hdbfs.com";
    private static String URL = BASE_URL + "/HDB_DMS_UPLD/HDB_DMS_UPLD_V4.5/index.php";

    Context context;

    public UserFunction(Context context) {
        jsonParser = new JSONParser(context);
    }

    JSONParser jsonParser;

    public void cutomToast(String message, Context contxt) {
        // Inflate the Layout
        ViewGroup parent = null;
        LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_toast,
                parent, false);
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(contxt);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    public JSONObject loginUser(String username, String password, String imei_no, String imei_no_1,
                                String latitude, String longitude, String device_name,
                                String device_man, String android_id) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "search");
            params.put("action", "search");
            params.put("target", "login");
            params.put("latitude", latitude);
            params.put("longitude", longitude);

            params.put("username", username);
            params.put("password", password);
            params.put("imei", imei_no);
            params.put("imei_1", imei_no_1);
            params.put("device_name", device_name);
            params.put("device_man", device_man);
            params.put("app_version", "QDE");
            params.put("device_id", android_id);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, context);
            Log.e("JSON", json.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public void noInternetConnection(String message, Context contxt) {
        Toast toast = null;
        if (toast == null
                || toast.getView().getWindowVisibility() != View.VISIBLE) {
            ViewGroup parent = null;
            LayoutInflater inflater = (LayoutInflater) contxt
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.toast_no_internet, parent,
                    false);
            TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
            mesg_name.setText(message);
            // Create Custom Toast
            toast = new Toast(contxt);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }

    public boolean logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }

    public boolean off_logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.off_resetTables();

        return true;
    }





    public JSONObject get_my_uploads(String user_id, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "my_uploads");
            params.put("target", "los");
            params.put("user_id", user_id);
            params.put("pg_no", pg_no);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject get_all_loan_details(String loan_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_los_details_upload");
            params.put("target", "los");
            params.put("loan_no_", loan_no);
            params.put("pg_no", pg_no);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject loan_search(String losid, String cust_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_los");
            params.put("target", "los");
            params.put("losid", losid);
            params.put("cust_id", cust_id);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }
    public JSONObject get_los_details(String losid) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_los_details");
            params.put("target", "los");
            params.put("losid", losid);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject verify_los(String losid,String cust_id,String cust_name,String bracnh_id,String banch_name,String product,
                                 String app_type,String latitude,String longitude,String upload_by,String timestamp,String app_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "verify_los");
            params.put("target", "los");
            params.put("losid", losid);
            params.put("cust_id", cust_id);
            params.put("cust_name", cust_name);
            params.put("branch_id", bracnh_id);
            params.put("branch_name", banch_name);
            params.put("product", product);
            params.put("app_type", app_type);
            params.put("latitude", latitude);
            params.put("longitude", longitude);
            params.put("upload_by", upload_by);
            params.put("timestamp", timestamp);
            params.put("app_no", app_no);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }



    public JSONObject get_masters(String id) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_list_details_master");
            params.put("target", "loan");
            params.put("id", id);

            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject Update_details(String str_mail, String str_mobile,String str_uid) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("target", "los");
            map.put("action", "update");
            map.put("mobile", str_mobile);
            map.put("email", str_mail);
            map.put("userid", str_uid);
            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject check_masters() {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "get_list_master");
            params.put("target", "loan");


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public boolean off_isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.off_getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }


    public boolean isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }

    public boolean logoutUser(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();
        //this.cutomToast("Hope you had a wonderful day! Do check back soon.",context);

        return true;
    }


    public JSONObject get_img_master(String cust_id, String app_type ,String user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "los");
            params.put("action", "get_img");
            params.put("cust_id", cust_id);
            params.put("app_type", app_type);
            params.put("user_id", user_id);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunction.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

}