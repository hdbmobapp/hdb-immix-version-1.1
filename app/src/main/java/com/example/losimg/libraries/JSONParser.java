package com.example.losimg.libraries;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class JSONParser extends Activity {

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = null;

	String str_error = null;
	SharedPreferences pref;

	// constructor
	public JSONParser(Context context) {

		pref = context.getSharedPreferences("MyPref", 0); // 0 -

	}




	public JSONObject makeHttpRequest_new(String url, String method,
                                          HashMap<String, String> params, final Context cont) {
		String response = "";
		URL url_n;

		String timeout;


		timeout=pref.getString("timeout","40000");

		//timeout="40000";

		System.out.println("SSL timeout::::"+timeout);
		// check for request method
		try {
			url_n = new URL(url);
			HttpsURLConnection conn = (HttpsURLConnection) url_n.openConnection();
			conn.setReadTimeout(Integer.valueOf(timeout));
			conn.setConnectTimeout(Integer.valueOf(timeout));
			conn.setDoInput(true);
			conn.setDoOutput(true);


			if (method == "POST") {
				conn.setRequestMethod("POST");
			} else {
				conn.setRequestMethod("GET");
			}
			if (params != null) {
				OutputStream ostream = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(ostream, "UTF-8"));
				StringBuilder requestresult = new StringBuilder();
				boolean first = true;
				for (Map.Entry<String, String> entry : params.entrySet()) {
					if (first)
						first = false;
					else
						requestresult.append("&");
					requestresult.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
					requestresult.append("=");
					requestresult.append(entry.getValue());
				}
				writer.write(requestresult.toString());

				writer.flush();
				writer.close();
				ostream.close();

				System.out.println("requestresult::::" + requestresult);
			}
			int reqresponseCode = conn.getResponseCode();

			if (reqresponseCode == HttpsURLConnection.HTTP_OK) {
				String line;
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "iso-8859-1"));
				while ((line = bufferedReader.readLine()) != null) {
					response += line;
				}
			} else {
				response = "";
			}
			System.out.println("requestresult:::response:" + response);

		} catch (Exception e) {
			e.printStackTrace();
		}


		if (response != null) {

			try {

				json = response.toString();
			} catch (Exception e) {
				Log.e("Buffer Error", "Error converting result " + e.toString());
			}
			// try parse the string to a JSON object
			try {
				jObj = new JSONObject(json);
			} catch (JSONException e) {
				Log.e("JSON Parser", "Error parsing data " + e.toString());
			}
		} else {
			jObj = null;
		}


		// return JSON String
		return jObj;

	}

	@SuppressWarnings("unused")
	private void tToast(String s) {
	    Context context = getApplicationContext();
	    int duration = Toast.LENGTH_LONG;
	    Toast toast = Toast.makeText(context, s, duration);
	    toast.show();
	}
}