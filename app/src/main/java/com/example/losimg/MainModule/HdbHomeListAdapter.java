package com.example.losimg.MainModule;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hdb.R;


public class HdbHomeListAdapter extends ArrayAdapter<String> {

	private final Activity context;
	private final String[] itemname;
	private final Integer[] imgid;

	public HdbHomeListAdapter(Activity context, String[] itemname,
                              Integer[] imgid) {
		super(context, R.layout.dms_hdb_home_list_item, itemname);

		this.context = context;
		this.itemname = itemname;
		this.imgid = imgid;
	}


	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView = inflater
				.inflate(R.layout.hdb_home_list_item, null, true);

//		Typeface font = Typeface.createFromAsset(parent.getContext()
//				.getAssets(), "fonts/MyriadPro-Regular.otf");

		TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		TextView txtmsg_count = (TextView) rowView.findViewById(R.id.msg_count);
		txtmsg_count.setVisibility(View.GONE);

	/*	if (position == 2) {
			txtmsg_count.setText(HdbHome.str_total);
			if(HdbHome.str_total.equals("0")){
			txtmsg_count.setVisibility(View.GONE);}else{
				txtmsg_count.setVisibility(View.VISIBLE);
			}
		}


		if (position == 4) {
			System.out.println("COUNNT::::"+HdbHome.msg_count);
			txtmsg_count.setText(HdbHome.msg_count);
			txtmsg_count.setVisibility(View.VISIBLE);
			//txtmsg_count.setTypeface(font);
		}
*/
		txtTitle.setText(itemname[position]);
		imageView.setImageResource(imgid[position]);
		//txtTitle.setTypeface(font);

		return rowView;

	}
}
