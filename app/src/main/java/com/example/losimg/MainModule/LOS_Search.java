package com.example.losimg.MainModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.hdb.R;
import com.example.losimg.img_upload_retrofit.Image_List;
import com.example.losimg.img_upload_retrofit.ImagesList;
import com.example.losimg.libraries.ConnectionDetector;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class LOS_Search extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    UserFunction userFunction;
    JSONObject jsonobject = null;
    JSONObject json = null;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    String str_message;
    TextView txt_error;
    EditText edt_losid;
    String str_losid;
    LinearLayout ly_search_section;
    ProgressBar progressbar_loading;
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;
    LosSearchList adapter;
    ListView mList;
    public static String app_id = "app_id";
    public static String cust_id = "cust_id";
    public static String cust_name = "cust_name";
    public static String product = "product";
    public static String branch_id = "branch_id";
    public static String branch_name = "branch_name";
    public static String app_type = "app_type";
    public static String app_no = "app_no";

    public static String str_user_id;
    SharedPreferences pref;
    static String str_ts;
    Integer from;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_serach_los);
        userFunction = new UserFunction(this);
        cd = new ConnectionDetector(LOS_Search.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        System.out.println("str_user_id::::" + str_user_id);

        Intent i = getIntent();
        from = i.getIntExtra("from",0);

        str_losid = i.getStringExtra("losid");

        setlayout();
    }

    private void setlayout() {


        TextView txt_message = (TextView) findViewById(R.id.txt_message);
        txt_error = (TextView) findViewById(R.id.text_error_msg);

        edt_losid = (EditText) findViewById(R.id.edt_losid);
        ly_search_section = (LinearLayout) findViewById(R.id.ly_search_section);
        progressbar_loading = (ProgressBar) findViewById(R.id.progressbar_loading);
        mList = (ListView) findViewById(R.id.list);
        str_ts= getDateTime();

        if(from==1){
            ly_search_section.setVisibility(View.GONE);
            edt_losid.setText(str_losid);

            if (cd.isConnectingToInternet()) {
                // new VerifyLos().execute();
                arraylist = new ArrayList<HashMap<String, String>>();

                new DownloadJSON().execute();

            } else {
                txt_error.setText(getResources().getString(R.string.no_internet));
                txt_error.setVisibility(View.VISIBLE);

            }

        }else{
            ly_search_section.setVisibility(View.VISIBLE);


        }

        Button btn_verify = (Button) findViewById(R.id.btn_search);
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("app_cnt",0);
                editor.commit();

                str_losid = edt_losid.getText().toString();


                if (str_losid != null && str_losid != "" && !str_losid.isEmpty()) {

                    if (cd.isConnectingToInternet()) {
                        // new VerifyLos().execute();
                        arraylist = new ArrayList<HashMap<String, String>>();

                        new DownloadJSON().execute();

                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Enter LOSID ...",
                            getApplicationContext());
                }

            }

        });

/*
        btn_applicant_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_losid != null && str_losid != "" && !str_losid.isEmpty()) {

                   String str_ts= getDateTime();
                    Intent img_capture = new Intent(LOS_Search.this, Image_List.class);
                    img_capture.putExtra("str_losid", str_losid);
                    img_capture.putExtra("app_type", "P");
                    img_capture.putExtra("str_ts", str_ts);


                    startActivity(img_capture);
                }
            }

        });

        */
    }


    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LOS_Search.this);
            mProgressDialog.setMessage(getApplicationContext().getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;

            jsonobject = userFunction.get_los_details(str_losid);
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                JSONObject jsonobject_nw = jsonobject.getJSONObject("data");

                jsonarray = jsonobject_nw.getJSONArray("Data");

//                    txt_error.setVisibility(View.GONE);
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("app_id", jsonobject.getString("app_id"));
                    map.put("cust_id", jsonobject.getString("cust_id"));
                    map.put("cust_name", jsonobject.getString("cust_name"));
                    map.put("product", jsonobject.getString("product_id"));
                    map.put("branch_id", jsonobject.getString("branch_id"));
                    map.put("branch_name", jsonobject.getString("branch_name"));
                    map.put("app_type", jsonobject.getString("app_type"));
                    map.put("app_no", jsonobject.getString("app_no"));



                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                if(jsonarray.length()==0){
                    txt_error.setText("No record Found ");
                    txt_error.setVisibility(View.VISIBLE);
                }else{
                    txt_error.setVisibility(View.GONE);
                }

                adapter = new LosSearchList(getApplicationContext(), arraylist);
                mList.setAdapter(adapter);
                adapter.notifyDataSetChanged();


            }
            mProgressDialog.dismiss();

        }

    }
/*
    public class VerifyLos extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(LOS_Search.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            txt_error.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            jsonobject = userFunction.los_verify(str_losid);
            try {42228
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);
                        String error_msg = jsonobject.getString("message");
                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                            Str_cust_name = jsonobject.getString("cust_name");
                            str_branch = jsonobject.getString("branch_id");
                        } else {
                            str_message = error_msg;
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            TextView txt_message = (TextView) findViewById(R.id.txt_message);

            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);
                    txt_error.setText("LOSID Successfully Verified");
                    txt_name.setText("Name: " + Str_cust_name);
                    txt_branch.setText("Branch: " + str_branch);
                    ly_userinfo.setVisibility(View.VISIBLE);
                    ly_img_update.setVisibility(View.VISIBLE);

                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    // pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }


            }
            mProgressDialog.dismiss();

        }

    }
*/

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

    public void go_home(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(messagingActivity);
        finish();

    }

}