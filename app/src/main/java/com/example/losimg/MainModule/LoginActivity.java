package com.example.losimg.MainModule;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.R;

import com.example.losimg.libraries.ConnectionDetector;
import com.example.losimg.libraries.DBHelper;
import com.example.losimg.libraries.GPSTracker_New;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class LoginActivity extends Activity {

    Button btnLogin;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    CheckBox rememberMe;
    TextView txt_new_link;
    Integer off_rowcount;
    JSONArray jsonarray;
    ArrayList<String> master_flag;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    SharedPreferences sharedpreferences;
    String email, password;
    UserFunction userFunction;
    JSONObject json;
    private ConnectionDetector cd;
    private static Typeface font;
    //String user_name = null;
    double dbl_latitude;
    double dbl_longitude;
    String gcm = null;
    @SuppressWarnings("unused")
    private static final Pattern EMAIL_PATTERN = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,100}" + "@"
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,10}" + "(" + "."
                    + "[a-zA-Z0-9][a-zA-Z0-9-]{0,20}" + ")+");
    private static final Pattern USERNAME_PATTERN = Pattern
            .compile("[a-zA-Z0-9]{1,250}");
    private static final Pattern PASSWORD_PATTERN = Pattern
            .compile("[a-zA-Z0-9+_.]{2,14}");
    Context context;
    ProgressBar pb_progress;

    String res, resp_success;

    public static String KEY_STATUS = "status";

    public static String KEY_SUCCESS = "success";

    String
            flag_pm,
            id_pm,

    flag_pd,
            id_pd, flag_cc, id_cc, flag_cr, id_cr, flag_ccm, id_ccm, ac_flag, ac_id, bp_flag, bp_id, mp_flag, mp_id, mlp_flag, mlp_id, man_flag, man_id, app_flag, app_id;

    JSONObject jsonobject;

    ProgressDialog mProgressDialog;

    String str_dispo_id, str_dispo, str_flag;

    String reg_id;

    SharedPreferences pref;
    String lattitude = null, longitude = null;
    EditText edt_new_version_link;
    String new_version;
    TextView txt_new_version;
    String new_version_name;

    DBHelper rDB;

    HashMap<String, String> user = new HashMap<String, String>();

    private String android_id;
    String check_master_flag;
    ArrayList<HashMap<String, String>> arraylist;

    int count_btn = 0, count_btn_1 = 0, count_btn_2 = 0, count_btn_3 = 0, count_btn_4 = 0, count_btn_5 = 0, count_btn_6 = 0, count_btn_7 = 0, count_btn_8 = 0, count_btn_9 = 0, count_btn_10 = 0, count_btn_11 = 0;
    String imeiNumber1 ; //(API level 23)
    String imeiNumber2 ;// tm.getDeviceId(2);



    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA


    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.login);
        // font = Typeface.createFromAsset(getBaseContext().getAssets(),
        //       "fonts/Amaranth-Regular.otf");
        cd = new ConnectionDetector(getApplicationContext());

        userFunction = new UserFunction(this);

        arraylist = new ArrayList<>();
        master_flag = new ArrayList<String>();
        System.out.println("LOGIN REDIRECT::::");

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);





        rDB = new DBHelper(this);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        // for



/*
        if(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        }else {
            android_id= Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }

*/


        //cons code
        checkPermissions();
        SetLayout();


    }

    protected void SetLayout() {
        // Check if Internet present
        setContentView(R.layout.dms_sl_login);
        addListenerOnChkPwd();
        // stop executing code by return
        inputEmail = (EditText) findViewById(R.id.login_email);
        inputPassword = (EditText) findViewById(R.id.login_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        pb_progress = (ProgressBar) findViewById(R.id.progress_login);
        rememberMe = (CheckBox) findViewById(R.id.chk_remember_me);
        edt_new_version_link = (EditText) findViewById(R.id.edt_new_version_link);
        txt_new_version = (TextView) findViewById(R.id.txt_new_version);
        txt_new_link = (TextView) findViewById(R.id.txt_new_link);


        //txt_new_link.setText("https://hdbapp.hdbfs.com/HDB_APPS/home.htm");

        // ---set Custom Font to textview
        //  inputEmail.setTypeface(font);

        //inputPassword.setTypeface(font);

        // btnLogin.setTypeface(font);
        // loginErrorMsg.setTypeface(font);

        GPSTracker_New tracker = new GPSTracker_New(LoginActivity.this);
        // check if location is available

        if (tracker.isLocationEnabled()) {
            dbl_latitude = tracker.getLatitude();
            dbl_longitude = tracker.getLongitude();

            lattitude = String.valueOf(dbl_latitude);
            longitude = String.valueOf(dbl_longitude);
        } else {
            // show dialog box to user to enable location
            tracker.askToOnLocation();
        }


        String strUsername = pref.getString("user_username", null);
        String strPassword = pref.getString("user_password", null);

        inputEmail.setText(strUsername);
        inputPassword.setText(strPassword);



        //inputEmail.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //  inputEmail.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

        //CapsText.afterTextChanged((Editable)inputEmail);


        /*
        inputEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    inputEmail.setText(s);
                }
                inputEmail.setSelection(inputEmail.getText().length());
            }
        });
*/

        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                //  GCMRegistrar.register(LoginActivity.this,
                //         GCMIntentService.SENDER_ID);

                TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    //getDeviceId() is Deprecated so for android O we can use getImei() method
                    imeiNumber1=tm.getImei(1);
                    imeiNumber2=tm.getImei(2);
                }
                else {
                     // imeiNumber1=tm.getDeviceId(0);
                    //  imeiNumber2=tm.getDeviceId(1);
                }

                System.out.println("reg_id:::::" + reg_id);

                email = inputEmail.getText().toString();
                password = inputPassword.getText().toString();
                // Check if Internet present
                TextView error_pwd = (TextView) findViewById(R.id.login_password_error_msg);
                TextView error_email = (TextView) findViewById(R.id.login_email_error_msg);
                //       error_pwd.setTypeface(font);
                //     error_email.setTypeface(font);

                if (password.equals("") || email.equals("")) {

                    if (password.equals("")) {
                        error_pwd.setVisibility(View.VISIBLE);
                        error_pwd.setText("Please enter a password");

                    } else {
                        error_pwd.setVisibility(View.GONE);

                    }
                    if (email.equals("")) {
                        error_email.setVisibility(View.VISIBLE);
                        error_email.setText("Please enter a Employee Id");
                    } else {
                        error_email.setVisibility(View.GONE);

                    }
                } else {

                    if (cd.isConnectingToInternet()) {

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);

                        new Login().execute();

                    } else {
                        userFunction.noInternetConnection(
                                "Check your connection settings!",
                                getApplicationContext());

                        loginErrorMsg.setText("Check your connection settings!");
                        /*

                        error_pwd.setVisibility(View.GONE);
                        error_email.setVisibility(View.GONE);

                       // userFunction.logoutUserclear(getApplicationContext());

                        user=   db.off_getUserDetails();

                        System.out.println("HASHMAP::::" + user);

                        String val=(String)user.get("uid");
                        String dev_val=(String)user.get("dev_id");





                        if(val!=null) {

                            if (val.equals(email)) {

                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "yyyy-MM-dd");
                                String current_date = sdf.format(new Date());

                                Editor editor = pref.edit();
                                editor.putString("current_date_off", current_date);
                                editor.putInt("off_is_login", 1);


                                editor.commit();
                                System.out.println("OFF LOGIN:::");

                                off_rowcount=db.off_getRowCount();
                                if(off_rowcount>0) {
                                    if (!dev_val.equals("")) {

                                        System.out.println("DEVICE ID VAL:::" + dev_val);

                                        if (!dev_val.equals(android_id)) {
                                            userFunction.cutomToast(
                                                    "You have changed your handset. Contact your supervisor",
                                                    getApplicationContext()

                                            );

                                            loginErrorMsg.setText("You have changed your handset. Contact your supervisor");


                                        } else {
                                            Intent messagingActivity = new Intent(
                                                    LoginActivity.this, HdbHome.class);
                                            startActivity(messagingActivity);
                                            finish();
                                        }
                                    }else {
                                        Intent messagingActivity = new Intent(
                                                LoginActivity.this, Masters.class);
                                        startActivity(messagingActivity);
                                        finish();
                                    }
                                }else {
                                    userFunction.noInternetConnection(
                                            "Check your connection settings!",
                                            getApplicationContext());

                                    loginErrorMsg.setText("Check your connection settings!");
                                }
                            } else {
                               userFunction.cutomToast(
                                        "Username or Password is incorrect",
                                        getApplicationContext());

                                loginErrorMsg.setText("Username or Password is incorrect");

                            }
                        }else {
                            userFunction.noInternetConnection(
                                    "Check your connection settings!",
                                    getApplicationContext());

                            loginErrorMsg.setText("Check your connection settings!");
                        }



                        */
                    }
                }


            }
        });

    }

    public void addListenerOnChkPwd() {

        CheckBox chk_pwd = (CheckBox) findViewById(R.id.chk_show_pwd);

        chk_pwd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is chkIos checked?
                if (((CheckBox) v).isChecked()) {
                    inputPassword.setTransformationMethod(null);
                } else {
                    inputPassword
                            .setTransformationMethod(new PasswordTransformationMethod());

                }

            }
        });
    }


    // Login AsyncTask
    public class Login extends AsyncTask<Void, Void, Void> {

        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage(getApplicationContext().getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            String str_imei = getIMEI(LoginActivity.this);

            try {
                String deviceName = Build.MODEL;
                String deviceMan = Build.MANUFACTURER;

                json = userFunction.loginUser(email, password, imeiNumber1,imeiNumber2,
                        lattitude, longitude, deviceName, deviceMan, android_id);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            try {
                if (json != null) {
                    String KEY_STATUS = "status";
                    if (json.getString(KEY_STATUS) != null) {
                        loginErrorMsg.setText("");
                        String res = json.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = json.getString(KEY_SUCCESS);


                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {


                      //   String refreshtoken= FirebaseInstanceId.getInstance().getToken();

                     //       System.out.println("refreshtoken::::::"+refreshtoken);

                            JSONArray jsn_arr;
                            JSONObject json_user = json.getJSONObject("data");
                            //   JSONObject json_user1 = json.getJSONObject("imei");

                            // Clear all previous data in database


                            if (!json_user.getString("branchid").toString().equals("null")) {


                                if (json_user.getString("branchid") != null) {

                                    userFunction
                                            .logoutUserclear(getApplicationContext());

                                    userFunction
                                            .off_logoutUserclear(getApplicationContext());


                                    String KEY_EMAIL = "email";
                                    String KEY_NAME = "name";
                                    String KEY_UID = "id";
                                    String KEY_deviceid = "device_id";



                                    SimpleDateFormat sdf = new SimpleDateFormat(
                                            "yyyy-MM-dd");
                                    String current_date = sdf.format(new Date());

                                    Editor editor = pref.edit();
                                    editor.putString("current_date", current_date);

                                    editor.putInt("is_login", 1);
                                    editor.putString("user_id",
                                            json_user.getString(KEY_UID).toString());
                                    editor.putString("user_name",
                                            json_user.getString(KEY_NAME).toString());
                                    String KEY_BRANCH = "branch";
                                    editor.putString("user_brnch",
                                            json_user.getString(KEY_BRANCH).toString());
                                    editor.putString("user_email",
                                            json_user.getString(KEY_EMAIL).toString());
                                    editor.putString("user_designation",
                                            json_user.getString("designation").toString());
                                    editor.putString("user_designation_value",
                                            json_user.getString("user_designation_value").toString());

                                    editor.putString("avi_tel",
                                            json_user.getString("Avinash").toString());

                                    editor.putString("sach_tel",
                                            json_user.getString("Sachin").toString());

                                    editor.putString("vija_tel",
                                            json_user.getString("Vijay").toString());

                                    editor.putString("service_tel",
                                            json_user.getString("Service").toString());

                                    editor.putString("mobile",
                                            json_user.getString("mobile").toString());

                                    editor.putString("timeout",
                                            json_user.getString("timeout").toString());

                                    editor.putInt("img_size",
                                            json_user.getInt("img_size"));


                                    Boolean del_flag;


                                    if (!json_user.getString("img_height").equals("")) {
                                        editor.putInt("img_height",
                                                Integer.valueOf(json_user.getString("img_height")));
                                    }

                                    if (!json_user.getString("img_width").equals("")) {
                                        editor.putInt("img_width",
                                                Integer.valueOf(json_user.getString("img_width")));
                                    }

                                    if (!json_user.getString("img_comp_size").equals("")) {
                                        editor.putInt("img_comp_size",
                                                Integer.valueOf(json_user.getString("img_comp_size").toString()));
                                    }

                                    System.out.println("Service::::::" + json_user.getString("Service").toString());
                                    if (rememberMe.isChecked()) {
                                        editor.putString("user_username", email);
                                        editor.putString("user_password", password);
                                    } else {
                                        editor.putString("user_username", "");
                                        editor.putString("user_password", "");
                                    }


                                    System.out.println("BRR ID:::" + json_user.getString("branchid"));
                                    editor.putString("branchid",
                                            json_user.getString("branchid"));
                                    editor.commit();

                                    Intent messagingActivity2 = new Intent(LoginActivity.this, HdbHome.class);
                                    startActivity(messagingActivity2);

                                } else {
                                    callPopup_branch();
                                }
                            } else {
                                callPopup_branch();
                            }


                        } else {
                            String KEY_DEVID = "imei";

                            if (!json.getString(KEY_DEVID).toString().equals("")) {
                                System.out.println("hahaahhehehehee inside");
                              //  db.update_deviceid(json.getString(KEY_DEVID));
                            }


                            if (new_version != null && new_version != "") {
                                edt_new_version_link.setText(new_version);
                                edt_new_version_link.setVisibility(View.VISIBLE);
                                txt_new_version.setText(new_version_name);
                                txt_new_version.setVisibility(View.VISIBLE);
                            }

                            loginErrorMsg.setText(Html.fromHtml(json.getString("message").toString()));

                            txt_new_link.setText(json.getString("link").toString());

                        }


                    }
                } else {
                    userFunction.cutomToast(
                            getResources().getString(R.string.error_message),
                            getApplicationContext());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //pb_progress.setVisibility(View.GONE);
            mProgressDialog.dismiss();
        }

    }


    public String getIMEI(Context context) {

        TelephonyManager mngr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return null;
    }






    private void callPopup_branch() {


        // custom dialog
        AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
        // dialog.setContentView(R.layout.pop_up_branch);
        dialog.setTitle("Branch Not Mapped");


        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        dialog.setMessage("Branch is not mapped against your Employee ID. Contact your Supervisor");

        dialog.show();


    }

    public void go_contacts(View v) {
        Intent messagingActivity = new Intent(getApplicationContext(),
                Contact_us.class);
        startActivity(messagingActivity);
        finish();

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
            }
            return;
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }
}


