package com.example.losimg.MainModule;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.losimg.libraries.ConnectionDetector;
import com.example.losimg.libraries.LoadMoreListView;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Loan_upload_search extends Activity {
    // Within which the entire activity is enclosed
    // ListView represents Navigation Drawer
    ListView mList;
    static String str_region;

    public int pgno = 0;
    String str_losid;

    // ActionBarDrawerToggle indicates the presence of Navigation Drawer in the
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    ConnectionDetector cd;

    // Title of the action bar
    String mTitle = "";
    static String str_ts;
    SharedPreferences pref;

    String str_user_id;
    String str_user_name;
    String str_losid_no;
    String str_app_form_no;

    Loan_upload_search_adapter adapter;
    JSONObject jsonobject = null;
    JSONObject json = null;
    ProgressDialog mProgressDialog;
    UserFunction userFunction;

    public static String app_id = "app_id";
    public static String cust_id = "cust_id";
    public static String cust_name = "cust_name";
    public static String product = "product";
    public static String branch_id = "branch_id";
    public static String branch_name = "branch_name";
    public static String app_type = "app_type";
    public static String app_no = "app_no";
    public static String upload_date = "upload_date";


    UserFunction userfunction;
    TextView txt_error;
    EditText edt_losid;
    Button btn_losid;
    View rootView;
    ProgressBar progressbar_loading;

    Button btn_home;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_loan_upload_search);
        userFunction = new UserFunction(Loan_upload_search.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -

        str_user_id = pref.getString("user_id", null);
        str_user_name = pref.getString("user_name", null);

        System.out.println("user_id:::" + str_user_id);

        setlayout();


        cd = new ConnectionDetector(getApplicationContext());


        // Setting the adapter on mDrawerList
        // mDrawerList.setAdapter(adapter);

        btn_home = (Button) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent messagingActivity = new Intent(getApplicationContext(),
                        HdbHome.class);
                startActivity(messagingActivity);

            }
        });


    }


    public void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        arraylist = new ArrayList<HashMap<String, String>>();
        edt_losid = (EditText) findViewById(R.id.edt_losid);
        str_ts = getDateTime();

        mList = (ListView) findViewById(R.id.list);


        progressbar_loading = (ProgressBar) findViewById(R.id.progressbar_loading);

        Button btn_verify = (Button) findViewById(R.id.btn_search);
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("app_cnt", 0);
                editor.commit();

                str_losid = edt_losid.getText().toString();


                if (str_losid != null && str_losid != "" && !str_losid.isEmpty()) {

                    if (cd.isConnectingToInternet()) {
                        // new VerifyLos().execute();
                        arraylist = new ArrayList<HashMap<String, String>>();

                        new DownloadJSON().execute();

                    } else {
                        txt_error.setText(getResources().getString(R.string.no_internet));
                        txt_error.setVisibility(View.VISIBLE);

                    }
                } else {
                    userFunction.cutomToast("Enter LOSID ...",
                            getApplicationContext());
                }

            }

        });

    }

    public class DownloadJSON extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Loan_upload_search.this);
            mProgressDialog.setMessage(getApplicationContext().getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;

            jsonobject = userFunction.get_all_loan_details(str_losid, "0");
            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                //JSONObject jsonobject_nw = jsonobject.getJSONObject("data");

                jsonarray = jsonobject.getJSONArray("Data");

//                    txt_error.setVisibility(View.GONE);
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("app_id", jsonobject.getString("app_id"));
                    map.put("cust_id", jsonobject.getString("cust_id"));
                    map.put("cust_name", jsonobject.getString("cust_name"));
                    map.put("product", jsonobject.getString("product_id"));
                    map.put("branch_id", jsonobject.getString("branch_id"));
                    map.put("branch_name", jsonobject.getString("branch_name"));
                    map.put("app_type", jsonobject.getString("app_type"));
                    map.put("app_no", jsonobject.getString("app_no"));
                    map.put("upload_date", jsonobject.getString("upload_date"));


                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            if (jsonobject == null) {
//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);
            } else {
                if(jsonarray.length()==0){
                    txt_error.setText("No record Found ");
                    txt_error.setVisibility(View.VISIBLE);
                }else{
                    txt_error.setVisibility(View.GONE);
                }
                adapter = new Loan_upload_search_adapter(getApplicationContext(), arraylist);
                adapter.notifyDataSetChanged();
                mList.setAdapter(adapter);

                System.out.println("INSIDE::: post excdf:");
                ((LoadMoreListView) mList)
                        .setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
                            public void onLoadMore() {
                                // Do the work to load more items at the end of list
                                // here
                                new LoadDataTask().execute();
                            }
                        });

            }
            mProgressDialog.dismiss();
        }

    }


    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }


    private class LoadDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            if (isCancelled()) {
                return null;
            }

            // Simulates a background task
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            String tmpgno;

            int value = pgno + 1;
            pgno = value;
            tmpgno = Integer.toString(value);


            //getActivity().getApplicationContext().viewPager.setCurrentItem(2, true);

            jsonobject = userFunction.get_all_loan_details(str_losid, tmpgno);

            // Create an array
            if (jsonobject != null) try {
                // Locate the array name in JSON
                //  JSONObject jsonobject_nw = jsonobject.getJSONObject("data");

                jsonarray = jsonobject.getJSONArray("Data");

//                    txt_error.setVisibility(View.GONE);
                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<>();
                    jsonobject = jsonarray.getJSONObject(i);
                    map.put("app_id", jsonobject.getString("app_id"));
                    map.put("cust_id", jsonobject.getString("cust_id"));
                    map.put("cust_name", jsonobject.getString("cust_name"));
                    map.put("product", jsonobject.getString("product_id"));
                    map.put("branch_id", jsonobject.getString("branch_id"));
                    map.put("branch_name", jsonobject.getString("branch_name"));
                    map.put("app_type", jsonobject.getString("app_type"));
                    map.put("app_no", jsonobject.getString("app_no"));


                    // Set the JSON Objects into the array
                    arraylist.add(map);
                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            // We need notify the adapter that the data have been changed
            adapter.notifyDataSetChanged();

            // Call onLoadMoreComplete when the LoadMore task, has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();

            super.onPostExecute(result);
        }

        @Override
        protected void onCancelled() {
            // Notify the loading more operation has finished
            ((LoadMoreListView) mList).onLoadMoreComplete();
        }
    }

    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }
}
