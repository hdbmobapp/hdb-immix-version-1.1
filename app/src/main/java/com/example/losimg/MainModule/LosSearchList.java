package com.example.losimg.MainModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.losimg.img_upload_retrofit.Image_List;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by DELL on 12/4/2017.
 */

public class LosSearchList extends BaseAdapter {


    // Declare Variables
    ProgressDialog dialog = null;
    ProgressDialog mProgressDialog;
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();
    String str_product_id;
    ArrayAdapter<String> adapter;

    Integer int_pg_from;

    String app_id;
    JSONObject jsonobject = null;
    UserFunction userFunction;
    ViewHolder holder = null;
    Boolean is_upload = false;
    Integer int_r=0;
    SharedPreferences pref;

    public LosSearchList(Context context,
                         ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        TableRow row_remraks;
        dialog = new ProgressDialog(context);
        userFunction = new UserFunction(context);
        pref = context.getSharedPreferences("MyPref", 0); // 0 -

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }
    // Declare Variables

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);


        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.dms_uploads_list_item, parent,
                    false);


            holder = new ViewHolder();

            holder.tv_app_id = (TextView) itemView.findViewById(R.id.txt_losid);

            holder.tv_cust_id = (TextView) itemView
                    .findViewById(R.id.txt_cust_id);

            holder.tv_customer_name = (TextView) itemView
                    .findViewById(R.id.txt_cust_name);

            holder.tc_product = (TextView) itemView
                    .findViewById(R.id.txt_product);

            holder.tv_applicant_type = (TextView) itemView
                    .findViewById(R.id.txt_applicant_type);


            holder.tv_branch_name = (TextView) itemView
                    .findViewById(R.id.txt_branch_name);

            holder.btn_verify = (Button) itemView
                    .findViewById(R.id.btn_verify);
            holder.btn_capture_pic = (Button) itemView
                    .findViewById(R.id.btn_pic_capture);

            holder.tv_app_cnt = (TextView) itemView
                    .findViewById(R.id.txt_app_cnt);


//            holder.tv_phone_no = (TextView) itemView
//                    .findViewById(R.id.txt_product);

            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }


        //System.out.println("PR::::" + resultp.get(MyUploads.product) + ":::err code:::" + resultp.get(MyUploads.error_code) + ":::err desc:::" + resultp.get(MyUploads.error_desc));


        holder.tv_app_id.setText(resultp.get(LOS_Search.app_id));
        holder.tv_cust_id.setText(resultp.get(LOS_Search.cust_id));
        holder.tv_customer_name.setText(resultp.get(LOS_Search.cust_name));
        holder.tc_product.setText(resultp.get(LOS_Search.product));
        holder.tv_branch_name.setText(resultp.get(LOS_Search.branch_name));


        //  holder.btn_capture_pic.setVisibility(View.INVISIBLE);

        String str_type_app = null;
        String type_app = resultp.get(LOS_Search.app_type);

        switch (type_app) {
            case "P":
                str_type_app = "Primary Applicant";
                holder.tv_app_cnt.setText("P");
                break;
            case "C":
                str_type_app = "CO-Applicant";
                int_r = pref.getInt("app_cnt", 0);
                holder.tv_app_cnt.setText("CO_APP_"+(int_r+1));
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("app_cnt",int_r+1);
                editor.commit();
                break;
            case "G":
                str_type_app = "Guarantor";
                break;

            default:
                str_type_app = "";
        }

        holder.tv_applicant_type.setText(str_type_app);

        //  holder.tv_phone_no.setText(resultp.get(LOS_Search.phone_no));


        // holder.tv_product.setText(resultp.get(MyUploads.product));

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);
                app_id = resultp.get(LOS_Search.app_id).toString();
                Intent pd_form = new Intent(context, Loan_Result.class);

                pd_form.putExtra("str_losid", resultp.get(LOS_Search.app_id).toString());
                pd_form.putExtra("str_cust_id", resultp.get(LOS_Search.cust_id).toString());
                pd_form.putExtra("str_ts",LOS_Search.str_ts);
                pd_form.putExtra("app_type", resultp.get(LOS_Search.app_type).toString());
                pd_form.putExtra("str_app_no", resultp.get(LOS_Search.app_no).toString());


                pd_form.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(pd_form);


            }
        });


/*
        holder.btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                String losid, cust_id, cust_name, bracnh_id, banch_name, product,
                        app_type, latitude = null, longitude = null, upload_by, timestamp;

                losid = resultp.get(LOS_Search.app_id);
                cust_id = resultp.get(LOS_Search.cust_id);
                cust_name = resultp.get(LOS_Search.cust_name);
                bracnh_id = resultp.get(LOS_Search.branch_id);
                banch_name = resultp.get(LOS_Search.branch_name);
                product = resultp.get(LOS_Search.product);
                app_type = resultp.get(LOS_Search.app_type);
                upload_by = LOS_Search.str_user_id;
                timestamp = LOS_Search.str_ts;

                // is_upload=false;
                System.out.println("is_upload::00::" + is_upload);


                System.out.println("is_upload::::" + is_upload);

                if (is_upload == true) {
                    System.out.println("is_upload:999:::" + is_upload);

                    holder.btn_capture_pic.setVisibility(View.VISIBLE);
                    holder.btn_verify.setVisibility(View.INVISIBLE);

                }
                notifyDataSetChanged();



            }
        });
*/

        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_app_id;
        public TextView tv_cust_id;
        public TextView tv_customer_name;
        public TextView tc_product;
        public TextView tv_applicant_type;
        public TextView tv_branch_name;
        public TextView tv_app_cnt;
        //public TextView tv_phone_no;
        public Button btn_verify, btn_capture_pic;


    }



    private String getDateTime() {
        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        return format;
    }

}
