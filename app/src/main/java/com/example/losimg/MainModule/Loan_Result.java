package com.example.losimg.MainModule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.hdb.R;
import com.example.losimg.img_upload_retrofit.Image_List;
import com.example.losimg.libraries.ConnectionDetector;
import com.example.losimg.libraries.DBHelper;
import com.example.losimg.libraries.GPSTracker_New;
import com.example.losimg.libraries.UserFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Loan_Result extends AppCompatActivity {
    ProgressDialog mProgressDialog;
    UserFunction userFunction;
    JSONObject jsonobject = null;
    JSONObject json = null;
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String res;
    String resp_success;
    String str_message;
    ConnectionDetector cd;
    String str_user_id, str_losid;
    SharedPreferences pref;

    String str_name, str_branch_id, str_branch_name, str_product, str_app_type, str_loan_no, str_cust_id;
    TextView txt_error, txt_message;
    Button btn_capture_pic, btn_verify;
    String str_app_no;
    String str_ts;
    String str_app_nw_type;
    Integer int_is_verified;
    // action bar
    ArrayList<HashMap<String, String>> arraylist;
    JSONArray jsonarray = null;
    Loan_Result_Adapter adapter;
    GridView mList;
    public static String img_name = "img_name";
    public static String img_data = "img_data";
    public static String img_no = "img_no";
    public static String img_count = "img_count";

   static Double dbl_latitude;
    static Double dbl_longitude;
    String lattitude = null, longitude = null;
    DBHelper rDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.dms_loan_result);
        userFunction = new UserFunction(this);
        cd = new ConnectionDetector(Loan_Result.this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 -
        str_user_id = pref.getString("user_id", null);
        System.out.println("str_user_id::::" + str_user_id);
        arraylist = new ArrayList<HashMap<String, String>>();
        rDB = new DBHelper(this);

        rDB.delete_IMGo_status("0");

        Intent i = getIntent();
        str_losid = i.getStringExtra("str_losid");
        str_cust_id = i.getStringExtra("str_cust_id");
        str_ts = i.getStringExtra("str_ts");
        str_app_no = i.getStringExtra("str_app_no");

        setlayout();
    }


    private void setlayout() {
        txt_error = (TextView) findViewById(R.id.text_error_msg);
        txt_message = (TextView) findViewById(R.id.text_error_msg);
        btn_capture_pic = (Button) findViewById(R.id.btn_pic_capture);
        btn_verify = (Button) findViewById(R.id.btn_verify);
        mList = (GridView) findViewById(R.id.grid);

        Button btn_add_customer = (Button) findViewById(R.id.btn_add_customer);


        btn_add_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent los_search = new Intent(Loan_Result.this, LOS_Search.class);
                los_search.putExtra("losid", str_loan_no);
                los_search.putExtra("from", 1);
                startActivity(los_search);

            }
        });

        if (cd.isConnectingToInternet()) {
            // new VerifyLos().execute();
            new SearchLos().execute();

        } else {
            txt_error.setText(getResources().getString(R.string.no_internet));
            txt_error.setVisibility(View.VISIBLE);

        }

        btn_capture_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (str_app_type) {
                    case "P":
                        str_app_nw_type = "P";
                        break;
                    case "C":
                        str_app_nw_type = "COAPP" + str_app_no;

                        break;
                    case "G":
                        str_app_nw_type = "G" + str_app_no;
                        break;

                    default:
                        str_app_nw_type = "";
                }

                Intent pd_form = new Intent(getApplicationContext(), Image_List.class);
                pd_form.putExtra("str_losid", str_loan_no.trim());
                pd_form.putExtra("str_cust_id", str_cust_id.trim());
                pd_form.putExtra("str_ts", str_ts);
                pd_form.putExtra("app_type", str_app_nw_type.trim());
                pd_form.putExtra("app_type_nw", str_app_type.trim());

                startActivity(pd_form);


            }
        });

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GPSTracker_New tracker = new GPSTracker_New(Loan_Result.this);
                // check if location is available

                if (tracker.isLocationEnabled()) {
                    dbl_latitude = tracker.getLatitude();
                    dbl_longitude = tracker.getLongitude();

                    if (dbl_latitude != null) {

                        lattitude = String.valueOf(dbl_latitude);
                        longitude = String.valueOf(dbl_longitude);
                    }
                } else {
                    // show dialog box to user to enable location
                    tracker.askToOnLocation();
                }

                new Verify_los().execute(str_loan_no, str_cust_id, str_name, str_branch_id, str_branch_name, str_product,
                        str_app_type, lattitude, longitude, str_user_id, str_ts, str_app_no);


            }
        });


    }


    public class SearchLos extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Loan_Result.this);
            mProgressDialog.setMessage(getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            // txt_error.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            json = jsonobject;
            jsonobject = userFunction.loan_search(str_losid, str_cust_id);
            try {
                if (jsonobject != null) {
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        res = jsonobject.getString(KEY_STATUS);
                        resp_success = jsonobject.getString(KEY_SUCCESS);
                        String error_msg = jsonobject.getString("message");
                        if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                            str_name = jsonobject.getString("cust_name");
                            str_branch_id = jsonobject.getString("branch_id");
                            str_branch_name = jsonobject.getString("branch_name");
                            str_product = jsonobject.getString("product_id");
                            str_app_type = jsonobject.getString("app_type");
                            str_loan_no = jsonobject.getString("app_id");
                            str_cust_id = jsonobject.getString("cust_id");
                            int_is_verified = jsonobject.getInt("is_verified");

                            jsonarray = jsonobject.getJSONArray("Data");
                            if (jsonarray.length() > 0) {
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    HashMap<String, String> map = new HashMap<>();
                                    jsonobject = jsonarray.getJSONObject(i);
                                    map.put("img_name", jsonobject.getString("img_name"));
                                    map.put("img_data", jsonobject.getString("img_data"));
                                    map.put("img_no", jsonobject.getString("img_no"));
                                    map.put("img_count", jsonobject.getString("img_count"));


                                    // Set the JSON Objects into the array
                                    arraylist.add(map);
                                }
                            }

                        } else {
                            str_message = error_msg;
                        }
                    }
                } else {
                    str_message = "Something Went wrong please try again...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            TextView txt_name = (TextView) findViewById(R.id.txt_cust_name);
            TextView txt_branch_name = (TextView) findViewById(R.id.txt_branch_name);
            TextView txt_product = (TextView) findViewById(R.id.txt_product);
            TextView txt_app_type = (TextView) findViewById(R.id.txt_applicant_type);
            TextView txt_loan_no = (TextView) findViewById(R.id.txt_losid);


            if (jsonobject == null) {
                userFunction.cutomToast(
                        getResources().getString(R.string.error_message),
                        getApplicationContext());
                txt_error.setText(getResources().getString(R.string.error_message));
                txt_error.setVisibility(View.VISIBLE);

            } else {
                txt_error.setVisibility(View.GONE);

                if (Integer.parseInt(res) == 200 && resp_success.equals("true")) {

                    String str_type_app = null;
                    String type_app=null;
                    type_app = str_app_type;

                    if(type_app!=null) {
                        switch (type_app) {
                            case "P":
                                str_type_app = "Primary Applicant";
                                break;
                            case "C":
                                str_type_app = "CO-Applicant";
                                break;
                            case "G":
                                str_type_app = "Guarantor";
                                break;

                            default:
                                str_type_app = "";
                        }
                    }
                    adapter = new Loan_Result_Adapter(getApplicationContext(), arraylist);
                    adapter.notifyDataSetChanged();
                    mList.setAdapter(adapter);

                    txt_error.setVisibility(View.VISIBLE);
                    txt_message.setVisibility(View.GONE);

                    txt_name.setText(str_name);
                    txt_branch_name.setText(str_branch_name);
                    txt_product.setText(str_product);
                    txt_app_type.setText(str_type_app);
                    txt_loan_no.setText(str_loan_no);

                    if (int_is_verified == 0) {
                        btn_verify.setVisibility(View.VISIBLE);
                        btn_capture_pic.setVisibility(View.INVISIBLE);
                    } else {
                        btn_verify.setVisibility(View.INVISIBLE);
                        btn_capture_pic.setVisibility(View.VISIBLE);
                    }

                } else {
                    txt_message.setVisibility(View.VISIBLE);
                    // pd_details_layout.setVisibility(View.GONE);
                    txt_message.setText(str_message);
                }


            }
            mProgressDialog.dismiss();

        }

    }

    public class Verify_los extends AsyncTask<String, Void, Void> {

        protected void onPreExecute() {

            super.onPreExecute();
            mProgressDialog = new ProgressDialog(Loan_Result.this);
            mProgressDialog.setMessage(Loan_Result.this.getString(R.string.plz_wait));
            mProgressDialog.setCancelable(true);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            jsonobject = userFunction.verify_los(params[0], params[1], params[2], params[3], params[4],
                    params[5], params[6], params[7], params[8], params[9], params[10], params[11]);
            // Create an array

            return null;
        }

        @Override
        protected void onPostExecute(Void args) {

            if (jsonobject == null) {
//                userFunction.cutomToast(
//                        getResources().getString(R.string.error_message),
//                        getActivity().getApplicationContext());
                userFunction.cutomToast("unable to proceess your request", Loan_Result.this);
            } else {

                try {

                    String KEY_STATUS = "status";
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String KEY_SUCCESS = "success";
                        String resp_success = jsonobject.getString(KEY_SUCCESS);

                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {

                            userFunction.cutomToast("LOS verified  Successfully", Loan_Result.this);
                            btn_capture_pic.setVisibility(View.VISIBLE);
                            btn_verify.setVisibility(View.INVISIBLE);


                        } else {
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            mProgressDialog.dismiss();


        }

    }

    public void go_home_acivity(View v) {
        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

}


