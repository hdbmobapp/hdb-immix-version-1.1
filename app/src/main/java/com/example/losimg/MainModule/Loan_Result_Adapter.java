package com.example.losimg.MainModule;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;


import com.example.hdb.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DELL on 12/14/2017.
 */

public class Loan_Result_Adapter extends BaseAdapter {


    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    ArrayAdapter<String> adapter;
    ImageLoader img_loader;

    public Loan_Result_Adapter(Context context,
                               ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        TableRow row_remraks;

        img_loader = ImageLoader.getInstance();
        img_loader.init(ImageLoaderConfiguration.createDefault(context));
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }
    // Declare Variables

    @SuppressWarnings("unused")
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = null;
        // Get the position
        resultp = data.get(position);


        if (itemView == null) {
            // The view is not a recycled one: we have to inflate
            itemView = inflater.inflate(R.layout.dms_loan_result_adapter, parent,
                    false);


            holder = new ViewHolder();
            holder.tv_image_name = (TextView) itemView.findViewById(R.id.tv_image_name);
            holder.tv_img_count = (TextView) itemView.findViewById(R.id.tv_img_count);
            holder.img_img_pc = (ImageView) itemView.findViewById(R.id.img_img_pc);
            itemView.setTag(holder);


        } else {
            // View recycled !
            // no need to inflate
            // no need to findViews by id
            holder = (ViewHolder) itemView.getTag();
        }


        /*

        if (position % 2 == 1) {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        } else {
            itemView.setBackgroundColor(context.getResources().getColor(R.color.layout_back_color));
        }
*/


        //System.out.println("PR::::" + resultp.get(MyUploads.product) + ":::err code:::" + resultp.get(MyUploads.error_code) + ":::err desc:::" + resultp.get(MyUploads.error_desc));
        // holder.img_img_pc.setImageBitmap(decodedByte);
        // holder.img_img_pc.setText(resultp.get(Loan_upload_search.app_id));
        holder.tv_image_name.setText(resultp.get(Loan_Result.img_name));
        holder.tv_img_count.setText(resultp.get(Loan_Result.img_count));


        if (!resultp.get(Loan_Result.img_no).toString().contains("null")) {

            if (Integer.parseInt(resultp.get(Loan_Result.img_count).toString()) > 0) {
                holder.tv_img_count.setVisibility(View.VISIBLE);
            } else {
                holder.tv_img_count.setVisibility(View.GONE) ;

            }

        }else{
            holder.tv_img_count.setVisibility(View.GONE);

        }

        if (resultp.get(Loan_Result.img_no).toString().contains("null")) {
           // holder.img_img_pc.setImageDrawable(context.getDrawable(R.drawable.pending));
            Picasso.with(context).load(R.drawable.pending).into(holder.img_img_pc);

        }else{
           // holder.img_img_pc.setImageDrawable(context.getDrawable(R.drawable.uploaded_succ));
            Picasso.with(context).load(R.drawable.uploaded_succ).into(holder.img_img_pc);

        }

        /*
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            holder.img_img_pc.setBackground(R.drawable.pending);

        }
        */
        return itemView;
    }

    private static class ViewHolder {
        public TextView tv_image_name;
        public ImageView img_img_pc;
        public TextView tv_img_count;


    }


}

