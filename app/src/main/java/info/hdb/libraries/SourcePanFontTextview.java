package info.hdb.libraries;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class SourcePanFontTextview extends TextView {

    public SourcePanFontTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
   public SourcePanFontTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
   public SourcePanFontTextview(Context context) {
        super(context);
   }
   @Override
public void setTypeface(Typeface tf, int style) {
         if (style == Typeface.BOLD) {
              super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Semibold.ttf"));
          } else {
             super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Amaranth-Regular.otf"));
          }
    }
}