package info.hdb.libraries;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by obaro on 02/04/2015.
 */
public class ExampleDBHelper extends SQLiteOpenHelper {
    String urlString = "http://220.226.218.17:443/HDB_COLL_V2.9.1/img_upload_12.php";
    public static final String DATABASE_NAME = "collection_app.db";
    private static final int DATABASE_VERSION = 4;

    public static final String IMAGE_TABLE_NAME = "tbl_image_master";
    public static final String IMAGE_COLUMN_ID = "_id";
    public static final String IMAGE_LOSID = "img_losid";
    public static final String IMAGE_FORMNO = "img_formno";
    public static final String IMAGE_USERID = "img_userid";
    public static final String IMAGE_TS = "img_ts";
    public static final String IMAGE_MAPCOUNT = "img_map_count";
    public static final String IMAGE_STATUS = "img_status";
    public static final String IMAGE_LAST = "img_lastid";
    public static final String IMAGE_PATH = "img_path";

    public static final String IMAGE_CAPTION = "img_caption";

    public static final String KEY_STATUS = "status";
    private static String KEY_SUCCESS = "success";

    private static final String IMAGE_DIRECTORY_NAME = "/HDB";
ConnectionDetector cd;
    String str_user_id;
    UserFunctions userFunction;
    SharedPreferences pref;
    Context context;
    private static int VERSION = 1;
    private static String DBNAME = "HDBHMCV";
    ProgressDialog dialog;
    InputStream is = null;
    JSONObject jObj = null;
    String json = null;
    public ExampleDBHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        userFunction = new UserFunctions();
        this.context = context;
        cd = new ConnectionDetector(context);
        pref = context.getSharedPreferences("MyPref", 0);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        userFunction=new UserFunctions();

        db.execSQL(
                "CREATE TABLE " + IMAGE_TABLE_NAME +
                        "(" + IMAGE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        IMAGE_LOSID + " TEXT, " +
                        IMAGE_FORMNO + " TEXT, " +
                        IMAGE_USERID + " TEXT, " +
                        IMAGE_TS + " TEXT, " +
                        IMAGE_MAPCOUNT + " TEXT, " +
                        IMAGE_LAST + " TEXT, " +
                        IMAGE_PATH + " TEXT, " +
                        IMAGE_STATUS + " INTEGER, " +
                        IMAGE_CAPTION + " TEXT )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + IMAGE_TABLE_NAME);
        onCreate(db);
    }



    public boolean insertImage(String path,String losid,String userid,String ts,String status,String pic_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);


        contentValues.put(IMAGE_LOSID, losid);
        contentValues.put(IMAGE_USERID, userid);
        contentValues.put(IMAGE_TS, ts);

        contentValues.put(IMAGE_PATH, path);

        contentValues.put(IMAGE_CAPTION, pic_name);

        db.insert(IMAGE_TABLE_NAME, null, contentValues);
        return true;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, IMAGE_TABLE_NAME);
        return numRows;
    }

    public boolean updateImage(String ts,int status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(IMAGE_STATUS, status);
        db.update(IMAGE_TABLE_NAME, contentValues, IMAGE_TS + " = ? ", new String[] { ts } );
        return true;
    }

    public Integer deleteImage(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(IMAGE_TABLE_NAME,
                IMAGE_COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }

    public Cursor getImage(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " +
                IMAGE_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }
    public Cursor getImages() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE " + IMAGE_TS + ">? order by " + IMAGE_COLUMN_ID + " desc", new String[]{"0"});

        return res;
    }

    public Cursor getAllImages(String ts) {
        System.out.println("MYTS::::::" + ts);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + IMAGE_TABLE_NAME + " WHERE "+ IMAGE_TS  + "=? order by "+IMAGE_COLUMN_ID +" desc", new String[]{ts} );
        return res;
    }

    public String getPendingPhotoCount() {

        SQLiteDatabase db = this.getReadableDatabase();

        String countQuery_pic = "SELECT COUNT (" + IMAGE_PATH + ") FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_pic, null);

        System.out.println("countQuerylllll:" + countQuery_pic);

        String pending_count = "";

        try {
            if (cursor != null) {

                if (cursor.moveToNext()) {
                    pending_count = cursor.getString(0);
                    System.out.println("pending_count:" + pending_count);

                    return cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }
        return pending_count;
    }

    public String getphoto() {

        SQLiteDatabase db = this.getReadableDatabase();
        String image_path = null;


        String countQuery_val = "SELECT " + IMAGE_PATH +  "  FROM "
                + IMAGE_TABLE_NAME + " WHERE " + IMAGE_STATUS
                + "= 1";
        Cursor cursor = db.rawQuery(countQuery_val, null);

        System.out.println("countQuerymlop:" + countQuery_val);

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    image_path = cursor.getString(0);
                        upload_image(image_path);

                       // String cmp_iomage = compressImage(image_path, image_new_path, image_latitude, image_longitude);

                           // upload_image( image_path);

                    }
                }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // cursor.close();
            cursor.close();
            db.close();
        }
        //     closeConnection();

        return image_path;
        // System.out.println("countQuery:"+product_qnt);
    }

    public void upload_image(String img_path) {

        if (img_path != null) {
            //	Log.d("uploading File : ", img_path);
            File file1 = new File(img_path);
            if (file1.exists()) {
                // Log.d("uploading File Esists : ", "true");
                System.out.println("img_path::::" + img_path);
                if (cd.isConnectingToInternet()) {
                  //  doFileUpload(img_path);
                    uploadFile(img_path);
                } else {
                    userFunction.cutomToast("No internet connection...", context);
                }
            } else {
                this.deletepic(img_path);
                this.deletepic(img_path);

            }
        } else {

            try {
                if (Integer.parseInt(this.getPendingPhotoCount()) == 0 && !userFunction.isUserLoggedIn(context)) {


                    File mediaStorageDir;
                    String extStorageDirectory = Environment
                            .getExternalStorageDirectory()
                            .toString();

                    // External sdcard location
                    mediaStorageDir = new File(
                            extStorageDirectory
                                    + IMAGE_DIRECTORY_NAME);

                    if (mediaStorageDir.isDirectory()) {
                        try {
                            String[] children = mediaStorageDir
                                    .list();
                            for (int i = 0; i < children.length; i++) {
                                new File(mediaStorageDir, children[i]).delete();
                            }

                        } catch (Exception e) {
                            // block
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public boolean deletepic(String path) {
        File file = new File(path);
        Log.d("DELETING PIC ", path);
        boolean chkFlag = false;
        try {
            if (file.delete()) {
            }

            // mDB.execSQL(" DELETE FROM "+ DATABASE_PICTURE_TABLE +
            // " where  "+KEY_Photo1 +" ='"+path+"'");

            SQLiteDatabase db = this.getWritableDatabase();
            chkFlag = db.delete(IMAGE_TABLE_NAME, IMAGE_PATH + "=" + " '"
                    + path + "'", null) > 0;
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


        // return true;
        return chkFlag;
    }

    public void delete_pic() {
        SQLiteDatabase db = this.getWritableDatabase();

        String del_img = "delete from " + IMAGE_TABLE_NAME + " where " + IMAGE_STATUS + "= 0";

         db.execSQL(del_img);

        db.close();


    }
    public int uploadFile(final String selectedFilePath){
        File file1 = new File(selectedFilePath);

        int serverResponseCode = 0;
        String response = "";

        HttpsURLConnection connection;
        DataOutputStream dataOutputStream;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";


        int bytesRead,bytesAvailable,bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File selectedFile = new File(selectedFilePath);


        String[] parts = selectedFilePath.split("/");
        final String fileName = parts[parts.length-1];

        if (!selectedFile.isFile()){
            dialog.dismiss();


            return 0;
        }else{
            try{
                FileInputStream fileInputStream = new FileInputStream(selectedFile);
                URL url = new URL(urlString);
                connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);//Allow Inputs
                connection.setDoOutput(true);//Allow Outputs
                connection.setUseCaches(false);//Don't use a cached Copy
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                connection.setRequestProperty("uploadedfile1",selectedFilePath);

                //creating new dataoutputstream
                dataOutputStream = new DataOutputStream(connection.getOutputStream());

                //writing bytes to data outputstream
                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile1\";filename=\""
                        + selectedFilePath + "\"" + lineEnd);

                dataOutputStream.writeBytes(lineEnd);

                //returns no. of bytes present in fileInputStream
                bytesAvailable = fileInputStream.available();
                //selecting the buffer size as minimum of available bytes or 1 MB
                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                //setting the buffer as byte array of size of bufferSize
                buffer = new byte[bufferSize];

                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
                bytesRead = fileInputStream.read(buffer,0,bufferSize);

                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
                while (bytesRead > 0){
                    //write the bytes read from inputstream
                    dataOutputStream.write(buffer,0,bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }

                dataOutputStream.writeBytes(lineEnd);
                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();
                System.out.println("serverResponseMessage::"+serverResponseMessage);


                if(serverResponseCode == 200){
                    String line;
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"iso-8859-1"));
                    while ((line = bufferedReader.readLine()) != null) {
                        response += line;
                    }
                }

                if (response != null) {

                    try {

                        json = response.toString();
                    } catch (Exception e) {
                        Log.e("Buffer Error", "Error converting result " + e.toString());
                    }
                    // try parse the string to a JSON object
                    try {
                        jObj = new JSONObject(json);
                    } catch (JSONException e) {
                        Log.e("JSON Parser", "Error parsing data " + e.toString());
                    }
                } else {
                    jObj = null;
                }



                if (jObj != null) {
                    try {
                        System.out.println("response_str::::"+jObj);
                        if (jObj.getString(KEY_STATUS) != null) {
                            String res = jObj.getString(KEY_STATUS);
                            String resp_success = jObj.getString(KEY_SUCCESS);
                            if (Integer.parseInt(res) == 200
                                    && resp_success.equals("true")) {
                                deletepic(selectedFilePath);
                                file1.delete();
                            } else {
                                Log.i("RESPONSE MESSAGE ", jObj.getString("message").toString());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                //closing the input and output streams
                fileInputStream.close();
                dataOutputStream.flush();
                dataOutputStream.close();



            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (MalformedURLException e) {
                e.printStackTrace();
                Toast.makeText(context, "URL error!", Toast.LENGTH_SHORT).show();

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
            }
            //dialog.dismiss();
            return serverResponseCode;
        }

    }

}