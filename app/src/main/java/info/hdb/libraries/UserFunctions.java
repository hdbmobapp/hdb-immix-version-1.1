package info.hdb.libraries;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hdb.Age;
import com.example.hdb.AllocationList;
import com.example.hdb.Dashboard;
import com.example.hdb.HdbHome;
import com.example.hdb.R;
import com.example.hdb.RequestDB;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

@SuppressLint("NewApi")
public class UserFunctions extends Activity {

    private JSONParser jsonParser;
    SharedPreferences pref;
    private static String URL = "https://hdbapp.hdbfs.com/HDBFS_APPS/PD_COLLX/COLLX/index.php";
    private static String IndexURL = "https://hdbapp.hdbfs.com/HDBFS_APPS/PD_COLLX/COLLX/index_new.php";
    private static String IndexURLGET = "https://hdbapp.hdbfs.com/HDBFS_APPS/PD_COLLX/COLLX/index_get.php";
    private static String OTP_URL = "https://hdbapp.hdbfs.com/HDBFS_APPS/PD_COLLX/COLLX/otp.php";


    Context context;
    RequestDB rdb;
    SharedPreferences pData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //  setContentView(R.layout.setting);
    }

    // constructor
    public UserFunctions() {
        jsonParser = new JSONParser();

    }


    public JSONObject loginUser(String username, String password, String imeiNumber1, String imeiNumber2, String latitude, String longitude, String device_name, String device_man, String android_id, String str_api_level) {
        // Building Parameters
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "login");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("username", username);
            map.put("password", password);
            map.put("imei1", imeiNumber1);
            map.put("imei2", imeiNumber2);
            map.put("device_name", device_name);
            map.put("device_man", device_man);
            map.put("app_version", "Version 4.2");
            map.put("device_id", android_id);
            map.put("api_level", str_api_level);


            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);
            Log.e("JSON", json.toString());
            System.out.println("JSON" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public JSONObject GpsLoction(String user_id, String latitude, String longitude) {
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();


            map.put("action", "save");
            map.put("target", "location");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("user_id", user_id);
            map.put("app_version", "Version 4.2");


            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getLoan(String losid, String latitude, String longitude, String user_id, String designation_val, String designation) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "loan");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("losid", losid);
            map.put("user_id", user_id);
            map.put("designation", designation_val);
            map.put("designation_2", designation);

            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public void resetData(Context context) {
        pData = context.getSharedPreferences("pData", 0); // 0 -

        //String str_timestamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault().format(new Date();

        DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String str_timestamp = sdf.format(new Date());

        Editor dEditor = pData.edit();

        dEditor.putString("str_DOC_1", "");
        dEditor.putString("str_DOC_2", "");
        dEditor.putString("str_DOC_3", "");
        dEditor.putString("str_DOC_4", "");
        dEditor.putString("str_DOC_5", "");
        dEditor.putString("str_DOC_6", "");
        dEditor.putString("str_DOC_7", "");
        dEditor.putString("str_DOC_8", "");
        dEditor.putString("str_DOC_9", "");
        dEditor.putString("str_DOC_10", "");
        dEditor.putString("RAW", "");
        dEditor.putString("str_timestamp", str_timestamp);


        dEditor.commit();
    }

    public JSONObject getDashboard(String userid, String latitude, String longitude) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "new_dashboard");
            map.put("latitude", latitude);
            map.put("longitude", longitude);
            map.put("userid", userid);
            map.put("tag", "new_dashboard");

            Log.e("map : ", map.toString());
            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject InsertData(String locid,
                                 String customer_name,
                                 String acc_status,
                                 String PDD_Status,
                                 String Data_Entry,
                                 String Visit_Mode,
                                 String Number_Contacted,
                                 String Bucket_Type,
                                 String Met_Spoke,
                                 String DispoCode,
                                 String Next_Follow_Date,
                                 String Remarks,
                                 String New_Contact_Number,
                                 String New_Contact_Address,
                                 String New_Contact_Email,
                                 String Receipt_Number,
                                 String Total_Amount,
                                 String EMI_Amount,
                                 String Other_Amount,
                                 String Mode,
                                 String Cheque_Num,
                                 String PAN_no,
                                 String Form_60,
                                 String Coll_Sighted,
                                 String Coll_Condition,
                                 String third_Party,
                                 String third_Party_Name,
                                 String third_Party_Address,
                                 String Repo_Doable,
                                 String Photo1,
                                 String Photo2,
                                 String Photo3,
                                 String Photo4,
                                 String Photo5,
                                 String Photo6,
                                 String Photo7,
                                 String Photo8,
                                 String Photo9,
                                 String Photo10,
                                 String UserID,
                                 String Upload_Date,
                                 String GPS_Latitiude,
                                 String GPS_Longitude,
                                 String ts,
                                 String distance,
                                 String product,
                                 String bom_bkt, String risk_bkt, String lpp_due, String cbc_due,
                                 String str_BIZ_type_val, String str_PROPERTY_type_val, String str_Current_BIZ_type_val,
                                 String str_Not_able_pay_type_val, String str_Coll_Occupant_type_value,
                                 String str_cust_has_loan,
                                 String str_max_emi_amt_capble, String str_hand_loane_value, String str_loan_mnth_emi, String str_ssn, String data_entry_type, String pic_count,
                                 String str_ach_status, String str_distance_km, String str_distance_type, String str_map_address) {
        // Building Parameters
        JSONObject json = null;


        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "save");
            map.put("target", "loan");
            map.put("latitude", GPS_Latitiude);
            map.put("longitude", GPS_Longitude);
            map.put("losid", locid);
            map.put("customer_name", customer_name);
            map.put("account_status", acc_status);
            map.put("pdd_status", PDD_Status);
            map.put("data_entry", Data_Entry);
            map.put("visit_mode", Visit_Mode);
            map.put("number_contacted", Number_Contacted);
            map.put("bucket_type", Bucket_Type);
            map.put("met_spoke", Met_Spoke);
            map.put("dispocode", DispoCode);
            map.put("next_follow_date", Next_Follow_Date);
            map.put("remarks", Remarks);
            map.put("new_contact_number", New_Contact_Number);
            map.put("new_contact_address", New_Contact_Address);
            map.put("new_contact_email", New_Contact_Email);
            map.put("receipt_number", Receipt_Number);
            map.put("total_amount", Total_Amount);
            map.put("emi_amount", EMI_Amount);
            map.put("other_amount", Other_Amount);
            map.put("mode", Mode);
            map.put("cheque_num", Cheque_Num);
            map.put("pan_no", PAN_no);
            map.put("form_60", Form_60);
            map.put("coll_sighted", Coll_Sighted);
            map.put("coll_condition", Coll_Condition);
            map.put("third_party", third_Party);
            map.put("third_party_name", third_Party_Name);
            map.put("third_party_address", third_Party_Address);
            map.put("repo_doable", Repo_Doable);

            map.put("photo1", Photo1);
            map.put("photo2", Photo2);
            map.put("photo3", Photo3);
            map.put("photo4", Photo4);
            map.put("photo5", Photo5);
            map.put("photo6", Photo6);
            map.put("photo7", Photo7);
            map.put("photo8", Photo8);
            map.put("photo9", Photo9);
            map.put("photo10", Photo10);

            map.put("userid", UserID);
            map.put("upload_date", Upload_Date);
            map.put("ts", ts);
            map.put("distance", distance);

            map.put("product", product);
            map.put("bom_bkt", bom_bkt);
            map.put("risk_bkt", risk_bkt);
            map.put("lpp_due", lpp_due);
            map.put("cbc_due", cbc_due);
            map.put("str_version", "Version 2.9");

            map.put("biz", str_BIZ_type_val);
            map.put("property", str_PROPERTY_type_val);
            map.put("cur_biz", str_Current_BIZ_type_val);
            map.put("unable_to_pay_period", str_Not_able_pay_type_val);
            map.put("coll_occupant", str_Coll_Occupant_type_value);
            map.put("cust_has_hand_loans", str_cust_has_loan);

            map.put("max_emi_amt", str_max_emi_amt_capble);
            map.put("hand_loan_value", str_hand_loane_value);
            map.put("hand_loan_emi", str_loan_mnth_emi);
            map.put("settle_amt", str_ssn);
            map.put("entry_type", data_entry_type);

            map.put("pic_count", pic_count);

            map.put("ach_status", str_ach_status);
            map.put("distance_km", str_distance_km);
            map.put("distance_type", str_distance_type);
            map.put("map_address", str_map_address);

            System.out.println("REQ map " + map.toString());
            Log.e("REQ map", map.toString());
            json = jsonParser.makeHttpRequest_new(URL, "POST", map, context);

        } catch (Exception e) {

            e.printStackTrace();
        }

        System.out.println("JSON SAVE::::" + json);
        // Log.e("JSON", json.toString();
        return json;
    }


    /**
     * function make Login Request
     * @param name
     * @param email
     * @param password
     * */
    /**
     * Function get Login status
     */
    public boolean isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }

    public boolean off_isUserLoggedIn(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        if (!db.chkDBExist) {
            return false;
        }
        int count = db.off_getRowCount();
        if (count > 0) {
            // user logged in
            return true;
        }
        return false;
    }


    /**
     * Function to logout user
     * Reset Database
     */
    public boolean logoutUser(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();
        //this.cutomToast("Hope you had a wonderful day! Do check back soon.",context);

        return true;
    }

    public boolean logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.resetTables();

        return true;
    }

    public boolean off_logoutUserclear(Context context) {
        DatabaseHandler db = new DatabaseHandler(context);
        db.off_resetTables();

        return true;
    }


    public JSONObject getReportsGrid(String report_no, String str_user_id, String str_table_name) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "reports_dashboard");
            map.put("report_no", report_no);
            map.put("user_id", str_user_id);
            map.put("table_name", str_table_name);


            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("New json::::" + json);
        return json;
    }


    public JSONObject getACMList(String region, String flag, String str_type) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("region", region);
            map.put("flag", flag);
            map.put("tag", "ACMList");
            map.put("type", str_type);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
//		Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getCAlist(String region, String supervisor_id, String flag, String str_type) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("region", region);
            map.put("supervisor_code", supervisor_id);
            map.put("flag", flag);
            map.put("tag", "ACMList");
            map.put("type", str_type);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getMessageBox(String user_id, String page_no) {

        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("userid", user_id);
            map.put("tag", "message_box");
            map.put("pg", page_no);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getTodaysReport(String user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("userid", user_id);
            map.put("tag", "todays_details");
            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getAllocation(String user_id, String pg) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("userid", user_id);
            map.put("tag", "allocation");
            map.put("pg", pg);
            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getTodaysReportInfo(String user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("userid", user_id);
            map.put("tag", "details_for_user");
            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject GetPanIndiaData() {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "pan_india");

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject get_otp(String mobile_no, String rand_no,String otp_mode) {


        JSONObject json = null;
        try {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "send_otp");
            map.put("target", "send_message");
            map.put("mobile_no", mobile_no);
            map.put("otp", rand_no);
            map.put("otp_mode", otp_mode);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);


        } catch (Exception e) {

            e.printStackTrace();
        }
        // Log.e("JSON", json.toString());
        return json;
    }




    public JSONObject get_msg_count(String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "message_count");
            map.put("userid", str_user_id);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject update_message(String str_msg_id, String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "updateMessage");
            map.put("userid", str_user_id);
            map.put("msg_id", str_msg_id);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getCaseList(String str_user_id, String pgno) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "CaseList");
            map.put("userid", str_user_id);
            map.put("pgno", pgno);


            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject update_allocation(String str_los_id, String str_user_id, String str_me_status, String str_me_week) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "update_allocation");
            map.put("losid", str_los_id);
            map.put("userid", str_user_id);
            map.put("me_status", str_me_status);
            map.put("me_week", str_me_week);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getAcmTypeList(String str_region, String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tag", "search_ACM_type");
            map.put("userid", str_user_id);
            map.put("region", str_region);

            json = jsonParser.makeHttpRequest_new(IndexURL, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public void cutomToast(String message, Context contxt) {
        // Inflate the Layout
        ViewGroup parent = null;
        LayoutInflater inflater = (LayoutInflater) contxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_toast,
                parent, false);
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(contxt);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public void cutomToastAct(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_layout_id));
        TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
        mesg_name.setText(message);
        // Create Custom Toast
        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void noInternetConnection(String message, Context contxt) {
        Toast toast = null;
        if (toast == null
                || toast.getView().getWindowVisibility() != View.VISIBLE) {
            ViewGroup parent = null;
            LayoutInflater inflater = (LayoutInflater) contxt
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.toast_no_internet, parent,
                    false);
            TextView mesg_name = (TextView) layout.findViewById(R.id.textView1);
            mesg_name.setText(message);
            // Create Custom Toast
            toast = new Toast(contxt);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }


    public boolean dateCompare(String nDate) {
        boolean stat = false;

        Date current = new Date();

        String myFormatString = "dd/MM/yyyy";
        SimpleDateFormat df = new SimpleDateFormat(myFormatString);
        Date givenDate;
        try {
            givenDate = df.parse(nDate);
            if (givenDate.after(current)) {
                stat = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return stat;

    }

    public void notifyio(String msg, Context context) {
        // define sound URI, the sound to be played when there's a notification
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        cutomToast("Insdie:::", context);
        // intent triggered, you can add other intent for other actions
        Intent intent = new Intent(context, Dashboard.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // this is it, we'll build the notification!
        // in the addAction method, if you don't want any icon, just set the first param to 0
        Notification mNotification = new Notification.Builder(context)
                .setContentTitle("HDBFS")
                .setContentText(msg)
                .setSmallIcon(R.drawable.app_icon)
                .setContentIntent(pIntent)
                .setSound(soundUri)
                .addAction(R.drawable.app_icon, "View", pIntent)
                .addAction(0, "Remind", pIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // If you want to hide the notification after it was selected, do the code below
        // myNotification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, mNotification);
    }

    public Void checkforLogout(SharedPreferences pref, Context contextt) {
        String str_last_date = pref.getString("current_date", null);

        String str_last_date_off = pref.getString("current_date_off", null);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String current_date = sdf.format(new Date());

        Date date1 = null;
        Date date2 = null;
        Date date3 = null;
        try {
            if (current_date != null) {
                date1 = sdf.parse(current_date);
            }
            if (str_last_date != null) {
                date2 = sdf.parse(str_last_date);
            }

            if (str_last_date_off != null) {
                date3 = sdf.parse(str_last_date_off);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if (current_date != null && str_last_date != null) {
            if (date1.after(date2)) {
                Editor editor = pref.edit();
                editor.putInt("is_login", 0);
                editor.commit();

                if (current_date != null && str_last_date_off != null) {
                    if (date1.after(date2) && date1.after(date3)) {
                        Editor editor1 = pref.edit();

                        editor1.putInt("off_is_login", 0);
                        editor1.commit();
                        Intent i = new Intent(contextt, HdbHome.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contextt.startActivity(i);
                        finish();
                    }
                }
            }
        }
        return null;
    }


    public JSONObject insertRemark(String losid, String remark, String remark_by_id, String remark_by_name, String designation, String remark_id) {
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "save");
            map.put("target", "add_remarks");
            map.put("losid", losid);
            map.put("remark", remark);
            map.put("remark_by_id", remark_by_id);
            map.put("remark_by_name", remark_by_name);
            map.put("designation", designation);
            map.put("remarkid", remark_id);

            //  map.put("type", str_type);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
//		Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getRemarks(String losid, String parent_id)

    {
        JSONObject json = null;
//System.out.println("Remark ID=====:::::::::"+remark_id);

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("losid", losid);
            map.put("parentid", parent_id);
            map.put("action", "search");
            map.put("target", "search_remarks");

            //  map.put("type", str_type);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
//		Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getLoanTrails(String str_loan_no, String pgno) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "search");
            map.put("target", "new_menu");
            map.put("loan_no", str_loan_no);
            map.put("pgno", pgno);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getnewContactList(String str_loan_no, String pgno) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getcontactlist");
            map.put("target", "new_menu");
            map.put("loan_no", str_loan_no);
            map.put("pgno", pgno);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getLoanDetails(String str_loan_no, String str_reg_id, String pgno) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getlosdetails");
            map.put("target", "new_menu");
            map.put("losid", str_loan_no);
            map.put("reg_no", str_reg_id);
            map.put("pgno", pgno);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject get_masters(String id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getmasters");
            map.put("target", "new_menu");
            map.put("id", id);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_rem_data_gen(String product) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "new_target_prod");
            map.put("target", "get_product_res");
            map.put("product", product);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject submit_data(String str_prod, String str_loan_no, String str_cust_name,
                                  String str_amt, String str_cust_cont_no, String str_emp_cont_no,
                                  String str_user_id, String str_new_amt, String str_contact_no_new) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "ol_payment_insert");
            map.put("target", "ol_payment");
            map.put("Upload_By", str_user_id);
            map.put("str_prod", str_prod);
            map.put("Loan_no", str_loan_no);
            map.put("str_cust_name", str_cust_name);
            map.put("total_amt", str_amt);
            map.put("Cust_Contact_no", str_cust_cont_no);
            map.put("Emp_Contact_no", str_emp_cont_no);
            map.put("str_new_amt", str_new_amt);
            map.put("str_contact_no_new", str_contact_no_new);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject rcpt_generate(String str_loc_gen, String strs_emp_name, String str_loan_no, String strs_emp_code) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_rcpt_action");
            map.put("target", "get_rcpt_target");
            map.put("str_loc_gen ", str_loc_gen);
            map.put("username", strs_emp_name);
            map.put("Loan_no", str_loan_no);
            map.put("str_emp_code", strs_emp_code);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject InsertData_Ert(String str_pay_mode, String str_loan_no, String str_cust_name, String str_product, String str_branch_id, String str_branch, String str_contact_no,
                                     String str_unique_id, String str_total_emi_due, String str_pickup_type, String strs_emp_code, String strs_emp_name,
                                     String strs_chq_no, String strs_bank_name, String strs_chq_date,
                                     String strs_rcpt_date, String strs_emi_amt, String strs_lpp_amt,
                                     String strs_bcc_amt, String strs_coll_oth_chrg, String strs_total, String strs_remarks, String strs_pan_no, String str_rm_rs, String str_lat, String str_long,
                                     String str_pan, String strs_rcpt_amt, String str_brch_gst_sc, String str_branch_state,
                                     String str_cust_gst_sc, String str_cust_state, String str_gstn, String str_cust_id, String str_loc_gen, String str_rm_rs_gen,
                                     String str_rel, String str_otp_contact_no, String str_payee_name, String str_form60, String str_if_other, String str_email, String str_transcation_id, String str_ercpt_no, String str_product_name
                                    ,String str_from,String str_id) {


        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "ercpt_datasave_act");
            map.put("target", "ercpt_datasave_trg");

            map.put("str_Pay_Mode", str_pay_mode);
            map.put("str_Loan_No", str_loan_no);
            map.put("str_Customer_name", str_cust_name);
            map.put("str_Product", str_product);
            map.put("str_Branch_id", str_branch_id);
            map.put("str_Branch_name", str_branch);
            map.put("str_Contact_Number", str_contact_no);
            map.put("str_Uniqueid", str_unique_id);
            map.put("str_Total_Emi_Due", str_total_emi_due);
            System.out.println("str_Total_Emi_Due" + str_total_emi_due);
            map.put("str_Pickup_Type", str_pickup_type);
            map.put("str_Employee_Code", strs_emp_code);
            map.put("str_Employee_Name", strs_emp_name);
            map.put("str_Cheque_Number", strs_chq_no);
            map.put("str_Bank_Name", strs_bank_name);
            map.put("str_Cheque_Date", strs_chq_date);
            map.put("str_Receipt_Date", strs_rcpt_date);
            map.put("str_EMI_Amount", strs_emi_amt);
            map.put("str_LPP_Amount", strs_lpp_amt);
            map.put("str_BCC_Amount", strs_bcc_amt);
            map.put("str_Coll_Charges", strs_coll_oth_chrg);
            map.put("str_Grand_Total", strs_total);
            map.put("str_Remarks", strs_remarks);
            map.put("str_Pan_No", strs_pan_no);
            map.put("str_Remarks_Reason", str_rm_rs);
            map.put("str_lat", str_lat);
            map.put("str_long", str_long);
            map.put("str_Pan_No_Available", str_pan);
            map.put("str_form60", str_form60);


            map.put("strs_rcpt_amt", strs_rcpt_amt);
            map.put("str_brch_gst_sc", str_brch_gst_sc);
            map.put("str_branch_state", str_branch_state);
            map.put("str_cust_gst_sc", str_cust_gst_sc);
            map.put("str_cust_state", str_cust_state);
            map.put("str_gstn", str_gstn);
            map.put("str_cust_id", str_cust_id);
            map.put("str_loc_gen", str_loc_gen);
            map.put("str_rm_rs_gen", str_rm_rs_gen);

            map.put("str_payee_name", str_payee_name);
            map.put("str_otp_contact_no", str_otp_contact_no);
            map.put("str_rel", str_rel);
            map.put("str_if_other", str_if_other);
            map.put("str_email", str_email);
            map.put("str_transcation_id", str_transcation_id);
            map.put("e_rcpt_no", str_ercpt_no);
            map.put("str_product_name", str_product_name);


            map.put("str_from", str_from);
            map.put("str_id", str_id);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject get_lms_Locc_data(String losid,String user_id,String str_lat,String str_long) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "lms_action");
            map.put("target", "lms_target");
            map.put("losid_n", losid);
            map.put("user_id", user_id);
            map.put("str_lat", str_lat);
            map.put("str_long", str_long);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject get_los_genrr_data(String losid,String user_id,String str_lat,String str_long) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "los_action");
            map.put("target", "los_target");
            map.put("losid_n", losid);
            map.put("user_id", user_id);
            map.put("str_lat", str_lat);
            map.put("str_long", str_long);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_rem_data() {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getmasterrem");
            map.put("target", "new_target");

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject check_masters(String flag_dc, String flag_pm, String id_dc, String id_pm, String flag_pd, String id_pd) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "verify_masters");
            map.put("target", "new_menu");
            map.put("flag_dc", flag_dc);
            map.put("flag_pm", flag_pm);
            map.put("id_dc", id_dc);
            map.put("id_pm", id_pm);

            map.put("flag_pd", flag_pd);
            map.put("id_pd", id_pd);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public void send_Alloc_notification(String msg, Context context) {

        Intent intent = new Intent(context, AllocationList.class);


        int icon = R.drawable.app_icon;

        int mNotificationId = 001;

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context);
        Notification notification = mBuilder.setSmallIcon(icon).setTicker("HDB-COLLX").setWhen(0)
                .setAutoCancel(true)
                .setContentTitle("HDB-COLLX")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentIntent(resultPendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon))
                .setContentText(msg).build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationId, notification);
    }

    public JSONObject getFAQList(String flag, String str_keywrd, String pgno) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "faq");
            map.put("target", "new_menu");
            map.put("flag", flag);
            map.put("flag", pgno);

            map.put("str_keyword", str_keywrd);
            map.put("str_app_name", "collx");


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getFAQ_LangList(String faq_id) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "faqs_lang_master");
            map.put("target", "new_menu");

            map.put("faq_id", faq_id);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject checkContactdetails(String losid, String new_cont_no) {
        // Building Parameters
        JSONObject json = null;

        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "getcontactverify");
            map.put("target", "new_menu");

            map.put("losid", losid);
            map.put("new_cont_no", new_cont_no);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Log.e("JSON", json.toString();
        return json;
    }

    private static Age calculateAge(Date birthDate) {
        int years = 0;
        int months = 0;
        int days = 0;
        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());
        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        // System.out.println("CURR TIME::::"+);
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);
        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;
        //Get difference between months
        months = currMonth - birthMonth;
        //if month difference is in negative then reduce years by one and calculate the number of months.
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
            months = 11;
        }
        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
        } else {
            days = 0;
            if (months == 12) {
                years++;
                months = 0;
            }
        }
        //Create new Age object
        return new Age(days, months, years);
    }

    public String dateCompare(int day, int month, int year, String curr_date) {
        String stat;


        String[] parts = curr_date.split("-");
        String nsd = parts[2];
        String mnth = parts[1];
        String days = parts[0];


        System.out.println("YEAR:::" + nsd + "MONTHS::::" + mnth + "DAYS::::" + days);


        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");


        String bday = day + "/" + month + "/" + year;

        Date birthDate = null;

        try {
            birthDate = sdf.parse(bday);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Yeh !! It's my date of birth :-)
        Age age = calculateAge(birthDate);
        if (age.years < 18) {
            stat = "";
        } else {
            stat = "Age : " + String.valueOf(age.years);
        }

        return stat;

    }

    public JSONObject getCO_PDCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_coll");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);
            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject getCU_PDCaseList(String user_id, String losid_no, String app_form_no, String pg_no) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_pd");
            map.put("target", "loan");
            map.put("userid", user_id);
            map.put("losid", losid_no);
            map.put("app_form_no", app_form_no);

            map.put("pg", pg_no);

            map.put("losid_form_no", losid_no);
            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject Update_details(String str_mail, String str_mobile, String str_uid) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("target", "login");
            params.put("action", "update");
            params.put("email", str_mail);
            params.put("mobile", str_mobile);
            params.put("userid", str_uid);


            json = jsonParser.makeHttpRequest_new(URL, "POST", params, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject getAdditonalmenus(String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("action", "additional_menus");
            params.put("target", "new_menu");

            params.put("user_id", str_user_id);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", params, UserFunctions.this);

        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e("JSON", json.toString());
        return json;
    }




    public JSONObject get_pickup_data(String str_user_id,String str_schedule) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "get_pickdata_action");
            map.put("target", "get_pickdata_target");
            map.put("str_user_id",str_user_id);
            map.put("str_schedule",str_schedule);

            System.out.println("str_schedule::::::::::::::::::" + str_user_id);
            System.out.println("str_schedule::::::::::::::::::" + str_schedule);
            System.out.println("json::::::::::::::::::" + json);
            System.out.println("json::::::::::::::::::" + json);

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            // System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }

    public JSONObject get_pickup_status() {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "pickup_action");
            map.put("target", "pickup_target");

            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);
            // System.out.println("json::::::::::::::::::" + json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


    public JSONObject insertpickupdata(String str_status,String str_rmk,String str_lat,String str_long,String str_id,String str_user_id) {
        JSONObject json = null;
        try {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("action", "insert_pickupdata_action");
            map.put("target", "insert_pickupdata_target");


            map.put("str_id",str_id);
            map.put("str_status", str_status);
            map.put("str_rmk",str_rmk);
            map.put("str_lat",str_lat);
            map.put("str_long",str_long);
            map.put("str_upload_by",str_user_id);


            json = jsonParser.makeHttpRequest_new(IndexURLGET, "POST", map, context);


        } catch (Exception e) {
            e.printStackTrace();
        }
        //Log.e("JSON", json.toString();
        return json;
    }


}

