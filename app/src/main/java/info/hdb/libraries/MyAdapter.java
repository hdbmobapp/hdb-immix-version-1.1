package info.hdb.libraries;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay on 5/6/2016.
 */
public class MyAdapter extends ArrayAdapter<String> {

    int lay_id;
    Context context;

    private LayoutInflater layinf;
    private List<String> mData;
    private ArrayList<String> productdata;
    private Spinner spn;

//    Typeface face= Typeface.createFromAsset(getContext().getAssets(), "font/Verdana.ttf");

    public MyAdapter(Activity context, int res, int text, List<String> items) {
        super(context, res, text, items);
        this.lay_id = res;
        this.context = context;

        layinf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public MyAdapter(Activity context, int res, int text, String[] items) {
        super(context, res, text, items);
        this.lay_id = res;
        this.context = context;

        layinf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public MyAdapter(Activity context, int res, String[] items) {
        super(context, res, items);
        this.lay_id = res;
        this.context = context;

        layinf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public MyAdapter(Activity context, int res, int text, ArrayList<String> items) {
        super(context, res, text, items);
        this.lay_id = res;
        this.context = context;

        layinf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(pos, convertView, parent);
        View view1 = super.getView(pos, convertView, parent);


        view.setBackground(null);
        view.setPadding(0, 0, 0, 0);
        // view.setTypeface(face);
        view.setTextColor(Color.BLACK);

        if (pos == 0) {
            view.setTextColor(Color.RED);
           // view.setBackground(context.getResources().getDrawable(R.color.blue_gray));
        } else {
            view.setTextColor(Color.BLACK);
        }

        return view;
    }

    @Override
    public View getDropDownView(int pos, View convertView, ViewGroup parent) {

        TextView view = (TextView) super.getView(pos, convertView, parent);
        //view.setTypeface(face);
        return view;
    }
}
