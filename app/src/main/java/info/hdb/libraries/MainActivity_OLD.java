package info.hdb.libraries;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hdb.Dashboard;
import com.example.hdb.HdbHome;
import com.example.hdb.R;
import com.example.hdb.RequestDB;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity_OLD extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    File new_file = null;
    ConnectionDetector cd;
    UserFunctions userFunctions;
    int REQUEST_CAMERA = 0;
    //RequestDB rdb;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private String imgPath;

    private static final String IMAGE_DIRECTORY_NAME = "/HDB";

    ExampleDBHelper dbHelper;

    SharedPreferences pref;
    String str_uri;
    RequestDB rDB;
    File file;
    int pic_position = 0;
    ArrayList<String> pic_list;
    ArrayList<String> pic_values;
    ArrayList<String> cu_pic_values;
    ArrayList<String> cu_pic_names;


    //HashMap<String,String> cu_pic_name =new HashMap<String,String>();


    String str_pic_catpure_list = null;
    String str_pic_catpure_list1 = null;
    ArrayAdapter<String> adapter_pic_catpure;
    Spinner spn_pic_catpure;
    Integer int_total_pic;
    List<String> list;
    List<String> list1;
    String losid, form_no, userid, ts, map_count, last_id, id;
    String str_position;
    String str_total_pic_count, str_pic_name;
    Integer total_pic_count;

    String pic_cnt;

    String str_coll_sigh;

    String map_lat, map_long, ts_date;

    String str_label;

    private Uri url_file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setTheme(R.style.AppThemerr);

        setContentView(R.layout.activity_main_old);


        // StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        // StrictMode.setThreadPolicy(policy);


        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

       /* ActionBar home = getActionBar();
        home.setDisplayShowHomeEnabled(false);
        home.setDisplayShowTitleEnabled(false);

        LayoutInflater home_inflater = LayoutInflater.from(this);

        View home_view = home_inflater.inflate(R.layout.homelayout, null);
        TextView home_title = (TextView) home_view.findViewById(R.id.txt_title);
        home_title.setText("IMAGE UPLOAD");

        home.setCustomView(home_view);
        home.setDisplayShowCustomEnabled(true);*/

        cu_pic_names = new ArrayList<String>();
        cu_pic_values = new ArrayList<String>();

        pic_values = new ArrayList<String>();
        pic_list = new ArrayList<String>();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        str_coll_sigh = pref.getString("coll_sighted", "no");


        dbHelper = new ExampleDBHelper(this);
        userFunctions = new UserFunctions();
        cd = new ConnectionDetector(this);
        rDB = new RequestDB(this);

        Intent i = getIntent();

        id = i.getStringExtra("id");
        losid = i.getStringExtra("cus_losid");
        userid = i.getStringExtra("cus_userid");
        ts = i.getStringExtra("cus_ts");
        pic_cnt = i.getStringExtra("cus_pic_count");
        str_label = i.getStringExtra("visit_mode");
        ts_date = i.getStringExtra("str_date");
        map_lat = i.getStringExtra("lattitude");
        map_long = i.getStringExtra("longitude");
        map_count = i.getStringExtra("coll_map");


        cu_pic_values.clear();
        cu_pic_names.clear();


        cu_pic_values.add("0");
        cu_pic_values.add("1");
        cu_pic_values.add("2");
        cu_pic_values.add("3");
        cu_pic_values.add("4");


        cu_pic_names.add("Select Image Type");
        cu_pic_names.add(str_label + " " + "Image 1");
        cu_pic_names.add(str_label + " " + "Image 2");
        cu_pic_names.add(str_label + " " + "Image 3");
        cu_pic_names.add(str_label + " " + "Image 4");

        if (str_coll_sigh == null || str_coll_sigh.isEmpty() || str_coll_sigh.equals("null")) {

        } else {

            if (str_coll_sigh.equals("yes")) {
                cu_pic_values.add("5");
                cu_pic_values.add("6");
                cu_pic_values.add("7");

                cu_pic_names.add("Collateral 1");
                cu_pic_names.add("Collateral 2");
                cu_pic_names.add("Others");


            }
        }

        //}*/

        pic_list = cu_pic_names;
        pic_values = cu_pic_values;


        //   ts_date = i.getStringExtra("ts_date");

        //  last_id = i.getStringExtra("cus_last");

        list = pic_values;
        list1 = pic_list;
        //  list = new ArrayList<String>(Arrays.asList(pic_values));

        ImageView btnSelectPhoto = (ImageView) findViewById(R.id.btnSelectPhoto);
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_pic_catpure_list != null) {
                    takePicture();
                    /*
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            getOutputMediaFile(MEDIA_TYPE_IMAGE));
                    startActivityForResult(intent, REQUEST_CAMERA);
                    */

                } else {
                    Toast.makeText(MainActivity_OLD.this, "Select Image category.....", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });


        Button btnSubmitPhoto = (Button) findViewById(R.id.btnsubmitpic);
        btnSubmitPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (pic_cnt == null || pic_cnt.equals(null)) {
                    Long status_co = rDB.update_status("1", str_total_pic_count, ts);
                    System.out.println("status_co::::1:::" + status_co);

                    if (cd.isConnectingToInternet() == true) {
                        if (status_co > 0) {
                            dbHelper.updateImage(ts, 1);
                            rDB.getrequestForDashboard(ts, "ON");
                        }
                    } else {

                        dbHelper.updateImage(ts, 1);
                        userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                        Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                        startActivity(cust);
                        finish();
                    }
                }


                if (Integer.valueOf(pic_cnt) == 0) {
                    if (total_pic_count >= 3) {
                        Long status_co = rDB.update_status("1", str_total_pic_count, ts);
                        System.out.println("status_co::::2:::" + status_co);

                        if (cd.isConnectingToInternet() == true) {
                            if (status_co > 0) {
                                dbHelper.updateImage(ts, 1);
                                rDB.getrequestForDashboard(ts, "ON");
                            }
                        } else {
                            dbHelper.updateImage(ts, 1);
                            userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                            Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                            startActivity(cust);
                            finish();
                        }
                    } else {
                        userFunctions.cutomToast(getResources().getString(R.string.at_least_pic), MainActivity_OLD.this);
                    }
                } else {

                    Long status_co = rDB.update_status("1", str_total_pic_count, ts);
                    System.out.println("status_co::::3:::" + status_co);
                    if (cd.isConnectingToInternet() == true) {
                        if (status_co > 0) {
                            dbHelper.updateImage(ts, 1);
                            rDB.getrequestForDashboard(ts, "ON");
                        }
                    } else {
                        dbHelper.updateImage(ts, 1);
                        userFunctions.cutomToast(getResources().getString(R.string.local_data), MainActivity_OLD.this);

                        Intent cust = new Intent(MainActivity_OLD.this, Dashboard.class);
                        startActivity(cust);
                        finish();
                    }
                }

            }

        });

        spn_pic_catpure = (Spinner) findViewById(R.id.spn_pic_capture);

        //  ivImage = (ImageView) findViewById(R.id.ivImage);

        adapter_pic_catpure = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, list1);
        adapter_pic_catpure
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_pic_catpure.setAdapter(adapter_pic_catpure);
        spn_pic_catpure.setOnItemSelectedListener(this);

        setlistview();
    }


    public void setlistview() {
        dbHelper = new ExampleDBHelper(this);


        final Cursor cursor = dbHelper.getAllImages(ts);
        total_pic_count = cursor.getCount();
        GridView lvItems = (GridView) findViewById(R.id.listView1);
        // Find ListView to populate
        // Setup cursor adapter using cursor from last step
        ImageListAdapter todoAdapter = new ImageListAdapter(this, cursor, 0);
        // Attach cursor adapter to the ListView
        lvItems.setAdapter(todoAdapter);
        Integer int_map_count = Integer.parseInt(map_count);
        int_total_pic = total_pic_count;
        if (int_map_count > 0) {
            total_pic_count = int_total_pic - 1;
        }
        str_total_pic_count = String.valueOf(total_pic_count);
    }


    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
                               long arg3) {

        switch (arg0.getId()) {
            case R.id.spn_pic_capture:
                str_pic_catpure_list = pic_values.get(pic_list.indexOf(list1.get(position)));
                str_pic_catpure_list1 = list1.get(position);
                // str_pic_catpure_list =list1.get(position);
                //String.valueOf(position) ;
                str_position = String.valueOf(position);
                str_pic_name = list1.get(position);
                SharedPreferences.Editor peditor = pref.edit();
                peditor.putString("str_pic_name", str_pic_name);
                peditor.putString("str_pic_catpure_list", str_pic_catpure_list);

                peditor.commit();

                pic_position = Integer.parseInt(str_position);
                if (position == 0) {
                    str_pic_catpure_list = null;
                }

                break;

            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


    public void go_home_acivity(View v) {

        // do stuff
        Intent home_activity = new Intent(getApplicationContext(),
                HdbHome.class);
        startActivity(home_activity);
        finish();
    }

    private String storeImage(Bitmap image, String userid, String losid, String ts, String str_pic_name) {
        String new_strMyImagePath = null;
        File pictureFile = getOutputMediaFile2(userid, losid, ts, str_pic_name);
        if (pictureFile == null) {
            return new_strMyImagePath;
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                fos.close();
                //pictureFile=File.
            } catch (FileNotFoundException e) {

            } catch (IOException e) {

            }
            new_strMyImagePath = pictureFile.getAbsolutePath();

            return new_strMyImagePath;
        }

    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile2(String userid, String losid, String ts, String str_pic_name) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        */
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB_COLLX");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        String mImageName = userid + "_" + losid + "_" + ts + "_COLLX_" + str_pic_name + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File new_file3 = getOutputMediaFile1235();
        System.out.println(":::::111111" + url_file);
        System.out.println(":::::111111" + str_pic_catpure_list);


        url_file = Uri.fromFile(new_file3);
        SharedPreferences.Editor peditor = pref.edit();
        peditor.remove(str_uri);
        peditor.commit();

        str_uri = url_file.toString();

        peditor.putString("str_uri", str_uri);
        peditor.commit();

        System.out.println(":::::111111" + url_file);

        System.out.println(":::::222222");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, url_file);
        System.out.println(":::::333333");
        startActivityForResult(intent, 100);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {


                str_uri = pref.getString("str_uri", null);
                str_pic_name = pref.getString("str_pic_name", null);
                url_file = Uri.parse(str_uri);
                //imageView.setImageURI(file);
                if (url_file != null) {
                    String str_img_path = url_file.toString();
                    File new_img_file = new File(url_file.toString());
                    str_img_path = new_img_file.getAbsolutePath();

                    String ssstr_img_path = null;
                    try {
                        str_img_path = str_img_path.replace("/file:", "");
                        System.out.println("Insite  try:::" + str_img_path);
                        str_pic_catpure_list = pref.getString("str_pic_catpure_list", null);
                        Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(str_img_path, 600, 400, userid, losid, ts);
                        ssstr_img_path = storeImage(bitmap, userid, losid, ts, str_pic_catpure_list);
                        // System.out.println("Insite  try:::"+str_img_path);
                    } catch (Exception e) {
                        System.out.println("Insite  catch:::" + str_img_path);

                        ssstr_img_path = new_img_file.getAbsolutePath();
                        ssstr_img_path = ssstr_img_path.replace("/file:", "");

                    }

                    if (str_uri != null) {
                        // show the result here.
                        if (dbHelper.insertImage(ssstr_img_path, losid, userid, ts, "0", str_pic_name)) {
                            //  Toast.makeText(getApplicationContext(), "iMAGE Inserted", Toast.LENGTH_SHORT).show();

                            boolean ss = list.contains(str_pic_catpure_list);
                            if (ss == true) {
                                list.remove(str_pic_catpure_list);
                                list1.remove(str_pic_catpure_list1);
                                adapter_pic_catpure = new ArrayAdapter<>(this,
                                        android.R.layout.simple_spinner_dropdown_item, list1);
                                adapter_pic_catpure
                                        .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spn_pic_catpure.setAdapter(adapter_pic_catpure);
                                spn_pic_catpure.setOnItemSelectedListener(this);


                                Toast.makeText(getApplicationContext(), "IMAGE", Toast.LENGTH_SHORT).show();

                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Could not Insert IMAGE", Toast.LENGTH_SHORT).show();
                        }
                        setlistview();
                    }
                }
            } else {

                userFunctions.cutomToast("Could not capture Image Try again", getApplicationContext());
            }
        }

    }

    private File getOutputMediaFile1235() {

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "HDB_COLLX");
        /*
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
                 */
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraDemo", "failed to create directory");
                return null;
            }
        }
        System.out.println(":::::111111" + str_pic_catpure_list);
        str_pic_catpure_list = pref.getString("str_pic_catpure_list", null);

        new_file = new File(mediaStorageDir.getPath() + File.separator + userid + "_" + losid + "_" + ts + "_COLLX_" + str_pic_catpure_list + ".JPEG");
        System.out.println("new_file:::" + new_file);
        if (new_file != null) {
            return new_file;
        } else {
            return null;
        }
    }
}
