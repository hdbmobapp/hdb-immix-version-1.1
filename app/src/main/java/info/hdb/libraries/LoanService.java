package info.hdb.libraries;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;

import com.example.hdb.Dashboard;
import com.example.hdb.R;
import com.example.hdb.RequestDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

@SuppressLint("NewApi")
public class LoanService extends Service {

	public Context context = this;
	public Handler handler = null;
	public static Runnable runnable = null;

	RequestDB requestdb;
	UserFunctions userfunction;

	private static final String IMAGE_DIRECTORY_NAME = "/HDB";

    JSONObject jsonobject;
    String GPS_Latitiude;
    String GPS_Longitude;
    SharedPreferences pref;
    String str_user_id;

    private static String KEY_SUCCESS = "success";
    private static String KEY_STATUS = "status";
    private ConnectionDetector cd;
	public static Runnable runnable_123 = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		cd = new ConnectionDetector(getApplicationContext());
        userfunction=new UserFunctions();
		// Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();
		handler = new Handler();
		runnable = new Runnable() {
			@Override
			public void run() {

				try {

					requestdb = new RequestDB(LoanService.this);
					userfunction = new UserFunctions();
					//if (cd.isConnectingToInternet()) {
						requestdb.getrequest();
					//}

                   insertLocation();
					try {
						if (Integer.parseInt(requestdb.getPendingCount()) == 0) {
							// Only Starts if all the Requests are updated.
							String img_path = requestdb.getphoto();
							if (img_path != null) {
							//	Log.d("uploading File : ", img_path);
								File file1 = new File(img_path);
								if (file1.exists()) {
	//								Log.d("uploading File Esists : ", "true");
									if (cd.isConnectingToInternet()) {
										//requestdb.doFileUpload(img_path);
									}
								} else {
//									Log.d("uploading File dose not Esists : ",
//											"true");
									requestdb.deletepic(img_path);
								}
							} else {

								try {
									if (Integer.parseInt(requestdb
											.getPendingPhotoCount()) == 0
											&& !userfunction
													.isUserLoggedIn(getApplicationContext())) {
//										Log.d("No Item to upload in DB ",
//												"TRUE");

										File mediaStorageDir;

										String extStorageDirectory = Environment
												.getExternalStorageDirectory()
												.toString();


										// External sdcard location
										mediaStorageDir = new File(
												extStorageDirectory
														+ IMAGE_DIRECTORY_NAME);

										if (mediaStorageDir.isDirectory()) {
											try {
												String[] children = mediaStorageDir
														.list();
												for(int i = 0; i < children.length; i++) {
												//	Log.d("Auto Delete ", children[i]);
													new File(mediaStorageDir,children[i]).delete();
												}
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

						}
					} catch (Exception e1) {
						e1.printStackTrace();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				// Toast.makeText(context,
				// "Service is still running lon service",
				// Toast.LENGTH_LONG).show();
				// requestdb.closeConnection();
				handler.postDelayed(runnable, 2600000);
			}
		};

		handler.postDelayed(runnable, 2600000);


		runnable_123 = new Runnable() {
			@Override
			public void run() {
				try {
					boolean is_notifyed = false;

					Integer int_collx_pending_count = 0;
					Integer PendingPhotoCoun = 0;

					requestdb = new RequestDB(LoanService.this);
					userfunction = new UserFunctions();

					int_collx_pending_count = Integer.parseInt(requestdb.getPendingCount());

					if (int_collx_pending_count > 0) {
						is_notifyed = true;
					}

					PendingPhotoCoun = Integer.parseInt(requestdb.getPendingPhotoCount());
					if (PendingPhotoCoun > 0) {
						is_notifyed = true;
					}

					if(is_notifyed==true) {
						Integer my_count = int_collx_pending_count +
								PendingPhotoCoun;
						if(my_count>0) {
							send_notification("You have " + my_count + " Files Pending in your Mobile Click here ....", context);
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				handler.postDelayed(runnable_123, 1800000);
			}
		};
		handler.postDelayed(runnable_123, 1800000);

	}

	@Override
	public void onDestroy() {
		/*

		 * IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE
		 * FOLLOWING LINE
		 */
		// handler.removeCallbacks(runnable);
		// Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();
	}

	@SuppressWarnings("deprecation")
    @Override
	public void onStart(Intent intent, int startid) {
		// Toast.makeText(this, "Service started by user.",
		// Toast.LENGTH_LONG).show();
	}
public void insertLocation(){

        try {
            pref = getApplicationContext().getSharedPreferences(
                    "MyPref", 0); // 0 -

            str_user_id = pref.getString("user_id", null);
           // GPS_Latitiude = String.valueOf(gps.getLatitude());
           // GPS_Longitude = String.valueOf(gps.getLongitude());
            requestdb = new RequestDB(LoanService.this);
            userfunction = new UserFunctions();
            if (cd.isConnectingToInternet()) {
                if (str_user_id != null) {
                    new InsertData().execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



}


    public class InsertData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // then do your work
            jsonobject = userfunction.GpsLoction(str_user_id, GPS_Latitiude,
                    GPS_Longitude);
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            // check for login response
            // check for login response
            try {
                if(jsonobject!=null){
                    if (jsonobject.getString(KEY_STATUS) != null) {
                        String res = jsonobject.getString(KEY_STATUS);
                        String resp_success = jsonobject.getString(KEY_SUCCESS);
                        if (Integer.parseInt(res) == 200
                                && resp_success.equals("true")) {
                            //notifyio();
                        } else {
                            // String
                            // error_msg=jsonobject.getString("message").toString();
                            // userFunction.cutomToast(error_msg, RequestDB.this);

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


	public void send_notification(String msg, Context context) {

		Intent intent = new Intent(context, Dashboard.class);


		int icon = R.drawable.app_icon;

		int mNotificationId = 001;

		PendingIntent resultPendingIntent =
				PendingIntent.getActivity(
						context,
						0,
						intent,
						PendingIntent.FLAG_CANCEL_CURRENT
				);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context);
		Notification notification = mBuilder.setSmallIcon(icon).setTicker("HDB-COLLX").setWhen(0)
				.setAutoCancel(true)
				.setContentTitle("HDB-COLLX")
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setContentIntent(resultPendingIntent)
				.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon))
				.setContentText(msg).build();

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(mNotificationId, notification);
	}
}